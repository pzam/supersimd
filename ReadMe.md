![](/doc/logo/SuperSIMDX.svg)


SuperSIMD is a Low-Level inlineable SIMD programming library using an extended version Intel Intrinsics standard. 
It aims to provide compatibility with Intel Short Vector Math Library (SVML), a portable version of AMD's eXtended Operations (XOP), FMA4(AMD) to FMA3(Intel) compatibility and a compile target to ARM NEON native instruction in addition to Intel FMA/SSE family native instruction. Library is maintained to work on MSVC, GCC/G++ and Clang.


----------

In your  C/C++ program just include our header...


    #include <supersimd.h>

----------

## MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


## About SuperSIMD API

See the IntrinsicsGuide.html in the Intrinsics-Guide folder.