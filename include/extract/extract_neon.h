/***************************************************************************************************/
// HEADER_NAME /extract/extract_neon.h
/***************************************************************************************************/
#if ARM
#ifndef _MM_SHUFFLE8
#define _MM_SHUFFLE8(z, y, x, w, v, u, t, s)									\
																				\
					(((z) << 21) | ((y) << 18) | ((x) << 15) | ((w) << 12)) |	\
					((v) << 9) | ((u) << 6) | ((t) << 3) | ((s)))
#endif
//MMX
// INTEGER
#define _mm_extract_pi8( a, imm8) vget_lane_s8(a, imm8);
#define _mm_extract_pu8( a, imm8) vget_lane_u8(a, imm8);
//__int16 _mm_extract_pi16(_m64 a, int imm8)			//Implemented by SSE
#define _mm_extract_pu16( a, imm8) vget_lane_u16(a, imm8)
#define _mm_extract_pi32( a, imm8) vget_lane_s32(a, imm8)
#define _mm_extract_pu32( a, imm8) vget_lane_u32(a, imm8)
#define _mm_extract_pi64( a, imm8) vget_lane_s64(a, imm8)
#define _mm_extract_pu64( a, imm8) vget_lane_u64(a, imm8)

//SSE
//float _mm_extract_ps(__m128 a, int imm8)
#define _mm_extract_ps( a, imm8) vgetq_lane_f32(a, imm8)

//double _mm_extract_ps(__m128d a, int imm8)
#define _mm_extract_pd( a, imm8) vgetq_lane_f64(a, imm8)


// INTEGER
//int _mm_extract_epi8 (__m128i a, const int imm8)		//Implemented by SSE4.1
#define _mm_extract_epu8( a, imm8) vgetq_lane_u8(a, imm8)
//int _mm_extract_epi16 (__m128i a, int imm8)			//Implemented by SSE2
#define _mm_extract_epu16( a, imm8) vgetq_lane_u16(a, imm8)
//int _mm_extract_epi32(__m128i a, int imm8)			//Implemented from the SSE4.1 version
#define _mm_extract_epu32( a, imm8) vgetq_lane_u32(a, imm8)
//__int64 _mm_extract_epi64 (__m128i a, const int imm8)	//Implemented by SSE4.1
#define _mm_extract_epu64( a, imm8) vgetq_lane_u64(a, imm8)
#endif

