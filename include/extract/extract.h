/***************************************************************************************************/
// HEADER_NAME /extract/extract.h
/***************************************************************************************************/
#if INTEL

//MMX
#ifdef _MSC_VER
#define _mm_extract_pi8( a, imm8) a.m64_i8[imm8]
#define _mm_extract_pu8( a, imm8) a.m64_u8[imm8]
//__int16 _mm_extract_pi16(_m64 a, int imm8)			//Implemented by SSE
#define _mm_extract_pu16( a, imm8) a.m64_u16[imm8]
#define _mm_extract_pi32( a, imm8) a.m64_i32[imm8]
#define _mm_extract_pu32( a, imm8) a.m64_u32[imm8]
#define _mm_extract_pi64( a, imm8) a.m64_i64[imm8]
#define _mm_extract_pu64( a, imm8) a.m64_u64[imm8]
#else //GCC/CLANG
#undef _mm_extract_pi8
#define _mm_extract_pi8 _mm_extract_pi8_impl
inline __int8 _mm_extract_pi8_impl(__m64 a, int imm8)
{
    union __v64
    {
        __m64 m64;
        __int8 m64_i8[8];
    };
    __v64 ret;
    ret.m64 = a;

    return ret.m64_i8[imm8];
}

#undef _mm_extract_pu8
#define _mm_extract_pu8 _mm_extract_pu8_impl
inline __uint8 _mm_extract_pu8_impl(__m64 a, int imm8)
{
    union __v64
    {
        __m64 m64;
        __uint8 m64_u8[8];
    };
    __v64 ret;
    ret.m64 = a;

    return ret.m64_u8[imm8];
}
//__int16 _mm_extract_pi16(_m64 a, int imm8)			//Implemented by SSE
#undef _mm_extract_pu16
#define _mm_extract_pu16 _mm_extract_pu16_impl
inline __uint16 _mm_extract_pu16_impl(__m64 a, int imm8)
{
    union __v64
    {
        __m64 m64;
        __uint16 m64_u16[4];
    };
    __v64 ret;
    ret.m64 = a;

    return ret.m64_u16[imm8];
}
#undef _mm_extract_pi32
#define _mm_extract_pi32 _mm_extract_pi32_impl
inline __int32 _mm_extract_pi32_impl(__m64 a, int imm8)
{
    union __v64
    {
        __m64 m64;
        __int32 m64_i32[2];
    };
    __v64 ret;
    ret.m64 = a;

    return ret.m64_i32[imm8];
}

#undef _mm_extract_pu32
#define _mm_extract_pu32 _mm_extract_pu32_impl
inline __uint32 _mm_extract_pu32_impl(__m64 a, int imm8)
{
    union __v64
    {
        __m64 m64;
        __uint32 m64_u32[2];
    };
    __v64 ret;
    ret.m64 = a;

    return ret.m64_u32[imm8];
}
#undef _mm_extract_pi64
#define _mm_extract_pi64 _mm_extract_pi64_impl
inline __int64 _mm_extract_pi64_impl(__m64 a, int imm8)
{
    union __v64
    {
        __m64 m64;
        __int64 m64_i64[1];
    };
    __v64 ret;
    ret.m64 = a;

    return ret.m64_i64[imm8];
}

#undef _mm_extract_pu64
#define _mm_extract_pu64 _mm_extract_pu64_impl
inline __uint64 _mm_extract_pu64_impl(__m64 a, int imm8)
{
    union __v64
    {
        __m64 m64;
        __uint64 m64_u64[1];
    };
    __v64 ret;
    ret.m64 = a;

    return ret.m64_u64[imm8];
}
#endif


//SSE

//REAL
#if !SSE4_1_EXTRACT
//float _mm_extract_ps(__m128 a, int imm8)				//Improved from the SSE4.1 version
#define _mm_extract_ps( a, imm8) _mm_cvtss_f32(_mm_shuffle_ps( a, a, _MM_SHUFFLE(0, 0, 0, imm8) ))
#endif

//double _mm_extract_pd(__m128d a, int imm8)
#define _mm_extract_pd( a, imm8) _mm_cvtsd_f64(_mm_shuffle_pd( a, a, _MM_SHUFFLE2(0, imm8) ))

#if LEGACY_SSE2
	#ifdef _MSC_VER
	#define _mm_extract_epu8( a, imm8) a.m128i_u8[imm8]
	#define _mm_extract_epu16( a, imm8) a.m128i_u16[imm8]
	#define _mm_extract_epu32( a, imm8) a.m128i_u32[imm8]
	#define _mm_extract_epu64( a, imm8) a.m128i_u64[imm8]

	#define _mm_extract_epi8( a, imm8) a.m128i_i8[imm8]
	#define _mm_extract_epi16( a, imm8) a.m128i_i16[imm8]
	#define _mm_extract_epi32( a, imm8) a.m128i_i32[imm8]
	#define _mm_extract_epi64( a, imm8) a.m128i_i64[imm8]
	
	#else //GCC/CLANG
		
	#undef _mm_extract_epi8
	#define _mm_extract_epi8 _mm_extract_epi8_impl
	inline __int8 _mm_extract_epi8_impl(__m128i a, int imm8)
	{
		union __v128i
		{
			__m128i m128i;
			__int8 m128i_i8[16];
		};
		__v128i ret;
		ret.m128i = a;

		return ret.m128i_i8[imm8];
	}

	#undef _mm_extract_epu8
	#define _mm_extract_epu8 _mm_extract_epu8_impl
	inline __uint8 _mm_extract_epu8_impl(__m128i a, int imm8)
	{
		union __v128i
		{
			__m128i m128i;
			__uint8 m128i_u8[16];
		};
		__v128i ret;
		ret.m128i = a;

		return ret.m128i_u8[imm8];
	}
	/*
	Part of SSE2
	__int16 _mm_extract_epi16(__m128i a, int imm8)
	*/
	
	#undef _mm_extract_epu16
	#define _mm_extract_epu16 _mm_extract_epu16_impl
	inline __uint16 _mm_extract_epu16_impl(__m128i a, int imm8)
	{
		union __v128i
		{
			__m128i m128i;
			__uint8 m128i_u16[8];
		};
		__v128i ret;
		ret.m128i = a;

		return ret.m128i_u16[imm8];
	}
	#undef _mm_extract_epi32
	#define _mm_extract_epi32 _mm_extract_epi32_impl
	inline __int32 _mm_extract_epi32_impl(__m128i a, int imm8)
	{
		union __v128i
		{
			__m128i m128i;
			__int8 m128i_i32[4];
		};
		__v128i ret;
		ret.m128i = a;

		return ret.m128i_i32[imm8];
	}

	#undef _mm_extract_epu32
	#define _mm_extract_epu32 _mm_extract_epu32_impl
	inline __uint32 _mm_extract_epu32_impl(__m128i a, int imm8)
	{
		union __v128i
		{
			__m128i m128i;
			__uint8 m128i_u32[4];
		};
		__v128i ret;
		ret.m128i = a;

		return ret.m128i_u32[imm8];
	}
	#undef _mm_extract_epi64
	#define _mm_extract_epi64 _mm_extract_epi64_impl
	inline __int64 _mm_extract_epi64_impl(__m128i a, int imm8)
	{
		union __v128i
		{
			__m128i m128i;
			__int8 m128i_i64[2];
		};
		__v128i ret;
		ret.m128i = a;

		return ret.m128i_i64[imm8];
	}

	#undef _mm_extract_epu64
	#define _mm_extract_epu64 _mm_extract_epu64_impl
	inline __uint64 _mm_extract_epu64_impl(__m128i a, int imm8)
	{
		union __v128i
		{
			__m128i m128i;
			__uint8 m128i_u64[2];
		};
		__v128i ret;
		ret.m128i = a;

		return ret.m128i_u64[imm8];
	}
	#endif
#else
	// INTEGER
	//int _mm_extract_epi8 (__m128i a, const int imm8)		//Implemented by SSE4.1
	#define _mm_extract_epu8 _mm_extract_epi8
	//int _mm_extract_epi16 (__m128i a, int imm8)			//Implemented by SSE2
	#define _mm_extract_epu16 _mm_extract_epi16
	//int _mm_extract_epi32(__m128i a, int imm8)			//Implemented from the SSE4.1 version
	#define _mm_extract_epu32 _mm_extract_epi32
	//__int64 _mm_extract_epi64 (__m128i a, const int imm8)	//Implemented by SSE4.1
	#define _mm_extract_epu64 _mm_extract_epi64
#endif
#endif

