/***************************************************************************************************/
// HEADER_NAME /extract/avx_extract.h
/***************************************************************************************************/
#if INTEL
//AVX

//REAL
//float _mm256_extract_ps(__m128 a, int imm8)
#ifdef _MSC_VER
#define _mm256_extract_ps( a, imm8) a.m256_f32[imm8]
#else
#define _mm256_extract_ps( a, imm8) _mm256_cvtss_f32(_mm256_shuffle_ps( a, a, _MM_SHUFFLE8(0, 0, 0, 0, 0, 0, 0, imm8) ))
#endif

//double _mm256_extract_pd(__m128d a, int imm8)
#ifdef _MSC_VER
#define _mm256_extract_pd( a, imm8) a.m256d_f64[imm8]
#else
#define _mm256_extract_pd( a, imm8) _mm256_cvtsd_f64(_mm256_shuffle_pd( a, a, _MM_SHUFFLE(0, 0, 0, imm8) ))
#endif




// INTEGER

#if __AVX2__ || _MSC_VER

//int _mm256_extract_epi8 (__m256i a, const int imm8)		//Implemented by AVX-2
#ifdef _MSC_VER
#define _mm256_extract_epi8( a, imm8) a.m256i_i8[imm8]
#endif

#ifdef _MSC_VER
#define _mm256_extract_epu8( a, imm8) a.m256i_u8[imm8]
#else
#define _mm256_extract_epu8 _mm256_extract_epi8
#endif

//int _mm256_extract_epi16 (__m256i a, int imm8)			//Implemented by AVX-2
#ifdef _MSC_VER
#define _mm256_extract_epi16( a, imm8) a.m256i_i16[imm8]
#endif

#ifdef _MSC_VER
#define _mm256_extract_epu16( a, imm8) a.m256i_u16[imm8]
#else
#define _mm256_extract_epu16 _mm256_extract_epi16
#endif

#endif

//int _mm256_extract_epi32(__m256i a, int imm8)				//Implemented by AVX
#ifdef _MSC_VER
#define _mm256_extract_epi32( a, imm8) a.m256i_i32[imm8]
#endif

//int _mm256_extract_epu32(__m256i a, int imm8)
#ifdef _MSC_VER
#define _mm256_extract_epu32( a, imm8) a.m256i_u32[imm8]
#else
#define _mm256_extract_epu32 _mm256_extract_epi32
#endif

//__int64 _mm256_extract_epi64(__m256i a, int imm8)			//Implemented by AVX
#ifdef _MSC_VER
#define _mm256_extract_epi64( a, imm8) a.m256i_i64[imm8]
#endif

//__int64 _mm256_extract_epu64 (__m256i a, const int imm8)
#ifdef _MSC_VER
#define _mm256_extract_epu64( a, imm8) a.m256i_u64[imm8]
#else
#define _mm256_extract_epu64 _mm256_extract_epi64
#endif

#endif

