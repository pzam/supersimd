/***************************************************************************************************/
// HEADER_NAME /extra/insertextra.h
/***************************************************************************************************/

#if !ARM
//__m128i_u8 _mm_insert_epu8 (__m128i_u8 a, __uint8 i, const int imm8)
#define _mm_insert_epu8(a, i, imm8) _mm_insert_epi8(a, (__int8)i, imm8)

//__m128i_u16 _mm_insert_epi16 (__m128i_u16 a, __uint16 i,const int imm8)
#define _mm_insert_epu16(a, i, imm8) _mm_insert_epi16(a, (__int16)i, imm8)

//__m128i_u32 _mm_insert_epu32 (__m128i_u32 a, int i, const int imm8)
#define _mm_insert_epu32(a, i, imm8) _mm_insert_epi32(a, (__int32)i, imm8)

//__m128i_u64 _mm_insert_epu64 (__m128i_u64 a, __uint64 i, const int imm8)
#define _mm_insert_epu64(a, i, imm8) _mm_insert_epi64(a, (__int64)i, imm8)

__forceinline __m128d _mm_insert_pd(__m128d a, __m128d b, const int imm8)
{
	if(imm8 == 0)
	{
		return _mm_set_pd(_mm_extract_pd(a, 1), _mm_extract_pd(b, 0));
	}	
	else //(imm8 == 1)
	{
		return _mm_set_pd(_mm_extract_pd(b, 1), _mm_extract_pd(a, 0));
	}
} 
#endif
