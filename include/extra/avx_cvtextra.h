/***************************************************************************************************/
// HEADER_NAME /extra/avx_cvtextra.h
/***************************************************************************************************/
/*
IMPLEMENTED IN AVX2
__m256i_i32 _mm256_cvtepi16_epi32 (__m128i_i16 a)
__m256i_i64 _mm256_cvtepi16_epi64 (__m128i_i16 a)
__m256i_i64 _mm256_cvtepi32_epi64 (__m128i_i32 a)
__m256i_i16 _mm256_cvtepi8_epi16 (__m128i_i8 a)
__m256i_i32 _mm256_cvtepi8_epi32 (__m128i_i8 a)
__m256i_i64 _mm256_cvtepi8_epi64 (__m128i_i8 a)
__m256i_i32 _mm256_cvtepu16_epi32 (__m128i_u16 a)
__m256i_i64 _mm256_cvtepu16_epi64 (__m128i_u16 a)
__m256i_i64 _mm256_cvtepu32_epi64 (__m128i_u32 a)
__m256i_i16 _mm256_cvtepu8_epi16 (__m128i_u8 a)
__m256i_i32 _mm256_cvtepu8_epi32 (__m128i_u8 a)
__m256i_i64 _mm256_cvtepu8_epi64 (__m128i_u8 a)
*/

#if __AVX__
//TODO _mm256_XXXXXX_si256 to specific data types!
/***************************************************************************************************/
// _mm256_cvtsi256_xxxx
/***************************************************************************************************/
//NON-PORTABLE
__forceinline __int8  _mm256_cvtsi256_si8(__m256i_i8 a){
	return _mm_cvtsi128_si32(_mm256_castsi256_si128(a)) & 0xFFU;
}
__forceinline __int16 _mm256_cvtsi256_si16(__m256i_i16 a) {
	return _mm_cvtsi128_si32(_mm256_castsi256_si128(a)) & 0xFFFFU;
}

//int _mm256_cvtsi256_si32(__m256i_i32 a) // IMPLEMNTED BY AVX
#ifndef __INTRIN_H_ && _MSC_VER
//__int64 _mm256_cvtsi256_si64(__m256i_i64 a)
#define _mm256_cvtsi256_si64(a) (_mm_cvtsi128_si64(_mm256_castsi256_si128(a)))
#endif

/***************************************************************************************************/
// Cvt For API Consistency
/***************************************************************************************************/

__forceinline __m256i_i64 _mm256_cvtsi64x_si256(__int64 a)
{
	return _mm256_set_m128i(_mm_setzero_epi64(), _mm_cvtsi64_si128(a));
}

__forceinline __int64 _mm256_cvtsi256_si64x (__m256i_i64 a){
	return _mm256_cvtsi256_si64(a);
}


/***************************************************************************************************/
// EXTRACT
/***************************************************************************************************/
// __uint8 _mm256_cvtepu8_su8(__m256i_u8 a)
#define _mm256_cvtepu8_su8 (__uint8)_mm256_cvtsi256_si8
// __int8 _mm256_cvtepi8_si8(__m256i_i8 a)
#define _mm256_cvtepi8_si8 _mm256_cvtsi256_si8

// __uint16 _mm256_cvtepu16_su16(__m256i_u16 a)
#define _mm256_cvtepu16_su16 (__uint16)_mm256_cvtsi256_si16
// __int16 _mm256_cvtepi16_si16(__m256i_i16 a)
#define _mm256_cvtepi16_si16 _mm256_cvtsi256_si16

// __uint32 _mm256_cvtepu32_su32 (__m256i_u32 a)
#define _mm256_cvtepu32_su32 (__uint32)_mm256_cvtsi256_si32
// __int32 _mm256_cvtepi32_si32 (__m256i_i32 a)
#define _mm256_cvtepi32_si32 _mm256_cvtsi256_si32

#if BIT64
// __uint64 _mm256_cvtepu64_su64 (__m256i_u64 a)
#define _mm256_cvtepu64_su64 (__uint64)_mm256_cvtsi256_si64
// __int64 _mm256_cvtepi64_si64 (__m256i_i64 a)
#define _mm256_cvtepi64_si64 _mm256_cvtsi256_si64


// __uint64 _mm256_cvtepu64_su64x (__m256i_u64 a)
#define _mm256_cvtepu64_su64x (__uint64)_mm256_cvtsi256_si64x
// __int64 _mm256_cvtepi64_si64x (__m256i_i64 a)
#define _mm256_cvtepi64_si64x _mm256_cvtsi256_si64x
#endif


/***************************************************************************************************/
// SET
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cvtsu8_epu8 (__uint8 a){
	return _mm256_insert_epu8 (_mm256_setzero_epu8(),a, 0);
}
__forceinline __m256i_i8 _mm256_cvtsi8_epi8 (__int8 a){
	return _mm256_insert_epi8 (_mm256_setzero_epi8(),a, 0);
}

__forceinline __m256i_u16 _mm256_cvtsu16_epu16 (__uint16 a){
	// NOTE:
	// Generates Better results with MSVC instead of using _mm256_insert_epi16
	// GCC makes no difference.
	return _mm256_set_epu16 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, (__int16)a);
}
__forceinline __m256i_i16 _mm256_cvtsi16_epi16 (__int16 a){
	// NOTE:
	// Generates Better results with MSVC instead of using _mm256_insert_epi16
	// GCC makes no difference.
	return _mm256_set_epi16 (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, a);
}

//FOR COMPATIBILITY
__forceinline __m256i_i32 _mm_cvtsi32_si256(__int32 a)
{
	return _mm256_set_m128i(_mm_setzero_epi32(), _mm_cvtsi32_si128(a));
}

__forceinline __m256i_u32 _mm256_cvtsu32_epu32 (__uint32 a){
	return _mm_cvtsi32_si256((__int32)a);//FIX ME
}
__forceinline __m256i_i32 _mm256_cvtsi32_epi32 (__int32 a){
	return _mm_cvtsi32_si256(a);//FIX ME
}

#if BIT64
#if __clang__ || __GNUC_ 
//FOR COMPATIBILITY
#define _mm256_cvtsi64_si256(a) _mm256_set_epi64x(0, 0, 0, a);
#define _mm256_cvtsi64x_si256 _mm256_cvtsi64_si256
#endif

__forceinline __m256i_u64 _mm256_cvtsu64_epu64 (__uint64 a){
	return _mm256_cvtsi64x_si256((__int64)a);
}
__forceinline __m256i_i64 _mm256_cvtsi64_epi64 (__int64 a){
	return _mm256_cvtsi64x_si256(a);
}


__forceinline __m256i_u64 _mm256_cvtsu64x_epu64 (__uint64 a){
	return _mm256_cvtsi64x_si256((__int64)a);
}
__forceinline __m256i_i64 _mm256_cvtsi64x_epi64 (__int64 a){
	return _mm256_cvtsi64x_si256(a);
}
#endif
#endif
