/***************************************************************************************************/
// HEADER_NAME /extra/avx2_absextra.h
/***************************************************************************************************/

/**********************************************************************/
// ABS
/**********************************************************************/

//NOTE
//AVX2 implemented
//__m256i_i8 _mm256_abs_epi8 (__m256i_i8 a) 
//__m256i_i16 _mm256_abs_epi16 (__m256i_i16 a)
//__m256i_i32 _mm256_abs_epi32 (__m256i_i32 a)

//AVX-512 implemented
//__m256i_i64 _mm256_abs_epi64 (__m128i_i64 a)

#if __AVX2__
__forceinline __m256i_u8 _mm256_absdiff_epu8 (__m256i_u8 a, __m256i_u8 b)
{
	return _mm256_subs_epu8(a, b);
}
__forceinline __m256i_i8 _mm256_absdiff_epi8 (__m256i_i8 a, __m256i_i8 b)
{
	return _mm256_abs_epi8(_mm256_sub_epi8(a, b));
}
__forceinline __m256i_u16 _mm256_absdiff_epu16 (__m256i_u16 a, __m256i_u16 b)
{
	return _mm256_subs_epu16(a, b);
}
__forceinline __m256i_i16 _mm256_absdiff_epi16 (__m256i_i16 a, __m256i_i16 b)
{
	return _mm256_abs_epi16(_mm256_sub_epi16(a, b));
}
__forceinline __m256i_u32 _mm256_absdiff_epu32 (__m256i_u32 a, __m256i_u32 b)
{
	return _mm256_sub_epi32(a, b);
}
__forceinline __m256i_i32 _mm256_absdiff_epi32 (__m256i_i32 a, __m256i_i32 b)
{
	return _mm256_abs_epi32(_mm256_sub_epi32(a, b));
}
__forceinline __m256i_u64 _mm256_absdiff_epu64 (__m256i_u64 a, __m256i_u64 b)
{
	return _mm256_sub_epi64(a, b);
}
#if __AVX512VL__ && __AVX512F__
__forceinline __m256i_i64 _mm256_absdiff_epi64 (__m256i_i64 a, __m256i_i64 b)
{
	return _mm256_abs_epi64(_mm256_sub_epi64(a, b));
}
#endif

/**********************************************************************/
#endif