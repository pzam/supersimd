/***************************************************************************************************/
// HEADER_NAME /extra/avx2_movemaskextra.h
/***************************************************************************************************/

//AVX2
#if __AVX2__
//int _mm256_movemask_epi8 (__m256i_i8 a) // Implemented in AVX2

__forceinline int _mm256_movemask_epu8 (__m256i_u8 a)
{
	return _mm256_movemask_epi8(_mm256_castepu8_epi8(a));
}
__forceinline int _mm256_movemask_epi16 (__m256i_i16 a)
{
	return _mm256_movemask_epi8(_mm256_castepi16_epi8(a));
}
__forceinline int _mm256_movemask_epu16 (__m256i_u16 a)
{
	return _mm256_movemask_epi8(_mm256_castepu16_epi8(a));
}
__forceinline int _mm256_movemask_epi32 (__m256i_i32 a)
{
	return _mm256_movemask_epi8(_mm256_castepi32_epi8(a));
}
__forceinline int _mm256_movemask_epu32 (__m256i_u32 a)
{
	return _mm256_movemask_epi8(_mm256_castepu32_epi8(a));
}
__forceinline int _mm256_movemask_epi64 (__m256i_i64 a)
{
	return _mm256_movemask_epi8(_mm256_castepi64_epi8(a));
}
__forceinline int _mm256_movemask_epu64 (__m256i_u64 a)
{
	return _mm256_movemask_epi8(_mm256_castepu64_epi8(a));
}
#endif
// int _mm256_movemask_ps (__m256 a) //Implemented in AVX
// int _mm256_movemask_pd (__m256d a) //Implemented in AVX
