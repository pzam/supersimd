/***************************************************************************************************/
// HEADER_NAME /extra/cvtavx512extra.h
/***************************************************************************************************/
//AVX-512 Compatibility 

#if !__AVX512VL__ && !__AVX512DQ__
#if BIT64
//SSE
//You only care about values in the range: [-2^51, 2^51]
#undef _mm_cvtepi64_pd
#define _mm_cvtepi64_pd _mm_cvtepi64_pd_impl
__forceinline __m128d _mm_cvtepi64_pd_impl(__m128i_i64 a){
	a = _mm_add_epi64(a, _mm_castpd_epi64(__m128d_Int64ToDoubleMagic));
	return _mm_sub_pd(_mm_castepi64_pd(a), __m128d_Int64ToDoubleMagic);
}


//You only care about values in the range: [0, 2^52).
#undef _mm_cvtepu64_pd
#define _mm_cvtepu64_pd _mm_cvtepu64_pd_impl
__forceinline __m128d _mm_cvtepu64_pd_impl(__m128i_u64 a){
	a = _mm_or_epi64(a, _mm_castpd_epi64(__m128d_UInt64ToDoubleMagic));
	return _mm_sub_pd(_mm_castepi64_pd(a), __m128d_UInt64ToDoubleMagic);
}

#if !ARM
//You only care about values in the range: [-2^51, 2^51]
#undef _mm_cvtpd_epi64
#define _mm_cvtpd_epi64 _mm_cvtpd_epi64_impl
__forceinline __m128i_i64 _mm_cvtpd_epi64_impl(__m128d a){//ARM
	a = _mm_add_pd(a, __m128d_Int64ToDoubleMagic);
	return _mm_sub_epi64( _mm_castpd_epi64(a), _mm_castpd_epi64(__m128d_Int64ToDoubleMagic));
}
#endif
#define _mm_cvttpd_epi64 _mm_cvtpd_epi64

#if !ARM
//You only care about values in the range: [0, 2^52)
#undef _mm_cvtpd_epu64
#define _mm_cvtpd_epu64 _mm_cvtpd_epu64_impl
__forceinline __m128i_u64 _mm_cvtpd_epu64_impl(__m128d a){
	a = _mm_add_pd(a, __m128d_UInt64ToDoubleMagic);
	return _mm_xor_epu64( _mm_castpd_epi64(a), _mm_castpd_epi64(__m128d_UInt64ToDoubleMagic));
}
#endif
#define _mm_cvttpd_epu64 _mm_cvtpd_epu64
#endif
//MISSING
/*
__m128i _mm_cvtepi16_epi8 (__m128i a)
__m128i _mm_cvtepi32_epi16 (__m128i a)
__m128i _mm_cvtepi32_epi8 (__m128i a)
__m128i _mm_cvtepi64_epi16 (__m128i a)
__m128i _mm_cvtepi64_epi32 (__m128i a)
__m128i _mm_cvtepi64_epi8 (__m128i a)
__m128 _mm_cvtepi64_ps (__m128i a)
__m128d _mm_cvtepu32_pd (__m128i a)
__m128 _mm_cvtepu64_ps (__m128i a)
*/

#endif

