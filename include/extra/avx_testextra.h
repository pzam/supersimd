/***************************************************************************************************/
// HEADER_NAME /extra/avx_testextra.h
/***************************************************************************************************/

/***************************************************************************************************/
// Portable Test
/***************************************************************************************************/

//AVX
#if __AVX__
#define _mm256_testc_epi8 _mm256_testc_si256
#define _mm256_testz_epi8 _mm256_testz_si256
#define _mm256_testnzc_epi8 _mm256_testnzc_si256

#define _mm256_testc_epu8 _mm256_testc_si256
#define _mm256_testz_epu8 _mm256_testz_si256
#define _mm256_testnzc_epu8 _mm256_testnzc_si256


#define _mm256_testc_epi16 _mm256_testc_si256
#define _mm256_testz_epi16 _mm256_testz_si256
#define _mm256_testnzc_epi16 _mm256_testnzc_si256

#define _mm256_testc_epu16 _mm256_testc_si256
#define _mm256_testz_epu16 _mm256_testz_si256
#define _mm256_testnzc_epu16 _mm256_testnzc_si256


#define _mm256_testc_epi32 _mm256_testc_si256
#define _mm256_testz_epi32 _mm256_testz_si256
#define _mm256_testnzc_epi32 _mm256_testnzc_si256

#define _mm256_testc_epu32 _mm256_testc_si256
#define _mm256_testz_epu32 _mm256_testz_si256
#define _mm256_testnzc_epu32 _mm256_testnzc_si256


#define _mm256_testc_epi64 _mm256_testc_si256
#define _mm256_testz_epi64 _mm256_testz_si256
#define _mm256_testnzc_epi64 _mm256_testnzc_si256

#define _mm256_testc_epu64 _mm256_testc_si256
#define _mm256_testz_epu64 _mm256_testz_si256
#define _mm256_testnzc_epu64 _mm256_testnzc_si256
#endif

