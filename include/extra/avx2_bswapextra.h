/***************************************************************************************************/
// HEADER_NAME /extra/avx2_bswapextra.h
/***************************************************************************************************/
//BSWAP

//AVX
#if __AVX2__ && !NO_AVX2

__forceinline __m256i_i32 _mm256_bswap_si256 (__m256i_i32 a)
{
	// Reverse order of all bytes in the 128-bit word.
	return _mm256_shuffle_epi8(_mm256_castepi32_epi8(a), _mm256_set_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31));
}

#define _mm256_bswap_epi8 _mm256_castepi32_epi8(_mm256_bswap_si128(_mm256_castepu8_epi32(a)))
#define _mm256_bswap_epu8(a) _mm256_castepi32_epu8(_mm256_bswap_si128(_mm256_castepu8_epi32(a)))

__forceinline __m256i_i16 _mm256_bswap_epi16 (__m256i_i16 a)
{
	// Swap upper and higher byte in each 16-bit word:
	return _mm256_or_epi16(
		_mm256_slli_epi16(_mm256_and_epi16(a, __m256i_i16_0x00ff), 8),
		_mm256_srli_epi16(_mm256_and_epi16(a, __m256i_i16_0xff00), 8)
	);
}
#define _mm256_bswap_epu16(a) _mm256_castepi16_epu16(_mm256_bswap_epi16(_mm256_castepu16_epi16(a)))

__forceinline __m256i_i32 _mm256_bswap_epi32 (__m256i_i32 a)
{
	// Reverse order of bytes in each 32-bit word.
	return _mm256_castepi8_epi32(_mm256_shuffle_epi8(_mm256_castepi32_epi8(a), _mm256_set_epi8(
		28, 29, 30, 31,
		24,  25, 26, 27,
		20,  21,  22,  23,
		16,  17,  18,  19,
		12, 13, 14, 15,
		8,  9, 10, 11,
		4,  5,  6,  7,
		0,  1,  2,  3
	)));
}
#define _mm256_bswap_epu32(a) _mm256_castepi32_epu32(_mm256_bswap_epi32(_mm256_castepu32_epi32(a)))

__forceinline __m256i_i64 _mm256_bswap_epi64 (__m256i_i64 a)
{
	// Reverse order of bytes in each 64-bit word.
	return _mm256_castepi8_epi64(_mm256_shuffle_epi8(_mm256_castepi64_epi8(a), _mm256_set_epi8(
		24, 25, 26, 27,	28, 29, 30, 31,
		16, 17, 18, 19,	20, 21, 22, 23,
		8, 9, 10, 11, 12, 13, 14, 15,
		0,  1,  2,  3,  4,  5,  6,  7
	)));

}
#define _mm256_bswap_epu64(a) _mm256_castepi64_epu64(_mm256_bswap_epi64(_mm256_castepu64_epi64(a)))
#endif


/***************************************************************************************************/
//NO_AVX2
#if __AVX2__ && NO_AVX2

__forceinline __m256i_i32 _mm256_bswap_si256 (__m256i_i32 a)
{
	// Reverse order of all bytes in the 128-bit word.
	return _mm256_shuffle_epi8(_mm256_castepi32_epi8(a), _mm256_set_epi8(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31));
}

#define _mm256_bswap_epi8 _mm256_castepi32_epi8(_mm256_bswap_si128(_mm256_castepu8_epi32(a)))
#define _mm256_bswap_epu8(a) _mm256_castepi32_epu8(_mm256_bswap_si128(_mm256_castepu8_epi32(a)))

__forceinline __m256i_i16 _mm256_bswap_epi16 (__m256i_i16 a)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_bswap_epi16 (elo_a);
	__m128i ehi_r = _mm_bswap_epi16 (ehi_a);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#define _mm256_bswap_epu16(a) _mm256_castepi16_epu16(_mm256_bswap_epi16(_mm256_castepu16_epi16(a)))

__forceinline __m256i_i32 _mm256_bswap_epi32 (__m256i_i32 a)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_bswap_epi32 (elo_a);
	__m128i ehi_r = _mm_bswap_epi32 (ehi_a);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#define _mm256_bswap_epu32(a) _mm256_castepi32_epu32(_mm256_bswap_epi32(_mm256_castepu32_epi32(a)))

__forceinline __m256i_i64 _mm256_bswap_epi64 (__m256i_i64 a)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_bswap_epi64 (elo_a);
	__m128i ehi_r = _mm_bswap_epi64 (ehi_a);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#define _mm256_bswap_epu64(a) _mm256_castepi64_epu64(_mm256_bswap_epi64(_mm256_castepu64_epi64(a)))
#endif
/***************************************************************************************************/