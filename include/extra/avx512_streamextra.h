/***************************************************************************************************/
// HEADER_NAME /extra/avx2_streamextra.h
/***************************************************************************************************/

//AVX-512
#if __AVX512F__
#define _mm512_stream_epu8 _mm512_stream_si512
#define _mm512_stream_epi8 _mm512_stream_si512
#define _mm512_stream_epu16 _mm512_stream_si512
#define _mm512_stream_epi16 _mm512_stream_si512
#define _mm512_stream_epu32 _mm512_stream_si512
#define _mm512_stream_epi32 _mm512_stream_si512
#define _mm512_stream_epu64 _mm512_stream_si512
#define _mm512_stream_epi64 _mm512_stream_si512

/***************************************************************************************************/
// Portable Stream Integer
/***************************************************************************************************/
__forceinline void _mm512_stream_su8 (__uint8* mem_addr, __uint8 a)
{
	*mem_addr = a;
}
__forceinline void _mm512_stream_si8 (__int8* mem_addr, __int8 a)
{
	*mem_addr = a;
}
__forceinline void _mm512_stream_su16 (__uint16* mem_addr, __uint16 a)
{
	*mem_addr = a;
}
__forceinline void _mm512_stream_si16 (__int16* mem_addr, __uint16 a)
{
	*mem_addr = a;
}

__forceinline void _mm512_stream_su32 (__uint32* mem_addr, __uint32 a)
{
	*mem_addr = a;
}
__forceinline void _mm512_stream_si32 (int* mem_addr, int a)
{
	*mem_addr = a;
}

__forceinline void _mm512_stream_su64 (__uint64* mem_addr, __uint64 a)
{
	*mem_addr = a;
}
__forceinline void _mm512_stream_si64 (__int64* mem_addr, __int64 a)
{
	*mem_addr = a;
}

/***************************************************************************************************/
// Portable Stream load
/***************************************************************************************************/

//AVX-512

#define _mm512_stream_load_epu8 _mm512_stream_load_si512
#define _mm512_stream_load_epi8 _mm512_stream_load_si512
#define _mm512_stream_load_epu16 _mm512_stream_load_si512
#define _mm512_stream_load_epi16 _mm512_stream_load_si512
#define _mm512_stream_load_epu32 _mm512_stream_load_si512
#define _mm512_stream_load_epi32 _mm512_stream_load_si512
#define _mm512_stream_load_epu64 _mm512_stream_load_si512
#define _mm512_stream_load_epi64 _mm512_stream_load_si512
#endif

