/***************************************************************************************************/
// HEADER_NAME /extra/multiplicationextra.h
/***************************************************************************************************/

/***************************************************************************************************/
// MULTIPLICATION
/***************************************************************************************************/

#if !ARM
__forceinline __m128i_u8 _mm_mul_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	return _mm_set_epi8
	(
		(__uint8)_mm_extract_epi8(a,15) * (__uint8)_mm_extract_epi8(b,15),
		(__uint8)_mm_extract_epi8(a,14) * (__uint8)_mm_extract_epi8(b,14),
		(__uint8)_mm_extract_epi8(a,13) * (__uint8)_mm_extract_epi8(b,13),
		(__uint8)_mm_extract_epi8(a,12) * (__uint8)_mm_extract_epi8(b,12),
		(__uint8)_mm_extract_epi8(a,11) * (__uint8)_mm_extract_epi8(b,11),
		(__uint8)_mm_extract_epi8(a,10) * (__uint8)_mm_extract_epi8(b,10),
		(__uint8)_mm_extract_epi8(a,9) * (__uint8)_mm_extract_epi8(b,9),
		(__uint8)_mm_extract_epi8(a,8) * (__uint8)_mm_extract_epi8(b,8),
		(__uint8)_mm_extract_epi8(a,7) * (__uint8)_mm_extract_epi8(b,7),
		(__uint8)_mm_extract_epi8(a,6) * (__uint8)_mm_extract_epi8(b,6),
		(__uint8)_mm_extract_epi8(a,5) * (__uint8)_mm_extract_epi8(b,5),
		(__uint8)_mm_extract_epi8(a,4) * (__uint8)_mm_extract_epi8(b,4),
		(__uint8)_mm_extract_epi8(a,3) * (__uint8)_mm_extract_epi8(b,3),
		(__uint8)_mm_extract_epi8(a,2) * (__uint8)_mm_extract_epi8(b,2),
		(__uint8)_mm_extract_epi8(a,1) * (__uint8)_mm_extract_epi8(b,1),
		(__uint8)_mm_extract_epi8(a,0) * (__uint8)_mm_extract_epi8(b,0)
	);
}
__forceinline __m128i_i8 _mm_mul_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return _mm_set_epi8
	(
		_mm_extract_epi8(a,15) * _mm_extract_epi8(b,15),
		_mm_extract_epi8(a,14) * _mm_extract_epi8(b,14),
		_mm_extract_epi8(a,13) * _mm_extract_epi8(b,13),
		_mm_extract_epi8(a,12) * _mm_extract_epi8(b,12),
		_mm_extract_epi8(a,11) * _mm_extract_epi8(b,11),
		_mm_extract_epi8(a,10) * _mm_extract_epi8(b,10),
		_mm_extract_epi8(a,9) * _mm_extract_epi8(b,9),
		_mm_extract_epi8(a,8) * _mm_extract_epi8(b,8),
		_mm_extract_epi8(a,7) * _mm_extract_epi8(b,7),
		_mm_extract_epi8(a,6) * _mm_extract_epi8(b,6),
		_mm_extract_epi8(a,5) * _mm_extract_epi8(b,5),
		_mm_extract_epi8(a,4) * _mm_extract_epi8(b,4),
		_mm_extract_epi8(a,3) * _mm_extract_epi8(b,3),
		_mm_extract_epi8(a,2) * _mm_extract_epi8(b,2),
		_mm_extract_epi8(a,1) * _mm_extract_epi8(b,1),
		_mm_extract_epi8(a,0) * _mm_extract_epi8(b,0)
	);
}

__forceinline __m128i_u16 _mm_mul_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	return _mm_set_epi16
	(
		(__uint16)_mm_extract_epi16(a,7) * (__uint16)_mm_extract_epi16(b,7),
		(__uint16)_mm_extract_epi16(a,6) * (__uint16)_mm_extract_epi16(b,6),
		(__uint16)_mm_extract_epi16(a,5) * (__uint16)_mm_extract_epi16(b,5),
		(__uint16)_mm_extract_epi16(a,4) * (__uint16)_mm_extract_epi16(b,4),
		(__uint16)_mm_extract_epi16(a,3) * (__uint16)_mm_extract_epi16(b,3),
		(__uint16)_mm_extract_epi16(a,2) * (__uint16)_mm_extract_epi16(b,2),
		(__uint16)_mm_extract_epi16(a,1) * (__uint16)_mm_extract_epi16(b,1),
		(__uint16)_mm_extract_epi16(a,0) * (__uint16)_mm_extract_epi16(b,0)
	);
}
__forceinline __m128i_i16 _mm_mul_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return _mm_set_epi16
	(
		_mm_extract_epi16(a,7) * _mm_extract_epi16(b,7),
		_mm_extract_epi16(a,6) * _mm_extract_epi16(b,6),
		_mm_extract_epi16(a,5) * _mm_extract_epi16(b,5),
		_mm_extract_epi16(a,4) * _mm_extract_epi16(b,4),
		_mm_extract_epi16(a,3) * _mm_extract_epi16(b,3),
		_mm_extract_epi16(a,2) * _mm_extract_epi16(b,2),
		_mm_extract_epi16(a,1) * _mm_extract_epi16(b,1),
		_mm_extract_epi16(a,0) * _mm_extract_epi16(b,0)
	);
}
//__m128i_u32 _mm_mul_epu32 (__m128i_u32 a, __m128i_u32 b) //IMPLEMENTED BY SSE2


#if BIT64
__forceinline __m128i_u64 _mm_mul_epu64 (__m128i_u64 a, __m128i_u64 b)
{
	return _mm_set_epi64x(
		(__uint64)_mm_extract_epi64 (a, 1) * (__uint64)_mm_extract_epi64 (b, 1),
		(__uint64)_mm_extract_epi64 (a, 0) * (__uint64)_mm_extract_epi64 (b, 0)
	);
}

__forceinline __m128i_i64 _mm_mul_epi64(__m128i_i64 a, __m128i_i64 b)
{
	return _mm_set_epi64x(
		_mm_extract_epi64(a, 1) * _mm_extract_epi64(b, 1),
		_mm_extract_epi64(a, 0) * _mm_extract_epi64(b, 0)
	);
}
#endif

#endif

/***************************************************************************************************/
// 8-bit Integers Multiplication Lo Hi
/***************************************************************************************************/
#if !ARM
__forceinline __m128i_i16 _mm_mullo_epi8(__m128i_i8 a, __m128i_i8 b) {
	// unpack and multiply
	__m128i_i16 v_even, v_odd;
	
	v_even = _mm_mullo_epi16(_mm_castepi8_epi16(a), _mm_castepi8_epi16(b));
	v_odd = _mm_mullo_epi16(_mm_srli_epi16(_mm_castepi8_epi16(a), 8),_mm_srli_epi16(_mm_castepi8_epi16(b), 8));
	
	return _mm_or_epi8(_mm_castepi16_epi8(_mm_slli_epi16(v_odd, 8)), _mm_castepi16_epi8(_mm_and_epi16(v_even, _mm_set1_hex_epi16(0xFF))));
}
#endif

__forceinline __m128i_i16 _mm_mulhi_epi8(__m128i_i8 a, __m128i_i8 b) {
	// unpack and multiply
	__m128i_i16 v_even, v_odd;
	
	v_even = _mm_mulhi_epi16(_mm_castepi8_epi16(a), _mm_castepi8_epi16(b));
	v_odd = _mm_mulhi_epi16(_mm_srli_epi16(_mm_castepi8_epi16(a), 8),_mm_srli_epi16(_mm_castepi8_epi16(b), 8));
	
	return _mm_or_epi8(_mm_castepi16_epi8(_mm_slli_epi16(v_odd, 8)), _mm_castepi16_epi8(_mm_and_epi16(v_even, _mm_set1_hex_epi16(0xFF))));
}

/*
TODO
__m128i_u64 _mm_mulhi_epu32(__m128i_u32 a, __m128i_u32 b)
__m128i_i64 _mm_mulhi_epi32(__m128i_i32 a, __m128i_i32 b)
*/

#if !__AVX512VL__ && !__AVX512F__
#if BIT64
#undef _mm_mullo_epi64
#define _mm_mullo_epi64 _mm_mullo_epi64_impl
__forceinline __m128i_i64 _mm_mullo_epi64_impl(__m128i_i64 a, __m128i_i64 b) {
	return _mm_add_epi64(
		_mm_mul_epu32(a, b),
		_mm_slli_epi64
		(
			_mm_add_epi64(
				_mm_mul_epu32(b, _mm_shuffle_epi32(a, _MM_SHUFFLE(2, 3, 0, 1))),
				_mm_mul_epu32(a, _mm_shuffle_epi32(b, _MM_SHUFFLE(2, 3, 0, 1)))
			),32
		)
	);
}
/*
TODO
__m128i_i64 _mm_mulhi_epi64(__m128i_i64 a, __m128i_i64 b)
*/

#endif
#endif