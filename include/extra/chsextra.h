/***************************************************************************************************/
// HEADER_NAME /extra/chsextra.h
/***************************************************************************************************/

/***************************************************************************************************/
// Change Signs
/***************************************************************************************************/

/***************************************************************************************************/
// SSE
/***************************************************************************************************/
__forceinline __m128 _mm_chs_ps(const __m128 a) {
	return _mm_mul_ps(a, __m128_minus_1);
} 
#if BIT64
__forceinline __m128d _mm_chs_pd(const __m128d a) {
	return _mm_mul_pd(a, __m128d_minus_1);
}
#endif

__forceinline __m128 _mm_chs_ss(const __m128 a) {
	return _mm_mul_ss(a, _mm_set_ss(-1));
} 
#if BIT64
__forceinline __m128d _mm_chs_sd(const __m128d a) {
	return _mm_mul_sd(a, _mm_set_sd(-1));
}
#endif

__forceinline __m128i_i8 _mm_chs_epi8(__m128i_i8 a) {
	return _mm_mul_epi8(a, __m128i_i8_minus_1);
}
__forceinline __m128i_i16 _mm_chs_epi16(__m128i_i16 a) {
	return _mm_mul_epi16(a, __m128i_i16_minus_1);
}
__forceinline __m128i_i32 _mm_chs_epi32(__m128i_i32 a) {
	return _mm_mul_epi32(a, __m128i_i32_minus_1);
}
#if BIT64
__forceinline __m128i_i64 _mm_chs_epi64(__m128i_i64 a) {
	return _mm_cvtpd_epi64(_mm_chs_pd(_mm_cvtepi64_pd(a)));
}
#endif