/***************************************************************************************************/
// HEADER_NAME /extra/avx_signextra.h
/***************************************************************************************************/

/***************************************************************************************************/
// AVX
/***************************************************************************************************/
#if __AVX__
__forceinline __m256 _mm256_preservesignbit_ps(const __m256 a) {
	return _mm256_and_ps(a, __m256_sign_mask);
}
__forceinline __m256d _mm256_preservesignbit_pd(const __m256d a) {
	return _mm256_and_pd(a, __m256d_sign_mask);
}

__forceinline __m256 _mm256_effectsignbit_ps(const __m256 a, const __m256 b) {
	return _mm256_xor_ps(a, b);
}
__forceinline __m256d _mm256_effectsignbit_pd(const __m256d a, const __m256d b) {
	return _mm256_xor_pd(a, b);
}

__forceinline __m256 _mm256_preservesign_ps(const __m256 a) {
	return _mm256_xor_ps(_mm256_and_ps(a, __m256_sign_mask), __m256_1);
}
__forceinline __m256d _mm256_preservesign_pd(const __m256d a) {
	return _mm256_xor_pd(_mm256_and_pd(a, __m256d_sign_mask), __m256d_1);
}
#endif

