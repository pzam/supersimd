/***************************************************************************************************/
// HEADER_NAME /extra/no_fma.h
/***************************************************************************************************/

#if NO_FMA
// Workaround if FMA is not present for architecture.
// C Macros reverting FMA intrinsics to SSE instructions.
#undef _mm_fmadd_ps
#define _mm_fmadd_ps(a,b,c) _mm_add_ps(_mm_mul_ps(a,b),c)
#undef _mm_fmadd_pd
#define _mm_fmadd_pd(a,b,c) _mm_add_pd(_mm_mul_pd(a,b),c)
#undef _mm_fmadd_ss
#define _mm_fmadd_ss(a,b,c) _mm_add_ss(_mm_mul_ss(a,b),c)
#undef _mm_fmadd_sd
#define _mm_fmadd_sd(a,b,c) _mm_add_sd(_mm_mul_sd(a,b),c)

#undef _mm_fmsub_ps
#define _mm_fmsub_ps(a,b,c) _mm_sub_ps(_mm_mul_ps(a,b),c)
#undef _mm_fmsub_pd
#define _mm_fmsub_pd(a,b,c) _mm_sub_pd(_mm_mul_pd(a,b),c)
#undef _mm_fmsub_ss
#define _mm_fmsub_ss(a,b,c) _mm_sub_ss(_mm_mul_ss(a,b),c)
#undef _mm_fmsub_sd
#define _mm_fmsub_sd(a,b,c) _mm_sub_sd(_mm_mul_sd(a,b),c)

#undef _mm_fnmadd_ps
#define _mm_fnmadd_ps(a,b,c) _mm_add_ps(_mm_chs_ps(_mm_mul_ps(a,b)),c)
#undef _mm_fnmadd_pd
#define _mm_fnmadd_pd(a,b,c) _mm_add_pd(_mm_chs_pd(_mm_mul_pd(a,b)),c)
#undef _mm_fnmadd_ss
#define _mm_fnmadd_ss(a,b,c) _mm_add_ss(_mm_chs_ss(_mm_mul_ss(a,b)),c)
#undef _mm_fnmadd_sd
#define _mm_fnmadd_sd(a,b,c) _mm_add_sd(_mm_chs_sd(_mm_mul_sd(a,b)),c)

#undef _mm_fnmsub_ps
#define _mm_fnmsub_ps(a,b,c) _mm_sub_ps(_mm_chs_ps(_mm_mul_ps(a,b)),c)
#undef _mm_fnmsub_pd
#define _mm_fnmsub_pd(a,b,c) _mm_sub_pd(_mm_chs_ps(_mm_mul_pd(a,b)),c)
#undef _mm_fnmsub_ss
#define _mm_fnmsub_ss(a,b,c) _mm_sub_ss(_mm_chs_ss(_mm_mul_ss(a,b)),c)
#undef _mm_fnmsub_sd
#define _mm_fnmsub_sd(a,b,c) _mm_sub_sd(_mm_chs_sd(_mm_mul_sd(a,b)),c)

#undef _mm_fmaddsub_ps
#define _mm_fmaddsub_ps(a,b,c) _mm_addsub_ps(_mm_mul_ps(a,b),c)
#undef _mm_fmaddsub_pd
#define _mm_fmaddsub_pd(a,b,c) _mm_addsub_pd(_mm_mul_pd(a,b),c)

#undef _mm_fmsubadd_ps
#define _mm_fmsubadd_ps(a,b,c) _mm_subadd_ps(_mm_mul_ps(a,b),c)
#undef _mm_fmsubadd_pd
#define _mm_fmsubadd_pd(a,b,c) _mm_subadd_pd(_mm_mul_pd(a,b),c)

#undef _mm256_fmadd_ps
#define _mm256_fmadd_ps(a,b,c) _mm256_add_ps(_mm256_mul_ps(a,b),c)
#undef _mm256_fmadd_pd
#define _mm256_fmadd_pd(a,b,c) _mm256_add_pd(_mm256_mul_pd(a,b),c)

#undef _mm256_fmsub_ps
#define _mm256_fmsub_ps(a,b,c) _mm256_sub_ps(_mm256_mul_ps(a,b),c)
#undef _mm256_fmsub_pd
#define _mm256_fmsub_pd(a,b,c) _mm256_sub_pd(_mm256_mul_pd(a,b),c)

#undef _mm256_fnmadd_ps
#define _mm256_fnmadd_ps(a,b,c) _mm256_add_ps(_mm256_chs_ps(_mm256_mul_ps(a,b)),c)
#undef _mm256_fnmadd_pd
#define _mm256_fnmadd_pd(a,b,c) _mm256_add_pd(_mm256_chs_pd(_mm256_mul_pd(a,b)),c)

#undef _mm256_fnmadd_ps
#define _mm256_fnmsub_ps(a,b,c) _mm256_sub_ps(_mm256_chs_ps(_mm256_mul_ps(a,b)),c)
#undef _mm256_fnmadd_pd
#define _mm256_fnmsub_pd(a,b,c) _mm256_sub_pd(_mm256_chs_ps(_mm256_mul_pd(a,b)),c)

#undef _mm256_fmaddsub_ps
#define _mm256_fmaddsub_ps(a,b,c) _mm256_addsub_ps(_mm256_mul_ps(a,b),c)
#undef _mm256_fmaddsub_pd
#define _mm256_fmaddsub_pd(a,b,c) _mm256_addsub_pd(_mm256_mul_pd(a,b),c)

#undef _mm256_fmsubadd_ps
#define _mm256_fmsubadd_ps(a,b,c) _mm256_subadd_ps(_mm256_mul_ps(a,b),c)
#undef _mm256_fmsubadd_pd
#define _mm256_fmsubadd_pd(a,b,c) _mm256_subadd_pd(_mm256_mul_pd(a,b),c)

#endif

