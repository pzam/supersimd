/***************************************************************************************************/
// HEADER_NAME /extra/avx2_cmovextra.h
/***************************************************************************************************/

//AVX
#if __AVX2__
__forceinline __m256i_u8 _mm256_cmov_epu8(__m256i_u8 a, __m256i_u8 b, __m256i_u8 mask)
{
	return _mm256_or_epu8(_mm256_andnot_epu8(mask, b), _mm256_and_epu8(mask, a));
	
}
__forceinline __m256i_i8 _mm256_cmov_epi8(__m256i_i8 a, __m256i_i8 b, __m256i_i8 mask)
{
	return _mm256_or_epi8(_mm256_andnot_epi8(mask, b), _mm256_and_epi8(mask, a));
}

__forceinline __m256i_u16 _mm256_cmov_epu16(__m256i_u16 a, __m256i_u16 b, __m256i_u16 mask)
{
	return _mm256_or_epu16(_mm256_andnot_epu16(mask, b), _mm256_and_epu16(mask, a));
	
}
__forceinline __m256i_i16 _mm256_cmov_epi16(__m256i_i16 a, __m256i_i16 b, __m256i_i16 mask)
{
	return _mm256_or_epi16(_mm256_andnot_epi16(mask, b), _mm256_and_epi16(mask, a));
}

__forceinline __m256i_u32 _mm256_cmov_epu32(__m256i_u32 a, __m256i_u32 b, __m256i_u32 mask)
{
	return _mm256_or_epu32(_mm256_andnot_epu32(mask, b), _mm256_and_epu32(mask, a));
	
}
__forceinline __m256i_i32 _mm256_cmov_epi32(__m256i_i32 a, __m256i_i32 b, __m256i_i32 mask)
{
	return _mm256_or_epi32(_mm256_andnot_epi32(mask, b), _mm256_and_epi32(mask, a));
}

__forceinline __m256i_u32 _mm256_cmov_epu64(__m256i_u64 a, __m256i_u64 b, __m256i_u64 mask)
{
	return _mm256_or_epu64(_mm256_andnot_epu64(mask, b), _mm256_and_epu64(mask, a));
	
}
__forceinline __m256i_i64 _mm256_cmov_epi64(__m256i_i64 a, __m256i_i64 b, __m256i_i64 mask)
{
	return _mm256_or_epi64(_mm256_andnot_epi64(mask, b), _mm256_and_epi64(mask, a));
}
#endif