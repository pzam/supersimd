/***************************************************************************************************/
// HEADER_NAME /extra/setextra.h
/***************************************************************************************************/
/***************************************************************************************************/
// SET REVERSED
/***************************************************************************************************/

#if BIT64
// Duplicate definition for clang compatibility (CLANG INTRIN.H FIX)
/*********************************************************************/
#if __clang__
#define SETREPI64X 1
#endif

#if __GNUC__
#define SETREPI64X 1
#endif

#if _MSC_VER
#ifndef __INTRIN_H_
#define SETREPI64X 1
#endif
#endif

/*********************************************************************/
#if SETREPI64X
__forceinline __m128i_i64 _mm_setr_epi64x(__int64 e0, __int64 e1)
{
	return _mm_set_epi64x(e1, e0);
}
#endif
#endif

/***************************************************************************************************/
// SET UNSIGNED INTERGERS
/***************************************************************************************************/
#if !ARM

//MMX
#if defined(__MMX__) || defined(_M_IX86)
#define _mm_set_pu8 _mm_set_pi8
#define _mm_set_pu16 _mm_set_pi16
#define _mm_set_pu32 _mm_set_pi32
#define _mm_set_pi64x _m_from_int64
#define _mm_set_pu64x _m_from_int64

#define _mm_setr_pu8 _mm_setr_pi8
#define _mm_setr_pu16 _mm_setr_pi16
#define _mm_setr_pu32 _mm_setr_pi32
#define _mm_setr_pi64x _m_from_int64
#define _mm_setr_pu64x _m_from_int64

#define _mm_set1_pu8 _mm_set1_pi8
#define _mm_set1_pu16 _mm_set1_pi16
#define _mm_set1_pu32 _mm_set1_pi32
#define _mm_set1_pi64x _m_from_int64
#define _mm_set1_pu64x _m_from_int64
#endif


//SSE
#define _mm_set_epu8 _mm_set_epi8
#define _mm_set_epu16 _mm_set_epi16
#define _mm_set_epu32 _mm_set_epi32
#define _mm_set_epu64 _mm_set_epi64
#define _mm_set_epu64x _mm_set_epi64x

#define _mm_setr_epu8 _mm_setr_epi8
#define _mm_setr_epu16 _mm_setr_epi16
#define _mm_setr_epu32 _mm_setr_epi32
#define _mm_setr_epu64 _mm_setr_epi64
#define _mm_setr_epu64x _mm_setr_epi64x

#define _mm_set1_epu8 _mm_set1_epi8
#define _mm_set1_epu16 _mm_set1_epi16
#define _mm_set1_epu32 _mm_set1_epi32
#define _mm_set1_epu64 _mm_set1_epi64
#define _mm_set1_epu64x _mm_set1_epi64x

#endif

/***************************************************************************************************/
// SET HEXADECIMAL VALUE
/***************************************************************************************************/
#define _mm_set_hex_epu8 _mm_set_epu8
#define _mm_set_hex_epi8 _mm_set_epi8
#define _mm_setr_hex_epu8 _mm_setr_epu8
#define _mm_setr_hex_epi8 _mm_setr_epi8
#define _mm_set1_hex_epu8 _mm_set1_epu8
#define _mm_set1_hex_epi8 _mm_set1_epi8

#define _mm_set_hex_epu16 _mm_set_epu16
#define _mm_set_hex_epi16 _mm_set_epi16
#define _mm_setr_hex_epu16 _mm_setr_epu16
#define _mm_setr_hex_epi16 _mm_setr_epi16
#define _mm_set1_hex_epu16 _mm_set1_epu16
#define _mm_set1_hex_epi16 _mm_set1_epi16

#define _mm_set_hex_epu32 _mm_set_epu32
#define _mm_set_hex_epi32 _mm_set_epi32
#define _mm_setr_hex_epu32 _mm_setr_epu32
#define _mm_setr_hex_epi32 _mm_setr_epi32
#define _mm_set1_hex_epu32 _mm_set1_epu32
#define _mm_set1_hex_epi32 _mm_set1_epi32

#define _mm_set_hex_epu64x _mm_set_epu64x
#define _mm_set_hex_epi64x _mm_set_epi64x
#define _mm_setr_hex_epu64x _mm_setr_epu64x
#define _mm_setr_hex_epi64x _mm_setr_epi64x
#define _mm_set1_hex_epu64x _mm_set1_epu64x
#define _mm_set1_hex_epi64x _mm_set1_epi64x

//SSE
__forceinline __m128 _mm_set_hex_ps(int e3, int e2, int e1, int e0)
{
	return _mm_castepi32_ps(_mm_set_epi32(e3, e2, e1, e0));
}
__forceinline __m128 _mm_setr_hex_ps(int e3, int e2, int e1, int e0)
{
	return _mm_castepi32_ps(_mm_setr_epi32(e3, e2, e1, e0));
}
__forceinline __m128 _mm_set1_hex_ps(int a)
{
	return _mm_castepi32_ps(_mm_set1_epi32(a));
}

#if BIT64
__forceinline __m128d _mm_set_hex_pd(__int64 e1, __int64 e0)
{
	return _mm_castepi64_pd(_mm_set_epi64x(e1, e0));
}
__forceinline __m128d _mm_setr_hex_pd(__int64 e1, __int64 e0)
{
	return _mm_castepi64_pd(_mm_setr_epi64x(e1, e0));
}
__forceinline __m128d _mm_set1_hex_pd(__int64 a)
{
	return _mm_castepi64_pd(_mm_set1_epi64x(a));
}
#endif

/***************************************************************************************************/
// SET-ZERO
/***************************************************************************************************/
#if !ARM
//MMX
#if defined(__MMX__) || defined(_M_IX86)
__forceinline __m64i_u8 _mm_setzero_pu8(void)
{
	return _mm_setzero_si64();
}
__forceinline __m64i_i8 _mm_setzero_pi8(void)
{
	return _mm_setzero_si64();
}
__forceinline __m64i_u16 _mm_setzero_pu16(void)
{
	return _mm_setzero_si64();
}
__forceinline __m64i_i16 _mm_setzero_pi16(void)
{
	return _mm_setzero_si64();
}
__forceinline __m64i_u32 _mm_setzero_pu32(void)
{
	return _mm_setzero_si64();
}
__forceinline __m64i_i32 _mm_setzero_pi32(void)
{
	return _mm_setzero_si64();
}
__forceinline __m64i_u64 _mm_setzero_pu64(void)
{
	return _mm_setzero_si64();
}
__forceinline __m64i_i64 _mm_setzero_pi64(void)
{
	return _mm_setzero_si64();
}
#endif
//SSE
__forceinline __m128i_u8 _mm_setzero_epu8(void)
{
	return _mm_setzero_si128();
}
__forceinline __m128i_i8 _mm_setzero_epi8(void)
{
	return _mm_setzero_si128();
}
__forceinline __m128i_u16 _mm_setzero_epu16(void)
{
	return _mm_setzero_si128();
}
__forceinline __m128i_i16 _mm_setzero_epi16(void)
{
	return _mm_setzero_si128();
}
__forceinline __m128i_u32 _mm_setzero_epu32(void)
{
	return _mm_setzero_si128();
}
__forceinline __m128i_i32 _mm_setzero_epi32(void)
{
	return _mm_setzero_si128();
}
__forceinline __m128i_u64 _mm_setzero_epu64(void)
{
	return _mm_setzero_si128();
}
__forceinline __m128i_i64 _mm_setzero_epi64(void)
{
	return _mm_setzero_si128();
}
#endif

/***************************************************************************************************/
// SET-UNDEFINED
/***************************************************************************************************/
//SSE
#if !ARM
#define _mm_undefined_epu8 _mm_undefined_si128
#define _mm_undefined_epi8 _mm_undefined_si128
#define _mm_undefined_epu16 _mm_undefined_si128
#define _mm_undefined_epi16 _mm_undefined_si128
#define _mm_undefined_epu32 _mm_undefined_si128
#define _mm_undefined_epi32 _mm_undefined_si128
#define _mm_undefined_epu64 _mm_undefined_si128
#define _mm_undefined_epi64 _mm_undefined_si128
#endif
