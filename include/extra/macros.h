/***************************************************************************************************/
// HEADER_NAME /extra/macros.h
/***************************************************************************************************/
// Shuffle Macro like _MM_SHUFFLE that function in reversed order
#ifndef _MM_RSHUFFLE
#define _MM_RSHUFFLE(x, y, z, w) ((x) | ((y) << 2) | ((z) << 4) | ((w) << 6))
#endif

#ifndef _MM_SHUFFLE8
#define _MM_SHUFFLE8(z, y, x, w, v, u, t, s)									\
																				\
					(((z) << 21) | ((y) << 18) | ((x) << 15) | ((w) << 12)) |	\
					((v) << 9) | ((u) << 6) | ((t) << 3) | ((s)))
#endif

//Duplicate element throughout vector
#define _mm_duplicate_ps(v, i) _mm_shuffle_ps((v), (v), _MM_SHUFFLE(i, i, i, i))

#define _mm_duplicate_0_ps(v) _mm_shuffle_ps((v), (v), _MM_SHUFFLE(0, 0, 0, 0))
#define _mm_duplicate_1_ps(v) _mm_shuffle_ps((v), (v), _MM_SHUFFLE(1, 1, 1, 1))
#define _mm_duplicate_2_ps(v) _mm_shuffle_ps((v), (v), _MM_SHUFFLE(2, 2, 2, 2))
#define _mm_duplicate_3_ps(v) _mm_shuffle_ps((v), (v), _MM_SHUFFLE(3, 3, 3, 3))

