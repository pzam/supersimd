/***************************************************************************************************/
// HEADER_NAME /extra/avx2_blendextra.h
/***************************************************************************************************/


//AVX2
#if __AVX2__
__forceinline __m256i _mm256_blendv_si256 (__m256i a, __m256i b, __m256i mask)
{
	// Replace bit in a with bit in b when matching bit in mask is set:
	return _mm256_or_si256(_mm256_andnot_si256(mask, a), _mm256_and_si256(mask, b));
}

/***************************************************************************************************/
// Portable blendv
/***************************************************************************************************/

__forceinline __m256i_u8 _mm256_blendv_epu8 (__m256i_u8 a, __m256i_u8 b, __m256i_u8 mask) {
	return _mm256_castepi8_epu8(_mm256_blendv_epi8(_mm256_castepu8_epi8(a), _mm256_castepu8_epi8(b), _mm256_castepu8_epi8(mask)));
}
// _mm256_blendv_epi8 already implemented by AVX2

__forceinline __m256i_u16 _mm256_blendv_epu16 (__m256i_u16 a, __m256i_u16 b, __m256i_u16 mask) {
	return _mm256_castepi8_epu16(_mm256_blendv_epi8(_mm256_castepu16_epi8(a), _mm256_castepu16_epi8(b), _mm256_castepu16_epi8(mask)));
}
__forceinline __m256i_i16 _mm256_blendv_epi16 (__m256i_i16 a, __m256i_i16 b, __m256i_i16 mask) {
	return _mm256_castepi8_epi16(_mm256_blendv_epi8(_mm256_castepi16_epi8(a), _mm256_castepi16_epi8(b), _mm256_castepi16_epi8(mask)));
}
__forceinline __m256i_u32 _mm256_blendv_epu32 (__m256i_u32 a, __m256i_u32 b, __m256i_u32 mask) {
	return _mm256_castepi8_epu32(_mm256_blendv_epi8(_mm256_castepu32_epi8(a), _mm256_castepu32_epi8(b), _mm256_castepu32_epi8(mask)));
}
__forceinline __m256i_i32 _mm256_blendv_epi32 (__m256i_i32 a, __m256i_i32 b, __m256i_i32 mask) {
	return _mm256_castepi8_epi32(_mm256_blendv_epi8(_mm256_castepi32_epi8(a), _mm256_castepi32_epi8(b), _mm256_castepi32_epi8(mask)));
}
__forceinline __m256i_u64 _mm256_blendv_epu64 (__m256i_u64 a, __m256i_u64 b, __m256i_u64 mask) {
	return _mm256_castepi8_epu64(_mm256_blendv_epi8(_mm256_castepu64_epi8(a), _mm256_castepu64_epi8(b), _mm256_castepu64_epi8(mask)));
}
__forceinline __m256i_i64 _mm256_blendv_epi64 (__m256i_i64 a, __m256i_i64 b, __m256i_i64 mask) {
	return _mm256_castepi8_epi64(_mm256_blendv_epi8(_mm256_castepi64_epi8(a), _mm256_castepi64_epi8(b), _mm256_castepi64_epi8(mask)));
}

/***************************************************************************************************/
// Portable blend
/***************************************************************************************************/
//_mm256_blend_epi8 MISSING!!!
//_mm256_blend_epi16 already implemented by AVX2
//_mm256_blend_epi32 already implemented by AVX2
//_mm256_blend_epi64 MISSING!!!


//_mm256_blend_epu8  MISSING!!!
#define _mm256_blend_epu16 _mm256_castepi16_epu16(_mm256_blend_epi16(_mm256_castepu16_epi16(a), _mm256_castepu16_epi16(b), imm8))
#define _mm256_blend_epu32 _mm256_castepi32_epu32(_mm256_blend_epi32(_mm256_castepu32_epi32(a), _mm256_castepu32_epi32(b), imm8))
//_mm256_blend_epu64  MISSING!!!
#endif