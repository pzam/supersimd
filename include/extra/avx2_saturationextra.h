/***************************************************************************************************/
// HEADER_NAME /extra/avx2_saturationextra.h
/***************************************************************************************************/

//#if __AVX2__
#if 0
/***************************************************************************************************/
// SATURATION ADD
/***************************************************************************************************/
//__m256i_u8 _mm256_adds_epu8(__m256i_u8 a, __m256i_u8 b) IMPLEMENTED IN AVX2
//__m256i_i8 _mm256_adds_epi8(__m256i_i8 a, __m256i_i8 b) IMPLEMENTED IN AVX2
//__m256i_u16 _mm256_adds_epu16(__m256i_u16 a, __m256i_u16 b) IMPLEMENTED IN AVX2
//__m256i_i16 _mm256_adds_epi16(__m256i_i16 a, __m256i_i16 b) IMPLEMENTED IN AVX2

__forceinline __m256i_u32 _mm256_adds_epu32(__m256i_u32 a, __m256i_u32 b)
{
	__m256i_u32 sum;
	__m256i_u32 cmp;
	sum = _mm256_add_epu32(a, b);
	cmp = _mm256_cmplt_epu32(sum, a);
	sum = _mm256_or_epu32(sum, cmp);
	return sum;
}

__forceinline __m256i_i32 _mm256_adds_epi32(__m256i_i32 a, __m256i_i32 b)
{
	__m256i_i32 bool_a_is_negative;
	__m256i_i32 bool_b_is_negative;
	__m256i_i32 bool_sum_is_negative;
	__m256i_i32 sum;

	bool_a_is_negative = _mm256_cmpneq_epi32(_mm256_and_epi32(a, __m256i_i32_max), _mm256_setzero_epi32());
	bool_b_is_negative = _mm256_cmpneq_epi32(_mm256_and_epi32(b, __m256i_i32_min), _mm256_setzero_epi32());

	sum = _mm256_add_epi32(a, b);
	bool_sum_is_negative = _mm256_cmpneq_epi32(_mm256_and_epi32(sum, __m256i_i32_min), _mm256_setzero_epi32());

	sum = _mm256_blendv_epi32(sum, _mm256_setzero_epi32(), _mm256_cmpneq_epi32(bool_a_is_negative, bool_b_is_negative));
	sum = _mm256_blendv_epi32(sum, __m256i_i32_max, _mm256_cmpeq_epi32(bool_a_is_negative, _mm256_not_epi32(bool_sum_is_negative)));
	sum = _mm256_blendv_epi32(sum, __m256i_i32_min, _mm256_cmpeq_epi32(_mm256_not_epi32(bool_a_is_negative), bool_sum_is_negative));

	return sum;
}

#if BIT64
__forceinline __m256i_i64 _mm256_adds_epi64(__m256i_i64 a, __m256i_i64 b)
{
	__m256i_i64 bool_a_is_negative;
	__m256i_i64 bool_b_is_negative;
	__m256i_i64 bool_sum_is_negative;
	__m256i_i64 sum;

	bool_a_is_negative = _mm256_cmpneq_epi64(_mm256_and_epi64(a, __m256i_i64_max), _mm256_setzero_epi64());
	bool_b_is_negative = _mm256_cmpneq_epi64(_mm256_and_epi64(b, __m256i_i64_min), _mm256_setzero_epi64());

	sum = _mm256_add_epi64(a, b);
	bool_sum_is_negative = _mm256_cmpneq_epi64(_mm256_and_epi64(sum, __m256i_i64_min), _mm256_setzero_epi64());

	sum = _mm256_blendv_epi64(sum, _mm256_setzero_epi64(), _mm256_cmpneq_epi64(bool_a_is_negative, bool_b_is_negative));
	sum = _mm256_blendv_epi64(sum, __m256i_i64_max, _mm256_cmpeq_epi64(bool_a_is_negative, _mm256_not_epi64(bool_sum_is_negative)));
	sum = _mm256_blendv_epi64(sum, __m256i_i64_min, _mm256_cmpeq_epi64(_mm256_not_epi64(bool_a_is_negative), bool_sum_is_negative));

	return sum;
}

__forceinline __m256i_u64 _mm256_adds_epu64(__m256i_u64 a, __m256i_u64 b)
{
	__m256i_u64 sum;
	__m256i_u64 cmp;
	sum = _mm256_add_epu64(a, b);
	cmp = _mm256_cmplt_epu64(sum, a);
	sum = _mm256_or_epu64(sum, cmp);
	return sum;
}
#endif
/***************************************************************************************************/
// SATURATION SUBTRACT
/***************************************************************************************************/
//__m256i_u8 _mm256_subs_epu8(__m256i_u8 a, __m256i_u8 b) IMPLEMENTED IN AVX2
//__m256i_i8 _mm256_subs_epi8(__m256i_i8 a, __m256i_i8 b) IMPLEMENTED IN AVX2
//__m256i_u16 _mm256_subs_epu16(__m256i_u16 a, __m256i_u16 b) IMPLEMENTED IN AVX2
//__m256i_i16 _mm256_subs_epi16(__m256i_i16 a, __m256i_i16 b) IMPLEMENTED IN AVX2

__forceinline __m256i_i32 _mm256_subs_epu32(__m256i_u32 a, __m256i_u32 b)
{
	__m256i_u32 sum;
	__m256i_u32 cmp;
	sum = _mm256_sub_epu32(a, b);
	cmp = _mm256_cmple_epu32(sum, a);
	sum = _mm256_and_epu32(sum, cmp);
	return sum;
}
__forceinline __m256i_i32 _mm256_subs_epi32(__m256i_i32 a, __m256i_i32 b)
{
	__m256i_i32 bool_a_is_negative;
	__m256i_i32 bool_b_is_negative;
	__m256i_i32 bool_sum_is_negative;
	__m256i_i32 sum;

	bool_a_is_negative = _mm256_cmpneq_epi32(_mm256_and_epi32(a, __m256i_i32_max), _mm256_setzero_epi32());
	bool_b_is_negative = _mm256_cmpneq_epi32(_mm256_and_epi32(b, __m256i_i32_min), _mm256_setzero_epi32());

	sum = _mm256_sub_epi32(a, b);
	bool_sum_is_negative = _mm256_cmpneq_epi32(_mm256_and_epi32(sum, __m256i_i32_min), _mm256_setzero_epi32());

	sum = _mm256_blendv_epi32(sum, _mm256_setzero_epi32(), _mm256_cmpneq_epi32(bool_a_is_negative, bool_b_is_negative));
	sum = _mm256_blendv_epi32(sum, __m256i_i32_max, _mm256_cmpeq_epi32(bool_a_is_negative, _mm256_not_epi32(bool_sum_is_negative)));
	sum = _mm256_blendv_epi32(sum, __m256i_i32_min, _mm256_cmpeq_epi32(_mm256_not_epi32(bool_a_is_negative), bool_sum_is_negative));

	return sum;
}

#if BIT64
__forceinline __m256i_i64 _mm256_subs_epu64(__m256i_u64 a, __m256i_u64 b)
{
	__m256i_u64 sum;
	__m256i_u64 cmp;
	sum = _mm256_sub_epu64(a, b);
	cmp = _mm256_cmple_epu64(sum, a);
	sum = _mm256_and_epu64(sum, cmp);
	return sum;
}

__forceinline __m256i_i64 _mm256_subs_epi64(__m256i_i64 a, __m256i_i64 b)
{
	__m256i_i64 bool_a_is_negative;
	__m256i_i64 bool_b_is_negative;
	__m256i_i64 bool_sum_is_negative;
	__m256i_i64 sum;

	bool_a_is_negative = _mm256_cmpneq_epi64(_mm256_and_epi64(a, __m256i_i64_max), _mm256_setzero_epi64());
	bool_b_is_negative = _mm256_cmpneq_epi64(_mm256_and_epi64(b, __m256i_i64_min), _mm256_setzero_epi64());

	sum = _mm256_sub_epi64(a, b);
	bool_sum_is_negative = _mm256_cmpneq_epi64(_mm256_and_epi64(sum, __m256i_i64_min), _mm256_setzero_epi64());

	sum = _mm256_blendv_epi64(sum, _mm256_setzero_epi64(), _mm256_cmpneq_epi64(bool_a_is_negative, bool_b_is_negative));
	sum = _mm256_blendv_epi64(sum, __m256i_i64_max, _mm256_cmpeq_epi64(bool_a_is_negative, _mm256_not_epi64(bool_sum_is_negative)));
	sum = _mm256_blendv_epi64(sum, __m256i_i64_min, _mm256_cmpeq_epi64(_mm256_not_epi64(bool_a_is_negative), bool_sum_is_negative));

	return sum;
}
#endif

#endif