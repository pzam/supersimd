/***************************************************************************************************/
// HEADER_NAME /extra/signextra.h
/***************************************************************************************************/

/***************************************************************************************************/
// SSE
/***************************************************************************************************/
__forceinline __m128 _mm_preservesignbit_ps(const __m128 a) {
	return _mm_and_ps(a, __m128_sign_mask);
}
#if BIT64
__forceinline __m128d _mm_preservesignbit_pd(const __m128d a) {
	return _mm_and_pd(a, __m128d_sign_mask);
}
#endif

__forceinline __m128 _mm_effectsignbit_ps(const __m128 a, const __m128 b) {
	return _mm_xor_ps(a, b);
}
#if BIT64
__forceinline __m128d _mm_effectsignbit_pd(const __m128d a, const __m128d b) {
	return _mm_xor_pd(a, b);
}
#endif

__forceinline __m128 _mm_preservesign_ps(const __m128 a) {
	return _mm_xor_ps(_mm_and_ps(a, __m128_sign_mask), __m128_1);
}
#if BIT64
__forceinline __m128d _mm_preservesign_pd(const __m128d a) {
	return _mm_xor_pd(_mm_and_pd(a, __m128d_sign_mask), __m128d_1);
}
#endif
