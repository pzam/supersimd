/***************************************************************************************************/
// HEADER_NAME /extra/blendextra.h
/***************************************************************************************************/
#if !ARM
//SSE4
// _mm_blendv_si128 Not Portable
__forceinline __m128i _mm_blendv_si128 (__m128i a, __m128i b, __m128i mask) {
	return _mm_or_si128(_mm_andnot_si128(mask, a), _mm_and_si128(mask, b));
}

/***************************************************************************************************/
// Portable blendv
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_blendv_epu8 (__m128i_u8 a, __m128i_u8 b, __m128i_u8 mask) {
	return _mm_castepi8_epu8(_mm_blendv_epi8(_mm_castepu8_epi8(a), _mm_castepu8_epi8(b), _mm_castepu8_epi8(mask)));
}
// _mm_blendv_epi8 already implemented by SSE 4.1

__forceinline __m128i_u16 _mm_blendv_epu16 (__m128i_u16 a, __m128i_u16 b, __m128i_u16 mask) {
	return _mm_castepi8_epu16(_mm_blendv_epi8(_mm_castepu16_epi8(a), _mm_castepu16_epi8(b), _mm_castepu16_epi8(mask)));
}
__forceinline __m128i_i16 _mm_blendv_epi16 (__m128i_i16 a, __m128i_i16 b, __m128i_i16 mask) {
	return _mm_castepi8_epi16(_mm_blendv_epi8(_mm_castepi16_epi8(a), _mm_castepi16_epi8(b), _mm_castepi16_epi8(mask)));
}
__forceinline __m128i_u32 _mm_blendv_epu32 (__m128i_u32 a, __m128i_u32 b, __m128i_u32 mask) {
	return _mm_castepi8_epu32(_mm_blendv_epi8(_mm_castepu32_epi8(a), _mm_castepu32_epi8(b), _mm_castepu32_epi8(mask)));
}
__forceinline __m128i_i32 _mm_blendv_epi32 (__m128i_i32 a, __m128i_i32 b, __m128i_i32 mask) {
	return _mm_castepi8_epi32(_mm_blendv_epi8(_mm_castepi32_epi8(a), _mm_castepi32_epi8(b), _mm_castepi32_epi8(mask)));
}
__forceinline __m128i_u64 _mm_blendv_epu64 (__m128i_u64 a, __m128i_u64 b, __m128i_u64 mask) {
	return _mm_castepi8_epu64(_mm_blendv_epi8(_mm_castepu64_epi8(a), _mm_castepu64_epi8(b), _mm_castepu64_epi8(mask)));
}
__forceinline __m128i_i64 _mm_blendv_epi64 (__m128i_i64 a, __m128i_i64 b, __m128i_i64 mask) {
	return _mm_castepi8_epi64(_mm_blendv_epi8(_mm_castepi64_epi8(a), _mm_castepi64_epi8(b), _mm_castepi64_epi8(mask)));
}
/***************************************************************************************************/
// Portable blend
/***************************************************************************************************/
//_mm_blend_epi8 MISSING!!!
//_mm_blend_epi16 already implemented by SSE 4.1
#if !__AVX2__ || NO_AVX2
//__m128i_u8 _mm_blend_epi32(__m128i_i32 a, __m128i_i32 b, const int imm8)
#define _mm_blend_epi32(a,b,imm8) _mm_castps_epi32(_mm_blend_ps(_mm_castepi32_ps(a), _mm_castepi32_ps(b), imm8 ))

#endif
//_mm_blend_epi64 MISSING!!!



//_mm_blend_epu8  MISSING!!!
#define _mm_blend_epu16 _mm_castepi16_epu16(_mm_blend_epi16(_mm_castepu16_epi16(a), _mm_castepu16_epi16(b), imm8))
#define _mm_blend_epu32 _mm_castepi32_epu32(_mm_blend_epi32(_mm_castepu32_epi32(a), _mm_castepu32_epi32(b), imm8))
//_mm_blend_epu64  MISSING!!!


#endif
