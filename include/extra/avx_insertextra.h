/***************************************************************************************************/
// HEADER_NAME /extra/avx_insertextra.h
/***************************************************************************************************/

#if __AVX__
//__m128i_u8 _mm_insert_epu8 (__m128i_u8 a, __uint8 i, const int imm8)
#define _mm256_insert_epu8(a, i, imm8) _mm256_insert_epi8(a, (__int8)i, imm8)

//__m128i_u16 _mm_insert_epi16 (__m128i_u16 a, __uint16 i,const int imm8)
#define _mm256_insert_epu16(a, i, imm8) _mm256_insert_epi16(a, (__int16)i, imm8)

//__m128i_u32 _mm_insert_epu32 (__m128i_u32 a, int i, const int imm8)
#define _mm256_insert_epu32(a, i, imm8) _mm256_insert_epi32(a, (__int32)i, imm8)

//__m128i_u64 _mm_insert_epu64 (__m128i_u64 a, __uint64 i, const int imm8)
#define _mm256_insert_epu64(a, i, imm8) _mm256_insert_epi64(a, (__int64)i, imm8)

__forceinline __m256 _mm256_insert_ps(__m256 a, __m256 b, const int imm8)
{
	if (imm8 == 0)
	{
		return _mm256_set_ps
		(
		_mm256_extract_ps(a, 7), _mm256_extract_ps(a, 6),_mm256_extract_ps(a, 5), _mm256_extract_ps(a, 4),
		_mm256_extract_ps(a, 3), _mm256_extract_ps(a, 2),_mm256_extract_ps(a, 1), _mm256_extract_ps(b, 0)
		);
	}
	else if(imm8 == 1)
	{
		return _mm256_set_ps
		(
		_mm256_extract_ps(a, 7), _mm256_extract_ps(a, 6),_mm256_extract_ps(a, 5), _mm256_extract_ps(a, 4),
		_mm256_extract_ps(a, 3), _mm256_extract_ps(a, 2),_mm256_extract_ps(b, 1), _mm256_extract_ps(a, 0)
		);
	}
	else if(imm8 == 2)
	{
				return _mm256_set_ps
		(
		_mm256_extract_ps(a, 7), _mm256_extract_ps(a, 6),_mm256_extract_ps(a, 5), _mm256_extract_ps(a, 4),
		_mm256_extract_ps(a, 3), _mm256_extract_ps(b, 2),_mm256_extract_ps(a, 1), _mm256_extract_ps(a, 0)
		);
	}
	else if(imm8 == 3)
	{
		return _mm256_set_ps
		(
		_mm256_extract_ps(a, 7), _mm256_extract_ps(a, 6),_mm256_extract_ps(a, 5), _mm256_extract_ps(a, 4),
		_mm256_extract_ps(b, 3), _mm256_extract_ps(a, 2),_mm256_extract_ps(a, 1), _mm256_extract_ps(a, 0)
		);
	}
	else if(imm8 == 4)
	{
		return _mm256_set_ps
		(
		_mm256_extract_ps(a, 7), _mm256_extract_ps(a, 6),_mm256_extract_ps(a, 5), _mm256_extract_ps(b, 4),
		_mm256_extract_ps(a, 3), _mm256_extract_ps(a, 2),_mm256_extract_ps(a, 1), _mm256_extract_ps(a, 0)
		);
	}
	else if(imm8 == 5)
	{
		return _mm256_set_ps
		(
		_mm256_extract_ps(a, 7), _mm256_extract_ps(a, 6),_mm256_extract_ps(b, 5), _mm256_extract_ps(a, 4),
		_mm256_extract_ps(a, 3), _mm256_extract_ps(a, 2),_mm256_extract_ps(a, 1), _mm256_extract_ps(a, 0)
		);
	}
	else if(imm8 == 6)
	{
		return _mm256_set_ps
		(
		_mm256_extract_ps(a, 7), _mm256_extract_ps(b, 6),_mm256_extract_ps(a, 5), _mm256_extract_ps(a, 4),
		_mm256_extract_ps(a, 3), _mm256_extract_ps(a, 2),_mm256_extract_ps(a, 1), _mm256_extract_ps(a, 0)
		);
	}
	else if(imm8 == 7)
	{
		return _mm256_set_ps
		(
		_mm256_extract_ps(b, 7), _mm256_extract_ps(a, 6),_mm256_extract_ps(a, 5), _mm256_extract_ps(a, 4),
		_mm256_extract_ps(a, 3), _mm256_extract_ps(a, 2),_mm256_extract_ps(a, 1), _mm256_extract_ps(a, 0)
		);
	}
}

__forceinline __m256d _mm256_insert_pd(__m256d a, __m256d b, const int imm8)
{
	if (imm8 == 0)
	{
		return _mm256_set_pd(_mm256_extract_pd(a, 3), _mm256_extract_pd(a, 2),_mm256_extract_pd(a, 1), _mm256_extract_pd(b, 0));
	}
	else if(imm8 == 1)
	{
		return _mm256_set_pd(_mm256_extract_pd(a, 3), _mm256_extract_pd(a, 2),_mm256_extract_pd(b, 1), _mm256_extract_pd(a, 0));
	}
	else if(imm8 == 2)
	{
		return _mm256_set_pd(_mm256_extract_pd(a, 3), _mm256_extract_pd(b, 2),_mm256_extract_pd(a, 1), _mm256_extract_pd(a, 0));
	}
	else //(imm8 == 3)
	{
		return _mm256_set_pd(_mm256_extract_pd(b, 3), _mm256_extract_pd(a, 2),_mm256_extract_pd(a, 1), _mm256_extract_pd(a, 0));
	}
}

#endif