/***************************************************************************************************/
// HEADER_NAME /extra/avx2_multiplicationextra.h
/***************************************************************************************************/

/***************************************************************************************************/
// MULTIPLICATION
/***************************************************************************************************/

#if __AVX2__
__forceinline __m256i_u8 _mm256_mul_epu8 (__m256i_u8 a, __m256i_u8 b)
{
	return _mm256_set_epi8
	(
		(__uint8)_mm256_extract_epi8(a, 31)* (__uint8)_mm256_extract_epi8(b, 31),
		(__uint8)_mm256_extract_epi8(a, 30)* (__uint8)_mm256_extract_epi8(b, 30),
		(__uint8)_mm256_extract_epi8(a, 29)* (__uint8)_mm256_extract_epi8(b, 29),
		(__uint8)_mm256_extract_epi8(a, 28)* (__uint8)_mm256_extract_epi8(b, 28),
		(__uint8)_mm256_extract_epi8(a, 27)* (__uint8)_mm256_extract_epi8(b, 27),
		(__uint8)_mm256_extract_epi8(a, 26)* (__uint8)_mm256_extract_epi8(b, 26),
		(__uint8)_mm256_extract_epi8(a, 25)* (__uint8)_mm256_extract_epi8(b, 25),
		(__uint8)_mm256_extract_epi8(a, 24)* (__uint8)_mm256_extract_epi8(b, 24),
		(__uint8)_mm256_extract_epi8(a, 23)* (__uint8)_mm256_extract_epi8(b, 23),
		(__uint8)_mm256_extract_epi8(a, 22)* (__uint8)_mm256_extract_epi8(b, 22),
		(__uint8)_mm256_extract_epi8(a, 21)* (__uint8)_mm256_extract_epi8(b, 21),
		(__uint8)_mm256_extract_epi8(a, 20)* (__uint8)_mm256_extract_epi8(b, 20),
		(__uint8)_mm256_extract_epi8(a, 19)* (__uint8)_mm256_extract_epi8(b, 19),
		(__uint8)_mm256_extract_epi8(a, 18)* (__uint8)_mm256_extract_epi8(b, 18),
		(__uint8)_mm256_extract_epi8(a, 17)* (__uint8)_mm256_extract_epi8(b, 17),
		(__uint8)_mm256_extract_epi8(a, 16)* (__uint8)_mm256_extract_epi8(b, 16),
		(__uint8)_mm256_extract_epi8(a, 15)* (__uint8)_mm256_extract_epi8(b, 15),
		(__uint8)_mm256_extract_epi8(a, 14)* (__uint8)_mm256_extract_epi8(b, 14),
		(__uint8)_mm256_extract_epi8(a, 13)* (__uint8)_mm256_extract_epi8(b, 13),
		(__uint8)_mm256_extract_epi8(a, 12)* (__uint8)_mm256_extract_epi8(b, 12),
		(__uint8)_mm256_extract_epi8(a, 11)* (__uint8)_mm256_extract_epi8(b, 11),
		(__uint8)_mm256_extract_epi8(a, 10)* (__uint8)_mm256_extract_epi8(b, 10),
		(__uint8)_mm256_extract_epi8(a, 9)* (__uint8)_mm256_extract_epi8(b, 9),
		(__uint8)_mm256_extract_epi8(a, 8)* (__uint8)_mm256_extract_epi8(b, 8),
		(__uint8)_mm256_extract_epi8(a, 7)* (__uint8)_mm256_extract_epi8(b, 7),
		(__uint8)_mm256_extract_epi8(a, 6)* (__uint8)_mm256_extract_epi8(b, 6),
		(__uint8)_mm256_extract_epi8(a, 5)* (__uint8)_mm256_extract_epi8(b, 5),
		(__uint8)_mm256_extract_epi8(a, 4)* (__uint8)_mm256_extract_epi8(b, 4),
		(__uint8)_mm256_extract_epi8(a, 3)* (__uint8)_mm256_extract_epi8(b, 3),
		(__uint8)_mm256_extract_epi8(a, 2)* (__uint8)_mm256_extract_epi8(b, 2),
		(__uint8)_mm256_extract_epi8(a, 1)* (__uint8)_mm256_extract_epi8(b, 1),
		(__uint8)_mm256_extract_epi8(a, 0)* (__uint8)_mm256_extract_epi8(b, 0)
	);
}
__forceinline __m256i_i8 _mm256_mul_epi8 (__m256i_i8 a, __m256i_i8 b)
{
	return _mm256_set_epi8
	(
		_mm256_extract_epi8(a,31) * _mm256_extract_epi8(b,31),
		_mm256_extract_epi8(a,30) * _mm256_extract_epi8(b,30),
		_mm256_extract_epi8(a,29) * _mm256_extract_epi8(b,29),
		_mm256_extract_epi8(a,28) * _mm256_extract_epi8(b,28),
		_mm256_extract_epi8(a,27) * _mm256_extract_epi8(b,27),
		_mm256_extract_epi8(a,26) * _mm256_extract_epi8(b,26),
		_mm256_extract_epi8(a,25) * _mm256_extract_epi8(b,25),
		_mm256_extract_epi8(a,24) * _mm256_extract_epi8(b,24),
		_mm256_extract_epi8(a,23) * _mm256_extract_epi8(b,23),
		_mm256_extract_epi8(a,22) * _mm256_extract_epi8(b,22),
		_mm256_extract_epi8(a,21) * _mm256_extract_epi8(b,21),
		_mm256_extract_epi8(a,20) * _mm256_extract_epi8(b,20),
		_mm256_extract_epi8(a,19) * _mm256_extract_epi8(b,19),
		_mm256_extract_epi8(a,18) * _mm256_extract_epi8(b,18),
		_mm256_extract_epi8(a,17) * _mm256_extract_epi8(b,17),
		_mm256_extract_epi8(a,16) * _mm256_extract_epi8(b,16),
		_mm256_extract_epi8(a,15) * _mm256_extract_epi8(b,15),
		_mm256_extract_epi8(a,14) * _mm256_extract_epi8(b,14),
		_mm256_extract_epi8(a,13) * _mm256_extract_epi8(b,13),
		_mm256_extract_epi8(a,12) * _mm256_extract_epi8(b,12),
		_mm256_extract_epi8(a,11) * _mm256_extract_epi8(b,11),
		_mm256_extract_epi8(a,10) * _mm256_extract_epi8(b,10),
		_mm256_extract_epi8(a,9) * _mm256_extract_epi8(b,9),
		_mm256_extract_epi8(a,8) * _mm256_extract_epi8(b,8),
		_mm256_extract_epi8(a,7) * _mm256_extract_epi8(b,7),
		_mm256_extract_epi8(a,6) * _mm256_extract_epi8(b,6),
		_mm256_extract_epi8(a,5) * _mm256_extract_epi8(b,5),
		_mm256_extract_epi8(a,4) * _mm256_extract_epi8(b,4),
		_mm256_extract_epi8(a,3) * _mm256_extract_epi8(b,3),
		_mm256_extract_epi8(a,2) * _mm256_extract_epi8(b,2),
		_mm256_extract_epi8(a,1) * _mm256_extract_epi8(b,1),
		_mm256_extract_epi8(a,0) * _mm256_extract_epi8(b,0)
	);
}

__forceinline __m256i_u16 _mm256_mul_epu16 (__m256i_u16 a, __m256i_u16 b)
{
	return _mm256_set_epi16
	(
		(__uint16)_mm256_extract_epi16(a,15) * (__uint16)_mm256_extract_epi16(b,15),
		(__uint16)_mm256_extract_epi16(a,14) * (__uint16)_mm256_extract_epi16(b,14),
		(__uint16)_mm256_extract_epi16(a,13) * (__uint16)_mm256_extract_epi16(b,13),
		(__uint16)_mm256_extract_epi16(a,12) * (__uint16)_mm256_extract_epi16(b,12),
		(__uint16)_mm256_extract_epi16(a,11) * (__uint16)_mm256_extract_epi16(b,11),
		(__uint16)_mm256_extract_epi16(a,10) * (__uint16)_mm256_extract_epi16(b,10),
		(__uint16)_mm256_extract_epi16(a,9) * (__uint16)_mm256_extract_epi16(b,9),
		(__uint16)_mm256_extract_epi16(a,8) * (__uint16)_mm256_extract_epi16(b,8),
		(__uint16)_mm256_extract_epi16(a,7) * (__uint16)_mm256_extract_epi16(b,7),
		(__uint16)_mm256_extract_epi16(a,6) * (__uint16)_mm256_extract_epi16(b,6),
		(__uint16)_mm256_extract_epi16(a,5) * (__uint16)_mm256_extract_epi16(b,5),
		(__uint16)_mm256_extract_epi16(a,4) * (__uint16)_mm256_extract_epi16(b,4),
		(__uint16)_mm256_extract_epi16(a,3) * (__uint16)_mm256_extract_epi16(b,3),
		(__uint16)_mm256_extract_epi16(a,2) * (__uint16)_mm256_extract_epi16(b,2),
		(__uint16)_mm256_extract_epi16(a,1) * (__uint16)_mm256_extract_epi16(b,1),
		(__uint16)_mm256_extract_epi16(a,0) * (__uint16)_mm256_extract_epi16(b,0)
	);
}
__forceinline __m256i_i16 _mm256_mul_epi16 (__m256i_i16 a, __m256i_i16 b)
{
	return _mm256_set_epi16
	(
		_mm256_extract_epi16(a,15) * _mm256_extract_epi16(b,15),
		_mm256_extract_epi16(a,14) * _mm256_extract_epi16(b,14),
		_mm256_extract_epi16(a,13) * _mm256_extract_epi16(b,13),
		_mm256_extract_epi16(a,12) * _mm256_extract_epi16(b,12),
		_mm256_extract_epi16(a,11) * _mm256_extract_epi16(b,11),
		_mm256_extract_epi16(a,10) * _mm256_extract_epi16(b,10),
		_mm256_extract_epi16(a,9) * _mm256_extract_epi16(b,9),
		_mm256_extract_epi16(a,8) * _mm256_extract_epi16(b,8),
		_mm256_extract_epi16(a,7) * _mm256_extract_epi16(b,7),
		_mm256_extract_epi16(a,6) * _mm256_extract_epi16(b,6),
		_mm256_extract_epi16(a,5) * _mm256_extract_epi16(b,5),
		_mm256_extract_epi16(a,4) * _mm256_extract_epi16(b,4),
		_mm256_extract_epi16(a,3) * _mm256_extract_epi16(b,3),
		_mm256_extract_epi16(a,2) * _mm256_extract_epi16(b,2),
		_mm256_extract_epi16(a,1) * _mm256_extract_epi16(b,1),
		_mm256_extract_epi16(a,0) * _mm256_extract_epi16(b,0)
	);
}
//__m256i_i32 _mm256_mul_epi32(__m256i_i32 a, __m256i_i32 b) //IMPEMENTED BY AVX2
//__m256i_u32 _mm256_mul_epu32(__m256i_u32 a, __m256i_u32 b) //IMPEMENTED BY AVX2


#if BIT64
__forceinline __m256i_u64 _mm256_mul_epu64 (__m256i_u64 a, __m256i_u64 b)
{
	return _mm256_set_epi64x(
		(__uint64)_mm256_extract_epi64 (a, 3) * (__uint64)_mm256_extract_epi64 (b, 3),
		(__uint64)_mm256_extract_epi64 (a, 2) * (__uint64)_mm256_extract_epi64 (b, 2),
		(__uint64)_mm256_extract_epi64 (a, 1) * (__uint64)_mm256_extract_epi64 (b, 1),
		(__uint64)_mm256_extract_epi64 (a, 0) * (__uint64)_mm256_extract_epi64 (b, 0)
	);
}
__forceinline __m256i_i64 _mm256_mul_epi64(__m256i_i64 a, __m256i_i64 b)
{
	return _mm256_set_epi64x(
		_mm256_extract_epi64(a, 3) * _mm256_extract_epi64(b, 3),
		_mm256_extract_epi64(a, 2) * _mm256_extract_epi64(b, 2),
		_mm256_extract_epi64(a, 1) * _mm256_extract_epi64(b, 1),
		_mm256_extract_epi64(a, 0) * _mm256_extract_epi64(b, 0)
	);
}
#endif

#endif

/***************************************************************************************************/
// 8-bit Integers Multiplication Lo Hi
/***************************************************************************************************/
#if __AVX2__
__forceinline __m256i_i16 _mm256_mullo_epi8(__m256i_i8 a, __m256i_i8 b) {
	// unpack and multiply
	__m256i_i16 v_even, v_odd;
	
	v_even = _mm256_mullo_epi16(_mm256_castepi8_epi16(a), _mm256_castepi8_epi16(b));
	v_odd = _mm256_mullo_epi16(_mm256_srli_epi16(_mm256_castepi8_epi16(a), 8),_mm256_srli_epi16(_mm256_castepi8_epi16(b), 8));
	
	return _mm256_or_epi8(_mm256_castepi16_epi8(_mm256_slli_epi16(v_odd, 8)), _mm256_castepi16_epi8(_mm256_and_epi16(v_even, _mm256_set1_hex_epi16(0xFF))));
}


__forceinline __m256i_i16 _mm256_mulhi_epi8(__m256i_i8 a, __m256i_i8 b) {
	// unpack and multiply
	__m256i_i16 v_even, v_odd;
	
	v_even = _mm256_mulhi_epi16(_mm256_castepi8_epi16(a), _mm256_castepi8_epi16(b));
	v_odd = _mm256_mulhi_epi16(_mm256_srli_epi16(_mm256_castepi8_epi16(a), 8),_mm256_srli_epi16(_mm256_castepi8_epi16(b), 8));
	
	return _mm256_or_epi8(_mm256_castepi16_epi8(_mm256_slli_epi16(v_odd, 8)), _mm256_castepi16_epi8(_mm256_and_epi16(v_even, _mm256_set1_hex_epi16(0xFF))));
}

/*
TODO
__m256i_u64 _mm256_mulhi_epu32(__m256i_u64 a, __m256i_u64 b)
__m256i_i64 _mm256_mulhi_epi32(__m256i_i64 a, __m256i_i64 b)
*/

#if !__AVX512VL__ && !__AVX512F__
#if BIT64
#undef _mm256_mullo_epi64
#define _mm256_mullo_epi64 _mm256_mullo_epi64_impl
__forceinline __m256i_i64 _mm256_mullo_epi64_impl(__m256i_i64 a, __m256i_i64 b) {
	return _mm256_add_epi64(
		_mm256_mul_epu32(a, b),
		_mm256_slli_epi64
		(
			_mm256_add_epi64(
				_mm256_mul_epu32(b, _mm256_shuffle_epi32(a, _MM_SHUFFLE(2, 3, 0, 1))),
				_mm256_mul_epu32(a, _mm256_shuffle_epi32(b, _MM_SHUFFLE(2, 3, 0, 1)))
			), 32
		)
	);
}
/*
TODO
__m256i_i64 _mm256_mulhi_epi64(__m256i_i64 a, __m256i_i64 b)
*/

#endif
#endif

#endif