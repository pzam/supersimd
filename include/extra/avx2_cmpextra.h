/***************************************************************************************************/
// HEADER_NAME /extra/avx2_cmpextra.h
/***************************************************************************************************/
/***************************************************************************************************/
// AVX2
/***************************************************************************************************/

#if __AVX2__
/***************************************************************************************************/
// Unsigned
/***************************************************************************************************/


/***************************************************************************************************/
// EQ
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cmpeq_epu8(__m256i_u8 a, __m256i_u8 b)
{
	return _mm256_castepi8_epu8(_mm256_cmpeq_epi8(_mm256_castepu8_epi8(a), _mm256_castepu8_epi8(b)));
}
__forceinline __m256i_u16 _mm256_cmpeq_epu16(__m256i_u16 a, __m256i_u16 b)
{
	return _mm256_castepi16_epu16(_mm256_cmpeq_epi16(_mm256_castepu16_epi16(a), _mm256_castepu16_epi16(b)));
}
__forceinline __m256i_u32 _mm256_cmpeq_epu32(__m256i_u32 a, __m256i_u32 b)
{
	return _mm256_castepi32_epu32(_mm256_cmpeq_epi32(_mm256_castepu32_epi32(a), _mm256_castepu32_epi32(b)));
}
__forceinline __m256i_u64 _mm256_cmpeq_epu64(__m256i_u64 a, __m256i_u64 b)
{
	return _mm256_castepi64_epu64(_mm256_cmpeq_epi64(_mm256_castepu64_epi64(a), _mm256_castepu64_epi64(b)));
}
/***************************************************************************************************/
// NE
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cmpneq_epu8(__m256i_u8 a, __m256i_u8 b)
{
	return _mm256_not_epu8(_mm256_cmpeq_epu8(a, b));
}
__forceinline __m256i_u16 _mm256_cmpneq_epu16(__m256i_u16 a, __m256i_u16 b)
{
	return _mm256_not_epu16(_mm256_cmpeq_epu16(a, b));
}
__forceinline __m256i_u32 _mm256_cmpneq_epu32(__m256i_u32 a, __m256i_u32 b)
{
	return _mm256_not_epu32(_mm256_cmpeq_epu32(a, b));
}
__forceinline __m256i_u64 _mm256_cmpneq_epu64(__m256i_u64 a, __m256i_u64 b)
{
	return _mm256_not_epu64(_mm256_cmpeq_epu64(a, b));
}



/***************************************************************************************************/
// GE
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cmpge_epu8(__m256i_u8 a, __m256i_u8 b)
{
	return _mm256_castepi8_epu8(_mm256_cmpeq_epi8(_mm256_castepu8_epi8(_mm256_max_epu8(a, b)), _mm256_castepu8_epi8(a)));
}
__forceinline __m256i_u16 _mm256_cmpge_epu16(__m256i_u16 a, __m256i_u16 b)
{
	return _mm256_castepi16_epu16(_mm256_cmpeq_epi16(_mm256_castepu16_epi16(_mm256_max_epu16(a, b)), _mm256_castepu16_epi16(a)));
}
__forceinline __m256i_u32 _mm256_cmpge_epu32(__m256i_u32 a, __m256i_u32 b)
{
	return _mm256_castepi32_epu32(_mm256_cmpeq_epi32(_mm256_castepu32_epi32(_mm256_max_epu32(a, b)), _mm256_castepu32_epi32(a)));
}
__forceinline __m256i_u64 _mm256_cmpge_epu64(__m256i_u64 a, __m256i_u64 b)
{
	return _mm256_castepi64_epu64(_mm256_cmpeq_epi64(_mm256_castepu64_epi64(_mm256_max_epu64(a, b)), _mm256_castepu64_epi64(a)));
}
/***************************************************************************************************/
// NGE
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cmpnge_epu8(__m256i_u8 a, __m256i_u8 b)
{
	return _mm256_not_epu8(_mm256_cmpge_epu8(a,b));
}
__forceinline __m256i_u16 _mm256_cmpnge_epu16(__m256i_u16 a, __m256i_u16 b)
{
	return _mm256_not_epu16(_mm256_cmpge_epu16(a,b));
}
__forceinline __m256i_u32 _mm256_cmpnge_epu32(__m256i_u32 a, __m256i_u32 b)
{
	return _mm256_not_epu32(_mm256_cmpge_epu32(a,b));
}
__forceinline __m256i_u64 _mm256_cmpnge_epu64(__m256i_u64 a, __m256i_u64 b)
{
	return _mm256_not_epu64(_mm256_cmpge_epu64(a,b));
}


/***************************************************************************************************/
// GT
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cmpgt_epu8(__m256i_u8 a, __m256i_u8 b)
{
	return _mm256_not_epu8(_mm256_cmpge_epu8(b, a));
}
__forceinline __m256i_u16 _mm256_cmpgt_epu16(__m256i_u16 a, __m256i_u16 b)
{
	return _mm256_not_epu16(_mm256_cmpge_epu16(b, a));
}
__forceinline __m256i_u32 _mm256_cmpgt_epu32(__m256i_u32 a, __m256i_u32 b)
{
	return _mm256_not_epu32(_mm256_cmpge_epu32(b, a));
}
__forceinline __m256i_u64 _mm256_cmpgt_epu64(__m256i_u64 a, __m256i_u64 b)
{
	return _mm256_not_epu64(_mm256_cmpge_epu64(b, a));
}
/***************************************************************************************************/
// NGT
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cmpngt_epu8(__m256i_u8 a, __m256i_u8 b)
{
	return _mm256_cmpge_epu8(b, a);
}
__forceinline __m256i_u16 _mm256_cmpngt_epu16(__m256i_u16 a, __m256i_u16 b)
{
	return _mm256_cmpge_epu16(b, a);
}
__forceinline __m256i_u32 _mm256_cmpngt_epu32(__m256i_u32 a, __m256i_u32 b)
{
	return _mm256_cmpge_epu32(b, a);
}
__forceinline __m256i_u64 _mm256_cmpngt_epu64(__m256i_u64 a, __m256i_u64 b)
{
	return _mm256_cmpge_epu64(b, a);
}


/***************************************************************************************************/
// LE
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cmple_epu8(__m256i_u8 a, __m256i_u8 b) {
	return _mm256_castepi8_epu8(_mm256_cmpeq_epi8(_mm256_castepu8_epi8(_mm256_min_epu8(a, b)), _mm256_castepu8_epi8(a)));
}
__forceinline __m256i_u16 _mm256_cmple_epu16(__m256i_u16 a, __m256i_u16 b) {
	return _mm256_castepi16_epu16(_mm256_cmpeq_epi16(_mm256_castepu16_epi16(_mm256_min_epu16(a, b)), _mm256_castepu16_epi16(a)));
}
__forceinline __m256i_u32 _mm256_cmple_epu32(__m256i_u32 a, __m256i_u32 b) {
	return _mm256_castepi32_epu32(_mm256_cmpeq_epi32(_mm256_castepu32_epi32(_mm256_min_epu32(a, b)), _mm256_castepu32_epi32(a)));
}
__forceinline __m256i_u64 _mm256_cmple_epu64(__m256i_u64 a, __m256i_u64 b) {
	return _mm256_castepi64_epu64(_mm256_cmpeq_epi64(_mm256_castepu64_epi64(_mm256_min_epu64(a, b)), _mm256_castepu64_epi64(a)));
}
/***************************************************************************************************/
// NLE
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cmpnle_epu8(__m256i_u8 a, __m256i_u8 b) {
	return _mm256_not_epu8(_mm256_cmple_epu8(a, b));
}
__forceinline __m256i_u16 _mm256_cmpnle_epu16(__m256i_u16 a, __m256i_u16 b) {
	return _mm256_not_epu16(_mm256_cmple_epu16(a, b));
}
__forceinline __m256i_u32 _mm256_cmpnle_epu32(__m256i_u32 a, __m256i_u32 b) {
	return _mm256_not_epu32(_mm256_cmple_epu32(a, b));
}
__forceinline __m256i_u64 _mm256_cmpnle_epu64(__m256i_u64 a, __m256i_u64 b) {
	return _mm256_not_epu64(_mm256_cmple_epu64(a, b));
}


/***************************************************************************************************/
// LT
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cmplt_epu8(__m256i_u8 a, __m256i_u8 b) {
	return _mm256_not_epu8(_mm256_cmple_epu8(b, a));
}
__forceinline __m256i_u16 _mm256_cmplt_epu16(__m256i_u16 a, __m256i_u16 b) {
	return _mm256_not_epu8(_mm256_cmple_epu16(b, a));
}
__forceinline __m256i_u32 _mm256_cmplt_epu32(__m256i_u32 a, __m256i_u32 b) {
	return _mm256_not_epu32(_mm256_cmple_epu32(b, a));
}
__forceinline __m256i_u64 _mm256_cmplt_epu64(__m256i_u64 a, __m256i_u64 b) {
	return _mm256_not_epu64(_mm256_cmple_epu64(b, a));
}
/***************************************************************************************************/
// NLT
/***************************************************************************************************/
__forceinline __m256i_u8 _mm256_cmpnlt_epu8(__m256i_u8 a, __m256i_u8 b) {
	return _mm256_cmple_epu8(b, a);
}
__forceinline __m256i_u16 _mm256_cmpnlt_epu16(__m256i_u16 a, __m256i_u16 b) {
	return _mm256_cmple_epu16(b, a);
}
__forceinline __m256i_u32 _mm256_cmpnlt_epu32(__m256i_u32 a, __m256i_u32 b) {
	return _mm256_cmple_epu32(b, a);
}
__forceinline __m256i_u64 _mm256_cmpnlt_epu64(__m256i_u64 a, __m256i_u64 b) {
	return _mm256_cmple_epu64(b, a);
}



/***************************************************************************************************/
// Signed
/***************************************************************************************************/


// NOTE: GE Depends on LT DO NOT REODER!!!

/***************************************************************************************************/
// LT
/***************************************************************************************************/
__forceinline __m256i_i8 _mm256_cmplt_epi8(__m256i_i8 a, __m256i_i8 b)
{
	return _mm256_cmpgt_epi8(b, a);
}
__forceinline __m256i_i16 _mm256_cmplt_epi16(__m256i_i16 a, __m256i_i16 b)
{
	return _mm256_cmpgt_epi16(b, a);
}
__forceinline __m256i_i32 _mm256_cmplt_epi32(__m256i_i32 a, __m256i_i32 b)
{
	return _mm256_cmpgt_epi32(b, a);
}
__forceinline __m256i_i64 _mm256_cmplt_epi64(__m256i_i64 a, __m256i_i64 b)
{
	return _mm256_cmpgt_epi64(b, a);
}
/***************************************************************************************************/
// NLT
/***************************************************************************************************/
__forceinline __m256i_i8 _mm256_cmpnlt_epi8(__m256i_i8 a, __m256i_i8 b) {
	return _mm256_not_epi8(_mm256_cmplt_epi8(a, b));
}
__forceinline __m256i_i16 _mm256_cmpnlt_epi16(__m256i_i16 a, __m256i_i16 b) {
	return _mm256_not_epi16(_mm256_cmplt_epi16(a, b));
}
__forceinline __m256i_i32 _mm256_cmpnlt_epi32(__m256i_i32 a, __m256i_i32 b) {
	return _mm256_not_epi32(_mm256_cmplt_epi32(a, b));
}
__forceinline __m256i_i64 _mm256_cmpnlt_epi64(__m256i_i64 a, __m256i_i64 b) {
	return _mm256_not_epi64(_mm256_cmplt_epi64(a, b));
}



/***************************************************************************************************/
// EQ
/***************************************************************************************************/
/*
__m256i_i8 _mm256_cmpeq_epi8 (__m256i_i8 a, __m256i_i8 b)		//IMPEMENTED BY AVX2
__m256i_i16 _mm256_cmpeq_epi16 (__m256i_i16 a, __m256i_i16 b)	//IMPEMENTED BY AVX2
__m256i_i32 _mm256_cmpeq_epi32 (__m256i_i32 a, __m256i_i32 b)	//IMPEMENTED BY AVX2
__m256i_i64 _mm256_cmpeq_epi64 (__m256i_i64 a, __m256i_i64 b)	//IMPEMENTED BY AVX2
*/

/***************************************************************************************************/
// NE
/***************************************************************************************************/
__forceinline __m256i_i8 _mm256_cmpneq_epi8(__m256i_i8 a, __m256i_i8 b)
{
	return _mm256_not_epi8(_mm256_cmpeq_epi8(a, b));
}
__forceinline __m256i_i16 _mm256_cmpneq_epi16(__m256i_i16 a, __m256i_i16 b)
{
	return _mm256_not_epi16(_mm256_cmpeq_epi16(a, b));
}
__forceinline __m256i_i32 _mm256_cmpneq_epi32(__m256i_i32 a, __m256i_i32 b)
{
	return _mm256_not_epi32(_mm256_cmpeq_epi32(a, b));
}
__forceinline __m256i_i64 _mm256_cmpneq_epi64(__m256i_i64 a, __m256i_i64 b)
{
	return _mm256_not_epi64(_mm256_cmpeq_epi64(a, b));
}



/***************************************************************************************************/
// GE
/***************************************************************************************************/
__forceinline __m256i_i8 _mm256_cmpge_epi8(__m256i_i8 a, __m256i_i8 b)
{
	return _mm256_not_epi8(_mm256_cmplt_epi8(a, b));
}
__forceinline __m256i_i16 _mm256_cmpge_epi16(__m256i_i16 a, __m256i_i16 b)
{
	return _mm256_not_epi16(_mm256_cmplt_epi16(a, b));
}
__forceinline __m256i_i32 _mm256_cmpge_epi32(__m256i_i32 a, __m256i_i32 b)
{
	return _mm256_not_epi32(_mm256_cmplt_epi32(a, b));
}
__forceinline __m256i_i64 _mm256_cmpge_epi64(__m256i_i64 a, __m256i_i64 b)
{
	return _mm256_not_epi64(_mm256_cmplt_epi64(a, b));
}
/***************************************************************************************************/
// NGE
/***************************************************************************************************/
__forceinline __m256i_i8 _mm256_cmpnge_epi8(__m256i_i8 a, __m256i_i8 b)
{
	return _mm256_cmplt_epi8(a, b);
}
__forceinline __m256i_i16 _mm256_cmpnge_epi16(__m256i_i16 a, __m256i_i16 b)
{
	return _mm256_cmplt_epi16(a, b);
}
__forceinline __m256i_i32 _mm256_cmpnge_epi32(__m256i_i32 a, __m256i_i32 b)
{
	return _mm256_cmplt_epi32(a, b);
}
__forceinline __m256i_i64 _mm256_cmpnge_epi64(__m256i_i64 a, __m256i_i64 b)
{
	return _mm256_cmplt_epi64(a, b);
}


/***************************************************************************************************/
// GT
/***************************************************************************************************/
/*
__m256i_i8 _mm256_cmpgt_epi8 (__m256i_i8 a, __m256i_i8 b)		//IMPEMENTED BY AVX2
__m256i_i16 _mm256_cmpgt_epi16 (__m256i_i16 a, __m256i_i16 b)	//IMPEMENTED BY AVX2
__m256i_i32 _mm256_cmpgt_epi32 (__m256i_i32 a, __m256i_i32 b)	//IMPEMENTED BY AVX2
__m256i_i64 _mm256_cmpgt_epi64 (__m256i_i64 a, __m256i_i64 b)	//IMPEMENTED BY AVX2
*/


/***************************************************************************************************/
// NGT
/***************************************************************************************************/
__forceinline __m256i_i8 _mm256_cmpngt_epi8(__m256i_i8 a, __m256i_i8 b)
{
	return _mm256_not_epi8(_mm256_cmpgt_epi8(a, b));
}
__forceinline __m256i_i16 _mm256_cmpngt_epi16(__m256i_i16 a, __m256i_i16 b)
{
	return _mm256_not_epi16(_mm256_cmpgt_epi16(a, b));
}
__forceinline __m256i_i32 _mm256_cmpngt_epi32(__m256i_i32 a, __m256i_i32 b)
{
	return _mm256_not_epi32(_mm256_cmpgt_epi32(a, b));
}
__forceinline __m256i_i64 _mm256_cmpngt_epi64(__m256i_i64 a, __m256i_i64 b)
{
	return _mm256_not_epi64(_mm256_cmpgt_epi64(a, b));
}


/***************************************************************************************************/
// LE
/***************************************************************************************************/
__forceinline __m256i_i8 _mm256_cmple_epi8(__m256i_i8 a, __m256i_i8 b) {
	return _mm256_not_epi8(_mm256_cmpgt_epi8(a, b));
}
__forceinline __m256i_i16 _mm256_cmple_epi16(__m256i_i16 a, __m256i_i16 b) {
	return _mm256_not_epi16(_mm256_cmpgt_epi16(a, b));
}
__forceinline __m256i_i32 _mm256_cmple_epi32(__m256i_i32 a, __m256i_i32 b) {
	return _mm256_not_epi32(_mm256_cmpgt_epi32(a, b));
}
__forceinline __m256i_i64 _mm256_cmple_epi64(__m256i_i64 a, __m256i_i64 b) {
	return _mm256_not_epi64(_mm256_cmpgt_epi64(a, b));
}
/***************************************************************************************************/
// NLE
/***************************************************************************************************/
__forceinline __m256i_i8 _mm256_cmpnle_epi8(__m256i_i8 a, __m256i_i8 b) {
	return _mm256_cmpgt_epi8(a, b);
}
__forceinline __m256i_i16 _mm256_cmpnle_epi16(__m256i_i16 a, __m256i_i16 b) {
	return _mm256_cmpgt_epi16(a, b);
}
__forceinline __m256i_i32 _mm256_cmpnle_epi32(__m256i_i32 a, __m256i_i32 b) {
	return _mm256_cmpgt_epi32(a, b);
}
__forceinline __m256i_i64 _mm256_cmpnle_epi64(__m256i_i64 a, __m256i_i64 b) {
	return _mm256_cmpgt_epi64(a, b);
}
#endif
