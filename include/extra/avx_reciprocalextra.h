/***************************************************************************************************/
// HEADER_NAME /extra/avx_reciprocalextra.h
/***************************************************************************************************/

/***************************************************************************************************/
// RECIPROCAL
/***************************************************************************************************/

#if !__AVX512VL__ && !__AVX512F__
#if __AVX__
#undef _mm256_rcp14_pd
#define _mm256_rcp14_pd _mm256_rcp14_pd_impl
__forceinline __m256d _mm256_rcp14_pd_impl(__m256d a)
{
	return _mm256_div_pd(__m256d_1, a);
}
#endif
#endif

#if __AVX__
__forceinline __m256d _mm256_rsqrt_pd(__m256d a)
{
	return _mm256_rcp14_pd(_mm256_sqrt_pd(a));
}
#endif