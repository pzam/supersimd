/***************************************************************************************************/
// HEADER_NAME /extra/avx_setextra.h
/***************************************************************************************************/
#if __AVX__
/***************************************************************************************************/
// SET Vector
/***************************************************************************************************/
#define _mm256_set_m128i_u8 _mm256_set_m128i
#define _mm256_set_m128i_u16 _mm256_set_m128i
#define _mm256_set_m128i_u32 _mm256_set_m128i
#define _mm256_set_m128i_u64 _mm256_set_m128i

#define _mm256_set_m128i_i8 _mm256_set_m128i
#define _mm256_set_m128i_i16 _mm256_set_m128i
#define _mm256_set_m128i_i32 _mm256_set_m128i
#define _mm256_set_m128i_i64 _mm256_set_m128i

/***************************************************************************************************/
// SET UNSIGNED INTERGERS
/***************************************************************************************************/
#define _mm256_set_epu8 _mm256_set_epi8
#define _mm256_set_epu16 _mm256_set_epi16
#define _mm256_set_epu32 _mm256_set_epi32
#define _mm256_set_epu64x _mm256_set_epi64x

#define _mm256_setr_epu8 _mm256_setr_epi8
#define _mm256_setr_epu16 _mm256_setr_epi16
#define _mm256_setr_epu32 _mm256_setr_epi32
#define _mm256_setr_epu64x _mm256_setr_epi64x

#define _mm256_set1_epu8 _mm256_set1_epi8
#define _mm256_set1_epu16 _mm256_set1_epi16
#define _mm256_set1_epu32 _mm256_set1_epi32
#define _mm256_set1_epu64x _mm256_set1_epi64x

/***************************************************************************************************/
// SET HEXADECIMAL VALUE
/***************************************************************************************************/

#define _mm256_set_hex_epu8 _mm256_set_epu8
#define _mm256_set_hex_epi8 _mm256_set_epi8
#define _mm256_setr_hex_epu8 _mm256_setr_epu8
#define _mm256_setr_hex_epi8 _mm256_setr_epi8
#define _mm256_set1_hex_epu8 _mm256_set1_epu8
#define _mm256_set1_hex_epi8 _mm256_set1_epi8

#define _mm256_set_hex_epu16 _mm256_set_epu16
#define _mm256_set_hex_epi16 _mm256_set_epi16
#define _mm256_setr_hex_epu16 _mm256_setr_epu16
#define _mm256_setr_hex_epi16 _mm256_setr_epi16
#define _mm256_set1_hex_epu16 _mm256_set1_epu16
#define _mm256_set1_hex_epi16 _mm256_set1_epi16

#define _mm256_set_hex_epu32 _mm256_set_epu32
#define _mm256_set_hex_epi32 _mm256_set_epi32
#define _mm256_setr_hex_epu32 _mm256_setr_epu32
#define _mm256_setr_hex_epi32 _mm256_setr_epi32
#define _mm256_set1_hex_epu32 _mm256_set1_epu32
#define _mm256_set1_hex_epi32 _mm256_set1_epi32

#define _mm256_set_hex_epu64x _mm256_set_epu64x
#define _mm256_set_hex_epi64x _mm256_set_epi64x
#define _mm256_setr_hex_epu64x _mm256_setr_epu64x
#define _mm256_setr_hex_epi64x _mm256_setr_epi64x
#define _mm256_set1_hex_epu64x _mm256_set1_epu64x
#define _mm256_set1_hex_epi64x _mm256_set1_epi64x

#define _mm256_set_hex_epu8 _mm256_set_epu8
#define _mm256_set_hex_epi8 _mm256_set_epi8
#define _mm256_setr_hex_epu8 _mm256_setr_epu8
#define _mm256_setr_hex_epi8 _mm256_setr_epi8

#define _mm256_set_hex_epu16 _mm256_set_epu16
#define _mm256_set_hex_epi16 _mm256_set_epi16
#define _mm256_setr_hex_epu16 _mm256_setr_epu16
#define _mm256_setr_hex_epi16 _mm256_setr_epi16

#define _mm256_set_hex_epu32 _mm256_set_epu32
#define _mm256_set_hex_epi32 _mm256_set_epi32
#define _mm256_setr_hex_epu32 _mm256_setr_epu32
#define _mm256_setr_hex_epi32 _mm256_setr_epi32

#define _mm256_set_hex_epu64x _mm256_set_epu64x
#define _mm256_set_hex_epi64x _mm256_set_epi64x
#define _mm256_setr_hex_epu64x _mm256_setr_epu64x
#define _mm256_setr_hex_epi64x _mm256_setr_epi64x


__forceinline __m256 _mm256_set_hex_ps
(
int e7, int e6, int e5, int e4, 
int e3, int e2, int e1, int e0
)
{
	return _mm256_castepi32_ps(_mm256_set_epi32(e7, e6, e5, e4, e3, e2, e1, e0));
}
__forceinline __m256 _mm256_setr_hex_ps
(
int e7, int e6, int e5, int e4, 
int e3, int e2, int e1, int e0
)
{
	return _mm256_castepi32_ps(_mm256_setr_epi32(e7, e6, e5, e4, e3, e2, e1, e0));
}
__forceinline __m256 _mm256_set1_hex_ps(int a)
{
	return _mm256_castepi32_ps(_mm256_set1_epi32(a));
}
__forceinline __m256d _mm256_set_hex_pd(__int64 e3, __int64 e2, __int64 e1, __int64 e0)
{
	return _mm256_castepi64_pd(_mm256_set_epi64x(e3, e2, e1, e0));
}
__forceinline __m256d _mm256_setr_hex_pd(__int64 e3, __int64 e2, __int64 e1, __int64 e0)
{
	return _mm256_castepi64_pd(_mm256_setr_epi64x(e3, e2, e1, e0));
}
__forceinline __m256d _mm256_set1_hex_pd(__int64 a)
{
	return _mm256_castepi64_pd(_mm256_set1_epi64x (a));
}

/***************************************************************************************************/
// SET-ZERO
/***************************************************************************************************/

__forceinline __m256i_u8 _mm256_setzero_epu8(void)
{
	return _mm256_setzero_si256();
}
__forceinline __m256i_i8 _mm256_setzero_epi8(void)
{
	return _mm256_setzero_si256();
}
__forceinline __m256i_u16 _mm256_setzero_epu16(void)
{
	return _mm256_setzero_si256();
}
__forceinline __m256i_i16 _mm256_setzero_epi16(void)
{
	return _mm256_setzero_si256();
}
__forceinline __m256i_u32 _mm256_setzero_epu32(void)
{
	return _mm256_setzero_si256();
}
__forceinline __m256i_i32 _mm256_setzero_epi32(void)
{
	return _mm256_setzero_si256();
}
__forceinline __m256i_u64 _mm256_setzero_epu64(void)
{
	return _mm256_setzero_si256();
}
__forceinline __m256i_i64 _mm256_setzero_epi64(void)
{
	return _mm256_setzero_si256();
}


/***************************************************************************************************/
// SET-UNDEFINED
/***************************************************************************************************/

//AVX
#define _mm256_undefined_epu8 _mm256_undefined_si256
#define _mm256_undefined_epi8 _mm256_undefined_si256
#define _mm256_undefined_epu16 _mm256_undefined_si256
#define _mm256_undefined_epi16 _mm256_undefined_si256
#define _mm256_undefined_epu32 _mm256_undefined_si256
#define _mm256_undefined_epi32 _mm256_undefined_si256
#define _mm256_undefined_epu64 _mm256_undefined_si256
#define _mm256_undefined_epi64 _mm256_undefined_si256

#endif
