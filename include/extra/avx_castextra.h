/***************************************************************************************************/
// HEADER_NAME /extra/avx_castextra.h
/***************************************************************************************************/

#if __AVX__
//AVX2
/*******************************************************************************/
//__m256i_i16 _mm256_castepi8_epi16 (__m256i_i8 a)
#define _mm256_castepi8_epi16 

//__m256i_i32 _mm256_castepi8_epi32 (__m256i_i8 a)
#define _mm256_castepi8_epi32 

//__m256i_i64 _mm256_castepi8_epi64 (__m256i_i8 a)
#define _mm256_castepi8_epi64 

//__m256i_u8 _mm256_castepi8_epu8 (__m256i_i8 a)
#define _mm256_castepi8_epu8 

//__m256i_u16 _mm256_castepi8_epu16 (__m256i_i8 a)
#define _mm256_castepi8_epu16 

//__m256i_u32 _mm256_castepi8_epu32 (__m256i_i8 a)
#define _mm256_castepi8_epu32 

//__m256i_u64 _mm256_castepi8_epu64 (__m256i_i8 a)
#define _mm256_castepi8_epu64 

//__m256 _mm256_castepi8_ps (__m256i_i8 a)
#define _mm256_castepi8_ps _mm256_castsi256_ps

//__m256d _mm256_castepi8_pd (__m256i_i8 a)
#define _mm256_castepi8_pd _mm256_castsi256_pd
/*******************************************************************************/

/*******************************************************************************/
//__m256i_i8 _mm256_castepi16_epi8 (__m256i_i16 a)
#define _mm256_castepi16_epi8 

//__m256i_i32 _mm256_castepi16_epi32 (__m256i_i16 a)
#define _mm256_castepi16_epi32 

//__m256i_i64 _mm256_castepi16_epi64 (__m256i_i16 a)
#define _mm256_castepi16_epi64 

//__m256i_u8 _mm256_castepi16_epu8 (__m256i_i16 a)
#define _mm256_castepi16_epu8 

//__m256i_u16 _mm256_castepi16_epu16 (__m256i_i16 a)
#define _mm256_castepi16_epu16 

//__m256i_u32 _mm256_castepi16_epu32 (__m256i_i16 a)
#define _mm256_castepi16_epu32 

//__m256i_u64 _mm256_castepi16_epu64 (__m256i_i16 a)
#define _mm256_castepi16_epu64 

//__m256 _mm256_castepi16_ps (__m256i_i16 a)
#define _mm256_castepi16_ps _mm256_castsi256_ps

//__m256d _mm256_castepi16_pd (__m256i_i16 a)
#define _mm256_castepi16_pd _mm256_castsi256_pd
/*******************************************************************************/

/*******************************************************************************/
//__m256i_i8 _mm256_castepi32_epi8 (__m256i_i32 a)
#define _mm256_castepi32_epi8 

//__m256i_i16 _mm256_castepi32_epi16 (__m256i_i32 a)
#define _mm256_castepi32_epi16 

//__m256i_i64 _mm256_castepi32_epi64 (__m256i_i32 a)
#define _mm256_castepi32_epi64 

//__m256i_u8 _mm256_castepi32_epu8 (__m256i_i32 a)
#define _mm256_castepi32_epu8 

//__m256i_u16 _mm256_castepi32_epu16 (__m256i_i32 a)
#define _mm256_castepi32_epu16 

//__m256i_u32 _mm256_castepi32_epu32 (__m256i_i32 a)
#define _mm256_castepi32_epu32 

//__m256i_u64 _mm256_castepi32_epu64 (__m256i_i32 a)
#define _mm256_castepi32_epu64 

//__m256 _mm256_castepi32_ps (__m256i_i32 a)
#define _mm256_castepi32_ps _mm256_castsi256_ps

//__m256d _mm256_castepi32_pd (__m256i_i32 a)
#define _mm256_castepi32_pd _mm256_castsi256_pd
/*******************************************************************************/

/*******************************************************************************/
//__m256i_i8 _mm256_castps_epi8 (__m256 a)
#define _mm256_castps_epi8 _mm256_castps_si256

//__m256i_i16 _mm256_castps_epi16 (__m256 a)
#define _mm256_castps_epi16 _mm256_castps_si256

//__m256i_i32 _mm256_castps_epi32 (__m256 a)
#define _mm256_castps_epi32 _mm256_castps_si256

//__m256i_i64 _mm256_castps_epi64 (__m256 a)
#define _mm256_castps_epi64 _mm256_castps_si256

//__m256i_u8 _mm256_castps_epu8 (__m256 a)
#define _mm256_castps_epu8 _mm256_castps_si256

//__m256i_u16 _mm256_castps_epu16 (__m256 a)
#define _mm256_castps_epu16 _mm256_castps_si256

//__m256i_u32 _mm256_castps_epu32 (__m256 a)
#define _mm256_castps_epu32 _mm256_castps_si256

//__m256i_u64 _mm256_castps_epu64 (__m256 a)
#define _mm256_castps_epu64 _mm256_castps_si256
/*******************************************************************************/

/*******************************************************************************/
//__m256i_i8 _mm256_castepu8_epi8 (__m256i_u8 a)
#define _mm256_castepu8_epi8 

//__m256i_i16 _mm256_castepu8_epi16 (__m256i_u8 a)
#define _mm256_castepu8_epi16 

//__m256i_i32 _mm256_castepu8_epi32 (__m256i_u8 a)
#define _mm256_castepu8_epi32 

//__m256i_i64 _mm256_castepu8_epi64 (__m256i_u8 a)
#define _mm256_castepu8_epi64 

//__m256i_u16 _mm256_castepu8_epu16 (__m256i_u8 a)
#define _mm256_castepu8_epu16 

//__m256i_u32 _mm256_castepu8_epu32 (__m256i_u8 a)
#define _mm256_castepu8_epu32 

//__m256i_u64 _mm256_castepu8_epu64 (__m256i_u8 a)
#define _mm256_castepu8_epu64 

//__m256 _mm256_castepu8_ps (__m256i_u8 a)
#define _mm256_castepu8_ps _mm256_castsi256_ps

//__m256d _mm256_castepu8_pd (__m256i_u8 a)
#define _mm256_castepu8_pd _mm256_castsi256_pd
/*******************************************************************************/

/*******************************************************************************/
//__m256i_i8 _mm256_castepu16_epi8 (__m256i_u16 a)
#define _mm256_castepu16_epi8 

//__m256i_i16 _mm256_castepu16_epi16 (__m256i_u16 a)
#define _mm256_castepu16_epi16 

//__m256i_i32 _mm256_castepu16_epi32 (__m256i_u16 a)
#define _mm256_castepu16_epi32 

//__m256i_i64 _mm256_castepu16_epi64 (__m256i_u16 a)
#define _mm256_castepu16_epi64 

//__m256i_u8 _mm256_castepu16_epu8 (__m256i_u16 a)
#define _mm256_castepu16_epu8 

//__m256i_u32 _mm256_castepu16_epu32 (__m256i_u16 a)
#define _mm256_castepu16_epu32 

//__m256i_u64 _mm256_castepu16_epu64 (__m256i_u16 a)
#define _mm256_castepu16_epu64 

//__m256 _mm256_castepu16_ps (__m256i_u16 a)
#define _mm256_castepu16_ps _mm256_castsi256_ps

//__m256d _mm256_castepu16_pd (__m256i_u16 a)
#define _mm256_castepu16_pd _mm256_castsi256_pd
/*******************************************************************************/

/*******************************************************************************/
//__m256i_i8 _mm256_castepu32_epi8 (__m256i_u32 a)
#define _mm256_castepu32_epi8 

//__m256i_i16 _mm256_castepu32_epi16 (__m256i_u32 a)
#define _mm256_castepu32_epi16 

//__m256i_i32 _mm256_castepu32_epi32 (__m256i_u32 a)
#define _mm256_castepu32_epi32 

//__m256i_i64 _mm256_castepu32_epi64 (__m256i_u32 a)
#define _mm256_castepu32_epi64 

//__m256i_u8 _mm256_castepu32_epu8 (__m256i_u32 a)
#define _mm256_castepu32_epu8 

//__m256i_u16 _mm256_castepu32_epu16 (__m256i_u32 a)
#define _mm256_castepu32_epu16 

//__m256i_u64 _mm256_castepu32_epu64 (__m256i_u32 a)
#define _mm256_castepu32_epu64 

//__m256 _mm256_castepu32_ps(__m256i_u32 a)
#define _mm256_castepu32_ps _mm256_castsi256_ps

//__m256d _mm256_castepu32_pd(__m256i_u32 a)
#define _mm256_castepu32_pd _mm256_castsi256_pd
/*******************************************************************************/

/*******************************************************************************/
//__m256i_i8 _mm256_castepu64_epi8 (__m256i_u64 a)
#define _mm256_castepu64_epi8 

//__m256i_i16 _mm256_castepu64_epi16 (__m256i_u64 a)
#define _mm256_castepu64_epi16 

//__m256i_i32 _mm256_castepu64_epi32 (__m256i_u64 a)
#define _mm256_castepu64_epi32 

//__m256i_i64 _mm256_castepu64_epi64 (__m256i_u64 a)
#define _mm256_castepu64_epi64 

//__m256i_u8 _mm256_castepu64_epu8 (__m256i_u64 a)
#define _mm256_castepu64_epu8 

//__m256i_u16 _mm256_castepu64_epu16 (__m256i_u64 a)
#define _mm256_castepu64_epu16 

//__m256i_u32 _mm256_castepu64_epu32 (__m256i_u64 a)
#define _mm256_castepu64_epu32 

//__m256 _mm256_castepu64_ps(__m256i_u64 a)
#define _mm256_castepu64_ps _mm256_castsi256_ps

//__m256d _mm256_castepu64_pd(__m256i_u64 a)
#define _mm256_castepu64_pd _mm256_castsi256_pd
/*******************************************************************************/

/*******************************************************************************/
//__m256i_i8 _mm256_castepi64_epi8(__m256i_i64 a)
#define _mm256_castepi64_epi8 

//__m256i_i16 _mm256_castepi64_epi16(__m256i_i64 a)
#define _mm256_castepi64_epi16 

//__m256i_i32 _mm256_castepi64_epi32(__m256i_i64 a)
#define _mm256_castepi64_epi32 

//__m256i_u8 _mm256_castepi64_epu8(__m256i_i64 a)
#define _mm256_castepi64_epu8 

//__m256i_u16 _mm256_castepi64_epu16(__m256i_i64 a)
#define _mm256_castepi64_epu16 

//__m256i_u32 _mm256_castepi64_epu32(__m256i_i64 a)
#define _mm256_castepi64_epu32 

//__m256i_u64 _mm256_castepi64_epu64(__m256i_i64 a)
#define _mm256_castepi64_epu64 

//__m256 _mm256_castepi64_ps(__m256i_i64 a)
#define _mm256_castepi64_ps _mm256_castsi256_ps

//__m256d _mm256_castepi64_pd(__m256i_i64 a)
#define _mm256_castepi64_pd _mm256_castsi256_pd
/*******************************************************************************/

/*******************************************************************************/
//__m256i_i8 _mm256_castpd_epi8(__m256d a)
#define _mm256_castpd_epi8 _mm256_castpd_si256

//__m256i_i16 _mm256_castpd_epi16(__m256d a)
#define _mm256_castpd_epi16 _mm256_castpd_si256

//__m256i_i32 _mm256_castpd_epi32(__m256d a)
#define _mm256_castpd_epi32 _mm256_castpd_si256

//__m256i_i64 _mm256_castpd_epi64(__m256d a)
#define _mm256_castpd_epi64 _mm256_castpd_si256

//__m256i_u8 _mm256_castpd_epu8(__m256d a)
#define _mm256_castpd_epu8 _mm256_castpd_si256

//__m256i_u16 _mm256_castpd_epu16(__m256d a)
#define _mm256_castpd_epu16 _mm256_castpd_si256

//__m256i_u32 _mm256_castpd_epu32(__m256d a)
#define _mm256_castpd_epu32 _mm256_castpd_si256

//__m256i_u64 _mm256_castpd_epu64(__m256d a)
#define _mm256_castpd_epu64 _mm256_castpd_si256
/*******************************************************************************/
#endif
