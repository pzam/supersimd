/***************************************************************************************************/
// HEADER_NAME /extra/avx2_bitreverseextra.h
/***************************************************************************************************/

#if __AVX2__
__forceinline __m256i_u8 _mm256_bitreverse_epu8(const __m256i_u8 a)
{
	__m256i_u8 ret = a;

	ret = _mm256_or_epu8((_mm256_srli_epu8(_mm256_and_epu8(ret, __m256i_u8_0xf0), 4)), (_mm256_slli_epu8(_mm256_and_epu8(ret, __m256i_u8_0x0f), 4)));
	ret = _mm256_or_epu8((_mm256_srli_epu8(_mm256_and_epu8(ret, __m256i_u8_0xcc), 2)), (_mm256_slli_epu8(_mm256_and_epu8(ret, __m256i_u8_0x33), 2)));
	ret = _mm256_or_epu8((_mm256_srli_epu8(_mm256_and_epu8(ret, __m256i_u8_0xaa), 1)), (_mm256_slli_epu8(_mm256_and_epu8(ret, __m256i_u8_0x55), 1)));

	return ret;
}
__forceinline __m256i_i8 _mm256_bitreverse_epi8(const __m256i_i8 a)
{
	return _mm256_castepu8_epi8(_mm256_bitreverse_epu8(_mm256_castepi8_epu8(a)));
}

__forceinline __m256i_u16 _mm256_bitreverse_epu16(__m256i_u16 a)
{
	return _mm256_bswap_epu16(_mm256_castepu8_epu16(_mm256_bitreverse_epu8(_mm256_castepu16_epu8(a))));
}

__forceinline __m256i_i16 _mm256_bitreverse_epi16(const __m256i_i16 a)
{
	return _mm256_castepu16_epi16(_mm256_bitreverse_epu16(_mm256_castepi16_epu16(a)));
}

__forceinline __m256i_u32 _mm256_bitreverse_epu32(__m256i_u32 a)
{
	return _mm256_bswap_epu32(_mm256_castepu8_epu32(_mm256_bitreverse_epu8(_mm256_castepu32_epu8(a))));
}

__forceinline __m256i_i32 _mm256_bitreverse_epi32(const __m256i_i32 a)
{
	return _mm256_castepu32_epi32(_mm256_bitreverse_epu32(_mm256_castepi32_epu32(a)));
}

__forceinline __m256i_u64 _mm256_bitreverse_epu64(__m256i_u64 a)
{
	return _mm256_bswap_epu64(_mm256_castepu8_epu64(_mm256_bitreverse_epu8(_mm256_castepu64_epu8(a))));
}

__forceinline __m256i_i64 _mm256_bitreverse_epi64(const __m256i_i64 a)
{
	return _mm256_castepu64_epi64(_mm256_bitreverse_epu64(_mm256_castepi64_epu64(a)));
}
#endif