/***************************************************************************************************/
// HEADER_NAME /extra/minmaxextra.h
/***************************************************************************************************/

#if BIT64
#if !__AVX512VL__ && !__AVX512F__
/***************************************************************************************************/
// Min
/***************************************************************************************************/
#undef _mm_min_epi64
#define _mm_min_epi64 _mm_min_epi64_impl
__forceinline __m128i_i64 _mm_min_epi64_impl(__m128i_i64 a, __m128i_i64 b) 
{
	return _mm_castpd_epu64(_mm_blendv_pd(_mm_castepu64_pd(a), _mm_castepu64_pd(b), _mm_castepu64_pd(_mm_cmpgt_epi64
	(
		_mm_castepu64_epi64(a),
		_mm_castepu64_epi64(b)
	))));
}

#undef _mm_min_epu64
#define _mm_min_epu64 _mm_min_epu64_impl
__forceinline __m128i_u64 _mm_min_epu64_impl(__m128i_u64 a, __m128i_u64 b) 
{
	return _mm_castpd_epu64(_mm_blendv_pd(_mm_castepu64_pd(a), _mm_castepu64_pd(b), _mm_castepu64_pd(_mm_cmpgt_epi64
	(
		_mm_add_epi64(_mm_castepu64_epi64(a), _mm_set1_epi64x(9223372036854775807)),
		_mm_add_epi64(_mm_castepu64_epi64(b), _mm_set1_epi64x(9223372036854775807))
	))));
}



/***************************************************************************************************/
// Max
/***************************************************************************************************/
#undef _mm_max_epi64
#define _mm_max_epi64 _mm_max_epi64_impl
__forceinline __m128i_i64 _mm_max_epi64_impl(__m128i_i64 a, __m128i_i64 b) 
{
	return _mm_castpd_epu64(_mm_blendv_pd(_mm_castepu64_pd(b), _mm_castepu64_pd(a), _mm_castepu64_pd(_mm_cmpgt_epi64
	(
		_mm_castepu64_epi64(a),
		_mm_castepu64_epi64(b)
	))));
}
#undef _mm_max_epu64
#define _mm_max_epu64 _mm_max_epu64_impl
__forceinline __m128i_u64 _mm_max_epu64_impl(__m128i_u64 a, __m128i_u64 b) 
{
	return _mm_castpd_epu64(_mm_blendv_pd (_mm_castepu64_pd(b), _mm_castepu64_pd(a), _mm_castepu64_pd(_mm_cmpgt_epi64
	(
		_mm_add_epi64(_mm_castepu64_epi64(a), _mm_set1_epi64x(9223372036854775807)), 
		_mm_add_epi64(_mm_castepu64_epi64(b), _mm_set1_epi64x(9223372036854775807))
	))));
}
#endif
#endif

