/***************************************************************************************************/
// HEADER_NAME /extra/avx_streamextra.h
/***************************************************************************************************/

//AVX
#if __AVX__
#define _mm256_stream_epu8 _mm256_stream_si256
#define _mm256_stream_epi8 _mm256_stream_si256
#define _mm256_stream_epu16 _mm256_stream_si256
#define _mm256_stream_epi16 _mm256_stream_si256
#define _mm256_stream_epu32 _mm256_stream_si256
#define _mm256_stream_epi32 _mm256_stream_si256
#define _mm256_stream_epu64 _mm256_stream_si256
#define _mm256_stream_epi64 _mm256_stream_si256

/***************************************************************************************************/
// Portable Stream Integer
/***************************************************************************************************/
__forceinline void _mm256_stream_su8 (__uint8* mem_addr, __uint8 a)
{
	*mem_addr = a;
}
__forceinline void _mm256_stream_si8 (__int8* mem_addr, __int8 a)
{
	*mem_addr = a;
}
__forceinline void _mm256_stream_su16 (__uint16* mem_addr, __uint16 a)
{
	*mem_addr = a;
}
__forceinline void _mm256_stream_si16 (__int16* mem_addr, __uint16 a)
{
	*mem_addr = a;
}

__forceinline void _mm256_stream_su32 (__uint32* mem_addr, __uint32 a)
{
	*mem_addr = a;
}

__forceinline void _mm256_stream_si32 (__int32* mem_addr, __int32 a)
{
	*mem_addr = a;
}

__forceinline void _mm256_stream_si64(__int64* mem_addr, __int64 a)
{
	*mem_addr = a;
}

__forceinline void _mm256_stream_su64 (__uint64* mem_addr, __uint64 a)
{
	*mem_addr = a;
}
#endif

