/***************************************************************************************************/
// HEADER_NAME /extra/avx2_minmaxextra.h
/***************************************************************************************************/
#if __AVX2__

#if BIT64
#if !__AVX512VL__ && !__AVX512F__
/***************************************************************************************************/
// Min
/***************************************************************************************************/
#define _mm256_min_epi64 _mm256_min_epi64_impl
__forceinline __m256i_i64 _mm256_min_epi64_impl(__m256i_i64 a, __m256i_i64 b) 
{
	return _mm256_castpd_epu64(_mm256_blendv_pd(_mm256_castepu64_pd(a), _mm256_castepu64_pd(b), _mm256_castepu64_pd(_mm256_cmpgt_epi64
	(
		_mm256_castepu64_epi64(a),
		_mm256_castepu64_epi64(b)
	))));
}
#define _mm256_min_epu64 _mm256_min_epu64_impl
__forceinline __m256i_u64 _mm256_min_epu64_impl(__m256i_u64 a, __m256i_u64 b) 
{
	return _mm256_castpd_epu64(_mm256_blendv_pd(_mm256_castepu64_pd(a), _mm256_castepu64_pd(b), _mm256_castepu64_pd(_mm256_cmpgt_epi64
	(
		_mm256_add_epi64(_mm256_castepu64_epi64(a), _mm256_set1_epi64x(9223372036854775807)),
		_mm256_add_epi64(_mm256_castepu64_epi64(b), _mm256_set1_epi64x(9223372036854775807))
	))));
}



/***************************************************************************************************/
// Max
/***************************************************************************************************/
#define _mm256_max_epi64 _mm256_max_epi64_impl
__forceinline __m256i_i64 _mm256_max_epi64_impl(__m256i_i64 a, __m256i_i64 b) 
{
	return _mm256_castpd_epu64(_mm256_blendv_pd(_mm256_castepu64_pd(b), _mm256_castepu64_pd(a), _mm256_castepu64_pd(_mm256_cmpgt_epi64
	(
		_mm256_castepu64_epi64(a),
		_mm256_castepu64_epi64(b)
	))));
}
#define _mm256_max_epu64 _mm256_max_epu64_impl
__forceinline __m256i_u64 _mm256_max_epu64_impl(__m256i_u64 a, __m256i_u64 b) 
{
	return _mm256_castpd_epu64(_mm256_blendv_pd (_mm256_castepu64_pd(b), _mm256_castepu64_pd(a), _mm256_castepu64_pd(_mm256_cmpgt_epi64
	(
		_mm256_add_epi64(_mm256_castepu64_epi64(a), _mm256_set1_epi64x(9223372036854775807)), 
		_mm256_add_epi64(_mm256_castepu64_epi64(b), _mm256_set1_epi64x(9223372036854775807))
	))));
}
#endif
#endif

#endif

