/***************************************************************************************************/
// HEADER_NAME /extra/avx512_setextra.h
/***************************************************************************************************/
#if __AVX512F__
/***************************************************************************************************/
// SET
/***************************************************************************************************/
#define _mm512_set_epi64x _mm512_set_epi64
/***************************************************************************************************/
// SET REVERSED
/***************************************************************************************************/

#define _mm512_setr_epi64x _mm512_setr_epi64
/***************************************************************************************************/
// SET UNSIGNED INTERGERS
/***************************************************************************************************/

//AVX-512
#define _mm512_set_epu8 _mm512_set_epi8
#define _mm512_set_epu16 _mm512_set_epi16
#define _mm512_set_epu32 _mm512_set_epi32
#define _mm512_set_epu64x _mm512_set_epi64x

#define _mm512_setr_epu8 _mm512_setr_epi8
#define _mm512_setr_epu16 _mm512_setr_epi16
#define _mm512_setr_epu32 _mm512_setr_epi32
#define _mm512_setr_epu64x _mm512_setr_epi64x

#define _mm512_set1_epu8 _mm512_set1_epi8
#define _mm512_set1_epu16 _mm512_set1_epi16
#define _mm512_set1_epu32 _mm512_set1_epi32
#define _mm512_set1_epu64x _mm512_set1_epi64x

/***************************************************************************************************/
// SET HEXADECIMAL VALUE
/***************************************************************************************************/

#define _mm512_set_hex_epu8 _mm512_set_epu8
#define _mm512_set_hex_epi8 _mm512_set_epi8
#define _mm512_setr_hex_epu8 _mm512_setr_epu8
#define _mm512_setr_hex_epi8 _mm512_setr_epi8
#define _mm512_set1_hex_epu8 _mm512_set1_epu8
#define _mm512_set1_hex_epi8 _mm512_set1_epi8

#define _mm512_set_hex_epu16 _mm512_set_epu16
#define _mm512_set_hex_epi16 _mm512_set_epi16
#define _mm512_setr_hex_epu16 _mm512_setr_epu16
#define _mm512_setr_hex_epi16 _mm512_setr_epi16
#define _mm512_set1_hex_epu16 _mm512_set1_epu16
#define _mm512_set1_hex_epi16 _mm512_set1_epi16

#define _mm512_set_hex_epu32 _mm512_set_epu32
#define _mm512_set_hex_epi32 _mm512_set_epi32
#define _mm512_setr_hex_epu32 _mm512_setr_epu32
#define _mm512_setr_hex_epi32 _mm512_setr_epi32
#define _mm512_set1_hex_epu32 _mm512_set1_epu32
#define _mm512_set1_hex_epi32 _mm512_set1_epi32

#define _mm512_set_hex_epu64 _mm512_set_epu64
#define _mm512_set_hex_epi64 _mm512_set_epi64
#define _mm512_setr_hex_epu64 _mm512_setr_epu64
#define _mm512_setr_hex_epi64 _mm512_setr_epi64
#define _mm512_set1_hex_epu64 _mm512_set1_epu64
#define _mm512_set1_hex_epi64 _mm512_set1_epi64

//AVX-512
__forceinline __m512 _mm512_set_hex_ps
(
int e15, int e14, int e13, int e12, 
int e11, int e10, int e9, int e8, 
int e7, int e6, int e5, int e4, 
int e3, int e2, int e1, int e0
)
{
	return _mm512_castepi32_ps(_mm512_set_epi32(e15, e14, e13, e12, e11, e10, e9, e8, e7, e6, e5, e4, e3, e2, e1, e0));
}
__forceinline __m512 _mm512_setr_hex_ps
(
int e15, int e14, int e13, int e12, 
int e11, int e10, int e9, int e8, 
int e7, int e6, int e5, int e4, 
int e3, int e2, int e1, int e0
)
{
	return _mm512_castepi32_ps(_mm512_setr_epi32(e15, e14, e13, e12, e11, e10, e9, e8, e7, e6, e5, e4, e3, e2, e1, e0));
}

__forceinline __m512 _mm512_set1_hex_ps(int a)
{
	return _mm512_castepi32_ps(_mm512_set1_epi32(a));
}

__forceinline __m512d _mm512_set_hex_pd
(
__int64 e7, __int64 e6, __int64 e5, __int64 e4, 
__int64 e3, __int64 e2, __int64 e1, __int64 e0
)
{
	return _mm512_castepi64_pd(_mm512_set_epi64(e7, e6, e5, e4, e3, e2, e1, e0));
}
__forceinline __m512d _mm512_setr_hex_pd
(
__int64 e7, __int64 e6, __int64 e5, __int64 e4, 
__int64 e3, __int64 e2, __int64 e1, __int64 e0
)
{
	return _mm512_castepi64_pd(_mm512_setr_epi64(e7, e6, e5, e4, e3, e2, e1, e0));
}
__forceinline __m512d _mm512_set1_hex_pd(__int64 a)
{
	return _mm512_castepi64_pd(_mm512_set1_epi64 (a));
}

/***************************************************************************************************/
// SET-ZERO
/***************************************************************************************************/

//AVX-512
__forceinline __m512i_u8 _mm512_setzero_epu8(void)
{
	return _mm512_setzero_si512();
}
__forceinline __m512i_i8 _mm512_setzero_epi8(void)
{
	return _mm512_setzero_si512();
}
__forceinline __m512i_u16 _mm512_setzero_epu16(void)
{
	return _mm512_setzero_si512();
}
__forceinline __m512i_i16 _mm512_setzero_epi16(void)
{
	return _mm512_setzero_si512();
}
__forceinline __m512i_u32 _mm512_setzero_epu32(void)
{
	return _mm512_setzero_si512();
}
__forceinline __m512i_i32 _mm512_setzero_epi32(void)
{
	return _mm512_setzero_si512();
}
__forceinline __m512i_u64 _mm512_setzero_epu64(void)
{
	return _mm512_setzero_si512();
}
__forceinline __m512i_i64 _mm512_setzero_epi64(void)
{
	return _mm512_setzero_si512();
}


/***************************************************************************************************/
// SET-UNDEFINED
/***************************************************************************************************/

//AVX-512
#define _mm512_undefined_epu8 _mm512_undefined_si512
#define _mm512_undefined_epi8 _mm512_undefined_si512
#define _mm512_undefined_epu16 _mm512_undefined_si512
#define _mm512_undefined_epi16 _mm512_undefined_si512
#define _mm512_undefined_epu32 _mm512_undefined_si512
#define _mm512_undefined_epi32 _mm512_undefined_si512
#define _mm512_undefined_epu64 _mm512_undefined_si512
#define _mm512_undefined_epi64 _mm512_undefined_si512
#endif
