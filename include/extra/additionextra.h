/***************************************************************************************************/
// HEADER_NAME /extra/additionextra.h
/***************************************************************************************************/


/***************************************************************************************************/
// ADDITION
/***************************************************************************************************/
#if !ARM
//ADD
__forceinline __m128i_u8 _mm_add_epu8(__m128i_u8 a, __m128i_u8 b)
{
	__m128i_i8 ret;
	__m128i_i8 alt = _mm_cmplt_epi8(a, _mm_setzero_epi8());
	__m128i_i8 blt = _mm_cmplt_epi8(b, _mm_setzero_epi8());

	ret = _mm_add_epi8(a, b);
	ret = _mm_blendv_epi8(ret, _mm_sub_epi8(b, a), alt);
	ret = _mm_blendv_epi8(ret, _mm_sub_epi8(a, b), blt);

	return ret;
}

__forceinline __m128i_u16 _mm_add_epu16(__m128i_u16 a, __m128i_u16 b)
{
	__m128i_i16 ret;
	__m128i_i16 alt = _mm_cmplt_epi16(a, _mm_setzero_epi16());
	__m128i_i16 blt = _mm_cmplt_epi16(b, _mm_setzero_epi16());

	ret = _mm_add_epi16(a, b);
	ret = _mm_blendv_epi16(ret, _mm_sub_epi16(b, a), alt);
	ret = _mm_blendv_epi16(ret, _mm_sub_epi16(a, b), blt);

	return ret;
}

__forceinline __m128i_u32 _mm_add_epu32(__m128i_u32 a, __m128i_u32 b)
{
	__m128i_i32 ret;
	__m128i_i32 alt = _mm_cmplt_epi32(a, _mm_setzero_epi32());
	__m128i_i32 blt = _mm_cmplt_epi32(b, _mm_setzero_epi32());

	ret = _mm_add_epi32(a, b);
	ret = _mm_blendv_epi32(ret, _mm_sub_epi32(b, a), alt);
	ret = _mm_blendv_epi32(ret, _mm_sub_epi32(a, b), blt);

	return ret;
}
#if BIT64
__forceinline __m128i_u64 _mm_add_epu64(__m128i_u64 a, __m128i_u64 b)
{
	__m128i_i64 ret;
	__m128i_i64 alt = _mm_cmplt_epi64(a,_mm_setzero_epi64());
	__m128i_i64 blt = _mm_cmplt_epi64(b,_mm_setzero_epi64());

	ret = _mm_add_epi64(a, b);
	ret = _mm_blendv_epi64(ret, _mm_sub_epi64(b, a), alt);
	ret = _mm_blendv_epi64(ret, _mm_sub_epi64(a, b), blt);

	return ret;
}
#endif
#endif
