/***************************************************************************************************/
// HEADER_NAME /extra/absextra.h
/***************************************************************************************************/

/**********************************************************************/
// ABS
/**********************************************************************/
#if !ARM
__forceinline __m128 _mm_abs_ps(const __m128 a) {
	return _mm_andnot_ps(_mm_set1_ps(-0.0f), a);
}
__forceinline __m128d _mm_abs_pd(const __m128d a) {
	return _mm_andnot_pd(_mm_set1_pd(-0.0), a);
}
//NOTE
//SSSE3 implemented
//__m128i_i8 _mm_abs_epi8 (__m128i_i8 a) 
//__m128i_i16 _mm_abs_epi16 (__m128i_i16 a)
//__m128i_i32 _mm_abs_epi32 (__m128i_i32 a)

//AVX-512 implemented
//__m128i_i64 _mm_abs_epi64 (__m128i_i64 a)

#if !__AVX512VL__ && !__AVX512F__
#define _mm_abs_epi64 _mm_abs_epi64_impl
__forceinline __m128i_i64 _mm_abs_epi64_impl (__m128i_i64 a){
	return _mm_cvtpd_epi64(_mm_abs_pd(_mm_cvtepi64_pd(a)));
}
#endif

#endif

/**********************************************************************/
// ABSDIFF
/**********************************************************************/

// Calculate absolute difference: abs(x - y):
#if !ARM
__forceinline __m128i_u8 _mm_absdiff_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	return _mm_subs_epu8(a, b);
}
__forceinline __m128i_i8 _mm_absdiff_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return _mm_abs_epi8(_mm_sub_epi8(a, b));
}
__forceinline __m128i_u16 _mm_absdiff_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	return _mm_subs_epu16(a, b);
}
__forceinline __m128i_i16 _mm_absdiff_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return _mm_abs_epi16(_mm_sub_epi16(a, b));
}
__forceinline __m128i_u32 _mm_absdiff_epu32 (__m128i_u32 a, __m128i_u32 b)
{
	return _mm_sub_epi32(a, b);
}
__forceinline __m128i_i32 _mm_absdiff_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return _mm_abs_epi32(_mm_sub_epi32(a, b));
}
__forceinline __m128i_u64 _mm_absdiff_epu64 (__m128i_u64 a, __m128i_u64 b)
{
	return _mm_sub_epi64(a, b);
}
#if !__AVX512VL__ && !__AVX512F__
__forceinline __m128i_i64 _mm_absdiff_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	return _mm_abs_epi64(_mm_sub_epi64(a, b));
}
#endif
#endif

/**********************************************************************/
#if !ARM
__forceinline __m128 _mm_absdiff_ps(__m128 a, __m128 b) {
	return _mm_abs_ps(_mm_sub_ps(a, b));
}
__forceinline __m128d _mm_absdiff_pd(__m128d a, __m128d b) {
	return _mm_abs_pd(_mm_sub_pd(a, b));
}
#endif

/**********************************************************************/