/***************************************************************************************************/
// HEADER_NAME /extra/movemaskextra.h
/***************************************************************************************************/

//int _mm_movemask_epi8 (__m128i_i8 a) // Implemented in SSE2


__forceinline int _mm_movemask_epu8 (__m128i_u8 a)
{
	return _mm_movemask_epi8(_mm_castepu8_epi8(a));
}
__forceinline int _mm_movemask_epi16 (__m128i_i16 a)
{
	return _mm_movemask_epi8(_mm_castepi16_epi8(a));
}
__forceinline int _mm_movemask_epu16 (__m128i_u16 a)
{
	return _mm_movemask_epi8(_mm_castepu16_epi8(a));
}
__forceinline int _mm_movemask_epi32 (__m128i_i32 a)
{
	return _mm_movemask_epi8(_mm_castepi32_epi8(a));
}
__forceinline int _mm_movemask_epu32 (__m128i_u32 a)
{
	return _mm_movemask_epi8(_mm_castepu32_epi8(a));
}
__forceinline int _mm_movemask_epi64 (__m128i_i64 a)
{
	return _mm_movemask_epi8(_mm_castepi64_epi8(a));
}
__forceinline int _mm_movemask_epu64 (__m128i_u64 a)
{
	return _mm_movemask_epi8(_mm_castepu64_epi8(a));
}

// int _mm_movemask_ps (__m128 a) //Implemented in SSE
// int _mm_movemask_pd (__m128d a) //Implemented in SSE2
