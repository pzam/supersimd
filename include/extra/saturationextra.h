/***************************************************************************************************/
// HEADER_NAME /extra/saturationextra.h
/***************************************************************************************************/

/***************************************************************************************************/
// SATURATION ADD
/***************************************************************************************************/

//__m64i_u8 _mm_adds_pu8(__m64i_u8 a, __m64i_u8 b) IMPLEMENTED IN MMX
//__m64i_i8 _mm_adds_pi8(__m64i_i8 a, __m64i_i8 b) IMPLEMENTED IN MMX
//__m64i_u16 _mm_adds_pu16(__m64i_u16 a, __m64i_u16 b) IMPLEMENTED IN MMX
//__m64i_i16 _mm_adds_pi16(__m64i_i16 a, __m64i_i16 b) IMPLEMENTED IN MMX

//__m128i_u8 _mm_adds_epu8(__m128i_u8 a, __m128i_u8 b) IMPLEMENTED IN SSE2
//__m128i_i8 _mm_adds_epi8(__m128i_i8 a, __m128i_i8 b) IMPLEMENTED IN SSE2
//__m128i_u16 _mm_adds_epu16(__m128i_u16 a, __m128i_u16 b) IMPLEMENTED IN SSE2
//__m128i_i16 _mm_adds_epi16(__m128i_i16 a, __m128i_i16 b) IMPLEMENTED IN SSE2

__forceinline __m128i_u32 _mm_adds_epu32(__m128i_u32 a, __m128i_u32 b)
{
	__m128i_u32 sum;
	__m128i_u32 cmp;
	sum = _mm_add_epu32(a, b);
	cmp = _mm_cmplt_epu32(sum, a);
	sum = _mm_or_epu32(sum, cmp);
	return sum;
}

__forceinline __m128i_i32 _mm_adds_epi32(__m128i_i32 a, __m128i_i32 b)
{
	__m128i_i32 bool_a_is_negative;
	__m128i_i32 bool_b_is_negative;
	__m128i_i32 bool_sum_is_negative;
	__m128i_i32 sum;

	bool_a_is_negative = _mm_cmpneq_epi32(_mm_and_epi32(a, __m128i_i32_max), _mm_setzero_epi32());
	bool_b_is_negative = _mm_cmpneq_epi32(_mm_and_epi32(b, __m128i_i32_min), _mm_setzero_epi32());

	sum = _mm_add_epi32(a, b);
	bool_sum_is_negative = _mm_cmpneq_epi32(_mm_and_epi32(sum, __m128i_i32_min), _mm_setzero_epi32());

	sum = _mm_blendv_epi32(sum, _mm_setzero_epi32(), _mm_cmpneq_epi32(bool_a_is_negative, bool_b_is_negative));
	sum = _mm_blendv_epi32(sum, __m128i_i32_max, _mm_cmpeq_epi32(bool_a_is_negative, _mm_not_epi32(bool_sum_is_negative)));
	sum = _mm_blendv_epi32(sum, __m128i_i32_min, _mm_cmpeq_epi32(_mm_not_epi32(bool_a_is_negative) , bool_sum_is_negative));

	return sum;
}

#if BIT64
__forceinline __m128i_i64 _mm_adds_epi64(__m128i_i64 a, __m128i_i64 b)
{
	__m128i_i64 bool_a_is_negative;
	__m128i_i64 bool_b_is_negative;
	__m128i_i64 bool_sum_is_negative;
	__m128i_i64 sum;

	bool_a_is_negative = _mm_cmpneq_epi64(_mm_and_epi64(a, __m128i_i64_max), _mm_setzero_epi64());
	bool_b_is_negative = _mm_cmpneq_epi64(_mm_and_epi64(b, __m128i_i64_min), _mm_setzero_epi64());

	sum = _mm_add_epi64(a, b);
	bool_sum_is_negative = _mm_cmpneq_epi64(_mm_and_epi64(sum, __m128i_i64_min), _mm_setzero_epi64());

	sum = _mm_blendv_epi64(sum, _mm_setzero_epi64(), _mm_cmpneq_epi64(bool_a_is_negative, bool_b_is_negative));
	sum = _mm_blendv_epi64(sum, __m128i_i64_max, _mm_cmpeq_epi64(bool_a_is_negative, _mm_not_epi64(bool_sum_is_negative)));
	sum = _mm_blendv_epi64(sum, __m128i_i64_min, _mm_cmpeq_epi64(_mm_not_epi64(bool_a_is_negative), bool_sum_is_negative));

	return sum;
}

__forceinline __m128i_u64 _mm_adds_epu64(__m128i_u64 a, __m128i_u64 b)
{
	__m128i_u64 sum;
	__m128i_u64 cmp;
	sum = _mm_add_epu64(a, b);
	cmp = _mm_cmplt_epu64(sum, a);
	sum = _mm_or_epu64(sum, cmp);
	return sum;
}
#endif
/***************************************************************************************************/
// SATURATION SUBTRACT
/***************************************************************************************************/
//__m64i_u8 _mm_subs_pu8(__m64i_u8 a, __m64i_u8 b) IMPLEMENTED IN MMX
//__m64i_i8 _mm_subs_pi8(__m64i_i8 a, __m64i_i8 b) IMPLEMENTED IN MMX
//__m64i_u16 _mm_subs_pu16(__m64i_u16 a, __m64i_u16 b) IMPLEMENTED IN MMX
//__m64i_i16 _mm_subs_pi16(__m64i_i16 a, __m64i_i16 b) IMPLEMENTED IN MMX

//__m128i_u8 _mm_subs_epu8(__m128i_u8 a, __m128i_u8 b) IMPLEMENTED IN SSE2
//__m128i_i8 _mm_subs_epi8(__m128i_i8 a, __m128i_i8 b) IMPLEMENTED IN SSE2
//__m128i_u16 _mm_subs_epu16(__m128i_u16 a, __m128i_u16 b) IMPLEMENTED IN SSE2
//__m128i_i16 _mm_subs_epi16(__m128i_i16 a, __m128i_i16 b) IMPLEMENTED IN SSE2

__forceinline __m128i_i32 _mm_subs_epu32(__m128i_u32 a, __m128i_u32 b)
{
	__m128i_u32 sum;
	__m128i_u32 cmp;
	sum = _mm_sub_epu32(a, b);
	cmp = _mm_cmple_epu32(sum, a);
	sum = _mm_and_epu32(sum, cmp);
	return sum;
}
__forceinline __m128i_i32 _mm_subs_epi32(__m128i_i32 a, __m128i_i32 b)
{
	__m128i_i32 bool_a_is_negative;
	__m128i_i32 bool_b_is_negative;
	__m128i_i32 bool_sum_is_negative;
	__m128i_i32 sum;

	bool_a_is_negative = _mm_cmpneq_epi32(_mm_and_epi32(a, __m128i_i32_max), _mm_setzero_epi32());
	bool_b_is_negative = _mm_cmpneq_epi32(_mm_and_epi32(b, __m128i_i32_min), _mm_setzero_epi32());

	sum = _mm_sub_epi32(a, b);
	bool_sum_is_negative = _mm_cmpneq_epi32(_mm_and_epi32(sum, __m128i_i32_min), _mm_setzero_epi32());

	sum = _mm_blendv_epi32(sum, _mm_setzero_epi32(), _mm_cmpneq_epi32(bool_a_is_negative, bool_b_is_negative));
	sum = _mm_blendv_epi32(sum, __m128i_i32_max, _mm_cmpeq_epi32(bool_a_is_negative, _mm_not_epi32(bool_sum_is_negative)));
	sum = _mm_blendv_epi32(sum, __m128i_i32_min, _mm_cmpeq_epi32(_mm_not_epi32(bool_a_is_negative), bool_sum_is_negative));

	return sum;
}

#if BIT64
__forceinline __m128i_i64 _mm_subs_epu64(__m128i_u64 a, __m128i_u64 b)
{
	__m128i_u64 sum;
	__m128i_u64 cmp;
	sum = _mm_sub_epu64(a, b);
	cmp = _mm_cmple_epu64(sum, a);
	sum = _mm_and_epu64(sum, cmp);
	return sum;
}

__forceinline __m128i_i64 _mm_subs_epi64(__m128i_i64 a, __m128i_i64 b)
{
	__m128i_i64 bool_a_is_negative;
	__m128i_i64 bool_b_is_negative;
	__m128i_i64 bool_sum_is_negative;
	__m128i_i64 sum;

	bool_a_is_negative = _mm_cmpneq_epi64(_mm_and_epi64(a, __m128i_i64_max), _mm_setzero_epi64());
	bool_b_is_negative = _mm_cmpneq_epi64(_mm_and_epi64(b, __m128i_i64_min), _mm_setzero_epi64());

	sum = _mm_sub_epi64(a, b);
	bool_sum_is_negative = _mm_cmpneq_epi64(_mm_and_epi64(sum, __m128i_i64_min), _mm_setzero_epi64());

	sum = _mm_blendv_epi64(sum, _mm_setzero_epi64(), _mm_cmpneq_epi64(bool_a_is_negative, bool_b_is_negative));
	sum = _mm_blendv_epi64(sum, __m128i_i64_max, _mm_cmpeq_epi64(bool_a_is_negative, _mm_not_epi64(bool_sum_is_negative)));
	sum = _mm_blendv_epi64(sum, __m128i_i64_min, _mm_cmpeq_epi64(_mm_not_epi64(bool_a_is_negative), bool_sum_is_negative));

	return sum;
}
#endif
