/***************************************************************************************************/
// HEADER_NAME /extra/bitreverseextra.h
/***************************************************************************************************/

#if !ARM64
__forceinline __m128i_u8 _mm_bitreverse_epu8(const __m128i_u8 a)
{
	__m128i_u8 ret = a;

	ret = _mm_or_epu8((_mm_srli_epu8(_mm_and_epu8(ret, __m128i_u8_0xf0), 4)), (_mm_slli_epu8(_mm_and_epu8(ret, __m128i_u8_0x0f), 4)));
	ret = _mm_or_epu8((_mm_srli_epu8(_mm_and_epu8(ret, __m128i_u8_0xcc), 2)), (_mm_slli_epu8(_mm_and_epu8(ret, __m128i_u8_0x33), 2)));
	ret = _mm_or_epu8((_mm_srli_epu8(_mm_and_epu8(ret, __m128i_u8_0xaa), 1)), (_mm_slli_epu8(_mm_and_epu8(ret, __m128i_u8_0x55), 1)));

	return ret;
}
#endif

__forceinline __m128i_i8 _mm_bitreverse_epi8(const __m128i_i8 a)
{
	return _mm_castepu8_epi8(_mm_bitreverse_epu8(_mm_castepi8_epu8(a)));
}

__forceinline __m128i_u16 _mm_bitreverse_epu16(__m128i_u16 a)
{
	return _mm_bswap_epu16(_mm_castepu8_epu16(_mm_bitreverse_epu8(_mm_castepu16_epu8(a))));
}

__forceinline __m128i_i16 _mm_bitreverse_epi16(const __m128i_i16 a)
{
	return _mm_castepu16_epi16(_mm_bitreverse_epu16(_mm_castepi16_epu16(a)));
}

__forceinline __m128i_u32 _mm_bitreverse_epu32(__m128i_u32 a)
{
	return _mm_bswap_epu32(_mm_castepu8_epu32(_mm_bitreverse_epu8(_mm_castepu32_epu8(a))));
}

__forceinline __m128i_i32 _mm_bitreverse_epi32(const __m128i_i32 a)
{
	return _mm_castepu32_epi32(_mm_bitreverse_epu32(_mm_castepi32_epu32(a)));
}
#if BIT64
__forceinline __m128i_u64 _mm_bitreverse_epu64(__m128i_u64 a)
{
	return _mm_bswap_epu64(_mm_castepu8_epu64(_mm_bitreverse_epu8(_mm_castepu64_epu8(a))));
}
#endif

#if BIT64
__forceinline __m128i_i64 _mm_bitreverse_epi64(const __m128i_i64 a)
{
	return _mm_castepu64_epi64(_mm_bitreverse_epu64(_mm_castepi64_epu64(a)));
}
#endif