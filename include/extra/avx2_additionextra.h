/***************************************************************************************************/
// HEADER_NAME /extra/avx2_additionextra.h
/***************************************************************************************************/


/***************************************************************************************************/
// ADDITION
/***************************************************************************************************/
#if __AVX2__

//ADD
__forceinline __m256i_u8 _mm256_add_epu8(__m256i_u8 a, __m256i_u8 b)
{
	__m256i_i8 ret;
	__m256i_i8 alt = _mm256_cmplt_epi8(a, _mm256_setzero_epi8());
	__m256i_i8 blt = _mm256_cmplt_epi8(b, _mm256_setzero_epi8());

	ret = _mm256_add_epi8(a, b);
	ret = _mm256_blendv_epi8(ret, _mm256_sub_epi8(b, a), alt);
	ret = _mm256_blendv_epi8(ret, _mm256_sub_epi8(a, b), blt);
	ret = _mm256_blendv_epi8(ret, _mm256_add_epi8(b, a), _mm256_and_epi8(alt, blt));

	return ret;
}

__forceinline __m256i_u16 _mm256_add_epu16(__m256i_u16 a, __m256i_u16 b)
{
	__m256i_i16 ret;
	__m256i_i16 alt = _mm256_cmplt_epi16(a, _mm256_setzero_epi16());
	__m256i_i16 blt = _mm256_cmplt_epi16(b, _mm256_setzero_epi16());

	ret = _mm256_add_epi16(a, b);
	ret = _mm256_blendv_epi16(ret, _mm256_sub_epi16(b, a), alt);
	ret = _mm256_blendv_epi16(ret, _mm256_sub_epi16(a, b), blt);
	ret = _mm256_blendv_epi16(ret, _mm256_add_epi16(b, a), _mm256_and_epi16(alt, blt));

	return ret;
}

__forceinline __m256i_u32 _mm256_add_epu32(__m256i_u32 a, __m256i_u32 b)
{
	__m256i_i32 ret;
	__m256i_i32 alt = _mm256_cmplt_epi32(a, _mm256_setzero_epi32());
	__m256i_i32 blt = _mm256_cmplt_epi32(b, _mm256_setzero_epi32());

	ret = _mm256_add_epi32(a, b);
	ret = _mm256_blendv_epi32(ret, _mm256_sub_epi32(b, a), alt);
	ret = _mm256_blendv_epi32(ret, _mm256_sub_epi32(a, b), blt);
	ret = _mm256_blendv_epi32(ret, _mm256_add_epi32(b, a), _mm256_and_epi32(alt,blt));

	return ret;
}

__forceinline __m256i_u64 _mm256_add_epu64(__m256i_u64 a, __m256i_u64 b)
{
	__m256i_i64 ret;
	__m256i_i64 alt = _mm256_cmplt_epi64(a,_mm256_setzero_epi64());
	__m256i_i64 blt = _mm256_cmplt_epi64(b,_mm256_setzero_epi64());

	ret = _mm256_add_epi64(a, b);
	ret = _mm256_blendv_epi64(ret, _mm256_sub_epi64(b, a), alt);
	ret = _mm256_blendv_epi64(ret, _mm256_sub_epi64(a, b), blt);
	ret = _mm256_blendv_epi64(ret, _mm256_add_epi64(b, a), _mm256_and_epi64(alt, blt));

	return ret;
}
#endif
