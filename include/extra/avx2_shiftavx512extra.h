/***************************************************************************************************/
// HEADER_NAME /extra/avx2_shiftavx512extra.h
/***************************************************************************************************/
#if __AVX2__
//NOT PART OF AVX-512
__forceinline __m256i_i8 _mm256_rol_epi8(__m256i_i8 a, int imm8)
{
	__m256i_i32 l, r;
	l = _mm256_sll_epi8(a, _mm256_castepi32_epi8(_mm_cvtsi32_epi32(imm8 & 7)));
	r = _mm256_srl_epi8(a, _mm256_castepi32_epi8(_mm_cvtsi32_epi32((8-imm8) & 7)));
	return _mm256_or_epi8(l,r);
}
//NOT PART OF AVX-512
__forceinline __m256i_i16 _mm256_rol_epi16(__m256i_i16 a, int imm8)
{
	__m256i_i32 l, r;
	l = _mm256_sll_epi16(a, _mm256_castepi32_epi16(_mm_cvtsi32_epi32((__int16)imm8 & 15)));
	r = _mm256_srl_epi16(a, _mm256_castepi32_epi16(_mm_cvtsi32_epi32((16-(__int16)imm8) & 15)));
	return _mm256_or_epi16(l,r);
}



#if !__AVX512VL__ && !__AVX512F__
#define _mm256_rol_epi32 _mm256_rol_epi32_impl
__forceinline __m256i_i32 _mm256_rol_epi32_impl(__m256i_i32 a, int imm8)
{
	__m256i_i32 l, r;
	l = _mm256_sll_epi32(a,_mm_cvtsi32_epi32(imm8 & 31));
	r = _mm256_srl_epi32(a,_mm_cvtsi32_epi32((32-imm8) & 31));
	return _mm256_or_epi32(l,r);
}

#if BIT64
#define _mm256_rol_epi64 _mm256_rol_epi64_impl
__forceinline __m256i_i64 _mm256_rol_epi64_impl(__m256i_i64 a, int imm8)
{
	__m256i_i64 l, r;
	l = _mm256_sll_epi64(a,_mm_cvtsi64_epi64(imm8 & 63));
	r = _mm256_srl_epi64(a,_mm_cvtsi64_epi64((64-imm8) & 63));
	return _mm256_or_epi64(l,r);
}
#endif

#endif


__forceinline __m256i_i8 _mm256_rolv_epi8(__m256i_i8 a, __m256i_i8 b)
{
	return _mm256_set_epi8
	(
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 31)), 31)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 30)), 30)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 29)), 29)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 28)), 28)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 27)), 27)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 26)), 26)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 25)), 25)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 24)), 24)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 23)), 23)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 22)), 22)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 21)), 21)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 20)), 20)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 19)), 19)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 18)), 18)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 17)), 17)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 16)), 16)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 15)), 15)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 14)), 14)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 13)), 13)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 12)), 12)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 11)), 11)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 10)), 10)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 9)), 9)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 8)), 8)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 7)), 7)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 6)), 6)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 5)), 5)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 4)), 4)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 3)), 3)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 2)), 2)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 1)), 1)),
		(_mm256_extract_epi8(_mm256_rol_epi8( a, _mm256_extract_epi8(b, 0)), 0))
	);
}

__forceinline __m256i_i16 _mm256_rolv_epi16(__m256i_i16 a, __m256i_i16 b)
{
	return _mm256_set_epi16
	(
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 15)), 15)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 14)), 14)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 13)), 13)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 12)), 12)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 11)), 11)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 10)), 10)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 9)), 9)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 8)), 8)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 7)), 7)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 6)), 6)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 5)), 5)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 4)), 4)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 3)), 3)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 2)), 2)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 1)), 1)),
		(_mm256_extract_epi16(_mm256_rol_epi16( a, _mm256_extract_epi16(b, 0)), 0))
	);
}
#if !__AVX512VL__ && !__AVX512F__
#define _mm256_rolv_epi32 _mm256_rolv_epi32_impl
__forceinline __m256i_i32 _mm256_rolv_epi32_impl(__m256i_i32 a, __m256i_i32 b)
{
	return _mm256_set_epi32
	(
		(_mm256_extract_epi32(_mm256_rol_epi32( a, _mm256_extract_epi32(b, 7)), 7)),
		(_mm256_extract_epi32(_mm256_rol_epi32( a, _mm256_extract_epi32(b, 6)), 6)),
		(_mm256_extract_epi32(_mm256_rol_epi32( a, _mm256_extract_epi32(b, 5)), 5)),
		(_mm256_extract_epi32(_mm256_rol_epi32( a, _mm256_extract_epi32(b, 4)), 4)),
		(_mm256_extract_epi32(_mm256_rol_epi32( a, _mm256_extract_epi32(b, 3)), 3)),
		(_mm256_extract_epi32(_mm256_rol_epi32( a, _mm256_extract_epi32(b, 2)), 2)),
		(_mm256_extract_epi32(_mm256_rol_epi32( a, _mm256_extract_epi32(b, 1)), 1)),
		(_mm256_extract_epi32(_mm256_rol_epi32( a, _mm256_extract_epi32(b, 0)), 0))
	);
}
#if BIT64
#define _mm256_rolv_epi64 _mm256_rolv_epi64_impl
__forceinline __m256i_i64 _mm256_rolv_epi64_impl(__m256i_i64 a, __m256i_i64 b)
{
	return _mm256_set_epi64x
	(
		(_mm256_extract_epi64(_mm256_rol_epi64( a, _mm256_extract_epi64(b, 3)), 3)),
		(_mm256_extract_epi64(_mm256_rol_epi64( a, _mm256_extract_epi64(b, 2)), 2)),
		(_mm256_extract_epi64(_mm256_rol_epi64( a, _mm256_extract_epi64(b, 1)), 1)),
		(_mm256_extract_epi64(_mm256_rol_epi64( a, _mm256_extract_epi64(b, 0)), 0))
	);
}
#endif
#endif

#endif

