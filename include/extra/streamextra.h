/***************************************************************************************************/
// HEADER_NAME /extra/streamextra.h
/***************************************************************************************************/

#if !ARM
//SSE
#define _mm_stream_epu8 _mm_stream_si128
#define _mm_stream_epi8 _mm_stream_si128
#define _mm_stream_epu16 _mm_stream_si128
#define _mm_stream_epi16 _mm_stream_si128
#define _mm_stream_epu32 _mm_stream_si128
#define _mm_stream_epi32 _mm_stream_si128
#define _mm_stream_epu64 _mm_stream_si128
#define _mm_stream_epi64 _mm_stream_si128
#endif
/***************************************************************************************************/
// Portable Stream Integer
/***************************************************************************************************/
__forceinline void _mm_stream_su8 (__uint8* mem_addr, __uint8 a)
{
	*mem_addr = a;
}
__forceinline void _mm_stream_si8 (__int8* mem_addr, __int8 a)
{
	*mem_addr = a;
}
__forceinline void _mm_stream_su16 (__uint16* mem_addr, __uint16 a)
{
	*mem_addr = a;
}
__forceinline void _mm_stream_si16 (__int16* mem_addr, __uint16 a)
{
	*mem_addr = a;
}
#if !ARM
__forceinline void _mm_stream_su32 (__uint32* mem_addr, __uint32 a)
{
	_mm_stream_si32 ((__int32*)mem_addr, (__int32)a);
}
// void _mm_stream_si32 (int* mem_addr, int a) //IMPLEMENTED BY SSE2
#ifndef __INTRIN_H_
#if _MSC_VER 
__forceinline void _mm_stream_si64(__int64* mem_addr, __int64 a)
{
	*mem_addr = a;
}
#endif
#endif

#if BIT64
__forceinline void _mm_stream_su64 (__uint64* mem_addr, __uint64 a)
{
	_mm_stream_si64((__int64*)mem_addr, (__int64)a);
}
// void _mm_stream_si64 (__int64* mem_addr, __int64 a) //IMPLEMENTED BY SSE2
#endif
#endif
/***************************************************************************************************/
// Portable Stream load
/***************************************************************************************************/
//SSE
#if !ARM
#define _mm_stream_load_epu8 _mm_stream_load_si128
#define _mm_stream_load_epi8 _mm_stream_load_si128
#define _mm_stream_load_epu16 _mm_stream_load_si128
#define _mm_stream_load_epi16 _mm_stream_load_si128
#define _mm_stream_load_epu32 _mm_stream_load_si128
#define _mm_stream_load_epi32 _mm_stream_load_si128
#define _mm_stream_load_epu64 _mm_stream_load_si128
#define _mm_stream_load_epi64 _mm_stream_load_si128
#endif
