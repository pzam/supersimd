/***************************************************************************************************/
// HEADER_NAME /extra/testextra.h
/***************************************************************************************************/

#if !ARM
/***************************************************************************************************/
// AVX Backport
/***************************************************************************************************/
#if !__AVX__

// int _mm_testc_ps (__m128 a, __m128 b)
#define _mm_testc_ps(a, b) _mm_testc_si128(_mm_castps_si128(a), _mm_castps_si128(b))

// int _mm_testz_ps (__m128 a, __m128 b)
#define _mm_testz_ps(a, b) _mm_testz_si128(_mm_castps_si128(a), _mm_castps_si128(b))

// int _mm_testnzc_ps (__m128 a, __m128 b)
#define _mm_testnzc_ps(a, b) _mm_testnzc_si128(_mm_castps_si128(a), _mm_castps_si128(b))

// int _mm_testc_pd (__m128d a, __m128d b)
#define _mm_testc_pd(a, b) _mm_testc_si128(_mm_castpd_si128(a), _mm_castpd_si128(b))

// int _mm_testz_pd (__m128d a, __m128d b)
#define _mm_testz_pd(a, b) _mm_testz_si128(_mm_castpd_si128(a), _mm_castpd_si128(b))

// int _mm_testnzc_pd (__m128d a, __m128d b)
#define _mm_testnzc_pd(a, b) _mm_testnzc_si128(_mm_castpd_si128(a), _mm_castpd_si128(b))

#endif
/***************************************************************************************************/
// Portable Test
/***************************************************************************************************/
#define _mm_testc_epi8 _mm_testc_si128
#define _mm_testz_epi8 _mm_testz_si128
#define _mm_testnzc_epi8 _mm_testnzc_si128

#define _mm_testc_epu8 _mm_testc_si128
#define _mm_testz_epu8 _mm_testz_si128
#define _mm_testnzc_epu8 _mm_testnzc_si128


#define _mm_testc_epi16 _mm_testc_si128
#define _mm_testz_epi16 _mm_testz_si128
#define _mm_testnzc_epi16 _mm_testnzc_si128

#define _mm_testc_epu16 _mm_testc_si128
#define _mm_testz_epu16 _mm_testz_si128
#define _mm_testnzc_epu16 _mm_testnzc_si128


#define _mm_testc_epi32 _mm_testc_si128
#define _mm_testz_epi32 _mm_testz_si128
#define _mm_testnzc_epi32 _mm_testnzc_si128

#define _mm_testc_epu32 _mm_testc_si128
#define _mm_testz_epu32 _mm_testz_si128
#define _mm_testnzc_epu32 _mm_testnzc_si128


#define _mm_testc_epi64 _mm_testc_si128
#define _mm_testz_epi64 _mm_testz_si128
#define _mm_testnzc_epi64 _mm_testnzc_si128

#define _mm_testc_epu64 _mm_testc_si128
#define _mm_testz_epu64 _mm_testz_si128
#define _mm_testnzc_epu64 _mm_testnzc_si128

#endif
