/***************************************************************************************************/
// HEADER_NAME /extra/loadextra.h
/***************************************************************************************************/


#if !ARM
/***************************************************************************************************/
// Portable Load
/***************************************************************************************************/
#define _mm_load_epu8 _mm_load_si128
#define _mm_load_epi8 _mm_load_si128

#define _mm_load_epu16 _mm_load_si128
#define _mm_load_epi16 _mm_load_si128

#define _mm_load_epu32 _mm_load_si128
#define _mm_load_epi32 _mm_load_si128

#define _mm_load_epu64 _mm_load_si128
#define _mm_load_epi64 _mm_load_si128

/***************************************************************************************************/
// Portable Loadu
/***************************************************************************************************/
#define _mm_loadu_epu8 _mm_loadu_si128
#define _mm_lddqu_epu8 _mm_lddqu_si128

#define _mm_loadu_epi8 _mm_loadu_si128
#define _mm_lddqu_epi8 _mm_lddqu_si128

#define _mm_loadu_epu16 _mm_loadu_si128
#define _mm_lddqu_epu16 _mm_lddqu_si128

#define _mm_loadu_epi16 _mm_loadu_si128
#define _mm_lddqu_epi16 _mm_lddqu_si128

#define _mm_loadu_epu32 _mm_loadu_si128
#define _mm_lddqu_epu32 _mm_lddqu_si128

#define _mm_loadu_epi32 _mm_loadu_si128
#define _mm_lddqu_epi32 _mm_lddqu_si128

#define _mm_loadu_epu64 _mm_loadu_si128
#define _mm_lddqu_epu64 _mm_lddqu_si128

#define _mm_loadu_epi64 _mm_loadu_si128
#define _mm_lddqu_epi64 _mm_lddqu_si128

/***************************************************************************************************/
// Portable Loadu Void*
/***************************************************************************************************/
#if !_MSC_VER 
__forceinline __m128i_i8 _mm_loadu_si8 (void const* mem_addr)
{
	return _mm_set_epi8(
		*(__int16*)mem_addr+15, *(__int16*)mem_addr+14, 
		*(__int16*)mem_addr+13, *(__int16*)mem_addr+12, 
		*(__int16*)mem_addr+11, *(__int16*)mem_addr+10, 
		*(__int16*)mem_addr+9, *(__int16*)mem_addr+8, 
		*(__int16*)mem_addr+7, *(__int16*)mem_addr+6, 
		*(__int16*)mem_addr+5, *(__int16*)mem_addr+4, 
		*(__int16*)mem_addr+3, *(__int16*)mem_addr+2, 
		*(__int16*)mem_addr+1, *(__int16*)mem_addr+0
	);
}

#if !defined(__INTEL_COMPILER)
__forceinline __m128i_i16 _mm_loadu_si16 (void const* mem_addr)
{
	return _mm_set_epi16(
		*(__int16*)mem_addr+7, *(__int16*)mem_addr+6, 
		*(__int16*)mem_addr+5, *(__int16*)mem_addr+4, 
		*(__int16*)mem_addr+3, *(__int16*)mem_addr+2, 
		*(__int16*)mem_addr+1, *(__int16*)mem_addr+0
	);
}
__forceinline __m128i_i32 _mm_loadu_si32 (void const* mem_addr)
{
		return _mm_set_epi32(
		*(__int32*)mem_addr+3, *(__int32*)mem_addr+2, 
		*(__int32*)mem_addr+1, *(__int32*)mem_addr+0
	);
}
#if !__clang__
__forceinline __m128i_i64 _mm_loadu_si64 (void const* mem_addr)
{
	return _mm_set_epi64x(
		*(__int64*)mem_addr+1, *(__int64*)mem_addr+0
	);
}
#endif
#endif
#endif

#define _mm_loadu_su8 _mm_loadu_si8
#define _mm_loadu_su16 _mm_loadu_si16
#define _mm_loadu_su32 _mm_loadu_si32
#define _mm_loadu_su64 _mm_loadu_si64

#endif

