/***************************************************************************************************/
// HEADER_NAME /extra/shiftavx512extra.h
/***************************************************************************************************/

//NOT PART OF AVX-512
#if BIT64
__forceinline __m128i_i8 _mm_rol_epi8(__m128i_i8 a, int imm8)
{
	__m128i_i32 l, r;
	l = _mm_sll_epi8(a, _mm_castepi32_epi8(_mm_cvtsi32_epi32(imm8 & 7)));
	r = _mm_srl_epi8(a, _mm_castepi32_epi8(_mm_cvtsi32_epi32((8-imm8) & 7)));
	return _mm_or_epi8(l,r);
}
#endif
//NOT PART OF AVX-512
__forceinline __m128i_i16 _mm_rol_epi16(__m128i_i16 a, int imm8)
{
	__m128i_i32 l, r;
	l = _mm_sll_epi16(a, _mm_castepi32_epi16(_mm_cvtsi32_epi32((__int16)imm8 & 15)));
	r = _mm_srl_epi16(a, _mm_castepi32_epi16(_mm_cvtsi32_epi32((16-(__int16)imm8) & 15)));
	return _mm_or_epi16(l,r);
}



#if !__AVX512VL__ && !__AVX512F__
#define _mm_rol_epi32 _mm_rol_epi32_impl
__forceinline __m128i_i32 _mm_rol_epi32_impl(__m128i_i32 a, int imm8)
{
	__m128i_i32 l, r;
	l = _mm_sll_epi32(a,_mm_cvtsi32_epi32(imm8 & 31));
	r = _mm_srl_epi32(a,_mm_cvtsi32_epi32((32-imm8) & 31));
	return _mm_or_epi32(l,r);
}

#if BIT64
#define _mm_rol_epi64 _mm_rol_epi64_impl
__forceinline __m128i_i64 _mm_rol_epi64_impl(__m128i_i64 a, int imm8)
{
	__m128i_i64 l, r;
	l = _mm_sll_epi64(a,_mm_cvtsi64_epi64(imm8 & 63));
	r = _mm_srl_epi64(a,_mm_cvtsi64_epi64((64-imm8) & 63));
	return _mm_or_epi64(l,r);
}
#endif

#endif

#if BIT64
__forceinline __m128i_i8 _mm_rolv_epi8(__m128i_i8 a, __m128i_i8 b)
{
	return _mm_set_epi8
	(
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 15)), 15)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 14)), 14)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 13)), 13)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 12)), 12)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 11)), 11)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 10)), 10)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 9)), 9)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 8)), 8)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 7)), 7)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 6)), 6)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 5)), 5)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 4)), 4)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 3)), 3)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 2)), 2)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 1)), 1)),
		(_mm_extract_epi8(_mm_rol_epi8( a, _mm_extract_epi8(b, 0)), 0))
	);
}
#endif

__forceinline __m128i_i16 _mm_rolv_epi16(__m128i_i16 a, __m128i_i16 b)
{
	return _mm_set_epi16
	(
		(_mm_extract_epi16(_mm_rol_epi16( a, _mm_extract_epi16(b, 7)), 7)),
		(_mm_extract_epi16(_mm_rol_epi16( a, _mm_extract_epi16(b, 6)), 6)),
		(_mm_extract_epi16(_mm_rol_epi16( a, _mm_extract_epi16(b, 5)), 5)),
		(_mm_extract_epi16(_mm_rol_epi16( a, _mm_extract_epi16(b, 4)), 4)),
		(_mm_extract_epi16(_mm_rol_epi16( a, _mm_extract_epi16(b, 3)), 3)),
		(_mm_extract_epi16(_mm_rol_epi16( a, _mm_extract_epi16(b, 2)), 2)),
		(_mm_extract_epi16(_mm_rol_epi16( a, _mm_extract_epi16(b, 1)), 1)),
		(_mm_extract_epi16(_mm_rol_epi16( a, _mm_extract_epi16(b, 0)), 0))
	);
}
#if !__AVX512VL__ && !__AVX512F__
#define _mm_rolv_epi32 _mm_rolv_epi32_impl
__forceinline __m128i_i32 _mm_rolv_epi32_impl(__m128i_i32 a, __m128i_i32 b)
{
	return _mm_set_epi32
	(
		(_mm_extract_epi32(_mm_rol_epi32( a, _mm_extract_epi32(b, 3)), 3)),
		(_mm_extract_epi32(_mm_rol_epi32( a, _mm_extract_epi32(b, 2)), 2)),
		(_mm_extract_epi32(_mm_rol_epi32( a, _mm_extract_epi32(b, 1)), 1)),
		(_mm_extract_epi32(_mm_rol_epi32( a, _mm_extract_epi32(b, 0)), 0))
	);
}
#if BIT64
#define _mm_rolv_epi64 _mm_rolv_epi64_impl
__forceinline __m128i_i64 _mm_rolv_epi64_impl(__m128i_i64 a, __m128i_i64 b)
{
	return _mm_set_epi64x
	(
		(_mm_extract_epi64(_mm_rol_epi64( a, _mm_extract_epi64(b, 1)), 1)),
		(_mm_extract_epi64(_mm_rol_epi64( a, _mm_extract_epi64(b, 0)), 0))
	);
}
#endif
#endif

