/***************************************************************************************************/
// HEADER_NAME /extra/cvtextra.h
/***************************************************************************************************/

#if !ARM


//TODO _mm_XXXXXX_si128 to specific data types!
/***************************************************************************************************/
// _mm_cvtsi128_xxxx
/***************************************************************************************************/
//NON-PORTABLE
__forceinline __int8  _mm_cvtsi128_si8(__m128i_i8 a){
	return _mm_cvtsi128_si32(a) & 0xFFU;  
}
__forceinline __int16 _mm_cvtsi128_si16(__m128i_i16 a) {
	return _mm_cvtsi128_si32(a) & 0xFFFFU;
}
/*
IMPLEMNTED BY SSE2
int _mm_cvtsi128_si32 (__m128i_i32 a)
__int64 _mm_cvtsi128_si64 (__m128i_i64 a)
__int64 _mm_cvtsi128_si64x (__m128i_64 a)
*/
/***************************************************************************************************/
// Cvt For API Consistency
/***************************************************************************************************/
//NON-PORTABLE
#ifndef __INTRIN_H_ || _MSC_VER || __clang__

#if BIT64
__forceinline __m128i_i64 _mm_cvtsi64x_si128(__int64 a)
{
	return _mm_cvtsi64_si128(a);
}

__forceinline __int64 _mm_cvtsi128_si64x (__m128i_i64 a){
	return _mm_cvtsi128_si64(a);
}
#endif

#endif

/***************************************************************************************************/
// EXTRACT
/***************************************************************************************************/
// __uint8 _mm_cvtepu8_su8(__m128i_u8 a)
#define _mm_cvtepu8_su8 (__uint8)_mm_cvtsi128_si8
// __int8 _mm_cvtepi8_si8(__m128i_i8 a)
#define _mm_cvtepi8_si8 _mm_cvtsi128_si8

// __uint16 _mm_cvtepu16_su16(__m128i_u16 a)
#define _mm_cvtepu16_su16 (__uint16)_mm_cvtsi128_si16
// __int16 _mm_cvtepi16_si16(__m128i_i16 a)
#define _mm_cvtepi16_si16 _mm_cvtsi128_si16

// __uint32 _mm_cvtepu32_su32 (__m128i_u32 a)
#define _mm_cvtepu32_su32 (__uint32)_mm_cvtsi128_si32
// __int32 _mm_cvtepi32_si32 (__m128i_i32 a)
#define _mm_cvtepi32_si32 _mm_cvtsi128_si32

#if BIT64
// __uint64 _mm_cvtepu64_su64 (__m128i_u64 a)
#define _mm_cvtepu64_su64 (__uint64)_mm_cvtsi128_si64
// __int64 _mm_cvtepi64_si64 (__m128i_i64 a)
#define _mm_cvtepi64_si64 _mm_cvtsi128_si64


// __uint64 _mm_cvtepu64_su64x (__m128i_u64 a)
#define _mm_cvtepu64_su64x (__uint64)_mm_cvtsi128_si64x
// __int64 _mm_cvtepi64_si64x (__m128i_i64 a)
#define _mm_cvtepi64_si64x _mm_cvtsi128_si64x
#endif

/***************************************************************************************************/
// SET
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cvtsu8_epu8 (__uint8 a){
	return _mm_insert_epu8 (_mm_setzero_epu8(),a, 0);
}
__forceinline __m128i_i8 _mm_cvtsi8_epi8 (__int8 a){
	return _mm_insert_epi8 (_mm_setzero_epi8(),a, 0);
}

__forceinline __m128i_u16 _mm_cvtsu16_epu16 (__uint16 a){
	// NOTE:
	// Generates Better results with MSVC instead of using _mm_insert_epi16
	// GCC makes no difference.
	return _mm_set_epu16 (0, 0, 0, 0, 0, 0, 0, (__int16)a);
}
__forceinline __m128i_i16 _mm_cvtsi16_epi16 (__int16 a){
	// NOTE:
	// Generates Better results with MSVC instead of using _mm_insert_epi16
	// GCC makes no difference.
	return _mm_set_epi16 (0, 0, 0, 0, 0, 0, 0, a);
}

__forceinline __m128i_u32 _mm_cvtsu32_epu32 (__uint32 a){
	return _mm_cvtsi32_si128((__int32)a);
}
__forceinline __m128i_i32 _mm_cvtsi32_epi32 (__int32 a){
	return _mm_cvtsi32_si128(a);
}

#if BIT64
#if __clang__
#define _mm_cvtsi64x_si128 _mm_cvtsi64_si128
#endif

__forceinline __m128i_u64 _mm_cvtsu64_epu64 (__uint64 a){
	return _mm_cvtsi64_si128((__int64)a);
}
__forceinline __m128i_i64 _mm_cvtsi64_epi64 (__int64 a){
	return _mm_cvtsi64_si128(a);
}


__forceinline __m128i_u64 _mm_cvtsu64x_epu64 (__uint64 a){
	return _mm_cvtsi64x_si128((__int64)a);
}
__forceinline __m128i_i64 _mm_cvtsi64x_epi64 (__int64 a){
	return _mm_cvtsi64x_si128(a);
}
#endif
#endif



/***************************************************************************************************/
// AVX
/***************************************************************************************************/
//NEEDS MSVC 2018+
//AVX2 Missing features