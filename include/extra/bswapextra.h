/***************************************************************************************************/
// HEADER_NAME /extra/bswapextra.h
/***************************************************************************************************/
//BSWAP
//SSE
#if !ARM

__forceinline __m128i_i32 _mm_bswap_si128 (__m128i_i32 a) //NOT PORTABLE
{
	// Reverse order of all bytes in the 128-bit word.
	return _mm_castepi8_epi32(_mm_shuffle_epi8(_mm_castepi32_epi8(a), _mm_set_epi8(0, 1,  2,  3, 4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15)));
}

#define _mm_bswap_epi8 _mm_castepi32_epi8(_mm_bswap_si128(_mm_castepu8_epi32(a)))
#define _mm_bswap_epu8(a) _mm_castepi32_epu8(_mm_bswap_si128(_mm_castepu8_epi32(a)))

__forceinline __m128i_i16 _mm_bswap_epi16 (__m128i_i16 a)
{
	// Swap upper and higher byte in each 16-bit word:
	return _mm_or_epi16(
		_mm_slli_epi16(_mm_and_epi16(a, __m128i_i16_0x00ff), 8),
		_mm_srli_epi16(_mm_and_epi16(a, __m128i_i16_0xff00), 8)
	);
}
#define _mm_bswap_epu16(a) _mm_castepi16_epu16(_mm_bswap_epi16(_mm_castepu16_epi16(a)))

__forceinline __m128i_i32 _mm_bswap_epi32 (__m128i_i32 a)
{
	// Reverse order of bytes in each 32-bit word.
	return _mm_castepi8_epi32(_mm_shuffle_epi8(_mm_castepi32_epi8(a), _mm_set_epi8(
		12, 13, 14, 15,
		8,  9, 10, 11,
		4,  5,  6,  7,
		0,  1,  2,  3
	)));
}
#define _mm_bswap_epu32(a) _mm_castepi32_epu32(_mm_bswap_epi32(_mm_castepu32_epi32(a)))

#if BIT64
__forceinline __m128i_i64 _mm_bswap_epi64 (__m128i_i64 a)
{
	// Reverse order of bytes in each 64-bit word.
	return _mm_castepi8_epi64(_mm_shuffle_epi8(_mm_castepi64_epi8(a), _mm_set_epi8(
		8, 9, 10, 11, 12, 13, 14, 15,
		0,  1,  2,  3,  4,  5,  6,  7
	)));

}
#define _mm_bswap_epu64(a) _mm_castepi64_epu64(_mm_bswap_epi64(_mm_castepu64_epi64(a)))
#endif

#endif
