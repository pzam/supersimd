/***************************************************************************************************/
// HEADER_NAME /extra/maskextra.h
/***************************************************************************************************/

/***************************************************************************************************/
// Create a Vector Mask form Interger HEX
/***************************************************************************************************/


//SSE
__forceinline __m128i_i8 _mm_maskfromint_si128 (int imm8) 
{
	__uint8 v0 = 0x0;
	__uint8 v1 = 0x0;
	__uint8 v2 = 0x0;
	__uint8 v3 = 0x0;
	__uint8 v4 = 0x0;
	__uint8 v5 = 0x0;
	__uint8 v6 = 0x0;
	__uint8 v7 = 0x0;
	__uint8 v8 = 0x0;
	__uint8 v9 = 0x0;
	__uint8 v10 = 0x0;
	__uint8 v11 = 0x0;
	__uint8 v12 = 0x0;
	__uint8 v13 = 0x0;
	__uint8 v14 = 0x0;
	__uint8 v15 = 0x0;
	
	
	if(imm8 & 0x8000)
		v15 = 0xFF;
	if(imm8 & 0x4000)
		v14 = 0xFF;
	if(imm8 & 0x2000)
		v13 = 0xFF;
	if(imm8 & 0x1000)
		v12 = 0xFF;
	if(imm8 & 0x0800)
		v11 = 0xFF;
	if(imm8 & 0x0400)
		v10 = 0xFF;
	if(imm8 & 0x0200)
		v9 = 0xFF;
	if(imm8 & 0x0100)
		v8 = 0xFF;
	if(imm8 & 0x80)
		v7 = 0xFF;
	if(imm8 & 0x40)
		v6 = 0xFF;
	if(imm8 & 0x20)
		v5 = 0xFF;
	if(imm8 & 0x10)
		v4 = 0xFF;
	if(imm8 & 0x08)
		v3 = 0xFF;
	if(imm8 & 0x04)
		v2 = 0xFF;
	if(imm8 & 0x02)
		v1 = 0xFF;
	if(imm8 & 0x01)
		v0 = 0xFF;
	
	return _mm_set_epi8 (v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15);
}


__forceinline __m128i_i8 _mm_maskfromint_epi8 (int imm8) 
{
	__int8 v0 = 0x0;
	__int8 v1 = 0x0;
	__int8 v2 = 0x0;
	__int8 v3 = 0x0;
	__int8 v4 = 0x0;
	__int8 v5 = 0x0;
	__int8 v6 = 0x0;
	__int8 v7 = 0x0;
	__int8 v8 = 0x0;
	__int8 v9 = 0x0;
	__int8 v10 = 0x0;
	__int8 v11 = 0x0;
	__int8 v12 = 0x0;
	__int8 v13 = 0x0;
	__int8 v14 = 0x0;
	__int8 v15 = 0x0;
	
	
	if(imm8 & 0x8000)
		v15 = 0xFF;
	if(imm8 & 0x4000)
		v14 = 0xFF;
	if(imm8 & 0x2000)
		v13 = 0xFF;
	if(imm8 & 0x1000)
		v12 = 0xFF;
	if(imm8 & 0x0800)
		v11 = 0xFF;
	if(imm8 & 0x0400)
		v10 = 0xFF;
	if(imm8 & 0x0200)
		v9 = 0xFF;
	if(imm8 & 0x0100)
		v8 = 0xFF;
	if(imm8 & 0x80)
		v7 = 0xFF;
	if(imm8 & 0x40)
		v6 = 0xFF;
	if(imm8 & 0x20)
		v5 = 0xFF;
	if(imm8 & 0x10)
		v4 = 0xFF;
	if(imm8 & 0x08)
		v3 = 0xFF;
	if(imm8 & 0x04)
		v2 = 0xFF;
	if(imm8 & 0x02)
		v1 = 0xFF;
	if(imm8 & 0x01)
		v0 = 0xFF;
	
	return _mm_set_epi8 (v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15);
}

__forceinline __m128i_u8 _mm_maskfromint_epu8 (int imm8) 
{
	__uint8 v0 = 0x0;
	__uint8 v1 = 0x0;
	__uint8 v2 = 0x0;
	__uint8 v3 = 0x0;
	__uint8 v4 = 0x0;
	__uint8 v5 = 0x0;
	__uint8 v6 = 0x0;
	__uint8 v7 = 0x0;
	__uint8 v8 = 0x0;
	__uint8 v9 = 0x0;
	__uint8 v10 = 0x0;
	__uint8 v11 = 0x0;
	__uint8 v12 = 0x0;
	__uint8 v13 = 0x0;
	__uint8 v14 = 0x0;
	__uint8 v15 = 0x0;
	
	
	if(imm8 & 0x8000)
		v15 = 0xFF;
	if(imm8 & 0x4000)
		v14 = 0xFF;
	if(imm8 & 0x2000)
		v13 = 0xFF;
	if(imm8 & 0x1000)
		v12 = 0xFF;
	if(imm8 & 0x0800)
		v11 = 0xFF;
	if(imm8 & 0x0400)
		v10 = 0xFF;
	if(imm8 & 0x0200)
		v9 = 0xFF;
	if(imm8 & 0x0100)
		v8 = 0xFF;
	if(imm8 & 0x80)
		v7 = 0xFF;
	if(imm8 & 0x40)
		v6 = 0xFF;
	if(imm8 & 0x20)
		v5 = 0xFF;
	if(imm8 & 0x10)
		v4 = 0xFF;
	if(imm8 & 0x08)
		v3 = 0xFF;
	if(imm8 & 0x04)
		v2 = 0xFF;
	if(imm8 & 0x02)
		v1 = 0xFF;
	if(imm8 & 0x01)
		v0 = 0xFF;
	
	return _mm_set_epu8 (v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15);
}

__forceinline __m128i_i16 _mm_maskfromint_epi16 (int imm8) 
{
	__int16 v0 = 0x0;
	__int16 v1 = 0x0;
	__int16 v2 = 0x0;
	__int16 v3 = 0x0;
	__int16 v4 = 0x0;
	__int16 v5 = 0x0;
	__int16 v6 = 0x0;
	__int16 v7 = 0x0;
	
	if(imm8 & 0x80)
		v7 = 0xFFFF;
	if(imm8 & 0x40)
		v6 = 0xFFFF;
	if(imm8 & 0x20)
		v5 = 0xFFFF;
	if(imm8 & 0x10)
		v4 = 0xFFFF;
	if(imm8 & 0x08)
		v3 = 0xFFFF;
	if(imm8 & 0x04)
		v2 = 0xFFFF;
	if(imm8 & 0x02)
		v1 = 0xFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFF;
	
	return _mm_set_epi16 (v0, v1, v2, v3, v4, v5, v6, v7);
}


__forceinline __m128i_u16 _mm_maskfromint_epu16 (int imm8) 
{
	__uint16 v0 = 0x0;
	__uint16 v1 = 0x0;
	__uint16 v2 = 0x0;
	__uint16 v3 = 0x0;
	__uint16 v4 = 0x0;
	__uint16 v5 = 0x0;
	__uint16 v6 = 0x0;
	__uint16 v7 = 0x0;
	
	if(imm8 & 0x80)
		v7 = 0xFFFF;
	if(imm8 & 0x40)
		v6 = 0xFFFF;
	if(imm8 & 0x20)
		v5 = 0xFFFF;
	if(imm8 & 0x10)
		v4 = 0xFFFF;
	if(imm8 & 0x08)
		v3 = 0xFFFF;
	if(imm8 & 0x04)
		v2 = 0xFFFF;
	if(imm8 & 0x02)
		v1 = 0xFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFF;
	
	return _mm_set_epu16 (v0, v1, v2, v3, v4, v5, v6, v7);
}

__forceinline __m128i_i32 _mm_maskfromint_epi32 (int imm8) 
{
	__int32 v0 = 0x0;
	__int32 v1 = 0x0;
	__int32 v2 = 0x0;
	__int32 v3 = 0x0;
	
	if(imm8 & 0x08)
		v3 = 0xFFFFFFFF;
	if(imm8 & 0x04)
		v2 = 0xFFFFFFFF;
	if(imm8 & 0x02)
		v1 = 0xFFFFFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFFFFFF;
	
	return _mm_set_epi32 (v0, v1, v2, v3);
}

__forceinline __m128i_u32 _mm_maskfromint_epu32 (int imm8) 
{
	__uint32 v0 = 0x0;
	__uint32 v1 = 0x0;
	__uint32 v2 = 0x0;
	__uint32 v3 = 0x0;
	
	if(imm8 & 0x08)
		v3 = 0xFFFFFFFF;
	if(imm8 & 0x04)
		v2 = 0xFFFFFFFF;
	if(imm8 & 0x02)
		v1 = 0xFFFFFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFFFFFF;
	
	return _mm_set_epu32 (v0, v1, v2, v3);
}

__forceinline __m128i_i64 _mm_maskfromint_epi64 (int imm8) 
{
	__int64 v0 = 0x0;
	__int64 v1 = 0x0;
	
	if(imm8 & 0x02)
		v1 = 0xFFFFFFFFFFFFFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFFFFFFFFFFFFFF;
	
	return _mm_set_epi64x (v0, v1);
}

__forceinline __m128i_u64 _mm_maskfromint_epu64 (int imm8) 
{
	__uint64 v0 = 0x0;
	__uint64 v1 = 0x0;
	
	if(imm8 & 0x02)
		v1 = 0xFFFFFFFFFFFFFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFFFFFFFFFFFFFF;
	
	return _mm_set_epu64x (v0, v1);
}

