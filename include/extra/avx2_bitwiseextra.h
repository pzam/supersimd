/***************************************************************************************************/
// HEADER_NAME /extra/avx2_bitwiseextra.h
/***************************************************************************************************/

//BITWISE NOT

//AVX2
#if __AVX2__
__forceinline __m256i _mm256_not_si256 (__m256i x)//Not Portable
{
	return _mm256_xor_si256(x, _mm256_cmpeq_epi32(_mm256_setzero_si256(), _mm256_setzero_si256()));
}


__forceinline __m256 _mm256_not_ps (__m256 a)
{
	return _mm256_xor_ps(a, _mm256_cmpeq_ps(_mm256_setzero_ps(), _mm256_setzero_ps()));
}

__forceinline __m256d _mm256_not_pd (__m256d a)
{
	return _mm256_xor_pd(a, _mm256_cmpeq_pd(_mm256_setzero_pd(), _mm256_setzero_pd()));
}



/***************************************************************************************************/
// Portable AVX Bitwise Functions
/***************************************************************************************************/
// Use these functions instead of _mm256_not_si256 to ensure portability!
// NOT [!]
// __m256i_u8 _mm256_not_epu8 (__m256i_u8 a)
// __m256i_i8 _mm256_not_epi8 (__m256i_i8 a)
// __m256i_u16 _mm256_not_epu16 (__m256i_u16 a)
// __m256i_i16 _mm256_not_epi16 (__m256i_i16 a)
// __m256i_u32 _mm256_not_epu32 (__m256i_u32 a)
// __m256i_i32 _mm256_not_epi32 (__m256i_i32 a)
// __m256i_u64 _mm256_not_epu64 (__m256i_u64 a)
// __m256i_i64 _mm256_not_epi64 (__m256i_i64 a)

#define _mm256_not_epu8 _mm256_not_si256
#define _mm256_not_epi8 _mm256_not_si256
#define _mm256_not_epu16 _mm256_not_si256
#define _mm256_not_epi16 _mm256_not_si256
#define _mm256_not_epu32 _mm256_not_si256
#define _mm256_not_epi32 _mm256_not_si256
#define _mm256_not_epu64 _mm256_not_si256
#define _mm256_not_epi64 _mm256_not_si256


// AND [&]
// __m256i_u8 _mm256_and_epu8 (__m256i_u8 a)
// __m256i_i8 _mm256_and_epi8 (__m256i_i8 a)
// __m256i_u16 _mm256_and_epu16 (__m256i_u16 a)
// __m256i_i16 _mm256_and_epi16 (__m256i_i16 a)
// __m256i_u32 _mm256_and_epu32 (__m256i_u32 a)
// __m256i_i32 _mm256_and_epi32 (__m256i_i32 a)
// __m256i_u64 _mm256_and_epu64 (__m256i_u64 a)
// __m256i_i64 _mm256_and_epi64 (__m256i_i64 a)

#define _mm256_and_epu8 _mm256_and_si256
#define _mm256_and_epi8 _mm256_and_si256
#define _mm256_and_epu16 _mm256_and_si256
#define _mm256_and_epi16 _mm256_and_si256
#define _mm256_and_epu32 _mm256_and_si256
#define _mm256_and_epi32 _mm256_and_si256
#define _mm256_and_epu64 _mm256_and_si256
#define _mm256_and_epi64 _mm256_and_si256


// ANDNOT [~a & b]
// __m256i_u8 _mm256_andnot_epu8 (__m256i_u8 a)
// __m256i_i8 _mm256_andnot_epi8 (__m256i_i8 a)
// __m256i_u16 _mm256_andnot_epu16 (__m256i_u16 a)
// __m256i_i16 _mm256_andnot_epi16 (__m256i_i16 a)
// __m256i_u32 _mm256_andnot_epu32 (__m256i_u32 a)
// __m256i_i32 _mm256_andnot_epi32 (__m256i_i32 a)
// __m256i_u64 _mm256_andnot_epu64 (__m256i_u64 a)
// __m256i_i64 _mm256_andnot_epi64 (__m256i_i64 a)

#define _mm256_andnot_epu8 _mm256_andnot_si256
#define _mm256_andnot_epi8 _mm256_andnot_si256
#define _mm256_andnot_epu16 _mm256_andnot_si256
#define _mm256_andnot_epi16 _mm256_andnot_si256
#define _mm256_andnot_epu32 _mm256_andnot_si256
#define _mm256_andnot_epi32 _mm256_andnot_si256
#define _mm256_andnot_epu64 _mm256_andnot_si256
#define _mm256_andnot_epi64 _mm256_andnot_si256


// OR [|]
// __m256i_u8 _mm256_or_epu8 (__m256i_u8 a)
// __m256i_i8 _mm256_or_epi8 (__m256i_i8 a)
// __m256i_u16 _mm256_or_epu16 (__m256i_u16 a)
// __m256i_i16 _mm256_or_epi16 (__m256i_i16 a)
// __m256i_u32 _mm256_or_epu32 (__m256i_u32 a)
// __m256i_i32 _mm256_or_epi32 (__m256i_i32 a)
// __m256i_u64 _mm256_or_epu64 (__m256i_u64 a)
// __m256i_i64 _mm256_or_epi64 (__m256i_i64 a)

#define _mm256_or_epu8 _mm256_or_si256
#define _mm256_or_epi8 _mm256_or_si256
#define _mm256_or_epu16 _mm256_or_si256
#define _mm256_or_epi16 _mm256_or_si256
#define _mm256_or_epu32 _mm256_or_si256
#define _mm256_or_epi32 _mm256_or_si256
#define _mm256_or_epu64 _mm256_or_si256
#define _mm256_or_epi64 _mm256_or_si256


// XOR [^]
// __m256i_u8 _mm256_xor_epu8 (__m256i_u8 a)
// __m256i_i8 _mm256_xor_epi8 (__m256i_i8 a)
// __m256i_u16 _mm256_xor_epu16 (__m256i_u16 a)
// __m256i_i16 _mm256_xor_epi16 (__m256i_i16 a)
// __m256i_u32 _mm256_xor_epu32 (__m256i_u32 a)
// __m256i_i32 _mm256_xor_epi32 (__m256i_i32 a)
// __m256i_u64 _mm256_xor_epu64 (__m256i_u64 a)
// __m256i_i64 _mm256_xor_epi64 (__m256i_i64 a)

#define _mm256_xor_epu8 _mm256_xor_si256
#define _mm256_xor_epi8 _mm256_xor_si256
#define _mm256_xor_epu16 _mm256_xor_si256
#define _mm256_xor_epi16 _mm256_xor_si256
#define _mm256_xor_epu32 _mm256_xor_si256
#define _mm256_xor_epi32 _mm256_xor_si256
#define _mm256_xor_epu64 _mm256_xor_si256
#define _mm256_xor_epi64 _mm256_xor_si256


#endif
/***************************************************************************************************/
// BIT SHIFTING
/***************************************************************************************************/
#if __AVX2__
__forceinline __m256i_i8 _mm256_slli_epi8(__m256i_i8 a, const int imm8) {//
	return _mm256_and_si256(_mm256_slli_epi16(a, imm8), _mm256_set1_epi8((__uint8)(0xFFU << (imm8 & 15))));
}
#define _mm256_slli_epu8 _mm256_slli_epi8

__forceinline __m256i_i8 _mm256_sll_epi8(__m256i_i8 a, __m128i_i8 b) {//
	return _mm256_and_si256(_mm256_sll_epi16(a, b), _mm256_set1_epi8(0xFFU << _mm_cvtsi128_si64(b)));
}
#define _mm256_sll_epu8 _mm256_sll_epi8

__forceinline __m256i_i8 _mm256_sllv_epi8(__m256i_i8 a, __m256i_i8 b) {//
	return _mm256_and_si256(_mm256_sllv_epi16(a, b), _mm256_set1_epi8(0xFFU << _mm256_cvtsi256_si64(b)));
}
#define _mm256_sllv_epu8 _mm256_sllv_epi8

__forceinline __m256i_i8 _mm256_srli_epi8(__m256i_i8 a, const int b) {
	return _mm256_and_si256(_mm256_srli_epi16(a, b), _mm256_set1_epi8(0xFFU >> b));
}
#define _mm256_srli_epu8 _mm256_srli_epi8

__forceinline __m256i_i8 _mm256_srl_epi8(__m256i_i8 a, __m128i_i8 b) {
	return _mm256_and_si256(_mm256_srl_epi16(a, b), _mm256_set1_epi8(0xFFU >> _mm_cvtsi128_si64(b)));
}
#define _mm256_srl_epu8 _mm256_srl_epi8

__forceinline __m256i_i8 _mm256_srlv_epi8(__m256i_i8 a, __m256i_i8 b) {
	return _mm256_and_si256(_mm256_srlv_epi16(a, b), _mm256_set1_epi8(0xFFU >> _mm256_cvtsi256_si64(b)));
}
#define _mm256_srlv_epu8 _mm256_srlv_epi8

__forceinline __m256i_i8 _mm256_srai_epi8(__m256i_i8 a, const int b) {
	return _mm256_packs_epi16( _mm256_srai_epi16(_mm256_unpacklo_epi8(a, a), 8 + b), _mm256_srai_epi16(_mm256_unpackhi_epi8(a, a), 8 + b));
}
#define _mm256_srai_epu8 _mm256_srai_epi8

__forceinline __m256i_i8 _mm256_sra_epi8(__m256i_i8 a, __m128i_i8 b) {

	__m128i elo_a = _mm256_extractf128_si256(a, 0);
	__m128i ehi_a = _mm256_extractf128_si256(a, 1);

	__m256i_i8 ya = _mm256_set_m128i(_mm_cvtepi8_epi16(ehi_a), _mm_cvtepi8_epi16(elo_a));
	__m256i_i8 ysr = _mm256_set_m128i(_mm_cvtepi8_epi16(_mm_srli_si128(ehi_a, 8)), _mm_cvtepi8_epi16(_mm_srli_si128(elo_a, 8)));

	return _mm256_packs_epi16( _mm256_sra_epi16(ya, b), _mm256_sra_epi16(ysr, b));
}
#define _mm256_sra_epu8 _mm256_sra_epi8

__forceinline __m256i_i8 _mm256_srav_epi8(__m256i_i8 a, __m256i_i8 b) {

	__m128i elo_a = _mm256_extractf128_si256(a, 0);
	__m128i ehi_a = _mm256_extractf128_si256(a, 1);

	__m256i_i8 ya = _mm256_set_m128i(_mm_cvtepi8_epi16(ehi_a), _mm_cvtepi8_epi16(elo_a));
	__m256i_i8 ysr = _mm256_set_m128i(_mm_cvtepi8_epi16(_mm_srli_si128(ehi_a, 8)), _mm_cvtepi8_epi16(_mm_srli_si128(elo_a, 8)));

	return _mm256_packs_epi16( _mm256_srav_epi16(ya, b), _mm256_srav_epi16(ysr, b));
}
#define _mm256_srav_epu8 _mm256_srav_epi8

//IMPLEMENTED BY AVX2
//__m256i_i16 _mm256_slli_epi16 (__m256i_i16 a, int imm8)
#define _mm256_slli_epu16 _mm256_slli_epi16
//__m256i_i32 _mm256_slli_epi32 (__m256i_i32 a, int imm8)
#define _mm256_slli_epu32 _mm256_slli_epi32
//__m256i_i64 _mm256_slli_epi64 (__m256i_i64 a, int imm8)
#define _mm256_slli_epu64 _mm256_slli_epi64


//__m256i_i16 _mm256_sll_epi16 (__m256i_i16 a, __m256i_i16 b)
#define _mm256_sll_epu16 _mm256_sll_epi16
//__m256i_i32 _mm256_sll_epi32 (__m256i_i32 a, __m256i_i32 b)
#define _mm256_sll_epu32 _mm256_sll_epi32
//__m256i_i64 _mm256_sll_epi64 (__m256i_i64 a, __m256i_i64 b)
#define _mm256_sll_epu64 _mm256_sll_epi64


//__m256i_i16 _mm256_srli_epi16 (__m256i_i16 a, int imm8)
#define _mm256_srli_epu16 _mm256_srli_epi16
//__m256i_i32 _mm256_srli_epi32 (__m256i_i32 a, int imm8)
#define _mm256_srli_epu32 _mm256_srli_epi32
//__m256i_i64 _mm256_srli_epi64 (__m256i_i64 a, int imm8)
#define _mm256_srli_epu64 _mm256_srli_epi64


//__m256i_i16 _mm256_srl_epi16 (__m256i_i16 a, __m256i_i16 b)
#define _mm256_srl_epu16 _mm256_srl_epi16
//__m256i_i32 _mm256_srl_epi32 (__m256i_i32 a, __m256i_i32 b)
#define _mm256_srl_epu32 _mm256_srl_epi32
//__m256i_i64 _mm256_srl_epi64 (__m256i_i64 a, __m256i_i64 b)
#define _mm256_srl_epu64 _mm256_srl_epi64


//__m256i_i16 _mm256_srai_epi16 (__m256i_i16 a, int imm8)
#define _mm256_srai_epu16 _mm256_srai_epi16
//__m256i_i32 _mm256_srai_epi32 (__m256i_i32 a, int imm8)
#define _mm256_srai_epu32 _mm256_srai_epi32
//__m256i_i64 _mm256_srai_epi64 (__m256i_i64 a, int imm8) //AVX-512
#define _mm256_srai_epu64 _mm256_srai_epi64


//__m256i_i16 _mm256_sra_epi16 (__m256i_i16 a, __m256i_i16 b)
#define _mm256_sra_epu16 _mm256_sra_epi16
//__m256i_i32 _mm256_sra_epi32 (__m256i_i32 a, __m256i_i32 b)
#define _mm256_sra_epu32 _mm256_sra_epi32
//__m256i_i64 _mm256_sra_epi64 (__m256i_i64 a, __m256i_i64 b) //AVX-512
#define _mm256_sra_epu64 _mm256_sra_epi64

#endif

/***************************************************************************************************/
// ALTERNATE NAMES FOR BIT SHIFTING
/***************************************************************************************************/
#if __AVX2__
__forceinline __m256i_i8 _mm256_bslli_epi8(__m256i_i8 a, int imm8) {
	return _mm256_slli_epi8(a, imm8);
}
__forceinline __m256i_i16 _mm256_bslli_epi16 (__m256i_i16 a, int imm8){
	return _mm256_slli_epi16(a, imm8);
}
__forceinline __m256i_i32 _mm256_bslli_epi32 (__m256i_i32 a, int imm8){
	return _mm256_slli_epi32(a, imm8);
}
#if BIT64
__forceinline __m256i_i64 _mm256_bslli_epi64 (__m256i_i64 a, int imm8){
	return _mm256_slli_epi64(a, imm8);
}
// _mm256_bslli_si256 implemented by SSE2
#endif


__forceinline __m256i_i8 _mm256_bsll_epi8 (__m256i_i8 a, __m128i_i8 b){
	return _mm256_sll_epi8(a, b);
}
__forceinline __m256i_i8 _mm256_bsllv_epi8 (__m256i_i8 a, __m256i_i8 b){
	return _mm256_sllv_epi8(a, b);
}

__forceinline __m256i_i16 _mm256_bsll_epi16 (__m256i_i16 a, __m128i_i16 b){
	return _mm256_sll_epi16(a, b);
}
__forceinline __m256i_i16 _mm256_bsllv_epi16 (__m256i_i16 a, __m256i_i16 b){
	return _mm256_sllv_epi16(a, b);
}

__forceinline __m256i_i32 _mm256_bsll_epi32 (__m256i_i32 a, __m128i_i32 b){
	return _mm256_sll_epi32(a, b);
}
__forceinline __m256i_i32 _mm256_bsllv_epi32 (__m256i_i32 a, __m256i_i32 b){
	return _mm256_sllv_epi32(a, b);
}
#if BIT64
__forceinline __m256i_i64 _mm256_bsll_epi64 (__m256i_i64 a, __m128i_i64 b){
	return _mm256_sll_epi64(a, b);
}
__forceinline __m256i_i64 _mm256_bsllv_epi64 (__m256i_i64 a, __m256i_i64 b){
	return _mm256_sllv_epi64(a, b);
}
#endif


__forceinline __m256i_i8 _mm256_bsrli_epi8(__m256i_i8 a, int imm8) {
	return _mm256_srli_epi8(a, imm8);
}
__forceinline __m256i_i16 _mm256_bsrli_epi16 (__m256i_i16 a, int imm8){
	return _mm256_srli_epi16(a, imm8);
}
__forceinline __m256i_i32 _mm256_bsrli_epi32 (__m256i_i32 a, int imm8){
	return _mm256_srli_epi32(a, imm8);
}
#if BIT64
__forceinline __m256i_i64 _mm256_bsrli_epi64 (__m256i_i64 a, int imm8){
	return _mm256_srli_epi64(a, imm8);
}
// _mm256_bsrli_si256 implemented by AVX2
#endif


__forceinline __m256i_i8 _mm256_bsrl_epi8(__m256i_i8 a, __m128i_i8 b) {
	return _mm256_srl_epi8(a, b);
}
__forceinline __m256i_i8 _mm256_bsrlv_epi8(__m256i_i8 a, __m256i_i8 b) {
	return _mm256_srlv_epi8(a, b);
}

__forceinline __m256i_i16 _mm256_bsrl_epi16 (__m256i_i16 a, __m128i_i16 b){
	return _mm256_srl_epi16(a, b);
}
__forceinline __m256i_i16 _mm256_bsrlv_epi16 (__m256i_i16 a, __m256i_i16 b){
	return _mm256_srlv_epi16(a, b);
}

__forceinline __m256i_i32 _mm256_bsrl_epi32 (__m256i_i32 a, __m128i_i32 b){
	return _mm256_srl_epi32(a, b);
}
__forceinline __m256i_i32 _mm256_bsrlv_epi32 (__m256i_i32 a, __m256i_i32 b){
	return _mm256_srlv_epi32(a, b);
}
#if BIT64
__forceinline __m256i_i64 _mm256_bsrl_epi64 (__m256i_i64 a, __m128i_i64 b){
	return _mm256_srl_epi64(a, b);
}
__forceinline __m256i_i64 _mm256_bsrlv_epi64 (__m256i_i64 a, __m256i_i64 b){
	return _mm256_srlv_epi64(a, b);
}
#endif
#endif