/***************************************************************************************************/
// HEADER_NAME /extra/bitwiseextra.h
/***************************************************************************************************/

//BITWISE NOT

//Use only function for 32-bit ints or floats otherwise can not guarantee portability!
#if !ARM

#if defined(__MMX__) || defined(_M_IX86)
__forceinline __m64 _mm_not_si64 (__m64 a)
{
	return _mm_xor_si64(a, _mm_cmpeq_pi32(_mm_setzero_si64(), _mm_setzero_si64()));
}
#endif

__forceinline __m128i _mm_not_si128 (__m128i a)
{
	return _mm_xor_si128(a, _mm_cmpeq_epi32(_mm_setzero_si128(), _mm_setzero_si128()));
}
#endif
#if !ARM
__forceinline __m128 _mm_not_ps (__m128 a)
{
	return _mm_xor_ps(a, _mm_cmpeq_ps(_mm_setzero_ps(), _mm_setzero_ps()));
}
#endif

#if BIT64
__forceinline __m128d _mm_not_pd (__m128d a)
{
	return _mm_xor_pd(a, _mm_cmpeq_pd(_mm_setzero_pd(), _mm_setzero_pd()));
}
#endif

#if !ARM

#if defined(__MMX__) || defined(_M_IX86)
/***************************************************************************************************/
// Portable MMX Bitwise Functions
/***************************************************************************************************/
// Use these functions instead of _mm_not_si64 to ensure portability!

// NOT [!]
// __m64i_u8 _mm_not_pu8 (__m64i_u8 a)
// __m64i_i8 _mm_not_pi8 (__m64i_i8 a)
// __m64i_u16 _mm_not_pu16 (__m64i_u16 a)
// __m64i_i16 _mm_not_pi16 (__m64i_i16 a)
// __m64i_u32 _mm_not_pu32 (__m64i_u32 a)
// __m64i_i32 _mm_not_pi32 (__m64i_i32 a)
// __m64i_u64 _mm_not_pu64 (__m64i_u64 a)
// __m64i_i64 _mm_not_pi64 (__m64i_i64 a)

#define _mm_not_pu8 _mm_not_si64
#define _mm_not_pi8 _mm_not_si64
#define _mm_not_pu16 _mm_not_si64
#define _mm_not_pi16 _mm_not_si64
#define _mm_not_pu32 _mm_not_si64
#define _mm_not_pi32 _mm_not_si64
#define _mm_not_pu64 _mm_not_si64
#define _mm_not_pi64 _mm_not_si64



// AND [&]
// __m64i_u8 _mm_and_pu8 (__m64i_u8 a)
// __m64i_i8 _mm_and_pi8 (__m64i_i8 a)
// __m64i_u16 _mm_and_pu16 (__m64i_u16 a)
// __m64i_i16 _mm_and_pi16 (__m64i_i16 a)
// __m64i_u32 _mm_and_pu32 (__m64i_u32 a)
// __m64i_i32 _mm_and_pi32 (__m64i_i32 a)
// __m64i_u64 _mm_and_pu64 (__m64i_u64 a)
// __m64i_i64 _mm_and_pi64 (__m64i_i64 a)

#define _mm_and_pu8 _mm_and_si64
#define _mm_and_pi8 _mm_and_si64
#define _mm_and_pu16 _mm_and_si64
#define _mm_and_pi16 _mm_and_si64
#define _mm_and_pu32 _mm_and_si64
#define _mm_and_pi32 _mm_and_si64
#define _mm_and_pu64 _mm_and_si64
#define _mm_and_pi64 _mm_and_si64


// ANDNOT [~a & b]
// __m64i_u8 _mm_andnot_pu8 (__m64i_u8 a)
// __m64i_i8 _mm_andnot_pi8 (__m64i_i8 a)
// __m64i_u16 _mm_andnot_pu16 (__m64i_u16 a)
// __m64i_i16 _mm_andnot_pi16 (__m64i_i16 a)
// __m64i_u32 _mm_andnot_pu32 (__m64i_u32 a)
// __m64i_i32 _mm_andnot_pi32 (__m64i_i32 a)
// __m64i_u64 _mm_andnot_pu64 (__m64i_u64 a)
// __m64i_i64 _mm_andnot_pi64 (__m64i_i64 a)

#define _mm_andnot_pu8 _mm_andnot_si64
#define _mm_andnot_pi8 _mm_andnot_si64
#define _mm_andnot_pu16 _mm_andnot_si64
#define _mm_andnot_pi16 _mm_andnot_si64
#define _mm_andnot_pu32 _mm_andnot_si64
#define _mm_andnot_pi32 _mm_andnot_si64
#define _mm_andnot_pu64 _mm_andnot_si64
#define _mm_andnot_pi64 _mm_andnot_si64



// OR [|]
// __m64i_u8 _mm_or_pu8 (__m64i_u8 a)
// __m64i_i8 _mm_or_pi8 (__m64i_i8 a)
// __m64i_u16 _mm_or_pu16 (__m64i_u16 a)
// __m64i_i16 _mm_or_pi16 (__m64i_i16 a)
// __m64i_u32 _mm_or_pu32 (__m64i_u32 a)
// __m64i_i32 _mm_or_pi32 (__m64i_i32 a)
// __m64i_u64 _mm_or_pu64 (__m64i_u64 a)
// __m64i_i64 _mm_or_pi64 (__m64i_i64 a)

#define _mm_or_pu8 _mm_or_si64
#define _mm_or_pi8 _mm_or_si64
#define _mm_or_pu16 _mm_or_si64
#define _mm_or_pi16 _mm_or_si64
#define _mm_or_pu32 _mm_or_si64
#define _mm_or_pi32 _mm_or_si64
#define _mm_or_pu64 _mm_or_si64
#define _mm_or_pi64 _mm_or_si64


// XOR [^]
// __m64i_u8 _mm_xor_pu8 (__m64i_u8 a)
// __m64i_i8 _mm_xor_pi8 (__m64i_i8 a)
// __m64i_u16 _mm_xor_pu16 (__m64i_u16 a)
// __m64i_i16 _mm_xor_pi16 (__m64i_i16 a)
// __m64i_u32 _mm_xor_pu32 (__m64i_u32 a)
// __m64i_i32 _mm_xor_pi32 (__m64i_i32 a)
// __m64i_u64 _mm_xor_pu64 (__m64i_u64 a)
// __m64i_i64 _mm_xor_pi64 (__m64i_i64 a)

#define _mm_xor_pu8 _mm_xor_si64
#define _mm_xor_pi8 _mm_xor_si64
#define _mm_xor_pu16 _mm_xor_si64
#define _mm_xor_pi16 _mm_xor_si64
#define _mm_xor_pu32 _mm_xor_si64
#define _mm_xor_pi32 _mm_xor_si64
#define _mm_xor_pu64 _mm_xor_si64
#define _mm_xor_pi64 _mm_xor_si64

#endif

#endif


#if !ARM
/***************************************************************************************************/
// Portable SSE Bitwise Functions
/***************************************************************************************************/
// Use these functions instead of _mm_not_si128 to ensure portability!

// NOT [!]
// __m128i_u8 _mm_not_epu8 (__m128i_u8 a)
// __m128i_i8 _mm_not_epi8 (__m128i_i8 a)
// __m128i_u16 _mm_not_epu16 (__m128i_u16 a)
// __m128i_i16 _mm_not_epi16 (__m128i_i16 a)
// __m128i_u32 _mm_not_epu32 (__m128i_u32 a)
// __m128i_i32 _mm_not_epi32 (__m128i_i32 a)
// __m128i_u64 _mm_not_epu64 (__m128i_u64 a)
// __m128i_i64 _mm_not_epi64 (__m128i_i64 a)

#define _mm_not_epu8 _mm_not_si128
#define _mm_not_epi8 _mm_not_si128
#define _mm_not_epu16 _mm_not_si128
#define _mm_not_epi16 _mm_not_si128
#define _mm_not_epu32 _mm_not_si128
#define _mm_not_epi32 _mm_not_si128
#define _mm_not_epu64 _mm_not_si128
#define _mm_not_epi64 _mm_not_si128


// AND [&]
// __m128i_u8 _mm_and_epu8 (__m128i_u8 a)
// __m128i_i8 _mm_and_epi8 (__m128i_i8 a)
// __m128i_u16 _mm_and_epu16 (__m128i_u16 a)
// __m128i_i16 _mm_and_epi16 (__m128i_i16 a)
// __m128i_u32 _mm_and_epu32 (__m128i_u32 a)
// __m128i_i32 _mm_and_epi32 (__m128i_i32 a)
// __m128i_u64 _mm_and_epu64 (__m128i_u64 a)
// __m128i_i64 _mm_and_epi64 (__m128i_i64 a)

#define _mm_and_epu8 _mm_and_si128
#define _mm_and_epi8 _mm_and_si128
#define _mm_and_epu16 _mm_and_si128
#define _mm_and_epi16 _mm_and_si128
#define _mm_and_epu32 _mm_and_si128
#define _mm_and_epi32 _mm_and_si128
#define _mm_and_epu64 _mm_and_si128
#define _mm_and_epi64 _mm_and_si128



// ANDNOT [~a & b]
// __m128i_u8 _mm_andnot_epu8 (__m128i_u8 a)
// __m128i_i8 _mm_andnot_epi8 (__m128i_i8 a)
// __m128i_u16 _mm_andnot_epu16 (__m128i_u16 a)
// __m128i_i16 _mm_andnot_epi16 (__m128i_i16 a)
// __m128i_u32 _mm_andnot_epu32 (__m128i_u32 a)
// __m128i_i32 _mm_andnot_epi32 (__m128i_i32 a)
// __m128i_u64 _mm_andnot_epu64 (__m128i_u64 a)
// __m128i_i64 _mm_andnot_epi64 (__m128i_i64 a)

#define _mm_andnot_epu8 _mm_andnot_si128
#define _mm_andnot_epi8 _mm_andnot_si128
#define _mm_andnot_epu16 _mm_andnot_si128
#define _mm_andnot_epi16 _mm_andnot_si128
#define _mm_andnot_epu32 _mm_andnot_si128
#define _mm_andnot_epi32 _mm_andnot_si128
#define _mm_andnot_epu64 _mm_andnot_si128
#define _mm_andnot_epi64 _mm_andnot_si128


// OR [|]
// __m128i_u8 _mm_or_epu8 (__m128i_u8 a)
// __m128i_i8 _mm_or_epi8 (__m128i_i8 a)
// __m128i_u16 _mm_or_epu16 (__m128i_u16 a)
// __m128i_i16 _mm_or_epi16 (__m128i_i16 a)
// __m128i_u32 _mm_or_epu32 (__m128i_u32 a)
// __m128i_i32 _mm_or_epi32 (__m128i_i32 a)
// __m128i_u64 _mm_or_epu64 (__m128i_u64 a)
// __m128i_i64 _mm_or_epi64 (__m128i_i64 a)

#define _mm_or_epu8 _mm_or_si128
#define _mm_or_epi8 _mm_or_si128
#define _mm_or_epu16 _mm_or_si128
#define _mm_or_epi16 _mm_or_si128
#define _mm_or_epu32 _mm_or_si128
#define _mm_or_epi32 _mm_or_si128
#define _mm_or_epu64 _mm_or_si128
#define _mm_or_epi64 _mm_or_si128


// XOR [^]
// __m128i_u8 _mm_xor_epu8 (__m128i_u8 a)
// __m128i_i8 _mm_xor_epi8 (__m128i_i8 a)
// __m128i_u16 _mm_xor_epu16 (__m128i_u16 a)
// __m128i_i16 _mm_xor_epi16 (__m128i_i16 a)
// __m128i_u32 _mm_xor_epu32 (__m128i_u32 a)
// __m128i_i32 _mm_xor_epi32 (__m128i_i32 a)
// __m128i_u64 _mm_xor_epu64 (__m128i_u64 a)
// __m128i_i64 _mm_xor_epi64 (__m128i_i64 a)

#define _mm_xor_epu8 _mm_xor_si128
#define _mm_xor_epi8 _mm_xor_si128
#define _mm_xor_epu16 _mm_xor_si128
#define _mm_xor_epi16 _mm_xor_si128
#define _mm_xor_epu32 _mm_xor_si128
#define _mm_xor_epi32 _mm_xor_si128
#define _mm_xor_epu64 _mm_xor_si128
#define _mm_xor_epi64 _mm_xor_si128


#endif


/***************************************************************************************************/
// BIT SHIFTING
/***************************************************************************************************/
#if !ARM
__forceinline __m128i_i8 _mm_slli_epi8(__m128i_i8 a, const int imm8) {
	return _mm_and_si128(_mm_slli_epi16(a, imm8), _mm_set1_epi8((__uint8)(0xFFU << (imm8 & 15))));
}
#define _mm_slli_epu8 _mm_slli_epi8

#if BIT64
__forceinline __m128i_i8 _mm_sll_epi8(__m128i_i8 a, __m128i_i8 b) {
	return _mm_and_si128(_mm_sll_epi16(a, b), _mm_set1_epi8(0xFFU << _mm_cvtsi128_si64(b)));
}
#define _mm_sll_epu8 _mm_sll_epi8
#endif

__forceinline __m128i_i8 _mm_srli_epi8(__m128i_i8 a, const int b) {
	return _mm_and_si128(_mm_srli_epi16(a, b), _mm_set1_epi8(0xFFU >> b));
}
#define _mm_srli_epu8 _mm_srli_epi8

#if BIT64
__forceinline __m128i_i8 _mm_srl_epi8(__m128i_i8 a, __m128i_i8 b) {
	return _mm_and_si128(_mm_srl_epi16(a, b), _mm_set1_epi8(0xFFU >> _mm_cvtsi128_si64(b)));
}
#define _mm_srl_epu8 _mm_srl_epi8
#endif

__forceinline __m128i_i8 _mm_srai_epi8(__m128i_i8 a, const int b) {
	return _mm_packs_epi16( _mm_srai_epi16(_mm_unpacklo_epi8(a, a), 8 + b), _mm_srai_epi16(_mm_unpackhi_epi8(a, a), 8 + b));
}
#define _mm_srai_epu8 _mm_srai_epi8

__forceinline __m128i_i8 _mm_sra_epi8(__m128i_i8 a, __m128i_i8 b) {
	return _mm_packs_epi16( _mm_sra_epi16(_mm_cvtepi8_epi16(a), b), _mm_sra_epi16(_mm_cvtepi8_epi16(_mm_srli_si128(a, 8)), b));
}
#define _mm_sra_epu8 _mm_sra_epi8

//IMPLEMENTED BY SSE2
//__m128i_i16 _mm_slli_epi16 (__m128i_i16 a, int imm8)
#define _mm_slli_epu16 _mm_slli_epi16
//__m128i_i32 _mm_slli_epi32 (__m128i_i32 a, int imm8)
#define _mm_slli_epu32 _mm_slli_epi32
//__m128i_i64 _mm_slli_epi64 (__m128i_i64 a, int imm8)
#define _mm_slli_epu64 _mm_slli_epi64


//__m128i_i16 _mm_sll_epi16 (__m128i_i16 a, __m128i_i16 b)
#define _mm_sll_epu16 _mm_sll_epi16
//__m128i_i32 _mm_sll_epi32 (__m128i_i32 a, __m128i_i32 b)
#define _mm_sll_epu32 _mm_sll_epi32
//__m128i_i64 _mm_sll_epi64 (__m128i_i64 a, __m128i_i64 b)
#define _mm_sll_epu64 _mm_sll_epi64


//__m128i_i16 _mm_srli_epi16 (__m128i_i16 a, int imm8)
#define _mm_srli_epu16 _mm_srli_epi16
//__m128i_i32 _mm_srli_epi32 (__m128i_i32 a, int imm8)
#define _mm_srli_epu32 _mm_srli_epi32
//__m128i_i64 _mm_srli_epi64 (__m128i_i64 a, int imm8)
#define _mm_srli_epu64 _mm_srli_epi64


//__m128i_i16 _mm_srl_epi16 (__m128i_i16 a, __m128i_i16 b)
#define _mm_srl_epu16 _mm_srl_epi16
//__m128i_i32 _mm_srl_epi32 (__m128i_i32 a, __m128i_i32 b)
#define _mm_srl_epu32 _mm_srl_epi32
//__m128i_i64 _mm_srl_epi64 (__m128i_i64 a, __m128i_i64 b)
#define _mm_srl_epu64 _mm_srl_epi64


//__m128i_i16 _mm_srai_epi16 (__m128i_i16 a, int imm8)
#define _mm_srai_epu16 _mm_srai_epi16
//__m128i_i32 _mm_srai_epi32 (__m128i_i32 a, int imm8)
#define _mm_srai_epu32 _mm_srai_epi32
//__m128i_i64 _mm_srai_epi64 (__m128i_i64 a, int imm8) //AVX-512
#define _mm_srai_epu64 _mm_srai_epi64


//__m128i_i16 _mm_sra_epi16 (__m128i_i16 a, __m128i_i16 b)
#define _mm_sra_epu16 _mm_sra_epi16
//__m128i_i32 _mm_sra_epi32 (__m128i_i32 a, __m128i_i32 b)
#define _mm_sra_epu32 _mm_sra_epi32
//__m128i_i64 _mm_sra_epi64 (__m128i_i64 a, __m128i_i64 b) //AVX-512
#define _mm_sra_epu64 _mm_sra_epi64

#endif

/***************************************************************************************************/
// ALTERNATE NAMES FOR BIT SHIFTING
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_bslli_epi8(__m128i_i8 a, int imm8) {
	return _mm_slli_epi8(a, imm8);
}
__forceinline __m128i_i16 _mm_bslli_epi16 (__m128i_i16 a, int imm8){
	return _mm_slli_epi16(a, imm8);
}
__forceinline __m128i_i32 _mm_bslli_epi32 (__m128i_i32 a, int imm8){
	return _mm_slli_epi32(a, imm8);
}
#if BIT64
__forceinline __m128i_i64 _mm_bslli_epi64 (__m128i_i64 a, int imm8){
	return _mm_slli_epi64(a, imm8);
}
// _mm_bslli_si128 implemented by SSE2
#endif

#if BIT64
__forceinline __m128i_i8 _mm_bsll_epi8 (__m128i_i8 a, __m128i_i8 b){
	return _mm_sll_epi8(a, b);
}
#endif
__forceinline __m128i_i16 _mm_bsll_epi16 (__m128i_i16 a, __m128i_i16 b){
	return _mm_sll_epi16(a, b);
}
__forceinline __m128i_i32 _mm_bsll_epi32 (__m128i_i32 a, __m128i_i32 b){
	return _mm_sll_epi32(a, b);
}
#if BIT64
__forceinline __m128i_i64 _mm_bsll_epi64 (__m128i_i64 a, __m128i_i64 b){
	return _mm_sll_epi64(a, b);
}
#endif


__forceinline __m128i_i8 _mm_bsrli_epi8(__m128i_i8 a, int imm8) {
	return _mm_srli_epi8(a, imm8);
}
__forceinline __m128i_i16 _mm_bsrli_epi16 (__m128i_i16 a, int imm8){
	return _mm_srli_epi16(a, imm8);
}
__forceinline __m128i_i32 _mm_bsrli_epi32 (__m128i_i32 a, int imm8){
	return _mm_srli_epi32(a, imm8);
}
#if BIT64
__forceinline __m128i_i64 _mm_bsrli_epi64 (__m128i_i64 a, int imm8){
	return _mm_srli_epi64(a, imm8);
}
// _mm_bsrli_si128 implemented by SSE2
#endif

#if BIT64
__forceinline __m128i_i8 _mm_bsrl_epi8(__m128i_i8 a, __m128i_i8 b) {
	return _mm_srl_epi8(a, b);
}
#endif
__forceinline __m128i_i16 _mm_bsrl_epi16 (__m128i_i16 a, __m128i_i16 b){
	return _mm_srl_epi16(a, b);
}
__forceinline __m128i_i32 _mm_bsrl_epi32 (__m128i_i32 a, __m128i_i32 b){
	return _mm_srl_epi32(a, b);
}
#if BIT64
__forceinline __m128i_i64 _mm_bsrl_epi64 (__m128i_i64 a, __m128i_i64 b){
	return _mm_srl_epi64(a, b);
}
#endif
