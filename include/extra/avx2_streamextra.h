/***************************************************************************************************/
// HEADER_NAME /extra/avx2_streamextra.h
/***************************************************************************************************/

/***************************************************************************************************/
// Portable Stream load
/***************************************************************************************************/

//AVX
#if __AVX2__
#define _mm256_stream_load_epu8 _mm256_stream_load_si256
#define _mm256_stream_load_epi8 _mm256_stream_load_si256
#define _mm256_stream_load_epu16 _mm256_stream_load_si256
#define _mm256_stream_load_epi16 _mm256_stream_load_si256
#define _mm256_stream_load_epu32 _mm256_stream_load_si256
#define _mm256_stream_load_epi32 _mm256_stream_load_si256
#define _mm256_stream_load_epu64 _mm256_stream_load_si256
#define _mm256_stream_load_epi64 _mm256_stream_load_si256
#endif


