/***************************************************************************************************/
// HEADER_NAME /extra/storeextra.h
/***************************************************************************************************/

#if !ARM
/***************************************************************************************************/
// Portable Store
/***************************************************************************************************/
#define _mm_store_epu8 _mm_store_si128
#define _mm_store_epi8 _mm_store_si128
#define _mm_store_epu16 _mm_store_si128
#define _mm_store_epi16 _mm_store_si128
#define _mm_store_epu32 _mm_store_si128
#define _mm_store_epi32 _mm_store_si128
#define _mm_store_epu64 _mm_store_si128
#define _mm_store_epi64 _mm_store_si128


/***************************************************************************************************/
// Portable Storeu
/***************************************************************************************************/
#define _mm_storeu_epu8 _mm_storeu_si128
#define _mm_storeu_epi8 _mm_storeu_si128
#define _mm_storeu_epu16 _mm_storeu_si128
#define _mm_storeu_epi16 _mm_storeu_si128
#define _mm_storeu_epu32 _mm_storeu_si128
#define _mm_storeu_epi32 _mm_storeu_si128
#define _mm_storeu_epu64 _mm_storeu_si128
#define _mm_storeu_epi64 _mm_storeu_si128

/***************************************************************************************************/
// Portable Storeu Void*
/***************************************************************************************************/

__forceinline void _mm_storeu_si8 (void* mem_addr, __m128i_i8 a)
{
	union __v128i
	{
		__m128i m128i;
		__int8 arr[16];
	};
	__v128i vec;
	vec.m128i = a;
	mem_addr = (void*)vec.arr;
}

#if !defined __INTEL_COMPILER
#if (_MSC_VER < 1900)
__forceinline void _mm_storeu_si16 (void* mem_addr, __m128i_i16 a)
{
	union __v128i
	{
		__m128i m128i;
		__int16 arr[8];
	};
	__v128i vec;
	vec.m128i = a;
	mem_addr = (void*)vec.arr;
}
__forceinline void _mm_storeu_si32 (void* mem_addr, __m128i_i32 a)
{
	union __v128i
	{
		__m128i m128i;
		__int32 arr[4];
	};
	__v128i vec;
	vec.m128i = a;
	mem_addr = (void*)vec.arr;
}
__forceinline void _mm_storeu_si64 (void* mem_addr, __m128i_i64 a)
{
	union __v128i
	{
		__m128i m128i;
		__int64 arr[2];
	};
	__v128i vec;
	vec.m128i = a;
	mem_addr = (void*)vec.arr;
}
#endif
#endif

#define _mm_storeu_su8 _mm_storeu_si8
#define _mm_storeu_su16 _mm_storeu_si16
#define _mm_storeu_su32 _mm_storeu_si32
#define _mm_storeu_su64 _mm_storeu_si64

#endif

