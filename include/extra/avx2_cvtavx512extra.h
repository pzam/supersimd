/***************************************************************************************************/
// HEADER_NAME /extra/avx2_cvtavx512extra.h
/***************************************************************************************************/
//AVX-512 Compatibility 

#if __AVX2__
//AVX
//You only care about values in the range: [-2^51, 2^51]
#undef _mm256_cvtepi64_pd
#define _mm256_cvtepi64_pd _mm256_cvtepi64_pd_impl
__forceinline __m256d _mm256_cvtepi64_pd_impl(__m256i_i64 a){
	a = _mm256_add_epi64(a, _mm256_castpd_epi64(__m256d_Int64ToDoubleMagic));
	return _mm256_sub_pd(_mm256_castepi64_pd(a), __m256d_Int64ToDoubleMagic);
}


//You only care about values in the range: [0, 2^52).
#undef _mm256_cvtepu64_pd
#define _mm256_cvtepu64_pd _mm256_cvtepu64_pd_impl
__forceinline __m256d _mm256_cvtepu64_pd_impl(__m256i_u64 a){
	a = _mm256_or_epu64(a, _mm256_castpd_epi64(__m256d_UInt64ToDoubleMagic));
	return _mm256_sub_pd(_mm256_castepi64_pd(a), __m256d_UInt64ToDoubleMagic);
}

//You only care about values in the range: [-2^51, 2^51]
#undef _mm256_cvtpd_epi64
#define _mm256_cvtpd_epi64 _mm256_cvtpd_epi64_impl
__forceinline __m256i_i64 _mm256_cvtpd_epi64_impl(__m256d a){
	a = _mm256_add_pd(a, __m256d_Int64ToDoubleMagic);
	return _mm256_sub_epi64( _mm256_castpd_epi64(a), _mm256_castpd_epi64(__m256d_Int64ToDoubleMagic));
}
#define _mm256_cvttpd_epi64 _mm256_cvtpd_epi64

//You only care about values in the range: [0, 2^52)
#undef _mm256_cvtpd_epu64
#define _mm256_cvtpd_epu64 _mm256_cvtpd_epu64_impl
__forceinline __m256i_u64 _mm256_cvtpd_epu64_impl(__m256d a){
	a = _mm256_add_pd(a, __m256d_UInt64ToDoubleMagic);
	return _mm256_xor_epu64( _mm256_castpd_epi64(a), _mm256_castpd_epi64(__m256d_UInt64ToDoubleMagic));
}
#define _mm256_cvttpd_epu64 _mm256_cvtpd_epu64
#endif

//MISSING
/*
__m128i _mm256_cvtepi16_epi8 (__m128i a)
__m128i _mm256_cvtepi32_epi16 (__m128i a)
__m128i _mm256_cvtepi32_epi8 (__m128i a)
__m128i _mm256_cvtepi64_epi16 (__m128i a)
__m128i _mm256_cvtepi64_epi32 (__m128i a)
__m128i _mm256_cvtepi64_epi8 (__m128i a)
__m128 _mm256_cvtepi64_ps (__m128i a)
__m128d _mm256_cvtepu32_pd (__m128i a)
__m128 _mm256_cvtepu64_ps (__m128i a)
*/


