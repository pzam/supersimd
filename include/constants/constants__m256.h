/***************************************************************************************************/
// HEADER_NAME /constants/constants__m256.h
/***************************************************************************************************/
#if __AVX__
static const __m256 __m256_minus_10 = _mm256_set1_ps(-10.0F);
static const __m256 __m256_minus_9 = _mm256_set1_ps(-9.0F);
static const __m256 __m256_minus_8 = _mm256_set1_ps(-8.0F);
static const __m256 __m256_minus_7 = _mm256_set1_ps(-7.0F);
static const __m256 __m256_minus_6 = _mm256_set1_ps(-6.0F);
static const __m256 __m256_minus_5 = _mm256_set1_ps(-5.0F);
static const __m256 __m256_minus_4 = _mm256_set1_ps(-4.0F);
static const __m256 __m256_minus_3 = _mm256_set1_ps(-3.0F);
static const __m256 __m256_minus_2 = _mm256_set1_ps(-2.0F);
static const __m256 __m256_minus_1 = _mm256_set1_ps(-1.0F);
static const __m256 __m256_1 = _mm256_set1_ps(1.0F);
static const __m256 __m256_2 = _mm256_set1_ps(2.0F);
static const __m256 __m256_3 = _mm256_set1_ps(3.0F);
static const __m256 __m256_4 = _mm256_set1_ps(4.0F);
static const __m256 __m256_5 = _mm256_set1_ps(5.0F);
static const __m256 __m256_6 = _mm256_set1_ps(6.0F);
static const __m256 __m256_7 = _mm256_set1_ps(7.0F);
static const __m256 __m256_8 = _mm256_set1_ps(8.0F);
static const __m256 __m256_9 = _mm256_set1_ps(9.0F);
static const __m256 __m256_10 = _mm256_set1_ps(10.0F);
static const __m256 __m256_1o2 = _mm256_set1_ps(0.5F);
static const __m256 __m256_1o3 = _mm256_set1_ps(0.33333333333F);

static const __m256 __m256_minus_PIo2F = _mm256_set1_ps(-1.57079632679490F);	//-M_PI / 2

static const __m256 __m256_PI = _mm256_set1_ps(3.14159265358979F);				// M_PI
static const __m256 __m256_SQRTPI = _mm256_set1_ps(1.77245385091F);				// SQRT(M_PI)
static const __m256 __m256_FoPI = _mm256_set1_ps(1.27323954473516F);			//4 / M_PI
static const __m256 __m256_PIo2F = _mm256_set1_ps(1.57079632679490F);			//M_PI / 2
static const __m256 __m256_PIo4F = _mm256_set1_ps(0.78539816339745F);			//M_PI / 4
static const __m256 __m256_TAN3PIo8 = _mm256_set1_ps( 2.414213562373095F);		//Tan( M_PI / 8)
static const __m256 __m256_TANPIo8 = _mm256_set1_ps( 0.4142135623730950F);		//Tan( 3 * M_PI / 8)


static const __m256 __m256_Deg2Rad = _mm256_set1_ps(0.01745329251F);			//M_PI/180
static const __m256 __m256_Rad2Deg = _mm256_set1_ps(57.2957795131F);			//180/M_PI

static const __m256 __m256_minus_DP1 = _mm256_set1_ps(-0.78515625F);
static const __m256 __m256_minus_DP2 = _mm256_set1_ps(-2.4187564849853515625e-4F);
static const __m256 __m256_minus_DP3 = _mm256_set1_ps(-3.77489497744594108e-8F);

static const __m256 __m256_sincof_p0 = _mm256_set1_ps(-1.9515295891E-4F);
static const __m256 __m256_sincof_p1 = _mm256_set1_ps( 8.3321608736E-3F);
static const __m256 __m256_sincof_p2 = _mm256_set1_ps(-1.6666654611E-1F);

static const __m256 __m256_coscof_p0 = _mm256_set1_ps( 2.443315711809948E-005F);
static const __m256 __m256_coscof_p1 = _mm256_set1_ps(-1.388731625493765E-003F);
static const __m256 __m256_coscof_p2 = _mm256_set1_ps( 4.166664568298827E-002F);

static const __m256 __m256_asinf_p0 = _mm256_set1_ps(4.2163199048E-2F);
static const __m256 __m256_asinf_p1 = _mm256_set1_ps(2.4181311049E-2F);
static const __m256 __m256_asinf_p2 = _mm256_set1_ps(4.5470025998E-2F);
static const __m256 __m256_asinf_p3 = _mm256_set1_ps(7.4953002686E-2F);
static const __m256 __m256_asinf_p4 = _mm256_set1_ps(1.6666752422E-1F);
static const __m256 __m256_asinf_p5 = _mm256_set1_ps(1.0E-4F);

static const __m256 __m256_pio2_hi = _mm256_set1_ps(1.57079637050628662109375F);
static const __m256 __m256_pio2_lo = _mm256_set1_ps(-4.37113900018624283e-8F);
static const __m256 __m256_pio4_hi = _mm256_set1_ps(0.785398185253143310546875F);

static const __m256 __m256_acos_p0 = _mm256_set1_ps(1.6666667163e-01F);
static const __m256 __m256_acos_p1 = _mm256_set1_ps(-3.2556581497e-01F);
static const __m256 __m256_acos_p2 = _mm256_set1_ps(2.0121252537e-01F);
static const __m256 __m256_acos_p3 = _mm256_set1_ps(-4.0055535734e-02F);
static const __m256 __m256_acos_p4 = _mm256_set1_ps(7.9153501429e-04F);
static const __m256 __m256_acos_p5 = _mm256_set1_ps(3.4793309169e-05F);

static const __m256 __m256_acos_q1 = _mm256_set1_ps(-2.4033949375e+00F);
static const __m256 __m256_acos_q2 = _mm256_set1_ps(2.0209457874e+00F);
static const __m256 __m256_acos_q3 = _mm256_set1_ps(-6.8828397989e-01F);
static const __m256 __m256_acos_q4 = _mm256_set1_ps(7.7038154006e-02F);

static const __m256 __m256_tanf_p0 = _mm256_set1_ps(9.38540185543E-3F);
static const __m256 __m256_tanf_p1 = _mm256_set1_ps(3.11992232697E-3F);
static const __m256 __m256_tanf_p2 = _mm256_set1_ps(2.44301354525E-2F);
static const __m256 __m256_tanf_p3 = _mm256_set1_ps(5.34112807005E-2F);
static const __m256 __m256_tanf_p4 = _mm256_set1_ps(1.33387994085E-1F);
static const __m256 __m256_tanf_p5 = _mm256_set1_ps(3.33331568548E-1F);

static const __m256 __m256_atanf_p0 = _mm256_set1_ps(8.05374449538e-2F);
static const __m256 __m256_atanf_p1 = _mm256_set1_ps(-1.38776856032e-1F);
static const __m256 __m256_atanf_p2 = _mm256_set1_ps(1.99777106478e-1F);
static const __m256 __m256_atanf_p3 = _mm256_set1_ps(-3.33329491539e-1F);


static const __m256 __m256_SQRTHF = _mm256_set1_ps(0.707106781186547524F);
static const __m256 __m256_log_p0 = _mm256_set1_ps(7.0376836292E-2F);
static const __m256 __m256_log_p1 = _mm256_set1_ps(-1.1514610310E-1F);
static const __m256 __m256_log_p2 = _mm256_set1_ps(1.1676998740E-1F);
static const __m256 __m256_log_p3 = _mm256_set1_ps(-1.2420140846E-1F);
static const __m256 __m256_log_p4 = _mm256_set1_ps(+1.4249322787E-1F);
static const __m256 __m256_log_p5 = _mm256_set1_ps(-1.6668057665E-1F);
static const __m256 __m256_log_p6 = _mm256_set1_ps(+2.0000714765E-1F);
static const __m256 __m256_log_p7 = _mm256_set1_ps(-2.4999993993E-1F);
static const __m256 __m256_log_p8 = _mm256_set1_ps(+3.3333331174E-1F);

static const __m256 __m256_log_q1 = _mm256_set1_ps(-2.12194440e-4F);
static const __m256 __m256_log_q2 = _mm256_set1_ps(0.693359375F);

static const __m256 __m256_exp_hi = _mm256_set1_ps(88.3762626647949F);
static const __m256 __m256_exp_lo = _mm256_set1_ps(-88.3762626647949F);

static const __m256 __m256_LOG2EF = _mm256_set1_ps(1.44269504088896341F);
static const __m256 __m256_LOG10EF = _mm256_set1_ps(0.4342944819032518F);

static const __m256 __m256_exp_C1 = _mm256_set1_ps(0.693359375F);
static const __m256 __m256_exp_C2 = _mm256_set1_ps(-2.12194440e-4F);

static const __m256 __m256_exp_p0 = _mm256_set1_ps(1.9875691500E-4F);
static const __m256 __m256_exp_p1 = _mm256_set1_ps(1.3981999507E-3F);
static const __m256 __m256_exp_p2 = _mm256_set1_ps(8.3334519073E-3F);
static const __m256 __m256_exp_p3 = _mm256_set1_ps(4.1665795894E-2F);
static const __m256 __m256_exp_p4 = _mm256_set1_ps(1.6666665459E-1F);
static const __m256 __m256_exp_p5 = _mm256_set1_ps(5.0000001201E-1F);

static const __m256 __m256_LOG2 = _mm256_set1_ps(0.69314718055994530942F);
static const __m256 __m256_LOG10 = _mm256_set1_ps(2.30258509299404568402F);

static const __m256 __m256_erf_p0 = _mm256_set1_ps(0.3275911F);
static const __m256 __m256_erf_p1 = _mm256_set1_ps(1.061405429F);
static const __m256 __m256_erf_p2 = _mm256_set1_ps(-1.453152027F);
static const __m256 __m256_erf_p3 = _mm256_set1_ps(1.421413741F);
static const __m256 __m256_erf_p4 = _mm256_set1_ps(-0.284496736F);
static const __m256 __m256_erf_p5 = _mm256_set1_ps(0.254829592F);

static const __m256  __m256_erfinv_a0 = _mm256_set1_ps(-0.140543331F);
static const __m256  __m256_erfinv_a1 = _mm256_set1_ps(0.914624893F);
static const __m256  __m256_erfinv_a2 = _mm256_set1_ps(-1.645349621F);
static const __m256  __m256_erfinv_a3 = _mm256_set1_ps(0.886226899F);

static const __m256  __m256_erfinv_b0 = _mm256_set1_ps(0.012229801F);
static const __m256  __m256_erfinv_b1 = _mm256_set1_ps(-0.329097515F);
static const __m256  __m256_erfinv_b2 = _mm256_set1_ps(1.442710462F);
static const __m256  __m256_erfinv_b3 = _mm256_set1_ps(-2.118377725F);

static const __m256  __m256_erfinv_c0 = _mm256_set1_ps(1.641345311F);
static const __m256  __m256_erfinv_c1 = _mm256_set1_ps(3.429567803F);
static const __m256  __m256_erfinv_c2 = _mm256_set1_ps(-1.62490649F);
static const __m256  __m256_erfinv_c3 = _mm256_set1_ps(-1.970840454F);

static const __m256  __m256_erfinv_d0 = _mm256_set1_ps(1.637067800F);
static const __m256  __m256_erfinv_d1 = _mm256_set1_ps(3.543889200F);
#endif

