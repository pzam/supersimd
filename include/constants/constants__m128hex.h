/***************************************************************************************************/
// HEADER_NAME /constants/constants__m128hex.h
/***************************************************************************************************/
static const __m128 __m128_min_norm_pos = _mm_set1_hex_ps(0x00800000);
static const __m128 __m128_mant_mask = _mm_set1_hex_ps(0x7F800000);
static const __m128 __m128_inv_mant_mask = _mm_set1_hex_ps(~0x7F800000);
static const __m128 __m128_sign_mask = _mm_set1_hex_ps(0x80000000);
static const __m128 __m128_inv_sign_mask = _mm_set1_hex_ps(~0x80000000);
static const __m128 __m128_maxint = _mm_set1_hex_ps(0xFFFFFFFF);			// NAN

static const __m128 __m128_asinf_trunc = _mm_set1_hex_ps(0xFFFFF000);
static const __m128 __m128_twoTo23 = _mm_set1_hex_ps(0x4B000000);

