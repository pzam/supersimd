/***************************************************************************************************/
// HEADER_NAME /constants/constants__m256hex.h
/***************************************************************************************************/
#if __AVX__
static const __m256 __m256_min_norm_pos = _mm256_set1_hex_ps(0x00800000);
static const __m256 __m256_mant_mask = _mm256_set1_hex_ps(0x7F800000);
static const __m256 __m256_inv_mant_mask = _mm256_set1_hex_ps(~0x7F800000);
static const __m256 __m256_sign_mask = _mm256_set1_hex_ps(0x80000000);
static const __m256 __m256_inv_sign_mask = _mm256_set1_hex_ps(~0x80000000);
static const __m256 __m256_maxint = _mm256_set1_hex_ps(0xFFFFFFFF);			// NAN

static const __m256 __m256_asinf_trunc = _mm256_set1_hex_ps(0xFFFFF000);
static const __m256 __m256_twoTo23 = _mm256_set1_hex_ps(0x4B000000);
#endif

