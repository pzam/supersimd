/***************************************************************************************************/
// HEADER_NAME /constants/constants__m256d.h
/***************************************************************************************************/
#if __AVX__
static const __m256d __m256d_minus_10 = _mm256_set1_pd(-10.0);
static const __m256d __m256d_minus_9 = _mm256_set1_pd(-9.0);
static const __m256d __m256d_minus_8 = _mm256_set1_pd(-8.0);
static const __m256d __m256d_minus_7 = _mm256_set1_pd(-7.0);
static const __m256d __m256d_minus_6 = _mm256_set1_pd(-6.0);
static const __m256d __m256d_minus_5 = _mm256_set1_pd(-5.0);
static const __m256d __m256d_minus_4 = _mm256_set1_pd(-4.0);
static const __m256d __m256d_minus_3 = _mm256_set1_pd(-3.0);
static const __m256d __m256d_minus_2 = _mm256_set1_pd(-2.0);
static const __m256d __m256d_minus_1 = _mm256_set1_pd(-1.0);
static const __m256d __m256d_1 = _mm256_set1_pd(1.0);
static const __m256d __m256d_2 = _mm256_set1_pd(2.0);
static const __m256d __m256d_3 = _mm256_set1_pd(3.0);
static const __m256d __m256d_4 = _mm256_set1_pd(4.0);
static const __m256d __m256d_5 = _mm256_set1_pd(5.0);
static const __m256d __m256d_6 = _mm256_set1_pd(6.0);
static const __m256d __m256d_7 = _mm256_set1_pd(7.0);
static const __m256d __m256d_8 = _mm256_set1_pd(8.0);
static const __m256d __m256d_9 = _mm256_set1_pd(9.0);
static const __m256d __m256d_10 = _mm256_set1_pd(10.0);
static const __m256d __m256d_1o2 = _mm256_set1_pd(0.5);
static const __m256d __m256d_1o3 = _mm256_set1_pd(0.33333333333);

/* the smallest non denormalized float number */

static const __m256d __m256d_minus_PIo2F = _mm256_set1_pd(-1.57079632679490);		//-M_PI / 2

static const __m256d __m256d_PI = _mm256_set1_pd(3.14159265358979);			// M_PI
static const __m256d __m256d_SQRTPI = _mm256_set1_pd(1.77245385091);		// SQRT(M_PI)
static const __m256d __m256d_FoPI = _mm256_set1_pd(1.27323954473516);		//4 / M_PI
static const __m256d __m256d_PIo2F = _mm256_set1_pd(1.57079632679490);		//M_PI / 2
static const __m256d __m256d_PIo4F = _mm256_set1_pd(0.78539816339745);		//M_PI / 4
static const __m256d __m256d_TAN3PIo8 = _mm256_set1_pd( 2.414213562373095);	//Tan( M_PI / 8)
static const __m256d __m256d_TANPIo8 = _mm256_set1_pd( 0.4142135623730950);	//Tan( 3 * M_PI / 8)


static const __m256d __m256d_Deg2Rad = _mm256_set1_pd(0.01745329251);	//M_PI/180
static const __m256d __m256d_Rad2Deg = _mm256_set1_pd(57.2957795131);	//180/M_PI

static const __m256d __m256d_minus_DP1 = _mm256_set1_pd(-0.78515625);
static const __m256d __m256d_minus_DP2 = _mm256_set1_pd(-2.4187564849853515625e-4);
static const __m256d __m256d_minus_DP3 = _mm256_set1_pd(-3.77489497744594108e-8);

static const __m256d __m256d_sincof_p0 = _mm256_set1_pd(-1.9515295891E-4);
static const __m256d __m256d_sincof_p1 = _mm256_set1_pd( 8.3321608736E-3);
static const __m256d __m256d_sincof_p2 = _mm256_set1_pd(-1.6666654611E-1);

static const __m256d __m256d_coscof_p0 = _mm256_set1_pd( 2.443315711809948E-005);
static const __m256d __m256d_coscof_p1 = _mm256_set1_pd(-1.388731625493765E-003);
static const __m256d __m256d_coscof_p2 = _mm256_set1_pd( 4.166664568298827E-002);

static const __m256d __m256d_asinf_p0 = _mm256_set1_pd(4.2163199048E-2);
static const __m256d __m256d_asinf_p1 = _mm256_set1_pd(2.4181311049E-2);
static const __m256d __m256d_asinf_p2 = _mm256_set1_pd(4.5470025998E-2);
static const __m256d __m256d_asinf_p3 = _mm256_set1_pd(7.4953002686E-2);
static const __m256d __m256d_asinf_p4 = _mm256_set1_pd(1.6666752422E-1);
static const __m256d __m256d_asinf_p5 = _mm256_set1_pd(1.0E-4);

static const __m256d __m256d_pio2_hi = _mm256_set1_pd(1.57079637050628662109375);  /* 0x3fc90fdb */
static const __m256d __m256d_pio2_lo = _mm256_set1_pd(-4.37113900018624283e-8);    /* 0xb33bbd2e */
static const __m256d __m256d_pio4_hi = _mm256_set1_pd(0.785398185253143310546875); /* 0x3f490fdb */

static const __m256d __m256d_acos_p0 = _mm256_set1_pd(1.6666667163e-01); /* 0x3e2aaaab */
static const __m256d __m256d_acos_p1 = _mm256_set1_pd(-3.2556581497e-01); /* 0xbea6b090 */
static const __m256d __m256d_acos_p2 = _mm256_set1_pd(2.0121252537e-01); /* 0x3e4e0aa8 */
static const __m256d __m256d_acos_p3 = _mm256_set1_pd(-4.0055535734e-02); /* 0xbd241146 */
static const __m256d __m256d_acos_p4 = _mm256_set1_pd(7.9153501429e-04); /* 0x3a4f7f04 */
static const __m256d __m256d_acos_p5 = _mm256_set1_pd(3.4793309169e-05); /* 0x3811ef08 */

static const __m256d __m256d_acos_q1 = _mm256_set1_pd(-2.4033949375e+00); /* 0xc019d139 */
static const __m256d __m256d_acos_q2 = _mm256_set1_pd(2.0209457874e+00); /* 0x4001572d */
static const __m256d __m256d_acos_q3 = _mm256_set1_pd(-6.8828397989e-01); /* 0xbf303361 */
static const __m256d __m256d_acos_q4 = _mm256_set1_pd(7.7038154006e-02); /* 0x3d9dc62e */

static const __m256d __m256d_tanf_p0 = _mm256_set1_pd(9.38540185543E-3);
static const __m256d __m256d_tanf_p1 = _mm256_set1_pd(3.11992232697E-3);
static const __m256d __m256d_tanf_p2 = _mm256_set1_pd(2.44301354525E-2);
static const __m256d __m256d_tanf_p3 = _mm256_set1_pd(5.34112807005E-2);
static const __m256d __m256d_tanf_p4 = _mm256_set1_pd(1.33387994085E-1);
static const __m256d __m256d_tanf_p5 = _mm256_set1_pd(3.33331568548E-1);

static const __m256d __m256d_atanf_p0 = _mm256_set1_pd(8.05374449538e-2);
static const __m256d __m256d_atanf_p1 = _mm256_set1_pd(-1.38776856032e-1);
static const __m256d __m256d_atanf_p2 = _mm256_set1_pd(1.99777106478e-1);
static const __m256d __m256d_atanf_p3 = _mm256_set1_pd(-3.33329491539e-1);


static const __m256d __m256d_SQRTHF = _mm256_set1_pd(0.707106781186547524);
static const __m256d __m256d_log_p0 = _mm256_set1_pd(7.0376836292E-2);
static const __m256d __m256d_log_p1 = _mm256_set1_pd(-1.1514610310E-1);
static const __m256d __m256d_log_p2 = _mm256_set1_pd(1.1676998740E-1);
static const __m256d __m256d_log_p3 = _mm256_set1_pd(-1.2420140846E-1);
static const __m256d __m256d_log_p4 = _mm256_set1_pd(+1.4249322787E-1);
static const __m256d __m256d_log_p5 = _mm256_set1_pd(-1.6668057665E-1);
static const __m256d __m256d_log_p6 = _mm256_set1_pd(+2.0000714765E-1);
static const __m256d __m256d_log_p7 = _mm256_set1_pd(-2.4999993993E-1);
static const __m256d __m256d_log_p8 = _mm256_set1_pd(+3.3333331174E-1);

static const __m256d __m256d_log_q1 = _mm256_set1_pd(-2.12194440e-4);
static const __m256d __m256d_log_q2 = _mm256_set1_pd(0.693359375);

static const __m256d __m256d_exp_hi = _mm256_set1_pd(88.3762626647949);
static const __m256d __m256d_exp_lo = _mm256_set1_pd(-88.3762626647949);

static const __m256d __m256d_LOG2EF = _mm256_set1_pd(1.44269504088896341);
static const __m256d __m256d_LOG10EF = _mm256_set1_pd(0.4342944819032518);

static const __m256d __m256d_exp_C1 = _mm256_set1_pd(0.693359375);
static const __m256d __m256d_exp_C2 = _mm256_set1_pd(-2.12194440e-4);

static const __m256d __m256d_exp_p0 = _mm256_set1_pd(1.9875691500E-4);
static const __m256d __m256d_exp_p1 = _mm256_set1_pd(1.3981999507E-3);
static const __m256d __m256d_exp_p2 = _mm256_set1_pd(8.3334519073E-3);
static const __m256d __m256d_exp_p3 = _mm256_set1_pd(4.1665795894E-2);
static const __m256d __m256d_exp_p4 = _mm256_set1_pd(1.6666665459E-1);
static const __m256d __m256d_exp_p5 = _mm256_set1_pd(5.0000001201E-1);

static const __m256d __m256d_LOG2 = _mm256_set1_pd(0.69314718055994530942F);
static const __m256d __m256d_LOG10 = _mm256_set1_pd(2.30258509299404568402);

static const __m256d __m256d_erf_p0 = _mm256_set1_pd(0.3275911);
static const __m256d __m256d_erf_p1 = _mm256_set1_pd(1.061405429);
static const __m256d __m256d_erf_p2 = _mm256_set1_pd(-1.453152027);
static const __m256d __m256d_erf_p3 = _mm256_set1_pd(1.421413741);
static const __m256d __m256d_erf_p4 = _mm256_set1_pd(-0.284496736);
static const __m256d __m256d_erf_p5 = _mm256_set1_pd(0.254829592);

static const __m256d  __m256d_erfinv_a0 = _mm256_set1_pd(-0.140543331);
static const __m256d  __m256d_erfinv_a1 = _mm256_set1_pd(0.914624893);
static const __m256d  __m256d_erfinv_a2 = _mm256_set1_pd(-1.645349621);
static const __m256d  __m256d_erfinv_a3 = _mm256_set1_pd(0.886226899);

static const __m256d  __m256d_erfinv_b0 = _mm256_set1_pd(0.012229801);
static const __m256d  __m256d_erfinv_b1 = _mm256_set1_pd(-0.329097515);
static const __m256d  __m256d_erfinv_b2 = _mm256_set1_pd(1.442710462);
static const __m256d  __m256d_erfinv_b3 = _mm256_set1_pd(-2.118377725);

static const __m256d  __m256d_erfinv_c0 = _mm256_set1_pd(1.641345311);
static const __m256d  __m256d_erfinv_c1 = _mm256_set1_pd(3.429567803);
static const __m256d  __m256d_erfinv_c2 = _mm256_set1_pd(-1.62490649);
static const __m256d  __m256d_erfinv_c3 = _mm256_set1_pd(-1.970840454);

static const __m256d  __m256d_erfinv_d0 = _mm256_set1_pd(1.637067800);
static const __m256d  __m256d_erfinv_d1 = _mm256_set1_pd(3.543889200);
#endif

