/***************************************************************************************************/
// HEADER_NAME /constants/constants__m128dhex.h
/***************************************************************************************************/
#if BIT64
static const __m128d __m128d_min_norm_pos = _mm_set1_hex_pd(0x0080000000000000);
static const __m128d __m128d_mant_mask = _mm_set1_hex_pd(0x7F80000000000000);
static const __m128d __m128d_inv_mant_mask = _mm_set1_hex_pd(~0x7F80000000000000);
static const __m128d __m128d_sign_mask = _mm_set1_hex_pd(0x8000000000000000);
static const __m128d __m128d_inv_sign_mask = _mm_set1_hex_pd(~0x8000000000000000);
static const __m128d __m128d_maxint = _mm_set1_hex_pd(0xFFFFFFFFFFFFFFFF);			// NAN

static const __m128d __m128d_asinf_trunc = _mm_set1_hex_pd(0xFFFFF00000000000);
static const __m128d __m128d_twoTo23 = _mm_set1_hex_pd(0x4B00000000000000);

//
//static const __m128d __m128d_Int64ToDoubleMagic = _mm_set1_pd(0x0018000000000000);	// 6.755399e+15
static const __m128d __m128d_Int64ToDoubleMagic = _mm_set1_hex_pd(0x4337FFFFE5B60600); //_mm_cvtepi64_pd

//static const __m128d __m128d_UInt64ToDoubleMagic = _mm_set1_pd(0x0010000000000000);	//4.503600e+15
static const __m128d __m128d_UInt64ToDoubleMagic = _mm_set1_hex_pd(0x433000001635E000);//_mm_cvtepu64_pd
#endif

