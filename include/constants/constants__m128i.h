/***************************************************************************************************/
// HEADER_NAME /constants/constants__m128i.h
/***************************************************************************************************/
//8bit
static const __m128i_u8 __m128i_u8_0xf0 = _mm_set1_hex_epu8(0xf0);
static const __m128i_u8 __m128i_u8_0x0f = _mm_set1_hex_epu8(0x0f);
static const __m128i_u8 __m128i_u8_0xcc = _mm_set1_hex_epu8(0xcc);
static const __m128i_u8 __m128i_u8_0x33 = _mm_set1_hex_epu8(0x33);
static const __m128i_u8 __m128i_u8_0xaa = _mm_set1_hex_epu8(0xaa);
static const __m128i_u8 __m128i_u8_0x55 = _mm_set1_hex_epu8(0x55);

static const __m128i_i8 __m128i_i8_1 = _mm_set1_epi8(1);
static const __m128i_i8 __m128i_i8_minus_1 = _mm_set1_epi8(-1);
static const __m128i_i8 __m128i_i8_2 = _mm_set1_epi8(2);
static const __m128i_i8 __m128i_i8_3 = _mm_set1_epi8(3);
static const __m128i_i8 __m128i_i8_4 = _mm_set1_epi8(4);
static const __m128i_i8 __m128i_i8_5 = _mm_set1_epi8(5);
static const __m128i_i8 __m128i_i8_6 = _mm_set1_epi8(6);
static const __m128i_i8 __m128i_i8_7 = _mm_set1_epi8(7);

static const __m128i_i8 __m128i_i8_16 = _mm_set1_epi8(16);

static const __m128i_i8 __m128i_i8_0xe0 = _mm_set1_hex_epi8(0xe0); //0b11100000

//16bit
static const __m128i_i16 __m128i_i16_1 = _mm_set1_epi16(1);
static const __m128i_i16 __m128i_i16_minus_1 = _mm_set1_epi16(-1);

static const __m128i_i16 __m128i_i16_0x00ff = _mm_set1_hex_epi16(0x00ff);
static const __m128i_i16 __m128i_i16_0xff00 = _mm_set1_hex_epi16(0xff00);

//32bit
static const __m128i_i32 __m128i_i32_1 = _mm_set1_epi32(1);
static const __m128i_i32 __m128i_i32_minus_1 = _mm_set1_epi32(-1);
static const __m128i_i32 __m128i_i32_inv1 = _mm_set1_epi32(~1);
static const __m128i_i32 __m128i_i32_2 = _mm_set1_epi32(2);
static const __m128i_i32 __m128i_i32_3 = _mm_set1_epi32(3);
static const __m128i_i32 __m128i_i32_4 = _mm_set1_epi32(4);
static const __m128i_i32 __m128i_i32_8 = _mm_set1_epi32(8);
static const __m128i_i32 __m128i_i32_16 = _mm_set1_epi32(16);
static const __m128i_i32 __m128i_i32_0x7f = _mm_set1_hex_epi32(0x7f);//127

static const __m128i_i32 __m128i_i32_max = _mm_set1_hex_epi32(0x7FFFFFFF);//2,147,483,647
static const __m128i_i32 __m128i_i32_min = _mm_set1_hex_epi32(0x80000000);//-2,147,483,648

#if BIT64
//64bit
static const __m128i_i64 __m128i_i64_1 = _mm_set1_epi64x(1);
static const __m128i_i64 __m128i_i64_minus_1 = _mm_set1_epi64x(-1);
static const __m128i_i64 __m128i_i64_inv1 = _mm_set1_epi64x(~1);
static const __m128i_i64 __m128i_i64_2 = _mm_set1_epi64x(2);
static const __m128i_i64 __m128i_i64_4 = _mm_set1_epi64x(4);
static const __m128i_i64 __m128i_i64_0x400 = _mm_set1_hex_epi64x(0x400);//1024


static const __m128i_i64 __m128i_i64_max = _mm_set1_hex_epi64x(0x7FFFFFFFFFFFFFFF);//9,223,372,036,854,775,807 
static const __m128i_i64 __m128i_i64_min = _mm_set1_hex_epi64x(0x8000000000000000);//-9,223,372,036,854,775,808
#endif

