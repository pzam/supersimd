/***************************************************************************************************/
// HEADER_NAME /avx2/no_avx2.h
/***************************************************************************************************/

//MISSING!!!
/*
// CAN NOT BE IMPLEMENTED PROPERLY!!! 

__m256i _mm256_alignr_epi8 (__m256i a, __m256i b, const int count)


__m256i _mm256_shuffle_epi8 (__m256i a, __m256i b)
__m256i _mm256_shuffle_epi32 (__m256i a, const int imm8)

__m256i _mm256_shufflehi_epi16 (__m256i a, const int imm8)
__m256i _mm256_shufflelo_epi16 (__m256i a, const int imm8)


__m128i _mm_broadcastb_epi8 (__m128i a)
__m256i _mm256_broadcastb_epi8 (__m128i a)
__m128i _mm_broadcastd_epi32 (__m128i a)
__m256i _mm256_broadcastd_epi32 (__m128i a)
__m128i _mm_broadcastq_epi64 (__m128i a)
__m256i _mm256_broadcastq_epi64 (__m128i a)
__m128d _mm_broadcastsd_pd (__m128d a)
__m256d _mm256_broadcastsd_pd (__m128d a)
__m256i _mm_broadcastsi128_si256 (__m128i a)
__m256i _mm256_broadcastsi128_si256 (__m128i a)
__m128 _mm_broadcastss_ps (__m128 a)
__m256 _mm256_broadcastss_ps (__m128 a)
__m128i _mm_broadcastw_epi16 (__m128i a)
__m256i _mm256_broadcastw_epi16 (__m128i a)




__m128i _mm_i32gather_epi32 (int const* base_addr, __m128i vindex, const int scale)
__m128i _mm_mask_i32gather_epi32 (__m128i src, int const* base_addr, __m128i vindex, __m128i mask, const int scale)
__m256i _mm256_i32gather_epi32 (int const* base_addr, __m256i vindex, const int scale)
__m256i _mm256_mask_i32gather_epi32 (__m256i src, int const* base_addr, __m256i vindex, __m256i mask, const int scale)
__m128i _mm_i32gather_epi64 (__int64 const* base_addr, __m128i vindex, const int scale)
__m128i _mm_mask_i32gather_epi64 (__m128i src, __int64 const* base_addr, __m128i vindex, __m128i mask, const int scale)
__m256i _mm256_i32gather_epi64 (__int64 const* base_addr, __m128i vindex, const int scale)
__m256i _mm256_mask_i32gather_epi64 (__m256i src, __int64 const* base_addr, __m128i vindex, __m256i mask, const int scale)
__m128d _mm_i32gather_pd (double const* base_addr, __m128i vindex, const int scale)
__m128d _mm_mask_i32gather_pd (__m128d src, double const* base_addr, __m128i vindex, __m128d mask, const int scale)
__m256d _mm256_i32gather_pd (double const* base_addr, __m128i vindex, const int scale)
__m256d _mm256_mask_i32gather_pd (__m256d src, double const* base_addr, __m128i vindex, __m256d mask, const int scale)
__m128 _mm_i32gather_ps (float const* base_addr, __m128i vindex, const int scale)
__m128 _mm_mask_i32gather_ps (__m128 src, float const* base_addr, __m128i vindex, __m128 mask, const int scale)
__m256 _mm256_i32gather_ps (float const* base_addr, __m256i vindex, const int scale)
__m256 _mm256_mask_i32gather_ps (__m256 src, float const* base_addr, __m256i vindex, __m256 mask, const int scale)
__m128i _mm_i64gather_epi32 (int const* base_addr, __m128i vindex, const int scale)
__m128i _mm_mask_i64gather_epi32 (__m128i src, int const* base_addr, __m128i vindex, __m128i mask, const int scale)
__m128i _mm256_i64gather_epi32 (int const* base_addr, __m256i vindex, const int scale)
__m128i _mm256_mask_i64gather_epi32 (__m128i src, int const* base_addr, __m256i vindex, __m128i mask, const int scale)
__m128i _mm_i64gather_epi64 (__int64 const* base_addr, __m128i vindex, const int scale)
__m128i _mm_mask_i64gather_epi64 (__m128i src, __int64 const* base_addr, __m128i vindex, __m128i mask, const int scale)
__m256i _mm256_i64gather_epi64 (__int64 const* base_addr, __m256i vindex, const int scale)
__m256i _mm256_mask_i64gather_epi64 (__m256i src, __int64 const* base_addr, __m256i vindex, __m256i mask, const int scale)
__m128d _mm_i64gather_pd (double const* base_addr, __m128i vindex, const int scale)
__m128d _mm_mask_i64gather_pd (__m128d src, double const* base_addr, __m128i vindex, __m128d mask, const int scale)
__m256d _mm256_i64gather_pd (double const* base_addr, __m256i vindex, const int scale)
__m256d _mm256_mask_i64gather_pd (__m256d src, double const* base_addr, __m256i vindex, __m256d mask, const int scale)
__m128 _mm_i64gather_ps (float const* base_addr, __m128i vindex, const int scale)
__m128 _mm_mask_i64gather_ps (__m128 src, float const* base_addr, __m128i vindex, __m128 mask, const int scale)
__m128 _mm256_i64gather_ps (float const* base_addr, __m256i vindex, const int scale)
__m128 _mm256_mask_i64gather_ps (__m128 src, float const* base_addr, __m256i vindex, __m128 mask, const int scale)





// Needs RE-Thinking

__m128i _mm_maskload_epi32 (int const* mem_addr, __m128i mask)
__m256i _mm256_maskload_epi32 (int const* mem_addr, __m256i mask)
__m128i _mm_maskload_epi64 (__int64 const* mem_addr, __m128i mask)
__m256i _mm256_maskload_epi64 (__int64 const* mem_addr, __m256i mask)

void _mm_maskstore_epi32 (int* mem_addr, __m128i mask, __m128i a)
void _mm256_maskstore_epi32 (int* mem_addr, __m256i mask, __m256i a)
void _mm_maskstore_epi64 (__int64* mem_addr, __m128i mask, __m128i a)
void _mm256_maskstore_epi64 (__int64* mem_addr, __m256i mask, __m256i a)


__m256i _mm256_madd_epi16 (__m256i a, __m256i b)
__m256i _mm256_maddubs_epi16 (__m256i a, __m256i b)
__m256i _mm256_mpsadbw_epu8 (__m256i a, __m256i b, const int imm8)


__m128i _mm_blend_epi32 (__m128i a, __m128i b, const int imm8) // Will be Implemented in blendextra.h

__m256i _mm256_blend_epi16 (__m256i a, __m256i b, const int imm8)
__m256i _mm256_blend_epi32 (__m256i a, __m256i b, const int imm8)

*/

/***************************************************************************************************/
// Backport Method of AVX2 to SSE, 256-bit SIMD operation is Split into 2x 128-bit SIMD operations. 
/***************************************************************************************************/
/*
__m256i avx2_funtion_backport (__m256i a, __m256i b)
{
	
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_xxx_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_xxx_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
*/
/***************************************************************************************************/

#if __AVX__
#define __AVX2__ 1
/***************************************************************************************************/
//Load/Store
/***************************************************************************************************/
//__m256i _mm256_stream_load_si256 (__m256i const* mem_addr)
#define _mm256_stream_load_si256 _mm256_lddqu_si256

/***************************************************************************************************/
//Extract
/***************************************************************************************************/
//__m128i _mm256_extracti128_si256 (__m256i a, const int imm8)
#undef _mm256_extracti128_si256
#define _mm256_extracti128_si256 _mm256_extractf128_si256

//int _mm256_extract_epi8 (__m256i a, const int index)
#undef _mm256_extract_epi8
#define _mm256_extract_epi8(a,index) (index < 16) ? _mm_extract_epi8(_mm256_extractf128_si256 ( a, 0), index): _mm_extract_epi8(_mm256_extractf128_si256 ( a, 1), (index - 16) )

//int _mm256_extract_epi16 (__m256i a, const int index)
#undef _mm256_extract_epi16
#define _mm256_extract_epi16(a,index) (index < 8) ? _mm_extract_epi16(_mm256_extractf128_si256 ( a, 0), index): _mm_extract_epi16(_mm256_extractf128_si256 ( a, 1), (index - 8) )


//double _mm256_cvtsd_f64 (__m256d a)
#undef _mm256_cvtsd_f64
#define _mm256_cvtsd_f64(a) _mm_cvtsd_f64(_mm256_extractf128_pd( a, 0))

//int _mm256_cvtsi256_si32 (__m256i a)
#undef _mm256_cvtsi256_si32
#define _mm256_cvtsi256_si32(a) _mm_cvtsi128_si32(_mm256_extractf128_si256( a, 0))
/***************************************************************************************************/
//Insert
/***************************************************************************************************/

//__m256i _mm256_inserti128_si256 (__m256i a, __m128i b, const int imm8)
#undef _mm256_inserti128_si256
#define _mm256_inserti128_si256 (a, b, imm8) (imm8 < 1) ? _mm256_set_m128i(_mm256_extractf128_si256(a, 1),b): _mm256_set_m128i(b,_mm256_extractf128_si256(a, 0))

/***************************************************************************************************/
//Blend
/***************************************************************************************************/

#undef _mm256_blendv_epi8
#define _mm256_blendv_epi8 _mm256_blendv_epi8_impl
__forceinline __m256i _mm256_blendv_epi8 (__m256i a, __m256i b, __m256i mask)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	__m128i elo_mask = _mm256_extractf128_si256 ( mask, 0);
	__m128i ehi_mask = _mm256_extractf128_si256 ( mask, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_blendv_epi8 (elo_a, elo_b, elo_mask);
	__m128i ehi_r = _mm_blendv_epi8 (ehi_a, ehi_b, ehi_mask);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Movemask
/***************************************************************************************************/

#undef _mm256_movemask_epi8
#define _mm256_movemask_epi8 _mm256_movemask_epi8_impl
__forceinline int _mm256_movemask_epi8_impl(__m256i a)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	int elo_r = _mm_movemask_epi8 (elo_a);
	int ehi_r = _mm_movemask_epi8 (ehi_a);
	

	return elo_r | ehi_r;
}

/***************************************************************************************************/
//Addition
/***************************************************************************************************/
#undef _mm256_add_epi8
#define _mm256_add_epi8 _mm256_add_epi8_impl
__forceinline __m256i _mm256_add_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_add_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_add_epi8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_add_epi16
#define _mm256_add_epi16 _mm256_add_epi16_impl
__forceinline __m256i _mm256_add_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_add_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_add_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_add_epi32
#define _mm256_add_epi32 _mm256_add_epi32_impl
__forceinline __m256i _mm256_add_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_add_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_add_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_add_epi64
#define _mm256_add_epi64 _mm256_add_epi64_impl
__forceinline __m256i _mm256_add_epi64_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_add_epi64 (elo_a, elo_b);
	__m128i ehi_r = _mm_add_epi64 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}


#undef _mm256_adds_epi8
#define _mm256_adds_epi8 _mm256_adds_epi8_impl
__forceinline __m256i _mm256_adds_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_adds_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_adds_epi8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_adds_epu8
#define _mm256_adds_epu8 _mm256_adds_epu8_impl
__forceinline __m256i _mm256_adds_epu8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_adds_epu8 (elo_a, elo_b);
	__m128i ehi_r = _mm_adds_epu8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_adds_epi16
#define _mm256_adds_epi16 _mm256_adds_epi16_impl
__forceinline __m256i _mm256_adds_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_adds_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_adds_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_adds_epu16
#define _mm256_adds_epu16 _mm256_adds_epu16_impl
__forceinline __m256i _mm256_adds_epu16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_adds_epu16 (elo_a, elo_b);
	__m128i ehi_r = _mm_adds_epu16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_hadd_epi16
#define _mm256_hadd_epi16 _mm256_hadd_epi16_impl
__forceinline __m256i _mm256_hadd_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_hadd_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_hadd_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_hadds_epi16
#define _mm256_hadds_epi16 _mm256_hadds_epi16_impl
__forceinline __m256i _mm256_hadds_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_hadds_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_hadds_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_hadd_epi32
#define _mm256_hadd_epi32 _mm256_hadd_epi32_impl
__forceinline __m256i _mm256_hadds_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_hadd_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_hadd_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Subtraction
/***************************************************************************************************/
#undef _mm256_sub_epi8
#define _mm256_sub_epi8 _mm256_sub_epi8_impl
__forceinline __m256i _mm256_sub_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sub_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_sub_epi8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_sub_epi16
#define _mm256_sub_epi16 _mm256_sub_epi16_impl
__forceinline __m256i _mm256_sub_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sub_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_sub_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_sub_epi32
#define _mm256_sub_epi32 _mm256_sub_epi32_impl
__forceinline __m256i _mm256_sub_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sub_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_sub_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_sub_epi64
#define _mm256_sub_epi64 _mm256_sub_epi64_impl
__forceinline __m256i _mm256_sub_epi64_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sub_epi64 (elo_a, elo_b);
	__m128i ehi_r = _mm_sub_epi64 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}


#undef _mm256_subs_epi8
#define _mm256_subs_epi8 _mm256_subs_epi8_impl
__forceinline __m256i _mm256_subs_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_subs_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_subs_epi8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_subs_epu8
#define _mm256_subs_epu8 _mm256_subs_epu8_impl
__forceinline __m256i _mm256_subs_epu8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_subs_epu8 (elo_a, elo_b);
	__m128i ehi_r = _mm_subs_epu8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_subs_epi16
#define _mm256_subs_epi16 _mm256_subs_epi16_impl
__forceinline __m256i _mm256_subs_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_subs_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_subs_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_subs_epu16
#define _mm256_subs_epu16 _mm256_subs_epu16_impl
__forceinline __m256i _mm256_subs_epu16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_subs_epu16 (elo_a, elo_b);
	__m128i ehi_r = _mm_subs_epu16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_hsub_epi16
#define _mm256_hsub_epi16 _mm256_hsub_epi16_impl
__forceinline __m256i _mm256_hsub_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_hsub_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_hsub_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_hsubs_epi16
#define _mm256_hsubs_epi16 _mm256_hsubs_epi16_impl
__forceinline __m256i _mm256_hsubs_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_hsubs_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_hsubs_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_hsub_epi32
#define _mm256_hsub_epi32 _mm256_hsub_epi32_impl
__forceinline __m256i _mm256_hsubs_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_hsub_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_hsub_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
/***************************************************************************************************/
//Multiplication
/***************************************************************************************************/
#undef _mm256_mul_epi32
#define _mm256_mul_epi32 _mm256_mul_epi32_impl
__forceinline __m256i _mm256_mul_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_mul_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_mul_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_mul_epu32
#define _mm256_mul_epu32 _mm256_mul_epu32_impl
__forceinline __m256i _mm256_mul_epu32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_mul_epu32 (elo_a, elo_b);
	__m128i ehi_r = _mm_mul_epu32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_mulhi_epi16
#define _mm256_mulhi_epi16 _mm256_mulhi_epi16_impl
__forceinline __m256i _mm256_mulhi_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_mulhi_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_mulhi_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_mulhi_epu16
#define _mm256_mulhi_epu16 _mm256_mulhi_epu16_impl
__forceinline __m256i _mm256_mulhi_epu16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_mulhi_epu16 (elo_a, elo_b);
	__m128i ehi_r = _mm_mulhi_epu16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_mulhrs_epi16
#define _mm256_mulhrs_epi16 _mm256_mulhrs_epi16_impl
__forceinline __m256i _mm256_mulhrs_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_mulhrs_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_mulhrs_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_mullo_epi16
#define _mm256_mullo_epi16 _mm256_mullo_epi16_impl
__forceinline __m256i _mm256_mullo_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_mullo_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_mullo_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_mullo_epi32
#define _mm256_mullo_epi32 _mm256_mullo_epi32_impl
__forceinline __m256i _mm256_mullo_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_mullo_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_mullo_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Absolute Value
/***************************************************************************************************/

#undef _mm256_sad_epu8
#define _mm256_sad_epu8 _mm256_sad_epu8_impl
__forceinline __m256i _mm256_sad_epu8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sad_epu8 (elo_a, elo_b);
	__m128i ehi_r = _mm_sad_epu8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_abs_epi8
#define _mm256_abs_epi8 _mm256_abs_epi8_impl
__forceinline __m256i _mm256_abs_epi8_impl(__m256i a)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_abs_epi8 (elo_a);
	__m128i ehi_r = _mm_abs_epi8 (ehi_a);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_abs_epi16
#define _mm256_abs_epi16 _mm256_abs_epi16_impl
__forceinline __m256i _mm256_abs_epi16_impl(__m256i a)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_abs_epi16 (elo_a);
	__m128i ehi_r = _mm_abs_epi16 (ehi_a);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_abs_epi32
#define _mm256_abs_epi32 _mm256_abs_epi32_impl
__forceinline __m256i _mm256_abs_epi32_impl(__m256i a)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_abs_epi32 (elo_a);
	__m128i ehi_r = _mm_abs_epi32 (ehi_a);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Sign
/***************************************************************************************************/
#undef _mm256_sign_epi8
#define _mm256_sign_epi8 _mm256_sign_epi8_impl
__forceinline __m256i _mm256_sign_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sign_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_sign_epi8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_sign_epi16
#define _mm256_sign_epi16 _mm256_sign_epi16_impl
__forceinline __m256i _mm256_sign_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sign_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_sign_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_sign_epi32
#define _mm256_sign_epi32 _mm256_sign_epi32_impl
__forceinline __m256i _mm256_sign_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sign_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_sign_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Signed Max
/***************************************************************************************************/
#undef _mm256_max_epi8
#define _mm256_max_epi8 _mm256_max_epi8_impl
__forceinline __m256i _mm256_max_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_max_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_max_epi8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_max_epi16
#define _mm256_max_epi16 _mm256_max_epi16_impl
__forceinline __m256i _mm256_max_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_max_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_max_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_max_epi32
#define _mm256_max_epi32 _mm256_max_epi32_impl
__forceinline __m256i _mm256_max_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_max_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_max_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Unsigned Max
/***************************************************************************************************/
#undef _mm256_max_epu8
#define _mm256_max_epu8 _mm256_max_epu8_impl
__forceinline __m256i _mm256_max_epu8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_max_epu8 (elo_a, elo_b);
	__m128i ehi_r = _mm_max_epu8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_max_epu16
#define _mm256_max_epu16 _mm256_max_epu16_impl
__forceinline __m256i _mm256_max_epu16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_max_epu16 (elo_a, elo_b);
	__m128i ehi_r = _mm_max_epu16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_max_epu32
#define _mm256_max_epu32 _mm256_max_epu32_impl
__forceinline __m256i _mm256_max_epu32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_max_epu32 (elo_a, elo_b);
	__m128i ehi_r = _mm_max_epu32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Signed Min
/***************************************************************************************************/
#undef _mm256_min_epi8
#define _mm256_min_epi8 _mm256_min_epi8_impl
__forceinline __m256i _mm256_min_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_min_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_min_epi8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_min_epi16
#define _mm256_min_epi16 _mm256_min_epi16_impl
__forceinline __m256i _mm256_min_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_min_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_min_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_min_epi32
#define _mm256_min_epi32 _mm256_min_epi32_impl
__forceinline __m256i _mm256_min_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_min_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_min_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Unsigned Min
/***************************************************************************************************/
#undef _mm256_min_epu8
#define _mm256_min_epu8 _mm256_min_epu8_impl
__forceinline __m256i _mm256_min_epu8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_min_epu8 (elo_a, elo_b);
	__m128i ehi_r = _mm_min_epu8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_min_epu16
#define _mm256_min_epu16 _mm256_min_epu16_impl
__forceinline __m256i _mm256_min_epu16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_min_epu16 (elo_a, elo_b);
	__m128i ehi_r = _mm_min_epu16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_min_epu32
#define _mm256_min_epu32 _mm256_min_epu32_impl
__forceinline __m256i _mm256_min_epu32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_min_epu32 (elo_a, elo_b);
	__m128i ehi_r = _mm_min_epu32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Packs
/***************************************************************************************************/

#undef _mm256_packs_epi16
#define _mm256_packs_epi16 _mm256_packs_epi16_impl
__forceinline __m256i _mm256_packs_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_packs_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_packs_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_packs_epi32
#define _mm256_packs_epi32 _mm256_packs_epi32_impl
__forceinline __m256i _mm256_packs_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_packs_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_packs_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Packus
/***************************************************************************************************/

#undef _mm256_packus_epi16
#define _mm256_packus_epi16 _mm256_packus_epi16_impl
__forceinline __m256i _mm256_packus_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_packus_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_packus_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_packus_epi32
#define _mm256_packus_epi32 _mm256_packus_epi32_impl
__forceinline __m256i _mm256_packus_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_packus_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_packus_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Avg
/***************************************************************************************************/
#undef _mm256_avg_epu8
#define _mm256_avg_epu8 _mm256_avg_epu8_impl
__forceinline __m256i _mm256_avg_epu8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_avg_epu8 (elo_a, elo_b);
	__m128i ehi_r = _mm_avg_epu8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_avg_epu16
#define _mm256_avg_epu16 _mm256_avg_epu16_impl
__forceinline __m256i _mm256_avg_epu16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_avg_epu16 (elo_a, elo_b);
	__m128i ehi_r = _mm_avg_epu16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Compare Equal
/***************************************************************************************************/
#undef _mm256_cmpeq_epi8
#define _mm256_cmpeq_epi8 _mm256_cmpeq_epi8_impl
__forceinline __m256i _mm256_cmpeq_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_cmpeq_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_cmpeq_epi8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_cmpeq_epi16
#define _mm256_cmpeq_epi16 _mm256_cmpeq_epi16_impl
__forceinline __m256i _mm256_cmpeq_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_cmpeq_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_cmpeq_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_cmpeq_epi32
#define _mm256_cmpeq_epi32 _mm256_cmpeq_epi32_impl
__forceinline __m256i _mm256_cmpeq_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_cmpeq_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_cmpeq_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_cmpeq_epi64
#define _mm256_cmpeq_epi64 _mm256_cmpeq_epi64_impl
__forceinline __m256i _mm256_cmpeq_epi64_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_cmpeq_epi64 (elo_a, elo_b);
	__m128i ehi_r = _mm_cmpeq_epi64 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Compare Greater
/***************************************************************************************************/
#undef _mm256_cmpgt_epi8
#define _mm256_cmpgt_epi8 _mm256_cmpgt_epi8_impl
__forceinline __m256i _mm256_cmpgt_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_cmpgt_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_cmpgt_epi8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_cmpgt_epi16
#define _mm256_cmpgt_epi16 _mm256_cmpgt_epi16_impl
__forceinline __m256i _mm256_cmpgt_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_cmpgt_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_cmpgt_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_cmpgt_epi32
#define _mm256_cmpgt_epi32 _mm256_cmpgt_epi32_impl
__forceinline __m256i _mm256_cmpgt_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_cmpgt_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_cmpgt_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
#undef _mm256_cmpgt_epi64
#define _mm256_cmpgt_epi64 _mm256_cmpgt_epi64_impl
__forceinline __m256i _mm256_cmpgt_epi64_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_cmpgt_epi64 (elo_a, elo_b);
	__m128i ehi_r = _mm_cmpgt_epi64 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}


/***************************************************************************************************/
//Bitwise
/***************************************************************************************************/

#undef _mm256_and_si256
#define _mm256_and_si256 _mm256_and_si256_impl
__forceinline __m256i _mm256_and_si256_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_and_si128 (elo_a, elo_b);
	__m128i ehi_r = _mm_and_si128 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_andnot_si256
#define _mm256_andnot_si256 _mm256_andnot_si256_impl
__forceinline __m256i _mm256_andnot_si256_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_andnot_si128 (elo_a, elo_b);
	__m128i ehi_r = _mm_andnot_si128 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_or_si256
#define _mm256_or_si256 _mm256_or_si256_impl
__forceinline __m256i _mm256_or_si256_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_or_si128 (elo_a, elo_b);
	__m128i ehi_r = _mm_or_si128 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_xor_si256
#define _mm256_xor_si256 _mm256_xor_si256_impl
__forceinline __m256i _mm256_xor_si256_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_xor_si128 (elo_a, elo_b);
	__m128i ehi_r = _mm_xor_si128 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

/***************************************************************************************************/
//Bitwise Shifting
/***************************************************************************************************/

//__m128i _mm_sllv_epi32(__m128i a, __m128i count)
#undef _mm_sllv_epi32
#define _mm_sllv_epi32 _mm_sll_epi32

//__m128i _mm_sllv_epi64(__m128i a, __m128i count)
#undef _mm_sllv_epi64
#define _mm_sllv_epi64 _mm_sll_epi64

//__m128i _mm_srav_epi32(__m128i a, __m128i count)
#undef _mm_srav_epi32
#define _mm_srav_epi32 _mm_sra_epi32

//__m128i _mm_srlv_epi32(__m128i a, __m128i count)
#undef _mm_srlv_epi32
#define _mm_srlv_epi32 _mm_srl_epi32

//__m128i _mm_srlv_epi64(__m128i a, __m128i count)
#undef _mm_srlv_epi64
#define _mm_srlv_epi64 _mm_srl_epi64

#undef _mm256_sllv_epi32
#define _mm256_sllv_epi32 _mm256_sllv_epi32_impl
__m256i _mm256_sllv_epi32_impl(__m256i a, __m256i count)
{
	__m128i elo_a = _mm256_extractf128_si256(a, 0);
	__m128i ehi_a = _mm256_extractf128_si256(a, 1);

	__m128i elo_b = _mm256_extractf128_si256(count, 0);
	__m128i ehi_b = _mm256_extractf128_si256(count, 1);

	//MAIN OPERATION
	__m128i elo_r = _mm_sll_epi32(elo_a, elo_b);
	__m128i ehi_r = _mm_sll_epi32(ehi_a, ehi_b);

	return _mm256_set_m128i(ehi_r, elo_r);
}

#undef _mm256_sllv_epi64
#define _mm256_sllv_epi64 _mm256_sllv_epi64_impl
__m256i _mm256_sllv_epi64_impl(__m256i a, __m256i count)
{
	__m128i elo_a = _mm256_extractf128_si256(a, 0);
	__m128i ehi_a = _mm256_extractf128_si256(a, 1);

	__m128i elo_b = _mm256_extractf128_si256(count, 0);
	__m128i ehi_b = _mm256_extractf128_si256(count, 1);

	//MAIN OPERATION
	__m128i elo_r = _mm_sll_epi64(elo_a, elo_b);
	__m128i ehi_r = _mm_sll_epi64(ehi_a, ehi_b);

	return _mm256_set_m128i(ehi_r, elo_r);
}

#undef _mm256_srav_epi32
#define _mm256_srav_epi32 _mm256_srav_epi32_impl
__m256i _mm256_srav_epi32_impl(__m256i a, __m256i count)
{
	__m128i elo_a = _mm256_extractf128_si256(a, 0);
	__m128i ehi_a = _mm256_extractf128_si256(a, 1);

	__m128i elo_b = _mm256_extractf128_si256(count, 0);
	__m128i ehi_b = _mm256_extractf128_si256(count, 1);

	//MAIN OPERATION
	__m128i elo_r = _mm_sra_epi32(elo_a, elo_b);
	__m128i ehi_r = _mm_sra_epi32(ehi_a, ehi_b);

	return _mm256_set_m128i(ehi_r, elo_r);
}

#undef _mm256_srlv_epi32
#define _mm256_srlv_epi32 _mm256_srlv_epi32_impl
__m256i _mm256_srlv_epi32_impl(__m256i a, __m256i count)
{
	__m128i elo_a = _mm256_extractf128_si256(a, 0);
	__m128i ehi_a = _mm256_extractf128_si256(a, 1);

	__m128i elo_b = _mm256_extractf128_si256(count, 0);
	__m128i ehi_b = _mm256_extractf128_si256(count, 1);

	//MAIN OPERATION
	__m128i elo_r = _mm_srl_epi32(elo_a, elo_b);
	__m128i ehi_r = _mm_srl_epi32(ehi_a, ehi_b);

	return _mm256_set_m128i(ehi_r, elo_r);
}

#undef _mm256_srlv_epi64
#define _mm256_srlv_epi64 _mm256_srlv_epi64_impl
__m256i _mm256_srlv_epi64_impl(__m256i a, __m256i count)
{
	__m128i elo_a = _mm256_extractf128_si256(a, 0);
	__m128i ehi_a = _mm256_extractf128_si256(a, 1);

	__m128i elo_b = _mm256_extractf128_si256(count, 0);
	__m128i ehi_b = _mm256_extractf128_si256(count, 1);

	//MAIN OPERATION
	__m128i elo_r = _mm_srlv_epi64(elo_a, elo_b);
	__m128i ehi_r = _mm_srlv_epi64(ehi_a, ehi_b);

	return _mm256_set_m128i(ehi_r, elo_r);
}


#undef _mm256_sll_epi16
#define _mm256_sll_epi16 _mm256_sll_epi16_impl
__forceinline __m256i _mm256_sll_epi16_impl(__m256i a, __m128i count)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sll_epi16 (elo_a, count);
	__m128i ehi_r = _mm_sll_epi16 (ehi_a, count);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_sll_epi32
#define _mm256_sll_epi32 _mm256_sll_epi32_impl
__forceinline __m256i _mm256_sll_epi32_impl(__m256i a, __m128i count)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sll_epi32 (elo_a, count);
	__m128i ehi_r = _mm_sll_epi32 (ehi_a, count);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_sll_epi64
#define _mm256_sll_epi64 _mm256_sll_epi64_impl
__forceinline __m256i _mm256_sll_epi64_impl(__m256i a, __m128i count)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sll_epi64 (elo_a, count);
	__m128i ehi_r = _mm_sll_epi64 (ehi_a, count);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}


#undef _mm256_sra_epi16
#define _mm256_sra_epi16 _mm256_sra_epi16_impl
__forceinline __m256i _mm256_sra_epi16_impl(__m256i a, __m128i count)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sra_epi16 (elo_a, count);
	__m128i ehi_r = _mm_sra_epi16 (ehi_a, count);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_sra_epi32
#define _mm256_sra_epi32 _mm256_sra_epi32_impl
__forceinline __m256i _mm256_sra_epi32_impl(__m256i a, __m128i count)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_sra_epi32 (elo_a, count);
	__m128i ehi_r = _mm_sra_epi32 (ehi_a, count);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_srl_epi16
#define _mm256_srl_epi16 _mm256_srl_epi16_impl
__forceinline __m256i _mm256_srl_epi16_impl(__m256i a, __m128i count)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_srl_epi16 (elo_a, count);
	__m128i ehi_r = _mm_srl_epi16 (ehi_a, count);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_srl_epi32
#define _mm256_srl_epi32 _mm256_srl_epi32_impl
__forceinline __m256i _mm256_srl_epi32_impl(__m256i a, __m128i count)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_srl_epi32 (elo_a, count);
	__m128i ehi_r = _mm_srl_epi32 (ehi_a, count);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_srl_epi64
#define _mm256_srl_epi64 _mm256_srl_epi64_impl
__forceinline __m256i _mm256_srl_epi64_impl(__m256i a, __m128i count)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_srl_epi64 (elo_a, count);
	__m128i ehi_r = _mm_srl_epi64 (ehi_a, count);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

//__m256i _mm256_bslli_epi128 (__m256i a, const int imm8)
#undef _mm256_bslli_epi128
#define _mm256_bslli_epi128(a,imm8) _mm256_set_m128i(_mm_bslli_si128(_mm256_extractf128_si256 (a, 1), imm8),_mm_bslli_si128(_mm256_extractf128_si256 (a, 0), imm8))

//__m256i _mm256_bsrli_epi128 (__m256i a, const int imm8)
#undef _mm256_bsrli_epi128
#define _mm256_bslli_epi128(a,imm8) _mm256_set_m128i(_mm_bsrli_si128(_mm256_extractf128_si256 (a, 1), imm8),_mm_bsrli_si128(_mm256_extractf128_si256 (a, 0), imm8))

//__m256i _mm256_slli_epi16 (__m256i a, int imm8)
#undef _mm256_slli_epi16
#define _mm256_slli_epi16(a,imm8) _mm256_set_m128i(_mm_slli_epi16(_mm256_extractf128_si256 (a, 1), imm8),_mm_slli_epi16(_mm256_extractf128_si256 (a, 0), imm8))

//__m256i _mm256_slli_epi32 (__m256i a, int imm8)
#undef _mm256_slli_epi32
#define _mm256_slli_epi32(a,imm8) _mm256_set_m128i(_mm_slli_epi32(_mm256_extractf128_si256 (a, 1), imm8),_mm_slli_epi32(_mm256_extractf128_si256 (a, 0), imm8))

//__m256i _mm256_slli_epi64 (__m256i a, int imm8)
#undef _mm256_slli_epi64
#define _mm256_slli_epi64(a,imm8) _mm256_set_m128i(_mm_slli_epi64(_mm256_extractf128_si256 (a, 1), imm8),_mm_slli_epi64(_mm256_extractf128_si256 (a, 0), imm8))

//__m256i _mm256_slli_si256 (__m256i a, const int imm8)
#undef _mm256_slli_si256
#define _mm256_slli_si256(a,imm8) _mm256_set_m128i(_mm_slli_si128(_mm256_extractf128_si256 (a, 1), imm8),_mm_slli_si128(_mm256_extractf128_si256 (a, 0), imm8))


//__m256i _mm256_srai_epi16 (__m256i a, int imm8)
#undef _mm256_srai_epi16
#define _mm256_srai_epi16(a,imm8) _mm256_set_m128i(_mm_srai_epi16(_mm256_extractf128_si256 (a, 1), imm8),_mm_srai_epi16(_mm256_extractf128_si256 (a, 0), imm8))

//__m256i _mm256_srai_epi32 (__m256i a, int imm8)
#undef _mm256_srai_epi32
#define _mm256_srai_epi32(a,imm8) _mm256_set_m128i(_mm_srai_epi32(_mm256_extractf128_si256 (a, 1), imm8),_mm_srai_epi32(_mm256_extractf128_si256 (a, 0), imm8))



//__m256i _mm256_srli_epi16 (__m256i a, int imm8)
#undef _mm256_srli_epi16
#define _mm256_srli_epi16(a,imm8) _mm256_set_m128i(_mm_srli_epi16(_mm256_extractf128_si256 (a, 1), imm8),_mm_srli_epi16(_mm256_extractf128_si256 (a, 0), imm8))

//__m256i _mm256_srli_epi32 (__m256i a, int imm8)
#undef _mm256_srli_epi32
#define _mm256_srli_epi32(a,imm8) _mm256_set_m128i(_mm_srli_epi32(_mm256_extractf128_si256 (a, 1), imm8),_mm_srli_epi32(_mm256_extractf128_si256 (a, 0), imm8))

//__m256i _mm256_srli_epi64 (__m256i a, int imm8)
#undef _mm256_srli_epi64
#define _mm256_srli_epi64(a,imm8) _mm256_set_m128i(_mm_srli_epi64(_mm256_extractf128_si256 (a, 1), imm8),_mm_srli_epi64(_mm256_extractf128_si256 (a, 0), imm8))

//__m256i _mm256_srli_si256 (__m256i a, const int imm8)
#undef _mm256_srli_si256
#define _mm256_srli_si256(a,imm8) _mm256_set_m128i(_mm_srli_si128(_mm256_extractf128_si256 (a, 1), imm8),_mm_srli_si128(_mm256_extractf128_si256 (a, 0), imm8))

/***************************************************************************************************/
//Unpack
/***************************************************************************************************/

#undef _mm256_unpackhi_epi8
#define _mm256_unpackhi_epi8 _mm256_unpackhi_epi8_impl
__forceinline __m256i _mm256_unpackhi_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_unpackhi_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_unpackhi_epi8 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_unpackhi_epi16
#define _mm256_unpackhi_epi16 _mm256_unpackhi_epi16_impl
__forceinline __m256i _mm256_unpackhi_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_unpackhi_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_unpackhi_epi16 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_unpackhi_epi32
#define _mm256_unpackhi_epi32 _mm256_unpackhi_epi32_impl
__forceinline __m256i _mm256_unpackhi_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_unpackhi_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_unpackhi_epi32 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_unpackhi_epi64
#define _mm256_unpackhi_epi64 _mm256_unpackhi_epi64_impl
__forceinline __m256i _mm256_unpackhi_epi64_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_unpackhi_epi64 (elo_a, elo_b);
	__m128i ehi_r = _mm_unpackhi_epi64 (ehi_a, ehi_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_unpacklo_epi8
#define _mm256_unpacklo_epi8 _mm256_unpacklo_epi8_impl
__forceinline __m256i _mm256_unpacklo_epi8_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_unpacklo_epi8 (elo_a, elo_b);
	__m128i ehi_r = _mm_unpacklo_epi8 (elo_a, elo_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_unpacklo_epi16
#define _mm256_unpacklo_epi16 _mm256_unpacklo_epi16_impl
__forceinline __m256i _mm256_unpacklo_epi16_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_unpacklo_epi16 (elo_a, elo_b);
	__m128i ehi_r = _mm_unpacklo_epi16 (elo_a, elo_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_unpacklo_epi32
#define _mm256_unpacklo_epi32 _mm256_unpacklo_epi32_impl
__forceinline __m256i _mm256_unpacklo_epi32_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_unpacklo_epi32 (elo_a, elo_b);
	__m128i ehi_r = _mm_unpacklo_epi32 (elo_a, elo_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}

#undef _mm256_unpacklo_epi64
#define _mm256_unpacklo_epi64 _mm256_unpacklo_epi64_impl
__forceinline __m256i _mm256_unpacklo_epi64_impl(__m256i a, __m256i b)
{
	__m128i elo_a = _mm256_extractf128_si256 ( a, 0);
	__m128i ehi_a = _mm256_extractf128_si256 ( a, 1);
	
	__m128i elo_b = _mm256_extractf128_si256 ( b, 0);
	__m128i ehi_b = _mm256_extractf128_si256 ( b, 1);
	
	//MAIN OPERATION
	__m128i elo_r = _mm_unpacklo_epi64 (elo_a, elo_b);
	__m128i ehi_r = _mm_unpacklo_epi64 (elo_a, elo_b);
	

	return _mm256_set_m128i(ehi_r,elo_r);
}
/***************************************************************************************************/
//Convert
/***************************************************************************************************/

#undef _mm256_cvtepi8_epi16
#define _mm256_cvtepi8_epi16 _mm256_cvtepi8_epi16_impl
__forceinline __m256i_i16 _mm256_cvtepi8_epi16_impl(__m128i_i8 a)
{
	return _mm256_set_epi16
	(
		(__int16)_mm_extract_epi8(a, 15),
		(__int16)_mm_extract_epi8(a, 14),
		(__int16)_mm_extract_epi8(a, 13),
		(__int16)_mm_extract_epi8(a, 12),
		(__int16)_mm_extract_epi8(a, 11),
		(__int16)_mm_extract_epi8(a, 10),
		(__int16)_mm_extract_epi8(a, 9),
		(__int16)_mm_extract_epi8(a, 8),
		(__int16)_mm_extract_epi8(a, 7),
		(__int16)_mm_extract_epi8(a, 6),
		(__int16)_mm_extract_epi8(a, 5),
		(__int16)_mm_extract_epi8(a, 4),
		(__int16)_mm_extract_epi8(a, 3),
		(__int16)_mm_extract_epi8(a, 2),
		(__int16)_mm_extract_epi8(a, 1),
		(__int16)_mm_extract_epi8(a, 0)
	);
}

#undef _mm256_cvtepi8_epi32
#define _mm256_cvtepi8_epi32 _mm256_cvtepi8_epi32_impl
__forceinline __m256i_i32 _mm256_cvtepi8_epi32_impl(__m128i_i8 a)
{
	return _mm256_set_epi32
	(
		(__int32)_mm_extract_epi8(a, 7),
		(__int32)_mm_extract_epi8(a, 6),
		(__int32)_mm_extract_epi8(a, 5),
		(__int32)_mm_extract_epi8(a, 4),
		(__int32)_mm_extract_epi8(a, 3),
		(__int32)_mm_extract_epi8(a, 2),
		(__int32)_mm_extract_epi8(a, 1),
		(__int32)_mm_extract_epi8(a, 0)
	);
}

#undef _mm256_cvtepi8_epi64
#define _mm256_cvtepi8_epi64 _mm256_cvtepi8_epi64_impl
__forceinline __m256i_i64 _mm256_cvtepi8_epi64_impl(__m128i_i8 a)
{
	return _mm256_set_epi64x
	(
		(__int64)_mm_extract_epi8(a, 3),
		(__int64)_mm_extract_epi8(a, 2),
		(__int64)_mm_extract_epi8(a, 1),
		(__int64)_mm_extract_epi8(a, 0)
	);
}

#undef _mm256_cvtepi16_epi32
#define _mm256_cvtepi16_epi32 _mm256_cvtepi16_epi32_impl
__forceinline __m256i_i32 _mm256_cvtepi16_epi32_impl(__m128i_i16 a)
{
	return _mm256_set_epi32
	(
		(__int32)_mm_extract_epi16(a, 7),
		(__int32)_mm_extract_epi16(a, 6),
		(__int32)_mm_extract_epi16(a, 5),
		(__int32)_mm_extract_epi16(a, 4),
		(__int32)_mm_extract_epi16(a, 3),
		(__int32)_mm_extract_epi16(a, 2),
		(__int32)_mm_extract_epi16(a, 1),
		(__int32)_mm_extract_epi16(a, 0)
	);
}

#undef _mm256_cvtepi16_epi64
#define _mm256_cvtepi16_epi64 _mm256_cvtepi16_epi64_impl
__forceinline __m256i_i64 _mm256_cvtepi16_epi64_impl(__m128i_i16 a)
{
	return _mm256_set_epi64x
	(
		(__int64)_mm_extract_epi16(a, 3),
		(__int64)_mm_extract_epi16(a, 2),
		(__int64)_mm_extract_epi16(a, 1),
		(__int64)_mm_extract_epi16(a, 0)
	);
}

#undef _mm256_cvtepi32_epi64
#define _mm256_cvtepi32_epi64 _mm256_cvtepi32_epi64_impl
__forceinline __m256i_i64 _mm256_cvtepi32_epi64_impl(__m128i_i32 a)
{
	return _mm256_set_epi64x
	(
		(__int64)_mm_extract_epi32(a, 3),
		(__int64)_mm_extract_epi32(a, 2),
		(__int64)_mm_extract_epi32(a, 1),
		(__int64)_mm_extract_epi32(a, 0)
	);
}

#undef _mm256_cvtepu8_epi16
#define _mm256_cvtepu8_epi16 _mm256_cvtepu8_epi16_impl
__forceinline __m256i_i16 _mm256_cvtepu8_epi16_impl(__m128i_u8 a)
{
	return _mm256_set_epi16
	(
		(__int16)_mm_extract_epu8(a, 15),
		(__int16)_mm_extract_epu8(a, 14),
		(__int16)_mm_extract_epu8(a, 13),
		(__int16)_mm_extract_epu8(a, 12),
		(__int16)_mm_extract_epu8(a, 11),
		(__int16)_mm_extract_epu8(a, 10),
		(__int16)_mm_extract_epu8(a, 9),
		(__int16)_mm_extract_epu8(a, 8),
		(__int16)_mm_extract_epu8(a, 7),
		(__int16)_mm_extract_epu8(a, 6),
		(__int16)_mm_extract_epu8(a, 5),
		(__int16)_mm_extract_epu8(a, 4),
		(__int16)_mm_extract_epu8(a, 3),
		(__int16)_mm_extract_epu8(a, 2),
		(__int16)_mm_extract_epu8(a, 1),
		(__int16)_mm_extract_epu8(a, 0)
	);
}

#undef _mm256_cvtepu8_epi32
#define _mm256_cvtepu8_epi32 _mm256_cvtepu8_epi32_impl
__forceinline __m256i_i32 _mm256_cvtepu8_epi32_impl(__m128i_u8 a)
{
	return _mm256_set_epi32
	(
		(__int32)_mm_extract_epu8(a, 7),
		(__int32)_mm_extract_epu8(a, 6),
		(__int32)_mm_extract_epu8(a, 5),
		(__int32)_mm_extract_epu8(a, 4),
		(__int32)_mm_extract_epu8(a, 3),
		(__int32)_mm_extract_epu8(a, 2),
		(__int32)_mm_extract_epu8(a, 1),
		(__int32)_mm_extract_epu8(a, 0)
	);
}

#undef _mm256_cvtepu8_epi64
#define _mm256_cvtepu8_epi64 _mm256_cvtepu8_epi64_impl
__forceinline __m256i_i64 _mm256_cvtepu8_epi64_impl(__m128i_u8 a)
{
	return _mm256_set_epi64x
	(
		(__int64)_mm_extract_epu8(a, 3),
		(__int64)_mm_extract_epu8(a, 2),
		(__int64)_mm_extract_epu8(a, 1),
		(__int64)_mm_extract_epu8(a, 0)
	);
}

#undef _mm256_cvtepu16_epi32
#define _mm256_cvtepu16_epi32 _mm256_cvtepu16_epi32_impl
__forceinline __m256i_i32 _mm256_cvtepu16_epi32_impl(__m128i_u16 a)
{
	return _mm256_set_epi32
	(
		(__int32)_mm_extract_epu16(a, 7),
		(__int32)_mm_extract_epu16(a, 6),
		(__int32)_mm_extract_epu16(a, 5),
		(__int32)_mm_extract_epu16(a, 4),
		(__int32)_mm_extract_epu16(a, 3),
		(__int32)_mm_extract_epu16(a, 2),
		(__int32)_mm_extract_epu16(a, 1),
		(__int32)_mm_extract_epu16(a, 0)
	);
}

#undef _mm256_cvtepu16_epi64
#define _mm256_cvtepu16_epi64 _mm256_cvtepu16_epi64_impl
__forceinline __m256i_i64 _mm256_cvtepu16_epi64_impl(__m128i_u16 a)
{
	return _mm256_set_epi64x
	(
		(__int64)_mm_extract_epu16(a, 3),
		(__int64)_mm_extract_epu16(a, 2),
		(__int64)_mm_extract_epu16(a, 1),
		(__int64)_mm_extract_epu16(a, 0)
	);
}

#undef _mm256_cvtepu32_epi64
#define _mm256_cvtepu32_epi64 _mm256_cvtepu32_epi64_impl
__forceinline __m256i_i64 _mm256_cvtepu32_epi64_impl(__m128i_u32 a)
{
	return _mm256_set_epi64x
	(
		(__int64)_mm_extract_epu32(a, 3),
		(__int64)_mm_extract_epu32(a, 2),
		(__int64)_mm_extract_epu32(a, 1),
		(__int64)_mm_extract_epu32(a, 0)
	);
}


/***************************************************************************************************/
// Permute
/***************************************************************************************************/
//__m256i _mm256_permute2x128_si256(__m256i a, __m256i b, const int imm8)
//__m256i _mm256_permute4x64_epi64(__m256i a, const int imm8)
//__m256d _mm256_permute4x64_pd(__m256d a, const int imm8)
//__m256i _mm256_permutevar8x32_epi32(__m256i a, __m256i idx)
//__m256 _mm256_permutevar8x32_ps(__m256 a, __m256i idx)




#endif