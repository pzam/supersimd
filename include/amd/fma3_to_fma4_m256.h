/***************************************************************************************************/
// HEADER_NAME /amd/fma3_to_fma4_m256.h
/***************************************************************************************************/

#if FMA256
//Multiply Add 
#define _mm256_fmadd_ps _mm256_macc_ps

//Multiply Add Subtract 
#define _mm256_fmaddsub_ps _mm256_maddsub_ps

//Multiply Subtract 
#define _mm256_fmsub_ps _mm256_msub_ps

//Multiply Subtract Add 
#define _mm256_fmsubadd_ps _mm256_msubadd_ps

//Negative Multiply Add 
#define _mm256_fnmadd_ps _mm256_nmacc_ps

//Negative Multiply Subtract 
#define _mm256_fnmsub_ps _mm256_nmsub_ps

#endif

