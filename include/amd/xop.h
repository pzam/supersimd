/***************************************************************************************************/
// HEADER_NAME /amd/xop.h
/***************************************************************************************************/
/*
***********************************
//ROUNDING ERRORS FOUND IN TEST
***********************************
_mm_haddq_epu32
_mm_macc_epi32
_mm_maccs_epi32
_mm_maccsd_epi16
_mm_maddsd_epi16

_mm_perm_epi8
***********************************
*/
/***************************************************************************************************/
// Vector integer comparison control macros
/***************************************************************************************************/

#define _MM_PCOMCTRL_LT    0
#define _MM_PCOMCTRL_LE    1
#define _MM_PCOMCTRL_GT    2
#define _MM_PCOMCTRL_GE    3
#define _MM_PCOMCTRL_EQ    4
#define _MM_PCOMCTRL_NEQ   5
#define _MM_PCOMCTRL_FALSE 6
#define _MM_PCOMCTRL_TRUE  7

/***************************************************************************************************/
// Control values for permute2 intrinsics
/***************************************************************************************************/

#define _MM_PERMUTE2_COPY    0 // just copy the selected value
// Note that using the constant 1 would have the same effect as 0

#define _MM_PERMUTE2_ZEROIF1 2 // zero selected value if src3 bit is 1
#define _MM_PERMUTE2_ZEROIF0 3 // zero selected value if src3 bit is 3


/***************************************************************************************************/
// Integer comparisons
/***************************************************************************************************/
#undef _mm_comlt_epu8
#define _mm_comlt_epu8 _mm_comlt_epu8_impl
__forceinline __m128i_u8 _mm_comlt_epu8_impl (__m128i_u8 x, __m128i_u8 y){
	return _mm_cmplt_epu8(x, y);
}
#undef _mm_comle_epu8
#define _mm_comle_epu8 _mm_comle_epu8_impl
__forceinline __m128i_u8 _mm_comle_epu8_impl (__m128i_u8 x, __m128i_u8 y){
	return _mm_cmple_epu8(x, y);
}
#undef _mm_comgt_epu8
#define _mm_comgt_epu8 _mm_comgt_epu8_impl
__forceinline __m128i_u8 _mm_comgt_epu8_impl (__m128i_u8 x, __m128i_u8 y){
	return _mm_cmpgt_epu8(x, y);
}
#undef _mm_comge_epu8
#define _mm_comge_epu8 _mm_comge_epu8_impl
__forceinline __m128i_u8 _mm_comge_epu8_impl (__m128i_u8 x, __m128i_u8 y){
	return _mm_cmpge_epu8(x,y);
}
#undef _mm_comeq_epu8
#define _mm_comeq_epu8 _mm_comeq_epu8_impl
__forceinline __m128i_u8 _mm_comeq_epu8_impl (__m128i_u8 x, __m128i_u8 y){
	return _mm_cmpeq_epu8(x,y);
}
#undef _mm_comneq_epu8
#define _mm_comneq_epu8 _mm_comneq_epu8_impl
__forceinline __m128i_u8 _mm_comneq_epu8_impl (__m128i_u8 x, __m128i_u8 y){
	return _mm_cmpneq_epu8(x, y);
}
#undef _mm_comfalse_epu8
#define _mm_comfalse_epu8 _mm_comfalse_epu8_impl
__forceinline __m128i_u8 _mm_comfalse_epu8_impl (__m128i_u8 x, __m128i_u8 y){
	return _mm_setzero_epu8();
}
#undef _mm_comtrue_epu8
#define _mm_comtrue_epu8 _mm_comtrue_epu8_impl
__forceinline __m128i_u8 _mm_comtrue_epu8_impl (__m128i_u8 x, __m128i_u8 y){
	return _mm_cmpeq_epu8( _mm_setzero_epu8(), _mm_setzero_epu8() );
}
/***************************************************************************************************/

#undef _mm_comlt_epu16
#define _mm_comlt_epu16 _mm_comlt_epu16_impl
__forceinline __m128i_u16 _mm_comlt_epu16_impl(__m128i_u16 x, __m128i_u16 y) {
	return _mm_cmplt_epu16(x, y);
}
#undef _mm_comle_epu16
#define _mm_comle_epu16 _mm_comle_epu16_impl
__forceinline __m128i_u16 _mm_comle_epu16_impl(__m128i_u16 x, __m128i_u16 y) {
	return _mm_cmple_epu16(x, y);
}
#undef _mm_comgt_epu16
#define _mm_comgt_epu16 _mm_comgt_epu16_impl
__forceinline __m128i_u16 _mm_comgt_epu16_impl(__m128i_u16 x, __m128i_u16 y) {
	return _mm_cmpgt_epu16(x, y);
}
#undef _mm_comge_epu16
#define _mm_comge_epu16 _mm_comge_epu16_impl
__forceinline __m128i_u16 _mm_comge_epu16_impl(__m128i_u16 x, __m128i_u16 y) {
	return _mm_cmpge_epu16(x, y);
}
#undef _mm_comeq_epu16
#define _mm_comeq_epu16 _mm_comeq_epu16_impl
__forceinline __m128i_u16 _mm_comeq_epu16_impl(__m128i_u16 x, __m128i_u16 y) {
	return _mm_cmpeq_epu16(x, y);
}
#undef _mm_comneq_epu16
#define _mm_comneq_epu16 _mm_comneq_epu16_impl
__forceinline __m128i_u16 _mm_comneq_epu16_impl(__m128i_u16 x, __m128i_u16 y) {
	return _mm_cmpneq_epu16(x, y);
}
#undef _mm_comfalse_epu16
#define _mm_comfalse_epu16 _mm_comfalse_epu16_impl
__forceinline __m128i_u16 _mm_comfalse_epu16_impl(__m128i_u16 x, __m128i_u16 y) {
	return _mm_setzero_epu16();
}
#undef _mm_comtrue_epu16
#define _mm_comtrue_epu16 _mm_comtrue_epu16_impl
__forceinline __m128i_u16 _mm_comtrue_epu16_impl(__m128i_u16 x, __m128i_u16 y) {
	return _mm_cmpeq_epu16(_mm_setzero_epu16(), _mm_setzero_epu16());
}
/***************************************************************************************************/

#undef _mm_comlt_epu32
#define _mm_comlt_epu32 _mm_comlt_epu32_impl
__forceinline __m128i_u32 _mm_comlt_epu32_impl(__m128i_u32 x, __m128i_u32 y) {
	return _mm_cmplt_epu32(x, y);
}
#undef _mm_comle_epu32
#define _mm_comle_epu32 _mm_comle_epu32_impl
__forceinline __m128i_u32 _mm_comle_epu32_impl(__m128i_u32 x, __m128i_u32 y) {
	return _mm_cmple_epu32(x, y);
}
#undef _mm_comgt_epu32
#define _mm_comgt_epu32 _mm_comgt_epu32_impl
__forceinline __m128i_u32 _mm_comgt_epu32_impl(__m128i_u32 x, __m128i_u32 y) {
	return _mm_cmpgt_epu32(x, y);
}
#undef _mm_comge_epu32
#define _mm_comge_epu32 _mm_comge_epu32_impl
__forceinline __m128i_u32 _mm_comge_epu32_impl(__m128i_u32 x, __m128i_u32 y) {
	return _mm_cmpge_epu32(x, y);
}
#undef _mm_comeq_epu32
#define _mm_comeq_epu32 _mm_comeq_epu32_impl
__forceinline __m128i_u32 _mm_comeq_epu32_impl(__m128i_u32 x, __m128i_u32 y) {
	return _mm_cmpeq_epu32(x, y);
}
#undef _mm_comneq_epu32
#define _mm_comneq_epu32 _mm_comneq_epu32_impl
__forceinline __m128i_u32 _mm_comneq_epu32_impl(__m128i_u32 x, __m128i_u32 y) {
	return _mm_cmpneq_epu32(x, y);
}
#undef _mm_comfalse_epu32
#define _mm_comfalse_epu32 _mm_comfalse_epu32_impl
__forceinline __m128i_u32 _mm_comfalse_epu32_impl(__m128i_u32 x, __m128i_u32 y) {
	return _mm_setzero_epu32();
}
#undef _mm_comtrue_epu32
#define _mm_comtrue_epu32 _mm_comtrue_epu32_impl
__forceinline __m128i_u32 _mm_comtrue_epu32_impl(__m128i_u32 x, __m128i_u32 y) {
	return _mm_cmpeq_epu32(_mm_setzero_epu32(), _mm_setzero_epu32());
}
/***************************************************************************************************/

#if BIT64
#undef _mm_comlt_epu64
#define _mm_comlt_epu64 _mm_comlt_epu64_impl
__forceinline __m128i_u64 _mm_comlt_epu64_impl(__m128i_u64 x, __m128i_u64 y) {
	return _mm_cmplt_epu64(x, y);
}
#undef _mm_comle_epu64
#define _mm_comle_epu64 _mm_comle_epu64_impl
__forceinline __m128i_u64 _mm_comle_epu64_impl(__m128i_u64 x, __m128i_u64 y) {
	return _mm_cmple_epu64(x, y);
}
#undef _mm_comgt_epu64
#define _mm_comgt_epu64 _mm_comgt_epu64_impl
__forceinline __m128i_u64 _mm_comgt_epu64_impl(__m128i_u64 x, __m128i_u64 y) {
	return _mm_cmpgt_epu64(x, y);
}
#undef _mm_comge_epu64
#define _mm_comge_epu64 _mm_comge_epu64_impl
__forceinline __m128i_u64 _mm_comge_epu64_impl(__m128i_u64 x, __m128i_u64 y) {
	return _mm_cmpge_epu64(x, y);
}
#undef _mm_comeq_epu64
#define _mm_comeq_epu64 _mm_comeq_epu64_impl
__forceinline __m128i_u64 _mm_comeq_epu64_impl(__m128i_u64 x, __m128i_u64 y) {
	return _mm_cmpeq_epu64(x, y);
}
#undef _mm_comneq_epu64
#define _mm_comneq_epu64 _mm_comneq_epu64_impl
__forceinline __m128i_u64 _mm_comneq_epu64_impl(__m128i_u64 x, __m128i_u64 y) {
	return _mm_cmpneq_epu64(x, y);
}
#undef _mm_comfalse_epu64
#define _mm_comfalse_epu64 _mm_comfalse_epu64_impl
__forceinline __m128i_u64 _mm_comfalse_epu64_impl(__m128i_u64 x, __m128i_u64 y) {
	return _mm_setzero_epu64();
}
#undef _mm_comtrue_epu64
#define _mm_comtrue_epu64 _mm_comtrue_epu64_impl
__forceinline __m128i_u64 _mm_comtrue_epu64_impl(__m128i_u64 x, __m128i_u64 y) {
	return _mm_cmpeq_epu64(_mm_setzero_epu64(), _mm_setzero_epu64());
}
#endif
/***************************************************************************************************/

#undef _mm_comlt_epi8
#define _mm_comlt_epi8 _mm_comlt_epi8_impl
__forceinline __m128i_i8 _mm_comlt_epi8_impl(__m128i_i8 x, __m128i_i8 y) {
	return _mm_cmplt_epi8(x, y);
}
#undef _mm_comle_epi8
#define _mm_comle_epi8 _mm_comle_epi8_impl
__forceinline __m128i_i8 _mm_comle_epi8_impl(__m128i_i8 x, __m128i_i8 y) {
	return _mm_cmple_epi8(x, y);
}
#undef _mm_comgt_epi8
#define _mm_comgt_epi8 _mm_comgt_epi8_impl
__forceinline __m128i_i8 _mm_comgt_epi8_impl(__m128i_i8 x, __m128i_i8 y) {
	return _mm_cmpgt_epi8(x, y);
}
#undef _mm_comge_epi8
#define _mm_comge_epi8 _mm_comge_epi8_impl
__forceinline __m128i_i8 _mm_comge_epi8_impl(__m128i_i8 x, __m128i_i8 y) {
	return _mm_cmpge_epi8(x, y);
}
#undef _mm_comeq_epi8
#define _mm_comeq_epi8 _mm_comeq_epi8_impl
__forceinline __m128i_i8 _mm_comeq_epi8_impl(__m128i_i8 x, __m128i_i8 y) {
	return _mm_cmpeq_epi8(x, y);
}
#undef _mm_comneq_epi8
#define _mm_comneq_epi8 _mm_comneq_epi8_impl
__forceinline __m128i_i8 _mm_comneq_epi8_impl(__m128i_i8 x, __m128i_i8 y) {
	return _mm_cmpneq_epi8(x, y);
}
#undef _mm_comfalse_epi8
#define _mm_comfalse_epi8 _mm_comfalse_epi8_impl
__forceinline __m128i_i8 _mm_comfalse_epi8_impl(__m128i_i8 x, __m128i_i8 y) {
	return _mm_setzero_epi8();
}
#undef _mm_comtrue_epi8
#define _mm_comtrue_epi8 _mm_comtrue_epi8_impl
__forceinline __m128i_i8 _mm_comtrue_epi8_impl(__m128i_i8 x, __m128i_i8 y) {
	return _mm_cmpeq_epi8(_mm_setzero_epi8(), _mm_setzero_epi8());
}
/***************************************************************************************************/

#undef _mm_comlt_epi16
#define _mm_comlt_epi16 _mm_comlt_epi16_impl
__forceinline __m128i_i16 _mm_comlt_epi16_impl(__m128i_i16 x, __m128i_i16 y) {
	return _mm_cmplt_epi16(x, y);
}
#undef _mm_comle_epi16
#define _mm_comle_epi16 _mm_comle_epi16_impl
__forceinline __m128i_i16 _mm_comle_epi16_impl(__m128i_i16 x, __m128i_i16 y) {
	return _mm_cmple_epi16(x, y);
}
#undef _mm_comgt_epi16
#define _mm_comgt_epi16 _mm_comgt_epi16_impl
__forceinline __m128i_i16 _mm_comgt_epi16_impl(__m128i_i16 x, __m128i_i16 y) {
	return _mm_cmpgt_epi16(x, y);
}
#undef _mm_comge_epi16
#define _mm_comge_epi16 _mm_comge_epi16_impl
__forceinline __m128i_i16 _mm_comge_epi16_impl(__m128i_i16 x, __m128i_i16 y) {
	return _mm_cmpge_epi16(x, y);
}
#undef _mm_comeq_epi16
#define _mm_comeq_epi16 _mm_comeq_epi16_impl
__forceinline __m128i_i16 _mm_comeq_epi16_impl(__m128i_i16 x, __m128i_i16 y) {
	return _mm_cmpeq_epi16(x, y);
}
#undef _mm_comneq_epi16
#define _mm_comneq_epi16 _mm_comneq_epi16_impl
__forceinline __m128i_i16 _mm_comneq_epi16_impl(__m128i_i16 x, __m128i_i16 y) {
	return _mm_cmpneq_epi16(x, y);
}
#undef _mm_comfalse_epi16
#define _mm_comfalse_epi16 _mm_comfalse_epi16_impl
__forceinline __m128i_i16 _mm_comfalse_epi16_impl(__m128i_i16 x, __m128i_i16 y) {
	return _mm_setzero_epi16();
}
#undef _mm_comtrue_epi16
#define _mm_comtrue_epi16 _mm_comtrue_epi16_impl
__forceinline __m128i_i16 _mm_comtrue_epi16_impl(__m128i_i16 x, __m128i_i16 y) {
	return _mm_cmpeq_epi16(_mm_setzero_epi16(), _mm_setzero_epi16());
}
/***************************************************************************************************/

#undef _mm_comlt_epi32
#define _mm_comlt_epi32 _mm_comlt_epi32_impl
__forceinline __m128i_i32 _mm_comlt_epi32_impl(__m128i_i32 x, __m128i_i32 y) {
	return _mm_cmplt_epi32(x, y);
}
#undef _mm_comle_epi32
#define _mm_comle_epi32 _mm_comle_epi32_impl
__forceinline __m128i_i32 _mm_comle_epi32_impl(__m128i_i32 x, __m128i_i32 y) {
	return _mm_cmple_epi32(x, y);
}
#undef _mm_comgt_epi32
#define _mm_comgt_epi32 _mm_comgt_epi32_impl
__forceinline __m128i_i32 _mm_comgt_epi32_impl(__m128i_i32 x, __m128i_i32 y) {
	return _mm_cmpgt_epi32(x, y);
}
#undef _mm_comge_epi32
#define _mm_comge_epi32 _mm_comge_epi32_impl
__forceinline __m128i_i32 _mm_comge_epi32_impl(__m128i_i32 x, __m128i_i32 y) {
	return _mm_cmpge_epi32(x, y);
}
#undef _mm_comeq_epi32
#define _mm_comeq_epi32 _mm_comeq_epi32_impl
__forceinline __m128i_i32 _mm_comeq_epi32_impl(__m128i_i32 x, __m128i_i32 y) {
	return _mm_cmpeq_epi32(x, y);
}
#undef _mm_comneq_epi32
#define _mm_comneq_epi32 _mm_comneq_epi32_impl
__forceinline __m128i_i32 _mm_comneq_epi32_impl(__m128i_i32 x, __m128i_i32 y) {
	return _mm_cmpneq_epi32(x, y);
}
#undef _mm_comfalse_epi32
#define _mm_comfalse_epi32 _mm_comfalse_epi32_impl
__forceinline __m128i_i32 _mm_comfalse_epi32_impl(__m128i_i32 x, __m128i_i32 y) {
	return _mm_setzero_epi32();
}
#undef _mm_comtrue_epi32
#define _mm_comtrue_epi32 _mm_comtrue_epi32_impl
__forceinline __m128i_i32 _mm_comtrue_epi32_impl(__m128i_i32 x, __m128i_i32 y) {
	return _mm_cmpeq_epi32(_mm_setzero_epi32(), _mm_setzero_epi32());
}
/***************************************************************************************************/

#if BIT64
#undef _mm_comlt_epi64
#define _mm_comlt_epi64 _mm_comlt_epi64_impl
__forceinline __m128i_i64 _mm_comlt_epi64_impl(__m128i_i64 x, __m128i_i64 y) {
	return _mm_cmplt_epi64(x, y);
}
#undef _mm_comle_epi64
#define _mm_comle_epi64 _mm_comle_epi64_impl
__forceinline __m128i_i64 _mm_comle_epi64_impl(__m128i_i64 x, __m128i_i64 y) {
	return _mm_cmple_epi64(x, y);
}
#undef _mm_comgt_epi64
#define _mm_comgt_epi64 _mm_comgt_epi64_impl
__forceinline __m128i_i64 _mm_comgt_epi64_impl(__m128i_i64 x, __m128i_i64 y) {
	return _mm_cmpgt_epi64(x, y);
}
#undef _mm_comge_epi64
#define _mm_comge_epi64 _mm_comge_epi64_impl
__forceinline __m128i_i64 _mm_comge_epi64_impl(__m128i_i64 x, __m128i_i64 y) {
	return _mm_cmpge_epi64(x, y);
}
#undef _mm_comeq_epi64
#define _mm_comeq_epi64 _mm_comeq_epi64_impl
__forceinline __m128i_i64 _mm_comeq_epi64_impl(__m128i_i64 x, __m128i_i64 y) {
	return _mm_cmpeq_epi64(x, y);
}
#undef _mm_comneq_epi64
#define _mm_comneq_epi64 _mm_comneq_epi64_impl
__forceinline __m128i_i64 _mm_comneq_epi64_impl(__m128i_i64 x, __m128i_i64 y) {
	return _mm_cmpneq_epi64(x, y);
}
#undef _mm_comfalse_epi64
#define _mm_comfalse_epi64 _mm_comfalse_epi64_impl
__forceinline __m128i_i64 _mm_comfalse_epi64_impl(__m128i_i64 x, __m128i_i64 y) {
	return _mm_setzero_epi64();
}
#undef _mm_comtrue_epi64
#define _mm_comtrue_epi64 _mm_comtrue_epi64_impl
__forceinline __m128i_i64 _mm_comtrue_epi64_impl(__m128i_i64 x, __m128i_i64 y) {
	return _mm_cmpeq_epi64(_mm_setzero_epi64(), _mm_setzero_epi64());
}
#endif
/***************************************************************************************************/

/***************************************************************************************************/
// Vector integer comparisons
/***************************************************************************************************/
#undef _mm_com_epu8
#define _mm_com_epu8(a, b, imm8) _mm_com_epu8_impl(a, b, imm8)
__forceinline __m128i_u8 _mm_com_epu8_impl(__m128i_u8 a, __m128i_u8 b,int imm8)
{
	if(imm8 == 0) 
		return _mm_comlt_epu8(a,b);
	if(imm8 == 1) 
		return _mm_comle_epu8(a,b);
	if(imm8 == 2) 
		return _mm_comgt_epu8(a,b);
	if(imm8 == 3) 
		return _mm_comge_epu8(a,b);
	if(imm8 == 4) 
		return _mm_comeq_epu8(a,b);
	if(imm8 == 5) 
		return _mm_comneq_epu8(a,b);
	if(imm8 == 6) 
		return _mm_comfalse_epu8(a,b);
	if(imm8 == 7) 
		return _mm_comtrue_epu8(a,b);
}
/***************************************************************************************************/
#undef _mm_com_epu16
#define _mm_com_epu16(a, b, imm8) _mm_com_epu16_impl(a, b, imm8)
__forceinline __m128i_u16 _mm_com_epu16_impl(__m128i_u16 a, __m128i_u16 b,int imm8)
{
	if(imm8 == 0) 
		return _mm_comlt_epu16(a,b);
	if(imm8 == 1) 
		return _mm_comle_epu16(a,b);
	if(imm8 == 2) 
		return _mm_comgt_epu16(a,b);
	if(imm8 == 3) 
		return _mm_comge_epu16(a,b);
	if(imm8 == 4) 
		return _mm_comeq_epu16(a,b);
	if(imm8 == 5) 
		return _mm_comneq_epu16(a,b);
	if(imm8 == 6) 
		return _mm_comfalse_epu16(a,b);
	if(imm8 == 7) 
		return _mm_comtrue_epu16(a,b);
}
/***************************************************************************************************/
#undef _mm_com_epu32
#define _mm_com_epu32(a, b, imm8) _mm_com_epu32_impl(a, b, imm8)
__forceinline __m128i_u32 _mm_com_epu32_impl(__m128i_u32 a, __m128i_u32 b,int imm8)
{
	if(imm8 == 0) 
		return _mm_comlt_epu32(a,b);
	if(imm8 == 1) 
		return _mm_comle_epu32(a,b);
	if(imm8 == 2) 
		return _mm_comgt_epu32(a,b);
	if(imm8 == 3) 
		return _mm_comge_epu32(a,b);
	if(imm8 == 4) 
		return _mm_comeq_epu32(a,b);
	if(imm8 == 5) 
		return _mm_comneq_epu32(a,b);
	if(imm8 == 6) 
		return _mm_comfalse_epu32(a,b);
	if(imm8 == 7) 
		return _mm_comtrue_epu32(a,b);
}
/***************************************************************************************************/
#if BIT64
#undef _mm_com_epu64
#define _mm_com_epu64(a, b, imm8) _mm_com_epu64_impl(a, b, imm8)
__forceinline __m128i_u64 _mm_com_epu64_impl(__m128i_u64 a, __m128i_u64 b,int imm8)
{
	if(imm8 == 0) 
		return _mm_comlt_epu64(a,b);
	if(imm8 == 1) 
		return _mm_comle_epu64(a,b);
	if(imm8 == 2) 
		return _mm_comgt_epu64(a,b);
	if(imm8 == 3) 
		return _mm_comge_epu64(a,b);
	if(imm8 == 4) 
		return _mm_comeq_epu64(a,b);
	if(imm8 == 5) 
		return _mm_comneq_epu64(a,b);
	if(imm8 == 6) 
		return _mm_comfalse_epu64(a,b);
	if(imm8 == 7) 
		return _mm_comtrue_epu64(a,b);
}
#endif
/***************************************************************************************************/
#undef _mm_com_epi8
#define _mm_com_epi8(a, b, imm8) _mm_com_epi8_impl(a, b, imm8)
__forceinline __m128i_i8 _mm_com_epi8_impl(__m128i_i8 a,__m128i_i8 b,int imm8)
{
	if(imm8 == 0) 
		return _mm_comlt_epi8(a,b);
	if(imm8 == 1) 
		return _mm_comle_epi8(a,b);
	if(imm8 == 2) 
		return _mm_comgt_epi8(a,b);
	if(imm8 == 3) 
		return _mm_comge_epi8(a,b);
	if(imm8 == 4) 
		return _mm_comeq_epi8(a,b);
	if(imm8 == 5) 
		return _mm_comneq_epi8(a,b);
	if(imm8 == 6) 
		return _mm_comfalse_epi8(a,b);
	if(imm8 == 7) 
		return _mm_comtrue_epi8(a,b);
}
/***************************************************************************************************/
#undef _mm_com_epi16
#define _mm_com_epi16(a, b, imm8) _mm_com_epi16_impl(a, b, imm8)
__forceinline __m128i_i16 _mm_com_epi16_impl(__m128i_i16 a,__m128i_i16 b,int imm8)
{
	if(imm8 == 0) 
		return _mm_comlt_epi16(a,b);
	if(imm8 == 1) 
		return _mm_comle_epi16(a,b);
	if(imm8 == 2) 
		return _mm_comgt_epi16(a,b);
	if(imm8 == 3) 
		return _mm_comge_epi16(a,b);
	if(imm8 == 4) 
		return _mm_comeq_epi16(a,b);
	if(imm8 == 5) 
		return _mm_comneq_epi16(a,b);
	if(imm8 == 6) 
		return _mm_comfalse_epi16(a,b);
	if(imm8 == 7) 
		return _mm_comtrue_epi16(a,b);
}
/***************************************************************************************************/
#undef _mm_com_epi32
#define _mm_com_epi32(a, b, imm8) _mm_com_epi32_impl(a, b, imm8)
__forceinline __m128i_i32 _mm_com_epi32_impl(__m128i_i32 a,__m128i_i32 b,int imm8)
{
	if(imm8 == 0) 
		return _mm_comlt_epi32(a,b);
	if(imm8 == 1) 
		return _mm_comle_epi32(a,b);
	if(imm8 == 2) 
		return _mm_comgt_epi32(a,b);
	if(imm8 == 3) 
		return _mm_comge_epi32(a,b);
	if(imm8 == 4) 
		return _mm_comeq_epi32(a,b);
	if(imm8 == 5) 
		return _mm_comneq_epi32(a,b);
	if(imm8 == 6) 
		return _mm_comfalse_epi32(a,b);
	if(imm8 == 7) 
		return _mm_comtrue_epi32(a,b);
}
/***************************************************************************************************/
#if BIT64
#undef _mm_com_epi64
#define _mm_com_epi64(a, b, imm8) _mm_com_epi64_impl(a, b, imm8)
__forceinline __m128i_i64 _mm_com_epi64_impl(__m128i_i64 a,__m128i_i64 b,int imm8)
{
	if(imm8 == 0) 
		return _mm_comlt_epi64(a,b);
	if(imm8 == 1) 
		return _mm_comle_epi64(a,b);
	if(imm8 == 2) 
		return _mm_comgt_epi64(a,b);
	if(imm8 == 3) 
		return _mm_comge_epi64(a,b);
	if(imm8 == 4) 
		return _mm_comeq_epi64(a,b);
	if(imm8 == 5) 
		return _mm_comneq_epi64(a,b);
	if(imm8 == 6) 
		return _mm_comfalse_epi64(a,b);
	if(imm8 == 7) 
		return _mm_comtrue_epi64(a,b);
}
#endif
/***************************************************************************************************/
/***************************************************************************************************/
// Precision control
/***************************************************************************************************/
#undef _mm_frcz_ps
#define _mm_frcz_ps(a) _mm_frcz_ps_impl(a)
__forceinline __m128 _mm_frcz_ps_impl(__m128 a){
	__m128 sign_bit, y;
	
	sign_bit = _mm_preservesignbit_ps(a);
	a = _mm_abs_ps(a);
	
	y = _mm_floor_ps(a);
	y = _mm_sub_ps(a, y);
	
	return _mm_effectsignbit_ps(y, sign_bit);
}

#undef _mm_frcz_ss
#define _mm_frcz_ss(high,src) _mm_frcz_ss_impl(high,src)
__forceinline __m128 _mm_frcz_ss (__m128 high,__m128 src)
{
	return _mm_insert_ps(high, _mm_frcz_ps(src), 0);
} 

#if BIT64
#undef _mm_frcz_pd
#define _mm_frcz_pd(a) _mm_frcz_pd_impl(a)
__forceinline __m128d _mm_frcz_pd_impl(__m128d a){
	__m128d sign_bit, y;
	
	sign_bit = _mm_preservesignbit_pd(a);
	a = _mm_abs_pd(a);
	
	y = _mm_floor_pd(a);
	y = _mm_sub_pd(a, y);
	
	return _mm_effectsignbit_pd(y, sign_bit);
}

#undef _mm_frcz_sd
#define _mm_frcz_sd(high,src) _mm_frcz_sd_impl(high,src)
__forceinline __m128d _mm_frcz_sd(__m128d high, __m128d src)
{
	return _mm_insert_pd(high, _mm_frcz_pd(src), 0);
}
#endif
/***************************************************************************************************/
#if __AVX__
#undef _mm256_frcz_ps
#define _mm256_frcz_ps(a) _mm256_frcz_ps_impl(a)
__forceinline __m256 _mm256_frcz_ps_impl(__m256 a){
	__m256 sign_bit, y;
	
	sign_bit = _mm256_preservesignbit_ps(a);
	a = _mm256_abs_ps(a);
	
	y = _mm256_floor_ps(a);
	y = _mm256_sub_ps(a, y);
	
	return _mm256_effectsignbit_ps(y, sign_bit);
}

#undef _mm256_frcz_pd
#define _mm256_frcz_pd(a) _mm256_frcz_pd_impl(a)
__forceinline __m256d _mm256_frcz_pd_impl(__m256d a){
	__m256d sign_bit, y;
	
	sign_bit = _mm256_preservesignbit_pd(a);
	a = _mm256_abs_pd(a);
	
	y = _mm256_floor_pd(a);
	y = _mm256_sub_pd(a, y);
	
	return _mm256_effectsignbit_pd(y, sign_bit);
}
#endif
/***************************************************************************************************/
/***************************************************************************************************/
// Horizontal add/subtract
/***************************************************************************************************/

#undef _mm_haddd_epi8
#define _mm_haddd_epi8(a) _mm_haddd_epi8_impl(a)
__forceinline __m128i_i32 _mm_haddd_epi8_impl(__m128i_i8 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epi32
	(
		(__int32)((__int8)_mm_extract_epi8(a, 15) + ((__int8)_mm_extract_epi8(a, 14) + ((__int8)_mm_extract_epi8(a, 13) + (__int8)_mm_extract_epi8(a, 12)))),
		(__int32)((__int8)_mm_extract_epi8(a, 11) + ((__int8)_mm_extract_epi8(a, 10) + ((__int8)_mm_extract_epi8(a, 9) + (__int8)_mm_extract_epi8(a, 8)))),
		(__int32)((__int8)_mm_extract_epi8(a, 7) + ((__int8)_mm_extract_epi8(a, 6) + ((__int8)_mm_extract_epi8(a, 5) + (__int8)_mm_extract_epi8(a, 4)))),
		(__int32)((__int8)_mm_extract_epi8(a, 3) + ((__int8)_mm_extract_epi8(a, 2) + ((__int8)_mm_extract_epi8(a, 1) + (__int8)_mm_extract_epi8(a, 0))))
	);
}

#if !ARM
#undef _mm_haddd_epi16
#define _mm_haddd_epi16(a) _mm_haddd_epi16_impl(a)
__forceinline __m128i_i32 _mm_haddd_epi16_impl(__m128i_i16 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epi32
	(
		(__int32)((__int16)_mm_extract_epi16(a, 7) + (__int16)_mm_extract_epi16(a, 6)),
		(__int32)((__int16)_mm_extract_epi16(a, 5) + (__int16)_mm_extract_epi16(a, 4)),
		(__int32)((__int16)_mm_extract_epi16(a, 3) + (__int16)_mm_extract_epi16(a, 2)),
		(__int32)((__int16)_mm_extract_epi16(a, 1) + (__int16)_mm_extract_epi16(a, 0))
	);
}
#endif

#undef _mm_haddd_epu8
#define _mm_haddd_epu8(a) _mm_haddd_epu8_impl(a)
__forceinline __m128i_u32 _mm_haddd_epu8_impl(__m128i_u8 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epu32
	(
		(__uint32)((__uint8)_mm_extract_epu8(a, 15) + ((__uint8)_mm_extract_epu8(a, 14) + ((__uint8)_mm_extract_epu8(a, 13) + (__uint8)_mm_extract_epu8(a, 12)))),
		(__uint32)((__uint8)_mm_extract_epu8(a, 11) + ((__uint8)_mm_extract_epu8(a, 10) + ((__uint8)_mm_extract_epu8(a, 9) + (__uint8)_mm_extract_epu8(a, 8)))),
		(__uint32)((__uint8)_mm_extract_epu8(a, 7) + ((__uint8)_mm_extract_epu8(a, 6) + ((__uint8)_mm_extract_epu8(a, 5) + (__uint8)_mm_extract_epu8(a, 4)))),
		(__uint32)((__uint8)_mm_extract_epu8(a, 3) + ((__uint8)_mm_extract_epu8(a, 2) + ((__uint8)_mm_extract_epu8(a, 1) + (__uint8)_mm_extract_epu8(a, 0))))
		);
}

#if !ARM
#undef _mm_haddd_epu16
#define _mm_haddd_epu16(a) _mm_haddd_epu16_impl(a)
__forceinline __m128i_u32 _mm_haddd_epu16_impl(__m128i_u16 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epu32
	(
		(__uint32)((__uint16)_mm_extract_epu16(a, 7) + (__uint16)_mm_extract_epu16(a, 6)),
		(__uint32)((__uint16)_mm_extract_epu16(a, 5) + (__uint16)_mm_extract_epu16(a, 4)),
		(__uint32)((__uint16)_mm_extract_epu16(a, 3) + (__uint16)_mm_extract_epu16(a, 2)),
		(__uint32)((__uint16)_mm_extract_epu16(a, 1) + (__uint16)_mm_extract_epu16(a, 0))
	);
}
#endif

#undef _mm_haddq_epi8
#define _mm_haddq_epi8(a) _mm_haddq_epi8_impl(a)
__forceinline __m128i_i64 _mm_haddq_epi8_impl(__m128i_i8 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epi64x
	(
		(__int64)((__int8)_mm_extract_epi8(a, 15) + ((__int8)_mm_extract_epi8(a, 14) + ((__int8)_mm_extract_epi8(a, 13) + ((__int8)_mm_extract_epi8(a, 12) + ((__int8)_mm_extract_epi8(a, 11) + ((__int8)_mm_extract_epi8(a, 10) + ((__int8)_mm_extract_epi8(a, 9) + (__int8)_mm_extract_epi8(a, 8)))))))),
		(__int64)((__int8)_mm_extract_epi8(a, 7) + ((__int8)_mm_extract_epi8(a, 6) + ((__int8)_mm_extract_epi8(a, 5) + ((__int8)_mm_extract_epi8(a, 4) + ((__int8)_mm_extract_epi8(a, 3) + ((__int8)_mm_extract_epi8(a, 2) + ((__int8)_mm_extract_epi8(a, 1) + (__int8)_mm_extract_epi8(a, 0))))))))
	);
}

#undef _mm_haddq_epi16
#define _mm_haddq_epi16(a) _mm_haddq_epi16_impl(a)
__forceinline __m128i_i64 _mm_haddq_epi16_impl(__m128i_i16 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epi64x
	(
		(__int64)(((__int16)_mm_extract_epi16(a, 7) + ((__int16)_mm_extract_epi16(a, 6) + ((__int16)_mm_extract_epi16(a, 5) + (__int16)_mm_extract_epi16(a, 4))))),
		(__int64)(((__int16)_mm_extract_epi16(a, 3) + ((__int16)_mm_extract_epi16(a, 2) + ((__int16)_mm_extract_epi16(a, 1) + (__int16)_mm_extract_epi16(a, 0)))))
	);
}

#if !ARM
#undef _mm_haddq_epi32
#define _mm_haddq_epi32(a) _mm_haddq_epi32_impl(a)
__forceinline __m128i_i64 _mm_haddq_epi32_impl(__m128i_i32 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epi64x
	(
		(__int64)((__int32)_mm_extract_epi32(a, 3) + (__int32)_mm_extract_epi32(a, 2)),
		(__int64)((__int32)_mm_extract_epi32(a, 1) + (__int32)_mm_extract_epi32(a, 0))
	);
}
#endif

#undef _mm_haddq_epu8
#define _mm_haddq_epu8(a) _mm_haddq_epu8_impl(a)
__forceinline __m128i_i64 _mm_haddq_epu8_impl(__m128i_i8 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epu64x
	(
		(__uint64)((__uint8)_mm_extract_epu8(a, 15) + ((__uint8)_mm_extract_epu8(a, 14) + ((__uint8)_mm_extract_epu8(a, 13) + ((__uint8)_mm_extract_epu8(a, 12) + ((__uint8)_mm_extract_epu8(a, 11) + ((__uint8)_mm_extract_epu8(a, 10) + ((__uint8)_mm_extract_epu8(a, 9) + (__uint8)_mm_extract_epu8(a, 8)))))))),
		(__uint64)((__uint8)_mm_extract_epu8(a, 7) + ((__uint8)_mm_extract_epu8(a, 6) + ((__uint8)_mm_extract_epu8(a, 5) + ((__uint8)_mm_extract_epu8(a, 4) + ((__uint8)_mm_extract_epu8(a, 3) + ((__uint8)_mm_extract_epu8(a, 2) + ((__uint8)_mm_extract_epu8(a, 1) + (__uint8)_mm_extract_epu8(a, 0))))))))
	);
}

#undef _mm_haddq_epu16
#define _mm_haddq_epu16(a) _mm_haddq_epu16_impl(a)
__forceinline __m128i_u64 _mm_haddq_epu16_impl(__m128i_u16 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epu64x
	(
		(__uint64)(((__uint16)_mm_extract_epu16(a, 7) + ((__uint16)_mm_extract_epu16(a, 6) + ((__uint16)_mm_extract_epu16(a, 5) + (__uint16)_mm_extract_epu16(a, 4))))),
		(__uint64)(((__uint16)_mm_extract_epu16(a, 3) + ((__uint16)_mm_extract_epu16(a, 2) + ((__uint16)_mm_extract_epu16(a, 1) + (__uint16)_mm_extract_epu16(a, 0)))))
	);
}

#if !ARM
#undef _mm_haddq_epu32
#define _mm_haddq_epu32(a) _mm_haddq_epu32_impl(a)
__forceinline __m128i_u64 _mm_haddq_epu32_impl(__m128i_u32 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epu64x
	(
		(__uint64)((__uint32)_mm_extract_epu32(a, 3) + (__uint32)_mm_extract_epu32(a, 2)),
		(__uint64)((__uint32)_mm_extract_epu32(a, 1) + (__uint32)_mm_extract_epu32(a, 0))
	);
}
#endif

#if !ARM
#undef _mm_haddw_epi8
#define _mm_haddw_epi8(a) _mm_haddw_epi8_impl(a)
__forceinline __m128i_i16 _mm_haddw_epi8_impl(__m128i_i8 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epi16
	(
		(__int16)((__int8)_mm_extract_epi8(a, 15) + (__int8)_mm_extract_epi8(a, 14)),
		(__int16)((__int8)_mm_extract_epi8(a, 13) + (__int8)_mm_extract_epi8(a, 12)),
		(__int16)((__int8)_mm_extract_epi8(a, 11) + (__int8)_mm_extract_epi8(a, 10)),
		(__int16)((__int8)_mm_extract_epi8(a, 9) + (__int8)_mm_extract_epi8(a, 8)),
		(__int16)((__int8)_mm_extract_epi8(a, 7) + (__int8)_mm_extract_epi8(a, 6)),
		(__int16)((__int8)_mm_extract_epi8(a, 5) + (__int8)_mm_extract_epi8(a, 4)),
		(__int16)((__int8)_mm_extract_epi8(a, 3) + (__int8)_mm_extract_epi8(a, 2)),
		(__int16)((__int8)_mm_extract_epi8(a, 1) + (__int8)_mm_extract_epi8(a, 0))
	);
}
#endif

#if !ARM
#undef _mm_haddw_epu8
#define _mm_haddw_epu8(a) _mm_haddw_epu8_impl(a)
__forceinline __m128i_u16 _mm_haddw_epu8_impl(__m128i_u8 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epu16
	(
		(__uint16)((__uint8)_mm_extract_epu8(a, 15) + (__uint8)_mm_extract_epu8(a, 14)),
		(__uint16)((__uint8)_mm_extract_epu8(a, 13) + (__uint8)_mm_extract_epu8(a, 12)),
		(__uint16)((__uint8)_mm_extract_epu8(a, 11) + (__uint8)_mm_extract_epu8(a, 10)),
		(__uint16)((__uint8)_mm_extract_epu8(a, 9) + (__uint8)_mm_extract_epu8(a, 8)),
		(__uint16)((__uint8)_mm_extract_epu8(a, 7) + (__uint8)_mm_extract_epu8(a, 6)),
		(__uint16)((__uint8)_mm_extract_epu8(a, 5) + (__uint8)_mm_extract_epu8(a, 4)),
		(__uint16)((__uint8)_mm_extract_epu8(a, 3) + (__uint8)_mm_extract_epu8(a, 2)),
		(__uint16)((__uint8)_mm_extract_epu8(a, 1) + (__uint8)_mm_extract_epu8(a, 0))
		);
}
#endif

#if !ARM
#undef _mm_hsubw_epi8
#define _mm_hsubw_epi8(a) _mm_hsubw_epi8_impl(a)
__forceinline __m128i_i16 _mm_hsubw_epi8_impl(__m128i_i8 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epi16
	(
		(__int16)((__int8)_mm_extract_epi8(a, 15) - (__int8)_mm_extract_epi8(a, 14)),
		(__int16)((__int8)_mm_extract_epi8(a, 13) - (__int8)_mm_extract_epi8(a, 12)),
		(__int16)((__int8)_mm_extract_epi8(a, 11) - (__int8)_mm_extract_epi8(a, 10)),
		(__int16)((__int8)_mm_extract_epi8(a, 9) - (__int8)_mm_extract_epi8(a, 8)),
		(__int16)((__int8)_mm_extract_epi8(a, 7) - (__int8)_mm_extract_epi8(a, 6)),
		(__int16)((__int8)_mm_extract_epi8(a, 5) - (__int8)_mm_extract_epi8(a, 4)),
		(__int16)((__int8)_mm_extract_epi8(a, 3) - (__int8)_mm_extract_epi8(a, 2)),
		(__int16)((__int8)_mm_extract_epi8(a, 1) - (__int8)_mm_extract_epi8(a, 0))
	);
}
#endif

#if !ARM
#undef _mm_hsubd_epi16
#define _mm_hsubd_epi16(a) _mm_hsubd_epi16_impl(a)
__forceinline __m128i_i32 _mm_hsubd_epi16_impl(__m128i_i16 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epi32
	(
		(__int32)((__int16)_mm_extract_epi16(a, 6) - (__int16)_mm_extract_epi16(a, 7)),
		(__int32)((__int16)_mm_extract_epi16(a, 4) - (__int16)_mm_extract_epi16(a, 5)),
		(__int32)((__int16)_mm_extract_epi16(a, 2) - (__int16)_mm_extract_epi16(a, 3)),
		(__int32)((__int16)_mm_extract_epi16(a, 0) - (__int16)_mm_extract_epi16(a, 1))
	);
}
#endif

#if !ARM
#undef _mm_hsubq_epi32
#define _mm_hsubq_epi32(a) _mm_hsubq_epi32_impl(a)
__forceinline __m128i_i64 _mm_hsubq_epi32_impl(__m128i_i32 a)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_set_epi64x
	(
		(__int64)_mm_extract_epi32(a, 2) - (__int64)_mm_extract_epi32(a, 3),
		(__int64)_mm_extract_epi32(a, 0) - (__int64)_mm_extract_epi32(a, 1)
	);
}
#endif

/***************************************************************************************************/
// Integer multiply-accumulate
/***************************************************************************************************/
#undef _mm_maccs_epi16
#define _mm_maccs_epi16(x,y,z) _mm_maccs_epi16_impl(x,y,z)
__forceinline __m128i_i16 _mm_maccs_epi16_impl(__m128i_i16 x, __m128i_i16 y, __m128i_i16 z) {
	return _mm_adds_epi16(_mm_mullo_epi16(x, y), z);
}

#undef _mm_macc_epi16
#define _mm_macc_epi16(x,y,z) _mm_macc_epi16_impl(x,y,z)
__forceinline __m128i_i16 _mm_macc_epi16_impl(__m128i_i16 x, __m128i_i16 y, __m128i_i16 z) {
	return _mm_add_epi16(_mm_mullo_epi16(x, y), z);
}

#undef _mm_maccsd_epi16
#define _mm_maccsd_epi16(x,y,z) _mm_maccsd_epi16_impl(x,y,z)
__forceinline __m128i_i32 _mm_maccsd_epi16_impl(__m128i_i16 x, __m128i_i16 y, __m128i_i32 z) {
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_adds_epi32
	(
		_mm_set_epi32
		(
			(__int32)(((__int16)_mm_extract_epi16(x, 6) * (__int16)_mm_extract_epi16(y, 6))),
			(__int32)(((__int16)_mm_extract_epi16(x, 4) * (__int16)_mm_extract_epi16(y, 4))),
			(__int32)(((__int16)_mm_extract_epi16(x, 2) * (__int16)_mm_extract_epi16(y, 2))),
			(__int32)(((__int16)_mm_extract_epi16(x, 0) * (__int16)_mm_extract_epi16(y, 0)))
			)
		, z
	);
}

#undef _mm_maccd_epi16
#define _mm_maccd_epi16(x,y,z) _mm_maccd_epi16_impl(x,y,z)
__forceinline __m128i_i32 _mm_maccd_epi16_impl(__m128i_i16 x, __m128i_i16 y, __m128i_i32 z) {

	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_add_epi32
	(
		_mm_set_epi32
		(
			(__int32)((__int16)_mm_extract_epi16(x, 6) * (__int16)_mm_extract_epi16(y, 6)),
			(__int32)((__int16)_mm_extract_epi16(x, 4) * (__int16)_mm_extract_epi16(y, 4)),
			(__int32)((__int16)_mm_extract_epi16(x, 2) * (__int16)_mm_extract_epi16(y, 2)),
			(__int32)((__int16)_mm_extract_epi16(x, 0) * (__int16)_mm_extract_epi16(y, 0))
		),z
	);
}

#undef _mm_maddsd_epi16
#define _mm_maddsd_epi16(x,y,z) _mm_maddsd_epi16_impl(x,y,z)
__forceinline __m128i_i32 _mm_maddsd_epi16_impl(__m128i_i16 x, __m128i_i16 y, __m128i_i32 z) {

	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_adds_epi32
	(
		_mm_adds_epi32
		(
			_mm_set_epi32
			(
				(__int32)(((__int16)_mm_extract_epi16(x, 6) * (__int16)_mm_extract_epi16(y, 6))),
				(__int32)(((__int16)_mm_extract_epi16(x, 4) * (__int16)_mm_extract_epi16(y, 4))),
				(__int32)(((__int16)_mm_extract_epi16(x, 2) * (__int16)_mm_extract_epi16(y, 2))),
				(__int32)(((__int16)_mm_extract_epi16(x, 0) * (__int16)_mm_extract_epi16(y, 0)))
				),
			_mm_set_epi32
			(
				(__int32)(((__int16)_mm_extract_epi16(x, 7) * (__int16)_mm_extract_epi16(y, 7))),
				(__int32)(((__int16)_mm_extract_epi16(x, 5) * (__int16)_mm_extract_epi16(y, 5))),
				(__int32)(((__int16)_mm_extract_epi16(x, 3) * (__int16)_mm_extract_epi16(y, 3))),
				(__int32)(((__int16)_mm_extract_epi16(x, 1) * (__int16)_mm_extract_epi16(y, 1)))
				)
			)
		, z
		);

}

#undef _mm_maddd_epi16
#define _mm_maddd_epi16(x,y,z) _mm_maddd_epi16_impl(x,y,z)
__forceinline __m128i_i32 _mm_maddd_epi16_impl(__m128i_i16 x, __m128i_i16 y, __m128i_i32 z) {

	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_add_epi32
	(
		_mm_add_epi32
		(
			_mm_set_epi32
			(
				(__int32)(((__int16)_mm_extract_epi16(x, 6) * (__int16)_mm_extract_epi16(y, 6))),
				(__int32)(((__int16)_mm_extract_epi16(x, 4) * (__int16)_mm_extract_epi16(y, 4))),
				(__int32)(((__int16)_mm_extract_epi16(x, 2) * (__int16)_mm_extract_epi16(y, 2))),
				(__int32)(((__int16)_mm_extract_epi16(x, 0) * (__int16)_mm_extract_epi16(y, 0)))
				),
			_mm_set_epi32
			(
				(__int32)(((__int16)_mm_extract_epi16(x, 7) * (__int16)_mm_extract_epi16(y, 7))),
				(__int32)(((__int16)_mm_extract_epi16(x, 5) * (__int16)_mm_extract_epi16(y, 5))),
				(__int32)(((__int16)_mm_extract_epi16(x, 3) * (__int16)_mm_extract_epi16(y, 3))),
				(__int32)(((__int16)_mm_extract_epi16(x, 1) * (__int16)_mm_extract_epi16(y, 1)))
				)
			)
		, z
		);

}

#undef _mm_maccs_epi32
#define _mm_maccs_epi32(x,y,z) _mm_maccs_epi32_impl(x,y,z)
__forceinline __m128i_i32 _mm_maccs_epi32_impl(__m128i_i32 x, __m128i_i32 y, __m128i_i32 z) {
	return _mm_adds_epi32(_mm_mul_epi32(x, y), z);
}

#if !ARM
#undef _mm_macc_epi32
#define _mm_macc_epi32(x,y,z) _mm_macc_epi32_impl(x,y,z)
__forceinline __m128i_i32 _mm_macc_epi32_impl(__m128i_i32 x, __m128i_i32 y, __m128i_i32 z) {
	return _mm_add_epi32(_mm_mul_epi32(x, y), z);
}
#endif

#if BIT64
#undef _mm_maccslo_epi32
#define _mm_maccslo_epi32(x,y,z) _mm_maccslo_epi32_impl(x,y,z)
__forceinline __m128i_i64 _mm_maccslo_epi32_impl(__m128i_i32 x, __m128i_i32 y, __m128i_i64 z) {
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_adds_epi64
	(
		_mm_set_epi64x
		(
			(__int64)((__int64)_mm_extract_epi32(x, 2) * (__int64)_mm_extract_epi32(y, 2)),
			(__int64)((__int64)_mm_extract_epi32(x, 0) * (__int64)_mm_extract_epi32(y, 0))
			)
		, z
		);
}
#endif
#undef _mm_macclo_epi32
#define _mm_macclo_epi32(x,y,z) _mm_macclo_epi32_impl(x,y,z)
__forceinline __m128i_i64 _mm_macclo_epi32_impl(__m128i_i32 x, __m128i_i32 y, __m128i_i64 z) {
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_add_epi64
	(
		_mm_set_epi64x
		(
			(__int64)((__int64)_mm_extract_epi32(x, 2) * (__int64)_mm_extract_epi32(y, 2)),
			(__int64)((__int64)_mm_extract_epi32(x, 0) * (__int64)_mm_extract_epi32(y, 0))
			)
		, z
		);
}

#if BIT64
#undef _mm_maccshi_epi32
#define _mm_maccshi_epi32(x,y,z) _mm_maccshi_epi32_impl(x,y,z)
__forceinline __m128i_i64 _mm_maccshi_epi32_impl(__m128i_i32 x, __m128i_i32 y, __m128i_i64 z) {
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_adds_epi64
	(
		_mm_set_epi64x
		(
			(__int64)((__int64)_mm_extract_epi32(x, 3) * (__int64)_mm_extract_epi32(y, 3)),
			(__int64)((__int64)_mm_extract_epi32(x, 1) * (__int64)_mm_extract_epi32(y, 1))
			)
		, z
		);
}
#endif

#if !ARM64
#undef _mm_macchi_epi32
#define _mm_macchi_epi32(x,y,z) _mm_macchi_epi32_impl(x,y,z)
__forceinline __m128i_i64 _mm_macchi_epi32_impl(__m128i_i32 x, __m128i_i32 y, __m128i_i64 z) {
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)
	return _mm_add_epi64
	(
		_mm_set_epi64x
		(
			(__int64)((__int64)_mm_extract_epi32(x, 3) * (__int64)_mm_extract_epi32(y, 3)),
			(__int64)((__int64)_mm_extract_epi32(x, 1) * (__int64)_mm_extract_epi32(y, 1))
			)
		, z
		);
}
#endif


/***************************************************************************************************/
// Vector conditional moves
/***************************************************************************************************/
//__m128i _mm_cmov_si128(__m128i a, __m128i b, __m128i mask);
#undef _mm_cmov_si128
#define _mm_cmov_si128(a, b, mask) _mm_or_si128(_mm_andnot_si128(mask, b), _mm_and_si128(mask, a))

//__m256i _mm256_cmov_si256(__m256i, __m256i, __m256i);
#if __AVX2__
#undef _mm256_cmov_si256
#define _mm256_cmov_si256(a, b, mask) _mm256_or_si256(_mm256_andnot_si256(mask, b), _mm256_and_si256(mask, a))
#endif
/***************************************************************************************************/
#if __AVX2__
#undef _mm256_cmov_epu8
#define _mm256_cmov_epu8(a,b,mask) _mm256_cmov_epu8_impl(a,b,mask)
__forceinline __m256i_u8 _mm256_cmov_epu8_impl(__m256i_u8 a, __m256i_u8 b, __m256i_u8 mask)
{
	return _mm256_or_epu8(_mm256_andnot_epu8(mask, b), _mm256_and_epu8(mask, a));
}

#undef _mm256_cmov_epu16
#define _mm256_cmov_epu16(a,b,mask) _mm256_cmov_epu16_impl(a,b,mask)
__forceinline __m256i_u16 _mm256_cmov_epu16_impl(__m256i_u16 a, __m256i_u16 b, __m256i_u16 mask)
{
	return _mm256_or_epu16(_mm256_andnot_epu16(mask, b), _mm256_and_epu16(mask, a));
}

#undef _mm256_cmov_epu32
#define _mm256_cmov_epu32(a,b,mask) _mm256_cmov_epu32_impl(a,b,mask)
__forceinline __m256i_u32 _mm256_cmov_epu32_impl(__m256i_u32 a, __m256i_u32 b, __m256i_u32 mask)
{
	return _mm256_or_epu32(_mm256_andnot_epu32(mask, b), _mm256_and_epu32(mask, a));
}

#undef _mm256_cmov_epu64
#define _mm256_cmov_epu64(a,b,mask) _mm256_cmov_epu64_impl(a,b,mask)
__forceinline __m256i_u64 _mm256_cmov_epu64_impl(__m256i_u64 a, __m256i_u64 b, __m256i_u64 mask)
{
	return _mm256_or_epu64(_mm256_andnot_epu64(mask, b), _mm256_and_epu64(mask, a));
}
/***************************************************************************************************/
#undef _mm256_cmov_epi8
#define _mm256_cmov_epi8(a,b,mask) _mm256_cmov_epi8_impl(a,b,mask)
__forceinline __m256i_i8 _mm256_cmov_epi8_impl(__m256i_i8 a, __m256i_i8 b, __m256i_i8 mask)
{
	return _mm256_or_epi8(_mm256_andnot_epi8(mask, b), _mm256_and_epi8(mask, a));
}

#undef _mm256_cmov_epi16
#define _mm256_cmov_epi16(a,b,mask) _mm256_cmov_epi16_impl(a,b,mask)
__forceinline __m256i_i16 _mm256_cmov_epi16_impl(__m256i_i16 a, __m256i_i16 b, __m256i_i16 mask)
{
	return _mm256_or_epi16(_mm256_andnot_epi16(mask, b), _mm256_and_epi16(mask, a));
}

#undef _mm256_cmov_epi32
#define _mm256_cmov_epi32(a,b,mask) _mm256_cmov_epi32_impl(a,b,mask)
__forceinline __m256i_i32 _mm256_cmov_epi32_impl(__m256i_i32 a, __m256i_i32 b, __m256i_i32 mask)
{
	return _mm256_or_epi32(_mm256_andnot_epi32(mask, b), _mm256_and_epi32(mask, a));
}

#undef _mm256_cmov_epi64
#define _mm256_cmov_epi64(a,b,mask) _mm256_cmov_epi64_impl(a,b,mask)
__forceinline __m256i_i64 _mm256_cmov_epi64_impl(__m256i_i64 a, __m256i_i64 b, __m256i_i64 mask)
{
	return _mm256_or_epi64(_mm256_andnot_epi64(mask, b), _mm256_and_epi64(mask, a));
}
#endif
/***************************************************************************************************/

#undef _mm_cmov_pd
#define _mm_cmov_pd(a,b,mask) _mm_cmov_pd_impl(a,b,mask)
__forceinline __m128d _mm_cmov_pd_impl(__m128d a, __m128d b, __m128d mask)
{
	return _mm_or_pd(_mm_andnot_pd(mask, b), _mm_and_pd(mask, a));
}
#undef _mm_cmov_ps
#define _mm_cmov_ps(a,b,mask) _mm_cmov_ps_impl(a,b,mask)
__forceinline __m128 _mm_cmov_ps_impl(__m128 a, __m128 b, __m128 mask)
{
	return _mm_or_ps(_mm_andnot_ps(mask, b), _mm_and_ps(mask, a));
}

/***************************************************************************************************/
#if __AVX__
#undef _mm256_cmov_pd
#define _mm256_cmov_pd(a,b,mask) _mm256_cmov_pd_impl(a,b,mask)
__forceinline __m256d _mm256_cmov_pd_impl(__m256d a, __m256d b, __m256d mask)
{
	return _mm256_or_pd(_mm256_andnot_pd(mask, b), _mm256_and_pd(mask, a));
}
#undef _mm256_cmov_ps
#define _mm256_cmov_ps(a,b,mask) _mm256_cmov_ps_impl(a,b,mask)
__forceinline __m256 _mm256_cmov_ps_impl(__m256 a, __m256 b, __m256 mask)
{
	return _mm256_or_ps(_mm256_andnot_ps(mask, b), _mm256_and_ps(mask, a));
}
#endif
/***************************************************************************************************/


/***************************************************************************************************/
// Rotates
/***************************************************************************************************/
#if BIT64
#undef _mm_rot_epi8
#define _mm_rot_epi8(src,counts) _mm_rot_epi8_impl(src,counts)
__forceinline __m128i_i8 _mm_rot_epi8_impl(__m128i_i8 src, __m128i_i8 counts)
{
	return _mm_rolv_epi8(src, counts);
}
#endif

#undef _mm_rot_epi16
#define _mm_rot_epi16(src,counts) _mm_rot_epi16_impl(src,counts)
__forceinline __m128i_i16 _mm_rot_epi16_impl(__m128i_i16 src, __m128i_i16 counts)
{
	return _mm_rolv_epi16(src, counts);
}

#undef _mm_rot_epi32
#define _mm_rot_epi32(src,counts) _mm_rot_epi32_impl(src,counts)
__forceinline __m128i_i32 _mm_rot_epi32_impl(__m128i_i32 src, __m128i_i32 counts)
{
	return _mm_rolv_epi32(src, counts);
}

#if BIT64
#undef _mm_rot_epi64
#define _mm_rot_epi64(src,counts) _mm_rot_epi64_impl(src,counts)
__forceinline __m128i_i64 _mm_rot_epi64_impl(__m128i_i64 src, __m128i_i64 counts){
	return _mm_rolv_epi64(src, counts);
}
#endif

#if BIT64
#undef _mm_roti_epi8
#define _mm_roti_epi8(src,counts) _mm_roti_epi8_impl(src,counts)
__forceinline __m128i_i8 _mm_roti_epi8_impl(__m128i_i8 src, int counts)
{
	return _mm_rol_epi8(src, counts);
}
#endif

#undef _mm_roti_epi16
#define _mm_roti_epi16(src,counts) _mm_roti_epi16_impl(src,counts)
__forceinline __m128i_i16 _mm_roti_epi16_impl(__m128i_i16 src, int counts)
{
	return _mm_rol_epi16(src, counts);
}

#undef _mm_roti_epi32
#define _mm_roti_epi32(src,counts) _mm_roti_epi32_impl(src,counts)
__forceinline __m128i_i32 _mm_roti_epi32_impl(__m128i_i32 src, int counts)
{
	return _mm_rol_epi32(src, counts);
}

#if BIT64
#undef _mm_roti_epi64
#define _mm_roti_epi64(src,counts) _mm_roti_epi64_impl(src,counts)
__forceinline __m128i_i64 _mm_roti_epi64_impl(__m128i_i64 src, int counts)
{
	return _mm_rol_epi64(src, counts);
}
#endif

/***************************************************************************************************/
// Vector shifts Arithmatic
/***************************************************************************************************/

#undef _mm_sha_epi8
#define _mm_sha_epi8(src,counts) _mm_sha_epi8_impl(src,counts)
__forceinline __m128i_i8 _mm_sha_epi8_impl(__m128i_i8 src, __m128i_i8 counts)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)

	union __v128i
	{
		__m128i_i8 m128i;

		__uint8 m128i_u8[16];
		__int8 m128i_i8[16];
		__uint16 m128i_u16[8];
		__int16 m128i_i16[8];
		__uint32 m128i_u32[4];
		__int32 m128i_i32[4];
		__uint64 m128i_u64[2];
		__int64 m128i_i64[2];

	};

	__v128i ret;
	__v128i s;
	__v128i c;

	s.m128i = src;
	c.m128i = counts;

	ret.m128i_u8[0] = (c.m128i_i8[0] >= 0) ? s.m128i_u8[0] << c.m128i_i8[0] : (s.m128i_u8[0] >> (-c.m128i_i8[0]) | ~(~0U >> -c.m128i_i8[0]));
	ret.m128i_u8[1] = (c.m128i_i8[1] >= 0) ? s.m128i_u8[1] << c.m128i_i8[1] : (s.m128i_u8[1] >> (-c.m128i_i8[1]) | ~(~0U >> -c.m128i_i8[1]));
	ret.m128i_u8[2] = (c.m128i_i8[2] >= 0) ? s.m128i_u8[2] << c.m128i_i8[2] : (s.m128i_u8[2] >> (-c.m128i_i8[2]) | ~(~0U >> -c.m128i_i8[2]));
	ret.m128i_u8[3] = (c.m128i_i8[3] >= 0) ? s.m128i_u8[3] << c.m128i_i8[3] : (s.m128i_u8[3] >> (-c.m128i_i8[3]) | ~(~0U >> -c.m128i_i8[3]));
	ret.m128i_u8[4] = (c.m128i_i8[4] >= 0) ? s.m128i_u8[4] << c.m128i_i8[4] : (s.m128i_u8[4] >> (-c.m128i_i8[4]) | ~(~0U >> -c.m128i_i8[4]));
	ret.m128i_u8[5] = (c.m128i_i8[5] >= 0) ? s.m128i_u8[5] << c.m128i_i8[5] : (s.m128i_u8[5] >> (-c.m128i_i8[5]) | ~(~0U >> -c.m128i_i8[5]));
	ret.m128i_u8[6] = (c.m128i_i8[6] >= 0) ? s.m128i_u8[6] << c.m128i_i8[6] : (s.m128i_u8[6] >> (-c.m128i_i8[6]) | ~(~0U >> -c.m128i_i8[6]));
	ret.m128i_u8[7] = (c.m128i_i8[7] >= 0) ? s.m128i_u8[7] << c.m128i_i8[7] : (s.m128i_u8[7] >> (-c.m128i_i8[7]) | ~(~0U >> -c.m128i_i8[7]));
	ret.m128i_u8[8] = (c.m128i_i8[8] >= 0) ? s.m128i_u8[8] << c.m128i_i8[8] : (s.m128i_u8[8] >> (-c.m128i_i8[8]) | ~(~0U >> -c.m128i_i8[8]));
	ret.m128i_u8[9] = (c.m128i_i8[9] >= 0) ? s.m128i_u8[9] << c.m128i_i8[9] : (s.m128i_u8[9] >> (-c.m128i_i8[9]) | ~(~0U >> -c.m128i_i8[9]));
	ret.m128i_u8[10] = (c.m128i_i8[10] >= 0) ? s.m128i_u8[10] << c.m128i_i8[10] : (s.m128i_u8[10] >> (-c.m128i_i8[10]) | ~(~0U >> -c.m128i_i8[10]));
	ret.m128i_u8[11] = (c.m128i_i8[11] >= 0) ? s.m128i_u8[11] << c.m128i_i8[11] : (s.m128i_u8[11] >> (-c.m128i_i8[11]) | ~(~0U >> -c.m128i_i8[11]));
	ret.m128i_u8[12] = (c.m128i_i8[12] >= 0) ? s.m128i_u8[12] << c.m128i_i8[12] : (s.m128i_u8[12] >> (-c.m128i_i8[12]) | ~(~0U >> -c.m128i_i8[12]));
	ret.m128i_u8[13] = (c.m128i_i8[13] >= 0) ? s.m128i_u8[13] << c.m128i_i8[13] : (s.m128i_u8[13] >> (-c.m128i_i8[13]) | ~(~0U >> -c.m128i_i8[13]));
	ret.m128i_u8[14] = (c.m128i_i8[14] >= 0) ? s.m128i_u8[14] << c.m128i_i8[14] : (s.m128i_u8[14] >> (-c.m128i_i8[14]) | ~(~0U >> -c.m128i_i8[14]));
	ret.m128i_u8[15] = (c.m128i_i8[15] >= 0) ? s.m128i_u8[15] << c.m128i_i8[15] : (s.m128i_u8[15] >> (-c.m128i_i8[15]) | ~(~0U >> -c.m128i_i8[15]));

	return ret.m128i;
}

#undef _mm_sha_epi16
#define _mm_sha_epi16(src,counts) _mm_sha_epi16_impl(src,counts)
__forceinline __m128i_i16 _mm_sha_epi16_impl(__m128i_i16 src, __m128i_i16 counts)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)

	union __v128i
	{
		__m128i_i16 m128i;

		__uint8 m128i_u8[16];
		__int8 m128i_i8[16];
		__uint16 m128i_u16[8];
		__int16 m128i_i16[8];
		__uint32 m128i_u32[4];
		__int32 m128i_i32[4];
		__uint64 m128i_u64[2];
		__int64 m128i_i64[2];

	};

	__v128i ret;
	__v128i s;
	__v128i c;

	s.m128i = src;
	c.m128i = counts;

	ret.m128i_u16[0] = (c.m128i_i8[0] >= 0) ? s.m128i_u16[0] << c.m128i_i8[0] : (s.m128i_u16[0] >> (-c.m128i_i8[0]) | ~(~0U >> -c.m128i_i8[0]));
	ret.m128i_u16[1] = (c.m128i_i8[2] >= 0) ? s.m128i_u16[1] << c.m128i_i8[3] : (s.m128i_u16[1] >> (-c.m128i_i8[2]) | ~(~0U >> -c.m128i_i8[2]));
	ret.m128i_u16[2] = (c.m128i_i8[4] >= 0) ? s.m128i_u16[2] << c.m128i_i8[4] : (s.m128i_u16[2] >> (-c.m128i_i8[4]) | ~(~0U >> -c.m128i_i8[4]));
	ret.m128i_u16[3] = (c.m128i_i8[6] >= 0) ? s.m128i_u16[3] << c.m128i_i8[6] : (s.m128i_u16[3] >> (-c.m128i_i8[6]) | ~(~0U >> -c.m128i_i8[6]));
	ret.m128i_u16[4] = (c.m128i_i8[8] >= 0) ? s.m128i_u16[4] << c.m128i_i8[8] : (s.m128i_u16[4] >> (-c.m128i_i8[8]) | ~(~0U >> -c.m128i_i8[8]));
	ret.m128i_u16[5] = (c.m128i_i8[10] >= 0) ? s.m128i_u16[5] << c.m128i_i8[10] : (s.m128i_u16[5] >> (-c.m128i_i8[10]) | ~(~0U >> -c.m128i_i8[10]));
	ret.m128i_u16[6] = (c.m128i_i8[12] >= 0) ? s.m128i_u16[6] << c.m128i_i8[12] : (s.m128i_u16[6] >> (-c.m128i_i8[12]) | ~(~0U >> -c.m128i_i8[12]));
	ret.m128i_u16[7] = (c.m128i_i8[14] >= 0) ? s.m128i_u16[7] << c.m128i_i8[14] : (s.m128i_u16[7] >> (-c.m128i_i8[14]) | ~(~0U >> -c.m128i_i8[14]));

	return ret.m128i;
}

#undef _mm_sha_epi32
#define _mm_sha_epi32(src,counts) _mm_sha_epi32_impl(src,counts)
__forceinline __m128i_i32 _mm_sha_epi32_impl(__m128i_i32 src, __m128i_i32 counts)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)

	union __v128i
	{
		__m128i_i32 m128i;

		__uint8 m128i_u8[16];
		__int8 m128i_i8[16];
		__uint16 m128i_u16[8];
		__int16 m128i_i16[8];
		__uint32 m128i_u32[4];
		__int32 m128i_i32[4];
		__uint64 m128i_u64[2];
		__int64 m128i_i64[2];

	};

	__v128i ret;
	__v128i s;
	__v128i c;

	s.m128i = src;
	c.m128i = counts;

	ret.m128i_u32[0] = (c.m128i_i8[0] >= 0) ? s.m128i_u32[0] << c.m128i_i8[0] : (s.m128i_u32[0] >> (-c.m128i_i8[0]) | ~(~0U >> -c.m128i_i8[0]));
	ret.m128i_u32[1] = (c.m128i_i8[4] >= 0) ? s.m128i_u32[1] << c.m128i_i8[4] : (s.m128i_u32[1] >> (-c.m128i_i8[4]) | ~(~0U >> -c.m128i_i8[4]));
	ret.m128i_u32[2] = (c.m128i_i8[8] >= 0) ? s.m128i_u32[2] << c.m128i_i8[8] : (s.m128i_u32[2] >> (-c.m128i_i8[8]) | ~(~0U >> -c.m128i_i8[8]));
	ret.m128i_u32[3] = (c.m128i_i8[12] >= 0) ? s.m128i_u32[3] << c.m128i_i8[12] : (s.m128i_u32[3] >> (-c.m128i_i8[12]) | ~(~0U >> -c.m128i_i8[12]));

	return ret.m128i;
}

#undef _mm_sha_epi64
#define _mm_sha_epi64(src,counts) _mm_sha_epi64_impl(src,counts)
__forceinline __m128i_i64 _mm_sha_epi64_impl(__m128i_i64 src, __m128i_i64 counts)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)

	union __v128i
	{
		__m128i_i64 m128i;

		__uint8 m128i_u8[16];
		__int8 m128i_i8[16];
		__uint16 m128i_u16[8];
		__int16 m128i_i16[8];
		__uint32 m128i_u32[4];
		__int32 m128i_i32[4];
		__uint64 m128i_u64[2];
		__int64 m128i_i64[2];

	};

	__v128i ret;
	__v128i s;
	__v128i c;

	s.m128i = src;
	c.m128i = counts;

	ret.m128i_u64[0] = (c.m128i_i8[0] >= 0) ? s.m128i_u64[0] << c.m128i_i8[0] : (s.m128i_u64[0] >> (-c.m128i_i8[0]) | ~(~0U >> -c.m128i_i8[0]));
	ret.m128i_u64[1] = (c.m128i_i8[8] >= 0) ? s.m128i_u64[1] << c.m128i_i8[8] : (s.m128i_u32[0] >> (-c.m128i_i8[8]) | ~(~0U >> -c.m128i_i8[8]));

	return ret.m128i;

}

/***************************************************************************************************/

/***************************************************************************************************/
// Vector shifts Logical
/***************************************************************************************************/
#if !ARM
#undef _mm_shl_epi8
#define _mm_shl_epi8(src,counts) _mm_shl_epi8_impl(src,counts)
__forceinline __m128i_i8 _mm_shl_epi8_impl(__m128i_i8 src, __m128i_i8 counts)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)

	union __v128i
	{
		__m128i_i8 m128i;

		__uint8 m128i_u8[16];
		__int8 m128i_i8[16];
		__uint16 m128i_u16[8];
		__int16 m128i_i16[8];
		__uint32 m128i_u32[4];
		__int32 m128i_i32[4];
		__uint64 m128i_u64[2];
		__int64 m128i_i64[2];

	};

	__v128i ret;
	__v128i s;
	__v128i c;

	s.m128i = src;
	c.m128i = counts;

	ret.m128i_u8[0] = (c.m128i_i8[0] >= 0) ? s.m128i_u8[0] << c.m128i_i8[0] : s.m128i_u8[0] >> (-c.m128i_i8[0]);
	ret.m128i_u8[1] = (c.m128i_i8[1] >= 0) ? s.m128i_u8[1] << c.m128i_i8[1] : s.m128i_u8[1] >> (-c.m128i_i8[1]);
	ret.m128i_u8[2] = (c.m128i_i8[2] >= 0) ? s.m128i_u8[2] << c.m128i_i8[2] : s.m128i_u8[2] >> (-c.m128i_i8[2]);
	ret.m128i_u8[3] = (c.m128i_i8[3] >= 0) ? s.m128i_u8[3] << c.m128i_i8[3] : s.m128i_u8[3] >> (-c.m128i_i8[3]);
	ret.m128i_u8[4] = (c.m128i_i8[4] >= 0) ? s.m128i_u8[4] << c.m128i_i8[4] : s.m128i_u8[4] >> (-c.m128i_i8[4]);
	ret.m128i_u8[5] = (c.m128i_i8[5] >= 0) ? s.m128i_u8[5] << c.m128i_i8[5] : s.m128i_u8[5] >> (-c.m128i_i8[5]);
	ret.m128i_u8[6] = (c.m128i_i8[6] >= 0) ? s.m128i_u8[6] << c.m128i_i8[6] : s.m128i_u8[6] >> (-c.m128i_i8[6]);
	ret.m128i_u8[7] = (c.m128i_i8[7] >= 0) ? s.m128i_u8[7] << c.m128i_i8[7] : s.m128i_u8[7] >> (-c.m128i_i8[7]);
	ret.m128i_u8[8] = (c.m128i_i8[8] >= 0) ? s.m128i_u8[8] << c.m128i_i8[8] : s.m128i_u8[8] >> (-c.m128i_i8[8]);
	ret.m128i_u8[9] = (c.m128i_i8[9] >= 0) ? s.m128i_u8[9] << c.m128i_i8[9] : s.m128i_u8[9] >> (-c.m128i_i8[9]);
	ret.m128i_u8[10] = (c.m128i_i8[10] >= 0) ? s.m128i_u8[10] << c.m128i_i8[10] : s.m128i_u8[10] >> (-c.m128i_i8[10]);
	ret.m128i_u8[11] = (c.m128i_i8[11] >= 0) ? s.m128i_u8[11] << c.m128i_i8[11] : s.m128i_u8[11] >> (-c.m128i_i8[11]);
	ret.m128i_u8[12] = (c.m128i_i8[12] >= 0) ? s.m128i_u8[12] << c.m128i_i8[12] : s.m128i_u8[12] >> (-c.m128i_i8[12]);
	ret.m128i_u8[13] = (c.m128i_i8[13] >= 0) ? s.m128i_u8[13] << c.m128i_i8[13] : s.m128i_u8[13] >> (-c.m128i_i8[13]);
	ret.m128i_u8[14] = (c.m128i_i8[14] >= 0) ? s.m128i_u8[14] << c.m128i_i8[14] : s.m128i_u8[14] >> (-c.m128i_i8[14]);
	ret.m128i_u8[15] = (c.m128i_i8[15] >= 0) ? s.m128i_u8[15] << c.m128i_i8[15] : s.m128i_u8[15] >> (-c.m128i_i8[15]);

	return ret.m128i;
}

#undef _mm_shl_epi16
#define _mm_shl_epi16(src,counts) _mm_shl_epi16_impl(src,counts)
__forceinline __m128i_i16 _mm_shl_epi16_impl(__m128i_i16 src, __m128i_i16 counts)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)

	union __v128i
	{
		__m128i_i16 m128i;

		__uint8 m128i_u8[16];
		__int8 m128i_i8[16];
		__uint16 m128i_u16[8];
		__int16 m128i_i16[8];
		__uint32 m128i_u32[4];
		__int32 m128i_i32[4];
		__uint64 m128i_u64[2];
		__int64 m128i_i64[2];

	};

	__v128i ret;
	__v128i s;
	__v128i c;

	s.m128i = src;
	c.m128i = counts;

	ret.m128i_u16[0] = (c.m128i_i8[0] >= 0) ? s.m128i_u16[0] << c.m128i_i8[0] : s.m128i_u16[0] >> (-c.m128i_i8[0]);
	ret.m128i_u16[1] = (c.m128i_i8[2] >= 0) ? s.m128i_u16[1] << c.m128i_i8[3] : s.m128i_u16[1] >> (-c.m128i_i8[2]);
	ret.m128i_u16[2] = (c.m128i_i8[4] >= 0) ? s.m128i_u16[2] << c.m128i_i8[4] : s.m128i_u16[2] >> (-c.m128i_i8[4]);
	ret.m128i_u16[3] = (c.m128i_i8[6] >= 0) ? s.m128i_u16[3] << c.m128i_i8[6] : s.m128i_u16[3] >> (-c.m128i_i8[6]);
	ret.m128i_u16[4] = (c.m128i_i8[8] >= 0) ? s.m128i_u16[4] << c.m128i_i8[8] : s.m128i_u16[4] >> (-c.m128i_i8[8]);
	ret.m128i_u16[5] = (c.m128i_i8[10] >= 0) ? s.m128i_u16[5] << c.m128i_i8[10] : s.m128i_u16[5] >> (-c.m128i_i8[10]);
	ret.m128i_u16[6] = (c.m128i_i8[12] >= 0) ? s.m128i_u16[6] << c.m128i_i8[12] : s.m128i_u16[6] >> (-c.m128i_i8[12]);
	ret.m128i_u16[7] = (c.m128i_i8[14] >= 0) ? s.m128i_u16[7] << c.m128i_i8[14] : s.m128i_u16[7] >> (-c.m128i_i8[14]);

	return ret.m128i;
}

#undef _mm_shl_epi32
#define _mm_shl_epi32(src,counts) _mm_shl_epi32_impl(src,counts)
__forceinline __m128i_i32 _mm_shl_epi32_impl(__m128i_i32 src, __m128i_i32 counts)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)

	union __v128i
	{
		__m128i_i32 m128i;

		__uint8 m128i_u8[16];
		__int8 m128i_i8[16];
		__uint16 m128i_u16[8];
		__int16 m128i_i16[8];
		__uint32 m128i_u32[4];
		__int32 m128i_i32[4];
		__uint64 m128i_u64[2];
		__int64 m128i_i64[2];

	};

	__v128i ret;
	__v128i s;
	__v128i c;

	s.m128i = src;
	c.m128i = counts;

	ret.m128i_u32[0] = (c.m128i_i8[0] >= 0) ? s.m128i_u32[0] << c.m128i_i8[0] : s.m128i_u32[0] >> (-c.m128i_i8[0]);
	ret.m128i_u32[1] = (c.m128i_i8[4] >= 0) ? s.m128i_u32[1] << c.m128i_i8[4] : s.m128i_u32[1] >> (-c.m128i_i8[4]);
	ret.m128i_u32[2] = (c.m128i_i8[8] >= 0) ? s.m128i_u32[2] << c.m128i_i8[8] : s.m128i_u32[2] >> (-c.m128i_i8[8]);
	ret.m128i_u32[3] = (c.m128i_i8[12] >= 0) ? s.m128i_u32[3] << c.m128i_i8[12] : s.m128i_u32[3] >> (-c.m128i_i8[12]);

	return ret.m128i;
}

#undef _mm_shl_epi64
#define _mm_shl_epi64(src,counts) _mm_shl_epi64_impl(src,counts)
__forceinline __m128i_i64 _mm_shl_epi64_impl(__m128i_i64 src, __m128i_i64 counts)
{
	// SISD version just as fast as SIMD version on AMD CPU!!!(TESTED)

	union __v128i
	{
		__m128i_i64 m128i;

		__uint8 m128i_u8[16];
		__int8 m128i_i8[16];
		__uint16 m128i_u16[8];
		__int16 m128i_i16[8];
		__uint32 m128i_u32[4];
		__int32 m128i_i32[4];
		__uint64 m128i_u64[2];
		__int64 m128i_i64[2];

	};

	__v128i ret;
	__v128i s;
	__v128i c;

	s.m128i = src;
	c.m128i = counts;

	ret.m128i_u64[0] = (c.m128i_i8[0] >= 0) ? s.m128i_u64[0] << c.m128i_i8[0] : s.m128i_u64[0] >> (-c.m128i_i8[0]);
	ret.m128i_u64[1] = (c.m128i_i8[8] >= 0) ? s.m128i_u64[1] << c.m128i_i8[8] : s.m128i_u64[1] >> (-c.m128i_i8[8]);

	return ret.m128i;

}
#endif
/***************************************************************************************************/


/***************************************************************************************************/
// Permutation
/***************************************************************************************************/
#if BIT64
#undef _mm_perm_epi8
#define _mm_perm_epi8(src1,src2,selector) _mm_perm_epi8_impl(src1,src2,selector)
__forceinline __m128i_i8 _mm_perm_epi8_impl(const __m128i_i8 src1, const __m128i_i8 src2, const __m128i_i8 selector)
{
	__m128i_i8 ret = _mm_setzero_epi8();
	__m128i_i8 bit_selector = _mm_srli_epi8(selector, 5);
	__m128i_i8 perm_selector = _mm_andnot_epi8(__m128i_i8_0xe0, selector);

	__m128i_i8 sel_000b = _mm_cmpeq_epi8(bit_selector, _mm_setzero_epi8());
	__m128i_i8 sel_001b = _mm_cmpeq_epi8(bit_selector, __m128i_i8_1);
	__m128i_i8 sel_010b = _mm_cmpeq_epi8(bit_selector, __m128i_i8_2);
	__m128i_i8 sel_011b = _mm_cmpeq_epi8(bit_selector, __m128i_i8_3);
	__m128i_i8 sel_100b = _mm_cmpeq_epi8(bit_selector, __m128i_i8_4);
	__m128i_i8 sel_101b = _mm_cmpeq_epi8(bit_selector, __m128i_i8_5);
	__m128i_i8 sel_110b = _mm_cmpeq_epi8(bit_selector, __m128i_i8_6);
	__m128i_i8 sel_111b = _mm_cmpeq_epi8(bit_selector, __m128i_i8_7);
	
	__m128i_i8 perm_selector_gt7 = _mm_cmpgt_epi8(perm_selector, __m128i_i8_7);

	__m128i_i8 s1 = _mm_shuffle_epi8 (src1, perm_selector);
	__m128i_i8 s2 = _mm_shuffle_epi8 (src2, _mm_sub_epi8(perm_selector, __m128i_i8_16));

	s1 = _mm_blendv_epi8(s1, _mm_setzero_epi8(), perm_selector_gt7);
	s1 = _mm_blendv_epi8(s2, _mm_setzero_epi8(), _mm_not_epi8(perm_selector_gt7));

	ret = _mm_or_epi8(s1, s2);

	ret = _mm_blendv_epi8(ret, _mm_not_epi8(_mm_srl_epi8(ret, __m128i_i8_7)), sel_111b);
	ret = _mm_blendv_epi8(ret, _mm_srl_epi8(ret, __m128i_i8_7), sel_110b);
	ret = _mm_blendv_epi8(ret, _mm_cmpeq_epi8(_mm_setzero_epi8(), _mm_setzero_epi8()), sel_101b);
	ret = _mm_blendv_epi8(ret, _mm_setzero_epi8(), sel_100b);
	ret = _mm_blendv_epi8(ret, _mm_not_epi8(_mm_bitreverse_epu8(_mm_castepi8_epu8(ret))), sel_011b);
	ret = _mm_blendv_epi8(ret, _mm_castepu8_epi8(_mm_bitreverse_epu8(_mm_castepi8_epu8(ret))), sel_010b);
	ret = _mm_blendv_epi8(ret, _mm_not_epi8(ret), sel_001b);
	ret = _mm_blendv_epi8(ret, ret, sel_000b);

	return ret;

}
#endif

#if __AVX__
#undef _mm_permute2_ps
#define _mm_permute2_ps(src1,src2,selector,control) _mm_permute2_ps_impl(src1,src2,selector,control)
__forceinline __m128 _mm_permute2_ps_impl(const __m128 src1, const __m128 src2, const __m128i_i32 selector, int control)
{

	__m128i_i32 mod_selector = _mm_rem_epi32(selector, __m128i_i32_8);

	__m128i_i32 selector_gt8 = _mm_cmpgt_epi32(selector, __m128i_i32_8);
	__m128i_i32 selector_gt4 = _mm_cmpgt_epi32(mod_selector, __m128i_i32_4);

	__m128 p1 = _mm_permutevar_ps(src1, mod_selector);
	__m128 p2 = _mm_permutevar_ps(src2, _mm_sub_epi32(mod_selector, __m128i_i32_4));

	p1 = _mm_blendv_ps(p1, _mm_setzero_ps(), _mm_castepi32_ps(selector_gt4));
	p2 = _mm_blendv_ps(p2, _mm_setzero_ps(), _mm_castepi32_ps(_mm_not_epi32(selector_gt4)));

	__m128 ret = _mm_or_ps(p1, p2);


	if (control == 0 || control == 1)
	{
		return ret;
	}

	if (control == 2)
	{
		return _mm_blendv_ps(ret, _mm_setzero_ps(), _mm_castepi32_ps(selector_gt8));
	}

	if (control == 3)
	{
		return _mm_blendv_ps(ret, _mm_setzero_ps(), _mm_castepi32_ps(_mm_not_epi32(selector_gt8)));
	}

	//DEFAULT Control Error
	return _mm_setzero_ps();

}

#undef _mm_permute2_pd
#define _mm_permute2_pd(src1,src2,selector,control) _mm_permute2_pd_impl(src1,src2,selector,control)
__forceinline __m128d _mm_permute2_pd_impl(const __m128d src1, const __m128d src2, const __m128i_i64 selector, int control)
{

	__m128i_i64 mod_selector = _mm_rem_epi64(selector, __m128i_i64_4);

	__m128i_i64 selector_gt4 = _mm_cmpgt_epi64(selector, __m128i_i64_4);
	__m128i_i64 selector_gt2 = _mm_cmpgt_epi64(mod_selector, __m128i_i64_2);

	__m128d p1 = _mm_permutevar_pd(src1, mod_selector);
	__m128d p2 = _mm_permutevar_pd(src2, _mm_sub_epi64(mod_selector, __m128i_i64_2));

	p1 = _mm_blendv_pd(p1, _mm_setzero_pd(), _mm_castepi64_pd(selector_gt2));
	p2 = _mm_blendv_pd(p2, _mm_setzero_pd(), _mm_castepi64_pd(_mm_not_epi64(selector_gt2)));

	__m128d ret = _mm_or_pd(p1, p2);


	if (control == 0 || control == 1)
	{
		return ret;
	}

	if (control == 2)
	{
		return _mm_blendv_pd(ret, _mm_setzero_pd(), _mm_castepi64_pd(selector_gt4));
	}

	if (control == 3)
	{
		return _mm_blendv_pd(ret, _mm_setzero_pd(), _mm_castepi64_pd(_mm_not_epi64(selector_gt4)));
	}

	//DEFAULT Control Error
	return _mm_setzero_pd();

}
#endif

#if __AVX2__
#undef _mm256_permute2_ps
#define _mm256_permute2_ps(src1,src2,selector,control) _mm256_permute2_ps_impl(src1,src2,selector,control)
__forceinline __m256 _mm256_permute2_ps_impl(const __m256 src1, const __m256 src2, const __m256i_i32 selector, int control)
{
	//DOSE NOT USE MOD 16 BUT MOD 8 LIKE "_mm_permute2_ps"!!!
	__m256i_i32 mod_selector = _mm256_rem_epi32(selector, __m256i_i32_8);

	__m256i_i32 selector_gt8 = _mm256_cmpgt_epi32(selector, __m256i_i32_8);
	__m256i_i32 selector_gt4 = _mm256_cmpgt_epi32(mod_selector, __m256i_i32_4);

	__m256 p1 = _mm256_permutevar_ps(src1, mod_selector);
	__m256 p2 = _mm256_permutevar_ps(src2, _mm256_sub_epi32(mod_selector, __m256i_i32_4));

	p1 = _mm256_blendv_ps(p1, _mm256_setzero_ps(), _mm256_castepi32_ps(selector_gt4));
	p2 = _mm256_blendv_ps(p2, _mm256_setzero_ps(), _mm256_castepi32_ps(_mm256_not_epi32(selector_gt4)));

	__m256 ret = _mm256_or_ps(p1, p2);


	if (control == 0 || control == 1)
	{
		return ret;
	}

	if (control == 2)
	{
		return _mm256_blendv_ps(ret, _mm256_setzero_ps(), _mm256_castepi32_ps(selector_gt8));
	}

	if (control == 3)
	{
		return _mm256_blendv_ps(ret, _mm256_setzero_ps(), _mm256_castepi32_ps(_mm256_not_epi32(selector_gt8)));
	}

	//DEFAULT Control Error
	return _mm256_setzero_ps();
}

#undef _mm256_permute2_pd
#define _mm256_permute2_pd(src1,src2,selector,control) _mm256_permute2_pd_impl(src1,src2,selector,control)
__forceinline __m256d _mm256_permute2_pd_impl(const __m256d src1, const __m256d src2, const __m256i_i64 selector, int control)
{

	__m256i_i64 mod_selector = _mm256_rem_epi64(selector, __m256i_i64_8);

	__m256i_i64 selector_gt8 = _mm256_cmpgt_epi64(selector, __m256i_i64_8);
	__m256i_i64 selector_gt4 = _mm256_cmpgt_epi64(mod_selector, __m256i_i64_4);

	__m256d p1 = _mm256_permutevar_pd(src1, mod_selector);
	__m256d p2 = _mm256_permutevar_pd(src2, _mm256_sub_epi64(mod_selector, __m256i_i64_4));

	p1 = _mm256_blendv_pd(p1, _mm256_setzero_pd(), _mm256_castepi64_pd(selector_gt4));
	p2 = _mm256_blendv_pd(p2, _mm256_setzero_pd(), _mm256_castepi64_pd(_mm256_not_epi64(selector_gt4)));

	__m256d ret = _mm256_or_pd(p1, p2);


	if (control == 0 || control == 1)
	{
		return ret;
	}

	if (control == 2)
	{
		return _mm256_blendv_pd(ret, _mm256_setzero_pd(), _mm256_castepi64_pd(selector_gt8));
	}

	if (control == 3)
	{
		return _mm256_blendv_pd(ret, _mm256_setzero_pd(), _mm256_castepi64_pd(_mm256_not_epi64(selector_gt8)));
	}

	//DEFAULT Control Error
	return _mm256_setzero_pd();
}
#endif
/***************************************************************************************************/

