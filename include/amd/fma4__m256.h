/***************************************************************************************************/
// HEADER_NAME /amd/fma4_m256.h
/***************************************************************************************************/

#if FMA256
//Multiply Add 
#define _mm256_macc_ps _mm256_fmadd_ps

//Multiply Add Subtract 
#define _mm256_maddsub_ps _mm256_fmaddsub_ps

//Multiply Subtract 
#define _mm256_msub_ps _mm256_fmsub_ps

//Multiply Subtract Add 
#define _mm256_msubadd_ps _mm256_fmsubadd_ps

//Negative Multiply Add 
#define _mm256_nmacc_ps _mm256_fnmadd_ps

//Negative Multiply Subtract 
#define _mm256_nmsub_ps _mm256_fnmsub_ps
#endif

