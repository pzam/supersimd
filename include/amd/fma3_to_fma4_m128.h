/***************************************************************************************************/
// HEADER_NAME /amd/fma3_to_fma4_m128.h
/***************************************************************************************************/

//Multiply Add 
#define _mm_fmadd_ps _mm_macc_ps
#define _mm_fmadd_ss _mm_macc_ss

//Multiply Add Subtract 
#define _mm_fmaddsub_ps _mm_maddsub_ps


//Multiply Subtract 
#define _mm_fmsub_ps _mm_msub_ps
#define _mm_fmsub_ss _mm_msub_ss

//Multiply Subtract Add 
#define _mm_fmsubadd_ps _mm_msubadd_ps

//Negative Multiply Add 
#define _mm_fnmadd_ps _mm_nmacc_ps
#define _mm_fnmadd_ss _mm_nmacc_ss


//Negative Multiply Subtract 
#define _mm_fnmsub_ps _mm_nmsub_ps
#define _mm_fnmsub_ss _mm_nmsub_ss

