/***************************************************************************************************/
// HEADER_NAME /amd/fma4_m256d.h
/***************************************************************************************************/

#if FMA256
//Multiply Add 
#define _mm256_macc_pd _mm256_fmadd_pd

//Multiply Add Subtract 
#define _mm256_maddsub_pd _mm256_fmaddsub_pd

//Multiply Subtract 
#define _mm256_msub_pd _mm256_fmsub_pd

//Multiply Subtract Add 
#define _mm256_msubadd_pd _mm256_fmsubadd_pd

//Negative Multiply Add 
#define _mm256_nmacc_pd _mm256_fnmadd_pd

//Negative Multiply Subtract 
#define _mm256_nmsub_pd _mm256_fnmsub_pd
#endif

