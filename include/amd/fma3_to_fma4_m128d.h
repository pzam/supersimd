/***************************************************************************************************/
// HEADER_NAME /amd/fma3_to_fma4_m128d.h
/***************************************************************************************************/

#if BIT64
//Multiply Add 
#define _mm_fmadd_pd _mm_macc_pd
#define _mm_fmadd_sd _mm_macc_sd

//Multiply Add Subtract 
#define _mm_fmaddsub_pd _mm_maddsub_pd

//Multiply Subtract 
#define _mm_fmsub_pd _mm_msub_pd
#define _mm_fmsub_sd _mm_msub_sd

//Multiply Subtract Add 
#define _mm_fmsubadd_pd _mm_msubadd_pd

//Negative Multiply Add 
#define _mm_fnmadd_pd _mm_nmacc_pd
#define _mm_fnmadd_sd _mm_nmacc_sd


//Negative Multiply Subtract 
#define _mm_fnmsub_pd _mm_nmsub_pd
#define _mm_fnmsub_sd _mm_nmsub_sd
#endif

