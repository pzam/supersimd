/***************************************************************************************************/
// HEADER_NAME /amd/fma4_m128d.h
/***************************************************************************************************/

#if BIT64
//Multiply Add 
#define _mm_macc_pd _mm_fmadd_pd
#define _mm_macc_sd _mm_fmadd_sd

//Multiply Add Subtract 
#define _mm_maddsub_pd _mm_fmaddsub_pd

//Multiply Subtract 
#define _mm_msub_pd _mm_fmsub_pd
#define _mm_msub_sd _mm_fmsub_sd

//Multiply Subtract Add 
#define _mm_msubadd_pd _mm_fmsubadd_pd

//Negative Multiply Add 
#define _mm_nmacc_pd _mm_fnmadd_pd
#define _mm_nmacc_sd _mm_fnmadd_sd

//Negative Multiply Subtract 
#define _mm_nmsub_pd _mm_fnmsub_pd
#define _mm_nmsub_sd _mm_fnmsub_sd
#endif

