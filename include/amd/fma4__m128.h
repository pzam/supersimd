/***************************************************************************************************/
// HEADER_NAME /amd/fma4_m128.h
/***************************************************************************************************/

//Multiply Add 
#define _mm_macc_ps _mm_fmadd_ps
#define _mm_macc_ss _mm_fmadd_ss

//Multiply Add Subtract 
#define _mm_maddsub_ps _mm_fmaddsub_ps

//Multiply Subtract 
#define _mm_msub_ps _mm_fmsub_ps
#define _mm_msub_ss _mm_fmsub_ss

//Multiply Subtract Add 
#define _mm_msubadd_ps _mm_fmsubadd_ps

//Negative Multiply Add 

#define _mm_nmacc_ps _mm_fnmadd_ps
#define _mm_nmacc_ss _mm_fnmadd_ss

//Negative Multiply Subtract 
#define _mm_nmsub_ps _mm_fnmsub_ps
#define _mm_nmsub_ss _mm_fnmsub_ss

