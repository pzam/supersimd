/***************************************************************************************************/
// HEADER_NAME /amd/fma3_to_fma4_m256d.h
/***************************************************************************************************/

#if FMA256
//Multiply Add 
#define _mm256_fmadd_pd _mm256_macc_pd

//Multiply Add Subtract 
#define _mm256_fmaddsub_pd _mm256_maddsub_pd

//Multiply Subtract 
#define _mm256_fmsub_pd _mm256_msub_pd

//Multiply Subtract Add 
#define _mm256_fmsubadd_pd _mm256_msubadd_pd

//Negative Multiply Add 
#define _mm256_fnmadd_pd _mm256_nmacc_pd

//Negative Multiply Subtract 
#define _mm256_fnmsub_pd _mm256_nmsub_pd

#endif

