/***************************************************************************************************/
// Created by Promyk Zamojski Distributed under MIT License!
/***************************************************************************************************/
/***************************************************************************************************/
// HEADER_NAME /version/info.h
/***************************************************************************************************/


#define _SUPERSIMD 1									// Is True
#define _SUPERSIMD_VER 0.92_dev							// X.XX_dev Development Version
#define _SUPERSIMD_STABLE 0								// 0 is unstable 1 is stable


#define _SUPERSIMD_VEDNOR "Promyk Zamojski (Creator)"	// Distributor/Re-distributor Name

/*
About SuperSIMD Version

0.1 SVML __m128						(DONE)
0.2 SVML __m128d					(DONE)
0.3 SVML __m256						(DONE)
0.4 SVML __m256d					(DONE)
0.5 SVML __m128i					(DONE)
0.6 SVML __m256i					(DONE)
0.7 AMD64 FMA4 compatibility		(DONE)
0.8 ARM64 MMX/SSE 1-4/FMA			(DONE)
0.9 AMD64 XOP compatibility			(DONE)
	0.91 AVX2 Compatibility Mode	(WIP)
	0.92 Legacy[SSE2 BACKPORT]		(WIP)
	0.93 Memory
	0.94 DSP

...
1.0 STABLE
2.0 AVX-512(coming eventually when Intel ships 10th generation)
*/

