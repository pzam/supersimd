/***************************************************************************************************/
// HEADER_NAME /portable_types/portable_types.h
/***************************************************************************************************/
/**********************************************************************/
// SIMD Portable Extended Data Types
// 
// * More Readable.
// * Ensures portability between ARM64 and Intel/AMD.
/**********************************************************************/

/**********************************************************************/
// MMX SSE AVX
/**********************************************************************/

#if INTEL

// MMX
typedef __m64 		__m64i_u8;			//_pu8
typedef __m64 		__m64i_i8;			//_pi8
typedef __m64 		__m64i_u16;			//_pu16
typedef __m64 		__m64i_i16;			//_pi16
typedef __m64 		__m64i_u32;			//_pu32
typedef __m64 		__m64i_i32;			//_pi32
typedef __m64 		__m64i_u64;			//_pu64
typedef __m64 		__m64i_i64;			//_pi64

// SSE
typedef __m128i 	__m128i_u8;			//_epu8
typedef __m128i 	__m128i_i8;			//_epi8
typedef __m128i 	__m128i_u16;		//_epu16
typedef __m128i 	__m128i_i16;		//_epi16
typedef __m128i 	__m128i_u32;		//_epu32
typedef __m128i 	__m128i_i32;		//_epi32
typedef __m128i 	__m128i_u64;		//_epu64
typedef __m128i 	__m128i_i64;		//_epi64


// float  x 4 		__m128;				//_ps
// double x 2 		__m128d;			//_pd


// AVX
#if __AVX__
typedef __m256i 	__m256i_u8;			//_epu8
typedef __m256i 	__m256i_i8;			//_epi8
typedef __m256i 	__m256i_u16;		//_epu16
typedef __m256i 	__m256i_i16;		//_epi16
typedef __m256i 	__m256i_u32;		//_epu32
typedef __m256i 	__m256i_i32;		//_epi32
typedef __m256i 	__m256i_u64;		//_epu64
typedef __m256i 	__m256i_i64;		//_epi64


// float  x 4 		__m256;				//_ps
// double x 2 		__m256d;			//_pd
#endif

#endif

/**********************************************************************/
// ARM64 NEON
/**********************************************************************/
#if ARM

// MMX
typedef uint8x8_t		__m64i_u8;		//_pu8
typedef int8x8_t		__m64i_i8;		//_pi8
typedef uint16x4_t		__m64i_u16;		//_pu16
typedef int16x4_t		__m64i_i16;		//_pi16
typedef uint32x2_t		__m64i_u32;		//_pu32
typedef int32x2_t		__m64i_i32;		//_pi32
typedef uint64x1_t		__m64i_u64;		//_pu64
typedef int64x1_t		__m64i_i64;		//_pi64

typedef int16x4_t		__m64;			//default

// SSE
typedef uint8x16_t		__m128i_u8;		//_epu8
typedef int8x16_t		__m128i_i8;		//_epi8
typedef uint16x8_t		__m128i_u16; 	//_epu16
typedef int16x8_t		__m128i_i16;	//_epi16
typedef uint32x4_t		__m128i_u32;	//_epu32
typedef int32x4_t		__m128i_i32;	//_epi32
typedef uint64x2_t		__m128i_u64;	//_epu64
typedef int64x2_t		__m128i_i64;	//_epi64

typedef int32x4_t		__m128i;		//default


typedef float32x4_t 	__m128;			//_ps
#if ARM64
typedef float64x2_t 	__m128d;		//_pd
#endif

#endif

