/***************************************************************************************************/
// Horizontal add/subtract
/***************************************************************************************************/

__forceinline __m128i_i32 _mm_haddd_epi16(__m128i_i16 a)
{
	return vpaddlq_s16(a);
}

__forceinline __m128i_u32 _mm_haddd_epu16(__m128i_u16 a)
{
	return vpaddlq_u16(a);
}

__forceinline __m128i_i64 _mm_haddq_epi32(__m128i_i32 a)
{
	return vpaddlq_s32(a);
}

__forceinline __m128i_u64 _mm_haddq_epu32(__m128i_u32 a)
{
	return vpaddlq_u32(a);
}

__forceinline __m128i_i16 _mm_haddw_epi8(__m128i_i8 a)
{
	return vpaddlq_s8(a);
}

__forceinline __m128i_u16 _mm_haddw_epu8(__m128i_u8 a)
{
	return vpaddlq_u8(a);
}

__forceinline __m128i_i16 _mm_hsubw_epi8(__m128i_i8 a)
{
	int8_t ALIGNED(16) data[16] = { 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1 };
	int8x16_t sub = vld1q_s8(data);
	return vpaddlq_s8(vmulq_s8(a, sub));
}

__forceinline __m128i_i32 _mm_hsubd_epi16(__m128i_i16 a)
{
	int16_t ALIGNED(16) data[8] = { 1, -1, 1, -1, 1, -1, 1, -1 };
	int16x8_t sub = vld1q_s16(data);
	return vpaddlq_s16(vmulq_s16(a, sub));
}

__forceinline __m128i_i64 _mm_hsubq_epi32(__m128i_i32 a)
{
	int32_t ALIGNED(16) data[4] = { 1, -1, 1, -1 };
	int32x4_t sub = vld1q_s32(data);
	return vpaddlq_s32(vmulq_s32(a, sub));
}

/***************************************************************************************************/
// Integer multiply-accumulate
/***************************************************************************************************/

__forceinline __m128i_i32 _mm_macc_epi32(__m128i_i32 x, __m128i_i32 y, __m128i_i32 z) {
	return vmlaq_s32(z,x,y);
}


#if ARM64
__forceinline __m128i_i64 _mm_macchi_epi32(__m128i_i32 x, __m128i_i32 y, __m128i_i64 z) {
	return vmlal_high_s32(z,x,y);
}
#enif


/***************************************************************************************************/
// Vector shifts Logical
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_shl_epi8(__m128i_i8 src, __m128i_i8 counts)
{

	return vshlq_s8(src, counts);
}

__forceinline __m128i_i16 _mm_shl_epi16(__m128i_i16 src, __m128i_i16 counts)
{

	return vshlq_s16(src, counts);
}

__forceinline __m128i_i32 _mm_shl_epi32(__m128i_i32 src, __m128i_i32 counts)
{

	return vshlq_s32(src, counts);

}

#if BIT64
__forceinline __m128i_i64 _mm_shl_epi64(__m128i_i64 src, __m128i_i64 counts)
{

	return vshlq_s64(src, counts);

}
#endif