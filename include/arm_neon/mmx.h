/***************************************************************************************************/
// HEADER_NAME /arm_neon/mmx.h
/***************************************************************************************************/
#if ARM
#define __MMX__ 1
/***************************************************************************************************/
// Set Vector
/***************************************************************************************************/
__forceinline __m64i_i8 _mm_set_pi8 
(
__int8 e7, __int8 e6, __int8 e5, __int8 e4, 
__int8 e3, __int8 e2, __int8 e1, __int8 e0
)
{
#if _MSC_VER
	int8_t ALIGNED(8) data[8] = { e0, e1, e2, e3 ,e4, e5, e6, e7 };
#else
	__int8 ALIGNED(8) data[8] = { e0, e1, e2, e3 ,e4, e5, e6, e7 };
#endif
	return vld1_s8(data);
}

__forceinline __m64i_i16 _mm_set_pi16 (__int16 e3, __int16 e2, __int16 e1, __int16 e0)
{
	__int16 ALIGNED(8) data[4] = { e0, e1, e2, e3 };
	return vld1_s16(data);
}
__forceinline __m64i_i32 _mm_set_pi32 (__int32 e1, __int32 e0)
{
	__int32 ALIGNED(8) data[2] = { e0, e1 };
	return vld1_s32(data);
}

__forceinline __m64i_i8 _mm_set1_pi8 (__int8 a)
{
	return vdup_n_s8(a);
}
__forceinline __m64i_i16 _mm_set1_pi16 (__int16 a)
{
	return vdup_n_s16(a);
}
__forceinline __m64i_i32 _mm_set1_pi32 (__int32 a)
{
	return vdup_n_s32(a);
}
__forceinline __m64i_i8 _mm_setr_pi8 
(
__int8 e7, __int8 e6, __int8 e5, __int8 e4, 
__int8 e3, __int8 e2, __int8 e1, __int8 e0
)
{
#if _MSC_VER
	int8_t ALIGNED(8) data[8] = { e7, e6, e5, e4 ,e3, e2, e1, e0 };
#else
	__int8 ALIGNED(8) data[8] = { e7, e6, e5, e4 ,e3, e2, e1, e0 };
#endif
	return vld1_s8(data);
}
 __forceinline __m64i_i16 _mm_setr_pi16 (__int16 e3, __int16 e2, __int16 e1, __int16 e0) 
 {
	__int16 ALIGNED(8) data[4] = { e3, e2, e1, e0 };
	return vld1_s16(data);
}
__forceinline __m64i_i32 _mm_setr_pi32 (__int32 e1, __int32 e0)
{
	__int32 ALIGNED(8) data[2] = { e1, e0 };
	return vld1_s32(data);
}


/***************************************************************************************************/
// Set Zero
/***************************************************************************************************/
//DEFAULT 16BIT USE EXTENTIONS
__forceinline __m64i_i16 _mm_setzero_si64 (void)
{
	return vdup_n_s16(0);
	#pragma message("\
	Use _mm_setzero_pu8, _mm_setzero_pi8, \
	_mm_setzero_pu16, _mm_setzero_pu32, \
	_mm_setzero_pi32, _mm_setzero_pu64, \
	_mm_setzero_pi64 in place of _mm_setzero_si64 \
	")
}


/***************************************************************************************************/
// Comparator
/***************************************************************************************************/
__forceinline __m64i_i8 _mm_cmpeq_pi8 (__m64i_i8 a, __m64i_i8 b)
{
	return vreinterpret_s8_u8(vceq_s8(a,b));
}
__forceinline __m64i_i16 _mm_cmpeq_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	return vreinterpret_s16_u16(vceq_s16(a,b));
}
__forceinline __m64i_i32 _mm_cmpeq_pi32 (__m64i_i32 a, __m64i_i32 b)
{
	return vreinterpret_s32_u32(vceq_s32(a,b));
}
__forceinline __m64i_i8 _mm_cmpgt_pi8 (__m64i_i8 a, __m64i_i8 b)
{
	return vreinterpret_s8_u8(vcgt_s8(a,b));
}
__forceinline __m64i_i16 _mm_cmpgt_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	return vreinterpret_s16_u16(vcgt_s16(a,b));
}
__forceinline __m64i_i32 _mm_cmpgt_pi32 (__m64i_i32 a, __m64i_i32 b)
{
	return vreinterpret_s32_u32(vcgt_s32(a,b));
}

#define _m_pcmpeqb _mm_cmpeq_pi8
#define _m_pcmpeqw _mm_cmpeq_pi16
#define _m_pcmpeqd _mm_cmpeq_pi32
#define _m_pcmpgtb _mm_cmpgt_pi8
#define _m_pcmpgtw _mm_cmpgt_pi16
#define _m_pcmpgtd _mm_cmpgt_pi32


/***************************************************************************************************/
// Add
/***************************************************************************************************/
__forceinline __m64i_i8 _mm_add_pi8 (__m64i_i8 a, __m64i_i8 b)
{
	return  vadd_s8(a, b);
}
__forceinline __m64i_i16 _mm_add_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	return  vadd_s16(a, b);
}
__forceinline __m64i_i32 _mm_add_pi32 (__m64i_i32 a, __m64i_i32 b)
{
	return  vadd_s32(a, b);
}
__forceinline __m64i_i8 _mm_adds_pi8 (__m64i_i8 a, __m64i_i8 b)
{
	int8x8_t min = vdup_n_s8(-128);
	int8x8_t max = vdup_n_s8(127);
	
	int8x8_t bool_a_is_negative;
	int8x8_t bool_b_is_negative;
	int8x8_t bool_sum_is_negative;
	int8x8_t sum;

	bool_a_is_negative = vmvn_s8(vreinterpret_s8_u8(vceq_s8(vand_s8(a, max), vdup_n_s8(0))));
	bool_b_is_negative = vmvn_s8(vreinterpret_s8_u8(vceq_s8(vand_s8(b, min), vdup_n_s8(0))));

	sum = vadd_s8(a, b);
	bool_sum_is_negative = vmvn_s8(vreinterpret_s8_u8(vceq_s8(vand_s8(sum, min), vdup_n_s8(0))));

	sum = vbsl_s8( vreinterpret_u8_s8(vmvn_s8(vreinterpret_s8_u8(vceq_s8(bool_a_is_negative, bool_b_is_negative)))), vdup_n_s8(0), sum);
	sum = vbsl_s8( vreinterpret_u8_s8(vreinterpret_s8_u8(vceq_s8(bool_a_is_negative, vmvn_s8(bool_sum_is_negative)))), max, sum);
	sum = vbsl_s8( vreinterpret_u8_s8(vreinterpret_s8_u8(vceq_s8(vmvn_s8(bool_a_is_negative),bool_sum_is_negative))), min, sum);

	return sum;
}
__forceinline __m64i_i16 _mm_adds_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	int16x4_t min = vdup_n_s16(-32768);
	int16x4_t max = vdup_n_s16(32767);
	
	int16x4_t bool_a_is_negative;
	int16x4_t bool_b_is_negative;
	int16x4_t bool_sum_is_negative;
	int16x4_t sum;

	bool_a_is_negative = vmvn_s16(vreinterpret_s16_u16(vceq_s16(vand_s16(a, max), vdup_n_s16(0))));
	bool_b_is_negative = vmvn_s16(vreinterpret_s16_u16(vceq_s16(vand_s16(b, min), vdup_n_s16(0))));

	sum = vadd_s16(a, b);
	bool_sum_is_negative = vmvn_s16(vreinterpret_s16_u16(vceq_s16(vand_s16(sum, min), vdup_n_s16(0))));

	sum = vbsl_s16( vreinterpret_u16_s16(vmvn_s16(vreinterpret_s16_u16(vceq_s16(bool_a_is_negative, bool_b_is_negative)))), vdup_n_s16(0), sum);
	sum = vbsl_s16( vreinterpret_u16_s16(vreinterpret_s16_u16(vceq_s16(bool_a_is_negative, vmvn_s16(bool_sum_is_negative)))), max, sum);
	sum = vbsl_s16( vreinterpret_u16_s16(vreinterpret_s16_u16(vceq_s16(vmvn_s16(bool_a_is_negative),bool_sum_is_negative))), min, sum);

	return sum;
}

__forceinline __m64i_u8 _mm_adds_pu8 (__m64i_u8 a, __m64i_u8 b)
{
	uint8x8_t  sum;
	uint8x8_t  cmp;
	sum = vadd_u8(a, b);
	cmp = vclt_u8(sum,a);
	sum = vorr_u8(sum, cmp);
	
	return sum;
}
__forceinline __m64i_u16 _mm_adds_pu16 (__m64i_u16 a, __m64i_u16 b)
{
	uint16x4_t  sum;
	uint16x4_t  cmp;
	sum = vadd_u16(a, b);
	cmp = vclt_u16(sum,a);
	sum = vorr_u16(sum, cmp);
	
	return sum;
}

#define _m_paddb _mm_add_pi8
#define _m_paddw _mm_add_pi16
#define _m_paddd _mm_add_pi32
#define _m_paddsb _mm_adds_pi8
#define _m_paddsw _mm_adds_pi16
#define _m_paddusb _mm_adds_pu8
#define _m_paddusw _mm_adds_pu16


/***************************************************************************************************/
// Subtract
/***************************************************************************************************/
__forceinline __m64i_i8 _mm_sub_pi8 (__m64i_i8 a, __m64i_i8 b)
{
	return  vsub_s8(a, b);
}
__forceinline __m64i_i16 _mm_sub_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	return  vsub_s16(a, b);
}
__forceinline __m64i_i32 _mm_sub_pi32 (__m64i_i32 a, __m64i_i32 b)
{
	return  vsub_s32(a, b);
}
__forceinline __m64i_u8 _mm_subs_pu8 (__m64i_u8 a, __m64i_u8 b)
{
	uint8x8_t sum;
	uint8x8_t cmp;
	sum = vsub_u8(a, b);
	cmp = vcle_u8(sum, a);
	sum = vand_u8(sum, cmp);
	
	return sum;
}
__forceinline __m64i_u16 _mm_subs_pu16 (__m64i_u16 a, __m64i_u16 b)
{
	uint16x4_t sum;
	uint16x4_t cmp;
	sum = vsub_u16(a, b);
	cmp = vcle_u16(sum, a);
	sum = vand_u16(sum, cmp);
	
	return sum;
}
__forceinline __m64i_i8 _mm_subs_pi8 (__m64i_i8 a, __m64i_i8 b)
{
	int8x8_t min = vdup_n_s8(-128);
	int8x8_t max = vdup_n_s8(127);
	
	int8x8_t bool_a_is_negative;
	int8x8_t bool_b_is_negative;
	int8x8_t bool_sum_is_negative;
	int8x8_t sum;

	bool_a_is_negative = vmvn_s8(vreinterpret_s8_u8(vceq_s8(vand_s8(a, max), vdup_n_s8(0))));
	bool_b_is_negative = vmvn_s8(vreinterpret_s8_u8(vceq_s8(vand_s8(b, min), vdup_n_s8(0))));

	sum = vsub_s8(a, b);
	bool_sum_is_negative = vmvn_s8(vreinterpret_s8_u8(vceq_s8(vand_s8(sum, min), vdup_n_s8(0))));

	sum = vbsl_s8( vreinterpret_u8_s8(vmvn_s8(vreinterpret_s8_u8(vceq_s8(bool_a_is_negative, bool_b_is_negative)))), vdup_n_s8(0), sum);
	sum = vbsl_s8( vreinterpret_u8_s8(vreinterpret_s8_u8(vceq_s8(bool_a_is_negative, vmvn_s8(bool_sum_is_negative)))), max, sum);
	sum = vbsl_s8( vreinterpret_u8_s8(vreinterpret_s8_u8(vceq_s8(vmvn_s8(bool_a_is_negative),bool_sum_is_negative))), min, sum);

	return sum;
}
__forceinline __m64i_i16 _mm_subs_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	int16x4_t min = vdup_n_s16(-32768);
	int16x4_t max = vdup_n_s16(32767);
	
	int16x4_t bool_a_is_negative;
	int16x4_t bool_b_is_negative;
	int16x4_t bool_sum_is_negative;
	int16x4_t sum;

	bool_a_is_negative = vmvn_s16(vreinterpret_s16_u16(vceq_s16(vand_s16(a, max), vdup_n_s16(0))));
	bool_b_is_negative = vmvn_s16(vreinterpret_s16_u16(vceq_s16(vand_s16(b, min), vdup_n_s16(0))));

	sum = vsub_s16(a, b);
	bool_sum_is_negative = vmvn_s16(vreinterpret_s16_u16(vceq_s16(vand_s16(sum, min), vdup_n_s16(0))));

	sum = vbsl_s16( vreinterpret_u16_s16(vmvn_s16(vreinterpret_s16_u16(vceq_s16(bool_a_is_negative, bool_b_is_negative)))), vdup_n_s16(0), sum);
	sum = vbsl_s16( vreinterpret_u16_s16(vreinterpret_s16_u16(vceq_s16(bool_a_is_negative, vmvn_s16(bool_sum_is_negative)))), max, sum);
	sum = vbsl_s16( vreinterpret_u16_s16(vreinterpret_s16_u16(vceq_s16(vmvn_s16(bool_a_is_negative),bool_sum_is_negative))), min, sum);

	return sum;
}

#define _m_psubb _mm_sub_pi8
#define _m_psubw _mm_sub_pi16
#define _m_psubd _mm_sub_pi32
#define _m_psubusb _mm_subs_pu8
#define _m_psubusw _mm_subs_pu16
#define _m_psubsb _mm_subs_pi8
#define _m_psubsw _mm_subs_pi16


/***************************************************************************************************/
// Multiply
/***************************************************************************************************/
__forceinline __m64i_i32 _mm_madd_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	return vpaddl_s16(vmul_s16 (a, b));
}

__forceinline __m64i_i16 _mm_mulhi_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	return vshr_n_s16(vqdmulh_s16(a, b), 1);
}
__forceinline __m64i_i16 _mm_mullo_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	return vmul_s16(a, b);
}


#define _m_pmaddwd _mm_madd_pi16

#define _m_pmulhw _mm_mulhi_pi16
#define _m_pmullw _mm_mullo_pi16

/***************************************************************************************************/
// Shifting
/***************************************************************************************************/
__forceinline __m64i_i16 _mm_sll_pi16 (__m64i_i16 a, __m64i_i16 count)
{
	uint16x4_t gt15 = vcgt_s16(count, vdup_n_s16(15));
	int16x4_t ret = vshl_s16(a, count);
	return vbsl_s16 (gt15, ret, vdup_n_s16(0));
}
__forceinline __m64i_i32 _mm_sll_pi32 (__m64i_i32 a, __m64i_i32 count)
{
	uint32x2_t gt31 = vcgt_s32(count, vdup_n_s32(31));
	int32x2_t ret = vshl_s32(a, count);
	return vbsl_s32 (gt31, ret, vdup_n_s32(0));
}

#if ARM64
__forceinline __m64i_i64 _mm_sll_si64 (__m64i_i64 a, __m64i_i64 count)
{
	uint64x1_t gt63 = vcgt_s64(count, vdup_n_s64(63));
	int64x1_t ret = vshl_s64(a, count);
	return vbsl_s64 (gt63, ret, vdup_n_s64(0));
}
#endif

__forceinline __m64i_i16 _mm_slli_pi16 (__m64i_i16 a, int imm8)
{
	if ((imm8) <= 0) 
	{
		return a;
	}
	else if ((imm8) > 15) 
	{
		return vdup_n_s16(0);
	}
	else
	{
		return vshl_s16(a, vdup_n_s16(imm8));
	}
}
__forceinline __m64i_i32 _mm_slli_pi32 (__m64i_i32 a, int imm8)
{
	if ((imm8) <= 0) 
	{
		return a;
	}
	else if ((imm8) > 31) 
	{
		return vdup_n_s32(0);
	}
	else
	{
		return vshl_s32(a, vdup_n_s32(imm8));
	}
}

#if ARM64
__forceinline __m64i_i64 _mm_slli_si64 (__m64i_i64 a, int imm8)
{
	if ((imm8) <= 0) 
	{
		return a;
	}
	else if ((imm8) > 63) 
	{
		return vdup_n_s64(0);
	}
	else
	{
		return vshl_s64(a, vdup_n_s64(imm8));
	}
}
#endif

__forceinline __m64i_i16 _mm_sra_pi16 (__m64i_i16 a, __m64i_i16 count)
{
	uint16x4_t	gt15 = vcgt_s16(count, vdup_n_s16(15));
	int16x4_t	ret = vshl_s16(a, vneg_s16(count));
	int16x4_t	ret2 = vshr_n_s16(a, 8); 
				ret2 = vshr_n_s16(ret2, 8);
	return vbsl_s16 (gt15, ret, ret2);
}
__forceinline __m64i_i32 _mm_sra_pi32 (__m64i_i32 a, __m64i_i32 count)
{
	uint32x2_t	gt31 = vcgt_s32(count, vdup_n_s32(31));
	int32x2_t	ret = vshl_s32(a, vneg_s32(count));
	int32x2_t	ret2 = vshr_n_s32(a, 16); 
				ret2 = vshr_n_s32(ret2, 16);
	return vbsl_s32 (gt31, ret, ret2);
}

__forceinline __m64i_i16 _mm_srai_pi16 (__m64i_i16 a, int imm8)
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 15) )
	{
		return vshr_n_s16(vshr_n_s16(a, 8), 8);
	}
	else
	{
		return vshl_s16(a, vneg_s16(vdup_n_s16(imm8)));
	}
}
__forceinline __m64i_i32 _mm_srai_pi32 (__m64i_i32 a, int imm8)
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 31) )
	{
		return vshr_n_s32(vshr_n_s32(a, 16), 16);
	}
	else
	{
		return vshl_s32(a, vneg_s32(vdup_n_s32(imm8)));
	}
}

__forceinline __m64i_i16 _mm_srl_pi16 (__m64i_i16 a, __m64i_i16 count)
{
	uint16x4_t	gt15 = vcgt_s16(count, vdup_n_s16(15));
	uint16x4_t	lt0 = vclt_s16(count, vdup_n_s16(0));
	int16x4_t	ret = vshl_s16(a, vneg_s16(count));
	return vbsl_s16 (lt0, vbsl_s16 (gt15, ret, vdup_n_s16(0)), vdup_n_s16(0));
}
__forceinline __m64i_i32 _mm_srl_pi32 (__m64i_i32 a, __m64i_i32 count)
{
	uint32x2_t	gt31 = vcgt_s32(count, vdup_n_s32(31));
	uint32x2_t	lt0 = vclt_s32(count, vdup_n_s32(0));
	int32x2_t	ret = vshl_s32(a, vneg_s32(count));
	return vbsl_s32 (lt0, vbsl_s32 (gt31, ret, vdup_n_s32(0)), vdup_n_s32(0));
}

#if ARM64
__forceinline __m64i_i64 _mm_srl_si64 (__m64i_i64 a, __m64i_i64 count)
{
	uint64x1_t	gt63 = vcgt_s64(count, vdup_n_s64(63));
	uint64x1_t	lt0 = vclt_s64(count, vdup_n_s64(0));
	int64x1_t	ret = vshl_s64(a, vneg_s64(count));
	return vbsl_s64 (lt0, vbsl_s64 (gt63, ret, vdup_n_s64(0)), vdup_n_s64(0));
}
#endif

__forceinline __m64i_i16 _mm_srli_pi16 (__m64i_i16 a, int imm8)
{
	if((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 15) )
	{
		return vdup_n_s16(0);
	}
	else
	{
		return vshl_s16(a, vneg_s16(vdup_n_s16(imm8))); //vshr_n_s16(a, imm8)
	}
}
__forceinline __m64i_i32 _mm_srli_pi32 (__m64i_i32 a, int imm8)
{
	if((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 31) )
	{
		return vdup_n_s32(0);
	}
	else
	{
		return vshl_s32(a, vneg_s32(vdup_n_s32(imm8))); //vshr_n_s32(a, imm8)
	}
}

#if ARM64
__forceinline __m64i_i64 _mm_srli_si64 (__m64i_i64 a, int imm8)
{
	if((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 63) )
	{
		return vdup_n_s64(0);
	}
	else
	{
		return vshl_s64(a, vneg_s64(vdup_n_s64(imm8))); //vshr_n_s64(a, imm8)
	}
}
#endif

#define _m_psllw _mm_sll_pi16
#define _m_pslld _mm_sll_pi32

#if ARM64
#define _m_psllq _mm_sll_si64
#endif

#define _m_psllwi _mm_slli_pi16
#define _m_pslldi _mm_slli_pi32

#if ARM64
#define _m_psllqi _mm_slli_si64
#endif

#define _m_psraw _mm_sra_pi16
#define _m_psrad _mm_sra_pi32


#define _m_psrawi _mm_srai_pi16
#define _m_psradi _mm_srai_pi32

#define _m_psrlw _mm_srl_pi16
#define _m_psrld _mm_srl_pi32

#if ARM64
#define _m_psrlq _mm_srl_si64
#endif

#define _m_psrlwi _mm_srli_pi16
#define _m_psrldi _mm_srli_pi32

#if ARM64
#define _m_psrlqi _mm_srli_si64
#endif



/***************************************************************************************************/
// BITWISE
/***************************************************************************************************/
//DEFAULT 16BIT USE EXTENTIONS
__forceinline __m64i_i16 _mm_and_si64 (__m64i_i16 a, __m64i_i16 b){
	return vand_s16(a, b);
	#pragma message("\
	Use _mm_and_pu8, _mm_and_pi8, \
	_mm_and_pu16, _mm_and_pu32, \
	_mm_and_pi32, _mm_and_pu64, \
	_mm_and_pi64 in place of _mm_and_si64 \
	")
}
__forceinline __m64i_i16 _mm_andnot_si64 (__m64i_i16 a, __m64i_i16 b){
	return vbic_s16(b, a);
	#pragma message("\
	Use _mm_andnot_pu8, _mm_andnot_pi8, \
	_mm_andnot_pu16, _mm_andnot_pu32, \
	_mm_andnot_pi32, _mm_andnot_pu64, \
	_mm_andnot_pi64 in place of _mm_andnot_si64 \
	")
}
__forceinline __m64i_i16 _mm_or_si64 (__m64i_i16 a, __m64i_i16 b){
	return vorr_s16(a, b);
	#pragma message("\
	Use _mm_or_pu8, _mm_or_pi8, \
	_mm_or_pu16, _mm_or_pu32, \
	_mm_or_pi32, _mm_or_pu64, \
	_mm_or_pi64 in place of _mm_or_si64 \
	")
}
__forceinline __m64i_i16 _mm_xor_si64 (__m64i_i16 a, __m64i_i16 b){
	return veor_s16(a, b);
	#pragma message("\
	Use _mm_xor_pu8, _mm_xor_pi8, \
	_mm_xor_pu16, _mm_xor_pu32, \
	_mm_xor_pi32, _mm_xor_pu64, \
	_mm_xor_pi64 in place of _mm_xor_si64 \
	")
}

#define _m_pand _mm_and_si64
#define _m_pandn _mm_andnot_si64
#define _m_por _mm_or_si64
#define _m_pxor _mm_xor_si64

/***************************************************************************************************/
// Set
/***************************************************************************************************/
__forceinline __m64i_i32 _mm_cvtsi32_si64 (__int32 a)
{
	__m64i_i32 result = vdup_n_s32(0);
	return vset_lane_s32(a, result, 0);
}

#if ARM64
__forceinline __m64i_i64 _mm_cvtsi64_m64 (__int64 a)
{
	return vdup_n_s64(a);
}
#endif

#define _m_from_int _mm_cvtsi32_si64

#if ARM64
#define _m_from_int64 _mm_cvtsi64_m64
#endif

/***************************************************************************************************/
// Extract
/***************************************************************************************************/
__forceinline __int32 _mm_cvtsi64_si32 (__m64i_i32 a){
	return vget_lane_s32(a, 0);
}

#if ARM64
__forceinline __int64 _mm_cvtm64_si64 (__m64i_i64 a){
	return vget_lane_s64(a, 0);
}
#endif

#define _m_to_int _mm_cvtsi64_si32

#if ARM64
#define _m_to_int64 _mm_cvtm64_si64
#endif
/***************************************************************************************************/
// X87 Registers 
/***************************************************************************************************/
__forceinline void _m_empty (void)
{
	//DO NOTHING!
}
__forceinline void _mm_empty (void)
{
	//DO NOTHING!
}

/***************************************************************************************************/
// Pack
/***************************************************************************************************/
__forceinline __m64i_u8 _mm_packs_pu16 (__m64i_i16 a, __m64i_i16 b)
{
	__uint8 ALIGNED(8) data[8] = 
	{
		vget_lane_s16(a, 0) & 0xFFU, vget_lane_s16(a, 1) & 0xFFU, 
		vget_lane_s16(a, 2) & 0xFFU, vget_lane_s16(a, 3) & 0xFFU, 
		
		vget_lane_s16(b, 0) & 0xFFU, vget_lane_s16(b, 1) & 0xFFU, 
		vget_lane_s16(b, 2) & 0xFFU, vget_lane_s16(b, 3) & 0xFFU 
	};
	return vld1_u8(data);
}
__forceinline __m64i_i16 _mm_packs_pi32 (__m64i_i32 a, __m64i_i32 b)
{
	__int16 ALIGNED(8) data[4] = 
	{ 
		vget_lane_s32(a, 0) & 0xFFFFU, vget_lane_s32(a, 1) & 0xFFFFU, 
		vget_lane_s32(b, 0) & 0xFFFFU, vget_lane_s32(b, 1) & 0xFFFFU 
	};
	return vld1_s16(data);
}
__forceinline __m64i_i8 _mm_packs_pi16 (__m64i_i16 a, __m64i_i16 b)
{
#if _MSC_VER
	int8_t ALIGNED(8) data[8] =
#else
	__int8 ALIGNED(8) data[8] =
#endif
	{
		vget_lane_s16(a, 0) & 0xFF, vget_lane_s16(a, 1) & 0xFF, 
		vget_lane_s16(a, 2) & 0xFF, vget_lane_s16(a, 3) & 0xFF, 
		
		vget_lane_s16(b, 0) & 0xFF, vget_lane_s16(b, 1) & 0xFF, 
		vget_lane_s16(b, 2) & 0xFF, vget_lane_s16(b, 3) & 0xFF 
	};
	return vld1_s8(data);
}

#define _m_packuswb _mm_packs_pu16
#define _m_packssdw _mm_packs_pi32
#define _m_packsswb _mm_packs_pi16


/***************************************************************************************************/
//Unpack 
/***************************************************************************************************/
__forceinline __m64i_i8 _mm_unpackhi_pi8 (__m64i_i8 a, __m64i_i8 b)
{
#if _MSC_VER
	int8_t ALIGNED(8) data[8] =
#else
	__int8 ALIGNED(8) data[8] =
#endif 
	{ 
		vget_lane_s8(a, 4) , vget_lane_s8(b, 4) , 
		vget_lane_s8(a, 5) , vget_lane_s8(b, 5) ,
		vget_lane_s8(a, 6) , vget_lane_s8(b, 6) , 
		vget_lane_s8(a, 7) , vget_lane_s8(b, 7) 
	};
	return vld1_s8(data);
}
__forceinline __m64i_i16 _mm_unpackhi_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	__int16 ALIGNED(8) data[4] = 
	{ 
		vget_lane_s16(a, 2), vget_lane_s16(b, 2), 
		vget_lane_s16(a, 3), vget_lane_s16(b, 3)
	};
	return vld1_s16(data);
}
__forceinline __m64i_i32 _mm_unpackhi_pi32 (__m64i_i32 a, __m64i_i32 b)
{
	__int32 ALIGNED(8) data[2] = { vget_lane_s32(b, 1), vget_lane_s32(a, 1) };
	return vld1_s32(data);
}

__forceinline __m64i_i8 _mm_unpacklo_pi8 (__m64i_i8 a, __m64i_i8 b)
{
#if _MSC_VER
	int8_t ALIGNED(8) data[8] =
#else
	__int8 ALIGNED(8) data[8] =
#endif 
	{ 
		vget_lane_s8(a, 0) , vget_lane_s8(b, 0) , 
		vget_lane_s8(a, 1) , vget_lane_s8(b, 1) ,
		vget_lane_s8(a, 2) , vget_lane_s8(b, 2) , 
		vget_lane_s8(a, 3) , vget_lane_s8(b, 3) 
	};
	return vld1_s8(data);
}
__forceinline __m64i_i16 _mm_unpacklo_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	__int16 ALIGNED(8) data[4] = 
	{ 
		vget_lane_s16(a, 0), vget_lane_s16(b, 0), 
		vget_lane_s16(a, 2), vget_lane_s16(b, 2)
	};
	return vld1_s16(data);
}
__forceinline __m64i_i32 _mm_unpacklo_pi32 (__m64i_i32 a, __m64i_i32 b)
{
	__int32 ALIGNED(8) data[2] = { vget_lane_s32(b, 0), vget_lane_s32(a, 0) };
	return vld1_s32(data);
}

#define _m_punpckhbw _mm_unpackhi_pi8
#define _m_punpckhwd _mm_unpackhi_pi16
#define _m_punpckhdq _mm_unpackhi_pi32
#define _m_punpcklbw _mm_unpacklo_pi8
#define _m_punpcklwd _mm_unpacklo_pi16
#define _m_punpckldq _mm_unpacklo_pi32

#endif

