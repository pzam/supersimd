/***************************************************************************************************/
// HEADER_NAME /arm_neon/sse4_2.h
/***************************************************************************************************/
/*
OTHER
int _mm_popcnt_u32 (unsigned int a) popcnt
__int64 _mm_popcnt_u64 (unsigned __int64 a) popcnt

*/


#if ARM
#define __SSE4_2__ 1


#define _SIDD_UBYTE_OPS							0x00
#define _SIDD_UWORD_OPS							0x01
#define _SIDD_SBYTE_OPS							0x02
#define _SIDD_SWORD_OPS							0x03

#define _SIDD_CMP_EQUAL_ANY						0x00
#define _SIDD_CMP_RANGES						0x04
#define _SIDD_CMP_EQUAL_EACH					0x08
#define _SIDD_CMP_EQUAL_ORDERED					0x0c

#define _SIDD_POSITIVE_POLARITY					0x00
#define _SIDD_NEGATIVE_POLARITY					0x10
#define _SIDD_MASKED_POSITIVE_POLARITY			0x20
#define _SIDD_MASKED_NEGATIVE_POLARITY			0x30

#define _SIDD_LEAST_SIGNIFICANT					0x00
#define _SIDD_MOST_SIGNIFICANT					0x40

#define _SIDD_BIT_MASK							0x00
#define _SIDD_UNIT_MASK							0x40

/***************************************************************************************************/
// Comparator 
/***************************************************************************************************/
#if __ARM_ARCH_8__ || _M_ARM >= 8
__forceinline __m128i_i64 _mm_cmpgt_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	return vreinterpretq_s64_u64(vceqq_s64(a,b));
}
#endif
#if __ARM_FEATURE_CRC32
/***************************************************************************************************/
// CRC-32
/***************************************************************************************************/

__forceinline __uint8 _mm_crc32_u8 (__uint32 crc, __uint8 v)
{
	return __crc32cb( crc, v);
}
__forceinline __uint16 _mm_crc32_u16 (__uint32 crc, __uint16 v)
{
	return __crc32ch( crc, v);
}

__forceinline __uint32 _mm_crc32_u32 (__uint32 crc, __uint32 v)
{
	return __crc32cw( crc, v);
}
__forceinline __uint64 _mm_crc32_u64 (__uint64 crc, __uint64 v)
{
	return __crc32cd( crc, v);
}
#endif


/*
https://msdn.microsoft.com/en-us/library/bb514048(v=vs.120).aspx
int _mm_cmpestra (__m128i a, int la, __m128i b, int lb, const int imm8)

int _mm_cmpestrc (__m128i a, int la, __m128i b, int lb, const int imm8)
int _mm_cmpestri (__m128i a, int la, __m128i b, int lb, const int imm8)

int _mm_cmpestro (__m128i a, int la, __m128i b, int lb, const int imm8)
int _mm_cmpestrs (__m128i a, int la, __m128i b, int lb, const int imm8)
int _mm_cmpestrz (__m128i a, int la, __m128i b, int lb, const int imm8)


int _mm_cmpistra (__m128i a, __m128i b, const int imm8)
int _mm_cmpistrc (__m128i a, __m128i b, const int imm8)
int _mm_cmpistri (__m128i a, __m128i b, const int imm8)

__m128i _mm_cmpistrm (__m128i a, __m128i b, const int imm8)
__m128i _mm_cmpestrm (__m128i a, int la, __m128i b, int lb, const int imm8)

int _mm_cmpistro (__m128i a, __m128i b, const int imm8)
int _mm_cmpistrs (__m128i a, __m128i b, const int imm8)
int _mm_cmpistrz (__m128i a, __m128i b, const int imm8)

*/
#endif

