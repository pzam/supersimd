/***************************************************************************************************/
// HEADER_NAME /arm_neon/abs_extra.h
/***************************************************************************************************/
#if ARM
/***************************************************************************************************/
// Absolute Value
/***************************************************************************************************/
__forceinline __m128 _mm_abs_ps(const __m128 a) {
	return vabsq_f32(a);
}
#if ARM64
__forceinline __m128d _mm_abs_pd(const __m128d a) {
	return vabsq_f64(a);
}
#endif

//NOTE
//SSSE3 implemented
//__m128i_i8 _mm_abs_epi8 (__m128i_i8 a) 
//__m128i_i16 _mm_abs_epi16 (__m128i_i16 a)
//__m128i_i32 _mm_abs_epi32 (__m128i_i32 a)

//AVX-512 implemented
//__m128i_i64 _mm_abs_epi64 (__m128i_i64 a)

#if ARM64
__forceinline __m128i_i64 _mm_abs_epi64 (__m128i_i64 a){
	return vabsq_s64(a);
}
#endif

/***************************************************************************************************/
// Absolute Difference
/***************************************************************************************************/

__forceinline __m128i_u8 _mm_absdiff_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	return vabdq_u8(a, b);
}
__forceinline __m128i_i8 _mm_absdiff_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return vabdq_s8(a, b);
}
__forceinline __m128i_u16 _mm_absdiff_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	return vabdq_u16(a, b);
}
__forceinline __m128i_i16 _mm_absdiff_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vabdq_s16(a, b);
}
__forceinline __m128i_u32 _mm_absdiff_epu32 (__m128i_u32 a, __m128i_u32 b)
{
	return vabdq_u32(a, b);
}
__forceinline __m128i_i32 _mm_absdiff_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vabdq_s32(a, b);
}
#if ARM64
__forceinline __m128i_u64 _mm_absdiff_epu64 (__m128i_u64 a, __m128i_u64 b)
{
	return vsubq_u64(a, b);
}
__forceinline __m128i_i64 _mm_absdiff_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	return vabsq_s64(vsubq_s64(a, b));
}
#endif


__forceinline __m128 _mm_absdiff_ps(__m128 a, __m128 b) {
	return vabdq_f32(a,b);
}
#if ARM64
__forceinline __m128d _mm_absdiff_pd(__m128d a, __m128d b) {
	return vabdq_f64(a,b);
}
#endif

#endif

