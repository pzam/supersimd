/***************************************************************************************************/
// HEADER_NAME /arm_neon/blend_extra.h
/***************************************************************************************************/
#if ARM
// _mm_blendv_si128 Not Portable
__forceinline __m128i_i32 _mm_blendv_si128 (__m128i_i32 a, __m128i_i32 b, __m128i_i32 mask) {
	return vreinterpretq_s32_u8(vbslq_u8 (vreinterpretq_u8_s32(mask), vreinterpretq_u8_s32(b), vreinterpretq_u8_s32(a)));
	#pragma message("\
	Use _mm_blendv_epu8, _mm_blendv_epi8, \
	_mm_blendv_epu16, _mm_blendv_epu32, \
	_mm_blendv_epi32, _mm_blendv_epu64, \
	_mm_blendv_epi64 in place of _mm_blendv_si128 \
	")
}

__forceinline __m128i_u8 _mm_blendv_epu8 (__m128i_u8 a, __m128i_u8 b, __m128i_u8 mask) {
	return vbslq_u8 (mask, b, a);
}
// _mm_blendv_epi8 already implemented by SSE 4.1

__forceinline __m128i_u16 _mm_blendv_epu16 (__m128i_u16 a, __m128i_u16 b, __m128i_u16 mask) {
	return vbslq_u16 (mask, b, a);
}
__forceinline __m128i_i16 _mm_blendv_epi16 (__m128i_i16 a, __m128i_i16 b, __m128i_i16 mask) {
	return vbslq_s16( vreinterpretq_u16_s16(mask), b, a);
}
__forceinline __m128i_u32 _mm_blendv_epu32 (__m128i_u32 a, __m128i_u32 b, __m128i_u32 mask) {
	return vbslq_u32 (mask, b, a);
}
__forceinline __m128i_i32 _mm_blendv_epi32 (__m128i_i32 a, __m128i_i32 b, __m128i_i32 mask) {
	return vbslq_s32( vreinterpretq_u32_s32(mask), b, a);
}
#if ARM64
__forceinline __m128i_u64 _mm_blendv_epu64 (__m128i_u64 a, __m128i_u64 b, __m128i_u64 mask) {
	return vbslq_u64 (mask, b, a);
}
__forceinline __m128i_i64 _mm_blendv_epi64 (__m128i_i64 a, __m128i_i64 b, __m128i_i64 mask) {
	return vbslq_s64( vreinterpretq_u64_s64(mask), b, a);
}
#endif

#endif

