/***************************************************************************************************/
// HEADER_NAME /arm_neon/set_extra.h
/***************************************************************************************************/
#if ARM
#if defined(__MMX__)

// _mm_set_pi8 Implemented by MMX
__forceinline __m64i_u8 _mm_set_pu8
(
__uint8 e7, __uint8 e6, __uint8 e5, __uint8 e4, 
__uint8 e3, __uint8 e2, __uint8 e1, __uint8 e0
)
{
	__uint8 ALIGNED(8) data[8] = { e0, e1, e2, e3 ,e4, e5, e6, e7 };
	return vld1_u8(data);
}
// _mm_setr_pi8 Implemented by MMX
__forceinline __m64i_u8 _mm_setr_pu8
(
__uint8 e0, __uint8 e1, __uint8 e2, __uint8 e3, 
__uint8 e4, __uint8 e5, __uint8 e6, __uint8 e7
)
{
	__uint8 ALIGNED(8) data[8] = { e0, e1, e2, e3 ,e4, e5, e6, e7 };
	return vld1_u8(data);
}


// _mm_set_pi16 Implemented by MMX
__forceinline __m64i_u16 _mm_set_pu16 
(
__uint16 e3, __uint16 e2, 
__uint16 e1, __uint16 e0
)
{
	__uint16 ALIGNED(8) data[4] = { e0, e1, e2, e3 };
	return vld1_u16(data);
}

// _mm_setr_pi16 Implemented by MMX
__forceinline __m64i_u16 _mm_setr_pu16 
(
__uint16 e0, __uint16 e1, 
__uint16 e2, __uint16 e3
)
{
	__uint16 ALIGNED(8) data[4] = { e0, e1, e2, e3 };
	return vld1_u16(data);
}


// _mm_set_pi32 Implemented by MMX
__forceinline __m64i_u32 _mm_set_pu32 
(
__uint32 e1, __uint32 e0
)
{
	__uint32 ALIGNED(8) data[2] = { e0, e1 };
	return vld1_u32(data);
}
// _mm_setr_pi32 Implemented by MMX
__forceinline __m64i_u32 _mm_setr_pu32 
(
__uint32 e0, __uint32 e1
)
{
	__uint32 ALIGNED(8) data[2] = { e0, e1 };
	return vld1_u32(data);
}


#if ARM64
__forceinline __m64i_i64 _mm_set_pi64x (__int64 a)
{
	return vdup_n_s64(a);
}
#define _mm_setr_pi64x _mm_set_pi64x

__forceinline __m64i_u64 _mm_set_pu64x (__uint64 a)
{
	return vdup_n_u64(a);
}
#define _mm_setr_pu64x _mm_set_pu64x
#endif

__forceinline __m64i_u8 _mm_set1_pu8 (__uint8 a)
{
	return vdup_n_u8(a);
}
__forceinline __m64i_u16 _mm_set1_pu16 (__uint16 a)
{
	return vdup_n_u16(a);
}
__forceinline __m64i_u32 _mm_set1_pu32 (__uint32 a)
{
	return vdup_n_u32(a);
}
#if ARM64
__forceinline __m64i_u64 _mm_set1_pu64x (__uint64 a)
{
	return vdup_n_u64(a);
}
__forceinline __m64i_i64 _mm_set1_pi64x (__int64 a)
{
	return vdup_n_s64(a);
}
#endif
#endif


//SSE
__forceinline __m128i_u8 _mm_set_epu8
(
__uint8 e15, __uint8 e14, __uint8 e13, __uint8 e12, 
__uint8 e11, __uint8 e10, __uint8 e9, __uint8 e8,
__uint8 e7, __uint8 e6, __uint8 e5, __uint8 e4, 
__uint8 e3, __uint8 e2, __uint8 e1, __uint8 e0
)
{
	__uint8 ALIGNED(16) data[16] = 
	{ 
	e0, e1, e2, e3 ,e4, e5, e6, e7,
	e8, e9, e10, e11 ,e12, e13, e14, e15 
	};
	return vld1q_u8(data);
}
__forceinline __m128i_u8 _mm_setr_epu8
(
__uint8 e15, __uint8 e14, __uint8 e13, __uint8 e12, 
__uint8 e11, __uint8 e10, __uint8 e9, __uint8 e8,
__uint8 e7, __uint8 e6, __uint8 e5, __uint8 e4, 
__uint8 e3, __uint8 e2, __uint8 e1, __uint8 e0
)
{
	__uint8 ALIGNED(16) data[16] = 
	{ 
	e15, e14, e13, e12 ,e11, e10, e9, e8,
	e7, e6, e5, e4 ,e3, e2, e1, e0 
	};
	return vld1q_u8(data);
}


__forceinline __m128i_u16 _mm_set_epu16 
(
__uint16 e7, __uint16 e6, 
__uint16 e5, __uint16 e4,
__uint16 e3, __uint16 e2, 
__uint16 e1, __uint16 e0
)
{
	__uint16 ALIGNED(16) data[8] = 
	{ 
	e0, e1, e2, e3, 
	e4, e5, e6, e7 
	};
	return vld1q_u16(data);
}
__forceinline __m128i_u16 _mm_setr_epu16 
(
__uint16 e7, __uint16 e6, 
__uint16 e5, __uint16 e4,
__uint16 e3, __uint16 e2, 
__uint16 e1, __uint16 e0
)
{
	__uint16 ALIGNED(16) data[8] = 
	{ 
	e7, e6, e5, e4, 
	e3, e2, e1, e0 
	};
	return vld1q_u16(data);
}

__forceinline __m128i_u32 _mm_set_epu32 
(
__uint32 e3, __uint32 e2,
__uint32 e1, __uint32 e0
)
{
	__uint32 ALIGNED(16) data[4] = { e0, e1, e2, e3 };
	return vld1q_u32(data);
}
__forceinline __m128i_u32 _mm_setr_epu32 
(
__uint32 e3, __uint32 e2,
__uint32 e1, __uint32 e0
)
{
	__uint32 ALIGNED(16) data[4] = { e3, e2, e1, e0 };
	return vld1q_u32(data);
}


#if ARM64
__forceinline __m128i_u64 _mm_set_epu64 (__m64i_u64 e1, __m64i_u64 e0)
{
	return vcombine_s64 (e0, e1);
}
__forceinline __m128i_u64 _mm_setr_epu64 (__m64i_u64 e1, __m64i_u64 e0)
{
	return vcombine_s64 (e1, e0);
}

__forceinline __m128i_u64 _mm_set_epu64x (__uint64 e1, __uint64 e0)
{
	__uint64 ALIGNED(16) data[2] = { e0, e1 };
	return vld1q_u64(data);
}
__forceinline __m128i_u64 _mm_setr_epu64x (__uint64 e1, __uint64 e0)
{
	__uint64 ALIGNED(16) data[2] = { e1, e0 };
	return vld1q_u64(data);
}
#endif

__forceinline __m128i_u8 _mm_set1_epu8 (__uint8 a)
{
	return vdupq_n_u8(a);
}
__forceinline __m128i_u16 _mm_set1_epu16 (__uint16 a)
{
	return vdupq_n_u16(a);
}
__forceinline __m128i_u32 _mm_set1_epu32 (__uint32 a)
{
	return vdupq_n_u32(a);
}

#if ARM64
__forceinline __m128i_u64 _mm_set1_epu64 (__m64i_u64 a)
{
	return vcombine_s64 (a, a);
}
__forceinline __m128i_u64 _mm_set1_epu64x (__uint64 a)
{
	return vdupq_n_u64(a);
}
#endif

/***************************************************************************************************/
// SET-ZERO
/***************************************************************************************************/
#if defined(__MMX__)
//MMX
__forceinline __m64i_u8 _mm_setzero_pu8 (void)
{
	return vdup_n_u8(0);
}
__forceinline __m64i_i8 _mm_setzero_pi8 (void)
{
	return vdup_n_s8(0);
}
__forceinline __m64i_u16 _mm_setzero_pu16 (void)
{
	return vdup_n_u16(0);
}
__forceinline __m64i_i16 _mm_setzero_pi16 (void)
{
	return vdup_n_s16(0);
}
__forceinline __m64i_u32 _mm_setzero_pu32 (void)
{
	return vdup_n_u32(0);
}
__forceinline __m64i_i32 _mm_setzero_pi32 (void)
{
	return vdup_n_s32(0);
}

#if ARM64
__forceinline __m64i_u64 _mm_setzero_pu64 (void)
{
	return vdup_n_u64(0);
}
__forceinline __m64i_i64 _mm_setzero_pi64 (void)
{
	return vdup_n_s64(0);
}
#endif

//SSE
__forceinline __m128i_u8 _mm_setzero_epu8(void)
{
	return vdupq_n_u8(0);
}
__forceinline __m128i_i8 _mm_setzero_epi8(void)
{
	return vdupq_n_s8(0);
}
__forceinline __m128i_u16 _mm_setzero_epu16(void)
{
	return vdupq_n_u16(0);
}
__forceinline __m128i_i16 _mm_setzero_epi16(void)
{
	return vdupq_n_s16(0);
}
__forceinline __m128i_u32 _mm_setzero_epu32(void)
{
	return vdupq_n_u32(0);
}
__forceinline __m128i_i32 _mm_setzero_epi32(void)
{
	return vdupq_n_s32(0);
}

#if ARM64
__forceinline __m128i_u64 _mm_setzero_epu64(void)
{
	return vdupq_n_u64(0);
}
__forceinline __m128i_i64 _mm_setzero_epi64(void)
{
	return vdupq_n_s64(0);
}
#endif
#endif

/***************************************************************************************************/
// SET-UNDEFINED
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_undefined_epu8 (void)
{
	__m128i_u8 ret;
	return ret;
}
__forceinline __m128i_i8 _mm_undefined_epi8 (void)
{
	__m128i_i8 ret;
	return ret;
}
__forceinline __m128i_u16 _mm_undefined_epu16 (void)
{
	__m128i_u16 ret;
	return ret;
}
__forceinline __m128i_i16 _mm_undefined_epi16 (void)
{
	__m128i_i16 ret;
	return ret;
}
__forceinline __m128i_u32 _mm_undefined_epu32 (void)
{
	__m128i_u32 ret;
	return ret;
}
__forceinline __m128i_i32 _mm_undefined_epi32 (void)
{
	__m128i_i32 ret;
	return ret;
}

#if ARM64
__forceinline __m128i_u64 _mm_undefined_epu64 (void)
{
	__m128i_u64 ret;
	return ret;
}
__forceinline __m128i_i64 _mm_undefined_epi64 (void)
{
	__m128i_i64 ret;
	return ret;
}
#endif

#endif

