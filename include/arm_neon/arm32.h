/***************************************************************************************************/
// HEADER_NAME /arm_neon/arm32.h
/***************************************************************************************************/

// Back porting of ARM-64 for ARM-32 compatibility !!!

#if defined(__arm__) || defined(_M_ARM)

/************************************************************************************************/
// Vector divide
/************************************************************************************************/

__forceinline float32x2_t vdiv_f32 (float32x2_t a, float32x2_t b)
{
	float32x2_t x = vrecpe_f32(b);

	x = vmul_f32(vrecps_f32(b, x), x);
	x = vmul_f32(vrecps_f32(b, x), x);

	return vmul_f32(a, x);
}

__forceinline float32x4_t vdivq_f32 (float32x4_t a, float32x4_t b)
{
	float32x4_t x = vrecpeq_f32(b);

	x = vmulq_f32(vrecpsq_f32(b, x), x);
	x = vmulq_f32(vrecpsq_f32(b, x), x);

	return vmulq_f32(a, x);
}

/************************************************************************************************/
// Add across vector
/************************************************************************************************/

__forceinline int8_t vaddv_s8 (int8x8_t a)
{
	return	vget_lane_s8(a, 0) + vget_lane_s8(a, 1) + vget_lane_s8(a, 2) + vget_lane_s8(a, 3) +
			vget_lane_s8(a, 4) + vget_lane_s8(a, 5) + vget_lane_s8(a, 6) + vget_lane_s8(a, 7) ;
}
__forceinline int8_t vaddvq_s8 (int8x16_t a)
{
	return	vgetq_lane_s8(a, 0) + vgetq_lane_s8(a, 1) + vgetq_lane_s8(a, 2) + vgetq_lane_s8(a, 3)     +
			vgetq_lane_s8(a, 4) + vgetq_lane_s8(a, 5) + vgetq_lane_s8(a, 6) + vgetq_lane_s8(a, 7)     +
			vgetq_lane_s8(a, 8) + vgetq_lane_s8(a, 9) + vgetq_lane_s8(a, 10) + vgetq_lane_s8(a, 11)   +
			vgetq_lane_s8(a, 12) + vgetq_lane_s8(a, 13) + vgetq_lane_s8(a, 14) + vgetq_lane_s8(a, 15) ;
}
__forceinline int16_t vaddv_s16 (int16x4_t a)
{
	return	vget_lane_s16(a, 0) + vget_lane_s16(a, 1) + vget_lane_s16(a, 2) + vget_lane_s16(a, 3);
}
__forceinline int16_t vaddvq_s16 (int16x8_t a)
{
	return	vgetq_lane_s16(a, 0) + vgetq_lane_s16(a, 1) + vgetq_lane_s16(a, 2) + vgetq_lane_s16(a, 3) +
			vgetq_lane_s16(a, 4) + vgetq_lane_s16(a, 5) + vgetq_lane_s16(a, 6) + vgetq_lane_s16(a, 7) ;
}
__forceinline int32_t vaddv_s32 (int32x2_t a)
{
	return	vget_lane_s32(a, 0) + vget_lane_s32(a, 1);
}
__forceinline int32_t vaddvq_s32 (int32x4_t a)
{
	return	vgetq_lane_s32(a, 0) + vgetq_lane_s32(a, 1) + vgetq_lane_s32(a, 2) + vgetq_lane_s32(a, 3);
}
__forceinline uint8_t vaddv_u8(uint8x8_t a)
{
	return	vget_lane_u8(a, 0) + vget_lane_u8(a, 1) + vget_lane_u8(a, 2) + vget_lane_u8(a, 3) +
			vget_lane_u8(a, 4) + vget_lane_u8(a, 5) + vget_lane_u8(a, 6) + vget_lane_u8(a, 7) ;
}
__forceinline uint8_t vaddvq_u8 (uint8x16_t a)
{
	return	vgetq_lane_u8(a, 0) + vgetq_lane_u8(a, 1) + vgetq_lane_u8(a, 2) + vgetq_lane_u8(a, 3)     +
			vgetq_lane_u8(a, 4) + vgetq_lane_u8(a, 5) + vgetq_lane_u8(a, 6) + vgetq_lane_u8(a, 7)     +
			vgetq_lane_u8(a, 8) + vgetq_lane_u8(a, 9) + vgetq_lane_u8(a, 10) + vgetq_lane_u8(a, 11)   +
			vgetq_lane_u8(a, 12) + vgetq_lane_u8(a, 13) + vgetq_lane_u8(a, 14) + vgetq_lane_u8(a, 15) ;
}
__forceinline uint16_t vaddv_u16 (uint16x4_t a)
{
	return	vget_lane_u16(a, 0) + vget_lane_u16(a, 1) + vget_lane_u16(a, 2) + vget_lane_u16(a, 3);
}
__forceinline uint16_t vaddvq_u16 (uint16x8_t a)
{
	return	vgetq_lane_u16(a, 0) + vgetq_lane_u16(a, 1) + vgetq_lane_u16(a, 2) + vgetq_lane_u16(a, 3) +
			vgetq_lane_u16(a, 4) + vgetq_lane_u16(a, 5) + vgetq_lane_u16(a, 6) + vgetq_lane_u16(a, 7) ;
}
__forceinline uint32_t vaddv_u32 (uint32x2_t a)
{
	return	vget_lane_u32(a, 0) + vget_lane_u32(a, 1);
}
__forceinline uint32_t vaddvq_u32 (uint32x4_t a)
{
	return	vgetq_lane_u32(a, 0) + vgetq_lane_u32(a, 1) + vgetq_lane_u32(a, 2) + vgetq_lane_u32(a, 3);
}
__forceinline float32_t vaddv_f32 (float32x2_t a)
{
	return	vget_lane_f32(a, 0) + vget_lane_f32(a, 1);
}
__forceinline float32_t vaddvq_f32 (float32x4_t a)
{
	return	vgetq_lane_f32(a, 0) + vgetq_lane_f32(a, 1) + vgetq_lane_f32(a, 2) + vgetq_lane_f32(a, 3);
}

/************************************************************************************************/
// Vector square root
/************************************************************************************************/

__forceinline float32x2_t vsqrt_f32 (float32x2_t a)
{
	float32x2_t x = vrsqrte_f32(a);
	
	x = vreinterpret_f32_u32(
			vand_u32(vmvn_u32(
			vceq_u32(vdup_n_u32(0x7F800000), vreinterpret_u32_f32(x))
		), vreinterpret_u32_f32(x))
	);

	x = vmul_f32(vrsqrts_f32(vmul_f32(x, x), a), x);
	x = vmul_f32(vrsqrts_f32(vmul_f32(x, x), a), x);

	return vmul_f32(a, x);
}

__forceinline float32x4_t vsqrtq_f32 (float32x4_t a)
{
	float32x4_t x = vrsqrteq_f32(a);
	
	x = vreinterpretq_f32_u32(
			vandq_u32(vmvnq_u32(
			vceqq_u32(vdupq_n_u32(0x7F800000), vreinterpretq_u32_f32(x))
		), vreinterpretq_u32_f32(x))
	);

	x = vmulq_f32(vrsqrtsq_f32(vmulq_f32(x, x), a), x);
	x = vmulq_f32(vrsqrtsq_f32(vmulq_f32(x, x), a), x);

	return vmulq_f32(a, x);
}

/************************************************************************************************/
// Pairwise add
/************************************************************************************************/

__forceinline int16x8_t vpaddq_s16 (int16x8_t a, int16x8_t b)
{
	int16x4_t v1 = vget_low_s16(a);
	int16x4_t v2 = vget_high_s16(a);
	int16x4_t v3 = vget_low_s16(b);
	int16x4_t v4 = vget_high_s16(b);
	v1 = vpadd_s16(v1, v2);
	v3 = vpadd_s16(v3, v4);
	return vcombine_s16(v1, v3);
}

__forceinline int32x4_t vpaddq_s32 (int32x4_t a, int32x4_t b)
{
	int32x2_t v1 = vget_low_s32(a);
	int32x2_t v2 = vget_high_s32(a);
	int32x2_t v3 = vget_low_s32(b);
	int32x2_t v4 = vget_high_s32(b);
	v1 = vpadd_s32(v1, v2);
	v3 = vpadd_s32(v3, v4);
	return vcombine_s32(v1, v3);
}
__forceinline uint16x8_t vpaddq_u16 (uint16x8_t a, uint16x8_t b)
{
	uint16x4_t v1 = vget_low_u16(a);
	uint16x4_t v2 = vget_high_u16(a);
	uint16x4_t v3 = vget_low_u16(b);
	uint16x4_t v4 = vget_high_u16(b);
	v1 = vpadd_u16(v1, v2);
	v3 = vpadd_u16(v3, v4);
	return vcombine_u16(v1, v3);
}
__forceinline uint32x4_t vpaddq_u32 (uint32x4_t a, uint32x4_t b)
{
	uint32x2_t v1 = vget_low_u32(a);
	uint32x2_t v2 = vget_high_u32(a);
	uint32x2_t v3 = vget_low_u32(b);
	uint32x2_t v4 = vget_high_u32(b);
	v1 = vpadd_u32(v1, v2);
	v3 = vpadd_u32(v3, v4);
	return vcombine_u32(v1, v3);
}
__forceinline float32x4_t vpaddq_f32 (float32x4_t a, float32x4_t b)
{
	float32x2_t v1 = vget_low_f32(a);
	float32x2_t v2 = vget_high_f32(a);
	float32x2_t v3 = vget_low_f32(b);
	float32x2_t v4 = vget_high_f32(b);
	v1 = vpadd_f32(v1, v2);
	v3 = vpadd_f32(v3, v4);
	return vcombine_f32(v1, v3);
}

#endif

