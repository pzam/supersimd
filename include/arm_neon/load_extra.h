/***************************************************************************************************/
// HEADER_NAME /arm_neon/load_extra.h
/***************************************************************************************************/
#if ARM
/***************************************************************************************************/
// Portable Load
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_load_epu8(__m128i_u8 const* mem_addr)
{
	return *mem_addr;
}
__forceinline __m128i_i8 _mm_load_epi8(__m128i_i8 const* mem_addr)
{
	return *mem_addr;
}
__forceinline __m128i_u16 _mm_load_epu16(__m128i_u16 const* mem_addr)
{
	return *mem_addr;
}
__forceinline __m128i_i16 _mm_load_epi16(__m128i_i16 const* mem_addr)
{
	return *mem_addr;
}
__forceinline __m128i_u32 _mm_load_epu32(__m128i_u32 const* mem_addr)
{
	return *mem_addr;
}
__forceinline __m128i_i32 _mm_load_epi32(__m128i_i32 const* mem_addr)
{
	return *mem_addr;
}
#if ARM64
__forceinline __m128i_u64 _mm_load_epu64(__m128i_u64 const* mem_addr)
{
	return *mem_addr;
}
__forceinline __m128i_i64 _mm_load_epi64(__m128i_i64 const* mem_addr)
{
	return *mem_addr;
}
#endif


/***************************************************************************************************/
// Portable Loadu
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_loadu_epu8(__m128i_u8 const* mem_addr)
{
	return *mem_addr;
}
#define _mm_lddqu_epu8 _mm_loadu_epu8

__forceinline __m128i_i8 _mm_loadu_epi8(__m128i_i8 const* mem_addr)
{
	return *mem_addr;
}
#define _mm_lddqu_epi8 _mm_loadu_epi8

__forceinline __m128i_u16 _mm_loadu_epu16(__m128i_u16 const* mem_addr)
{
	return *mem_addr;
}
#define _mm_lddqu_epu16 _mm_loadu_epu16

__forceinline __m128i_i16 _mm_loadu_epi16(__m128i_i16 const* mem_addr)
{
	return *mem_addr;
}
#define _mm_lddqu_epi16 _mm_loadu_epi16


__forceinline __m128i_u32 _mm_loadu_epu32(__m128i_u32 const* mem_addr)
{
	return *mem_addr;
}
#define _mm_lddqu_epu32 _mm_loadu_epu32

__forceinline __m128i_i32 _mm_loadu_epi32(__m128i_i32 const* mem_addr)
{
	return *mem_addr;
}
#define _mm_lddqu_epi32 _mm_loadu_epi32

#if ARM64

__forceinline __m128i_u64 _mm_loadu_epu64(__m128i_u64 const* mem_addr)
{
	return *mem_addr;
}
#define _mm_lddqu_epu64 _mm_loadu_epu64

__forceinline __m128i_i64 _mm_loadu_epi64(__m128i_i64 const* mem_addr)
{
	return *mem_addr;
}
#define _mm_lddqu_epi64 _mm_loadu_epi64
#endif

/***************************************************************************************************/
// Portable Loadu Void*
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_loadu_su8 (void const* mem_addr)
{
	return vld1q_u8((__uint8*)mem_addr);
}
__forceinline __m128i_u16 _mm_loadu_su16 (void const* mem_addr)
{
	return vld1q_u16((__uint16*)mem_addr);
}
__forceinline __m128i_u32 _mm_loadu_su32 (void const* mem_addr)
{
	return vld1q_u32((__uint32*)mem_addr);
}
#if ARM64
__forceinline __m128i_u64 _mm_loadu_su64 (void const* mem_addr)
{
	return vld1q_u64((__uint64*)mem_addr);
}
#endif

#endif

