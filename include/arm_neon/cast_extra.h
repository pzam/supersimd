/***************************************************************************************************/
// HEADER_NAME /arm_neon/cast_extra.h
/***************************************************************************************************/
#if ARM
/*
IMPLEMENTED IN SSE 2

__m128 _mm_castpd_ps (__m128d a)
__m128d _mm_castps_pd (__m128 a)

//Please use specific cast operator in place of these.
__m128i_i64 _mm_castpd_si128 (__m128d a)
__m128i_i32 _mm_castps_si128 (__m128 a)
__m128d _mm_castsi128_pd (__m128i_i64 a)
__m128 _mm_castsi128_ps (__m128i_i32 a)

*/

//CASTING OPERATIONS DO NOTHING ON INTEL
//API IS RESERVED FOR ARM64 COMPATIBIBLTY 


/*******************************************************************************/
//__m128i_i16 _mm_castepi8_epi16 (__m128i_i8 a)
#define _mm_castepi8_epi16 vreinterpretq_s16_s8

//__m128i_i32 _mm_castepi8_epi32 (__m128i_i8 a)
#define _mm_castepi8_epi32 vreinterpretq_s32_s8

//__m128i_i64 _mm_castepi8_epi64 (__m128i_i8 a)
#define _mm_castepi8_epi64 vreinterpretq_s64_s8

//__m128i_u8 _mm_castepi8_epu8 (__m128i_i8 a)
#define _mm_castepi8_epu8 vreinterpretq_u8_s8

//__m128i_u16 _mm_castepi8_epu16 (__m128i_i8 a)
#define _mm_castepi8_epu16 vreinterpretq_u16_s8

//__m128i_u32 _mm_castepi8_epu32 (__m128i_i8 a)
#define _mm_castepi8_epu32 vreinterpretq_u32_s8

//__m128i_u64 _mm_castepi8_epu64 (__m128i_i8 a)
#define _mm_castepi8_epu64 vreinterpretq_u64_s8

//__m128 _mm_castepi8_ps (__m128i_i8 a)
#define _mm_castepi8_ps vreinterpretq_f32_s8

//__m128d _mm_castepi8_pd (__m128i_i8 a)
#define _mm_castepi8_pd vreinterpretq_f64_s8
/*******************************************************************************/

/*******************************************************************************/
//__m128i_i8 _mm_castepi16_epi8 (__m128i_i16 a)
#define _mm_castepi16_epi8 vreinterpretq_s8_s16

//__m128i_i32 _mm_castepi16_epi32 (__m128i_i16 a)
#define _mm_castepi16_epi32 vreinterpretq_s32_s16

//__m128i_i64 _mm_castepi16_epi64 (__m128i_i16 a)
#define _mm_castepi16_epi64 vreinterpretq_s64_s16

//__m128i_u8 _mm_castepi16_epu8 (__m128i_i16 a)
#define _mm_castepi16_epu8 vreinterpretq_u8_s16

//__m128i_u16 _mm_castepi16_epu16 (__m128i_i16 a)
#define _mm_castepi16_epu16 vreinterpretq_u16_s16

//__m128i_u32 _mm_castepi16_epu32 (__m128i_i16 a)
#define _mm_castepi16_epu32 vreinterpretq_u32_s16

//__m128i_u64 _mm_castepi16_epu64 (__m128i_i16 a)
#define _mm_castepi16_epu64 vreinterpretq_u64_s16

//__m128 _mm_castepi16_ps (__m128i_i16 a)
#define _mm_castepi16_ps vreinterpretq_f32_s16

//__m128d _mm_castepi16_pd (__m128i_i16 a)
#define _mm_castepi16_pd vreinterpretq_f64_s16
/*******************************************************************************/

/*******************************************************************************/
//__m128i_i8 _mm_castepi32_epi8 (__m128i_i32 a)
#define _mm_castepi32_epi8 vreinterpretq_s8_s32

//__m128i_i16 _mm_castepi32_epi16 (__m128i_i32 a)
#define _mm_castepi32_epi16 vreinterpretq_s16_s32

//__m128i_i64 _mm_castepi32_epi64 (__m128i_i32 a)
#define _mm_castepi32_epi64 vreinterpretq_s64_s32

//__m128i_u8 _mm_castepi32_epu8 (__m128i_i32 a)
#define _mm_castepi32_epu8 vreinterpretq_u8_s32

//__m128i_u16 _mm_castepi32_epu16 (__m128i_i32 a)
#define _mm_castepi32_epu16 vreinterpretq_u16_s32

//__m128i_u32 _mm_castepi32_epu32 (__m128i_i32 a)
#define _mm_castepi32_epu32 vreinterpretq_u32_s32

//__m128i_u64 _mm_castepi32_epu64 (__m128i_i32 a)
#define _mm_castepi32_epu64 vreinterpretq_u64_s32

//__m128 _mm_castepi32_ps (__m128i_i32 a)
#define _mm_castepi32_ps vreinterpretq_f32_s32

//__m128d _mm_castepi32_pd (__m128i_i32 a)
#define _mm_castepi32_pd vreinterpretq_f64_s32
/*******************************************************************************/

/*******************************************************************************/
//__m128i_i8 _mm_castps_epi8 (__m128 a)
#define _mm_castps_epi8 vreinterpretq_s8_f32

//__m128i_i16 _mm_castps_epi16 (__m128 a)
#define _mm_castps_epi16 vreinterpretq_s16_f32

//__m128i_i32 _mm_castps_epi32 (__m128 a)
#define _mm_castps_epi32 vreinterpretq_s32_f32

//__m128i_i64 _mm_castps_epi64 (__m128 a)
#define _mm_castps_epi64 vreinterpretq_s64_f32

//__m128i_u8 _mm_castps_epu8 (__m128 a)
#define _mm_castps_epu8 vreinterpretq_u8_f32

//__m128i_u16 _mm_castps_epu16 (__m128 a)
#define _mm_castps_epu16 vreinterpretq_u16_f32

//__m128i_u32 _mm_castps_epu32 (__m128 a)
#define _mm_castps_epu32 vreinterpretq_u32_f32

//__m128i_u64 _mm_castps_epu64 (__m128 a)
#define _mm_castps_epu64 vreinterpretq_u64_f32
/*******************************************************************************/

/*******************************************************************************/
//__m128i_i8 _mm_castepu8_epi8 (__m128i_u8 a)
#define _mm_castepu8_epi8 vreinterpretq_s8_u8

//__m128i_i16 _mm_castepu8_epi16 (__m128i_u8 a)
#define _mm_castepu8_epi16 vreinterpretq_s16_u8

//__m128i_i32 _mm_castepu8_epi32 (__m128i_u8 a)
#define _mm_castepu8_epi32 vreinterpretq_s32_u8

//__m128i_i64 _mm_castepu8_epi64 (__m128i_u8 a)
#define _mm_castepu8_epi64 vreinterpretq_s64_u8

//__m128i_u16 _mm_castepu8_epu16 (__m128i_u8 a)
#define _mm_castepu8_epu16 vreinterpretq_u16_u8

//__m128i_u32 _mm_castepu8_epu32 (__m128i_u8 a)
#define _mm_castepu8_epu32 vreinterpretq_u32_u8

//__m128i_u64 _mm_castepu8_epu64 (__m128i_u8 a)
#define _mm_castepu8_epu64 vreinterpretq_u64_u8

//__m128 _mm_castepu8_ps (__m128i_u8 a)
#define _mm_castepu8_ps vreinterpretq_f32_u8

//__m128d _mm_castepu8_pd (__m128i_u8 a)
#define _mm_castepu8_pd vreinterpretq_f64_u8
/*******************************************************************************/

/*******************************************************************************/
//__m128i_i8 _mm_castepu16_epi8 (__m128i_u16 a)
#define _mm_castepu16_epi8 vreinterpretq_s8_u16

//__m128i_i16 _mm_castepu16_epi16 (__m128i_u16 a)
#define _mm_castepu16_epi16 vreinterpretq_s16_u16

//__m128i_i32 _mm_castepu16_epi32 (__m128i_u16 a)
#define _mm_castepu16_epi32 vreinterpretq_s32_u16

//__m128i_i64 _mm_castepu16_epi64 (__m128i_u16 a)
#define _mm_castepu16_epi64 vreinterpretq_s64_u16

//__m128i_u8 _mm_castepu16_epu8 (__m128i_u16 a)
#define _mm_castepu16_epu8 vreinterpretq_u8_u16

//__m128i_u32 _mm_castepu16_epu32 (__m128i_u16 a)
#define _mm_castepu16_epu32 vreinterpretq_u32_u16

//__m128i_u64 _mm_castepu16_epu64 (__m128i_u16 a)
#define _mm_castepu16_epu64 vreinterpretq_u64_u16

//__m128 _mm_castepu16_ps (__m128i_u16 a)
#define _mm_castepu16_ps vreinterpretq_f32_u16

//__m128d _mm_castepu16_pd (__m128i_u16 a)
#define _mm_castepu16_pd vreinterpretq_f64_u16
/*******************************************************************************/

/*******************************************************************************/
//__m128i_i8 _mm_castepu32_epi8 (__m128i_u32 a)
#define _mm_castepu32_epi8 vreinterpretq_s8_u32

//__m128i_i16 _mm_castepu32_epi16 (__m128i_u32 a)
#define _mm_castepu32_epi16 vreinterpretq_s16_u32

//__m128i_i32 _mm_castepu32_epi32 (__m128i_u32 a)
#define _mm_castepu32_epi32 vreinterpretq_s32_u32

//__m128i_i64 _mm_castepu32_epi64 (__m128i_u32 a)
#define _mm_castepu32_epi64 vreinterpretq_s64_u32

//__m128i_u8 _mm_castepu32_epu8 (__m128i_u32 a)
#define _mm_castepu32_epu8 vreinterpretq_u8_u32

//__m128i_u16 _mm_castepu32_epu16 (__m128i_u32 a)
#define _mm_castepu32_epu16 vreinterpretq_u16_u32

//__m128i_u64 _mm_castepu32_epu64 (__m128i_u32 a)
#define _mm_castepu32_epu64 vreinterpretq_u64_u32

//__m128 _mm_castepu32_ps(__m128i_u32 a)
#define _mm_castepu32_ps vreinterpretq_f32_u32

//__m128d _mm_castepu32_pd(__m128i_u32 a)
#define _mm_castepu32_pd vreinterpretq_f64_u32
/*******************************************************************************/

/*******************************************************************************/
//__m128i_i8 _mm_castepu64_epi8 (__m128i_u64 a)
#define _mm_castepu64_epi8 vreinterpretq_s8_u64

//__m128i_i16 _mm_castepu64_epi16 (__m128i_u64 a)
#define _mm_castepu64_epi16 vreinterpretq_s16_u64

//__m128i_i32 _mm_castepu64_epi32 (__m128i_u64 a)
#define _mm_castepu64_epi32 vreinterpretq_s32_u64

//__m128i_i64 _mm_castepu64_epi64 (__m128i_u64 a)
#define _mm_castepu64_epi64 vreinterpretq_s64_u64

//__m128i_u8 _mm_castepu64_epu8 (__m128i_u64 a)
#define _mm_castepu64_epu8 vreinterpretq_u8_u64

//__m128i_u16 _mm_castepu64_epu16 (__m128i_u64 a)
#define _mm_castepu64_epu16 vreinterpretq_u16_u64

//__m128i_u32 _mm_castepu64_epu32 (__m128i_u64 a)
#define _mm_castepu64_epu32 vreinterpretq_u32_u64

//__m128 _mm_castepu64_ps(__m128i_u64 a)
#define _mm_castepu64_ps vreinterpretq_f32_u64

//__m128d _mm_castepu64_pd(__m128i_u64 a)
#define _mm_castepu64_pd vreinterpretq_f64_u64
/*******************************************************************************/

/*******************************************************************************/
//__m128i_i8 _mm_castepi64_epi8(__m128i_i64 a)
#define _mm_castepi64_epi8 vreinterpretq_s8_s64

//__m128i_i16 _mm_castepi64_epi16(__m128i_i64 a)
#define _mm_castepi64_epi16 vreinterpretq_s16_s64

//__m128i_i32 _mm_castepi64_epi32(__m128i_i64 a)
#define _mm_castepi64_epi32 vreinterpretq_s32_s64

//__m128i_u8 _mm_castepi64_epu8(__m128i_i64 a)
#define _mm_castepi64_epu8 vreinterpretq_u8_s64

//__m128i_u16 _mm_castepi64_epu16(__m128i_i64 a)
#define _mm_castepi64_epu16 vreinterpretq_u16_s64

//__m128i_u32 _mm_castepi64_epu32(__m128i_i64 a)
#define _mm_castepi64_epu32 vreinterpretq_u32_s64

//__m128i_u64 _mm_castepi64_epu64(__m128i_i64 a)
#define _mm_castepi64_epu64 vreinterpretq_u64_s64

//__m128 _mm_castepi64_ps(__m128i_i64 a)
#define _mm_castepi64_ps vreinterpretq_f32_s64

//__m128d _mm_castepi64_pd(__m128i_i64 a)
#define _mm_castepi64_pd vreinterpretq_f64_s64
/*******************************************************************************/

/*******************************************************************************/
//__m128i_i8 _mm_castpd_epi8(__m128d a)
#define _mm_castpd_epi8 vreinterpretq_s8_f64

//__m128i_i16 _mm_castpd_epi16(__m128d a)
#define _mm_castpd_epi16 vreinterpretq_s16_f64

//__m128i_i32 _mm_castpd_epi32(__m128d a)
#define _mm_castpd_epi32 vreinterpretq_s32_f64

//__m128i_i64 _mm_castpd_epi64(__m128d a)
#define _mm_castpd_epi64 vreinterpretq_s64_f64

//__m128i_u8 _mm_castpd_epu8(__m128d a)
#define _mm_castpd_epu8 vreinterpretq_u8_f64

//__m128i_u16 _mm_castpd_epu16(__m128d a)
#define _mm_castpd_epu16 vreinterpretq_u16_f64

//__m128i_u32 _mm_castpd_epu32(__m128d a)
#define _mm_castpd_epu32 vreinterpretq_u32_f64

//__m128i_u64 _mm_castpd_epu64(__m128d a)
#define _mm_castpd_epu64 vreinterpretq_u64_f64
/*******************************************************************************/
#endif

