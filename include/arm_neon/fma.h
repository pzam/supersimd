/***************************************************************************************************/
// HEADER_NAME /arm_neon/fma.h
/***************************************************************************************************/
#if ARM
/*
AVX UNSUPPORTED
__m256 _mm256_fmadd_ps (__m256 a, __m256 b, __m256 c)
__m256d _mm256_fmadd_pd (__m256d a, __m256d b, __m256d c)
__m256 _mm256_fmsub_ps (__m256 a, __m256 b, __m256 c)
__m256d _mm256_fmsub_pd (__m256d a, __m256d b, __m256d c)
__m256 _mm256_fnmadd_ps (__m256 a, __m256 b, __m256 c)
__m256d _mm256_fnmadd_pd (__m256d a, __m256d b, __m256d c)
__m256 _mm256_fnmsub_ps (__m256 a, __m256 b, __m256 c)
__m256d _mm256_fnmsub_pd (__m256d a, __m256d b, __m256d c)
__m256 _mm256_fmsubadd_ps (__m256 a, __m256 b, __m256 c)
__m256d _mm256_fmsubadd_pd (__m256d a, __m256d b, __m256d c)
__m256 _mm256_fmaddsub_ps (__m256 a, __m256 b, __m256 c)
__m256d _mm256_fmaddsub_pd (__m256d a, __m256d b, __m256d c)
*/

__forceinline __m128 _mm_fmadd_ps (__m128 a, __m128 b, __m128 c)
{
	return vmlaq_f32(c, a, b);
}
#define _mm_fmadd_ss _mm_fmadd_ps

#if ARM64
__forceinline __m128d _mm_fmadd_pd (__m128d a, __m128d b, __m128d c)
{
	return vmlaq_f64(c, a, b);
}
#define _mm_fmadd_sd _mm_fmadd_pd
#endif

__forceinline __m128 _mm_fmsub_ps (__m128 a, __m128 b, __m128 c)
{
	return vmlaq_f32(vnegq_f32(c), a, b);
}
#define _mm_fmsub_ss _mm_fmsub_ps

#if ARM64
__forceinline __m128d _mm_fmsub_pd (__m128d a, __m128d b, __m128d c)
{
	return vmlaq_f64(vnegq_f64(c), a, b);
}

#define _mm_fmsub_sd _mm_fmsub_pd
#endif

__forceinline __m128 _mm_fnmadd_ps (__m128 a, __m128 b, __m128 c)
{
	return vmlsq_f32(c, a, b);
}
#define _mm_fnmadd_ss _mm_fnmadd_ps

#if ARM64
__forceinline __m128d _mm_fnmadd_pd (__m128d a, __m128d b, __m128d c)
{
	return vmlsq_f64(c, a, b);
}
#define _mm_fnmadd_sd _mm_fnmadd_pd
#endif

__forceinline __m128 _mm_fnmsub_ps (__m128 a, __m128 b, __m128 c)
{
	return vmlsq_f32(vnegq_f32(c), a, b);
}
#define _mm_fnmsub_ss _mm_fnmsub_ps

#if ARM64
__forceinline __m128d _mm_fnmsub_pd (__m128d a, __m128d b, __m128d c)
{
	return vmlsq_f64(vnegq_f64(c), a, b);
}
#define _mm_fnmsub_sd _mm_fnmsub_pd
#endif


__forceinline __m128 _mm_fmsubadd_ps (__m128 a, __m128 b, __m128 c)
{
	float ALIGNED(16) data[4] = { 1, -1, 1, -1 };
	float32x4_t sub = vld1q_f32(data);
	return vmlaq_f32(vmulq_f32(c, sub), a, b);
}
#if ARM64
__forceinline __m128d _mm_fmsubadd_pd (__m128d a, __m128d b, __m128d c)
{
	double ALIGNED(16) data[2] = { 1, -1 };
	float64x2_t sub = vld1q_f64(data);
	return vmlaq_f64(vmulq_f64(c, sub), a, b);
}
#endif

__forceinline __m128 _mm_fmaddsub_ps (__m128 a, __m128 b, __m128 c)
{
	float ALIGNED(16) data[4] = { -1, 1, -1, 1 };
	float32x4_t sub = vld1q_f32(data);
	return vmlaq_f32(vmulq_f32(c, sub), a, b);
}
#if ARM64
__forceinline __m128d _mm_fmaddsub_pd (__m128d a, __m128d b, __m128d c)
{
	double ALIGNED(16) data[2] = { -1, 1 };
	float64x2_t sub = vld1q_f64(data);
	return vmlaq_f64(vmulq_f64(c, sub), a, b);
}
#endif

#endif

