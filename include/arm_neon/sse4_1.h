/***************************************************************************************************/
// HEADER_NAME /arm_neon/sse4_1.h
/***************************************************************************************************/
#if ARM
#define __SSE4_1__ 1

/* Rounding mode macros. */
#define _MM_FROUND_TO_NEAREST_INT	0x00	//0
#define _MM_FROUND_TO_NEG_INF		0x01	//1
#define _MM_FROUND_TO_POS_INF		0x02	//2
#define _MM_FROUND_TO_ZERO			0x03	//3
#define _MM_FROUND_CUR_DIRECTION	0x04	//4

#define _MM_FROUND_RAISE_EXC		0x00	//0
#define _MM_FROUND_NO_EXC			0x08	//8

#define _MM_FROUND_NINT				0x00	//0
#define _MM_FROUND_FLOOR			0x01	//1
#define _MM_FROUND_CEIL				0x02	//2
#define _MM_FROUND_TRUNC			0x03	//3
#define _MM_FROUND_RINT				0x04	//4
#define _MM_FROUND_NEARBYINT		0x0C	//12

//TODO TEST ALL ROUNDING MODES

/***************************************************************************************************/
// Extract
/***************************************************************************************************/
#define _mm_extract_epi8(a, imm8) vgetq_lane_s8(a, imm8)
#define _mm_extract_epi32(a, imm8) vgetq_lane_s32(a, imm8)
#define _mm_extract_epi64(a, imm8) vgetq_lane_s64(a, imm8)

/* 
	//NOTE overloaded in extract_neon.h
	//int _mm_extract_ps (__m128 a, const int imm8)
	#define _mm_extract_ps (a, imm8) (int*)vget_lane_f32 (a, imm8)
*/

/***************************************************************************************************/
// Insert
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_insert_epi8 (__m128i_i8 a, int i, const int imm8)//MSVC BROKEN!!!
{
	// AVOIDS COMPILER ERROR, COMPILER OPTIMIZATION FIXES ANY INEFFICIENCY!!!
	if(imm8 == 0)
		return vsetq_lane_s8 (i, a, 0); 
	if(imm8 == 1)
		return vsetq_lane_s8 (i, a, 1); 
	if(imm8 == 2)
		return vsetq_lane_s8 (i, a, 2); 
	if(imm8 == 3)
		return vsetq_lane_s8 (i, a, 3); 
	if(imm8 == 4)
		return vsetq_lane_s8 (i, a, 4); 
	if(imm8 == 5)
		return vsetq_lane_s8 (i, a, 5); 
	if(imm8 == 6)
		return vsetq_lane_s8 (i, a, 6); 
	if(imm8 == 7)
		return vsetq_lane_s8 (i, a, 7); 
	if(imm8 == 8)
		return vsetq_lane_s8 (i, a, 8); 
	if(imm8 == 9)
		return vsetq_lane_s8 (i, a, 9); 
	if(imm8 == 10)
		return vsetq_lane_s8 (i, a, 10); 
	if(imm8 == 11)
		return vsetq_lane_s8 (i, a, 11); 
	if(imm8 == 12)
		return vsetq_lane_s8 (i, a, 12); 
	if(imm8 == 13)
		return vsetq_lane_s8 (i, a, 13); 
	if(imm8 == 14)
		return vsetq_lane_s8 (i, a, 14); 
	if(imm8 == 15)
		return vsetq_lane_s8 (i, a, 15); 
}

__forceinline __m128i_i32 _mm_insert_epi32 (__m128i_i32 a, int i, const int imm8)//MSVC BROKEN!!!
{
	// AVOIDS COMPILER ERROR, COMPILER OPTIMIZATION FIXES ANY INEFFICIENCY!!!
	if(imm8 == 0)
		return vsetq_lane_s32 (i, a, 0); 
	if(imm8 == 1)
		return vsetq_lane_s32 (i, a, 1); 
	if(imm8 == 2)
		return vsetq_lane_s32 (i, a, 2); 
	if(imm8 == 3)
		return vsetq_lane_s32 (i, a, 3); 
}

#if ARM64
__forceinline __m128i_i64 _mm_insert_epi64 (__m128i_i64 a, __int64 i, const int imm8)//MSVC BROKEN!!!
{
	// AVOIDS COMPILER ERROR, COMPILER OPTIMIZATION FIXES ANY INEFFICIENCY!!!
	if(imm8 == 0)
		return vsetq_lane_s64 (i, a, 0); 
	if(imm8 == 1)
		return vsetq_lane_s64 (i, a, 1); 
}
#endif

__forceinline __m128 _mm_insert_ps (__m128 a, __m128 b, const int imm8)//MSVC BROKEN!!!
{
	// AVOIDS COMPILER ERROR, COMPILER OPTIMIZATION FIXES ANY INEFFICIENCY!!!
	if(imm8 == 0)
		return vsetq_lane_f32 (vgetq_lane_f32(b, 0), a, 0); 
	if(imm8 == 1)
		return vsetq_lane_f32 (vgetq_lane_f32(b, 1), a, 1); 
	if(imm8 == 2)
		return vsetq_lane_f32 (vgetq_lane_f32(b, 2), a, 2); 
	if(imm8 == 3)
		return vsetq_lane_f32 (vgetq_lane_f32(b, 3), a, 3); 
}

/***************************************************************************************************/
// Bitwise Selection
/***************************************************************************************************/
__forceinline __m128i_i16 _mm_blend_epi16 (__m128i_i16 a, __m128i_i16 b, const int imm8)
{

	uint16_t v0 = 0x0;
	uint16_t v1 = 0x0;
	uint16_t v2 = 0x0;
	uint16_t v3 = 0x0;
	uint16_t v4 = 0x0;
	uint16_t v5 = 0x0;
	uint16_t v6 = 0x0;
	uint16_t v7 = 0x0;
	
	if(imm8 & 0x80)
		v7 = 0xFFFF;
	if(imm8 & 0x40)
		v6 = 0xFFFF;
	if(imm8 & 0x20)
		v5 = 0xFFFF;
	if(imm8 & 0x10)
		v4 = 0xFFFF;
	if(imm8 & 0x08)
		v3 = 0xFFFF;
	if(imm8 & 0x04)
		v2 = 0xFFFF;
	if(imm8 & 0x02)
		v1 = 0xFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFF;
	
	uint16_t ALIGNED(16) v_data[8] = { v0, v1, v2, v3, v4, v5, v6, v7 };
	uint16x8_t mask = vld1q_u16(v_data);
	
	return vbslq_s16( mask, b, a );
}

#if ARM64
__forceinline __m128d _mm_blend_pd (__m128d a, __m128d b, const int imm8)
{

	uint64_t v0 = 0x0;
	uint64_t v1 = 0x0;
	
	if(imm8 & 0x02)
		v1 = 0xFFFFFFFFFFFFFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFFFFFFFFFFFFFF;
	
		uint64_t ALIGNED(16) v_data[2] = { v0, v1 };
		uint64x2_t mask = vld1q_u64(v_data);
	
	return vbslq_f64( mask, b, a );
}
#endif

__forceinline __m128 _mm_blend_ps (__m128 a, __m128 b, const int imm8)
{

	uint32_t v0 = 0x0;
	uint32_t v1 = 0x0;
	uint32_t v2 = 0x0;
	uint32_t v3 = 0x0;
	
	if(imm8 & 0x08)
		v3 = 0xFFFFFFFF;
	if(imm8 & 0x04)
		v2 = 0xFFFFFFFF;
	if(imm8 & 0x02)
		v1 = 0xFFFFFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFFFFFF;
	
		uint32_t ALIGNED(16) v_data[4] = { v0, v1, v2, v3 };
		uint32x4_t mask = vld1q_u32(v_data);
	
	return vbslq_f32( mask, b, a );
}

__forceinline __m128i_i8 _mm_blendv_epi8 (__m128i_i8 a, __m128i_i8 b, __m128i_i8 mask)
{
	return vbslq_s8(vreinterpretq_u8_s8(mask), b, a);
}

#if ARM64
__forceinline __m128d _mm_blendv_pd (__m128d a, __m128d b, __m128d mask)
{
	return vbslq_f64(vreinterpretq_u64_f64(mask), b, a);
}
#endif

__forceinline __m128 _mm_blendv_ps (__m128 a, __m128 b, __m128 mask)
{
	return vbslq_f32(vreinterpretq_u32_f32(mask), b, a);
}

/***************************************************************************************************/
// Test NOTE MAKE INTO PORTABLE VERSIONS!!!!
/***************************************************************************************************/
__forceinline int _mm_test_all_ones (__m128i_i32 a)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(a,vreinterpretq_s32_u32(vceqq_s32(a,a)))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}


__forceinline int _mm_test_all_zeros (__m128i_i32 a, __m128i_i32 mask)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(vandq_s32(a, mask),vbicq_s32(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_test_mix_ones_zeros (__m128i_i32 a, __m128i_i32 mask)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(vandq_s32(a, mask),vbicq_s32(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(a,vreinterpretq_s32_u32(vceqq_s32(a,a)))), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}

__forceinline int _mm_testc_si128 (__m128i_i32 a, __m128i_i32 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(a,b)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_si128 (__m128i_i32 a, __m128i_i32 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(vandq_s32(a, b),vbicq_s32(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_si128 (__m128i_i32 a, __m128i_i32 b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(vandq_s32(a, b),vbicq_s32(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(a,b)), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}

/***************************************************************************************************/
// Rounding
/***************************************************************************************************/
#if ARM64
__forceinline __m128d _mm_floor_pd (__m128d a)//MSVC BROKEN!!!
{
	uint64x2_t mask = vcgtq_f64(a, vdupq_n_f64(0));
	float64x2_t b1 = vcvtq_f64_s64(vcvtq_s64_f64(a));
	float64x2_t b2 = vcvtq_f64_s64(vcvtq_s64_f64(vsubq_f64(a, vdupq_n_f64(0.9998999999999999))));
	return vbslq_f64(mask, b1, b2);
}

#define _mm_floor_sd _mm_floor_pd
#endif

__forceinline __m128 _mm_floor_ps (__m128 a)//MSVC BROKEN!!!
{
	uint32x4_t mask = vcgtq_f32(a, vdupq_n_f32(0));
	float32x4_t b1 = vcvtq_f32_s32(vcvtq_s32_f32(a));
	float32x4_t b2 = vcvtq_f32_s32(vcvtq_s32_f32(vsubq_f32(a, vdupq_n_f32(0.9998999))));
	return vbslq_f32(mask, b1, b2);

}
#define _mm_floor_ss _mm_floor_ps

#if ARM64
__forceinline __m128d _mm_ceil_pd (__m128d a)
{
	uint64x2_t mask = vcleq_f64(a, vdupq_n_f64(0));
	float64x2_t b1 = vcvtq_f64_s64(vcvtq_s64_f64(a));
	float64x2_t b2 = vaddq_f64(vcvtq_f64_s64(vcvtq_s64_f64(vsubq_f64(a, vdupq_n_f64(0.0001)))), vdupq_n_f64(1));
	return vbslq_f64(mask, b1, b2);
}
#define _mm_ceil_sd _mm_ceil_pd
#endif
__forceinline __m128 _mm_ceil_ps (__m128 a)//MSVC BROKEN!!!
{
	uint32x4_t mask = vcleq_f32(a, vdupq_n_f32(0));
	float32x4_t b1 = vcvtq_f32_s32(vcvtq_s32_f32(a));
	float32x4_t b2 = vaddq_f32(vcvtq_f32_s32(vcvtq_s32_f32(vsubq_f32(a, vdupq_n_f32(0.0001)))), vdupq_n_f32(1));
	return vbslq_f32(mask, b1, b2);
}
#define _mm_ceil_ss _mm_ceil_ps

#if ARM64
__forceinline __m128d _mm_round_pd (__m128d a, int rounding)
{
	if(
		rounding == 0 || 
		rounding == 4|| rounding == 5 || 
		rounding == 6|| rounding == 7 ||
		rounding == 8 || rounding == 12 ||
		rounding == 13 || rounding == 14 ||
		rounding == 15 || rounding == 16
	)
	{
		uint64x2_t mask = vcltq_f64(a, vdupq_n_f64(0));
		float64x2_t b1 = vcvtq_f64_s64(vcvtq_s64_f64(vsubq_f64(a, vdupq_n_f64(0.5))));
		float64x2_t b2 = vcvtq_f64_s64(vcvtq_s64_f64(vaddq_f64(a, vdupq_n_f64(0.5))));
		return vbslq_f64(mask, b1, b2);
		
	}
	if(rounding == 1 || rounding == 9)
	{
		uint64x2_t mask = vcgtq_f64(a, vdupq_n_f64(0));
		float64x2_t b1 = vcvtq_f64_s64(vcvtq_s64_f64(a));
		float64x2_t b2 = vcvtq_f64_s64(vcvtq_s64_f64(vsubq_f64(a, vdupq_n_f64(0.9998999999999999))));
		return vbslq_f64(mask, b1, b2);
	}
	
	if(rounding == 2 || rounding == 10)
	{
		uint64x2_t mask = vcleq_f64(a, vdupq_n_f64(0));
		float64x2_t b1 = vcvtq_f64_s64(vcvtq_s64_f64(a));
		float64x2_t b2 = vaddq_f64(vcvtq_f64_s64(vcvtq_s64_f64(vsubq_f64(a, vdupq_n_f64(0.0001)))), vdupq_n_f64(1));
		return vbslq_f64(mask, b1, b2);
	}
	
	if
	(
		rounding == 3 || rounding == 11
	)
	{
		return vcvtq_f64_s64(vcvtq_s64_f64(a));
	}
}

__forceinline __m128d _mm_round_sd (__m128d a, __m128d b, int rounding)
{
	if(
		rounding == 0 || 
		rounding == 4|| rounding == 5 || 
		rounding == 6|| rounding == 7 ||
		rounding == 8 || rounding == 12 ||
		rounding == 13 || rounding == 14 ||
		rounding == 15 || rounding == 16
	)
	{
		double ALIGNED(16) data[2] = { 1, 0};
		float64x2_t cmp = vld1q_f64(data);
		uint64x2_t pass = vceqq_f64(cmp, vdupq_n_f64(1));
		
		uint64x2_t mask = vcltq_f64(b, vdupq_n_f64(0));
		float64x2_t b1 = vcvtq_f64_s64(vcvtq_s64_f64(vsubq_f64(b, vdupq_n_f64(0.5))));
		float64x2_t b2 = vcvtq_f64_s64(vcvtq_s64_f64(vaddq_f64(b, vdupq_n_f64(0.5))));
		return vbslq_f64(pass,vbslq_f64(mask, b1, b2), a);
		
	}
	if(rounding == 1 || rounding == 9)
	{
		double ALIGNED(16) data[2] = { 1, 0 };
		float64x2_t cmp = vld1q_f64(data);
		uint64x2_t pass = vceqq_f64(cmp, vdupq_n_f64(1));
		
		uint64x2_t mask = vcgtq_f64(b, vdupq_n_f64(0));
		float64x2_t b1 = vcvtq_f64_s64(vcvtq_s64_f64(b));
		float64x2_t b2 = vcvtq_f64_s64(vcvtq_s64_f64(vsubq_f64(b, vdupq_n_f64(0.9998999999999999))));
		return vbslq_f64(pass,vbslq_f64(mask, b1, b2),a);
	}
	
	if(rounding == 2 || rounding == 10)
	{
		double ALIGNED(16) data[2] = { 1, 0 };
		float64x2_t cmp = vld1q_f64(data);
		uint64x2_t pass = vceqq_f64(cmp, vdupq_n_f64(1));
		
		uint64x2_t mask = vcleq_f64(b, vdupq_n_f64(0));
		float64x2_t b1 = vcvtq_f64_s64(vcvtq_s64_f64(b));
		float64x2_t b2 = vaddq_f64(vcvtq_f64_s64(vcvtq_s64_f64(vsubq_f64(b, vdupq_n_f64(0.0001)))), vdupq_n_f64(1));
		return vbslq_f64(pass,vbslq_f64(mask, b1, b2),a);
	}
	
	if
	(
		rounding == 3 || rounding == 11
	)
	{
		double ALIGNED(16) data[2] = { 1, 0 };
		float64x2_t cmp = vld1q_f64(data);
		uint64x2_t pass = vceqq_f64(cmp, vdupq_n_f64(1));
		
		return vbslq_f64(pass,vcvtq_f64_s64(vcvtq_s64_f64(b)),a);
	}
}

#endif

__forceinline __m128 _mm_round_ps (__m128 a, int rounding)
{
	if(
		rounding == 0 || 
		rounding == 4|| rounding == 5 || 
		rounding == 6|| rounding == 7 ||
		rounding == 8 || rounding == 12 ||
		rounding == 13 || rounding == 14 ||
		rounding == 15 || rounding == 16
	)
	{
		uint32x4_t mask = vcltq_f32(a, vdupq_n_f32(0));
		float32x4_t b1 = vcvtq_f32_s32(vcvtq_s32_f32(vsubq_f32(a, vdupq_n_f32(0.5))));
		float32x4_t b2 = vcvtq_f32_s32(vcvtq_s32_f32(vaddq_f32(a, vdupq_n_f32(0.5))));
		return vbslq_f32(mask, b1, b2);
		
	}
	if(rounding == 1 || rounding == 9)
	{
		uint32x4_t mask = vcgtq_f32(a, vdupq_n_f32(0));
		float32x4_t b1 = vcvtq_f32_s32(vcvtq_s32_f32(a));
		float32x4_t b2 = vcvtq_f32_s32(vcvtq_s32_f32(vsubq_f32(a, vdupq_n_f32(0.9998999))));
		return vbslq_f32(mask, b1, b2);
	}
	
	if(rounding == 2 || rounding == 10)
	{
		uint32x4_t mask = vcleq_f32(a, vdupq_n_f32(0));
		float32x4_t b1 = vcvtq_f32_s32(vcvtq_s32_f32(a));
		float32x4_t b2 = vaddq_f32(vcvtq_f32_s32(vcvtq_s32_f32(vsubq_f32(a, vdupq_n_f32(0.0001)))), vdupq_n_f32(1));
		return vbslq_f32(mask, b1, b2);
	}
	
	if
	(
		rounding == 3 || rounding == 11
	)
	{
		return vcvtq_f32_s32(vcvtq_s32_f32(a));
	}
}
__forceinline __m128 _mm_round_ss (__m128 a, __m128 b, int rounding)
{
	if(
		rounding == 0 || 
		rounding == 4|| rounding == 5 || 
		rounding == 6|| rounding == 7 ||
		rounding == 8 || rounding == 12 ||
		rounding == 13 || rounding == 14 ||
		rounding == 15 || rounding == 16
	)
	{
		float ALIGNED(16) data[4] = { 1, 0, 0, 0 };
		float32x4_t cmp = vld1q_f32(data);
		uint32x4_t pass = vceqq_f32(cmp, vdupq_n_f32(1));
		
		uint32x4_t mask = vcltq_f32(b, vdupq_n_f32(0));
		float32x4_t b1 = vcvtq_f32_s32(vcvtq_s32_f32(vsubq_f32(b, vdupq_n_f32(0.5))));
		float32x4_t b2 = vcvtq_f32_s32(vcvtq_s32_f32(vaddq_f32(b, vdupq_n_f32(0.5))));
		return vbslq_f32(pass,vbslq_f32(mask, b1, b2), a);
		
	}
	if(rounding == 1 || rounding == 9)
	{
		float ALIGNED(16) data[4] = { 1, 0, 0, 0 };
		float32x4_t cmp = vld1q_f32(data);
		uint32x4_t pass = vceqq_f32(cmp, vdupq_n_f32(1));
		
		uint32x4_t mask = vcgtq_f32(b, vdupq_n_f32(0));
		float32x4_t b1 = vcvtq_f32_s32(vcvtq_s32_f32(b));
		float32x4_t b2 = vcvtq_f32_s32(vcvtq_s32_f32(vsubq_f32(b, vdupq_n_f32(0.9998999))));
		return vbslq_f32(pass,vbslq_f32(mask, b1, b2),a);
	}
	
	if(rounding == 2 || rounding == 10)
	{
		float ALIGNED(16) data[4] = { 1, 0, 0, 0 };
		float32x4_t cmp = vld1q_f32(data);
		uint32x4_t pass = vceqq_f32(cmp, vdupq_n_f32(1));
		
		uint32x4_t mask = vcleq_f32(b, vdupq_n_f32(0));
		float32x4_t b1 = vcvtq_f32_s32(vcvtq_s32_f32(b));
		float32x4_t b2 = vaddq_f32(vcvtq_f32_s32(vcvtq_s32_f32(vsubq_f32(b, vdupq_n_f32(0.0001)))), vdupq_n_f32(1));
		return vbslq_f32(pass,vbslq_f32(mask, b1, b2),a);
	}
	
	if
	(
		rounding == 3 || rounding == 11
	)
	{
		float ALIGNED(16) data[4] = { 1, 0, 0, 0 };
		float32x4_t cmp = vld1q_f32(data);
		uint32x4_t pass = vceqq_f32(cmp, vdupq_n_f32(1));
		
		return vbslq_f32(pass,vcvtq_f32_s32(vcvtq_s32_f32(b)),a);
	}
}


/***************************************************************************************************/
// Comparator
/***************************************************************************************************/
#if ARM64
__forceinline __m128i_i64 _mm_cmpeq_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	return vreinterpretq_s64_u64(vceqq_s64(a, b));
}
#endif

/***************************************************************************************************/
// Multiply 
/***************************************************************************************************/

__forceinline __m128i_i32 _mm_mul_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vmulq_s32(a, b);
}
#define _mm_mullo_epi32 _mm_mul_epi32
/***************************************************************************************************/
// Multiply Absolute Difference
/***************************************************************************************************/
#define abs_t(x) ( ( (x) < 0) ? -(x) : (x) )
__forceinline __m128i_u16 _mm_mpsadbw_epu8 (__m128i_u8 a, __m128i_u8 b, const int imm8)
{
	#if _MSC_VER
		__m128i_u16 ret;
		int temp1, temp2, temp3, temp4, index;
		int k =  (imm8 & 0x04);
		int l = ((imm8 & 0x03) * 4);
		
		for (index = 0; index < 8; index++)
		{
			temp1 = abs_t(a.n128_u8[k + index + 0] - b.n128_u8[l + 0]);
			temp2 = abs_t(a.n128_u8[k + index + 1] - b.n128_u8[l + 1]);
			temp3 = abs_t(a.n128_u8[k + index + 2] - b.n128_u8[l + 2]);
			temp4 = abs_t(a.n128_u8[k + index + 3] - b.n128_u8[l + 3]);
			ret.n128_u16[index] = temp1 + temp2 + temp3 + temp4;
		}
		return ret;
	#else
		__m128i_u16 ret;
		int temp1, temp2, temp3, temp4, index;
		int k =  (imm8 & 0x04);
		int l = ((imm8 & 0x03) * 4);
		
		for (index = 0; index < 8; index++)
		{
			temp1 = abs_t(a[k + index + 0] - b[l + 0]);
			temp2 = abs_t(a[k + index + 1] - b[l + 1]);
			temp3 = abs_t(a[k + index + 2] - b[l + 2]);
			temp4 = abs_t(a[k + index + 3] - b[l + 3]);
			ret[index] = temp1 + temp2 + temp3 + temp4;
		}
		return ret;
	#endif
}

/***************************************************************************************************/
// MAX
/***************************************************************************************************/

__forceinline __m128i_i8 _mm_max_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return vmaxq_s8(a, b);
}
__forceinline __m128i_i32 _mm_max_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vmaxq_s32(a, b);
}

__forceinline __m128i_u16 _mm_max_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	return vmaxq_u16(a, b);
}
__forceinline __m128i_u32 _mm_max_epu32 (__m128i_u32 a, __m128i_u32 b)
{
	return vmaxq_u32(a, b);
}
/***************************************************************************************************/
// MIN
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_min_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return vminq_s8(a, b);
}
__forceinline __m128i_i32 _mm_min_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vminq_s32(a, b);
}

__forceinline __m128i_u16 _mm_min_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	return vminq_u16(a, b);
}
__forceinline __m128i_u32 _mm_min_epu32 (__m128i_u32 a, __m128i_u32 b)
{
	return vminq_u32(a, b);
}

__forceinline __m128i_u16 _mm_minpos_epu16 (__m128i_u16 a)
{
	uint16_t pos = 0;
	uint16_t min = vgetq_lane_u16(a,0);
	if( vgetq_lane_u16(a,0) > vgetq_lane_u16(a,1) )
	{
		pos = 1;
		min = vgetq_lane_u16(a,1);
	}
	else if( min > vgetq_lane_u16(a,2) )
	{
		pos = 2;
		min = vgetq_lane_u16(a,2);
	}
	else if( min > vgetq_lane_u16(a,3) )
	{
		pos = 3;
		min = vgetq_lane_u16(a,3);
	}
	else if( min > vgetq_lane_u16(a,4) )
	{
		pos = 4;
		min = vgetq_lane_u16(a,4);
	}
	else if( min > vgetq_lane_u16(a,5) )
	{
		pos = 5;
		min = vgetq_lane_u16(a,5);
	}
	else if( min > vgetq_lane_u16(a,6) )
	{
		pos = 6;
		min = vgetq_lane_u16(a,6);
	}
	else if( min > vgetq_lane_u16(a,7) )
	{
		pos = 7;
		min = vgetq_lane_u16(a,7);
	}
	
	uint16_t ALIGNED(16) data[8] = 
	{ 
		min, 
		pos, 
		0, 
		0,
		0, 
		0, 
		0, 
		0
	};
	return vld1q_u16(data);
}
/***************************************************************************************************/
// Dot Product
/***************************************************************************************************/
#if ARM64
__forceinline __m128d _mm_dp_pd (__m128d a, __m128d b, const int imm8)
{
	if(imm8 < 0xFF)
	{
		uint64_t s0 = 0x0;
		uint64_t s1 = 0x0;
		
		uint64_t v0 = 0x0;
		uint64_t v1 = 0x0;
		
		if(imm8 & 0x20)
			s1 = 0xFFFFFFFFFFFFFFFF;
		if(imm8 & 0x10)
			s0 = 0xFFFFFFFFFFFFFFFF;
		
		if(imm8 & 0x02)
			v1 = 0xFFFFFFFFFFFFFFFF;
		if(imm8 & 0x01)
			v0 = 0xFFFFFFFFFFFFFFFF;
		
		uint64_t ALIGNED(16) s_data[2] = { s0, s1 };
		uint64x2_t mmul = vld1q_u64(s_data);
		
		uint64_t ALIGNED(16) v_data[2] = { v0, v1 };
		uint64x2_t sel = vld1q_u64(v_data);
		
		float64x2_t mul = vbslq_f64 
		(
			mmul,
			vmulq_f64 (a, b),
			vdupq_n_f64(0)
		);
		
		return vbslq_f64 
		(
			sel,
			vdupq_n_f64( vaddvq_f64( mul ) ),
			vdupq_n_f64(0)
		);
	
	}
	else
	{
		return vdupq_n_f64( vaddvq_f64(vmulq_f64 (a, b)) );
	}
}
#endif

__forceinline __m128 _mm_dp_ps (__m128 a, __m128 b, const int imm8)
{
	if(imm8 < 0xFF)
	{
		uint32_t s0 = 0x0;
		uint32_t s1 = 0x0;
		uint32_t s2 = 0x0;
		uint32_t s3 = 0x0;
		
		uint32_t v0 = 0x0;
		uint32_t v1 = 0x0;
		uint32_t v2 = 0x0;
		uint32_t v3 = 0x0;
		
		if(imm8 & 0x80)
			s3 = 0xFFFFFFFF;
		if(imm8 & 0x40)
			s2 = 0xFFFFFFFF;
		if(imm8 & 0x20)
			s1 = 0xFFFFFFFF;
		if(imm8 & 0x10)
			s0 = 0xFFFFFFFF;
		
		if(imm8 & 0x08)
			v3 = 0xFFFFFFFF;
		if(imm8 & 0x04)
			v2 = 0xFFFFFFFF;
		if(imm8 & 0x02)
			v1 = 0xFFFFFFFF;
		if(imm8 & 0x01)
			v0 = 0xFFFFFFFF;
		
		uint32_t ALIGNED(16) s_data[4] = { s0, s1, s2, s3 };
		uint32x4_t mmul = vld1q_u32(s_data);
		
		uint32_t ALIGNED(16) v_data[4] = { v0, v1, v2, v3 };
		uint32x4_t sel = vld1q_u32(v_data);
		
		float32x4_t mul = vbslq_f32 
		(
			mmul,
			vmulq_f32 (a, b),
			vdupq_n_f32(0)
		);
		
		return vbslq_f32 
		(
			sel,
			vdupq_n_f32( vaddvq_f32( mul ) ),
			vdupq_n_f32(0)
		);
	
	}
	else
	{
		return vdupq_n_f32( vaddvq_f32(vmulq_f32 (a, b)) );
	}
}

/***************************************************************************************************/
// CVT
/***************************************************************************************************/

__forceinline __m128i_i32 _mm_cvtepi16_epi32 (__m128i_i16 a)
{
	int32_t ALIGNED(16) data[4] = 
	{ 
		(int32_t)vgetq_lane_s16(a,0), 
		(int32_t)vgetq_lane_s16(a,1), 
		(int32_t)vgetq_lane_s16(a,2), 
		(int32_t)vgetq_lane_s16(a,3) 
	};
	return vld1q_s32(data);
}
#if ARM64
__forceinline __m128i_i64 _mm_cvtepi16_epi64 (__m128i_i16 a)
{
	int64_t ALIGNED(16) data[2] = 
	{ 
		(int64_t)vgetq_lane_s16(a,0), 
		(int64_t)vgetq_lane_s16(a,1)
	};
	return vld1q_s64(data);
}
__forceinline __m128i_i64 _mm_cvtepi32_epi64 (__m128i_i32 a)
{
	int64_t ALIGNED(16) data[2] = 
	{ 
		(int64_t)vgetq_lane_s32(a,0), 
		(int64_t)vgetq_lane_s32(a,1)
	};
	return vld1q_s64(data);
}
#endif

__forceinline __m128i_i16 _mm_cvtepi8_epi16 (__m128i_i8 a)//vmovl_s8(a)
{
	int16_t ALIGNED(16) data[8] = 
	{ 
		(int16_t)vgetq_lane_s8(a,0), 
		(int16_t)vgetq_lane_s8(a,1), 
		(int16_t)vgetq_lane_s8(a,2), 
		(int16_t)vgetq_lane_s8(a,3),
		(int16_t)vgetq_lane_s8(a,4), 
		(int16_t)vgetq_lane_s8(a,5), 
		(int16_t)vgetq_lane_s8(a,6), 
		(int16_t)vgetq_lane_s8(a,7)
	};
	return vld1q_s16(data);
}

__forceinline __m128i_i32 _mm_cvtepi8_epi32 (__m128i_i8 a)
{
	int32_t ALIGNED(16) data[4] = 
	{ 
		(int32_t)vgetq_lane_s8(a,0), 
		(int32_t)vgetq_lane_s8(a,1), 
		(int32_t)vgetq_lane_s8(a,2), 
		(int32_t)vgetq_lane_s8(a,3)
	};
	return vld1q_s32(data);
}

#if ARM64
__forceinline __m128i_i64 _mm_cvtepi8_epi64 (__m128i_i8 a)
{
	int64_t ALIGNED(16) data[2] = 
	{ 
		(int64_t)vgetq_lane_s8(a,0), 
		(int64_t)vgetq_lane_s8(a,1)
	};
	return vld1q_s64(data);
}
#endif

__forceinline __m128i_i32 _mm_cvtepu16_epi32 (__m128i_u16 a)
{
	int32_t ALIGNED(16) data[4] = 
	{ 
		(int32_t)vgetq_lane_u16(a,0), 
		(int32_t)vgetq_lane_u16(a,1), 
		(int32_t)vgetq_lane_u16(a,2), 
		(int32_t)vgetq_lane_u16(a,3)
	};
	return vld1q_s32(data);
}
#if ARM64
__forceinline __m128i_i64 _mm_cvtepu16_epi64 (__m128i_u16 a)
{
	int64_t ALIGNED(16) data[2] = 
	{ 
		(int64_t)vgetq_lane_u16(a,0), 
		(int64_t)vgetq_lane_u16(a,1)
	};
	return vld1q_s64(data);
}
__forceinline __m128i_i64 _mm_cvtepu32_epi64 (__m128i_u32 a)
{
	int64_t ALIGNED(16) data[2] = 
	{ 
		(int64_t)vgetq_lane_u32(a,0), 
		(int64_t)vgetq_lane_u32(a,1)
	};
	return vld1q_s64(data);
}
#endif
__forceinline __m128i_i16 _mm_cvtepu8_epi16 (__m128i_u8 a)
{
	int16_t ALIGNED(16) data[8] = 
	{ 
		(int16_t)vgetq_lane_u8(a,0), 
		(int16_t)vgetq_lane_u8(a,1), 
		(int16_t)vgetq_lane_u8(a,2), 
		(int16_t)vgetq_lane_u8(a,3),
		(int16_t)vgetq_lane_u8(a,4), 
		(int16_t)vgetq_lane_u8(a,5), 
		(int16_t)vgetq_lane_u8(a,6), 
		(int16_t)vgetq_lane_u8(a,7)
	};
	return vld1q_s16(data);
}

__forceinline __m128i_i32 _mm_cvtepu8_epi32 (__m128i_u8 a)
{
	int32_t ALIGNED(16) data[4] = 
	{ 
		(int32_t)vgetq_lane_u8(a,0), 
		(int32_t)vgetq_lane_u8(a,1), 
		(int32_t)vgetq_lane_u8(a,2), 
		(int32_t)vgetq_lane_u8(a,3)
	};
	return vld1q_s32(data);
}
#if ARM64
__forceinline __m128i_i64 _mm_cvtepu8_epi64 (__m128i_u8 a)
{
	int64_t ALIGNED(16) data[2] = 
	{ 
		(int64_t)vgetq_lane_u8(a,0), 
		(int64_t)vgetq_lane_u8(a,1)
	};
	return vld1q_s64(data);
}
#endif

__forceinline __m128i_u16 _mm_packus_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vcombine_u16( vqmovun_s32(a), vqmovun_s32(b) );
}

/***************************************************************************************************/
// Load NOTE MAKE INTO PORTABLE VERSIONS!!!!
/***************************************************************************************************/
__forceinline __m128i_i32 _mm_stream_load_si128 (const __m128i_i32* mem_addr)
{
	__m128i_i32 ret = *mem_addr;
	return ret;
}
#endif

