/***************************************************************************************************/
// HEADER_NAME /arm_neon/sse.h
/***************************************************************************************************/
#if ARM
/*
NEED MSVC x32 AND ARM32 versions
_mm_sfence (void)
_mm_setcsr (unsigned int a)
_mm_getcsr (void)
_mm_prefetch (char const* p, int i)
*/

#define __SSE__ 1

/***************************************************************************************************/
// FPU Control Macro
/***************************************************************************************************/
#define _MM_HINT_NTA 0
#define _MM_HINT_T0 1
#define _MM_HINT_T1 2
#define _MM_HINT_T2 3



/***************************************************************************************************/
// MACRO functions for setting and reading the MXCSR
/***************************************************************************************************/
/*
#define _MM_EXCEPT_MASK       //0x003f
#define _MM_EXCEPT_INVALID    //0x0001
#define _MM_EXCEPT_DENORM     //0x0002
#define _MM_EXCEPT_DIV_ZERO   //0x0004
#define _MM_EXCEPT_OVERFLOW   //0x0008
#define _MM_EXCEPT_UNDERFLOW  //0x0010
#define _MM_EXCEPT_INEXACT    //0x0020

#define _MM_MASK_MASK         //0x1f80
#define _MM_MASK_INVALID      //0x0080
#define _MM_MASK_DENORM       //0x0100
#define _MM_MASK_DIV_ZERO     //0x0200
#define _MM_MASK_OVERFLOW     //0x0400
#define _MM_MASK_UNDERFLOW    //0x0800
#define _MM_MASK_INEXACT      //0x1000

#define _MM_ROUND_MASK        //0x6000
#define _MM_ROUND_NEAREST     //0x0000
#define _MM_ROUND_DOWN        //0x2000
#define _MM_ROUND_UP          //0x4000
#define _MM_ROUND_TOWARD_ZERO //0x6000

#define _MM_FLUSH_ZERO_MASK   //0x8000
#define _MM_FLUSH_ZERO_ON     //0x8000
#define _MM_FLUSH_ZERO_OFF    //0x0000
*/

/***************************************************************************************************/
// Control Register Macros 
/***************************************************************************************************/
#define _MM_SET_EXCEPTION_STATE(mask)                               \
			_mm_setcsr((_mm_getcsr() & ~_MM_EXCEPT_MASK) | (mask))
#define _MM_GET_EXCEPTION_STATE()                                   \
			(_mm_getcsr() & _MM_EXCEPT_MASK)

#define _MM_SET_EXCEPTION_MASK(mask)                                \
			_mm_setcsr((_mm_getcsr() & ~_MM_MASK_MASK) | (mask))
#define _MM_GET_EXCEPTION_MASK()                                    \
			(_mm_getcsr() & _MM_MASK_MASK)

#define _MM_SET_ROUNDING_MODE(mode)                                 \
			_mm_setcsr((_mm_getcsr() & ~_MM_ROUND_MASK) | (mode))
#define _MM_GET_ROUNDING_MODE()                                     \
			(_mm_getcsr() & _MM_ROUND_MASK)

#define _MM_SET_FLUSH_ZERO_MODE(mode)                               \
			_mm_setcsr((_mm_getcsr() & ~_MM_FLUSH_ZERO_MASK) | (mode))
#define _MM_GET_FLUSH_ZERO_MODE(mode)                               \
			(_mm_getcsr() & _MM_FLUSH_ZERO_MASK)

/***************************************************************************************************/
// Shuffle
/***************************************************************************************************/
#define _MM_SHUFFLE(z, y, x, w)	( (z<<6) | (y<<4) | (x<<2) | w )
#define _MM_TRANSPOSE4_PS(row0, row1, row2, row3) {								\
			__m128 _Tmp3, _Tmp2, _Tmp1, _Tmp0;									\
																				\
			_Tmp0 = _mm_shuffle_ps((row0), (row1), _MM_SHUFFLE(1,0,1,0));		\
			_Tmp2 = _mm_shuffle_ps((row0), (row1), _MM_SHUFFLE(3,2,3,2));		\
			_Tmp1 = _mm_shuffle_ps((row2), (row3), _MM_SHUFFLE(1,0,1,0));		\
			_Tmp3 = _mm_shuffle_ps((row2), (row3), _MM_SHUFFLE(3,2,3,2));		\
																				\
			(row0) = _mm_shuffle_ps(_Tmp0, _Tmp1, _MM_SHUFFLE(2,0,2,0));		\
			(row1) = _mm_shuffle_ps(_Tmp0, _Tmp1, _MM_SHUFFLE(3,1,3,1));		\
			(row2) = _mm_shuffle_ps(_Tmp2, _Tmp3, _MM_SHUFFLE(2,0,2,0));		\
			(row3) = _mm_shuffle_ps(_Tmp2, _Tmp3, _MM_SHUFFLE(3,1,3,1));		\
}

__forceinline __m64i_i16 _mm_shuffle_pi16(__m64i_i16 a, int imm8 ) { 
	#if _MSC_VER
	switch (imm8) {
	default:
		__m64i_i16 ret;
		ret.n64_i16[0] = a.n64_i16[imm8 & 0x3];
		ret.n64_i16[1] = a.n64_i16[(imm8 >> 2) & 0x3];
		ret.n64_i16[2] = a.n64_i16[(imm8 >> 4) & 0x3];
		ret.n64_i16[3] = a.n64_i16[(imm8 >> 6) & 0x3];
		return ret;
	}
	#else
	switch (imm8) {
	default:
		__m64i_i16 ret;
		ret[0] = a[imm8 & 0x3];
		ret[1] = a[(imm8 >> 2) & 0x3];
		ret[2] = a[(imm8 >> 4) & 0x3];
		ret[3] = a[(imm8 >> 6) & 0x3];
		return ret;
	}
	#endif
	//Might Have To Reverse Order
}


#define _m_pshufw _mm_shuffle_pi16

__forceinline __m128 _mm_shuffle_ps(__m128 a, __m128 b, int imm8 ) { 
	#if _MSC_VER
	switch (imm8) {
	default:
		__m128 ret;
		ret.n128_f32[0] = a.n128_f32[imm8 & 0x3];
		ret.n128_f32[1] = a.n128_f32[(imm8 >> 2) & 0x3];
		ret.n128_f32[2] = b.n128_f32[(imm8 >> 4) & 0x3];
		ret.n128_f32[3] = b.n128_f32[(imm8 >> 6) & 0x3];
		return ret;
	}
	#else
	switch (imm8) {
	default:
		__m128 ret;
		ret[0] = a[imm8 & 0x3];
		ret[1] = a[(imm8 >> 2) & 0x3];
		ret[2] = b[(imm8 >> 4) & 0x3];
		ret[3] = b[(imm8 >> 6) & 0x3];
		return ret;
	}
	#endif
}


/***************************************************************************************************/
//FPU Control 
/***************************************************************************************************/
//WORK ON ME!!!
//UNMAPTED FROM x86_64 TO ARM64
__forceinline void _mm_setcsr (unsigned int a)
{
	#if _MSC_VER
		#if defined(_M_ARM)
			#pragma message("_mm_setcsr not supported on 32-bit ARM MSVC")
		#endif
		
		#if defined(_M_ARM64)
			#pragma message("_mm_setcsr not supported on 64-bit ARM MSVC")
		#endif
	#else
		#if defined(__arm__)
			#pragma message("_mm_setcsr not supported on 32-bit ARM")
		#endif
		#if defined(__aarch64__)
			__asm__ volatile ("msr fpcr, %0" : : "ri" (a));
		#endif
	#endif
}
__forceinline unsigned int _mm_getcsr (void)
{
	#if _MSC_VER
		#if defined(_M_ARM)
			#pragma message("_mm_getcsr not supported on 32-bit ARM MSVC")
		#endif
		
		#if defined(_M_ARM64)
			#pragma message("_mm_getcsr not supported on 64-bit ARM MSVC")
		#endif
	#else
		#if defined(__arm__)
			#pragma message("_mm_getcsr not supported on 32-bit ARM")
		#endif
		#if defined(__aarch64__)
			int fpsr = 0;
			__asm__ volatile ("mrs %0, fpcr" : "=r" (fpsr));
		#endif
	#endif
}

//https://msdn.microsoft.com/en-us/library/e9b52ceh.aspx

/* x86
Bits 		Name 	Description
63 			sum 	Summary: records the bitwise OR of the FPCR exception bits (bits 57 to 52).
62-60 		raz/ign Read-As-Zero: ignored when written.
59-58 		dyn 	Dynamic Rounding Mode: indicates the current rounding mode to be used by an IEEE floating-point instruction that specifies dynamic mode ( d qualifier). The bit as follows:
00 			Chopped rounding mode.
01 			Minus 	infinity.
10 			Normal 	rounding.
11 			Plus 	infinity.
57 			iov 	Integer overflow.
56 			ine 	Inexact result.
55 			unf 	Underflow.
54 			ovf 	Overflow.
53 			dze 	Division by zero.
52 			inv 	Invalid operation.
51-0 		raz/ign Read-As-Zero: ignored when written.
*/

/* ARM64
Bits		Name	Function

[31:27]		-		Reserved, res0.

[26]		AHP		Alternative half-precision control bit:
					0
						IEEE half-precision format selected.
					1
						Alternative half-precision format selected.

[25]		DN		Default NaN mode control bit:
					0
						NaN operands propagate through to the output of a floating-point operation.
					1
						Any operation involving one or more NaNs returns the Default NaN.

[24]		FZ		Flush-to-zero mode control bit:
					0
						Flush-to-zero mode disabled. 
						Behavior of the floating-point system is fully compliant with the 
						IEEE 754 standard.
					1
						Flush-to-zero mode enabled.

[23:22]		RMode	Rounding Mode control field:
					0b00
						Round to Nearest (RN) mode.
					0b01
						Round towards Plus Infinity (RP) mode.
					0b10
						Round towards Minus Infinity (RM) mode.
					0b11
						Round towards Zero (RZ) mode.

[21:0]		-		Reserved, res0.

*/

/***************************************************************************************************/
//Cache Hierarchy 
/***************************************************************************************************/
__forceinline  void _mm_prefetch (char const* p, int i)
{
	#if _MSC_VER
		#if defined(_M_ARM)
			#pragma message("_mm_prefetch not supported on 32-bit ARM MSVC")
		#endif
		
		#if defined(_M_ARM64)
			#pragma message("_mm_prefetch not supported on 64-bit ARM MSVC")
		#endif
	#else
		#if defined(__arm__)
			#pragma message("_mm_prefetch not supported on 32-bit ARM")
		#endif
		#if defined(__aarch64__)
				if( i == 0)
				{
					//DO NOTHING!
				}
				if( i == 1)
				{
					__asm__ volatile("prfm  pldl1keep, %[Addr]\n" : : [Addr] "r" (p) : "memory");
				}
				if( i == 2)
				{
					__asm__ volatile("prfm  pldl2keep, %[Addr]\n" : : [Addr] "r" (p) : "memory");
				}
				if( i == 3)
				{
					__asm__ volatile("prfm  pldl3keep, %[Addr]\n" : : [Addr] "r" (p) : "memory");
				}
		#endif
	#endif
}


/***************************************************************************************************/
// Memory Allocation
/***************************************************************************************************/
//void* _mm_malloc (size_t size, size_t align) 
#define _mm_malloc (size, align) aligned_alloc(align, size)
//void _mm_free (void * mem_addr)
#define _mm_free (mem_addr) free(mem_addr)


/***************************************************************************************************/
// Set
/***************************************************************************************************/
// Values Set In Reverse Order In Registers To Match Intel SSE Order 
__forceinline __m128 _mm_set_ps (float e3, float e2, float e1, float e0) {
	float ALIGNED(16) data[4] = { e0, e1, e2, e3 };
	return vld1q_f32(data);
}
__forceinline __m128 _mm_set_ps1 (float a) {
	return vdupq_n_f32(a);
}
#define _mm_set1_ps _mm_set_ps1

__forceinline __m128 _mm_set_ss (float a) {
	float ALIGNED(16) data[4] = { a, 0, 0, 0 };
	return vld1q_f32(data);
}

__forceinline __m128 _mm_setr_ps (float e3, float e2, float e1, float e0) {
	float ALIGNED(16) data[4] = { e3, e2, e1, e0 };
	return vld1q_f32(data);
}
__forceinline __m128 _mm_setzero_ps (void) {
	return vdupq_n_f32(0);
}

__forceinline __m64i_i16 _mm_insert_pi16 (__m64i_i16 a, __int16 i, const int imm8)//MSVC BROKEN!!!
{
	// AVOIDS COMPILER ERROR, COMPILER OPTIMIZATION FIXES ANY INEFFICIENCY!!!
	if(imm8 == 0)
		return vset_lane_s16 (i, a, 0); 
	if(imm8 == 1)
		return vset_lane_s16 (i, a, 1); 
	if(imm8 == 2)
		return vset_lane_s16 (i, a, 2); 
	if(imm8 == 3)
		return vset_lane_s16 (i, a, 3); 
}

__forceinline __m128 _mm_undefined_ps()
{
	__m128 ret;
	return ret;
}

#define _m_pinsrw _mm_insert_pi16


/***************************************************************************************************/
// Load
/***************************************************************************************************/
__forceinline __m128 _mm_load_ps (float const* mem_addr) { 
	return vld1q_f32(mem_addr);

}
#define _mm_loadu_ps _mm_load_ps

__forceinline __m128 _mm_load_ps1 (float const* mem_addr) { 
	return vld1q_dup_f32(mem_addr);
}
#define _mm_load1_ps _mm_load_ps1

__forceinline __m128 _mm_load_ss (float const* mem_addr) { 

	float ALIGNED(16) data[4] = { vgetq_lane_f32(vld1q_f32(mem_addr), 0), 0, 0, 0 };
	return vld1q_f32(data);
}

__forceinline __m128 _mm_loadh_pi (__m128 a, __m64i_i32 const* mem_addr)
{
	return vcombine_f32 (vget_low_f32(a), vreinterpret_f32_s32(*mem_addr));
}
__forceinline __m128 _mm_loadl_pi (__m128 a, __m64i_i32 const* mem_addr)
{
	return vcombine_f32 ( vreinterpret_f32_s32(*mem_addr),vget_high_f32(a) );
}

__m128 _mm_loadr_ps (float const* mem_addr) {
	//MSVC will show as error!
	float ALIGNED(16) rmem_addr[4];

	rmem_addr[0] = *mem_addr + 3;
	rmem_addr[1] = *mem_addr + 2;
	rmem_addr[2] = *mem_addr + 1;
	rmem_addr[3] = *mem_addr + 0;

	return vld1q_f32(rmem_addr);

}

__forceinline __m128i_i16 _mm_loadu_si16 (void const* mem_addr)
{
	return vld1q_s16((__int16*)mem_addr);
}

#if ARM64
__forceinline __m128i_i64 _mm_loadu_si64 (void const* mem_addr)
{
	return vld1q_s64((__int64*)mem_addr);
}
#endif

#define _mm_stream_pi(mem_addr, a) *a = *mem_addr

__forceinline void _mm_stream_ps (float* mem_addr, __m128 a)
{
	vst1q_f32(mem_addr, a);
}

/***************************************************************************************************/
// Store
/***************************************************************************************************/

__forceinline void _mm_store_ps(float *mem_addr, __m128 a) 
{
	vst1q_f32(mem_addr, a);

}
__forceinline void _mm_store_ps1 (float* mem_addr, __m128 a) 
{
	vst1q_f32(mem_addr, vdupq_n_f32(vgetq_lane_f32(a, 0)));
}
#define _mm_store1_ps (mem_addr, a) _mm_store_ps1(mem_addr, a)

__forceinline void _mm_store_ss (float* mem_addr, __m128 a) 
{
	vst1q_lane_f32(mem_addr, a, 0);
}

__forceinline void _mm_storeh_pi (__m64i_i32* mem_addr, __m128 a)
{
	*mem_addr = vget_high_s32 (vreinterpretq_s32_f32 (a));
}
__forceinline void _mm_storel_pi (__m64i_i32* mem_addr, __m128 a)
{
	*mem_addr = vget_low_s32 (vreinterpretq_s32_f32 (a));
}
__forceinline void _mm_storer_ps (float* mem_addr, __m128 a) {

	float ALIGNED(16) data[4] = 
	{
		vgetq_lane_f32(a, 3), 
		vgetq_lane_f32(a, 2), 
		vgetq_lane_f32(a, 1), 
		vgetq_lane_f32(a, 0) 
	};
	__m128 b = vld1q_f32(data);
	vst1q_f32(mem_addr, b);
}
__forceinline void _mm_storeu_ps(float *mem_addr, __m128 a) {
	vst1q_f32(mem_addr, a);
}

__forceinline void _mm_storeu_si16 (void* mem_addr, __m128i_i16 a)
{
	vst1q_s16((__int16*)mem_addr, a);
}

#if ARM64
__forceinline void _mm_storeu_si64 (void* mem_addr, __m128i_i64 a)
{
	vst1q_s64((__int64*)mem_addr, a);

}
#endif
/***************************************************************************************************/
// Extract
/***************************************************************************************************/
#define _mm_extract_pi16(a, imm8) vget_lane_s16(a, imm8)

__forceinline __m128 _mm_unpackhi_ps (__m128 a, __m128 b)
{
	float ALIGNED(16) data[4] = 
	{ 
		vgetq_lane_f32(a, 2), 
		vgetq_lane_f32(b, 2), 
		vgetq_lane_f32(a, 3), 
		vgetq_lane_f32(b, 3) 
	};
	return vld1q_f32(data);
}
__forceinline __m128 _mm_unpacklo_ps (__m128 a, __m128 b)
{
	float ALIGNED(16) data[4] = 
	{ 
		vgetq_lane_f32(a, 0), 
		vgetq_lane_f32(b, 0), 
		vgetq_lane_f32(a, 1), 
		vgetq_lane_f32(b, 1) 
	};
	return vld1q_f32(data);
}

#define _m_pextrw _mm_extract_pi16


/***************************************************************************************************/
// Move mask Boolean
/***************************************************************************************************/
__forceinline int _mm_movemask_pi8 (__m64i_i8 a)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_mask = vld1_s8(xr);

	uint8x8_t tmp = vshl_u8(vand_u8(vreinterpret_u8_s8(a), vdup_n_u8(0x80)), shift_mask);

	tmp = vpadd_u8(tmp,tmp);
	tmp = vpadd_u8(tmp,tmp);
	tmp = vpadd_u8(tmp,tmp);

	return (vget_lane_u8(tmp,0) & 0xFF);
}

/* NOTE
	uint8x8_t &ia = *(uint8x8_t *)&a;
	return 
	(
		((ia[0] >> 7)    )  | 
		((ia[1] >> 6) & 2)  | 
		((ia[2] >> 5) & 4)  | 
		((ia[3] >> 4) & 8)  | 
		((ia[4] >> 3) & 16) | 
		((ia[5] >> 2) & 32) | 
		((ia[6] >> 1) & 64) | 
		((ia[7] >> 0) & 128)  
	);
*/

__forceinline int _mm_movemask_ps (__m128 a)
{
	uint32_t ALIGNED(16) xr[4] = { 1, 2, 4, 8 };
	uint32x4_t shift_mask = vld1q_u32(xr);

	uint32x4_t t1 = vandq_u32(vtstq_u32(vreinterpretq_u32_f32(a), vdupq_n_u32(0x80000000)), shift_mask);
	uint32x2_t t2 = vorr_u32(vget_low_u32(t1), vget_high_u32(t1));

	return vget_lane_u32(t2, 0) | vget_lane_u32(t2, 1);
}
/* NOTE
	uint32x4_t &ia = *(uint32x4_t *)&a;
	return	((ia[0] >> 31)    ) | 
			((ia[1] >> 30) & 2) | 
			((ia[2] >> 29) & 4) | 
			((ia[3] >> 28) & 8);  
*/

#define _m_pmovmskb _mm_movemask_pi8


/***************************************************************************************************/
// Add
/***************************************************************************************************/
__forceinline __m128 _mm_add_ps(__m128 a, __m128 b) {
	return vaddq_f32(a, b);
}
#define _mm_add_ss _mm_add_ps


/***************************************************************************************************/
// Subtract
/***************************************************************************************************/
__forceinline __m128 _mm_sub_ps(__m128 a, __m128 b) {
	return vsubq_f32(a, b);
}
#define _mm_sub_ss _mm_sub_ps


/***************************************************************************************************/
// Multiply
/***************************************************************************************************/
__forceinline __m128 _mm_mul_ps(__m128 a, __m128 b) {
	return vmulq_f32(a, b);
}
#define _mm_mul_ss _mm_mul_ps


__forceinline __m64i_u16 _mm_mulhi_pu16 (__m64i_u16 a, __m64i_u16 b)
{
	return vshr_n_u16
	(
	vreinterpret_u16_s16 
						(
						vqdmulh_s16
									( 
									vreinterpret_s16_u16(a), 
									vreinterpret_s16_u16(b) 
									)
						),
	1);
}
#define _m_pmulhuw _mm_mulhi_pu16


/***************************************************************************************************/
// Divide
/***************************************************************************************************/
__forceinline __m128 _mm_div_ps(__m128 a, __m128 b ) {
	return vdivq_f32(a, b);
}
__forceinline __m128 _mm_rcp_ps(__m128 a) {
	return vrecpeq_f32(a);
}

#define _mm_div_ss _mm_div_ps
#define _mm_rcp_ss _mm_rcp_ps


/***************************************************************************************************/
// Square Root
/***************************************************************************************************/
__forceinline __m128 _mm_sqrt_ps(__m128 a) {
	return vsqrtq_f32(a);

}
__forceinline __m128 _mm_rsqrt_ps(__m128 a) {
	return vrsqrteq_f32(a);
}

#define _mm_sqrt_ss _mm_sqrt_ps
#define _mm_rsqrt_ss _mm_rsqrt_ps


/***************************************************************************************************/
// Average 
/***************************************************************************************************/
__forceinline __m64i_u8 _mm_avg_pu8 (__m64i_u8 a, __m64i_u8 b)
{
	return vshr_n_u8(vadd_u8(vadd_u8(a,b), vdup_n_u8(1) ), 1);
}
__forceinline __m64i_u16 _mm_avg_pu16 (__m64i_u16 a, __m64i_u16 b)
{
	return vshr_n_u16(vadd_u16(vadd_u16(a,b), vdup_n_u16(1) ), 1);
}

#define _m_pavgb _mm_avg_pu8
#define _m_pavgw _mm_avg_pu16


/***************************************************************************************************/
// Absolute Differences  
/***************************************************************************************************/
__forceinline __m64i_u8 _mm_sad_pu8 (__m64i_u8 a, __m64i_u8 b)
{
	uint8x8_t res = vdup_n_u8(0);
	return vset_lane_u8( vaddv_u8(vabd_u8(a, b)), res, 0);
	
}

#define _m_psadbw _mm_sad_pu8

/***************************************************************************************************/
// Bitwise
/***************************************************************************************************/
__forceinline __m128 _mm_and_ps(__m128 a, __m128 b) {
	return vreinterpretq_f32_s32(vandq_s32(vreinterpretq_s32_f32(a), vreinterpretq_s32_f32(b)));
}

__forceinline __m128 _mm_andnot_ps(__m128 a, __m128 b) {
	return vreinterpretq_f32_s32(vbicq_s32(vreinterpretq_s32_f32(b), vreinterpretq_s32_f32(a)));
}

__forceinline __m128 _mm_or_ps(__m128 a, __m128 b) {
	return vreinterpretq_f32_s32(vorrq_s32(vreinterpretq_s32_f32(a), vreinterpretq_s32_f32(b)));
}

__forceinline __m128 _mm_xor_ps(__m128 a, __m128 b) {
	return vreinterpretq_f32_s32(veorq_s32(vreinterpretq_s32_f32(a), vreinterpretq_s32_f32(b)));
}

/***************************************************************************************************/
// Maximum
/***************************************************************************************************/
__forceinline __m64i_u8 _mm_max_pu8 (__m64i_u8 a, __m64i_u8 b) {
	return vmax_u8(a, b);
}
__forceinline __m64i_i16 _mm_max_pi16 (__m64i_i16 a, __m64i_i16 b) {
	return vmax_s16(a, b);
}
__forceinline __m128 _mm_max_ps(__m128 a, __m128 b) {
	return vmaxq_f32(a, b);
}
#define _mm_max_ss _mm_max_ps

#define _m_pmaxub _mm_max_pu8
#define _m_pmaxsw _mm_max_pi16


/***************************************************************************************************/
// Minimum 
/***************************************************************************************************/
__forceinline __m64i_u8 _mm_min_pu8 (__m64i_u8 a, __m64i_u8 b) {
	return vmin_u8(a, b);
}
__forceinline __m64i_i16 _mm_min_pi16 (__m64i_i16 a, __m64i_i16 b) {
	return vmin_s16(a, b);
}
__forceinline __m128 _mm_min_ps (__m128 a, __m128 b) {
	return vminq_f32(a, b);
}
#define _mm_min_ss _mm_min_ps

#define _m_pminub _mm_min_pu8
#define _m_pminsw _mm_min_pi16


/***************************************************************************************************/
// Comparator  
/***************************************************************************************************/

//Basic
__forceinline __m128 _mm_cmpeq_ps (__m128 a, __m128 b) {
	return vreinterpretq_f32_u32(vceqq_f32(a,b));
}
#define _mm_cmpeq_ss _mm_cmpeq_ps

__forceinline __m128 _mm_cmpge_ps (__m128 a, __m128 b) {
	return vreinterpretq_f32_u32(vcgeq_f32(a,b));
}
#define _mm_cmpge_ss _mm_cmpge_ps

__forceinline __m128 _mm_cmpgt_ps (__m128 a, __m128 b) {
	return vreinterpretq_f32_u32(vcgtq_f32(a,b));
}
#define _mm_cmpgt_ss _mm_cmpgt_ps

__forceinline __m128 _mm_cmple_ps (__m128 a, __m128 b) {
	return vreinterpretq_f32_u32(vcleq_f32(a,b));
}
#define _mm_cmple_ss _mm_cmple_ps

__forceinline __m128 _mm_cmplt_ps (__m128 a, __m128 b) {
	return vreinterpretq_f32_u32(vcltq_f32(a,b));
}
#define _mm_cmplt_ss _mm_cmplt_ps


// Not Comparator
__forceinline __m128 _mm_cmpneq_ps(__m128 a, __m128 b) {
	return vreinterpretq_f32_u32( vmvnq_u32( vceqq_f32(a, b) ) );
}
#define _mm_cmpneq_ss _mm_cmpneq_ps

__forceinline __m128 _mm_cmpnge_ps (__m128 a, __m128 b) {
	return vreinterpretq_f32_u32( vmvnq_u32( vcgeq_f32(a, b) ) );
}
#define _mm_cmpnge_ss _mm_cmpnge_ps

__forceinline __m128 _mm_cmpngt_ps (__m128 a, __m128 b) {
	return vreinterpretq_f32_u32( vmvnq_u32( vcgtq_f32(a, b) ) );
}
#define _mm_cmpngt_ss _mm_cmpngt_ps

__forceinline __m128 _mm_cmpnle_ps (__m128 a, __m128 b) {
	return vreinterpretq_f32_u32( vmvnq_u32( vcleq_f32(a, b) ) );
}
#define _mm_cmpnle_ss _mm_cmpnle_ps

__forceinline __m128 _mm_cmpnlt_ps (__m128 a, __m128 b) {
	return vreinterpretq_f32_u32( vmvnq_u32( vcltq_f32(a, b) ) );
}
#define _mm_cmpnlt_ss _mm_cmpnlt_ps


// NaN
__forceinline __m128 _mm_cmpord_ps(__m128 a, __m128 b ) {
	uint32x4_t aa, bb;

	aa = vceqq_f32(a, a);
	bb = vceqq_f32(b, b);

	return vreinterpretq_f32_u32(vandq_u32(aa, bb));
}
#define _mm_cmpord_ss _mm_cmpord_ps

__forceinline __m128 _mm_cmpunord_ps (__m128 a, __m128 b) {
	uint32x4_t aa, bb;

	aa = vceqq_f32(a, a);
	bb = vceqq_f32(b, b);

	return vreinterpretq_f32_u32(vmvnq_u32(vorrq_u32(aa, bb)));
}
#define _mm_cmpunord_ss _mm_cmpunord_ps


// Return Boolean int
__forceinline int _mm_comieq_ss (__m128 a, __m128 b) {
	return vgetq_lane_u32(vceqq_f32(a, b), 0);
}
__forceinline int _mm_comige_ss (__m128 a, __m128 b) {
	return vgetq_lane_u32(vcgeq_f32(a, b), 0);
}
__forceinline int _mm_comigt_ss (__m128 a, __m128 b) {
	return vgetq_lane_u32(vcgtq_f32(a, b), 0);
}
__forceinline int _mm_comile_ss (__m128 a, __m128 b) {
	return vgetq_lane_u32(vcleq_f32(a, b), 0);
}
__forceinline int _mm_comilt_ss (__m128 a, __m128 b) {
	return vgetq_lane_u32(vcltq_f32(a, b), 0);
}
__forceinline int _mm_comineq_ss (__m128 a, __m128 b) {
	return vgetq_lane_u32(vmvnq_u32( vceqq_f32(a, b) ), 0);
}

#define _mm_ucomieq_ss _mm_comieq_ss
#define _mm_ucomige_ss _mm_comige_ss
#define _mm_ucomigt_ss _mm_comigt_ss
#define _mm_ucomile_ss _mm_comile_ss
#define _mm_ucomilt_ss _mm_comilt_ss
#define _mm_ucomineq_ss _mm_comineq_ss


/***************************************************************************************************/
// CVT
/***************************************************************************************************/
__forceinline __m64i_i8 _mm_cvtps_pi8 (__m128 a)
{
	unsigned char ALIGNED(8) data[8] = 
	{
		(__int8)vgetq_lane_f32(a,0),
		(__int8)vgetq_lane_f32(a,1),
		(__int8)vgetq_lane_f32(a,2),
		(__int8)vgetq_lane_f32(a,3),
		0,
		0,
		0,
		0
	};
	return vreinterpret_s8_u8(vld1_u8(data));//vld1_i8 will not compile
}
__forceinline __m64i_i16 _mm_cvtps_pi16 (__m128 a)
{
	return vmovn_s32(vcvtq_s32_f32(a));
}

__forceinline int _mm_cvtss_si32 (__m128 a) {
	return (int)vgetq_lane_f32(a, 0);
}

#define _mm_cvttss_si32 _mm_cvtss_si32
#define _mm_cvt_ss2si _mm_cvtss_si32
#define _mm_cvtt_ss2si _mm_cvttss_si32

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __int64 _mm_cvtss_si64 (__m128 a)
{
	return (__int64)vgetq_lane_f32( a, 0);
}
#define _mm_cvttss_si64 _mm_cvtss_si64 
#endif

__forceinline float _mm_cvtss_f32 (__m128 a) {
	return vgetq_lane_f32(a, 0);
}

__forceinline __m64i_i32 _mm_cvtt_ps2pi (__m128 a)
{
	return vcvt_s32_f32(vget_low_f32(a));
}
#define _mm_cvttps_pi32 _mm_cvtt_ps2pi
#define _mm_cvt_ps2pi _mm_cvtt_ps2pi
#define _mm_cvtps_pi32 _mm_cvtt_ps2pi

__forceinline __m128 _mm_cvtpu16_ps (__m64i_u16 a)
{
	float ALIGNED(16) data[4] = 
	{ 
	(float)vget_lane_u16(a, 0), 
	(float)vget_lane_u16(a, 1), 
	(float)vget_lane_u16(a, 2), 
	(float)vget_lane_u16(a, 3) 
	};
	return vld1q_f32(data);
}
__forceinline __m128 _mm_cvtpi16_ps (__m64i_i16 a)
{
	float ALIGNED(16) data[4] = 
	{ 
	(float)vget_lane_s16(a, 0), 
	(float)vget_lane_s16(a, 1), 
	(float)vget_lane_s16(a, 2), 
	(float)vget_lane_s16(a, 3) 
	};
	return vld1q_f32(data);
}

__forceinline __m128 _mm_cvtpi32_ps (__m128 a, __m64i_i32 b)
{
	return vcombine_f32(vcvt_f32_s32(b),vget_high_f32(a));
}

#define _mm_cvt_pi2ps _mm_cvtpi32_ps

__forceinline __m128 _mm_cvtpi32x2_ps (__m64i_i32 a, __m64i_i32 b)
{
	return vcombine_f32(vcvt_f32_s32(a),vcvt_f32_s32(b));
}
__forceinline __m128 _mm_cvtpi8_ps (__m64i_i8 a)
{
	float ALIGNED(16) data[4] = 
	{ 
	(float)vget_lane_s8(a, 0), 
	(float)vget_lane_s8(a, 1), 
	(float)vget_lane_s8(a, 2), 
	(float)vget_lane_s8(a, 3) 
	};
	return vld1q_f32(data);
}
__forceinline __m128 _mm_cvtpu8_ps (__m64i_u8 a)
{
	float ALIGNED(16) data[4] = 
	{ 
	(float)vget_lane_u8(a, 0), 
	(float)vget_lane_u8(a, 1), 
	(float)vget_lane_u8(a, 2), 
	(float)vget_lane_u8(a, 3) 
	};
	return vld1q_f32(data);
}
__forceinline __m128 _mm_cvtsi32_ss (__m128 a, int b)
{
	return vsetq_lane_f32 ((float) b, a, 0);
}

#define _mm_cvt_si2ss _mm_cvtsi32_ss

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128 _mm_cvtsi64_ss (__m128 a, __int64 b)
{
	return vsetq_lane_f32 ((float) b, a, 0);
}
#endif

__forceinline void _mm_maskmove_si64 (__m64i_i8 a, __m64i_i8 mask, char* mem_addr) 
{
	int8x8_t b = vld1_s8((int8_t*)mem_addr);
	int8x8_t res = vbsl_s8 (vreinterpret_u8_s8(mask), a, b);
	vst1_s8((int8_t*)mem_addr, res);
}

#define _m_maskmovq _mm_maskmove_si64

__forceinline __m128 _mm_movehl_ps (__m128 a, __m128 b)
{
	return vcombine_f32(vget_high_f32(b),vget_high_f32(a));
}
__forceinline __m128 _mm_movelh_ps (__m128 a, __m128 b)
{
	return vcombine_f32(vget_low_f32(a),vget_low_f32(b));
}
__forceinline __m128 _mm_move_ss (__m128 a, __m128 b)
{
	return vsetq_lane_f32 (vgetq_lane_f32(b, 0), a, 0);
}



/***************************************************************************************************/
// Memory Barrier 
/***************************************************************************************************/
__forceinline void _mm_sfence (void) {
	
	#if _MSC_VER
		#if defined(_M_ARM)
			__dmb(_ARM_BARRIER_ISHST);
		#endif
		
		#if defined(_M_ARM64)
			__dmb(_ARM64_BARRIER_ISHST);
		#endif
	#else
		#if defined(__arm__)
			#pragma message("_mm_sfence not supported on 32-bit ARM")
		#endif
		#if defined(__aarch64__)
			__asm__ volatile ("dmb ishst" ::: "memory");
		#endif
	#endif
}

#endif

