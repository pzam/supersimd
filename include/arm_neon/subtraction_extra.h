/***************************************************************************************************/
// HEADER_NAME /arm_neon/addition_extra.h
/***************************************************************************************************/

/***************************************************************************************************/
// SUBTRACT
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_sub_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	return vsubq_u8(a, b);
}
__forceinline __m128i_u16 _mm_sub_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	return vsubq_u16(a, b);
}
__forceinline __m128i_u32 _mm_sub_epu32 (__m128i_u32 a, __m128i_u32 b)
{
	return vsubq_u32(a, b);
}
#if ARM64
__forceinline __m128i_u64 _mm_sub_epu64 (__m128i_u64 a, __m128i_u64 b)
{
	return vsubq_u64(a, b);
}
#endif