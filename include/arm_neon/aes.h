/***************************************************************************************************/
// HEADER_NAME /arm_neon/aes.h
/***************************************************************************************************/

/*
__m128i _mm_aesdec_si128 (__m128i a, __m128i RoundKey)
aesdeclast
__m128i _mm_aesdeclast_si128 (__m128i a, __m128i RoundKey)
aesenc
__m128i _mm_aesenc_si128 (__m128i a, __m128i RoundKey)
aesenclast
__m128i _mm_aesenclast_si128 (__m128i a, __m128i RoundKey)
aesimc
__m128i _mm_aesimc_si128 (__m128i a)
aeskeygenassist
__m128i _mm_aeskeygenassist_si128 (__m128i a, const int imm8)
pclmulqdq
__m128i _mm_clmulepi64_si128 (__m128i a, __m128i b, const int imm8)
*/



/* NEON
uint8x16_t vaeseq_u8 (uint8x16_t data, uint8x16_t key)AES cryptography
uint8x16_t vaesdq_u8 (uint8x16_t data, uint8x16_t key)AES cryptography
uint8x16_t vaesmcq_u8 (uint8x16_t data)AES cryptography
uint8x16_t vaesimcq_u8 (uint8x16_t data)AES cryptography

*/