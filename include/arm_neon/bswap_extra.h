/***************************************************************************************************/
// HEADER_NAME /arm_neon/bswap_extra.h
/***************************************************************************************************/
#if ARM

__forceinline __m128i_i32 _mm_bswap_si128 (__m128i_i32 a)
{
	// Reverse order of all bytes in the 128-bit word.
	int8x16_t tmp = vrev64q_s8(vreinterpretq_s8_s32(a));
	return vreinterpretq_s32_s8(vcombine_s8(vget_high_s8(tmp), vget_low_s8(tmp)));
	#pragma message("\
	Use _mm_bswap_epu8, _mm_bswap_epi8, in place of _mm_bswap_si128 \
	")
}

__forceinline __m128i_i8 _mm_bswap_epi8 (__m128i_i8 a)
{
	int8x16_t tmp = vrev64q_s8(a);
	return vcombine_s8(vget_high_s8(tmp), vget_low_s8(tmp));
}
__forceinline __m128i_u8 _mm_bswap_epu8 (__m128i_u8 a)
{
	uint8x16_t tmp = vrev64q_u8(a);
	return vcombine_u8(vget_high_u8(tmp), vget_low_u8(tmp));
}


__forceinline __m128i_i16 _mm_bswap_epi16 (__m128i_i16 a)
{
	int16x8_t tmp = vrev64q_s16(a);
	return vcombine_s16(vget_high_s16(tmp), vget_low_s16(tmp));
}
__forceinline __m128i_u16 _mm_bswap_epu16 (__m128i_u16 a)
{
	uint16x8_t tmp = vrev64q_u16(a);
	return vcombine_u16(vget_high_u16(tmp), vget_low_u16(tmp));
}


__forceinline __m128i_i32 _mm_bswap_epi32 (__m128i_i32 a)
{
	int32x4_t tmp = vrev64q_s32(a);
	return vcombine_s32(vget_high_s32(tmp), vget_low_s32(tmp));
}
__forceinline __m128i_u32 _mm_bswap_epu32 (__m128i_u32 a)
{
	uint32x4_t tmp = vrev64q_u32(a);
	return vcombine_u32(vget_high_u32(tmp), vget_low_u32(tmp));
}

#if ARM64
__forceinline __m128i_i64 _mm_bswap_epi64 (__m128i_i64 a)
{
	uint8x16_t tmp = vreinterpretq_u8_s64(a);
	__uint8 ALIGNED(16) data[16] = 
	{ 
		vgetq_lane_u8(tmp,7), 
		vgetq_lane_u8(tmp,6), 
		vgetq_lane_u8(tmp,5), 
		vgetq_lane_u8(tmp,4), 
		vgetq_lane_u8(tmp,3), 
		vgetq_lane_u8(tmp,2), 
		vgetq_lane_u8(tmp,1), 
		vgetq_lane_u8(tmp,0), 
		vgetq_lane_u8(tmp,15), 
		vgetq_lane_u8(tmp,14), 
		vgetq_lane_u8(tmp,13), 
		vgetq_lane_u8(tmp,12), 
		vgetq_lane_u8(tmp,11), 
		vgetq_lane_u8(tmp,10), 
		vgetq_lane_u8(tmp,9), 
		vgetq_lane_u8(tmp,8)
	};
	return  vreinterpretq_s64_u8(vld1q_u8(data));
}

__forceinline __m128i_u64 _mm_bswap_epu64 (__m128i_u64 a)
{
	uint8x16_t tmp = vreinterpretq_u8_u64(a);
	__uint8 ALIGNED(16) data[16] = 
	{ 
		vgetq_lane_u8(tmp,7), 
		vgetq_lane_u8(tmp,6), 
		vgetq_lane_u8(tmp,5), 
		vgetq_lane_u8(tmp,4), 
		vgetq_lane_u8(tmp,3), 
		vgetq_lane_u8(tmp,2), 
		vgetq_lane_u8(tmp,1), 
		vgetq_lane_u8(tmp,0), 
		vgetq_lane_u8(tmp,15), 
		vgetq_lane_u8(tmp,14), 
		vgetq_lane_u8(tmp,13), 
		vgetq_lane_u8(tmp,12), 
		vgetq_lane_u8(tmp,11), 
		vgetq_lane_u8(tmp,10), 
		vgetq_lane_u8(tmp,9), 
		vgetq_lane_u8(tmp,8)
	};
	return  vreinterpretq_u64_u8(vld1q_u8(data));
}
#endif

#endif

