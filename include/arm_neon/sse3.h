/***************************************************************************************************/
// HEADER_NAME /arm_neon/sse3.h
/***************************************************************************************************/
/*
OTHER
void _mm_monitor (void const* p, unsigned extensions, unsigned hints) monitor
void _mm_mwait (unsigned extensions, unsigned hints) mwait

*/

#if ARM
#define __SSE3__ 1

/***************************************************************************************************/
// Add Subtract
/***************************************************************************************************/
#if ARM64
__forceinline __m128d _mm_addsub_pd (__m128d a, __m128d b)
{
	double ALIGNED(16) data[2] = { -1, 1 };
	float64x2_t sub = vld1q_f64(data);
	return vaddq_f64(a, vmulq_f64(b, sub));
}
#endif

__forceinline __m128 _mm_addsub_ps (__m128 a, __m128 b)
{
	float ALIGNED(16) data[4] = { -1, 1, -1, 1 };
	float32x4_t sub = vld1q_f32(data);
	return vaddq_f32(a, vmulq_f32(b, sub));
}

/***************************************************************************************************/
// Horizontal Add
/***************************************************************************************************/
#if ARM64
__forceinline __m128d _mm_hadd_pd (__m128d a, __m128d b)
{
	return vpaddq_f64(a, b);
}
#endif

__forceinline __m128 _mm_hadd_ps (__m128 a, __m128 b)
{
	return vpaddq_f32(a, b);
}

/***************************************************************************************************/
// Horizontal Subtract
/***************************************************************************************************/
#if ARM64
__forceinline __m128d _mm_hsub_pd (__m128d a, __m128d b)
{
	double ALIGNED(16) data[2] = { 1, -1 };
	float64x2_t sub = vld1q_f64(data);
	return vpaddq_f64(vmulq_f64(a, sub), vmulq_f64(b, sub));
}
#endif

__forceinline __m128 _mm_hsub_ps (__m128 a, __m128 b)
{
	float ALIGNED(16) data[4] = { 1, -1, 1, -1 };
	float32x4_t sub = vld1q_f32(data);
	return vpaddq_f32(vmulq_f32(a, sub), vmulq_f32(b, sub));
}

/***************************************************************************************************/
// Load
/***************************************************************************************************/
__forceinline __m128i_i32 _mm_lddqu_si128(__m128i_i32 const* mem_addr)
{
	//Default 32bit integer vector
	#pragma message("\
	Use _mm_lddqu_epu8, _mm_lddqu_epi8, \
	_mm_lddqu_epu16, _mm_lddqu_epi16, \
	_mm_lddqu_epu32, _mm_lddqu_epi32, \
	_mm_lddqu_epu64 or _mm_lddqu_epi64 \
	in place of _mm_lddqu_si128 \
	")
	return *mem_addr;
	
}
#if ARM64
__forceinline __m128d _mm_loaddup_pd (double const* mem_addr)
{
	return vdupq_n_f64(*mem_addr);
}
#endif
/***************************************************************************************************/
// Move
/***************************************************************************************************/
#if ARM64
__forceinline __m128d _mm_movedup_pd (__m128d a)
{
	double ALIGNED(16) data[2] = 
	{ 
		(double)vgetq_lane_f64(a, 0), 
		(double)vgetq_lane_f64(a, 0), 
	};
	return vld1q_f64(data);
}
#endif
__forceinline __m128 _mm_movehdup_ps (__m128 a)
{
	float ALIGNED(16) data[4] = 
	{ 
		(float)vgetq_lane_f32(a, 1), 
		(float)vgetq_lane_f32(a, 1), 
		(float)vgetq_lane_f32(a, 3), 
		(float)vgetq_lane_f32(a, 3) 
	};
	return vld1q_f32(data);
}
__forceinline __m128 _mm_moveldup_ps (__m128 a)
{
	float ALIGNED(16) data[4] = 
	{ 
		(float)vgetq_lane_f32(a, 0), 
		(float)vgetq_lane_f32(a, 0), 
		(float)vgetq_lane_f32(a, 2), 
		(float)vgetq_lane_f32(a, 2) 
	};
	return vld1q_f32(data);
}
#endif

