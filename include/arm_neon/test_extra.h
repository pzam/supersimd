/***************************************************************************************************/
// HEADER_NAME /arm_neon/test_extra.h
/***************************************************************************************************/
#if ARM

/***************************************************************************************************/
// Portable Test
/***************************************************************************************************/


/***************************************************************************************************/
// 8-bit
/***************************************************************************************************/

__forceinline int _mm_testc_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vceqq_s8(a,b), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vceqq_s8(vandq_s8(a, b),vbicq_s8(a, a)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vceqq_s8(vandq_s8(a, b),vbicq_s8(a, a)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vceqq_s8(a,b), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}

/***************************************************************************************************/
// unsigned 8-bit
/***************************************************************************************************/

__forceinline int _mm_testc_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vceqq_u8(a,b), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vceqq_u8(vandq_u8(a, b),vbicq_u8(a, a)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vceqq_u8(vandq_u8(a, b),vbicq_u8(a, a)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vceqq_u8(a,b), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}
/***************************************************************************************************/
// 16-bit
/***************************************************************************************************/

__forceinline int _mm_testc_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u16(vceqq_s16(a,b)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u16(vceqq_s16(vandq_s16(a, b),vbicq_s16(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u16(vceqq_s16(vandq_s16(a, b),vbicq_s16(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vreinterpretq_u8_u16(vceqq_s16(a,b)), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}

/***************************************************************************************************/
// unsigned 16-bit
/***************************************************************************************************/

__forceinline int _mm_testc_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u16(vceqq_u16(a,b)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u16(vceqq_u16(vandq_u16(a, b),vbicq_u16(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u16(vceqq_u16(vandq_u16(a, b),vbicq_u16(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vreinterpretq_u8_u16(vceqq_u16(a,b)), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}

/***************************************************************************************************/
// 32-bit
/***************************************************************************************************/

__forceinline int _mm_testc_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(a,b)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(vandq_s32(a, b),vbicq_s32(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(vandq_s32(a, b),vbicq_s32(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(a,b)), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}

/***************************************************************************************************/
// unsigned 32-bit
/***************************************************************************************************/

__forceinline int _mm_testc_epu32 (__m128i_u32 a, __m128i_u32 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_u32(a,b)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_epu32 (__m128i_u32 a, __m128i_u32 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_u32(vandq_u32(a, b),vbicq_u32(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_epu32 (__m128i_u32 a, __m128i_u32 b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_u32(vandq_u32(a, b),vbicq_u32(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_u32(a,b)), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}


#if ARM64
/***************************************************************************************************/
// 64-bit
/***************************************************************************************************/

__forceinline int _mm_testc_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_s64(a,b)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_s64(vandq_s64(a, b),vbicq_s64(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_s64(vandq_s64(a, b),vbicq_s64(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_s64(a,b)), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}

/***************************************************************************************************/
// unsigned 64-bit
/***************************************************************************************************/

__forceinline int _mm_testc_epu64 (__m128i_u64 a, __m128i_u64 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_u64(a,b)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_epu64 (__m128i_u64 a, __m128i_u64 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_u64(vandq_u64(a, b),vbicq_u64(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_epu64 (__m128i_u64 a, __m128i_u64 b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_u64(vandq_u64(a, b),vbicq_u64(a, a))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_u64(a,b)), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}
#endif




/***************************************************************************************************/
// AVX Backport
/***************************************************************************************************/

__forceinline int _mm_testc_ps (__m128 a, __m128 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_f32(a,b)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_ps (__m128 a, __m128 b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(vandq_s32( vreinterpretq_s32_f32(a) , vreinterpretq_s32_f32(b) ),vbicq_s32(vreinterpretq_s32_f32(a), vreinterpretq_s32_f32(a) ))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_ps (__m128 a, __m128 b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_s32(vandq_s32( vreinterpretq_s32_f32(a) , vreinterpretq_s32_f32(b) ),vbicq_s32(vreinterpretq_s32_f32(a), vreinterpretq_s32_f32(a) ))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vreinterpretq_u8_u32(vceqq_f32(a,b)), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}

#if ARM64
__forceinline int _mm_testc_pd (__m128d a, __m128d b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_f64(a,b)), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testz_pd (__m128d a, __m128d b)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_s64(vandq_s64( vreinterpretq_s64_f64(a) , vreinterpretq_s64_f64(b) ),vbicq_s64(vreinterpretq_s64_f64(a), vreinterpretq_s64_f64(a) ))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return (((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535);
}

__forceinline int _mm_testnzc_pd (__m128d a, __m128d b)
{
	int zf, cf;
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);

	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_s64(vandq_s64( vreinterpretq_s64_f64(a) , vreinterpretq_s64_f64(b) ),vbicq_s64(vreinterpretq_s64_f64(a), vreinterpretq_s64_f64(a) ))), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		zf = 1;
	else
		zf = 0;
		
	shift_vector = vld1_s8(xr);
	tmp = vandq_u8(vreinterpretq_u8_u64(vceqq_f64(a,b)), vdupq_n_u8(0x80));
	
	lo = vget_low_u8(tmp);
	hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	if(((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFFFF))==65535)
		cf = 1;
	else
		cf = 0;
		
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
}
#endif

#endif

