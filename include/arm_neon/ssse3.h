/***************************************************************************************************/
// HEADER_NAME /arm_neon/ssse3.h
/***************************************************************************************************/
#if ARM
#define __SSSE3__ 1

/***************************************************************************************************/
// Shuffle
/***************************************************************************************************/
__forceinline __m64i_i8 _mm_shuffle_pi8 (__m64i_i8 a, __m64i_i8 b)
{
	#if _MSC_VER
		__m64i_i8 ret;
		ret.n64_i8[0] = a.n64_i8[b.n64_i8[0]];
		ret.n64_i8[1] = a.n64_i8[b.n64_i8[1]];
		ret.n64_i8[2] = a.n64_i8[b.n64_i8[2]];
		ret.n64_i8[3] = a.n64_i8[b.n64_i8[3]];
		ret.n64_i8[4] = a.n64_i8[b.n64_i8[4]];
		ret.n64_i8[5] = a.n64_i8[b.n64_i8[5]];
		ret.n64_i8[6] = a.n64_i8[b.n64_i8[6]];
		ret.n64_i8[7] = a.n64_i8[b.n64_i8[7]];
		return ret;
	#else
		__m64i_i8 ret;
		ret[0] = a[b[0]];
		ret[1] = a[b[1]];
		ret[2] = a[b[2]];
		ret[3] = a[b[3]];
		ret[4] = a[b[4]];
		ret[5] = a[b[5]];
		ret[6] = a[b[6]];
		ret[7] = a[b[7]];
		return ret;
	#endif
}
__forceinline __m128i_i8 _mm_shuffle_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	#if _MSC_VER
		__m128i_i8 ret;
		ret.n128_i8[0] = a.n128_i8[b.n128_i8[0]];
		ret.n128_i8[1] = a.n128_i8[b.n128_i8[1]];
		ret.n128_i8[2] = a.n128_i8[b.n128_i8[2]];
		ret.n128_i8[3] = a.n128_i8[b.n128_i8[3]];
		ret.n128_i8[4] = a.n128_i8[b.n128_i8[4]];
		ret.n128_i8[5] = a.n128_i8[b.n128_i8[5]];
		ret.n128_i8[6] = a.n128_i8[b.n128_i8[6]];
		ret.n128_i8[7] = a.n128_i8[b.n128_i8[7]];
		ret.n128_i8[8] = a.n128_i8[b.n128_i8[8]];
		ret.n128_i8[9] = a.n128_i8[b.n128_i8[9]];
		ret.n128_i8[10] = a.n128_i8[b.n128_i8[10]];
		ret.n128_i8[11] = a.n128_i8[b.n128_i8[11]];
		ret.n128_i8[12] = a.n128_i8[b.n128_i8[12]];
		ret.n128_i8[13] = a.n128_i8[b.n128_i8[13]];
		ret.n128_i8[14] = a.n128_i8[b.n128_i8[14]];
		ret.n128_i8[15] = a.n128_i8[b.n128_i8[15]];
		return ret;
	#else
		__m128i_i8 ret;
		ret[0] = a[b[0]];
		ret[1] = a[b[1]];
		ret[2] = a[b[2]];
		ret[3] = a[b[3]];
		ret[4] = a[b[4]];
		ret[5] = a[b[5]];
		ret[6] = a[b[6]];
		ret[7] = a[b[7]];
		ret[8] = a[b[8]];
		ret[9] = a[b[9]];
		ret[10] = a[b[10]];
		ret[11] = a[b[11]];
		ret[12] = a[b[12]];
		ret[13] = a[b[13]];
		ret[14] = a[b[14]];
		ret[15] = a[b[15]];
		return ret;
	#endif
}
/***************************************************************************************************/
// Absolute Value
/***************************************************************************************************/
__forceinline __m64i_i8 _mm_abs_pi8 (__m64i_i8 a)
{
	return vabs_s8(a);
}
__forceinline __m64i_i16 _mm_abs_pi16 (__m64i_i16 a)
{
	return vabs_s16(a);
}
__forceinline __m64i_i32 _mm_abs_pi32 (__m64i_i32 a)
{
	return vabs_s32(a);
}
__forceinline __m128i_i8 _mm_abs_epi8 (__m128i_i8 a)
{
	return vabsq_s8(a);
}
__forceinline __m128i_i16 _mm_abs_epi16 (__m128i_i16 a)
{
	return vabsq_s16(a);
}
__forceinline __m128i_i32 _mm_abs_epi32 (__m128i_i32 a)
{
	return vabsq_s32(a);
}


/***************************************************************************************************/
// Horizontal Add
/***************************************************************************************************/
__forceinline __m64i_i16 _mm_hadd_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	return vpadd_s16(a, b);
}
#define _mm_hadds_pi16 _mm_hadd_pi16

__forceinline __m64i_i32 _mm_hadd_pi32 (__m64i_i32 a, __m64i_i32 b)
{
	return vpadd_s32(a, b);
}
__forceinline __m128i_i16 _mm_hadd_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vpaddq_s16(a, b);
}
#define _mm_hadds_epi16 _mm_hadd_epi16

__forceinline __m128i_i32 _mm_hadd_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vpaddq_s32(a, b);
}

/***************************************************************************************************/
// Horizontal Subtraction
/***************************************************************************************************/
__forceinline __m64i_i16 _mm_hsub_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	__int16 ALIGNED(8) data[4] = { 1, -1, 1, -1 };
	int16x4_t sub = vld1_s16(data);
	return vpadd_s16(vmul_s16(a, sub), vmul_s16(b, sub));
}
#define _mm_hsubs_pi16 _mm_hsub_pi16

__forceinline __m64i_i32 _mm_hsub_pi32 (__m64i_i32 a, __m64i_i32 b)
{
	__int32 ALIGNED(8) data[2] = { 1, -1 };
	int32x2_t sub = vld1_s32(data);
	return vpadd_s32(vmul_s32(a, sub), vmul_s32(b, sub));
}

__forceinline __m128i_i16 _mm_hsub_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	__int16 ALIGNED(16) data[8] = { 1, -1, 1, -1, 1, -1, 1, -1 };
	int16x8_t sub = vld1q_s16(data);
	return vpaddq_s16(vmulq_s16(a, sub), vmulq_s16(b, sub));
}
#define _mm_hsubs_epi16 _mm_hsub_epi16

__forceinline __m128i_i32 _mm_hsub_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	__int32 ALIGNED(16) data[4] = { 1, -1, 1, -1 };
	int32x4_t sub = vld1q_s32(data);
	return vpaddq_s32(vmulq_s32(a, sub), vmulq_s32(b, sub));
}

/***************************************************************************************************/
// Multiplication 
/***************************************************************************************************/

__forceinline __m64i_i16 _mm_maddubs_pi16 (__m64i_i8 a, __m64i_i8 b)
{
	return vpaddl_s8 (vmul_s8(a,b));
}
__forceinline __m128i_i16 _mm_maddubs_epi16 (__m128i_i8 a, __m128i_i8 b)
{
	return vpaddlq_s8 (vmulq_s8(a,b));
}

__forceinline __m64i_i16 _mm_mulhrs_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	return  vmovn_s32((vshlq_s32(vmulq_s32(vmovl_s16(a), vmovl_s16(b)), vnegq_s32(vdupq_n_s32(14)))));
}
__forceinline __m128i_i16 _mm_mulhrs_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	int16x4_t hi = vmovn_s32((vshlq_s32(vmulq_s32 (vmovl_s16 (vget_high_s16(a)), vmovl_s16 (vget_high_s16(b))), vnegq_s32(vdupq_n_s32(14)))));
	int16x4_t lo = vmovn_s32((vshlq_s32(vmulq_s32 (vmovl_s16 (vget_low_s16(a)), vmovl_s16 (vget_low_s16(b))), vnegq_s32(vdupq_n_s32(14)))));
	
	return vcombine_s16(lo, hi);
}

/***************************************************************************************************/
// Sign
/***************************************************************************************************/
__forceinline __m64i_i8 _mm_sign_pi8 (__m64i_i8 a, __m64i_i8 b)
{
	int8x8_t lt = vbsl_s8 (vclt_s8(b,vdup_n_s8(0)), vneg_s8(a), a);
	return vbsl_s8 (vceq_s8(b,vdup_n_s8(0)), vdup_n_s8(0), lt);
}
__forceinline __m64i_i16 _mm_sign_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	int16x4_t lt = vbsl_s16 (vclt_s16(b,vdup_n_s16(0)), vneg_s16(a), a);
	return vbsl_s16 (vceq_s16(b,vdup_n_s16(0)), vdup_n_s16(0), lt);
}
__forceinline __m64i_i32 _mm_sign_pi32 (__m64i_i32 a, __m64i_i32 b)
{
	int32x2_t lt = vbsl_s32 (vclt_s32(b,vdup_n_s32(0)), vneg_s32(a), a);
	return vbsl_s32 (vceq_s32(b,vdup_n_s32(0)), vdup_n_s32(0), lt);
}
__forceinline __m128i_i8 _mm_sign_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	int8x16_t lt = vbslq_s8 (vcltq_s8(b,vdupq_n_s8(0)), vnegq_s8(a), a);
	return vbslq_s8 (vceqq_s8(b,vdupq_n_s8(0)), vdupq_n_s8(0), lt);
}
__forceinline __m128i_i16 _mm_sign_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	int16x8_t lt = vbslq_s16 (vcltq_s16(b,vdupq_n_s16(0)), vnegq_s16(a), a);
	return vbslq_s16 (vceqq_s16(b,vdupq_n_s16(0)), vdupq_n_s16(0), lt);
}
__forceinline __m128i_i32 _mm_sign_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	int32x4_t lt = vbslq_s32 (vcltq_s32(b,vdupq_n_s32(0)), vnegq_s32(a) , a);
	return vbslq_s32 (vceqq_s32(b,vdupq_n_s32(0)), vdupq_n_s32(0), lt);
}

/***************************************************************************************************/
// Align Right
/***************************************************************************************************/

__forceinline __m64i_i8 _mm_alignr_pi8 (__m64i_i8 a, __m64i_i8 b, int count)
{
	if(count < 0)
	{
		return vdup_n_s8(0);
	}
	if(count == 0)
	{
		return b;
	}
	if(count == 1)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (b, 1) ,
			vget_lane_s8 (b, 2) ,
			vget_lane_s8 (b, 3) ,
			vget_lane_s8 (b, 4) ,
			vget_lane_s8 (b, 5) ,
			vget_lane_s8 (b, 6) ,
			vget_lane_s8 (b, 7) ,
			
			vget_lane_s8 (a, 0) 
		};
		
		return  vld1_s8(data);
	}
	if(count == 2)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (b, 2) ,
			vget_lane_s8 (b, 3) ,
			vget_lane_s8 (b, 4) ,
			vget_lane_s8 (b, 5) ,
			vget_lane_s8 (b, 6) ,
			vget_lane_s8 (b, 7) ,
			
			vget_lane_s8 (a, 0) ,
			vget_lane_s8 (a, 1) 
		};
		
		return  vld1_s8(data);
	}
	if(count == 3)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (b, 3) ,
			vget_lane_s8 (b, 4) ,
			vget_lane_s8 (b, 5) ,
			vget_lane_s8 (b, 6) ,
			vget_lane_s8 (b, 7) ,
			
			vget_lane_s8 (a, 0) ,
			vget_lane_s8 (a, 1) ,
			vget_lane_s8 (a, 2) 
		};
		
		return  vld1_s8(data);
	}
	if(count == 4)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (b, 4) ,
			vget_lane_s8 (b, 5) ,
			vget_lane_s8 (b, 6) ,
			vget_lane_s8 (b, 7) ,
			
			vget_lane_s8 (a, 0) ,
			vget_lane_s8 (a, 1) ,
			vget_lane_s8 (a, 2) ,
			vget_lane_s8 (a, 3) 
		};
		
		return  vld1_s8(data);
	}
	if(count == 5)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (b, 5) ,
			vget_lane_s8 (b, 6) ,
			vget_lane_s8 (b, 7) ,
			
			vget_lane_s8 (a, 0) ,
			vget_lane_s8 (a, 1) ,
			vget_lane_s8 (a, 2) ,
			vget_lane_s8 (a, 3) ,
			vget_lane_s8 (a, 4) 
		};
		
		return  vld1_s8(data);
	}
	if(count == 6)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (b, 6) ,
			vget_lane_s8 (b, 7) ,
			
			vget_lane_s8 (a, 0) ,
			vget_lane_s8 (a, 1) ,
			vget_lane_s8 (a, 2) ,
			vget_lane_s8 (a, 3) ,
			vget_lane_s8 (a, 4) ,
			vget_lane_s8 (a, 5) 
		};
		
		return  vld1_s8(data);
	}
	if(count == 7)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (b, 7) ,
			
			vget_lane_s8 (a, 0) ,
			vget_lane_s8 (a, 1) ,
			vget_lane_s8 (a, 2) ,
			vget_lane_s8 (a, 3) ,
			vget_lane_s8 (a, 4) ,
			vget_lane_s8 (a, 5) ,
			vget_lane_s8 (a, 6) 
		};
		
		return  vld1_s8(data);
	}
	if(count == 8)
	{
		return a;
	}
	if(count == 9)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (a, 1) ,
			vget_lane_s8 (a, 2) ,
			vget_lane_s8 (a, 3) ,
			vget_lane_s8 (a, 4) ,
			vget_lane_s8 (a, 5) ,
			vget_lane_s8 (a, 6) ,
			vget_lane_s8 (a, 7) ,
			0
		};
		
		return  vld1_s8(data);
	}
	if(count == 10)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (a, 2) ,
			vget_lane_s8 (a, 3) ,
			vget_lane_s8 (a, 4) ,
			vget_lane_s8 (a, 5) ,
			vget_lane_s8 (a, 6) ,
			vget_lane_s8 (a, 7) ,
			0,
			0
		};
		
		return  vld1_s8(data);
	}
	if(count == 11)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif 
		{ 
			vget_lane_s8 (a, 3) ,
			vget_lane_s8 (a, 4) ,
			vget_lane_s8 (a, 5) ,
			vget_lane_s8 (a, 6) ,
			vget_lane_s8 (a, 7) ,
			0,
			0,
			0
		};
		
		return  vld1_s8(data);
	}
	if(count == 12)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (a, 4) ,
			vget_lane_s8 (a, 5) ,
			vget_lane_s8 (a, 6) ,
			vget_lane_s8 (a, 7) ,
			0,
			0,
			0,
			0
		};
		
		return  vld1_s8(data);
	}
	if(count == 13)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (a, 5) ,
			vget_lane_s8 (a, 6) ,
			vget_lane_s8 (a, 7) ,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1_s8(data);
	}
	if(count == 14)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (a, 6) ,
			vget_lane_s8 (a, 7) ,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1_s8(data);
	}
	if(count == 15)
	{
#if _MSC_VER
		int8_t ALIGNED(8) data[8] =
#else
		__int8 ALIGNED(8) data[8] =
#endif
		{ 
			vget_lane_s8 (a, 7) ,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1_s8(data);
	}
	if(count >= 16)
	{
		return vdup_n_s8(0);
	}
}

__forceinline __m128i_i8 _mm_alignr_epi8 (__m128i_i8 a, __m128i_i8 b, int count)
{
	if(count < 0)
	{
		return vdupq_n_s8(0);
	}
	if(count == 0)
	{
		return b;
	}
	if(count == 1)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 1) ,
			vgetq_lane_s8 (b, 2) ,
			vgetq_lane_s8 (b, 3) ,
			vgetq_lane_s8 (b, 4) ,
			vgetq_lane_s8 (b, 5) ,
			vgetq_lane_s8 (b, 6) ,
			vgetq_lane_s8 (b, 7) ,
			vgetq_lane_s8 (b, 8) ,
			vgetq_lane_s8 (b, 9) ,
			vgetq_lane_s8 (b, 10) ,
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 2)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 2) ,
			vgetq_lane_s8 (b, 3) ,
			vgetq_lane_s8 (b, 4) ,
			vgetq_lane_s8 (b, 5) ,
			vgetq_lane_s8 (b, 6) ,
			vgetq_lane_s8 (b, 7) ,
			vgetq_lane_s8 (b, 8) ,
			vgetq_lane_s8 (b, 9) ,
			vgetq_lane_s8 (b, 10) ,
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 3)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 3) ,
			vgetq_lane_s8 (b, 4) ,
			vgetq_lane_s8 (b, 5) ,
			vgetq_lane_s8 (b, 6) ,
			vgetq_lane_s8 (b, 7) ,
			vgetq_lane_s8 (b, 8) ,
			vgetq_lane_s8 (b, 9) ,
			vgetq_lane_s8 (b, 10) ,
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 4)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 4) ,
			vgetq_lane_s8 (b, 5) ,
			vgetq_lane_s8 (b, 6) ,
			vgetq_lane_s8 (b, 7) ,
			vgetq_lane_s8 (b, 8) ,
			vgetq_lane_s8 (b, 9) ,
			vgetq_lane_s8 (b, 10) ,
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 5)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 5) ,
			vgetq_lane_s8 (b, 6) ,
			vgetq_lane_s8 (b, 7) ,
			vgetq_lane_s8 (b, 8) ,
			vgetq_lane_s8 (b, 9) ,
			vgetq_lane_s8 (b, 10) ,
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 6)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 6) ,
			vgetq_lane_s8 (b, 7) ,
			vgetq_lane_s8 (b, 8) ,
			vgetq_lane_s8 (b, 9) ,
			vgetq_lane_s8 (b, 10) ,
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) 
		};
		
		return  vld1q_s8(data);
	}
	
	if(count == 7)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 7) ,
			vgetq_lane_s8 (b, 8) ,
			vgetq_lane_s8 (b, 9) ,
			vgetq_lane_s8 (b, 10) ,
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 8)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif 
		{ 
			vgetq_lane_s8 (b, 8) ,
			vgetq_lane_s8 (b, 9) ,
			vgetq_lane_s8 (b, 10) ,
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 9)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 9) ,
			vgetq_lane_s8 (b, 10) ,
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 10)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 10) ,
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 11)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 11) ,
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 12)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 12) ,
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 13)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 13) ,
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 14)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 14) ,
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
		};
		
		return  vld1q_s8(data);
	}
	if(count == 15)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (b, 15) ,
			
			vgetq_lane_s8 (a, 0) ,
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) 
		};
		
		return  vld1q_s8(data);
	}
	if(count == 16)
	{
		return a;
	}
	if(count == 17)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 1) ,
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 18)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 2) ,
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 19)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 3) ,
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 20)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 4) ,
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 21)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 5) ,
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 22)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 6) ,
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 23)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 7) ,
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 24)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 8) ,
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 25)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 9) ,
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 26)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 10) ,
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 27)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 11) ,
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 28)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 12) ,
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 29)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 13) ,
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 30)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 14) ,
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count == 31)
	{
#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
#else
		__int8 ALIGNED(16) data[16] =
#endif
		{ 
			vgetq_lane_s8 (a, 15) ,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		
		return  vld1q_s8(data);
	}
	if(count >= 32)
	{
		return vdupq_n_s8(0);
	}
}
#endif

