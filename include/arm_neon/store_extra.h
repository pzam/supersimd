/***************************************************************************************************/
// HEADER_NAME /arm_neon/store_extra.h
/***************************************************************************************************/
#if ARM

/***************************************************************************************************/
// Portable Store
/***************************************************************************************************/
__forceinline void _mm_store_epu8(__m128i_u8* mem_addr, __m128i_u8 a)
{
	vst1q_u8((__uint8*)(mem_addr),(a));
}
__forceinline void _mm_store_epi8(__m128i_i8* mem_addr, __m128i_i8 a)
{
	vst1q_s8((__int8*)(mem_addr),(a));
}
__forceinline void _mm_store_epu16(__m128i_u16* mem_addr, __m128i_u16 a)
{
	vst1q_u16((__uint16*)(mem_addr),(a));
}
__forceinline void _mm_store_epi16(__m128i_i16* mem_addr, __m128i_i16 a)
{
	vst1q_s16((__int16*)(mem_addr),(a));
}
__forceinline void _mm_store_epu32(__m128i_u32* mem_addr, __m128i_u32 a)
{
	vst1q_u32((__uint32*)(mem_addr),(a));
}
__forceinline void _mm_store_epi32(__m128i_i32* mem_addr, __m128i_i32 a)
{
	vst1q_s32((__int32*)(mem_addr),(a));
}
#if ARM64
__forceinline void _mm_store_epu64(__m128i_u64* mem_addr, __m128i_u64 a)
{
	vst1q_u64((__uint64*)(mem_addr),(a));
}
__forceinline void _mm_store_epi64(__m128i_i64* mem_addr, __m128i_i64 a)
{
	vst1q_s64((__int64*)(mem_addr),(a));
}
#endif

/***************************************************************************************************/
// Portable Storeu
/***************************************************************************************************/
#define _mm_storeu_epu8 _mm_store_epu8
#define _mm_storeu_epi8 _mm_store_epi8
#define _mm_storeu_epu16 _mm_store_epu16
#define _mm_storeu_epi16 _mm_store_epi16
#define _mm_storeu_epu32 _mm_store_epu32
#define _mm_storeu_epi32 _mm_store_epi32
#define _mm_storeu_epu64 _mm_store_epu64
#define _mm_storeu_epi64 _mm_store_epi64

/***************************************************************************************************/
// Portable Storeu Void*
/***************************************************************************************************/
__forceinline void _mm_storeu_si8 (void* mem_addr, __m128i_i8 a)
{
	vst1q_s8((__int8*)mem_addr, a);
}
__forceinline void _mm_storeu_su8 (void* mem_addr, __m128i_u8 a)
{
	vst1q_u8((__uint8*)mem_addr, a);
}

__forceinline void _mm_storeu_su16 (void* mem_addr, __m128i_u16 a)
{
	vst1q_u16((__uint16*)mem_addr, a);
}
__forceinline void _mm_storeu_su32 (void* mem_addr, __m128i_u32 a)
{
	vst1q_u32((__uint32*)mem_addr, a);
}
#if ARM64
__forceinline void _mm_storeu_su64 (void* mem_addr, __m128i_u64 a)
{
	vst1q_u64((__uint64*)mem_addr, a);

}
#endif

#endif

