/***************************************************************************************************/
// HEADER_NAME /arm_neon/arithmetic_extra.h
/***************************************************************************************************/


/***************************************************************************************************/
// MULTIPLICATION
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_mul_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	return vmulq_u8(a, b);
}
__forceinline __m128i_i8 _mm_mul_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return vmulq_s8(a, b);
}

__forceinline __m128i_u16 _mm_mul_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	return vmulq_u16(a, b);
}
__forceinline __m128i_i16 _mm_mul_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vmulq_s16(a, b);
}
//__m128i_u32 _mm_mul_epu32 (__m128i_u32 a, __m128i_u32 b) //IMPLEMENTED BY SSE2

#if ARM64
__forceinline __m128i_u64 _mm_mul_epu64 (__m128i_u64 a, __m128i_u64 b)
{
	__uint64 ALIGNED(16) data[2] = { 
		(__uint64)vgetq_lane_u64 (a, 0) * (__uint64)vgetq_lane_u64 (b, 0),
		(__uint64)vgetq_lane_u64 (a, 1) * (__uint64)vgetq_lane_u64 (b, 1),
	};
	return vld1q_u64(data);
}
// __m128i_i64 _mm_mul_epi64 (__m128i_i64 a, __m128i_i64 b) //IMPLEMENTED BY SSE4.2
#endif

/***************************************************************************************************/
// 8-bit Integers Multiplication Lo Hi ?? ARM PROT MISSING
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_mullo_epi8(__m128i_i8 a, __m128i_i8 b) {
	return vmulq_s8(a, b);
}
#endif

