#if ARM64

//ARM IMPL USE vrbitq_u8

__forceinline __m128i_u8 _mm_bitreverse_epu8(const __m128i_u8 a)
{
	return vrbitq_u8 (a);
}
#endif