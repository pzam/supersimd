/***************************************************************************************************/
// HEADER_NAME /arm_neon/bitwise_extra.h
/***************************************************************************************************/
#if ARM

#if defined(__MMX__) || defined(_M_IX86)
__forceinline __m64 _mm_not_si64 (__m64 a)//FIXED
{
	return vmvn_s16(a);
}
#endif

__forceinline __m128i _mm_not_si128 (__m128i a)//FIXED
{
	return vmvnq_s32(a);
}
#endif
#if ARM
__forceinline __m128 _mm_not_ps (__m128 a)//FIXED
{
	return vreinterpretq_f32_s32(vmvnq_s32(vreinterpretq_s32_f32(a)));
}

// __m128d _mm_not_pd (__m128d a) //Implemented by ../extra/bitwiseextra.h
#endif

#if ARM
/***************************************************************************************************/
// Portable MMX-NEON Bitwise Functions
/***************************************************************************************************/
// NOT [!]
__forceinline __m64i_u8 _mm_not_pu8 (__m64i_u8 a){
	return vmvn_u8(a);
}
__forceinline __m64i_i8 _mm_not_pi8 (__m64i_i8 a){
	return vmvn_s8(a);
}
__forceinline __m64i_u16 _mm_not_pu16 (__m64i_u16 a){
	return vmvn_u16(a);
}
__forceinline __m64i_i16 _mm_not_pi16 (__m64i_i16 a){
	return vmvn_s16(a);
}
__forceinline __m64i_u32 _mm_not_pu32 (__m64i_u32 a){
	return vmvn_u32(a);
}
__forceinline __m64i_i32 _mm_not_pi32 (__m64i_i32 a){
	return vmvn_s32(a);
}
#if ARM64
__forceinline __m64i_u64 _mm_not_pu64 (__m64i_u64 a){
	return veor_u64(a, vceq_u64(vdup_n_u64(0),vdup_n_u64(0)));
}
__forceinline __m64i_i64 _mm_not_pi64 (__m64i_i64 a, __m64i_i64 b){
	return veor_s64(a, vreinterpret_s64_u64(vceq_s64(vdup_n_s64(0),vdup_n_s64(0))));
}
#endif

// AND [&]
__forceinline __m64i_u8 _mm_and_pu8 (__m64i_u8 a, __m64i_u8 b){
	return vand_u8(a, b);
}
__forceinline __m64i_i8 _mm_and_pi8 (__m64i_i8 a, __m64i_i8 b){
	return vand_s8(a, b);
}
__forceinline __m64i_u16 _mm_and_pu16 (__m64i_u16 a, __m64i_u16 b){
	return vand_u16(a, b);
}
__forceinline __m64i_i16 _mm_and_pi16 (__m64i_i16 a, __m64i_i16 b){
	return vand_s16(a, b);
}
__forceinline __m64i_u32 _mm_and_pu32 (__m64i_u32 a, __m64i_u32 b){
	return vand_u32(a, b);
}
__forceinline __m64i_i32 _mm_and_pi32 (__m64i_i32 a, __m64i_i32 b){
	return vand_s32(a, b);
}
#if ARM64
__forceinline __m64i_u64 _mm_and_pu64 (__m64i_u64 a, __m64i_u64 b){
	return vand_u64(a, b);
}
__forceinline __m64i_i64 _mm_and_pi64 (__m64i_i64 a, __m64i_i64 b){
	return vand_s64(a, b);
}
#endif

// ANDNOT [~a & b]
__forceinline __m64i_u8 _mm_andnot_pu8 (__m64i_u8 a, __m64i_u8 b){
	return vbic_u8(b, a);
}
__forceinline __m64i_i8 _mm_andnot_pi8 (__m64i_i8 a, __m64i_i8 b){
	return vbic_s8(b, a);
}
__forceinline __m64i_u16 _mm_andnot_pu16 (__m64i_u16 a, __m64i_u16 b){
	return vbic_u16(b, a);
}
__forceinline __m64i_i16 _mm_andnot_pi16 (__m64i_i16 a, __m64i_i16 b){
	return vbic_s16(b, a);
}
__forceinline __m64i_u32 _mm_andnot_pu32 (__m64i_u32 a, __m64i_u32 b){
	return vbic_u32(b, a);
}
__forceinline __m64i_i32 _mm_andnot_pi32 (__m64i_i32 a, __m64i_i32 b){
	return vbic_s32(b, a);
}
#if ARM64
__forceinline __m64i_u64 _mm_andnot_pu64 (__m64i_u64 a, __m64i_u64 b){
	return vbic_u64(b, a);
}
__forceinline __m64i_i64 _mm_andnot_pi64 (__m64i_i64 a, __m64i_i64 b){
	return vbic_s64(b, a);
}
#endif

// OR [|]
__forceinline __m64i_u8 _mm_or_pu8 (__m64i_u8 a, __m64i_u8 b){
	return vorr_u8(a, b);
}
__forceinline __m64i_i8 _mm_or_pi8 (__m64i_i8 a, __m64i_i8 b){
	return vorr_s8(a, b);
}
__forceinline __m64i_u16 _mm_or_pu16 (__m64i_u16 a, __m64i_u16 b){
	return vorr_u16(a, b);
}
__forceinline __m64i_i16 _mm_or_pi16 (__m64i_i16 a, __m64i_i16 b){
	return vorr_s16(a, b);
}
__forceinline __m64i_u32 _mm_or_pu32 (__m64i_u32 a, __m64i_u32 b){
	return vorr_u32(a, b);
}
__forceinline __m64i_i32 _mm_or_pi32 (__m64i_i32 a, __m64i_i32 b){
	return vorr_s32(a, b);
}
#if ARM64
__forceinline __m64i_u64 _mm_or_pu64 (__m64i_u64 a, __m64i_u64 b){
	return vorr_u64(a, b);
}
__forceinline __m64i_i64 _mm_or_pi64 (__m64i_i64 a, __m64i_i64 b){
	return vorr_s64(a, b);
}
#endif

// XOR [^]
__forceinline __m64i_u8 _mm_xor_pu8 (__m64i_u8 a, __m64i_u8 b){
	return veor_u8(a, b);
}
__forceinline __m64i_i8 _mm_xor_pi8 (__m64i_i8 a, __m64i_i8 b){
	return veor_s8(a, b);
}
__forceinline __m64i_u16 _mm_xor_pu16 (__m64i_u16 a, __m64i_u16 b){
	return veor_u16(a, b);
}
__forceinline __m64i_i16 _mm_xor_pi16 (__m64i_i16 a, __m64i_i16 b){
	return veor_s16(a, b);
}
__forceinline __m64i_u32 _mm_xor_pu32 (__m64i_u32 a, __m64i_u32 b){
	return veor_u32(a, b);
}
__forceinline __m64i_i32 _mm_xor_pi32 (__m64i_i32 a, __m64i_i32 b){
	return veor_s32(a, b);
}
#if ARM64
__forceinline __m64i_u64 _mm_xor_pu64 (__m64i_u64 a, __m64i_u64 b){
	return veor_u64(a, b);
}
__forceinline __m64i_i64 _mm_xor_pi64 (__m64i_i64 a, __m64i_i64 b){
	return veor_s64(a, b);
}
#endif

/***************************************************************************************************/
// Portable SSE-NEON Bitwise Functions
/***************************************************************************************************/
// NOT [!]
__forceinline __m128i_u8 _mm_not_epu8 (__m128i_u8 a){
	return vmvnq_u8(a);
}
__forceinline __m128i_i8 _mm_not_epi8 (__m128i_i8 a){
	return vmvnq_s8(a);
}
__forceinline __m128i_u16 _mm_not_epu16 (__m128i_u16 a){
	return vmvnq_u16(a);
}
__forceinline __m128i_i16 _mm_not_epi16 (__m128i_i16 a){
	return vmvnq_s16(a);
}
__forceinline __m128i_u32 _mm_not_epu32 (__m128i_u32 a){
	return vmvnq_u32(a);
}
__forceinline __m128i_i32 _mm_not_epi32 (__m128i_i32 a){
	return vmvnq_s32(a);
}
#if ARM64
__forceinline __m128i_u64 _mm_not_epu64 (__m128i_u64 a){
	return veorq_u64(a, vceqq_u64( vdupq_n_u64(0),vdupq_n_u64(0) ) );
}
__forceinline __m128i_i64 _mm_not_epi64 (__m128i_i64 a, __m128i_i64 b){
	return veorq_s64(a, vreinterpretq_s64_u64(vceqq_s64(vdupq_n_s64(0),vdupq_n_s64(0))));
}
#endif

// AND [&]
__forceinline __m128i_u8 _mm_and_epu8 (__m128i_u8 a, __m128i_u8 b){
	return vandq_u8(a, b);
}
__forceinline __m128i_i8 _mm_and_epi8 (__m128i_i8 a, __m128i_i8 b){
	return vandq_s8(a, b);
}
__forceinline __m128i_u16 _mm_and_epu16 (__m128i_u16 a, __m128i_u16 b){
	return vandq_u16(a, b);
}
__forceinline __m128i_i16 _mm_and_epi16 (__m128i_i16 a, __m128i_i16 b){
	return vandq_s16(a, b);
}
__forceinline __m128i_u32 _mm_and_epu32 (__m128i_u32 a, __m128i_u32 b){
	return vandq_u32(a, b);
}
__forceinline __m128i_i32 _mm_and_epi32 (__m128i_i32 a, __m128i_i32 b){
	return vandq_s32(a, b);
}
#if ARM64
__forceinline __m128i_u64 _mm_and_epu64 (__m128i_u64 a, __m128i_u64 b){
	return vandq_u64(a, b);
}
__forceinline __m128i_i64 _mm_and_epi64 (__m128i_i64 a, __m128i_i64 b){
	return vandq_s64(a, b);
}
#endif

// ANDNOT [~a & b]
__forceinline __m128i_u8 _mm_andnot_epu8 (__m128i_u8 a, __m128i_u8 b){
	return vbicq_u8(b, a);
}
__forceinline __m128i_i8 _mm_andnot_epi8 (__m128i_i8 a, __m128i_i8 b){
	return vbicq_s8(b, a);
}
__forceinline __m128i_u16 _mm_andnot_epu16 (__m128i_u16 a, __m128i_u16 b){
	return vbicq_u16(b, a);
}
__forceinline __m128i_i16 _mm_andnot_epi16 (__m128i_i16 a, __m128i_i16 b){
	return vbicq_s16(b, a);
}
__forceinline __m128i_u32 _mm_andnot_epu32 (__m128i_u32 a, __m128i_u32 b){
	return vbicq_u32(b, a);
}
__forceinline __m128i_i32 _mm_andnot_epi32 (__m128i_i32 a, __m128i_i32 b){
	return vbicq_s32(b, a);
}
#if ARM64
__forceinline __m128i_u64 _mm_andnot_epu64 (__m128i_u64 a, __m128i_u64 b){
	return vbicq_u64(b, a);
}
__forceinline __m128i_i64 _mm_andnot_epi64 (__m128i_i64 a, __m128i_i64 b){
	return vbicq_s64(b, a);
}
#endif

// OR [|]
__forceinline __m128i_u8 _mm_or_epu8 (__m128i_u8 a, __m128i_u8 b){
	return vorrq_u8(a, b);
}
__forceinline __m128i_i8 _mm_or_epi8 (__m128i_i8 a, __m128i_i8 b){
	return vorrq_s8(a, b);
}
__forceinline __m128i_u16 _mm_or_epu16 (__m128i_u16 a, __m128i_u16 b){
	return vorrq_u16(a, b);
}
__forceinline __m128i_i16 _mm_or_epi16 (__m128i_i16 a, __m128i_i16 b){
	return vorrq_s16(a, b);
}
__forceinline __m128i_u32 _mm_or_epu32 (__m128i_u32 a, __m128i_u32 b){
	return vorrq_u32(a, b);
}
__forceinline __m128i_i32 _mm_or_epi32 (__m128i_i32 a, __m128i_i32 b){
	return vorrq_s32(a, b);
}
#if ARM64
__forceinline __m128i_u64 _mm_or_epu64 (__m128i_u64 a, __m128i_u64 b){
	return vorrq_u64(a, b);
}
__forceinline __m128i_i64 _mm_or_epi64 (__m128i_i64 a, __m128i_i64 b){
	return vorrq_s64(a, b);
}
#endif

// XOR [^]
__forceinline __m128i_u8 _mm_xor_epu8 (__m128i_u8 a, __m128i_u8 b){
	return veorq_u8(a, b);
}
__forceinline __m128i_i8 _mm_xor_epi8 (__m128i_i8 a, __m128i_i8 b){
	return veorq_s8(a, b);
}
__forceinline __m128i_u16 _mm_xor_epu16 (__m128i_u16 a, __m128i_u16 b){
	return veorq_u16(a, b);
}
__forceinline __m128i_i16 _mm_xor_epi16 (__m128i_i16 a, __m128i_i16 b){
	return veorq_s16(a, b);
}
__forceinline __m128i_u32 _mm_xor_epu32 (__m128i_u32 a, __m128i_u32 b){
	return veorq_u32(a, b);
}
__forceinline __m128i_i32 _mm_xor_epi32 (__m128i_i32 a, __m128i_i32 b){
	return veorq_s32(a, b);
}
#if ARM64
__forceinline __m128i_u64 _mm_xor_epu64 (__m128i_u64 a, __m128i_u64 b){
	return veorq_u64(a, b);
}
__forceinline __m128i_i64 _mm_xor_epi64 (__m128i_i64 a, __m128i_i64 b){
	return veorq_s64(a, b);
}
#endif

/***************************************************************************************************/
// BIT SHIFTING
/***************************************************************************************************/

__forceinline __m128i_u8 _mm_slli_epu8(__m128i_u8 a, const int imm8) 
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ((imm8) > 7 || (imm8) < 0) 
	{
		return vdupq_n_u8(0);
	}
	else
	{
		return vshlq_u8(a, vdupq_n_s8(imm8));
	}
}
__forceinline __m128i_i8 _mm_slli_epi8(__m128i_i8 a, const int imm8) 

{
	if ((imm8) == 0) 
	{
		return a;
	}
	else if ((imm8) > 7 || (imm8) < 0) 
	{
		return vdupq_n_s8(0);
	}
	else
	{
		return vshlq_s8(a, vdupq_n_s8(imm8));
	}
}
//__m128i_i16 _mm_slli_epi16 (__m128i_i16 a, int imm8)//IMPLEMENTED BY SSE2
__forceinline __m128i_u16 _mm_slli_epu16 (__m128i_u16 a, int imm8)
{
	if ((imm8) == 0) 
	{
		return a;
	}
	else if ((imm8) > 15 || (imm8) < 0) 
	{
		return vdupq_n_u16(0);
	}
	else
	{
		return vshlq_u16(a, vdupq_n_s16(imm8));
	}
}

//__m128i_i32 _mm_slli_epi32 (__m128i_i32 a, int imm8)//IMPLEMENTED BY SSE2
__forceinline __m128i_u32 _mm_slli_epu32 (__m128i_u32 a, int imm8)
{
	if ((imm8) == 0) 
	{
		return a;
	}
	else if ((imm8) > 31 || (imm8) < 0) 
	{
		return vdupq_n_u32(0);
	}
	else
	{
		return vshlq_u32(a, vdupq_n_s32(imm8));
	}
}
#if ARM64
//__m128i_i64 _mm_slli_epi64 (__m128i_i64 a, int imm8)//IMPLEMENTED BY SSE2
__forceinline __m128i_u64 _mm_slli_epu64 (__m128i_u64 a, int imm8)
{
	if ((imm8) == 0) 
	{
		return a;
	}
	else if ((imm8) > 63 || (imm8) < 0) 
	{
		return vdupq_n_u64(0);
	}
	else
	{
		return vshlq_u64(a, vdupq_n_s64(imm8));
	}
}
#endif



__forceinline __m128i_u8 _mm_sll_epu8(__m128i_u8 a, __m128i_i8 count) 
{
	uint8x16_t gt7 = vcgtq_s8(count, vdupq_n_s8(7));
	uint8x16_t ret = vshlq_u8(a, count);
	return vbslq_u8 (gt7, ret, vdupq_n_u8(0));
}
__forceinline __m128i_i8 _mm_sll_epi8(__m128i_i8 a, __m128i_i8 count) 
{
	uint8x16_t gt7 = vcgtq_s8(count, vdupq_n_s8(7));
	int8x16_t ret = vshlq_s8(a, count);
	return vbslq_s8 (gt7, ret, vdupq_n_s8(0));
}

//__m128i_i16 _mm_sll_epi16 (__m128i_i16 a, __m128i_i16 b)//IMPLEMENTED BY SSE2

__forceinline __m128i_u16 _mm_sll_epu16 (__m128i_u16 a, __m128i_i16 count)
{
	uint16x8_t gt15 = vcgtq_s16(count, vdupq_n_s16(15));
	uint16x8_t ret = vshlq_u16(a, count);
	return vbslq_u16 (gt15, ret, vdupq_n_u16(0));
}
//__m128i_i32 _mm_sll_epi32 (__m128i_i32 a, __m128i_i32 b)//IMPLEMENTED BY SSE2
__forceinline __m128i_u32 _mm_sll_epu32 (__m128i_u32 a, __m128i_i32 count)
{
	uint32x4_t gt31 = vcgtq_s32(count, vdupq_n_s32(31));
	uint32x4_t ret = vshlq_u32(a, count);
	return vbslq_u32 (gt31, ret, vdupq_n_u32(0));
}
#if ARM64
//__m128i_i64 _mm_sll_epi64 (__m128i_i64 a, __m128i_i64 b)//IMPLEMENTED BY SSE2
__forceinline __m128i_u64 _mm_sll_epu64 (__m128i_u64 a, __m128i_i64 count)
{
	uint64x2_t gt63 = vcgtq_s64(count, vdupq_n_s64(63));
	uint64x2_t ret = vshlq_u64(a, count);
	return vbslq_u64 (gt63, ret, vdupq_n_u64(0));
}
#endif



__forceinline __m128i_u8 _mm_srli_epu8(__m128i_u8 a, const int imm8) 

{
	if ((imm8) == 0) 
	{
		return a;
	}
	else if ((imm8) > 7 || (imm8) < 0) 
	{
		return vdupq_n_u8(0);
	}
	else
	{
		return vshlq_u8(a, vnegq_s8(vdupq_n_s8(imm8)));
	}
}
__forceinline __m128i_i8 _mm_srli_epi8(__m128i_i8 a, const int imm8) 

{
	if ((imm8) == 0) 
	{
		return a;
	}
	else if ((imm8) > 7 || (imm8) < 0) 
	{
		return vdupq_n_s8(0);
	}
	else
	{
		return vshlq_s8(a, vnegq_s8(vdupq_n_s8(imm8)));
	}
}
//__m128i_i16 _mm_srli_epi16 (__m128i_i16 a, int imm8)//IMPLEMENTED BY SSE2
__forceinline __m128i_u16 _mm_srli_epu16 (__m128i_u16 a, int imm8)
{
	if ((imm8) == 0) 
	{
		return a;
	}
	else if ((imm8) > 15 || (imm8) < 0)
	{
		return vdupq_n_u16(0);
	}
	else
	{
		return vshlq_u16(a, vdupq_n_s16(imm8));
	}
}
//__m128i_i32 _mm_srli_epi32 (__m128i_i32 a, int imm8)//IMPLEMENTED BY SSE2
__forceinline __m128i_u32 _mm_srli_epu32 (__m128i_u32 a, int imm8)
{
	if ((imm8) == 0) 
	{
		return a;
	}
	else if ((imm8) > 31 || (imm8) < 0) 
	{
		return vdupq_n_u32(0);
	}
	else
	{
		return vshlq_u32(a, vdupq_n_s32(imm8));
	}
}
#if ARM64
//__m128i_i64 _mm_srli_epi64 (__m128i_i64 a, int imm8)//IMPLEMENTED BY SSE2
__forceinline __m128i_u64 _mm_srli_epu64 (__m128i_u64 a, int imm8)
{
	if ((imm8) == 0) 
	{
		return a;
	}
	else if ((imm8) > 63 || (imm8) < 0) 
	{
		return vdupq_n_u64(0);
	}
	else
	{
		return vshlq_u64(a, vdupq_n_s64(imm8));
	}
}
#endif



__forceinline __m128i_u8 _mm_srl_epu8(__m128i_u8 a, __m128i_i8 count) 
{
	uint8x16_t gt7 = vcgtq_s8(count, vdupq_n_s8(7));
	uint8x16_t lt0 = vcltq_s8(count, vdupq_n_s8(0));
	uint8x16_t ret = vshlq_u8(a, vnegq_s8(count));
	return vbslq_u8 (lt0, vbslq_u8 (gt7, ret, vdupq_n_u8(0)), vdupq_n_u8(0));
}
__forceinline __m128i_i8 _mm_srl_epi8(__m128i_i8 a, __m128i_i8 count) 
{
	uint8x16_t	gt7 = vcgtq_s8(count, vdupq_n_s8(7));
	uint8x16_t	lt0 = vcltq_s8(count, vdupq_n_s8(0));
	int8x16_t	ret = vshlq_s8(a, vnegq_s8(count));
	return vbslq_s8 (lt0, vbslq_s8 (gt7, ret, vdupq_n_s8(0)), vdupq_n_s8(0));
}
//__m128i_i16 _mm_srl_epi16 (__m128i_i16 a, __m128i_i16 b)//IMPLEMENTED BY SSE2
__forceinline __m128i_u16 _mm_srl_epu16 (__m128i_u16 a, __m128i_i16 count)
{
	uint16x8_t	gt15 = vcgtq_s16(count, vdupq_n_s16(15));
	uint16x8_t	lt0 = vcltq_s16(count, vdupq_n_s16(0));
	uint16x8_t	ret = vshlq_u16(a, vnegq_s16(count));
	return vbslq_u16 (lt0, vbslq_u16 (gt15, ret, vdupq_n_u16(0)), vdupq_n_u16(0));
}
//__m128i_i32 _mm_srl_epi32 (__m128i_i32 a, __m128i_i32 b)//IMPLEMENTED BY SSE2
__forceinline __m128i_u32 _mm_srl_epu32 (__m128i_u32 a, __m128i_i32 count)
{
	uint32x4_t	gt31 = vcgtq_s32(count, vdupq_n_s32(31));
	uint32x4_t	lt0 = vcltq_s32(count, vdupq_n_s32(0));
	uint32x4_t	ret = vshlq_u32(a, vnegq_s32(count));
	return vbslq_u32 (lt0, vbslq_u32 (gt31, ret, vdupq_n_u32(0)), vdupq_n_u32(0));
}
#if ARM64
//__m128i_i64 _mm_srl_epi64 (__m128i_i64 a, __m128i_i64 b)//IMPLEMENTED BY SSE2
__forceinline __m128i_u64 _mm_srl_epu64 (__m128i_u64 a, __m128i_i64 count)
{
	uint64x2_t	gt63 = vcgtq_s64(count, vdupq_n_s64(63));
	uint64x2_t	lt0 = vcltq_s64(count, vdupq_n_s64(0));
	uint64x2_t	ret = vshlq_u64(a, vnegq_s64(count));
	return vbslq_u64 (lt0, vbslq_u64 (gt63, ret, vdupq_n_u64(0)), vdupq_n_u64(0));
}
#endif



__forceinline __m128i_u8 _mm_srai_epu8(__m128i_u8 a, int imm8) 
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 7) )
	{
		return vshrq_n_u8(vshrq_n_u8(a, 4), 4);
	}
	else
	{
		return vshlq_u8(a, vnegq_s8(vdupq_n_s8(imm8))); //vshrq_n_u8(a, imm8);
	}
}
__forceinline __m128i_i8 _mm_srai_epi8(__m128i_i8 a, int imm8) 
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 7) )
	{
		return vshrq_n_s8(vshrq_n_s8(a, 4), 4);
	}
	else
	{
		return vshlq_s8(a, vnegq_s8(vdupq_n_s8(imm8))); //vshrq_n_u8(a, imm8);
	}
}
//__m128i_i16 _mm_srai_epi16 (__m128i_i16 a, int imm8) //IMPLEMENTED BY SSE2
__forceinline __m128i_u16 _mm_srai_epu16 (__m128i_u16 a, int imm8)
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 15) )
	{
		return vshrq_n_u16(vshrq_n_u16(a, 8), 8);
	}
	else
	{
		return vshlq_u16(a, vnegq_s16(vdupq_n_s16(imm8))); //vshrq_n_u16(a, imm8);
	}
}
//__m128i_i32 _mm_srai_epi32 (__m128i_i32 a, int imm8) //IMPLEMENTED BY SSE2
__forceinline __m128i_u32 _mm_srai_epu32 (__m128i_u32 a, int imm8)
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 31) )
	{
		return vshrq_n_u32(vshrq_n_u32(a, 16), 16);
	}
	else
	{
		return vshlq_u32(a, vnegq_s32(vdupq_n_s32(imm8))); //vshrq_n_u32(a, imm8);
	}
}
#if ARM64
//IMPLEMENTED BY AVX-512
__forceinline __m128i_i64 _mm_srai_epi64 (__m128i_i64 a, int imm8)
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 63) )
	{
		return vshrq_n_s64(vshrq_n_s64(a, 32), 32);
	}
	else
	{
		return vshlq_s64(a, vnegq_s64(vdupq_n_s64(imm8))); //vshrq_n_s64(a, imm8);
	}
}

__forceinline __m128i_u64 _mm_srai_epu64 (__m128i_u64 a, int imm8)
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 63) )
	{
		return vshrq_n_u64(vshrq_n_u64(a, 32), 32);
	}
	else
	{
		return vshlq_u64(a, vnegq_s64(vdupq_n_s64(imm8))); //vshrq_n_u64(a, imm8);
	}
}
#endif



__forceinline __m128i_u8 _mm_sra_epu8(__m128i_u8 a, __m128i_i8 count) {
	uint8x16_t	gt7 = vcgtq_s8(count, vdupq_n_s8(7));
	uint8x16_t	ret = vshlq_u8(a, vnegq_s8(count));
	uint8x16_t	ret2 = vshrq_n_u8(a, 4); 
				ret2 = vshrq_n_u8(ret2, 4);
	return vbslq_u8 (gt7, ret, ret2);
}

__forceinline __m128i_i8 _mm_sra_epi8(__m128i_i8 a, __m128i_i8 count) {
	uint8x16_t	gt7 = vcgtq_s8(count, vdupq_n_s8(7));
	int8x16_t	ret = vshlq_s8(a, vnegq_s8(count));
	int8x16_t	ret2 = vshrq_n_s8(a, 4); 
				ret2 = vshrq_n_s8(ret2, 4);
	return vbslq_s8 (gt7, ret, ret2);
}

// __m128i_i16 _mm_sra_epi16 (__m128i_i16 a, __m128i_i16 count) //IMPLEMENTED BY SSE2
__forceinline __m128i_u16 _mm_sra_epu16 (__m128i_u16 a, __m128i_i16 count) //(DISCREPANCY)
{
	uint16x8_t	gt15 = vcgtq_s16(count, vdupq_n_s16(15));
	uint16x8_t	ret = vshlq_u16(a, vnegq_s16(count));
	uint16x8_t	ret2 = vshrq_n_u16(a, 8); 
				ret2 = vshrq_n_u16(ret2, 8);
	return vbslq_u16 (gt15, ret, ret2);
}
//__m128i_i32 _mm_sra_epi32 (__m128i_i32 a, __m128i_i32 count) //IMPLEMENTED BY SSE2
__forceinline __m128i_u32 _mm_sra_epu32 (__m128i_u32 a, __m128i_i32 count) //(DISCREPANCY)
{
	uint32x4_t	gt31 = vcgtq_s32(count, vdupq_n_s32(31));
	uint32x4_t	ret = vshlq_u32(a, vnegq_s32(count));
	uint32x4_t	ret2 = vshrq_n_u32(a, 16); 
				ret2 = vshrq_n_u32(ret2, 16);
	return vbslq_u32 (gt31, ret, ret2);
}

#if ARM64
//IMPLEMENTED BY AVX-512
__forceinline __m128i_i64 _mm_sra_epi64 (__m128i_i64 a, __m128i_i64 count) //(DISCREPANCY)
{
	uint64x2_t	gt63 = vcgtq_s64(count, vdupq_n_s64(63));
	int64x2_t	ret = vshlq_s64(a, vnegq_s64(count));
	int64x2_t	ret2 = vshrq_n_s64(a, 32); 
				ret2 = vshrq_n_s64(ret2, 32);
	return vbslq_s64 (gt63, ret, ret2);
}

__forceinline __m128i_u64 _mm_sra_epu64 (__m128i_u64 a, __m128i_i64 count) //(DISCREPANCY)
{
	uint64x2_t	gt63 = vcgtq_s64(count, vdupq_n_s64(63));
	uint64x2_t	ret = vshlq_u64(a, vnegq_s64(count));
	uint64x2_t	ret2 = vshrq_n_u64(a, 32); 
				ret2 = vshrq_n_u64(ret2, 32);
	return vbslq_u64 (gt63, ret, ret2);
}
#endif

#endif

