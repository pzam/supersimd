/***************************************************************************************************/
// HEADER_NAME /arm_neon/sha.h
/***************************************************************************************************/

/*
sha1msg1
__m128i _mm_sha1msg1_epu32 (__m128i a, __m128i b)
sha1msg2
__m128i _mm_sha1msg2_epu32 (__m128i a, __m128i b)
sha1nexte
__m128i _mm_sha1nexte_epu32 (__m128i a, __m128i b)
sha1rnds4
__m128i _mm_sha1rnds4_epu32 (__m128i a, __m128i b, const int func)
sha256msg1
__m128i _mm_sha256msg1_epu32 (__m128i a, __m128i b)
sha256msg2
__m128i _mm_sha256msg2_epu32 (__m128i a, __m128i b)
sha256rnds2
__m128i _mm_sha256rnds2_epu32 (__m128i a, __m128i b, __m128i k)
*/


/* NEON
uint32x4_t vsha1cq_u32 (uint32x4_t hash_abcd, uint32_t hash_e, uint32x4_t wk)SHA1 cryptography
uint32x4_t vsha1pq_u32 (uint32x4_t hash_abcd, uint32_t hash_e, uint32x4_t wk)SHA1 cryptography
uint32x4_t vsha1mq_u32 (uint32x4_t hash_abcd, uint32_t hash_e, uint32x4_t wk)SHA1 cryptography
uint32_t vsha1h_u32 (uint32_t hash_e)SHA1 cryptography
uint32x4_t vsha1su0q_u32 (uint32x4_t w0_3, uint32x4_t w4_7, uint32x4_t w8_11)SHA1 cryptography
uint32x4_t vsha1su1q_u32 (uint32x4_t tw0_3, uint32x4_t w12_15)SHA1 cryptography
uint32x4_t vsha256hq_u32 (uint32x4_t hash_abcd, uint32x4_t hash_efgh, uint32x4_t wk)SHA2-256 cryptography
uint32x4_t vsha256h2q_u32 (uint32x4_t hash_efgh, uint32x4_t hash_abcd, uint32x4_t wk)SHA2-256 cryptography
uint32x4_t vsha256su0q_u32 (uint32x4_t w0_3, uint32x4_t w4_7)SHA2-256 cryptography
uint32x4_t vsha256su1q_u32 (uint32x4_t tw0_3, uint32x4_t w8_11, uint32x4_t w12_15)SHA2-256 cryptography

*/