/***************************************************************************************************/
// HEADER_NAME /arm_neon/cpu.h
/***************************************************************************************************/

#define _FEATURE_GENERIC_IA32 1 
#define _FEATURE_MMX 1 
#define _FEATURE_SSE 1 
#define _FEATURE_SSE2 1 
#define _FEATURE_SSE3 1 
#define _FEATURE_SSSE3 1 
#define _FEATURE_SSE4_1 1 
#define _FEATURE_SSE4_2 1 
#define _FEATURE_FMA 1 

#define _FEATURE_AVX 2
#define _FEATURE_AVX2 2


#define _FEATURE_KNCNI 3
#define _FEATURE_AVX512F 3
#define _FEATURE_AVX512ER 3
#define _FEATURE_AVX512PF 3
#define _FEATURE_AVX512CD 3

#define _FEATURE_SHA 4
#define _FEATURE_AES 5


#define _FEATURE_FPU 0 
#define _FEATURE_CMOV 0
#define _FEATURE_FXSAVE 0
#define _FEATURE_MOVBE0
#define _FEATURE_POPCNT 0
#define _FEATURE_PCLMULQDQ 0
#define _FEATURE_F16C 0 
#define _FEATURE_RDRND 0
#define _FEATURE_BMI 0
#define _FEATURE_LZCNT 0
#define _FEATURE_HLE 0
#define _FEATURE_RTM 0
#define _FEATURE_MPX 0
#define _FEATURE_ADX 0
#define _FEATURE_RDSEED 0



__forceinline void _allow_cpu_features (__uint64 a)
{
	//DO NOTHING
}

__forceinline int _may_i_use_cpu_feature (__uint64 a)
{
	// NEON COMPILER FLAGS WILL CHECK IF NEON EXISTS,
	// NO NEED TO CHECK IF FEATURE EXIST.
	if(a == 1) // MMX, SSE, FMA, Generic
		return 1;
	if(a == 2) // AVX
		return 0;
	if(a == 3) // AVX-512
		return 0;
	if(a == 4) // SHA
	{
		#if ARM64
		return 1;
		#endif
		#if ARM32
		return 0;
		#endif
	}
	if(a == 5) // AES
	{
		#if ARM64
		return 1;
		#endif
		#if ARM32
		return 0;
		#endif
	}
	
	return 0;
}
