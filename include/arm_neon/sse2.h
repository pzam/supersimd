/***************************************************************************************************/
// HEADER_NAME /arm_neon/sse2.h
/***************************************************************************************************/
#if ARM
/*
BROKEN _mm_shufflehi_epi16
MSVC MISSING _mm_shuffle_pd
MSVC MISSING _mm_clflush

NEED MSVC x32 AND ARM32 versions
_mm_clflush (void const* p)
_mm_pause (void)
_mm_lfence (void)
_mm_mfence (void)


OTHER
__m128 _mm_cvtph_ps (__m128i a) vcvtps2ph
__m128i _mm_cvtps_ph (__m128 a, int rounding) ...
float _cvtsh_ss (unsigned short a) ...
unsigned short _cvtss_sh (float a, int imm8) ...

*/


#define __SSE2__ 1
/***************************************************************************************************/
// Shuffle
/***************************************************************************************************/
#define _MM_SHUFFLE2(x,y) (((x)<<1) | (y))

__forceinline __m128i_i32 _mm_shuffle_epi32 (__m128i_i32 a, int imm8)
{
	#if _MSC_VER
	switch (imm8) {
	default:
		__m128i_i32 ret;
		ret.n128_i32[0] = a.n128_i32[imm8 & 0x3];
		ret.n128_i32[1] = a.n128_i32[(imm8 >> 2) & 0x3];
		ret.n128_i32[2] = a.n128_i32[(imm8 >> 4) & 0x3];
		ret.n128_i32[3] = a.n128_i32[(imm8 >> 6) & 0x3];
		return ret;
	}
	#else
	switch (imm8) {
	default:
		__m128i_i32 ret;
		ret[0] = a[imm8 & 0x3];
		ret[1] = a[(imm8 >> 2) & 0x3];
		ret[2] = a[(imm8 >> 4) & 0x3];
		ret[3] = a[(imm8 >> 6) & 0x3];
		return ret;
	}
	#endif
}
#if ARM64
__forceinline __m128d _mm_shuffle_pd (__m128d a, __m128d b, int imm8)
{
	#if _MSC_VER
	#pragma message("\
	_mm_shuffle_pd is missing on MSVC for ARM64!
	")
	#else
	switch (imm8) {
	default:
		__m128d ret;
		ret[0] = a[imm8 & 0x3];
		ret[1] = b[(imm8 >> 2) & 0x3];
		return ret;
	}
	#endif
}
#endif

__forceinline __m128i_i16 _mm_shufflelo_epi16 (__m128i_i16 a, int imm8)
{
	#if _MSC_VER
	switch (imm8) {
	default:
		__m128i_i16 ret;
		ret.n128_i16[0] = a.n128_i16[imm8 & 0x3];
		ret.n128_i16[1] = a.n128_i16[(imm8 >> 2) & 0x3];
		ret.n128_i16[2] = a.n128_i16[(imm8 >> 4) & 0x3];
		ret.n128_i16[3] = a.n128_i16[(imm8 >> 6) & 0x3];
		return vcombine_s16(vget_low_s16(ret),vget_high_s16(a));
	}
	#else
	switch (imm8) {
	default:
		__m128i_i16 ret;
		ret[0] = a[imm8 & 0x3];
		ret[1] = a[(imm8 >> 2) & 0x3];
		ret[2] = a[(imm8 >> 4) & 0x3];
		ret[3] = a[(imm8 >> 6) & 0x3];
		return vcombine_s16(vget_low_s16(ret),vget_high_s16(a));
	}
	#endif
}
__forceinline __m128i_i16 _mm_shufflehi_epi16 (__m128i_i16 a, int imm8)//BROKEN!!!
{
	#if _MSC_VER
	switch (imm8) {
	default:
		__m128i_i16 ret;
		ret.n128_i16[4] = a.n128_i16[(imm8 >> 8) & 0x3];
		ret.n128_i16[5] = a.n128_i16[(imm8 >> 10) & 0x3];
		ret.n128_i16[6] = a.n128_i16[(imm8 >> 12) & 0x3];
		ret.n128_i16[7] = a.n128_i16[(imm8 >> 14) & 0x3];
		return vcombine_s16(vget_low_s16(a),vget_high_s16(ret));
	}
	#else
	switch (imm8) {
	default:
		__m128i_i16 ret;
		ret[4] = a[(imm8 >> 8) & 0x3];
		ret[5] = a[(imm8 >> 10) & 0x3];
		ret[6] = a[(imm8 >> 12) & 0x3];
		ret[7] = a[(imm8 >> 14) & 0x3];
		return vcombine_s16(vget_low_s16(a),vget_high_s16(ret));
	}
	#endif
}

/***************************************************************************************************/
// Set
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_set_epi8
(
__int8 e15, __int8 e14, __int8 e13, __int8 e12, 
__int8 e11, __int8 e10, __int8 e9, __int8 e8, 
__int8 e7, __int8 e6, __int8 e5, __int8 e4, 
__int8 e3, __int8 e2, __int8 e1, __int8 e0
)
{
	#if _MSC_VER
	int8_t ALIGNED(16) data[16] = 
	#else
	__int8 ALIGNED(16) data[16] =
	#endif
	{ 
		e0, e1, e2, e3, 
		e4, e5, e6, e7, 
		e8, e9, e10, e11, 
		e12, e13, e14, e15 
	};
	return  vld1q_s8(data);
}
__forceinline __m128i_i8 _mm_setr_epi8
(
__int8 e15, __int8 e14, __int8 e13, __int8 e12, 
__int8 e11, __int8 e10, __int8 e9, __int8 e8, 
__int8 e7, __int8 e6, __int8 e5, __int8 e4, 
__int8 e3, __int8 e2, __int8 e1, __int8 e0
)
{
	#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
	#else
		__int8 ALIGNED(16) data[16] =
	#endif
	{ 
		e15, e14, e13, e12, 
		e11, e10, e9, e8, 
		e7, e6, e5, e4, 
		e3, e2, e1, e0 
	};
	return  vld1q_s8(data);
}
__forceinline __m128i_i8 _mm_set1_epi8 (__int8 a)
{
	return vdupq_n_s8(a);
}
__forceinline __m128i_i16 _mm_set_epi16 
(
__int16 e7, __int16 e6, __int16 e5, __int16 e4, 
__int16 e3, __int16 e2, __int16 e1, __int16 e0
)
{
	__int16 ALIGNED(16) data[8] = 
	{ 
		e0, e1, e2, e3, 
		e4, e5, e6, e7 
	};
	return  vld1q_s16(data);
}
__forceinline __m128i_i16 _mm_setr_epi16 
(
__int16 e7, __int16 e6, __int16 e5, __int16 e4, 
__int16 e3, __int16 e2, __int16 e1, __int16 e0
)
{
	__int16 ALIGNED(16) data[8] = 
	{ 
		e7, e6, e5, e4, 
		e3, e2, e1, e0 
	};
	return  vld1q_s16(data);
}
__forceinline __m128i_i16 _mm_set1_epi16 (__int16 a)
{
	return vdupq_n_s16(a);
}
__forceinline __m128i_i32 _mm_set_epi32 (int e3, int e2, int e1, int e0)
{
	__int32 ALIGNED(16) data[4] = { e0, e1, e2, e3 };
	return vld1q_s32(data);
}
__forceinline __m128i_i32 _mm_setr_epi32 (int e3, int e2, int e1, int e0)
{
	__int32 ALIGNED(16) data[4] = { e3, e2, e1, e0 };
	return vld1q_s32(data);
}
__forceinline __m128i_i32 _mm_set1_epi32 (int a)
{
	return vdupq_n_s32(a);
}

#if ARM64
__forceinline __m128i_i64 _mm_set_epi64 (__m64i_i64 e1, __m64i_i64 e0)
{
	return vcombine_s64 (e0, e1);
}
__forceinline __m128i_i64 _mm_set1_epi64 (__m64i_i64 a)
{
	return vcombine_s64 (a, a);
}
__forceinline __m128i_i64 _mm_setr_epi64 (__m64i_i64 e1, __m64i_i64 e0)
{
	return vcombine_s64 (e1, e0);
}

__forceinline __m128i_i64 _mm_set_epi64x (__int64 e1, __int64 e0)
{
	__int64 ALIGNED(16) data[2] = { e0, e1 };
	return vld1q_s64(data);
}
__forceinline __m128i_i64 _mm_set1_epi64x (__int64 a)
{
	return vdupq_n_s64(a);
}

__forceinline __m128d _mm_set_pd (double e1, double e0)
{
	double ALIGNED(16) data[2] = { e0, e1 };
	return vld1q_f64(data);
}
__forceinline __m128d _mm_setr_pd (double e1, double e0)
{
	double ALIGNED(16) data[2] = { e1, e0 };
	return vld1q_f64(data);
}
__forceinline __m128d _mm_set_pd1 (double a)
{
	return vdupq_n_f64(a);
}

__forceinline __m128d _mm_set1_pd (double a)
{
	return _mm_set_pd1 (a);
}
__forceinline __m128d _mm_set_sd (double a)
{
	double ALIGNED(16) data[2] = { a, 0 };
	return vld1q_f64(data);
}

__forceinline __m128d _mm_setzero_pd (void)
{
	//MSVC will show as error!
	return vdupq_n_f64(0);
}
#endif

__forceinline __m128i_i32 _mm_setzero_si128 ()
{
	return vdupq_n_s32(0);
	
	#pragma message("\
	Use _mm_setzero_epu8, _mm_setzero_epi8, \
	_mm_setzero_epu16, _mm_setzero_epu32, \
	_mm_setzero_epi32, _mm_setzero_epu64, \
	_mm_setzero_epi64 in place of _mm_setzero_si128 \
	")
}

#if ARM64
__forceinline __m128d _mm_undefined_pd (void)
{
	__m128d ret;
	return ret;
}
#endif

__forceinline __m128i_i32 _mm_undefined_si128 (void)
{
	__m128i_i32 ret;
	return ret;
	#pragma message("\
	Use _mm_undefined_epu8, _mm_undefined_epi8, \
	_mm_undefined_epu16, _mm_undefined_epu32, \
	_mm_undefined_epi32, _mm_undefined_epu64, \
	_mm_undefined_epi64 in place of _mm_undefined_si128 \
	")
}

__forceinline __m128i_i16 _mm_insert_epi16(__m128i_i16 a, int i, int imm8)
{
	// AVOIDS COMPILER ERROR, COMPILER OPTIMIZATION FIXES ANY INEFFICIENCY!!!
	if(imm8 == 0)
		return vsetq_lane_s16 (i, a, 0); 
	if(imm8 == 1)
		return vsetq_lane_s16 (i, a, 1); 
	if(imm8 == 2)
		return vsetq_lane_s16 (i, a, 2); 
	if(imm8 == 3)
		return vsetq_lane_s16 (i, a, 3); 
	if(imm8 == 4)
		return vsetq_lane_s16 (i, a, 4); 
	if(imm8 == 5)
		return vsetq_lane_s16 (i, a, 5); 
	if(imm8 == 6)
		return vsetq_lane_s16 (i, a, 6); 
	if(imm8 == 7)
		return vsetq_lane_s16 (i, a, 7); 
}
/***************************************************************************************************/
// Extract
/***************************************************************************************************/
#define _mm_extract_epi16(a, imm8) vgetq_lane_s16(a, imm8)

/***************************************************************************************************/
// Load
/***************************************************************************************************/
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_load_pd (double const* mem_addr)
{
	return vld1q_f64(mem_addr);//NT
}
#define _mm_loadu_pd _mm_load_pd

__forceinline __m128d _mm_loadr_pd (double const* mem_addr)
{
	double ALIGNED(16) rmem_addr[2];

	rmem_addr[0] = *mem_addr + 1;
	rmem_addr[1] = *mem_addr + 0;

	return vld1q_f64(rmem_addr);

}
__forceinline __m128d _mm_load_pd1 (double const* mem_addr){
	return vld1q_dup_f64(mem_addr);
}
#define _mm_load1_pd _mm_load_pd1

__forceinline __m128d _mm_load_sd (double const* mem_addr)
{
	double ALIGNED(16) data[2] = { vgetq_lane_f64(vld1q_f64(mem_addr), 0) ,0};
	return vld1q_f64(data);
}
#endif

__forceinline __m128i_i32 _mm_load_si128 (__m128i_i32 const* mem_addr)
{
	#pragma message("\
	Use _mm_load_epu8, _mm_load_epi8, \
	_mm_load_epu16, _mm_load_epi16, \
	_mm_load_epu32, _mm_load_epi32, \
	_mm_load_epu64 or _mm_load_epi64 \
	in place of _mm_load_si128 \
	")
	
	//MSVC will show as error!
	return *mem_addr;
}

__forceinline __m128i_i32 _mm_loadu_si128(__m128i_i32 const* mem_addr){
	//Default 32bit integer vector
	#pragma message("\
	Use _mm_loadu_epu8, _mm_loadu_epi8, \
	_mm_loadu_epu16, _mm_loadu_epi16, \
	_mm_loadu_epu32, _mm_loadu_epi32, \
	_mm_loadu_epu64 or _mm_loadu_epi64 \
	in place of _mm_loadu_si128 \
	")
	return *mem_addr;
	
}

__forceinline __m128i_i32 _mm_loadu_si32 (void const* mem_addr)
{
	return vld1q_s32((__int32*)mem_addr);
}

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_loadh_pd (__m128d a, double const* mem_addr) 
{
	return vcombine_f64 (vget_low_f64(a), vget_low_f64(vld1q_f64(mem_addr)) );
}
__forceinline __m128d _mm_loadl_pd (__m128d a, double const* mem_addr) 
{
	return vcombine_f64 (vget_low_f64(vld1q_f64(mem_addr)), vget_high_f64(a) );
}
__forceinline __m128i_i64 _mm_loadl_epi64 (__m128i_i64 const* mem_addr) 
{
	return vcombine_s64 (vget_low_s64(vld1q_s64((__int64*)mem_addr)), vget_high_s64(vdupq_n_s64(0)) );
}
#endif
/***************************************************************************************************/
// Store
/***************************************************************************************************/
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline void _mm_store_pd (double* mem_addr, __m128d a)
{
	vst1q_f64(mem_addr, a);

}
#define _mm_storeu_pd _mm_store_pd

__forceinline void _mm_store_pd1 (double* mem_addr, __m128d a)
{
	vst1q_f64(mem_addr, vdupq_n_f64(vgetq_lane_f64(a, 0)));
}
#define _mm_store1_pd _mm_store_pd1

__forceinline void _mm_store_sd (double* mem_addr, __m128d a) 
{
	vst1q_lane_f64(mem_addr, a, 0);
}
#endif

__forceinline void _mm_store_si128 (__m128i_i32* mem_addr, __m128i_i32 a)
{
	//Default 32bit integer vector
	#pragma message("\
	Use _mm_store_epu8, _mm_store_epi8, \
	_mm_store_epu16, _mm_store_epi16, \
	_mm_store_epu32, _mm_store_epi32, \
	_mm_store_epu64 or _mm_store_epi64 \
	in place of _mm_store_si128 \
	")
	vst1q_s32((__int32*)(mem_addr),(a));
}

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline void _mm_storeh_pd (double* mem_addr, __m128d a)
{
	*mem_addr = vgetq_lane_f64 (a, 1);
}

__forceinline void _mm_storel_epi64 (__m128i_i64* mem_addr, __m128i_i64 a)
{
	*mem_addr = vcombine_s64(vget_low_s64 (a), vget_high_s64 (*mem_addr) );
}

__forceinline void _mm_storel_pd (double* mem_addr, __m128d a)
{
	*mem_addr = vgetq_lane_f64 (a, 0);
}

__forceinline void _mm_storer_pd (double* mem_addr, __m128d a)
{

	double ALIGNED(16) data[2] = 
	{
		vgetq_lane_f64(a, 1), 
		vgetq_lane_f64(a, 0) 
	};
	__m128d b = vld1q_f64(data);
	vst1q_f64(mem_addr, b);
}
#endif

__forceinline void _mm_storeu_si32 (void* mem_addr, __m128i_i32 a)
{
	vst1q_s32((__int32*)(mem_addr),(a));
}

__forceinline void _mm_storeu_si128(__m128i_i32* mem_addr, __m128i_i32 a)
{
	//Default 32bit integer vector
	#pragma message("\
	Use _mm_storeu_epu8, _mm_storeu_epi8, \
	_mm_storeu_epu16, _mm_storeu_epi16, \
	_mm_storeu_epu32, _mm_storeu_epi32, \
	_mm_storeu_epu64 or _mm_storeu_epi64 \
	in place of _mm_storeu_si128 \
	")
	vst1q_s32((__int32*)(mem_addr),a);
}


/***************************************************************************************************/
// Stream
/***************************************************************************************************/
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline void _mm_stream_pd (double* mem_addr, __m128d a)
{
	vst1q_f64(mem_addr, a);
}
#endif

__forceinline void _mm_stream_si128 (__m128i_i32* mem_addr, __m128i_i32 a)
{
	//Default 32-bit
	#pragma message("\
	Use _mm_stream_epu8, _mm_stream_epi8, \
	_mm_stream_epu16, _mm_stream_epu32, \
	_mm_stream_epi32, _mm_stream_epu64, \
	_mm_stream_epi64 in place of _mm_stream_si128 \
	")
	//*mem_addr = a;
	vst1q_s32((__int32*)(mem_addr),a);
}
__forceinline void _mm_stream_si32 (int* mem_addr, int a)
{
	*mem_addr = a;
}
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline void _mm_stream_si64 (__int64* mem_addr, __int64 a)
{
	*mem_addr = a;
}
#endif

/***************************************************************************************************/
// Cache Hierarchy 
/***************************************************************************************************/
void _mm_clflush (void const* p)
{
	#if _MSC_VER
		#pragma message(" _mm_clflush is not supported on MSVC")
	#else
		#if defined(__arm__)
			#pragma message("_mm_clflush not supported on 32-bit ARM")
		#endif
		#if defined(__aarch64__)
			__asm__ __volatile__("dc ivac, %0\n\t" : : "r" (p) : "memory");
		#endif
	#endif
}
/***************************************************************************************************/
// Memory Barrier 
/***************************************************************************************************/
__forceinline void _mm_pause (void) {
	#if _MSC_VER
		__wfe();
	#else
		#if defined(__arm__)
			#pragma message("_mm_pause not supported on 32-bit ARM")
		#endif
		
		#if defined(__aarch64__)
			__asm__ volatile ("wfe" ::: "memory");
			/*
				NOTE
				__asm__ volatile ("sev" ::: "memory") 
				WILL AWAKEN FROM PAUSE!
			*/
			
		#endif
	#endif
}
__forceinline void _mm_lfence (void) {
	
	#if _MSC_VER
		#if defined(_M_ARM)
			__dmb(_ARM_BARRIER_ISH);
		#endif
		
		#if defined(_M_ARM64)
			__dmb(_ARM64_BARRIER_ISHLD);
		#endif
	#else
		#if defined(__arm__)
			#pragma message("_mm_lfence not supported on 32-bit ARM")
		#endif
		
		#if defined(__aarch64__)
		 __asm__ volatile ("dmb ishld" ::: "memory");
		 #endif
	#endif
}
__forceinline void _mm_mfence (void) {
	
	#if _MSC_VER
		#if defined(_M_ARM)
			__dmb(_ARM_BARRIER_ISH);
		#endif
		
		#if defined(_M_ARM64)
			__dmb(_ARM64_BARRIER_ISH);
		#endif
	#else
		#if defined(__arm__)
			#pragma message("_mm_mfence not supported on 32-bit ARM")
		#endif
		
		#if defined(__aarch64__)
			__asm__ volatile ("dmb ish" ::: "memory");
		#endif
	#endif
}


/***************************************************************************************************/
//Add
/***************************************************************************************************/
__forceinline __m64i_i16 _mm_add_si64 (__m64i_i16 a, __m64i_i16 b)
{
	return vadd_s16(a, b);
}

__forceinline __m128i_i8 _mm_add_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return vaddq_s8(a, b);
}

__forceinline __m128i_i8 _mm_adds_epi8(__m128i_i8 a, __m128i_i8 b)
{
	int8x16_t min = vdupq_n_s8(-128);
	int8x16_t max = vdupq_n_s8(127);
	
	int8x16_t bool_a_is_negative;
	int8x16_t bool_b_is_negative;
	int8x16_t bool_sum_is_negative;
	int8x16_t sum;

	bool_a_is_negative = vmvnq_s8(vreinterpretq_s8_u8(vceqq_s8(vandq_s8(a, max), vdupq_n_s8(0))));
	bool_b_is_negative = vmvnq_s8(vreinterpretq_s8_u8(vceqq_s8(vandq_s8(b, min), vdupq_n_s8(0))));

	sum = vaddq_s8(a, b);
	bool_sum_is_negative = vmvnq_s8(vreinterpretq_s8_u8(vceqq_s8(vandq_s8(sum, min), vdupq_n_s8(0))));

	sum = vbslq_s8( vreinterpretq_u8_s8(vmvnq_s8(vreinterpretq_s8_u8(vceqq_s8(bool_a_is_negative, bool_b_is_negative)))), vdupq_n_s8(0), sum);
	sum = vbslq_s8( vreinterpretq_u8_s8(vreinterpretq_s8_u8(vceqq_s8(bool_a_is_negative, vmvnq_s8(bool_sum_is_negative)))), max, sum);
	sum = vbslq_s8( vreinterpretq_u8_s8(vreinterpretq_s8_u8(vceqq_s8(vmvnq_s8(bool_a_is_negative),bool_sum_is_negative))), min, sum);

	return sum;
}

__forceinline __m128i_i16 _mm_add_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vaddq_s16(a, b);
}

__forceinline __m128i_i16 _mm_adds_epi16(__m128i_i16 a, __m128i_i16 b)
{
	int16x8_t min = vdupq_n_s16(-32768);
	int16x8_t max = vdupq_n_s16(32767);
	
	int16x8_t bool_a_is_negative;
	int16x8_t bool_b_is_negative;
	int16x8_t bool_sum_is_negative;
	int16x8_t sum;

	bool_a_is_negative = vmvnq_s16(vreinterpretq_s16_u16(vceqq_s16(vandq_s16(a, max), vdupq_n_s16(0))));
	bool_b_is_negative = vmvnq_s16(vreinterpretq_s16_u16(vceqq_s16(vandq_s16(b, min), vdupq_n_s16(0))));

	sum = vaddq_s16(a, b);
	bool_sum_is_negative = vmvnq_s16(vreinterpretq_s16_u16(vceqq_s16(vandq_s16(sum, min), vdupq_n_s16(0))));

	sum = vbslq_s16( vreinterpretq_u16_s16(vmvnq_s16(vreinterpretq_s16_u16(vceqq_s16(bool_a_is_negative, bool_b_is_negative)))), vdupq_n_s16(0), sum);
	sum = vbslq_s16( vreinterpretq_u16_s16(vreinterpretq_s16_u16(vceqq_s16(bool_a_is_negative, vmvnq_s16(bool_sum_is_negative)))), max, sum);
	sum = vbslq_s16( vreinterpretq_u16_s16(vreinterpretq_s16_u16(vceqq_s16(vmvnq_s16(bool_a_is_negative),bool_sum_is_negative))), min, sum);

	return sum;
}

__forceinline __m128i_i32 _mm_add_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vaddq_s32(a, b);
}
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128i_i64 _mm_add_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	return vaddq_s64(a, b);
}
#endif

__forceinline __m128i_u8 _mm_adds_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	uint8x16_t  sum;
	uint8x16_t  cmp;
	sum = vaddq_u8(a, b);
	cmp = vcltq_u8(sum,a);
	sum = vorrq_u8(sum, cmp);
	
	return sum;
}
__forceinline __m128i_u16 _mm_adds_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	uint16x8_t  sum;
	uint16x8_t  cmp;
	sum = vaddq_u16(a, b);
	cmp = vcltq_u16(sum,a);
	sum = vorrq_u16(sum, cmp);
	
	return sum;
}

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_add_pd (__m128d a, __m128d b)
{
	return vaddq_f64(a, b);
}
#define _mm_add_sd _mm_add_pd
#endif

/***************************************************************************************************/
// Subtract 
/***************************************************************************************************/

__forceinline __m64i_i16 _mm_sub_si64 (__m64i_i16 a, __m64i_i16 b)
{
	return vsub_s16(a, b);
}


__forceinline __m128i_i8 _mm_sub_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return vsubq_s8(a, b);
}

__forceinline __m128i_i8 _mm_subs_epi8(__m128i_i8 a, __m128i_i8 b)
{
	int8x16_t min = vdupq_n_s8(-128);
	int8x16_t max = vdupq_n_s8(127);
	
	int8x16_t bool_a_is_negative;
	int8x16_t bool_b_is_negative;
	int8x16_t bool_sum_is_negative;
	int8x16_t sum;

	bool_a_is_negative = vmvnq_s8(vreinterpretq_s8_u8(vceqq_s8(vandq_s8(a, max), vdupq_n_s8(0))));
	bool_b_is_negative = vmvnq_s8(vreinterpretq_s8_u8(vceqq_s8(vandq_s8(b, min), vdupq_n_s8(0))));

	sum = vsubq_s8(a, b);
	bool_sum_is_negative = vmvnq_s8(vreinterpretq_s8_u8(vceqq_s8(vandq_s8(sum, min), vdupq_n_s8(0))));

	sum = vbslq_s8( vreinterpretq_u8_s8(vmvnq_s8(vreinterpretq_s8_u8(vceqq_s8(bool_a_is_negative, bool_b_is_negative)))), vdupq_n_s8(0), sum);
	sum = vbslq_s8( vreinterpretq_u8_s8(vreinterpretq_s8_u8(vceqq_s8(bool_a_is_negative, vmvnq_s8(bool_sum_is_negative)))), max, sum);
	sum = vbslq_s8( vreinterpretq_u8_s8(vreinterpretq_s8_u8(vceqq_s8(vmvnq_s8(bool_a_is_negative),bool_sum_is_negative))), min, sum);

	return sum;
}

__forceinline __m128i_i16 _mm_sub_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vsubq_s16(a, b);
}

__forceinline __m128i_i16 _mm_subs_epi16(__m128i_i16 a, __m128i_i16 b)
{
	int16x8_t min = vdupq_n_s16(-32768);
	int16x8_t max = vdupq_n_s16(32767);
	
	int16x8_t bool_a_is_negative;
	int16x8_t bool_b_is_negative;
	int16x8_t bool_sum_is_negative;
	int16x8_t sum;

	bool_a_is_negative = vmvnq_s16(vreinterpretq_s16_u16(vceqq_s16(vandq_s16(a, max), vdupq_n_s16(0))));
	bool_b_is_negative = vmvnq_s16(vreinterpretq_s16_u16(vceqq_s16(vandq_s16(b, min), vdupq_n_s16(0))));

	sum = vsubq_s16(a, b);
	bool_sum_is_negative = vmvnq_s16(vreinterpretq_s16_u16(vceqq_s16(vandq_s16(sum, min), vdupq_n_s16(0))));

	sum = vbslq_s16( vreinterpretq_u16_s16(vmvnq_s16(vreinterpretq_s16_u16(vceqq_s16(bool_a_is_negative, bool_b_is_negative)))), vdupq_n_s16(0), sum);
	sum = vbslq_s16( vreinterpretq_u16_s16(vreinterpretq_s16_u16(vceqq_s16(bool_a_is_negative, vmvnq_s16(bool_sum_is_negative)))), max, sum);
	sum = vbslq_s16( vreinterpretq_u16_s16(vreinterpretq_s16_u16(vceqq_s16(vmvnq_s16(bool_a_is_negative),bool_sum_is_negative))), min, sum);

	return sum;
}

__forceinline __m128i_i32 _mm_sub_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vsubq_s32(a, b);
}

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128i_i64 _mm_sub_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	return vsubq_s64(a, b);
}
#endif

__forceinline __m128i_u8 _mm_subs_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	uint8x16_t sum;
	uint8x16_t cmp;
	sum = vsubq_u8(a, b);
	cmp = vcleq_u8(sum, a);
	sum = vandq_u8(sum, cmp);
	
	return sum;
}
__forceinline __m128i_u16 _mm_subs_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	uint16x8_t sum;
	uint16x8_t cmp;
	sum = vsubq_u16(a, b);
	cmp = vcleq_u16(sum, a);
	sum = vandq_u16(sum, cmp);
	
	return sum;
}

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_sub_pd (__m128d a, __m128d b)
{
	return vsubq_f64(a, b); 
}
#define _mm_sub_sd _mm_sub_pd
#endif

/***************************************************************************************************/
// Absolute Differences
/***************************************************************************************************/
__forceinline __m128i_u16 _mm_sad_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	uint16x8_t ret= vdupq_n_u16(0);

	ret = vsetq_lane_u16 ((__uint16)vaddv_u8( vabd_u8(vget_low_u8(a), vget_low_u8(b)) ), ret, 0);
	ret = vsetq_lane_u16 ((__uint16)vaddv_u8( vabd_u8(vget_high_u8(a), vget_high_u8(b)) ), ret, 4);

	return ret;
}


/***************************************************************************************************/
// Multiply
/***************************************************************************************************/
__forceinline __m128i_i32 _mm_madd_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vpaddlq_s16(vmulq_s16 (a, b));
}

__forceinline __m64i_u32 _mm_mul_su32 (__m64i_u32 a, __m64i_u32 b)
{
	return vmul_u32(a, b);
}

__forceinline __m128i_u32 _mm_mul_epu32 (__m128i_u32 a, __m128i_u32 b)
{
	return vmulq_u32(a, b);
}
__forceinline __m128i_i16 _mm_mulhi_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vshrq_n_s16(vqdmulhq_s16(a, b), 1);
}
__forceinline __m128i_u16 _mm_mulhi_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	return vshrq_n_u16(vreinterpretq_u16_s16(
	vqdmulhq_s16(vreinterpretq_s16_u16(a), vreinterpretq_s16_u16(b))), 1);
}
__forceinline __m128i_i16 _mm_mullo_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vmulq_s16(a, b);
}
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_mul_pd (__m128d a, __m128d b)
{
	return vmulq_f64(a, b);
}
#define _mm_mul_sd _mm_mul_pd
#endif

/***************************************************************************************************/
// Divide
/***************************************************************************************************/
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_div_pd (__m128d a, __m128d b)
{
	return  vdivq_f64(a, b);
	
}
#define _mm_div_sd _mm_div_pd
#endif

/***************************************************************************************************/
// Square Root
/***************************************************************************************************/
#if defined(__aarch64__) || defined(_M_ARM64)
__m128d _mm_sqrt_pd (__m128d a)
{
	//MSVC will show as error!
	return vsqrtq_f64(a);
}
#define _mm_sqrt_sd _mm_sqrt_pd
#endif

/***************************************************************************************************/
// Average
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_avg_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	return vshrq_n_u8(vaddq_u8(vaddq_u8(a,b), vdupq_n_u8(1) ), 1);
}

__forceinline __m128i_u16 _mm_avg_epu16 (__m128i_u16 a, __m128i_u16 b)
{
	return vshrq_n_u16(vaddq_u16(vaddq_u16(a,b), vdupq_n_u16(1) ), 1);
}


/***************************************************************************************************/
// Maximum
/***************************************************************************************************/
__forceinline __m128i_i16 _mm_max_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vmaxq_s16(a, b);
}

__forceinline __m128i_u8 _mm_max_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	return vmaxq_u8(a, b);
}
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_max_pd (__m128d a, __m128d b)
{
	return vmaxq_f64(a, b);
}
#define _mm_max_sd _mm_max_pd
#endif

/***************************************************************************************************/
// Minimum
/***************************************************************************************************/

__forceinline __m128i_i16 _mm_min_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vminq_s16(a, b);
}
__forceinline __m128i_u8 _mm_min_epu8 (__m128i_u8 a, __m128i_u8 b)
{
	return vminq_u8(a, b);
}
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_min_pd (__m128d a, __m128d b)
{
	return vminq_f64(a, b);
}
#define _mm_min_sd _mm_min_pd
#endif

/***************************************************************************************************/
// Bitwise
/***************************************************************************************************/
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_and_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_s64(vandq_s64(vreinterpretq_s64_f64(a), vreinterpretq_s64_f64(b)));
}
#endif

__forceinline __m128i_i32 _mm_and_si128 (__m128i_i32 a, __m128i_i32 b)
{
	return vandq_s32(a, b);
	#pragma message("\
	Use _mm_and_epu8, _mm_and_epi8, \
	_mm_and_epu16, _mm_and_epu32, \
	_mm_and_epi32, _mm_and_epu64, \
	_mm_and_epi64 in place of _mm_and_si128 \
	")
}

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_andnot_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_s64(vbicq_s64(vreinterpretq_s64_f64(b), vreinterpretq_s64_f64(a)));
}
__forceinline __m128i_i32 _mm_andnot_si128 (__m128i_i32 a, __m128i_i32 b)
{
	return vbicq_s32(b, a);
	#pragma message("\
	Use _mm_andnot_epu8, _mm_andnot_epi8, \
	_mm_andnot_epu16, _mm_andnot_epu32, \
	_mm_andnot_epi32, _mm_andnot_epu64, \
	_mm_andnot_epi64 in place of _mm_andnot_si128 \
	")
}

__forceinline __m128d _mm_or_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_s64(vorrq_s64(vreinterpretq_s64_f64(a), vreinterpretq_s64_f64(b)));
}
__forceinline __m128i_i32 _mm_or_si128 (__m128i_i32 a, __m128i_i32 b)
{
	return vorrq_s32(a, b);
	#pragma message("\
	Use _mm_or_epu8, _mm_or_epi8, \
	_mm_or_epu16, _mm_or_epu32, \
	_mm_or_epi32, _mm_or_epu64, \
	_mm_or_epi64 in place of _mm_or_si128 \
	")
}

__forceinline __m128d _mm_xor_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_s64(veorq_s64(vreinterpretq_s64_f64(a), vreinterpretq_s64_f64(b)));
}
__forceinline __m128i_i32 _mm_xor_si128 (__m128i_i32 a, __m128i_i32 b)
{
	return veorq_s32(a, b);
	#pragma message("\
	Use _mm_xor_epu8, _mm_xor_epi8, \
	_mm_xor_epu16, _mm_xor_epu32, \
	_mm_xor_epi32, _mm_xor_epu64, \
	_mm_xor_epi64 in place of _mm_xor_si128 \
	")
}
#endif

/***************************************************************************************************/
// Comparator 
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_cmpeq_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return vreinterpretq_s8_u8(vceqq_s8(a,b));
}
__forceinline __m128i_i16 _mm_cmpeq_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vreinterpretq_s16_u16(vceqq_s16(a,b));
}
__forceinline __m128i_i32 _mm_cmpeq_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vreinterpretq_s32_u32(vceqq_s32(a,b));
}
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_cmpeq_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_u64(vceqq_f64(a,b));
}
#define _mm_cmpeq_sd _mm_cmpeq_pd

__forceinline __m128d _mm_cmpge_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_u64(vcgeq_f64(a,b));
}
#define _mm_cmpge_sd _mm_cmpge_pd
#endif

__forceinline __m128i_i8 _mm_cmpgt_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return vreinterpretq_s8_u8(vcgtq_s8(a,b));
}
__forceinline __m128i_i16 _mm_cmpgt_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vreinterpretq_s16_u16(vcgtq_s16(a,b));
}
__forceinline __m128i_i32 _mm_cmpgt_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vreinterpretq_s32_u32(vcgtq_s32(a,b));
}
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_cmpgt_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_u64(vcgtq_f64(a,b));
}
#define _mm_cmpgt_sd _mm_cmpgt_pd

__forceinline __m128d _mm_cmple_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_u64(vcleq_f64(a,b));
}
#define _mm_cmple_sd _mm_cmple_pd
#endif

__forceinline __m128i_i8 _mm_cmplt_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	return vreinterpretq_s8_u8(vcltq_s8(a,b));
}
__forceinline __m128i_i16 _mm_cmplt_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vreinterpretq_s16_u16(vcltq_s16(a,b));
}
__forceinline __m128i_i32 _mm_cmplt_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vreinterpretq_s32_u32(vcltq_s32(a,b));
}

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128d _mm_cmplt_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_u64(vcltq_f64(a,b));
}
#define _mm_cmplt_sd _mm_cmplt_pd

__forceinline __m128d _mm_cmpneq_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_s64(veorq_s64(
	vreinterpretq_s64_u64(vceqq_f64(a, b)), 
	vreinterpretq_s64_u64(vceqq_s64(vdupq_n_s64(0),vdupq_n_s64(0))))
	);
}
#define _mm_cmpneq_sd _mm_cmpneq_pd

__forceinline __m128d _mm_cmpnge_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_s64(veorq_s64(
	vreinterpretq_s64_u64(vcgeq_f64(a, b)), 
	vreinterpretq_s64_u64(vceqq_s64(vdupq_n_s64(0),vdupq_n_s64(0))))
	);
}
#define _mm_cmpnge_sd _mm_cmpnge_pd

__forceinline __m128d _mm_cmpngt_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_s64(veorq_s64(
	vreinterpretq_s64_u64(vcgtq_f64(a, b)), 
	vreinterpretq_s64_u64(vceqq_s64(vdupq_n_s64(0),vdupq_n_s64(0))))
	);
}
#define _mm_cmpngt_sd _mm_cmpngt_pd

__forceinline __m128d _mm_cmpnle_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_s64(veorq_s64(
	vreinterpretq_s64_u64(vcleq_f64(a, b)), 
	vreinterpretq_s64_u64(vceqq_s64(vdupq_n_s64(0),vdupq_n_s64(0))))
	);
}
#define _mm_cmpnle_sd _mm_cmpnle_pd

__forceinline __m128d _mm_cmpnlt_pd (__m128d a, __m128d b)
{
	return vreinterpretq_f64_s64(veorq_s64(
	vreinterpretq_s64_u64(vcltq_f64(a, b)), 
	vreinterpretq_s64_u64(vceqq_s64(vdupq_n_s64(0),vdupq_n_s64(0))))
	);
}
#define _mm_cmpnlt_sd _mm_cmpnlt_pd

__forceinline __m128d _mm_cmpord_pd (__m128d a, __m128d b)
{
	uint64x2_t aa, bb;

	aa = vceqq_f64(a, a);
	bb = vceqq_f64(b, b);

	return vreinterpretq_f64_u64(vandq_u64(aa, bb));
}
#define _mm_cmpord_sd _mm_cmpord_pd

__forceinline __m128d _mm_cmpunord_pd (__m128d a, __m128d b)
{
	uint64x2_t aa, bb;

	aa = vceqq_f64(a, a);
	bb = vceqq_f64(b, b);

	return 
	vreinterpretq_f64_s64(veorq_s64(
	vreinterpretq_s64_u64(vandq_u64(aa, bb)), 
	vreinterpretq_s64_u64(vceqq_s64(vdupq_n_s64(0),vdupq_n_s64(0))))
	);
}
#define _mm_cmpunord_sd _mm_cmpunord_pd

__forceinline int _mm_comieq_sd (__m128d a, __m128d b)
{
	return vgetq_lane_u64(vceqq_f64(a, b), 0);
}
__forceinline int _mm_comige_sd (__m128d a, __m128d b)
{
	return vgetq_lane_u64(vcgeq_f64(a, b), 0);
}
__forceinline int _mm_comigt_sd (__m128d a, __m128d b)
{
	return vgetq_lane_u64(vcgtq_f64(a, b), 0);
}
__forceinline int _mm_comile_sd (__m128d a, __m128d b)
{
	return vgetq_lane_u64(vcleq_f64(a, b), 0);
}
__forceinline int _mm_comilt_sd (__m128d a, __m128d b)
{
	return vgetq_lane_u64(vcltq_f64(a, b), 0);
}
__forceinline int _mm_comineq_sd (__m128d a, __m128d b)
{
	return 
	vgetq_lane_u64(
	vreinterpretq_u64_s64(veorq_s64(
	vreinterpretq_s64_u64(vceqq_f64(a, b)), 
	vreinterpretq_s64_u64(vceqq_s64(vdupq_n_s64(0),vdupq_n_s64(0))))
	), 1);
}

#define _mm_ucomieq_sd _mm_comieq_sd
#define _mm_ucomige_sd _mm_comige_sd
#define _mm_ucomigt_sd _mm_comigt_sd
#define _mm_ucomile_sd _mm_comile_sd
#define _mm_ucomilt_sd _mm_comilt_sd
#define _mm_ucomineq_sd _mm_comineq_sd
#endif


/***************************************************************************************************/
// Shifting
/***************************************************************************************************/
__forceinline __m128i_i16 _mm_sll_epi16 (__m128i_i16 a, __m128i_i16 count)
{
	uint16x8_t gt15 = vcgtq_s16(count, vdupq_n_s16(15));
	int16x8_t ret = vshlq_s16(a, count);
	return vbslq_s16 (gt15, ret, vdupq_n_s16(0));
}
__forceinline __m128i_i32 _mm_sll_epi32 (__m128i_i32 a, __m128i_i32 count)
{
	uint32x4_t gt31 = vcgtq_s32(count, vdupq_n_s32(31));
	int32x4_t ret = vshlq_s32(a, count);
	return vbslq_s32 (gt31, ret, vdupq_n_s32(0));
}
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128i_i64 _mm_sll_epi64 (__m128i_i64 a, __m128i_i64 count)
{
	uint64x2_t gt63 = vcgtq_s64(count, vdupq_n_s64(63));
	int64x2_t ret = vshlq_s64(a, count);
	return vbslq_s64 (gt63, ret, vdupq_n_s64(0));
}
#endif

__forceinline __m128i_i16 _mm_slli_epi16 (__m128i_i16 a, int imm8)
{
	if ((imm8) <= 0) 
	{
		return a;
	}
	else if ((imm8) > 15) 
	{
		return vdupq_n_s16(0);
	}
	else
	{
		return vshlq_s16(a, vdupq_n_s16(imm8));
	}
}


__forceinline __m128i_i32 _mm_slli_epi32 (__m128i_i32 a, int imm8)
{
	if ((imm8) <= 0) 
	{
		return a;
	}
	else if ((imm8) > 31) 
	{
		return vdupq_n_s32(0);
	}
	else
	{
		return vshlq_s32(a, vdupq_n_s32(imm8));
	}
}
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128i_i64 _mm_slli_epi64 (__m128i_i64 a, int imm8)
{
	if ((imm8) <= 0) 
	{
		return a;
	}
	else if ((imm8) > 63) 
	{
		return vdupq_n_s64(0);
	}
	else
	{
		return vshlq_s64(a, vdupq_n_s64(imm8));
	}
}
#endif

//__m128i_i32 _mm_slli_si128 (__m128i_i32 a, int imm8)
#define _mm_slli_si128(a, imm)																			\
({																										\
	__m128i_i32 ret;																					\
	if ((imm) <= 0) {																					\
		ret = a;																						\
	}																									\
	else if ((imm) > 15) {																				\
		ret = vdupq_n_s32(0);																			\
	}																									\
	else {																								\
		ret = vreinterpretq_s32_s8(vextq_s8(vdupq_n_s8(0), vreinterpretq_s8_s32(a), 16 - (imm)));		\
	}																									\
	ret;																								\
})
#pragma message("\
Use _mm_slli_epu8, _mm_slli_epi8, \
_mm_slli_epu16, _mm_slli_epu32, \
_mm_slli_epi32, _mm_slli_epu64, \
_mm_slli_epi64 in place of _mm_slli_si128, \
_mm_slli_si128 will assume 32-bit int!!!\
")
#define _mm_bslli_si128 _mm_slli_si128


__forceinline __m128i_i16 _mm_sra_epi16 (__m128i_i16 a, __m128i_i16 count) //(DISCREPANCY)
{
	uint16x8_t	gt15 = vcgtq_s16(count, vdupq_n_s16(15));
	int16x8_t	ret = vshlq_s16(a, vnegq_s16(count));
	int16x8_t	ret2 = vshrq_n_s16(a, 8); 
				ret2 = vshrq_n_s16(ret2, 8);
	return vbslq_s16 (gt15, ret, ret2);
}

__forceinline __m128i_i32 _mm_sra_epi32 (__m128i_i32 a, __m128i_i32 count) //(DISCREPANCY)
{
	uint32x4_t	gt31 = vcgtq_s32(count, vdupq_n_s32(31));
	int32x4_t	ret = vshlq_s32(a, vnegq_s32(count));
	int32x4_t	ret2 = vshrq_n_s32(a, 16); 
				ret2 = vshrq_n_s32(ret2, 16);
	return vbslq_s32 (gt31, ret, ret2);
}


__forceinline __m128i_i16 _mm_srai_epi16 (__m128i_i16 a, int imm8)
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 15) )
	{
		return vshrq_n_s16(vshrq_n_s16(a, 8), 8);
	}
	else
	{
		return vshlq_s16(a, vnegq_s16(vdupq_n_s16(imm8))); //vshrq_n_s16(a, imm8);
	}
}
__forceinline __m128i_i32 _mm_srai_epi32 (__m128i_i32 a, int imm8)
{
	if ((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 31) )
	{
		return vshrq_n_s32(vshrq_n_s32(a, 16), 16);
	}
	else
	{
		return vshlq_s32(a, vnegq_s32(vdupq_n_s32(imm8))); //vshrq_n_s32(a, imm8);
	}
}

__forceinline __m128i_i16 _mm_srl_epi16 (__m128i_i16 a, __m128i_i16 count) //(DISCREPANCY)
{
	uint16x8_t	gt15 = vcgtq_s16(count, vdupq_n_s16(15));
	uint16x8_t	lt0 = vcltq_s16(count, vdupq_n_s16(0));
	int16x8_t	ret = vshlq_s16(a, vnegq_s16(count));
	return vbslq_s16 (lt0, vbslq_s16 (gt15, ret, vdupq_n_s16(0)), vdupq_n_s16(0));
}
__forceinline __m128i_i32 _mm_srl_epi32 (__m128i_i32 a, __m128i_i32 count) //(DISCREPANCY)
{
	uint32x4_t	gt31 = vcgtq_s32(count, vdupq_n_s32(31));
	uint32x4_t	lt0 = vcltq_s32(count, vdupq_n_s32(0));
	int32x4_t	ret = vshlq_s32(a, vnegq_s32(count));
	return vbslq_s32 (lt0, vbslq_s32 (gt31, ret, vdupq_n_s32(0)), vdupq_n_s32(0));
}
#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128i_i64 _mm_srl_epi64 (__m128i_i64 a, __m128i_i64 count) //(DISCREPANCY)
{
	uint64x2_t	gt63 = vcgtq_s64(count, vdupq_n_s64(63));
	uint64x2_t	lt0 = vcltq_s64(count, vdupq_n_s64(0));
	int64x2_t	ret = vshlq_s64(a, vnegq_s64(count));
	return vbslq_s64 (lt0, vbslq_s64 (gt63, ret, vdupq_n_s64(0)), vdupq_n_s64(0));
}
#endif

__forceinline __m128i_i16 _mm_srli_epi16 (__m128i_i16 a, int imm8)
{
	if((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 15) )
	{
		return vdupq_n_s16(0);
	}
	else
	{
		return vshlq_s16(a, vnegq_s16(vdupq_n_s16(imm8))); //vshrq_n_s16(a, imm8)
	}
}
__forceinline __m128i_i32 _mm_srli_epi32 (__m128i_i32 a, int imm8)  //(CRITICAL)
{
	if((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 31) )
	{
		return vdupq_n_s32(0);
	}
	else
	{
		return vshlq_s32(a, vnegq_s32(vdupq_n_s32(imm8))); //vshrq_n_s32(a, imm8)
	}
}

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline __m128i_i64 _mm_srli_epi64 (__m128i_i64 a, int imm8) //(CRITICAL)
{
	if((imm8) == 0)
	{
		return a;
	}
	else if ( ((imm8) < 0) || ((imm8) > 63) )
	{
		return vdupq_n_s64(0);
	}
	else
	{
		return vshlq_s64(a, vnegq_s64(vdupq_n_s64(imm8))); //vshrq_n_s64(a, imm8)
	}
}
#endif

//__m128i_i32 _mm_srli_si128 (__m128i_i32 a, int imm8)
#define _mm_srli_si128(a, imm8)																			\
({																										\
	__m128i_i32 ret;																					\
	if ((imm8) <= 0) {																					\
		ret = a;																						\
	}																									\
	else if ((imm8) > 15) {																				\
		ret = vdupq_n_s32(0);																			\
	}																									\
	else {																								\
		ret = vreinterpretq_s32_s8(vextq_s8(vreinterpretq_s8_s32(a), vdupq_n_s8(0), (imm8)));			\
	}																									\
	ret;																								\
})
#pragma message("\
Use _mm_srli_epu8, _mm_srli_epi8, \
_mm_srli_epu16, _mm_srli_epu32, \
_mm_srli_epi32, _mm_srli_epu64, \
_mm_srli_epi64 in place of _mm_srli_si128 \
_mm_srli_si128 will assume 32-bit int!!!\
")
#define _mm_bsrli_si128 _mm_srli_si128


/***************************************************************************************************/
// Casting
/***************************************************************************************************/
#if ARM64
__forceinline __m128 _mm_castpd_ps (__m128d a)
{
	return vreinterpretq_f32_f64(a);
}
__forceinline __m128i_i64 _mm_castpd_si128 (__m128d a)
{
	return vreinterpretq_s64_f64(a);
}
__forceinline __m128d _mm_castps_pd (__m128 a)
{
	return vreinterpretq_f64_f32(a);
}
__forceinline __m128i_i32 _mm_castps_si128 (__m128 a)
{
	return vreinterpretq_s32_f32(a);
}
__forceinline __m128d _mm_castsi128_pd (__m128i_i64 a)
{
	return vreinterpretq_f64_s64(a);
}
__forceinline __m128 _mm_castsi128_ps (__m128i_i32 a)
{
	return vreinterpretq_f32_s32(a);
}
#endif

/***************************************************************************************************/
// CVT
/***************************************************************************************************/
__forceinline int _mm_cvtsi128_si32 (__m128i_i32 a)
{
	return vgetq_lane_s32(a, 0);
}
#if ARM64
__forceinline int _mm_cvttsd_si32 (__m128d a)
{
	return vgetq_lane_s64(vcvtq_s64_f64(a), 0) & 0xFFFFFFFFU;
}
__forceinline int _mm_cvtsd_si32 (__m128d a)
{
	return vgetq_lane_s64(vcvtq_s64_f64(a), 0);
}
__forceinline __int64 _mm_cvtsi128_si64 (__m128i_i64 a)
{
	return vgetq_lane_s64(a, 0);
}
#define _mm_cvtsi128_si64x _mm_cvtsi128_si64

__forceinline __int64 _mm_cvttsd_si64 (__m128d a)
{
	return vgetq_lane_s64(vcvtq_s64_f64(a), 0) & 0xFFFFFFFFFFFFFFFFU;
}
#define _mm_cvttsd_si64x _mm_cvttsd_si64

__forceinline __int64 _mm_cvtsd_si64 (__m128d a)
{
	return vgetq_lane_s64(vcvtq_s64_f64(a), 0);
}
#define _mm_cvtsd_si64x _mm_cvtsd_si64

__forceinline double _mm_cvtsd_f64 (__m128d a)
{
	return vgetq_lane_f64(a, 0);
}
#endif

#if ARM64
__forceinline __m64i_i32 _mm_cvtpd_pi32 (__m128d a)
{
	return vmovn_s64(vcvtq_s64_f64(a));
}
#define _mm_cvttpd_pi32 _mm_cvtpd_pi32
#endif

__forceinline __m128 _mm_cvtepi32_ps (__m128i_i32 a)
{
	return vcvtq_f32_s32(a);
}
#if ARM64
__forceinline __m128d _mm_cvtepi32_pd (__m128i_i32 a)
{
	double ALIGNED(16) data[2] = 
	{ 
		(double)vgetq_lane_s32(a, 0), 
		(double)vgetq_lane_s32(a, 1) 
	};
	return vld1q_f64(data);
}
__forceinline __m128 _mm_cvtpd_ps (__m128d a)
{
	float ALIGNED(16) data[4] = 
	{ 
		(float)vgetq_lane_f64(a, 0), 
		(float)vgetq_lane_f64(a, 1), 
		0, 
		0, 
	};
	return vld1q_f32(data);
}
__forceinline __m128 _mm_cvtsd_ss (__m128 a, __m128d b)
{
	return vsetq_lane_f32 (vgetq_lane_f64(b,0), a, 0);
}
__forceinline __m128i_i32 _mm_cvtps_epi32 (__m128 a)
{
	return vcvtq_s32_f32(a);
}
__forceinline __m128i_i32 _mm_cvtpd_epi32 (__m128d a)
{
	int64x2_t ai;
	ai = vcvtq_s64_f64(a);
	__int32 ALIGNED(16) data[4] = 
	{ 
		(__int32)vgetq_lane_s64(ai, 0), 
		(__int32)vgetq_lane_s64(ai, 1), 
		0, 
		0 
	};
	return vld1q_s32(data);
}
#define _mm_cvttpd_epi32 _mm_cvtpd_epi32

__forceinline __m128d _mm_cvtpi32_pd (__m64i_i32 a)
{
	double ALIGNED(16) data[2] = 
	{ 
		(double)vget_lane_s32(a, 0), 
		(double)vget_lane_s32(a, 1) 
	};
	return vld1q_f64(data);
}
__forceinline __m128d _mm_cvtps_pd (__m128 a)
{
	double ALIGNED(16) data[2] = 
	{ 
		(double)vgetq_lane_f32(a, 0), 
		(double)vgetq_lane_f32(a, 1) 
	};
	return vld1q_f64(data);
}
__forceinline __m128d _mm_cvtsi32_sd (__m128d a, int b)
{
	return vsetq_lane_f64 ((double)b, a, 0);
}
#endif

__forceinline __m128i_i32 _mm_cvtsi32_si128 (int a)
{
	return vsetq_lane_s32(a, vdupq_n_s32(0), 0);
}

#if ARM64
__forceinline __m128d _mm_cvtsi64_sd (__m128d a, __int64 b)
{
	return vsetq_lane_f64 ((double)b, a, 0);
}
#define _mm_cvtsi64x_sd _mm_cvtsi64_sd

__forceinline __m128i_i64 _mm_cvtsi64_si128 (__int64 a)
{
	return vsetq_lane_s64(a, vdupq_n_s64(0), 0);
}
#define _mm_cvtsi64x_si128 _mm_cvtsi64_si128

__forceinline __m128d _mm_cvtss_sd (__m128d a, __m128 b)
{
	return vsetq_lane_f64((double)vgetq_lane_f32(b, 0), a, 0);
}
#endif

__forceinline __m128i_i32 _mm_cvttps_epi32 (__m128 a)
{
	return vcvtq_s32_f32(a);
}

/***************************************************************************************************/
// Move
/***************************************************************************************************/
#if ARM64
__forceinline __m128i_i64 _mm_move_epi64 (__m128i_i64 a)
{
	return vsetq_lane_s64 (vgetq_lane_s64(a, 0), vdupq_n_s64(0), 0);
}
__forceinline __m128d _mm_move_sd (__m128d a, __m128d b)
{
	return vsetq_lane_f64 (vgetq_lane_f64(b, 0), a, 0);
}

__forceinline __m64i_i64 _mm_movepi64_pi64 (__m128i_i64 a)
{
	return vget_low_s64(a);
}
__forceinline __m128i_i64 _mm_movpi64_epi64 (__m64i_i64 a)
{
	return vsetq_lane_s64 ((__int64)a, vdupq_n_s64(0), 0);
}
#endif

__forceinline void _mm_maskmoveu_si128 (__m128i_i8 a, __m128i_i8 mask, __int8* mem_addr) 
{
	int8x16_t b = vld1q_s8((int8_t*)mem_addr);
	int8x16_t res = vbslq_s8 (vreinterpretq_u8_s8(mask), a, b);
	vst1q_s8((int8_t*)mem_addr, res);
}
/***************************************************************************************************/
// Movemask Boolean
/***************************************************************************************************/
__forceinline int _mm_movemask_epi8 (__m128i_i8 a)
{
	int8_t ALIGNED(8) xr[8] = {-7,-6,-5,-4,-3,-2,-1,0};
	int8x8_t shift_vector = vld1_s8(xr);
	uint8x16_t tmp = vandq_u8(vreinterpretq_u8_s8(a), vdupq_n_u8(0x80));
	
	uint8x8_t lo = vget_low_u8(tmp);
	uint8x8_t hi = vget_high_u8(tmp);

	lo = vshl_u8(lo, shift_vector);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	lo = vpadd_u8(lo,lo);
	
	hi = vshl_u8(hi, shift_vector);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);
	hi = vpadd_u8(hi,hi);

	return ((vget_lane_u8(hi,0) << 8) | (vget_lane_u8(lo,0) & 0xFF));
}

/* NOTES
{
	uint8x16_t &ia = *(uint8x16_t *)&a;
	return 
	(
		((ia[0] >> 7    )) | ((ia[1] >> 6) & 2) | ((ia[2] >> 5) & 4) | ((ia[3] >> 4) & 8) |
		((ia[4] >> 3) & 16) | ((ia[5] >> 2) & 32) | ((ia[6] >> 1) & 64) | ((ia[7]) & 128)|
		((ia[8] >> 7    )) | ((ia[9] >> 6) & 2) | ((ia[10] >> 5) & 4) | ((ia[11] >> 4) & 8) |
		((ia[12] >> 3) & 16) | ((ia[13] >> 2) & 32) | ((ia[14] >> 1) & 64) | ((ia[15]) & 128)
	);
}
*/

#if defined(__aarch64__) || defined(_M_ARM64)
__forceinline int _mm_movemask_pd (__m128d a)
{
	#if _MSC_VER
		uint64x2_t &ia = *(uint64x2_t *)&a; 
		return (ia.n128_u64[0] >> 63) | ((ia.n128_u64[1] >> 62) & 2);
	#else
		uint64x2_t &ia = *(uint64x2_t *)&a; 
		return (ia[0] >> 63) | ((ia[1] >> 62) & 2);
	#endif
}
#endif

/***************************************************************************************************/
// Pack
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_packus_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vcombine_u8( vqmovun_s16(a), vqmovun_s16(b) );
}


__forceinline __m128i_i8 _mm_packs_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	return vcombine_s8( vmovn_s16(a), vmovn_s16(b) );
}

__forceinline __m128i_i16 _mm_packs_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	return vcombine_s16( vmovn_s32(a), vmovn_s32(b) );
}


/***************************************************************************************************/
// Unpack MAKE PORTABLE VERSION FO UNSIGNED
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_unpackhi_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
	#else
		__int8 ALIGNED(16) data[16] =
	#endif
	{ 
		vgetq_lane_s8(a, 8), 
		vgetq_lane_s8(b, 8), 
		vgetq_lane_s8(a, 9), 
		vgetq_lane_s8(b, 9), 
		vgetq_lane_s8(a, 10), 
		vgetq_lane_s8(b, 10), 
		vgetq_lane_s8(a, 11), 
		vgetq_lane_s8(b, 11), 
		vgetq_lane_s8(a, 12), 
		vgetq_lane_s8(b, 12), 
		vgetq_lane_s8(a, 13), 
		vgetq_lane_s8(b, 13), 
		vgetq_lane_s8(a, 14), 
		vgetq_lane_s8(b, 14), 
		vgetq_lane_s8(a, 15), 
		vgetq_lane_s8(b, 15) 
	};
	return  vld1q_s8(data);
}

__forceinline __m128i_i16 _mm_unpackhi_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	__int16 ALIGNED(16) data[8] = 
	{ 
		vgetq_lane_s16(a, 4), 
		vgetq_lane_s16(b, 4), 
		vgetq_lane_s16(a, 5), 
		vgetq_lane_s16(b, 5), 
		vgetq_lane_s16(a, 6), 
		vgetq_lane_s16(b, 6), 
		vgetq_lane_s16(a, 7), 
		vgetq_lane_s16(b, 7),
	};
	return  vld1q_s16(data);
}
__forceinline __m128i_i32 _mm_unpackhi_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	__int32 ALIGNED(16) data[4] = 
	{ 
		vgetq_lane_s32(a, 2), 
		vgetq_lane_s32(b, 2), 
		vgetq_lane_s32(a, 3), 
		vgetq_lane_s32(b, 3), 
	};
	return vld1q_s32(data);
}

#if ARM64
__forceinline __m128i_i64 _mm_unpackhi_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	return vcombine_s64(vget_high_s64(a), vget_high_s64(b));
}
__forceinline __m128d _mm_unpackhi_pd (__m128d a, __m128d b)
{
	return vcombine_f64(vget_high_f64(a), vget_high_f64(b));
}
#endif

__forceinline __m128i_i8 _mm_unpacklo_epi8 (__m128i_i8 a, __m128i_i8 b)
{
	#if _MSC_VER
		int8_t ALIGNED(16) data[16] =
	#else
		__int8 ALIGNED(16) data[16] =
	#endif
	{ 
		vgetq_lane_s8(a, 0), 
		vgetq_lane_s8(b, 0), 
		vgetq_lane_s8(a, 1), 
		vgetq_lane_s8(b, 1), 
		vgetq_lane_s8(a, 2), 
		vgetq_lane_s8(b, 2), 
		vgetq_lane_s8(a, 3), 
		vgetq_lane_s8(b, 3), 
		vgetq_lane_s8(a, 4), 
		vgetq_lane_s8(b, 4), 
		vgetq_lane_s8(a, 5), 
		vgetq_lane_s8(b, 5), 
		vgetq_lane_s8(a, 6), 
		vgetq_lane_s8(b, 6), 
		vgetq_lane_s8(a, 7), 
		vgetq_lane_s8(b, 7) 
	};
	return  vld1q_s8(data);
}

__forceinline __m128i_i16 _mm_unpacklo_epi16 (__m128i_i16 a, __m128i_i16 b)
{
	__int16 ALIGNED(16) data[8] = 
	{ 
		vgetq_lane_s16(a, 0), 
		vgetq_lane_s16(b, 0), 
		vgetq_lane_s16(a, 1), 
		vgetq_lane_s16(b, 1), 
		vgetq_lane_s16(a, 2), 
		vgetq_lane_s16(b, 2), 
		vgetq_lane_s16(a, 3), 
		vgetq_lane_s16(b, 3),
	};
	return  vld1q_s16(data);
}
__forceinline __m128i_i32 _mm_unpacklo_epi32 (__m128i_i32 a, __m128i_i32 b)
{
	__int32 ALIGNED(16) data[4] = 
	{ 
		vgetq_lane_s32(a, 0), 
		vgetq_lane_s32(b, 0), 
		vgetq_lane_s32(a, 1), 
		vgetq_lane_s32(b, 1), 
	};
	return vld1q_s32(data);
}

#if ARM64
__forceinline __m128i_i64 _mm_unpacklo_epi64 (__m128i_i64 a, __m128i_i64 b)
{
	return vcombine_s64(vget_low_s64(a), vget_low_s64(b));
}
__forceinline __m128d _mm_unpacklo_pd (__m128d a, __m128d b)
{
	return vcombine_f64(vget_low_f64(a), vget_low_f64(b));
}
#endif

#endif

