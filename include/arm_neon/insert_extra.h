/***************************************************************************************************/
// HEADER_NAME /arm_neon/insert_extra.h
/***************************************************************************************************/
#if ARM

__forceinline __m128i_u8 _mm_insert_epu8 (__m128i_u8 a, __uint8 i, const int imm8)//MSVC BROKEN!!!
{
	// AVOIDS COMPILER ERROR, COMPILER OPTIMIZATION FIXES ANY INEFFICIENCY!!!
	if(imm8 == 0)
		return vsetq_lane_u8 (i, a, 0); 
	if(imm8 == 1)
		return vsetq_lane_u8 (i, a, 1); 
	if(imm8 == 2)
		return vsetq_lane_u8 (i, a, 2); 
	if(imm8 == 3)
		return vsetq_lane_u8 (i, a, 3); 
	if(imm8 == 4)
		return vsetq_lane_u8 (i, a, 4); 
	if(imm8 == 5)
		return vsetq_lane_u8 (i, a, 5); 
	if(imm8 == 6)
		return vsetq_lane_u8 (i, a, 6); 
	if(imm8 == 7)
		return vsetq_lane_u8 (i, a, 7); 
	if(imm8 == 8)
		return vsetq_lane_u8 (i, a, 8); 
	if(imm8 == 9)
		return vsetq_lane_u8 (i, a, 9); 
	if(imm8 == 10)
		return vsetq_lane_u8 (i, a, 10); 
	if(imm8 == 11)
		return vsetq_lane_u8 (i, a, 11); 
	if(imm8 == 12)
		return vsetq_lane_u8 (i, a, 12); 
	if(imm8 == 13)
		return vsetq_lane_u8 (i, a, 13); 
	if(imm8 == 14)
		return vsetq_lane_u8 (i, a, 14); 
	if(imm8 == 15)
		return vsetq_lane_u8 (i, a, 15); 
}


__forceinline __m128i_u16 _mm_insert_epu16(__m128i_u16 a, __uint16 i,const int imm8)
{
	// AVOIDS COMPILER ERROR, COMPILER OPTIMIZATION FIXES ANY INEFFICIENCY!!!
	if(imm8 == 0)
		return vsetq_lane_u16 (i, a, 0); 
	if(imm8 == 1)
		return vsetq_lane_u16 (i, a, 1); 
	if(imm8 == 2)
		return vsetq_lane_u16 (i, a, 2); 
	if(imm8 == 3)
		return vsetq_lane_u16 (i, a, 3); 
	if(imm8 == 4)
		return vsetq_lane_u16 (i, a, 4); 
	if(imm8 == 5)
		return vsetq_lane_u16 (i, a, 5); 
	if(imm8 == 6)
		return vsetq_lane_u16 (i, a, 6); 
	if(imm8 == 7)
		return vsetq_lane_u16 (i, a, 7); 
}

//__m128i_u32 _mm_insert_epu32 (__m128i_u32 a, int i, const int imm8)
__forceinline __m128i_u32 _mm_insert_epu32 (__m128i_u32 a, __uint32 i, const int imm8)
{
	// AVOIDS COMPILER ERROR, COMPILER OPTIMIZATION FIXES ANY INEFFICIENCY!!!
	if(imm8 == 0)
		return vsetq_lane_u32 (i, a, 0); 
	if(imm8 == 1)
		return vsetq_lane_u32 (i, a, 1); 
	if(imm8 == 2)
		return vsetq_lane_u32 (i, a, 2); 
	if(imm8 == 3)
		return vsetq_lane_u32 (i, a, 3); 
}
#if ARM64
__forceinline __m128i_u64 _mm_insert_epu64 (__m128i_u64 a, __uint64 i, const int imm8)
{
	// AVOIDS COMPILER ERROR, COMPILER OPTIMIZATION FIXES ANY INEFFICIENCY!!!
	if(imm8 == 0)
		return vsetq_lane_u64 (i, a, 0); 
	if(imm8 == 1)
		return vsetq_lane_u64 (i, a, 1); 
}
#endif



#endif

