/***************************************************************************************************/
// HEADER_NAME /arm_neon/stream_extra.h
/***************************************************************************************************/
#if ARM
/***************************************************************************************************/
// Portable Stream
/***************************************************************************************************/
__forceinline void _mm_stream_epu8 (__m128i_u8* mem_addr, __m128i_u8 a)
{
	*mem_addr = a;
}
__forceinline void _mm_stream_epi8 (__m128i_i8* mem_addr, __m128i_i8 a)
{
	*mem_addr = a;
}

__forceinline void _mm_stream_epu16 (__m128i_u16* mem_addr, __m128i_u16 a)
{
	*mem_addr = a;
}
__forceinline void _mm_stream_epi16 (__m128i_i16* mem_addr, __m128i_i16 a)
{
	*mem_addr = a;
}

__forceinline void _mm_stream_epu32 (__m128i_u32* mem_addr, __m128i_u32 a)
{
	*mem_addr = a;
}
__forceinline void _mm_stream_epi32 (__m128i_i32* mem_addr, __m128i_i32 a)
{
	*mem_addr = a;
}
#if ARM64
__forceinline void _mm_stream_epu64 (__m128i_u64* mem_addr, __m128i_u64 a)
{
	*mem_addr = a;
}
__forceinline void _mm_stream_epi64 (__m128i_i64* mem_addr, __m128i_i64 a)
{
	*mem_addr = a;
}
#endif

/***************************************************************************************************/
// Portable Stream Integer
/***************************************************************************************************/

__forceinline void _mm_stream_su32 (__uint32* mem_addr, __uint32 a)
{
	*mem_addr = a;
}
// void _mm_stream_si32 (int* mem_addr, int a) //IMPLEMENTED BY SSE2

#if ARM64
__forceinline void _mm_stream_su64 (__uint64* mem_addr, __uint64 a)
{
	*mem_addr = a;
}
// void _mm_stream_si64 (__int64* mem_addr, __int64 a) //IMPLEMENTED BY SSE2
#endif

/***************************************************************************************************/
// Portable Stream load
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_stream_load_epu8 (const __m128i_u8* mem_addr)
{
	__m128i_u8 ret = *mem_addr;
	return ret;
}
__forceinline __m128i_i8 _mm_stream_load_epi8 (const __m128i_i8* mem_addr)
{
	__m128i_i8 ret = *mem_addr;
	return ret;
}
__forceinline __m128i_u16 _mm_stream_load_epu16 (const __m128i_u16* mem_addr)
{
	__m128i_u16 ret = *mem_addr;
	return ret;
}
__forceinline __m128i_i16 _mm_stream_load_epi16 (const __m128i_i16* mem_addr)
{
	__m128i_i16 ret = *mem_addr;
	return ret;
}
__forceinline __m128i_u32 _mm_stream_load_epu32 (const __m128i_u32* mem_addr)
{
	__m128i_u32 ret = *mem_addr;
	return ret;
}
__forceinline __m128i_i32 _mm_stream_load_epi32 (const __m128i_i32* mem_addr)
{
	__m128i_i32 ret = *mem_addr;
	return ret;
}
#if ARM64
__forceinline __m1264i_u64 _mm_stream_load_epu64 (const __m1264i_u64* mem_addr)
{
	__m1264i_u64 ret = *mem_addr;
	return ret;
}
__forceinline __m1264i_i64 _mm_stream_load_epi64 (const __m1264i_i64* mem_addr)
{
	__m1264i_i64 ret = *mem_addr;
	return ret;
}
#endif

#endif

