/***************************************************************************************************/
// HEADER_NAME /arm_neon/cvt_extra.h
/***************************************************************************************************/
#if ARM

/***************************************************************************************************/
// _mm_cvtsi128_xxxx
/***************************************************************************************************/

__forceinline __int8 _mm_cvtsi128_si8(__m128i_i8 a){
	return vgetq_lane_s8(a, 0);
}
__forceinline __int16 _mm_cvtsi128_si16(__m128i_i16 a) {
	return vgetq_lane_s16(a, 0);
}
/*
IMPLEMNTED BY SSE2
int _mm_cvtsi128_si32 (__m128i_i32 a)
__int64 _mm_cvtsi128_si64 (__m128i_i64 a)
__int64 _mm_cvtsi128_si64x (__m128i_64 a)
*/
/***************************************************************************************************/
// CVT For API Consistency
/***************************************************************************************************/
#if ARM64
__forceinline __m128i_i64 _mm_cvtsi64x_si128(__int64 a)//SSE2
{
	return vsetq_lane_s64(a, vdupq_n_s64(0), 0);
}

__forceinline __int64 _mm_cvtsi128_si64x (__m128i_i64 a)//SSE2
{
	return vgetq_lane_s64(a, 0);
}
#endif
/***************************************************************************************************/
// EXTRACT
/***************************************************************************************************/
__forceinline __uint8 _mm_cvtepu8_su8(__m128i_u8 a){
	return vgetq_lane_u8(a, 0);
}
__forceinline __int8 _mm_cvtepi8_si8(__m128i_i8 a){
	return vgetq_lane_s8(a, 0);
}

__forceinline __uint16 _mm_cvtepu16_su16(__m128i_u16 a){
	return vgetq_lane_u16(a, 0);
}
__forceinline __int16 _mm_cvtepi16_si16(__m128i_i16 a){
	return vgetq_lane_s16(a, 0);
}

__forceinline __uint32 _mm_cvtepu32_su32 (__m128i_u32 a){
	return vgetq_lane_u32(a, 0);
}
__forceinline __int32 _mm_cvtepi32_si32 (__m128i_i32 a){
	return vgetq_lane_s32(a, 0);
}


#if ARM64
__forceinline __uint64 _mm_cvtepu64_su64 (__m128i_u64 a){
	return vgetq_lane_u64(a, 0);
}
#define _mm_cvtepu64_su64x _mm_cvtepu64_su64

__forceinline __int64 _mm_cvtepi64_si64 (__m128i_i64 a){
	return vgetq_lane_s64(a, 0);
}
#define _mm_cvtepi64_si64x _mm_cvtepi64_si64
#endif



/***************************************************************************************************/
// SET
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cvtsu32_epu8 (__uint8 a){
	return vsetq_lane_u8(a, vdupq_n_u8(0), 0);
}
__forceinline __m128i_i8 _mm_cvtsi32_epi8 (__int8 a){
	return vsetq_lane_s8(a, vdupq_n_s8(0), 0);
}

__forceinline __m128i_u16 _mm_cvtsu32_epu16 (__uint16 a){
	return vsetq_lane_u16(a, vdupq_n_u16(0), 0);
}
__forceinline __m128i_i16 _mm_cvtsi32_epi16 (__int16 a){
	return vsetq_lane_s16(a, vdupq_n_s16(0), 0);
}

__forceinline __m128i_u32 _mm_cvtsu32_epu32 (__uint32 a)
{
	return vsetq_lane_u32(a, vdupq_n_u32(0), 0);
}
__forceinline __m128i_i32 _mm_cvtsi32_epi32 (__int32 a)
{
	return vsetq_lane_s32(a, vdupq_n_s32(0), 0);
}
#if ARM64
__forceinline __m128i_u64 _mm_cvtsu64_epu64 (__uint64 a)
{
	return vsetq_lane_u64(a, vdupq_n_u64(0), 0);
}
#define _mm_cvtsu64x_epu64 _mm_cvtsu64_epu64

__forceinline __m128i_i64 _mm_cvtsi64_epi64 (__int64 a)
{
	return vsetq_lane_s64(a, vdupq_n_s64(0), 0);
}
#define _mm_cvtsi64x_epi64 _mm_cvtsi64_epi64
#endif

/***************************************************************************************************/
//AVX CVT
/***************************************************************************************************/
#if ARM64
__forceinline __m128i_i64 _mm_cvtpd_epi64(__m128d a){
	return vcvtnq_s64_f64(a);
}

__forceinline __m128i_u64 _mm_cvtpd_epu64(__m128d a){
	return vcvtnq_u64_f64(a);
}

#endif


#endif

