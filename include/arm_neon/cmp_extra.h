/***************************************************************************************************/
// HEADER_NAME /arm_neon/cmp_extra.h
/***************************************************************************************************/
#if ARM
/***************************************************************************************************/
// Unsigned
/***************************************************************************************************/


/***************************************************************************************************/
// EQ
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cmpeq_epu8(__m128i_u8 a, __m128i_u8 b)
{
	return vceqq_u8(a,b);
}
__forceinline __m128i_u16 _mm_cmpeq_epu16(__m128i_u16 a, __m128i_u16 b)
{
	return vceqq_u16(a,b);
}
__forceinline __m128i_u32 _mm_cmpeq_epu32(__m128i_u32 a, __m128i_u32 b)
{
	return vceqq_u32(a,b);
}
#if ARM64
__forceinline __m128i_u64 _mm_cmpeq_epu64(__m128i_u64 a, __m128i_u64 b)
{
	return vceqq_u64(a,b);
}
#endif
/***************************************************************************************************/
// NE
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cmpneq_epu8(__m128i_u8 a, __m128i_u8 b)
{
	return vmvnq_u8(vceqq_u8(a, b));
}
__forceinline __m128i_u16 _mm_cmpneq_epu16(__m128i_u16 a, __m128i_u16 b)
{
	return vmvnq_u16(vceqq_u16(a, b));
}
__forceinline __m128i_u32 _mm_cmpneq_epu32(__m128i_u32 a, __m128i_u32 b)
{
	return vmvnq_u32(vceqq_u32(a, b));
}
#if ARM64
__forceinline __m128i_u64 _mm_cmpneq_epu64(__m128i_u64 a, __m128i_u64 b)
{
	return veorq_u64(vceqq_u64(a, b), vceqq_u64( vdupq_n_u64(0),vdupq_n_u64(0) ) );
}
#endif


/***************************************************************************************************/
// GE
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cmpge_epu8(__m128i_u8 a, __m128i_u8 b)
{
	return vcgeq_u8(a,b);
}
__forceinline __m128i_u16 _mm_cmpge_epu16(__m128i_u16 a, __m128i_u16 b)
{
	return vcgeq_u16(a,b);
}
__forceinline __m128i_u32 _mm_cmpge_epu32(__m128i_u32 a, __m128i_u32 b)
{
	return vcgeq_u32(a,b);
}
#if ARM64
__forceinline __m128i_u64 _mm_cmpge_epu64(__m128i_u64 a, __m128i_u64 b)
{
	return vcgeq_u64(a,b);
}
#endif
/***************************************************************************************************/
// NGE
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cmpnge_epu8(__m128i_u8 a, __m128i_u8 b)
{
	return vmvnq_u8(vcgeq_u8(a, b));
}
__forceinline __m128i_u16 _mm_cmpnge_epu16(__m128i_u16 a, __m128i_u16 b)
{
	return vmvnq_u16(vcgeq_u16(a, b));
}
__forceinline __m128i_u32 _mm_cmpnge_epu32(__m128i_u32 a, __m128i_u32 b)
{
	return vmvnq_u32(vcgeq_u32(a, b));
}
#if ARM64
__forceinline __m128i_u64 _mm_cmpnge_epu64(__m128i_u64 a, __m128i_u64 b)
{
	return veorq_u64(vcgeq_u64(a, b), vceqq_u64( vdupq_n_u64(0),vdupq_n_u64(0) ) );
}
#endif

/***************************************************************************************************/
// GT
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cmpgt_epu8(__m128i_u8 a, __m128i_u8 b)
{
	return vcgtq_u8(a,b);
}
__forceinline __m128i_u16 _mm_cmpgt_epu16(__m128i_u16 a, __m128i_u16 b)
{
	return vcgtq_u16(a,b);
}
__forceinline __m128i_u32 _mm_cmpgt_epu32(__m128i_u32 a, __m128i_u32 b)
{
	return vcgtq_u32(a,b);
}
#if ARM64
__forceinline __m128i_u64 _mm_cmpgt_epu64(__m128i_u64 a, __m128i_u64 b)
{
	return vcgtq_u64(a,b);
}
#endif
/***************************************************************************************************/
// NGT
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cmpngt_epu8(__m128i_u8 a, __m128i_u8 b)
{
	return vmvnq_u8(vcgtq_u8(a, b));
}
__forceinline __m128i_u16 _mm_cmpngt_epu16(__m128i_u16 a, __m128i_u16 b)
{
	return vmvnq_u16(vcgtq_u16(a, b));
}
__forceinline __m128i_u32 _mm_cmpngt_epu32(__m128i_u32 a, __m128i_u32 b)
{
	return vmvnq_u32(vcgtq_u32(a, b));
}
#if ARM64
__forceinline __m128i_u64 _mm_cmpngt_epu64(__m128i_u64 a, __m128i_u64 b)
{
	return veorq_u64(vcgtq_u64(a, b), vceqq_u64( vdupq_n_u64(0),vdupq_n_u64(0) ) );
}
#endif

/***************************************************************************************************/
// LE
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cmple_epu8(__m128i_u8 a, __m128i_u8 b) {
	return vcleq_u8(a,b);
}
__forceinline __m128i_u16 _mm_cmple_epu16(__m128i_u16 a, __m128i_u16 b) {
	return vcleq_u16(a,b);
}
__forceinline __m128i_u32 _mm_cmple_epu32(__m128i_u32 a, __m128i_u32 b) {
	return vcleq_u32(a,b);
}
#if ARM64
__forceinline __m128i_u64 _mm_cmple_epu64(__m128i_u64 a, __m128i_u64 b) {
	return vcleq_u64(a, b);
}
#endif
/***************************************************************************************************/
// NLE
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cmpnle_epu8(__m128i_u8 a, __m128i_u8 b) {
	return vmvnq_u8(vcleq_u8(a, b));
}
__forceinline __m128i_u16 _mm_cmpnle_epu16(__m128i_u16 a, __m128i_u16 b) {
	return vmvnq_u16(vcleq_u16(a, b));
}
__forceinline __m128i_u32 _mm_cmpnle_epu32(__m128i_u32 a, __m128i_u32 b) {
	return vmvnq_u32(vcleq_u32(a, b));
}
#if ARM64
__forceinline __m128i_u64 _mm_cmpnle_epu64(__m128i_u64 a, __m128i_u64 b) {
	return veorq_u64(vcleq_u64(a, b), vceqq_u64( vdupq_n_u64(0),vdupq_n_u64(0) ) );
}
#endif

/***************************************************************************************************/
// LT
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cmplt_epu8(__m128i_u8 a, __m128i_u8 b) {
	return vcltq_u8(a,b);
}
__forceinline __m128i_u16 _mm_cmplt_epu16(__m128i_u16 a, __m128i_u16 b) {
	return vcltq_u16(a,b);
}
__forceinline __m128i_u32 _mm_cmplt_epu32(__m128i_u32 a, __m128i_u32 b) {
	return vcltq_u32(a,b);
}
#if ARM64
__forceinline __m128i_u64 _mm_cmplt_epu64(__m128i_u64 a, __m128i_u64 b) {
	return vcltq_u64(a,b);
}
#endif
/***************************************************************************************************/
// NLT
/***************************************************************************************************/
__forceinline __m128i_u8 _mm_cmpnlt_epu8(__m128i_u8 a, __m128i_u8 b) {
	return vmvnq_u8(vcltq_u8(a, b));
}
__forceinline __m128i_u16 _mm_cmpnlt_epu16(__m128i_u16 a, __m128i_u16 b) {
	return vmvnq_u16(vcltq_u16(a, b));
}
__forceinline __m128i_u32 _mm_cmpnlt_epu32(__m128i_u32 a, __m128i_u32 b) {
	return vmvnq_u32(vcltq_u32(a, b));
}
#if ARM64
__forceinline __m128i_u64 _mm_cmpnlt_epu64(__m128i_u64 a, __m128i_u64 b) {
	return veorq_u64(vcltq_u64(a, b), vceqq_u64( vdupq_n_u64(0),vdupq_n_u64(0) ) );
}
#endif


/***************************************************************************************************/
// Signed
/***************************************************************************************************/

/***************************************************************************************************/
// EQ
/***************************************************************************************************/
// __m128i_i8 _mm_cmpeq_epi8(__m128i_i8 a, __m128i_i8 b) //IMPEMENTED BY SSE2
// __m128i_i16 _mm_cmpeq_epi16(__m128i_i16 a, __m128i_i16 b) //IMPEMENTED BY SSE2
// __m128i_i32 _mm_cmpeq_epi32(__m128i_i32 a, __m128i_i32 b)//IMPEMENTED BY SSE2
// __m128i_i64 _mm_cmpeq_epi64(__m128i_i64 a, __m128i_i64 b)//IMPEMENTED BY SSE4.1

/***************************************************************************************************/
// NE
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_cmpneq_epi8(__m128i_i8 a, __m128i_i8 b)
{
	return vmvnq_s8(vreinterpretq_s8_u8(vceqq_s8(a, b)));
}
__forceinline __m128i_i16 _mm_cmpneq_epi16(__m128i_i16 a, __m128i_i16 b)
{
	return vmvnq_s16(vreinterpretq_s16_u16(vceqq_s16(a, b)));
}
__forceinline __m128i_i32 _mm_cmpneq_epi32(__m128i_i32 a, __m128i_i32 b)
{
	return vmvnq_s32(vreinterpretq_s32_u32(vceqq_s32(a, b)));
}
#if ARM64
__forceinline __m128i_i64 _mm_cmpneq_epi64(__m128i_i64 a, __m128i_i64 b)
{
	return veorq_s64(vreinterpretq_s64_u64(vceqq_s64(a, b)), vreinterpretq_s64_u64(vceqq_s64( vdupq_n_s64(0),vdupq_n_s64(0) )) );
}
#endif


/***************************************************************************************************/
// GE
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_cmpge_epi8(__m128i_i8 a, __m128i_i8 b)
{
	return vreinterpretq_s8_u8(vcgeq_s8(a, b));
}
__forceinline __m128i_i16 _mm_cmpge_epi16(__m128i_i16 a, __m128i_i16 b)
{
	return vreinterpretq_s16_u16(vcgeq_s16(a, b));
}
__forceinline __m128i_i32 _mm_cmpge_epi32(__m128i_i32 a, __m128i_i32 b)
{
	return vreinterpretq_s32_u32(vcgeq_s32(a, b));
}
#if ARM64
__forceinline __m128i_i64 _mm_cmpge_epi64(__m128i_i64 a, __m128i_i64 b)
{
	return vreinterpretq_s64_u64(vcgeq_s64(a, b));
}
#endif
/***************************************************************************************************/
// NGE
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_cmpnge_epi8(__m128i_i8 a, __m128i_i8 b)
{
	return vmvnq_s8(vreinterpretq_s8_u8(vcgeq_s8(a, b)));
}
__forceinline __m128i_i16 _mm_cmpnge_epi16(__m128i_i16 a, __m128i_i16 b)
{
	return vmvnq_s16(vreinterpretq_s16_u16(vcgeq_s16(a, b)));
}
__forceinline __m128i_i32 _mm_cmpnge_epi32(__m128i_i32 a, __m128i_i32 b)
{
	return vmvnq_s32(vreinterpretq_s32_u32(vcgeq_s32(a, b)));
}
#if ARM64
__forceinline __m128i_i64 _mm_cmpnge_epi64(__m128i_i64 a, __m128i_i64 b)
{
	return veorq_s64(vreinterpretq_s64_u64(vcgeq_s64(a, b)), vreinterpretq_s64_u64(vceqq_s64( vdupq_n_s64(0),vdupq_n_s64(0) )) );
}
#endif

/***************************************************************************************************/
// GT
/***************************************************************************************************/
// __m128i_i8 _mm_cmpgt_epi8(__m128i_i8 a, __m128i_i8 b)//IMPEMENTED BY SSE2
// __m128i_i16 _mm_cmpgt_epi16(__m128i_i16 a, __m128i_i16 b)//IMPEMENTED BY SSE2
// __m128i_i32 _mm_cmpgt_epi32(__m128i_i32 a, __m128i_i32 b)//IMPEMENTED BY SSE2
// __m128i_i64 _mm_cmpgt_epi64(__m128i_i64 a, __m128i_i64 b)//IMPEMENTED BY SSE4.1

/***************************************************************************************************/
// NGT
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_cmpngt_epi8(__m128i_i8 a, __m128i_i8 b)
{
	return vmvnq_s8(vreinterpretq_s8_u8(vcgtq_s8(a, b)));
}
__forceinline __m128i_i16 _mm_cmpngt_epi16(__m128i_i16 a, __m128i_i16 b)
{
	return vmvnq_s16(vreinterpretq_s16_u16(vcgtq_s16(a, b)));
}
__forceinline __m128i_i32 _mm_cmpngt_epi32(__m128i_i32 a, __m128i_i32 b)
{
	return vmvnq_s32(vreinterpretq_s32_u32(vcgtq_s32(a, b)));
}
#if ARM64
__forceinline __m128i_i64 _mm_cmpngt_epi64(__m128i_i64 a, __m128i_i64 b)
{
	return veorq_s64(vreinterpretq_s64_u64(vcgtq_s64(a, b)), vreinterpretq_s64_u64(vceqq_s64( vdupq_n_s64(0),vdupq_n_s64(0) )) );
}
#endif

/***************************************************************************************************/
// LE
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_cmple_epi8(__m128i_i8 a, __m128i_i8 b) {
	return vreinterpretq_s8_u8(vcleq_s8(a, b));
}
__forceinline __m128i_i16 _mm_cmple_epi16(__m128i_i16 a, __m128i_i16 b) {
	return vreinterpretq_s16_u16(vcleq_s16(a, b));
}
__forceinline __m128i_i32 _mm_cmple_epi32(__m128i_i32 a, __m128i_i32 b) {
	return vreinterpretq_s32_u32(vcleq_s32(a, b));
}
#if ARM64
__forceinline __m128i_i64 _mm_cmple_epi64(__m128i_i64 a, __m128i_i64 b) {
	return vreinterpretq_s64_u64(vcleq_s64(a, b));
}
#endif
/***************************************************************************************************/
// NLE
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_cmpnle_epi8(__m128i_i8 a, __m128i_i8 b) {
	return vmvnq_s8(vreinterpretq_s8_u8(vcleq_s8(a, b)));
}
__forceinline __m128i_i16 _mm_cmpnle_epi16(__m128i_i16 a, __m128i_i16 b) {
	return vmvnq_s16(vreinterpretq_s16_u16(vcleq_s16(a, b)));
}
__forceinline __m128i_i32 _mm_cmpnle_epi32(__m128i_i32 a, __m128i_i32 b) {
	return vmvnq_s32(vreinterpretq_s32_u32(vcleq_s32(a, b)));
}
#if ARM64
__forceinline __m128i_i64 _mm_cmpnle_epi64(__m128i_i64 a, __m128i_i64 b) {
	return veorq_s64(vreinterpretq_s64_u64(vcleq_s64(a, b)), vreinterpretq_s64_u64(vceqq_s64( vdupq_n_s64(0),vdupq_n_s64(0) )) );
}
#endif

/***************************************************************************************************/
// LT
/***************************************************************************************************/
// __m128i_i8 _mm_cmplt_epi8(__m128i_i8 a, __m128i_i8 b) //IMPEMENTED BY SSE2
// __m128i_i16 _mm_cmplt_epi16(__m128i_i16 a, __m128i_i16 b) //IMPEMENTED BY SSE2
// __m128i_i32 _mm_cmplt_epi32(__m128i_i32 a, __m128i_i32 b) //IMPEMENTED BY SSE2
#if ARM64
__forceinline __m128i_i64 _mm_cmplt_epi64(__m128i_i64 a, __m128i_i64 b)
{
	return vreinterpretq_s64_u64(vcltq_s64(a, b));
}
#endif
/***************************************************************************************************/
// NLT
/***************************************************************************************************/
__forceinline __m128i_i8 _mm_cmpnlt_epi8(__m128i_i8 a, __m128i_i8 b) {
	return vmvnq_s8(vreinterpretq_s8_u8(vcltq_s8(a, b)));

}
__forceinline __m128i_i16 _mm_cmpnlt_epi16(__m128i_i16 a, __m128i_i16 b) {
	return vmvnq_s16(vreinterpretq_s16_u16(vcltq_s16(a, b)));
}
__forceinline __m128i_i32 _mm_cmpnlt_epi32(__m128i_i32 a, __m128i_i32 b) {
	return vmvnq_s32(vreinterpretq_s32_u32(vcltq_s32(a, b)));
}
#if ARM64
__forceinline __m128i_i64 _mm_cmpnlt_epi64(__m128i_i64 a, __m128i_i64 b) {
	return veorq_s64(vreinterpretq_s64_u64(vcltq_s64(a, b)), vreinterpretq_s64_u64(vceqq_s64( vdupq_n_s64(0),vdupq_n_s64(0) )) );
}
#endif

#endif

