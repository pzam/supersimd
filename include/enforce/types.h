/***************************************************************************************************/
// HEADER_NAME /enforce/types.h
/***************************************************************************************************/

/*

int					4	signed										–2,147,483,648 to 2,147,483,647
unsigned int		4	unsigned									0 to 4,294,967,295
__int8				1	char										–128 to 127
unsigned __int8		1	unsigned char								0 to 255
__int16				2	short, short int, signed short int			–32,768 to 32,767
unsigned __int16	2	unsigned short, unsigned short int			0 to 65,535
__int32				4	signed, signed int, int						–2,147,483,648 to 2,147,483,647
unsigned __int32	4	unsigned, unsigned int						0 to 4,294,967,295
__int64				8	long long, signed long long					–9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
unsigned __int64	8	unsigned long long							0 to 18,446,744,073,709,551,615
bool				1	none										false or true
char				1	none										–128 to 127 by default

0 to 255 when compiled by using /J
signed char			1	none										–128 to 127
unsigned char		1	none										0 to 255
short				2	short int, signed short int					–32,768 to 32,767
unsigned short		2	unsigned short int							0 to 65,535
long				4	long int, signed long int					–2,147,483,648 to 2,147,483,647
unsigned long		4	unsigned long int							0 to 4,294,967,295
long long			8	none (but equivalent to __int64)			–9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
unsigned long long	8	none (but equivalent to unsigned __int64)	0 to 18,446,744,073,709,551,615

enum			varies	none										See Remarks later in this article
float				4	none										3.4E +/- 38 (7 digits)
double				8	none										1.7E +/- 308 (15 digits)

long double		none												Same as double
wchar_t				2	__wchar_t									0 to 65,535

*/
//IEE754
#define IEE754_32BIT_BIAS 127
#define IEE754_32BIT_MANTISSA 23

#define IEE754_64BIT_BIAS 1023
#define IEE754_64BIT_MANTISSA 52


//Allocate byte size
#if _MSC_VER || __INTEL_COMPILER
	#define ALIGNED(bytes) __declspec(align( bytes ))
#else
	#define ALIGNED(bytes) __attribute__ (( aligned (bytes) ))
#endif

#if INTEL


#ifdef _INC_CRTDEFS
#ifdef __clang__
	#define _INC_CRTDEFS_DEF 0
#else
	#define _INC_CRTDEFS_DEF 1
#endif
#endif

#if __clang__
	#undef __uint8
	#undef __uint16
	#undef __uint32
	#undef __uint64
	#undef __int8
	#undef __int16
	#undef __int32
	#undef __int64
#endif

#if _MSC_VER || __INTEL_COMPILER
//Extended from Microsoft standard
typedef unsigned char			__uint8;
typedef unsigned short int		__uint16;
typedef unsigned int			__uint32;
typedef unsigned long long		__uint64;
#endif

#if !_MSC_VER && !__INTEL_COMPILER && !_INC_CRTDEFS_DEF
typedef char					__int8;
typedef short int				__int16;
typedef int						__int32;
typedef long long				__int64;
#endif

#ifdef __GNUC__ || __clang__
typedef u_int8_t				__uint8;
typedef u_int16_t				__uint16;
typedef u_int32_t				__uint32;
typedef u_int64_t				__uint64;
#endif

#endif


#if ARM
#if !_MSC_VER
typedef int8_t					__int8;
typedef int16_t					__int16;
typedef int32_t					__int32;

#if ARM64
typedef int64_t					__int64;
#endif

#endif

//Extended from Microsoft standard
typedef uint8_t					__uint8;
typedef uint16_t				__uint16;
typedef uint32_t				__uint32;

#if ARM64
typedef uint64_t				__uint64;
#endif

#endif

