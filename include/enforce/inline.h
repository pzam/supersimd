/***************************************************************************************************/
// HEADER_NAME /enforce/inline.h
/***************************************************************************************************/

//Inline
#if INTEL
#if __GNUC__ || __clang__
	#define __forceinline __attribute__((always_inline))
#endif
#endif

#if ARM
#if __GNUC__ || __clang__
	#define __forceinline inline
#endif
#endif

#if _MSC_VER
	#pragma inline_recursion(on)
#endif

