#ifndef __SUPERSIMD_H
#define __SUPERSIMD_H

/***************************************************************************************************/
// HEADER_NAME /supersimd.h
/***************************************************************************************************/

/************************************************/
// Information 
/************************************************/
#include <version/info.h>
/************************************************/
// Architecture
/************************************************/
#include <enforce/architecture.h>

/************************************************/
// X86_X64
/************************************************/
#if INTEL
	
	#include <mmintrin.h>				//MMX
	#include <xmmintrin.h>				//SSE
	#include <emmintrin.h>				//SSE2
	#if !LEGACY_SSE2
	#include <pmmintrin.h>				//SSE3
	#include <tmmintrin.h>				//SSSE3
	#include <smmintrin.h>				//SSE4.1
	#include <nmmintrin.h>				//SSE4.2
	#include <immintrin.h>				//AVX, AVX2, FMA, AVX-512
	#endif
	#if XOP_NATIVE
	#include <ammintrin.h>				//XOP NATIVE
	#endif
	
	#include <portable_types/portable_types.h>	//Improved Notation 
	
#endif

/************************************************/
// ARM
/************************************************/
#if ARM
	#if _MSC_VER
		#include <intrin.h>
	#else
		#include <arm_acle.h>
		#include <arm_neon.h>
	#endif
		#include <portable_types/portable_types.h>	//Improved Notation 
#endif

/************************************************/
// Normalize Compiler Differences 
/************************************************/
#include <enforce/inline.h>
#include <enforce/standards.h>
#include <enforce/types.h>

/************************************************/
// ARM64 NEON From SSE Family
/************************************************/
#if ARM

	#include <arm_neon/cpu.h>

	#include <arm_neon/arm32.h>
	
	#include <arm_neon/mmx.h>
	#include <arm_neon/sse.h>
	#include <arm_neon/sse2.h>
	#include <arm_neon/sse3.h>
	#include <arm_neon/ssse3.h>
	#include <arm_neon/sse4_1.h>
	#include <arm_neon/sse4_2.h>
	#include <arm_neon/fma.h>
	#include <arm_neon/xop.h>

	#include <arm_neon/abs_extra.h>
	#include <arm_neon/addition_extra.h>
	#include <arm_neon/subtraction_extra.h>
	#include <arm_neon/multiplication_extra.h>
	#include <arm_neon/reciprocal_extra.h>
	#include <arm_neon/bitreverse_extra.h>
	#include <arm_neon/bitwise_extra.h>
	#include <arm_neon/blend_extra.h>
	#include <arm_neon/bswap_extra.h>
	#include <arm_neon/cast_extra.h>
	#include <arm_neon/cmp_extra.h>
	#include <arm_neon/cvt_extra.h>

	#include <arm_neon/insert_extra.h>
	#include <arm_neon/load_extra.h>
	#include <arm_neon/minmax_extra.h>
	#include <arm_neon/set_extra.h>
	#include <arm_neon/store_extra.h>
	#include <arm_neon/stream_extra.h>
	#include <arm_neon/test_extra.h>

	//#include <arm_neon/aes.h>
	//#include <arm_neon/sha.h>
	
#endif

/************************************************/
// Extract Data
/************************************************/
#if INTEL
	#include <extract/extract.h>
	#include <extract/avx_extract.h>
	#include <extract/avx512_extract.h>//TODO
#endif

#if ARM
	#include <extract/extract_neon.h>
#endif

/************************************************/
#include <extra/macros.h>
/************************************************/
// OLD CPU SUPPORT
/************************************************/
#if LEGACY_SSE2  && !ARM
	#undef NO_FMA
	#define NO_FMA 1
	
	//MSVC
	#if _MSC_VER
	#pragma message("You must Enable Enhanced Instruction Set to /arch:SSE2 for LEGACY_SSE2 to work on MSVC")
	#endif
	
	//GCC
	#if __GNUC__
	#pragma GCC target("sse,sse2,tune=native")
	#endif
	
	//CLANG
	#if __clang__
	#pragma clang attribute push (__attribute__((target("sse,sse2"))), apply_to=function)
	#endif

	#include <legacy/legacy_sse3.h>
	#include <legacy/legacy_ssse3.h>
	#include <legacy/legacy_sse4_1.h>
	#include <legacy/legacy_sse4_2.h>
#endif
/************************************************/
// NO AVX2
/************************************************/
#if NO_AVX2 && !ARM
	#include <avx2/no_avx2.h>
#endif
/************************************************/
// Cast/Set/Load/Store
/************************************************/
#include <extra/castextra.h>
#include <extra/avx_castextra.h>
#include <extra/avx512_castextra.h>

#include <extra/loadextra.h>
#include <extra/avx_loadextra.h>//TODO
#include <extra/avx512_loadextra.h>//TODO

#include <extra/storeextra.h>
#include <extra/avx_storeextra.h>//TODO
#include <extra/avx512_storeextra.h>//TODO

#include <extra/streamextra.h>
#include <extra/avx_streamextra.h>//TODO
#include <extra/avx2_streamextra.h>//TODO
#include <extra/avx512_streamextra.h>//TODO

#include <extra/setextra.h>
#include <extra/avx_setextra.h>//TODO
#include <extra/avx512_setextra.h>//TODO

#include <extra/insertextra.h>
#include <extra/avx_insertextra.h>
#include <extra/avx512_insertextra.h>//TODO
/************************************************/
// Constants
/************************************************/
#include <constants/constants__m128i.h>
#include <constants/constants__m256i.h>
#include <constants/constants__m128.h>
#include <constants/constants__m256.h>
#include <constants/constants__m128d.h>
#include <constants/constants__m256d.h>
#include <constants/constants__m128hex.h>
#include <constants/constants__m256hex.h>
#include <constants/constants__m128dhex.h>
#include <constants/constants__m256dhex.h>
/************************************************/
// FMA4/MACC
/************************************************/
#if !XOP_NATIVE
	#include <amd/fma4__m128.h>
	#include <amd/fma4__m128d.h>
	#include <amd/fma4__m256.h>
	#include <amd/fma4__m256d.h>
#endif

#if XOP_NATIVE && !ARM
	#include <amd/fma3_to_fma4_m128.h>
	#include <amd/fma3_to_fma4_m128d.h>
	#include <amd/fma3_to_fma4_m256.h>
	#include <amd/fma3_to_fma4_m256d.h>
#endif

/*
	NOTE DEPENDECY ORDER IS FOR EXTRA IMPORTANT!!! 
*/
/************************************************/
// SSE EXTRA
/************************************************/
#include <extra/bitwiseextra.h>
#include <extra/minmaxextra.h>//TODO
#include <extra/blendextra.h>
#include <extra/multiplicationextra.h>
#include <extra/reciprocalextra.h>
#include <extra/subtractionextra.h>//TODO
#include <extra/bswapextra.h>
#include <extra/cvtavx512extra.h>
#include <extra/cvtextra.h>
#include <extra/signextra.h>
#include <extra/clampextra.h>
#include <extra/modextra.h>
#include <extra/absextra.h>
#include <extra/chsextra.h>
#include <extra/testextra.h>
#include <extra/shiftavx512extra.h>//XOP
#include <extra/cmpextra.h>//XOP
#include <extra/additionextra.h>
#include <extra/movemaskextra.h>
#include <extra/cmovextra.h> //XOP
#include <extra/maskextra.h> 
#include <extra/saturationextra.h> 
#include <extra/bitreverseextra.h> 
/************************************************/
// AVX EXTRA
/************************************************/
#include <extra/avx_cmpextra.h>
#include <extra/avx_cvtextra.h>
#include <extra/avx_signextra.h>
#include <extra/avx_clampextra.h>
#include <extra/avx_modextra.h>
#include <extra/avx_absextra.h>
#include <extra/avx_reciprocalextra.h>
#include <extra/avx_chsextra.h>
#include <extra/avx_testextra.h>
#include <extra/avx_maskextra.h>
/************************************************/
// AVX2 EXTRA
/************************************************/
#include <extra/avx2_bitwiseextra.h>
#include <extra/avx2_minmaxextra.h>
#include <extra/avx2_cmpextra.h>
#include <extra/avx2_blendextra.h>
#include <extra/avx2_bswapextra.h>
#include <extra/avx2_saturationextra.h>
#include <extra/avx2_multiplicationextra.h>
#include <extra/avx2_subtractionextra.h>//TODO
#include <extra/avx2_additionextra.h>
#include <extra/avx2_absextra.h>
#include <extra/avx2_chsextra.h>
#include <extra/avx2_cvtavx512extra.h>
#include <extra/avx2_movemaskextra.h>
#include <extra/avx2_cmovextra.h>
#include <extra/avx2_bitreverseextra.h>
#include <extra/avx2_shiftavx512extra.h>
/************************************************/
// NO FMA
/************************************************/
#if NO_FMA
	#include <extra/no_fma.h>
#endif

/************************************************/
// SVML
/************************************************/
#include <svml/division__m128i.h>
#include <svml/division__m256i.h>
#include <svml/round__m128.h>
#include <svml/round__m128d.h>
#include <svml/round__m256.h>
#include <svml/round__m256d.h>
#include <svml/exponentiation__m128.h>
#include <svml/exponentiation__m128d.h>
#include <svml/exponentiation__m256.h>
#include <svml/exponentiation__m256d.h>
#include <svml/trigonometry__m128.h>
#include <svml/trigonometry__m128d.h>
#include <svml/trigonometry__m256.h>
#include <svml/trigonometry__m256d.h>
#include <svml/distribution__m128.h>
#include <svml/distribution__m128d.h>
#include <svml/distribution__m256.h>
#include <svml/distribution__m256d.h>
/************************************************/
// XOP
/************************************************/
#if !XOP_NATIVE || ARM
	#include <amd/xop.h>
#endif
/************************************************/
#endif