/***************************************************************************************************/
// HEADER_NAME /legacy/legacy_sse4_1.h
/***************************************************************************************************/
//NOTE CHECK BLEND ORDER IF BROKEN
#define __SSE4_1__ 1

/* Rounding mode macros. */
#define _MM_FROUND_TO_NEAREST_INT	0x00	//0
#define _MM_FROUND_TO_NEG_INF		0x01	//1
#define _MM_FROUND_TO_POS_INF		0x02	//2
#define _MM_FROUND_TO_ZERO			0x03	//3
#define _MM_FROUND_CUR_DIRECTION	0x04	//4

#define _MM_FROUND_RAISE_EXC		0x00	//0
#define _MM_FROUND_NO_EXC			0x08	//8

#define _MM_FROUND_NINT				0x00	//0
#define _MM_FROUND_FLOOR			0x01	//1
#define _MM_FROUND_CEIL				0x02	//2
#define _MM_FROUND_TRUNC			0x03	//3
#define _MM_FROUND_RINT				0x04	//4
#define _MM_FROUND_NEARBYINT		0x0C	//12


#undef _mm_blend_epi8
#define _mm_blend_epi8 _mm_blend_epi8_impl
__forceinline __m128i _mm_blendv_epi8_impl (__m128i a, __m128i b, __m128i mask)
{
	return _mm_or_si128(_mm_andnot_si128(mask, a), _mm_and_si128(mask, b));
}

#undef _mm_blend_epi16
#define _mm_blend_epi16 _mm_blend_epi16_impl
__forceinline __m128i _mm_blend_epi16_impl (__m128i a, __m128i b, const int imm8)
{

	__uint16 v0 = 0x0;
	__uint16 v1 = 0x0;
	__uint16 v2 = 0x0;
	__uint16 v3 = 0x0;
	__uint16 v4 = 0x0;
	__uint16 v5 = 0x0;
	__uint16 v6 = 0x0;
	__uint16 v7 = 0x0;
	
	if(imm8 & 0x80)
		v7 = 0xFFFF;
	if(imm8 & 0x40)
		v6 = 0xFFFF;
	if(imm8 & 0x20)
		v5 = 0xFFFF;
	if(imm8 & 0x10)
		v4 = 0xFFFF;
	if(imm8 & 0x08)
		v3 = 0xFFFF;
	if(imm8 & 0x04)
		v2 = 0xFFFF;
	if(imm8 & 0x02)
		v1 = 0xFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFF;
	
	__m128i mask = _mm_set_epi16(v7, v6, v5, v4, v3, v2, v1, v0);

	return _mm_or_si128(_mm_andnot_si128(mask, a), _mm_and_si128(mask, b));
}

#undef _mm_blend_ps
#define _mm_blend_ps _mm_blend_ps_impl
__forceinline __m128 _mm_blend_ps_impl (__m128 a, __m128 b, const int imm8)
{
	__uint32 v0 = 0x0;
	__uint32 v1 = 0x0;
	__uint32 v2 = 0x0;
	__uint32 v3 = 0x0;
	
	if(imm8 & 0x08)
		v3 = 0xFFFFFFFF;
	if(imm8 & 0x04)
		v2 = 0xFFFFFFFF;
	if(imm8 & 0x02)
		v1 = 0xFFFFFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFFFFFF;
	
	__m128i mask = _mm_set_epi32(v3, v2, v1, v0);
	
	return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(mask, _mm_castps_si128(a)), _mm_and_si128(mask, _mm_castps_si128(b))));
}

#undef _mm_blend_ps
#define _mm_blend_ps _mm_blend_ps_impl
__forceinline __m128 _mm_blendv_ps_impl (__m128 a, __m128 b, __m128 mask)
{
	return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(_mm_castps_si128(mask), _mm_castps_si128(a)), _mm_and_si128(_mm_castps_si128(mask), _mm_castps_si128(b))));
}

#undef _mm_blend_pd
#define _mm_blend_pd _mm_blend_pd_impl
__forceinline __m128d _mm_blend_pd (__m128d a, __m128d b, const int imm8)
{
	__uint64 v0 = 0x0;
	__uint64 v1 = 0x0;
	
	if(imm8 & 0x02)
		v1 = 0xFFFFFFFFFFFFFFFF;
	if(imm8 & 0x01)
		v0 = 0xFFFFFFFFFFFFFFFF;
	
	__m128i mask = _mm_set_epi64x(v1, v0);
	
	return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(mask, _mm_castpd_si128(a)), _mm_and_si128(mask, _mm_castpd_si128(b))));
}

#undef _mm_blendv_pd
#define _mm_blendv_pd _mm_blendv_pd_impl
__forceinline __m128d _mm_blendv_pd (__m128d a, __m128d b, __m128d mask)
{
	return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(mask), _mm_castpd_si128(a)), _mm_and_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b))));
}

#undef _mm_ceil_pd
#define _mm_ceil_pd _mm_ceil_pd_impl
__forceinline __m128d _mm_ceil_pd_impl (__m128d a)
{

	__m128d cvtstof = _mm_set1_pd(0x0018000000000000);

	__m128d mask = _mm_cmple_pd(a, _mm_setzero_pd());
	__m128d b1 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(a, cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
	__m128d b2 = _mm_add_pd(_mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(_mm_sub_pd(a, _mm_set1_pd(0.0001)), cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof), _mm_set1_pd(1));
	
	return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b1)), _mm_and_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b2))));
}

#undef _mm_ceil_sd
#define _mm_ceil_sd _mm_ceil_pd


#undef _mm_ceil_ps
#define _mm_ceil_ps _mm_ceil_ps_impl
__forceinline __m128 _mm_ceil_ps_impl (__m128 a)
{
	__m128 mask = _mm_cmple_ps(a, _mm_setzero_ps());
	__m128 b1 = _mm_cvtepi32_ps(_mm_cvtps_epi32(a));
	__m128 b2 = _mm_add_ps(_mm_cvtepi32_ps(_mm_cvtps_epi32(_mm_sub_ps(a, _mm_set1_ps(0.0001)))), _mm_set1_ps(1));
	
	return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(_mm_castps_si128(mask), _mm_castps_si128(b1)), _mm_and_si128(_mm_castps_si128(mask), _mm_castps_si128(b2))));
}

#undef _mm_ceil_ss
#define _mm_ceil_ss _mm_ceil_ps




#undef _mm_floor_pd
#define _mm_floor_pd _mm_floor_pd_impl
__forceinline __m128d _mm_floor_pd_impl (__m128d a)
{
	__m128d cvtstof = _mm_set1_pd(0x0018000000000000);
	
	__m128d mask = _mm_cmpgt_pd(a, _mm_setzero_pd());
	__m128d b1 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(a, cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
	__m128d b2 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(_mm_sub_pd(a, _mm_set1_pd(0.9998999999999999)), cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
		
	return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b1)), _mm_and_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b2))));
}

#undef _mm_floor_pd
#define _mm_floor_sd _mm_floor_pd


#undef _mm_floor_ps
#define _mm_floor_ps _mm_floor_ps_impl
__forceinline __m128 _mm_floor_ps_impl (__m128 a)
{
	__m128 mask = _mm_cmpgt_ps(a, _mm_setzero_ps());
	__m128 b1 = _mm_cvtepi32_ps(_mm_cvtps_epi32(a));
	__m128 b2 = _mm_cvtepi32_ps(_mm_cvtps_epi32(_mm_sub_ps(a, _mm_set1_ps(0.9998999))));
	
	return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(_mm_castps_si128(mask), _mm_castps_si128(b1)), _mm_and_si128(_mm_castps_si128(mask), _mm_castps_si128(b2))));

}

#undef _mm_floor_ps
#define _mm_floor_ss _mm_floor_ps

#undef _mm_cmpeq_epi64
#define _mm_cmpeq_epi64 _mm_cmpeq_epi64_impl
__forceinline __m128i _mm_cmpeq_epi64_impl (__m128i a, __m128i b)
{
	// SAME AS 64-BIT SINCE 
	// EQ ONLY COMPARE IF BITS ARE 
	// SET IN THE SAME POSITION.
	return _mm_cmpeq_epi32(a,b);
}


#undef _mm_dp_pd
#define _mm_dp_pd _mm_dp_pd_impl
__forceinline __m128d _mm_dp_pd_impl (__m128d a, __m128d b, const int imm8)//wip
{
	if(imm8 < 0xFF)
	{
		__uint64 s0 = 0x0;
		__uint64 s1 = 0x0;

		
		__uint64 v0 = 0x0;
		__uint64 v1 = 0x0;


		if(imm8 & 0x20)
			s1 = 0xFFFFFFFFFFFFFFFF;
		if(imm8 & 0x10)
			s0 = 0xFFFFFFFFFFFFFFFF;

		if(imm8 & 0x02)
			v1 = 0xFFFFFFFFFFFFFFFF;
		if(imm8 & 0x01)
			v0 = 0xFFFFFFFFFFFFFFFF;

		__m128i mmul = _mm_set_epi64x(s1,s0);
		__m128i sel = _mm_set_epi64x(v1,v0);
		
		__m128d t;
		
		a = _mm_mul_pd(a,b);
		a = _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(mmul, _mm_castpd_si128(a)), _mm_and_si128(mmul, _mm_castpd_si128(_mm_setzero_pd()))));
		
		
		
		t = _mm_shuffle_pd( a , a, _MM_SHUFFLE2(0, 1) );
		a = _mm_add_pd( a, t );				 //(1, 0)

		a = _mm_set1_pd(_mm_cvtsd_f64(a));
		
		
		return  _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(sel, _mm_castpd_si128(a)), _mm_and_si128(sel, _mm_castpd_si128(_mm_setzero_pd()))));

	}
	else
	{
		__m128d t;
		
		a = _mm_mul_pd(a,b);
		
		
		t = _mm_shuffle_pd( a , a, _MM_SHUFFLE2(0, 1) );
		a = _mm_add_pd( a, t );				//(1, 0)

		return _mm_set1_pd(_mm_cvtsd_f64(a));
	}
}

#undef _mm_dp_ps
#define _mm_dp_ps _mm_dp_ps_impl
__forceinline __m128 _mm_dp_ps_impl (__m128 a, __m128 b, const int imm8)//wip
{
	if(imm8 < 0xFF)
	{
		__uint32 s0 = 0x0;
		__uint32 s1 = 0x0;
		__uint32 s2 = 0x0;
		__uint32 s3 = 0x0;
		
		__uint32 v0 = 0x0;
		__uint32 v1 = 0x0;
		__uint32 v2 = 0x0;
		__uint32 v3 = 0x0;
		
		if(imm8 & 0x80)
			s3 = 0xFFFFFFFF;
		if(imm8 & 0x40)
			s2 = 0xFFFFFFFF;
		if(imm8 & 0x20)
			s1 = 0xFFFFFFFF;
		if(imm8 & 0x10)
			s0 = 0xFFFFFFFF;
		
		if(imm8 & 0x08)
			v3 = 0xFFFFFFFF;
		if(imm8 & 0x04)
			v2 = 0xFFFFFFFF;
		if(imm8 & 0x02)
			v1 = 0xFFFFFFFF;
		if(imm8 & 0x01)
			v0 = 0xFFFFFFFF;

		__m128i mmul = _mm_set_epi32(s3,s2,s1,s0);
		__m128i sel = _mm_set_epi32(v3,v2,v1,v0);
		
		__m128 t;
		
		a = _mm_mul_ps(a,b);
		a = _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(mmul, _mm_castps_si128(a)), _mm_and_si128(mmul, _mm_castps_si128(_mm_setzero_ps()))));
		
		
		
		t = _mm_shuffle_ps( a , a, _MM_SHUFFLE(2, 3, 0, 1) );
		a = _mm_add_ps( a, t );				   //(3, 2, 1, 0)
		
		t = _mm_shuffle_ps( t , t, _MM_SHUFFLE(0, 1, 2, 3) );
		a = _mm_add_ps( a, t );				   //(3, 2, 1, 0)
		
		a = _mm_set1_ps(_mm_cvtss_f32(a));
		
		
		return  _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(sel, _mm_castps_si128(a)), _mm_and_si128(sel, _mm_castps_si128(_mm_setzero_ps()))));

	}
	else
	{
		__m128 t;
		
		a = _mm_mul_ps(a,b);
		
		
		t = _mm_shuffle_ps( a , a, _MM_SHUFFLE(2, 3, 0, 1) );
		a = _mm_add_ps( a, t );				   //(3, 2, 1, 0)
		
		t = _mm_shuffle_ps( t , t, _MM_SHUFFLE(0, 1, 2, 3) );
		a = _mm_add_ps( a, t );				   //(3, 2, 1, 0)
		
		return _mm_set1_ps(_mm_cvtss_f32(a));
	}
}

#undef _mm_test_all_ones
#define _mm_test_all_ones _mm_test_all_ones_impl
__forceinline int _mm_test_all_ones_impl (__m128i a)
{
	return _mm_movemask_epi8( _mm_and_si128(_mm_cmpeq_epi8(a,_mm_setzero_si128()), _mm_set1_epi8(0x80)) );
}

#undef _mm_test_all_zeros
#define _mm_test_all_zeros _mm_test_all_zeros_impl
__forceinline int _mm_test_all_zeros_impl (__m128i a, __m128i mask)
{
	return _mm_movemask_epi8(_mm_and_si128(_mm_cmpeq_epi8(_mm_and_si128(a,mask), _mm_andnot_si128(a,a)) , _mm_set1_epi8(0x80)));
}

#undef _mm_test_mix_ones_zeros
#define _mm_test_mix_ones_zeros _mm_test_mix_ones_zeros_impl
__forceinline int _mm_test_mix_ones_zeros_impl (__m128i a, __m128i mask)
{
	int zf, cf;
	cf = _mm_movemask_epi8( _mm_and_si128(_mm_cmpeq_epi8(a,_mm_setzero_si128()), _mm_set1_epi8(0x80)) );
	zf = _mm_movemask_epi8(_mm_and_si128(_mm_cmpeq_epi8(_mm_and_si128(a,mask), _mm_andnot_si128(a,a)) , _mm_set1_epi8(0x80)));
	
	if(zf == 0 && cf == 0)
		return 1;
	else
		return 0;
	
}

#undef _mm_testc_si128
#define _mm_testc_si128 _mm_testc_si128_impl
__forceinline int _mm_testc_si128_impl (__m128i a, __m128i b)
{
	return _mm_movemask_epi8( _mm_and_si128( _mm_cmpeq_epi8(a,b), _mm_set1_epi8(0x80)) );
}

#undef _mm_testnzc_si128
#define _mm_testnzc_si128 _mm_test_mix_ones_zeros_impl

#undef _mm_testz_si128
#define _mm_testz_si128 _mm_test_all_zeros_impl




#undef _mm_max_epi8
#define _mm_max_epi8 _mm_max_epi8_impl
__forceinline __m128i _mm_max_epi8_impl (__m128i a, __m128i b)
{
	__m128i mask = _mm_cmpgt_epi8(a, b);
	return _mm_or_si128(_mm_andnot_si128(mask, a), _mm_and_si128(mask, b));
}

#undef _mm_max_epi32
#define _mm_max_epi32 _mm_max_epi32_impl
__forceinline __m128i _mm_max_epi32_impl (__m128i a, __m128i b)
{
	__m128i mask = _mm_cmpgt_epi32(a, b);
	return _mm_or_si128(_mm_andnot_si128(mask, a), _mm_and_si128(mask, b));
}

#undef _mm_max_epu16
#define _mm_max_epu16 _mm_max_epu16_impl
__forceinline __m128i _mm_max_epu16_impl (__m128i a, __m128i b)
{
	__m128i mask = _mm_cmpgt_epi16
	(
		_mm_add_epi8(a, _mm_set1_epi8(32767)),
		_mm_add_epi8(b, _mm_set1_epi8(32767))
	);
	return _mm_or_si128(_mm_andnot_si128(mask, a), _mm_and_si128(mask, b));
}

#undef _mm_max_epu32
#define _mm_max_epu32 _mm_max_epu32_impl
__forceinline __m128i _mm_max_epu32_impl (__m128i a, __m128i b)
{
	__m128i mask = _mm_cmpgt_epi32
	(
		_mm_add_epi8(a, _mm_set1_epi8(2147483647)),
		_mm_add_epi8(b, _mm_set1_epi8(2147483647))
	);
	return _mm_or_si128(_mm_andnot_si128(mask, a), _mm_and_si128(mask, b));
}

#undef _mm_min_epi8
#define _mm_min_epi8 _mm_min_epi8_impl
__forceinline __m128i _mm_min_epi8_impl (__m128i a, __m128i b)
{
	__m128i mask = _mm_cmpgt_epi8(a, b);
	return _mm_or_si128(_mm_andnot_si128(mask, b), _mm_and_si128(mask, a));
}

#undef _mm_min_epi32
#define _mm_min_epi32 _mm_min_epi32_impl
__forceinline __m128i _mm_min_epi32_impl (__m128i a, __m128i b)
{
	__m128i mask = _mm_cmpgt_epi32(a, b);
	return _mm_or_si128(_mm_andnot_si128(mask, b), _mm_and_si128(mask, a));
}

#undef _mm_min_epu16
#define _mm_min_epu16 _mm_min_epu16_impl
__forceinline __m128i _mm_min_epu16_impl (__m128i a, __m128i b)
{
	__m128i mask = _mm_cmpgt_epi16
	(
		_mm_add_epi8(a, _mm_set1_epi8(32767)),
		_mm_add_epi8(b, _mm_set1_epi8(32767))
	);
	return _mm_or_si128(_mm_andnot_si128(mask, b), _mm_and_si128(mask, a));
}

#undef _mm_min_epu32
#define _mm_min_epu32 _mm_min_epu32_impl
__forceinline __m128i _mm_min_epu32_impl (__m128i a, __m128i b)
{
	__m128i mask = _mm_cmpgt_epi32
	(
		_mm_add_epi8(a, _mm_set1_epi8(2147483647)),
		_mm_add_epi8(b, _mm_set1_epi8(2147483647))
	);
	return _mm_or_si128(_mm_andnot_si128(mask, b), _mm_and_si128(mask, a));
}




#undef _mm_round_pd
#define _mm_round_pd _mm_round_pd_impl
__forceinline __m128d _mm_round_pd_impl (__m128d a, int rounding)
{
	if(
		rounding == 0 || 
		rounding == 4|| rounding == 5 || 
		rounding == 6|| rounding == 7 ||
		rounding == 8 || rounding == 12 ||
		rounding == 13 || rounding == 14 ||
		rounding == 15 || rounding == 16
	)
	{
		__m128d cvtstof = _mm_set1_pd(0x0018000000000000);		
		__m128d mask = _mm_cmple_pd(a, _mm_setzero_pd());


		__m128d b1 =_mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(_mm_sub_pd(a, _mm_set1_pd(0.5)), cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
		__m128d b2 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(_mm_add_pd(a, _mm_set1_pd(0.5)), cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
		
		return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b2)), _mm_and_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b1))));
		
	}
	if(rounding == 1 || rounding == 9)
	{
		__m128d cvtstof = _mm_set1_pd(0x0018000000000000);
		__m128d mask = _mm_cmpgt_pd(a, _mm_setzero_pd());
		__m128d b1 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(a, cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
		__m128d b2 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(_mm_sub_pd(a, _mm_set1_pd(0.9998999999999999)), cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);//
		
		return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b2)), _mm_and_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b1))));
	}
	
	if(rounding == 2 || rounding == 10)
	{
		__m128d cvtstof = _mm_set1_pd(0x0018000000000000);	
		__m128d mask = _mm_cmple_pd(a, _mm_setzero_pd());
		__m128d b1 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(a, cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
		__m128d b2 = _mm_add_pd(_mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(_mm_sub_pd(a, _mm_set1_pd(0.0001)), cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof), _mm_set1_pd(1));
		
		return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b2)), _mm_and_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b1))));
	}
	
	if
	(
		rounding == 3 || rounding == 11
	)
	{
		__m128d cvtstof = _mm_set1_pd(0x0018000000000000);	
		return _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(a, cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
	}
}

#undef _mm_round_ps
#define _mm_round_ps _mm_round_ps_impl
__forceinline __m128 _mm_round_ps_impl (__m128 a, int rounding)
{
	if(
		rounding == 0 || 
		rounding == 4|| rounding == 5 || 
		rounding == 6|| rounding == 7 ||
		rounding == 8 || rounding == 12 ||
		rounding == 13 || rounding == 14 ||
		rounding == 15 || rounding == 16
	)
	{
		__m128 mask = _mm_cmple_ps(a, _mm_setzero_ps());
		__m128 b1 = _mm_cvtepi32_ps(_mm_cvtps_epi32(_mm_sub_ps(a, _mm_set1_ps(0.5))));
		__m128 b2 = _mm_cvtepi32_ps(_mm_cvtps_epi32(_mm_add_ps(a, _mm_set1_ps(0.5))));
		
		return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(_mm_castps_si128(mask), _mm_castps_si128(b2)), _mm_and_si128(_mm_castps_si128(mask), _mm_castps_si128(b1))));
		
	}
	if(rounding == 1 || rounding == 9)
	{
		__m128 mask = _mm_cmpgt_ps(a, _mm_setzero_ps());
		__m128 b1 = _mm_cvtepi32_ps(_mm_cvtps_epi32(a));
		__m128 b2 = _mm_cvtepi32_ps(_mm_cvtps_epi32(_mm_sub_ps(a, _mm_set1_ps(0.9998999))));
		
		return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(_mm_castps_si128(mask), _mm_castps_si128(b2)), _mm_and_si128(_mm_castps_si128(mask), _mm_castps_si128(b1))));
	}
	
	if(rounding == 2 || rounding == 10)
	{
		__m128 mask = _mm_cmple_ps(a, _mm_setzero_ps());
		__m128 b1 = _mm_cvtepi32_ps(_mm_cvtps_epi32(a));
		__m128 b2 = _mm_add_ps(_mm_cvtepi32_ps(_mm_cvtps_epi32(_mm_sub_ps(a, _mm_set1_ps(0.0001)))), _mm_set1_ps(1));
		
		return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(_mm_castps_si128(mask), _mm_castps_si128(b2)), _mm_and_si128(_mm_castps_si128(mask), _mm_castps_si128(b1))));
	}
	
	if
	(
		rounding == 3 || rounding == 11
	)
	{
		return _mm_cvtepi32_ps(_mm_cvtps_epi32(a));
	}
}


#undef _mm_round_sd
#define _mm_round_sd _mm_round_sd_impl
__forceinline __m128d _mm_round_sd_impl (__m128d a, __m128d b, int rounding)
{
	if(
		rounding == 0 || 
		rounding == 4|| rounding == 5 || 
		rounding == 6|| rounding == 7 ||
		rounding == 8 || rounding == 12 ||
		rounding == 13 || rounding == 14 ||
		rounding == 15 || rounding == 16
	)
	{
	
		__m128d cvtstof = _mm_set1_pd(0x0018000000000000);
		
		__m128d pass = _mm_cmpeq_pd(_mm_set_pd(0,1), _mm_set1_pd(1));
		__m128d mask = _mm_cmplt_pd(b, _mm_setzero_pd());
		
		__m128d b1 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(_mm_sub_pd(b, _mm_set1_pd(0.5)), cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
		__m128d b2 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(_mm_add_pd(b, _mm_set1_pd(0.5)), cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
		
		return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(pass), _mm_castpd_si128(a)), _mm_and_si128(_mm_castpd_si128(pass), _mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b2)), _mm_and_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b1))))));
		
	}
	if(rounding == 1 || rounding == 9)
	{
		
		__m128d cvtstof = _mm_set1_pd(0x0018000000000000);
		__m128d pass = _mm_cmpeq_pd(_mm_set_pd(0,1), _mm_set1_pd(1));
		
		__m128d mask = _mm_cmpgt_pd(b, _mm_setzero_pd());
		__m128d b1 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(a, cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
		__m128d b2 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(_mm_sub_pd(b, _mm_set1_pd(0.9998999999999999)), cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
		
		return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(pass), _mm_castpd_si128(a)), _mm_and_si128(_mm_castpd_si128(pass), _mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b2)), _mm_and_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b1))))));
	}
	
	if(rounding == 2 || rounding == 10)
	{
		
		__m128d cvtstof = _mm_set1_pd(0x0018000000000000);
		__m128d pass = _mm_cmpeq_pd(_mm_set_pd(0,1), _mm_set1_pd(1));
		__m128d mask = _mm_cmple_pd(b, _mm_setzero_pd());

		__m128d b1 = _mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(b, cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof);
		__m128d b2 = _mm_add_pd(_mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(_mm_sub_pd(a, _mm_set1_pd(0.0001)), cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof), _mm_set1_pd(1));
		
		return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(pass), _mm_castpd_si128(a)), _mm_and_si128(_mm_castpd_si128(pass), _mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b2)), _mm_and_si128(_mm_castpd_si128(mask), _mm_castpd_si128(b1))))));
	}
	
	if
	(
		rounding == 3 || rounding == 11
	)
	{
		__m128d cvtstof = _mm_set1_pd(0x0018000000000000);
		__m128d pass = _mm_cmpeq_pd(_mm_set_pd(0,1), _mm_set1_pd(1));

		return _mm_castsi128_pd(_mm_or_si128(_mm_andnot_si128(_mm_castpd_si128(pass), _mm_castpd_si128(a)), _mm_and_si128(_mm_castpd_si128(pass), _mm_castpd_si128(_mm_sub_pd(_mm_castsi128_pd(_mm_add_epi64(_mm_sub_epi64( _mm_castpd_si128(_mm_add_pd(b, cvtstof)), _mm_castpd_si128(cvtstof)), _mm_castpd_si128(cvtstof))), cvtstof)))));
	}
}

#undef _mm_round_ss
#define _mm_round_ss _mm_round_ss_impl
__forceinline __m128 _mm_round_ss_impl (__m128 a, __m128 b, int rounding)
{
	if(
		rounding == 0 || 
		rounding == 4|| rounding == 5 || 
		rounding == 6|| rounding == 7 ||
		rounding == 8 || rounding == 12 ||
		rounding == 13 || rounding == 14 ||
		rounding == 15 || rounding == 16
	)
	{
		__m128 pass = _mm_cmpeq_ps(_mm_set_ps(0,0,0,1), _mm_set1_ps(1));
		
		__m128 mask = _mm_cmplt_ps(b, _mm_setzero_ps());
		__m128 b1 = _mm_cvtepi32_ps(_mm_cvtps_epi32(_mm_sub_ps(b, _mm_set1_ps(0.5))));
		__m128 b2 = _mm_cvtepi32_ps(_mm_cvtps_epi32(_mm_add_ps(b, _mm_set1_ps(0.5))));
		
		return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(_mm_castps_si128(pass), _mm_castps_si128(a)), _mm_and_si128(_mm_castps_si128(pass), _mm_or_si128(_mm_andnot_si128(_mm_castps_si128(mask), _mm_castps_si128(b2)), _mm_and_si128(_mm_castps_si128(mask), _mm_castps_si128(b1))))));
		
	}
	if(rounding == 1 || rounding == 9)
	{

		__m128 pass = _mm_cmpeq_ps(_mm_set_ps(0,0,0,1), _mm_set1_ps(1));
		
		__m128 mask = _mm_cmpgt_ps(b, _mm_setzero_ps());

		__m128 b1 = _mm_cvtepi32_ps(_mm_cvtps_epi32(b));
		__m128 b2 = _mm_cvtepi32_ps(_mm_cvtps_epi32(_mm_sub_ps(b, _mm_set1_ps(0.9998999))));

		return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(_mm_castps_si128(pass), _mm_castps_si128(a)), _mm_and_si128(_mm_castps_si128(pass), _mm_or_si128(_mm_andnot_si128(_mm_castps_si128(mask), _mm_castps_si128(b2)), _mm_and_si128(_mm_castps_si128(mask), _mm_castps_si128(b1))))));
	}
	
	if(rounding == 2 || rounding == 10)
	{

		__m128 pass = _mm_cmpeq_ps(_mm_set_ps(0,0,0,1), _mm_set1_ps(1));
		
		__m128 mask = _mm_cmple_ps(b, _mm_setzero_ps());
		
		__m128 b1 = _mm_cvtepi32_ps(_mm_cvtps_epi32(b));
		__m128 b2 = _mm_add_ps(_mm_cvtepi32_ps(_mm_cvtps_epi32(_mm_sub_ps(b, _mm_set1_ps(0.0001)))), _mm_set1_ps(1));

		return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(_mm_castps_si128(pass), _mm_castps_si128(a)), _mm_and_si128(_mm_castps_si128(pass), _mm_or_si128(_mm_andnot_si128(_mm_castps_si128(mask), _mm_castps_si128(b2)), _mm_and_si128(_mm_castps_si128(mask), _mm_castps_si128(b1))))));
	}
	
	if
	(
		rounding == 3 || rounding == 11
	)
	{
		__m128 pass = _mm_cmpeq_ps(_mm_set_ps(0,0,0,1), _mm_set1_ps(1));

		return _mm_castsi128_ps(_mm_or_si128(_mm_andnot_si128(_mm_castps_si128(pass), _mm_castps_si128(a)), _mm_and_si128(_mm_castps_si128(pass), _mm_castps_si128(_mm_cvtepi32_ps(_mm_cvtps_epi32(b)))))); 
	}
}




/*
Implemented in extract.h


__int8 _mm_extract_epi8_impl (__m128i a, const int imm8)
__int32 _mm_extract_epi32_impl (__m128i a, const int imm8)
__int64 _mm_extract_epi64_impl (__m128i a, const int imm8)

*/

#if SSE4_1_EXTRACT
#undef _mm_extract_ps
#define _mm_extract_ps _mm_extract_ps_impl
__forceinline __int32 _mm_extract_ps_impl (__m128 a, const int imm8)
{
	union __v128
	{
		__m128 m128;
		__int32 m128_f32[4];
	};
	__v128 ret;
	ret.m128 = a;

	return ret.m128_f32[imm8];
}
#endif


#undef _mm_insert_epi8
#define _mm_insert_epi8 _mm_insert_epi8_impl
__forceinline __m128i _mm_insert_epi8_impl (__m128i a, int i, const int imm8)
{
	union __v128i
	{
		__m128i m128i;
		__int8 m128i_i8[16];
	};
	__v128i ret;
	ret.m128i = a;

	ret.m128i_i8[imm8] = i;
	return ret.m128i;
}

#undef _mm_insert_epi32
#define _mm_insert_epi32 _mm_insert_epi32_impl
__forceinline __m128i _mm_insert_epi32_impl (__m128i a, int i, const int imm8)
{
	union __v128i
	{
		__m128i m128i;
		__int32 m128i_i32[4];
	};
	__v128i ret;
	ret.m128i = a;

	ret.m128i_i32[imm8] = i;
	return ret.m128i;
}

#undef _mm_insert_epi64
#define _mm_insert_epi64 _mm_insert_epi64_impl
__forceinline __m128i _mm_insert_epi64_impl (__m128i a, __int64 i, const int imm8)
{
	union __v128i
	{
		__m128i m128i;
		__int64 m128i_i64[2];
	};
	__v128i ret;
	ret.m128i = a;

	ret.m128i_i64[imm8] = i;
	return ret.m128i;
}

#undef _mm_insert_ps
#define _mm_insert_ps _mm_insert_ps_impl
__forceinline __m128 _mm_insert_ps_impl (__m128 a, __m128 b, const int imm8)
{
	union __v128
	{
		__m128 m128;
		float m128_f32[4];
	};
	__v128 ret, B;
	ret.m128 = a;
	B.m128 = b;

	ret.m128_f32[imm8] = B.m128_f32[imm8];
	return ret.m128;
}


#undef _mm_cvtepi8_epi16
#define _mm_cvtepi8_epi16 _mm_cvtepi8_epi16_impl
__forceinline __m128i _mm_cvtepi8_epi16_impl (__m128i a)
{
	return _mm_set_epi16
	(
		(__int16)_mm_extract_epi8(a,7),
		(__int16)_mm_extract_epi8(a,6),
		(__int16)_mm_extract_epi8(a,5),
		(__int16)_mm_extract_epi8(a,4),
		(__int16)_mm_extract_epi8(a,3),
		(__int16)_mm_extract_epi8(a,2),
		(__int16)_mm_extract_epi8(a,1),
		(__int16)_mm_extract_epi8(a,0)
	);
}

#undef _mm_cvtepi8_epi32
#define _mm_cvtepi8_epi32 _mm_cvtepi8_epi32_impl
__forceinline __m128i _mm_cvtepi8_epi32_impl (__m128i a)
{
	return _mm_set_epi32
	(
		(__int32)_mm_extract_epi8(a,3),
		(__int32)_mm_extract_epi8(a,2),
		(__int32)_mm_extract_epi8(a,1),
		(__int32)_mm_extract_epi8(a,0)
	);
}


#undef _mm_cvtepi8_epi64
#define _mm_cvtepi8_epi64 _mm_cvtepi8_epi64_impl
__forceinline __m128i _mm_cvtepi8_epi64_impl (__m128i a)
{
	return _mm_set_epi64x
	(
		(__int64)_mm_extract_epi8(a,1),
		(__int64)_mm_extract_epi8(a,0)
	);
}


#undef _mm_cvtepu8_epi16
#define _mm_cvtepu8_epi16 _mm_cvtepu8_epi16_impl
__forceinline __m128i _mm_cvtepu8_epi16_impl (__m128i a)
{
	return _mm_set_epi16
	(
		(__int16)_mm_extract_epu8(a,7),
		(__int16)_mm_extract_epu8(a,6),
		(__int16)_mm_extract_epu8(a,5),
		(__int16)_mm_extract_epu8(a,4),
		(__int16)_mm_extract_epu8(a,3),
		(__int16)_mm_extract_epu8(a,2),
		(__int16)_mm_extract_epu8(a,1),
		(__int16)_mm_extract_epu8(a,0)
	);
}

#undef _mm_cvtepu8_epi32
#define _mm_cvtepu8_epi32 _mm_cvtepu8_epi32_impl
__forceinline __m128i _mm_cvtepu8_epi32_impl (__m128i a)
{
	return _mm_set_epi32
	(
		(__int32)_mm_extract_epu8(a,3),
		(__int32)_mm_extract_epu8(a,2),
		(__int32)_mm_extract_epu8(a,1),
		(__int32)_mm_extract_epu8(a,0)
	);
}


#undef _mm_cvtepu8_epi64
#define _mm_cvtepu8_epi64 _mm_cvtepu8_epi64_impl
__forceinline __m128i _mm_cvtepu8_epi64_impl (__m128i a)
{
	return _mm_set_epi64x
	(
		(__int64)_mm_extract_epu8(a,1),
		(__int64)_mm_extract_epu8(a,0)
	);
}


#undef _mm_cvtepi16_epi32
#define _mm_cvtepi16_epi32 _mm_cvtepi16_epi32_impl
__forceinline __m128i _mm_cvtepi16_epi32 (__m128i a)
{
	return _mm_set_epi32
	(
		(__int32)_mm_extract_epi16(a,3),
		(__int32)_mm_extract_epi16(a,2),
		(__int32)_mm_extract_epi16(a,1),
		(__int32)_mm_extract_epi16(a,0)
	);
}


#undef _mm_cvtepi16_epi64
#define _mm_cvtepi16_epi64 _mm_cvtepi16_epi64_impl
__forceinline __m128i _mm_cvtepi16_epi64 (__m128i a)
{
	return _mm_set_epi64x
	(
		(__int64)_mm_extract_epi16(a,1),
		(__int64)_mm_extract_epi16(a,0)
	);
}


#undef _mm_cvtepu16_epi32
#define _mm_cvtepu16_epi32 _mm_cvtepu16_epi32_impl
__forceinline __m128i _mm_cvtepu16_epi32 (__m128i a)
{
	return _mm_set_epi32
	(
		(__int32)_mm_extract_epu16(a,3),
		(__int32)_mm_extract_epu16(a,2),
		(__int32)_mm_extract_epu16(a,1),
		(__int32)_mm_extract_epu16(a,0)
	);
}


#undef _mm_cvtepu16_epi64
#define _mm_cvtepu16_epi64 _mm_cvtepu16_epi64_impl
__forceinline __m128i _mm_cvtepu16_epi64 (__m128i a)
{
	return _mm_set_epi64x
	(
		(__int64)_mm_extract_epu16(a,1),
		(__int64)_mm_extract_epu16(a,0)
	);
}

#undef _mm_cvtepi32_epi64
#define _mm_cvtepi32_epi64 _mm_cvtepi32_epi64_impl
__forceinline __m128i _mm_cvtepi32_epi64 (__m128i a)
{
	return _mm_set_epi64x
	(
		(__int64)_mm_extract_epi32(a,1),
		(__int64)_mm_extract_epi32(a,0)
	);
}

#undef _mm_cvtepu32_epi64
#define _mm_cvtepu32_epi64 _mm_cvtepu32_epi64_impl
__forceinline __m128i _mm_cvtepu32_epi64 (__m128i a)
{
	return _mm_set_epi64x
	(
		(__int64)_mm_extract_epu32(a,1),
		(__int64)_mm_extract_epu32(a,0)
	);
}

#undef _mm_minpos_epu16
#define _mm_minpos_epu16 _mm_minpos_epu16_impl
__forceinline __m128i _mm_minpos_epu16_impl (__m128i a)
{
	__uint16 pos = 0;
	__uint16 min = _mm_extract_epu16(a,0);
	if( _mm_extract_epu16(a,0) > _mm_extract_epu16(a,1) )
	{
		pos = 1;
		min = _mm_extract_epu16(a,1);
	}
	else if( min > _mm_extract_epu16(a,2) )
	{
		pos = 2;
		min = _mm_extract_epu16(a,2);
	}
	else if( min > _mm_extract_epu16(a,3) )
	{
		pos = 3;
		min = _mm_extract_epu16(a,3);
	}
	else if( min > _mm_extract_epu16(a,4) )
	{
		pos = 4;
		min = _mm_extract_epu16(a,4);
	}
	else if( min > _mm_extract_epu16(a,5) )
	{
		pos = 5;
		min = _mm_extract_epu16(a,5);
	}
	else if( min > _mm_extract_epu16(a,6) )
	{
		pos = 6;
		min = _mm_extract_epu16(a,6);
	}
	else if( min > _mm_extract_epu16(a,7) )
	{
		pos = 7;
		min = _mm_extract_epu16(a,7);
	}
	
	
	return _mm_set_epi16
	(
		0, 
		0,
		0, 
		0, 
		0, 
		0,
		pos,	
		min

	);
}

#undef _mm_mpsadbw_epu8
#define _mm_mpsadbw_epu8 _mm_mpsadbw_epu8_impl
__forceinline __m128i _mm_mpsadbw_epu8_impl (__m128i a, __m128i b, const int imm8)
{
#define abs_t(x) ( ( (x) < 0) ? -(x) : (x) )	

	union __v128i
	{
		__m128i m128i;
		__uint8 m128i_u8[16];
	};
	__v128i RET;
	__v128i A;
	__v128i B;
	
	A.m128i = a;
	B.m128i = b;
	
	int temp1, temp2, temp3, temp4, index;
	int k =  (imm8 & 0x04);
	int l = ((imm8 & 0x03) * 4);
		
	for (index = 0; index < 8; index++)
	{
		temp1 = abs_t(A.m128i_u8[k + index + 0] - B.m128i_u8[l + 0]);
		temp2 = abs_t(A.m128i_u8[k + index + 1] - B.m128i_u8[l + 1]);
		temp3 = abs_t(A.m128i_u8[k + index + 2] - B.m128i_u8[l + 2]);
		temp4 = abs_t(A.m128i_u8[k + index + 3] - B.m128i_u8[l + 3]);
		RET.m128i_u8[index] = temp1 + temp2 + temp3 + temp4;
	}
	return RET.m128i;
}

#undef _mm_mul_epi32
#define _mm_mul_epi32 _mm_mul_epi32_impl
__forceinline __m128i _mm_mul_epi32_impl (__m128i a, __m128i b)
{
	return _mm_set_epi32
	(
		_mm_extract_epi32(a,3) * _mm_extract_epi32(b,3),
		_mm_extract_epi32(a,2) * _mm_extract_epi32(b,2),
		_mm_extract_epi32(a,1) * _mm_extract_epi32(b,1),
		_mm_extract_epi32(a,0) * _mm_extract_epi32(b,0)
	);
}

#undef _mm_mullo_epi32
#define _mm_mullo_epi32 _mm_mul_epi32_impl


#undef _mm_packus_epi32
#define _mm_packus_epi32 _mm_packus_epi32_impl
__forceinline __m128i _mm_packus_epi32_impl (__m128i a, __m128i b)
{
	return _mm_set_epi16
	(
		(__int16)_mm_extract_epi32(b,3),
		(__int16)_mm_extract_epi32(b,2),
		(__int16)_mm_extract_epi32(b,1),
		(__int16)_mm_extract_epi32(b,0),
		(__int16)_mm_extract_epi32(a,3),
		(__int16)_mm_extract_epi32(a,2),
		(__int16)_mm_extract_epi32(a,1),
		(__int16)_mm_extract_epi32(a,0)
	);
}


#undef _mm_stream_load_si128
#define _mm_stream_load_si128 _mm_stream_load_si128_impl
__forceinline __m128i _mm_stream_load_si128_impl (const __m128i* mem_addr)
{
	__m128i ret = *mem_addr;
	return ret;
}