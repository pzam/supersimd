/***************************************************************************************************/
// HEADER_NAME /legacy/legacy_sse3.h
/***************************************************************************************************/
#define __SSE3__ 1

#undef _mm_addsub_pd
#define _mm_addsub_pd _mm_addsub_pd_impl
__forceinline __m128d _mm_addsub_pd_impl (__m128d a, __m128d b)
{
	__m128d sub = _mm_set_pd(1,-1);
	return _mm_add_pd(a,_mm_mul_pd(b,sub));	
}

#undef _mm_addsub_ps
#define _mm_addsub_ps _mm_addsub_ps_impl
__forceinline __m128 _mm_addsub_ps_impl (__m128 a, __m128 b)
{
	__m128 sub = _mm_set_ps(1,-1,1,-1);
	return _mm_add_ps(a,_mm_mul_ps(b,sub));
}

#undef _mm_hadd_pd
#define _mm_hadd_pd _mm_hadd_pd_impl
__forceinline __m128d _mm_hadd_pd_impl (__m128d a, __m128d b)
{
    __m128d t;
    t = _mm_shuffle_pd( a, a, _MM_SHUFFLE2(0, 1) );
    a = _mm_add_pd( a, t );   

    t = _mm_shuffle_pd( b, b, _MM_SHUFFLE2(1, 0) );
    b = _mm_add_pd( b, t );
    return _mm_shuffle_pd( a, b, _MM_SHUFFLE2(1, 0) );;
}

#undef _mm_hadd_ps
#define _mm_hadd_ps _mm_hadd_ps_impl
__forceinline __m128 _mm_hadd_ps_impl (__m128 a, __m128 b)
{
    __m128 t;
    t = _mm_shuffle_ps( a , a, _MM_SHUFFLE(2, 3, 0, 1) ); 
    a = _mm_add_ps( a, t );         //(3, 2, 1, 0)

	t = _mm_shuffle_ps( b , b, _MM_SHUFFLE(2, 3, 0, 1) );
    b = _mm_add_ps( b, t );         //(3, 2, 1, 0)
	
	
    return _mm_shuffle_ps( a , b, _MM_SHUFFLE(2, 0, 2, 0) );
}

#undef _mm_hsub_pd
#define _mm_hsub_pd _mm_hsub_pd_impl
__forceinline __m128d _mm_hsub_pd_impl (__m128d a, __m128d b)
{
    __m128d t;
    t = _mm_shuffle_pd( a, a, _MM_SHUFFLE2(0, 1) );
    a = _mm_sub_pd( a, t );   

    t = _mm_shuffle_pd( b, b, _MM_SHUFFLE2(1, 0) );
    b = _mm_sub_pd( b, t );
    return _mm_shuffle_pd( a, b, _MM_SHUFFLE2(1, 0) );;
}

#undef _mm_hsub_ps
#define _mm_hsub_ps _mm_hsub_ps_impl
__forceinline __m128 _mm_hsub_ps_impl (__m128 a, __m128 b)
{
    __m128 t;
    t = _mm_shuffle_ps( a , a, _MM_SHUFFLE(2, 3, 0, 1) ); 
    a = _mm_sub_ps( a, t );         //(3, 2, 1, 0)

	t = _mm_shuffle_ps( b , b, _MM_SHUFFLE(2, 3, 0, 1) );
    b = _mm_sub_ps( b, t );         //(3, 2, 1, 0)
	
	
    return _mm_shuffle_ps( a , b, _MM_SHUFFLE(2, 0, 2, 0) );
}

#undef _mm_lddqu_si128
#define _mm_lddqu_si128 _mm_lddqu_si128_impl
__forceinline __m128i _mm_lddqu_si128_impl (__m128i const* mem_addr)
{
	return *mem_addr;
	
}

#undef _mm_loaddup_pd
#define _mm_loaddup_pd _mm_loaddup_pd_impl
__forceinline __m128d _mm_loaddup_pd_impl (double const* mem_addr)
{
	return _mm_load1_pd(mem_addr);
}

#undef _mm_movedup_pd
#define _mm_movedup_pd _mm_movedup_pd_impl
__forceinline __m128d _mm_movedup_pd_impl (__m128d a)
{
	return _mm_set_pd
	(
		_mm_extract_pd(a,0),
		_mm_extract_pd(a,0)
	);
}


#undef _mm_movehdup_ps
#define _mm_movehdup_ps _mm_movehdup_ps_impl
__forceinline __m128 _mm_movehdup_ps_impl (__m128 a)
{
	return _mm_set_ps
	(
		_mm_extract_ps(a,3),
		_mm_extract_ps(a,3),
		_mm_extract_ps(a,1),
		_mm_extract_ps(a,1)
	);
}

#undef _mm_moveldup_ps
#define _mm_moveldup_ps _mm_moveldup_ps_impl
__forceinline __m128 _impl (__m128 a)
{
	return _mm_set_ps
	(
		_mm_extract_ps(a,2),
		_mm_extract_ps(a,2),
		_mm_extract_ps(a,0),
		_mm_extract_ps(a,0)
	);
}
