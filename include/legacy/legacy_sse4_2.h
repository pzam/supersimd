/***************************************************************************************************/
// HEADER_NAME /legacy/legacy_sse4_1.h
/***************************************************************************************************/
#define __SSE4_2__ 1

/*
//TODO
int _mm_cmpestra (__m128i a, int la, __m128i b, int lb, const int imm8)

int _mm_cmpestrc (__m128i a, int la, __m128i b, int lb, const int imm8)

int _mm_cmpestri (__m128i a, int la, __m128i b, int lb, const int imm8)

__m128i _mm_cmpestrm (__m128i a, int la, __m128i b, int lb, const int imm8)

int _mm_cmpestro (__m128i a, int la, __m128i b, int lb, const int imm8)

int _mm_cmpestrs (__m128i a, int la, __m128i b, int lb, const int imm8)

int _mm_cmpestrz (__m128i a, int la, __m128i b, int lb, const int imm8)


int _mm_cmpistra (__m128i a, __m128i b, const int imm8)

int _mm_cmpistrc (__m128i a, __m128i b, const int imm8)

int _mm_cmpistri (__m128i a, __m128i b, const int imm8)

__m128i _mm_cmpistrm (__m128i a, __m128i b, const int imm8)

int _mm_cmpistro (__m128i a, __m128i b, const int imm8)

int _mm_cmpistrs (__m128i a, __m128i b, const int imm8)

int _mm_cmpistrz (__m128i a, __m128i b, const int imm8)

unsigned int _mm_crc32_u16 (unsigned int crc, unsigned short v)

unsigned int _mm_crc32_u32 (unsigned int crc, unsigned int v)

unsigned __int64 _mm_crc32_u64 (unsigned __int64 crc, unsigned __int64 v)

unsigned int _mm_crc32_u8 (unsigned int crc, unsigned char v)
*/
#if BIT64
#undef _mm_cmpgt_epi64
#define _mm_cmpgt_epi64 _mm_cmpgt_epi64_impl
__forceinline __m128i _mm_cmpgt_epi64_impl (__m128i a, __m128i b)
{
	return _mm_set_epi64x
	(
		_mm_extract_epi64(a,1) > _mm_extract_epi64(b,1),
		_mm_extract_epi64(a,0) > _mm_extract_epi64(b,0)
	);
}
#endif