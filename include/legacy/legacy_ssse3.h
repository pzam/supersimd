/***************************************************************************************************/
// HEADER_NAME /legacy/legacy_ssse3.h
/***************************************************************************************************/
#define __SSSE3__ 1

#if __MMX__
#undef _mm_abs_pi8
#define _mm_abs_pi8 _mm_abs_pi8_impl
__forceinline __m64 _mm_abs_pi8_impl (__m64 a)
{
	return _mm_andnot_si64(_mm_set1_pi8(0x80), a);
}
#endif

#if __MMX__
#undef _mm_abs_pi16
#define _mm_abs_pi16 _mm_abs_pi16_impl
__forceinline __m64 _mm_abs_pi16_impl (__m64 a)
{
	return _mm_andnot_si64(_mm_set1_pi16(0x8000), a);
}
#endif

#if __MMX__
#undef _mm_abs_pi32
#define _mm_abs_pi32 _mm_abs_pi32_impl
__forceinline __m64 _mm_abs_pi32 (__m64 a)
{
	return _mm_andnot_si64(_mm_set1_pi32(0x80000000), a);
}
#endif

#undef _mm_abs_epi8
#define _mm_abs_epi8 _mm_abs_epi8_impl
__forceinline __m128i _mm_abs_epi8_impl (__m128i a)
{
	return _mm_andnot_si128(_mm_set1_epi8(0x80), a);
}

#undef _mm_abs_epi16
#define _mm_abs_epi16 _mm_abs_epi16_impl
__forceinline __m128i _mm_abs_epi16_impl (__m128i a)
{
	return _mm_andnot_si128(_mm_set1_epi16(0x8000), a);
}

#undef _mm_abs_epi32
#define _mm_abs_epi32 _mm_abs_epi32_impl
__forceinline __m128i _mm_abs_epi32 (__m128i a)
{
	return _mm_andnot_si128(_mm_set1_epi32(0x80000000), a);
}

#if __MMX__
#undef _mm_alignr_pi8
#define _mm_alignr_pi8 _mm_alignr_ei8_impl
__forceinline __m64 _mm_alignr_pi8 (__m64 a, __m64 b, int count)
{
	if(count < 0)
	{
		return _mm_setzero_si64();
	}
	if(count == 0)
	{
		return b;
	}
	if(count == 1)
	{
		return _mm_set_pi8
		(
			_mm_extract_pi8 (a, 0) ,
		
			_mm_extract_pi8 (b, 7) ,
			_mm_extract_pi8 (b, 6) ,
			_mm_extract_pi8 (b, 5) ,
			_mm_extract_pi8 (b, 4) ,
			_mm_extract_pi8 (b, 3) ,
			_mm_extract_pi8 (b, 2) ,
			_mm_extract_pi8 (b, 1) 
		);
	}
	if(count == 2)
	{
		return _mm_set_pi8
		(
			_mm_extract_pi8 (a, 1) ,
			_mm_extract_pi8 (a, 0) ,
		
			_mm_extract_pi8 (b, 7) ,
			_mm_extract_pi8 (b, 6) ,
			_mm_extract_pi8 (b, 5) ,
			_mm_extract_pi8 (b, 4) ,
			_mm_extract_pi8 (b, 3) ,
			_mm_extract_pi8 (b, 2) 
		);
	}
	if(count == 3)
	{
		return _mm_set_pi8
		(
			_mm_extract_pi8 (a, 2) ,
			_mm_extract_pi8 (a, 1) ,
			_mm_extract_pi8 (a, 0) ,
		
			_mm_extract_pi8 (b, 7) ,
			_mm_extract_pi8 (b, 6) ,
			_mm_extract_pi8 (b, 5) ,
			_mm_extract_pi8 (b, 4) ,
			_mm_extract_pi8 (b, 3) 
		);
	}
	if(count == 4)
	{
		return _mm_set_pi8
		(
			_mm_extract_pi8 (a, 3) ,
			_mm_extract_pi8 (a, 2) ,
			_mm_extract_pi8 (a, 1) ,
			_mm_extract_pi8 (a, 0) ,
		
			_mm_extract_pi8 (b, 7) ,
			_mm_extract_pi8 (b, 6) ,
			_mm_extract_pi8 (b, 5) ,
			_mm_extract_pi8 (b, 4) 
		);
	}
	if(count == 5)
	{
		return _mm_set_pi8
		(
			_mm_extract_pi8 (a, 4) ,
			_mm_extract_pi8 (a, 3) ,
			_mm_extract_pi8 (a, 2) ,
			_mm_extract_pi8 (a, 1) ,
			_mm_extract_pi8 (a, 0) ,
		
			_mm_extract_pi8 (b, 7) ,
			_mm_extract_pi8 (b, 6) ,
			_mm_extract_pi8 (b, 5) 
		);
	}
	if(count == 6)
	{
		return _mm_set_pi8
		(
			_mm_extract_pi8 (a, 5) ,
			_mm_extract_pi8 (a, 4) ,
			_mm_extract_pi8 (a, 3) ,
			_mm_extract_pi8 (a, 2) ,
			_mm_extract_pi8 (a, 1) ,
			_mm_extract_pi8 (a, 0) ,
		
			_mm_extract_pi8 (b, 7) ,
			_mm_extract_pi8 (b, 6) 
		);
	}
	if(count == 7)
	{
		return _mm_set_pi8
		(
			_mm_extract_pi8 (a, 6) ,
			_mm_extract_pi8 (a, 5) ,
			_mm_extract_pi8 (a, 4) ,
			_mm_extract_pi8 (a, 3) ,
			_mm_extract_pi8 (a, 2) ,
			_mm_extract_pi8 (a, 1) ,
			_mm_extract_pi8 (a, 0) ,
		
			_mm_extract_pi8 (b, 7) 
		);
	}
	if(count == 8)
	{
		return a;
	}
	if(count == 9)
	{
		return _mm_set_pi8
		(
			0 ,
			_mm_extract_pi8 (a, 7) ,
			_mm_extract_pi8 (a, 6) ,
			_mm_extract_pi8 (a, 5) ,
			_mm_extract_pi8 (a, 4) ,
			_mm_extract_pi8 (a, 3) ,
			_mm_extract_pi8 (a, 2) ,
			_mm_extract_pi8 (a, 1) 
		);
	}
	if(count == 10)
	{
		return _mm_set_pi8
		(
			0 ,
			0 ,
			_mm_extract_pi8 (a, 7) ,
			_mm_extract_pi8 (a, 6) ,
			_mm_extract_pi8 (a, 5) ,
			_mm_extract_pi8 (a, 4) ,
			_mm_extract_pi8 (a, 3) ,
			_mm_extract_pi8 (a, 2) 
		);
	}
	if(count == 11)
	{
		return _mm_set_pi8
		(
			0 ,
			0 ,
			0 ,
			_mm_extract_pi8 (a, 7) ,
			_mm_extract_pi8 (a, 6) ,
			_mm_extract_pi8 (a, 5) ,
			_mm_extract_pi8 (a, 4) ,
			_mm_extract_pi8 (a, 3) 
		);
	}
	if(count == 12)
	{
		return _mm_set_pi8
		(
			0 ,
			0 ,
			0 ,
			0 ,
			_mm_extract_pi8 (a, 7) ,
			_mm_extract_pi8 (a, 6) ,
			_mm_extract_pi8 (a, 5) ,
			_mm_extract_pi8 (a, 4) 
		);
	}
	if(count == 13)
	{
		return _mm_set_pi8
		(
			0 ,
			0 ,
			0 ,
			0 ,
			0 ,
			_mm_extract_pi8 (a, 7) ,
			_mm_extract_pi8 (a, 6) ,
			_mm_extract_pi8 (a, 5) 
		);
	}
	if(count == 14)
	{
		return _mm_set_pi8
		(
			0 ,
			0 ,
			0 ,
			0 ,
			0 ,
			0 ,
			_mm_extract_pi8 (a, 7) ,
			_mm_extract_pi8 (a, 6) 
		);
	}
	if(count == 15)
	{
		return _mm_set_pi8
		(
			0 ,
			0 ,
			0 ,
			0 ,
			0 ,
			0 ,
			0 ,
			_mm_extract_pi8 (a, 7) 
		);
	}
	if(count >= 16)
	{
		return _mm_setzero_si64();
	}
}
#endif

#undef _mm_alignr_epi8
#define _mm_alignr_epi8 _mm_alignr_epi8_impl
__forceinline __m128i _mm_alignr_epi8_impl (__m128i a, __m128i b, int count)
{
	if(count < 0)
	{
		return _mm_setzero_si128();
	}
	if(count == 0)
	{
		return b;
	}
	if(count == 1)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11), 
			_mm_extract_epi8(b, 10), 
			_mm_extract_epi8(b, 9), 
			_mm_extract_epi8(b, 8), 
			_mm_extract_epi8(b, 7), 
			_mm_extract_epi8(b, 6), 
			_mm_extract_epi8(b, 5), 
			_mm_extract_epi8(b, 4), 
			_mm_extract_epi8(b, 3), 
			_mm_extract_epi8(b, 2), 
			_mm_extract_epi8(b, 1)
		);
	}
	if(count == 2)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11), 
			_mm_extract_epi8(b, 10), 
			_mm_extract_epi8(b, 9), 
			_mm_extract_epi8(b, 8), 
			_mm_extract_epi8(b, 7), 
			_mm_extract_epi8(b, 6), 
			_mm_extract_epi8(b, 5), 
			_mm_extract_epi8(b, 4), 
			_mm_extract_epi8(b, 3), 
			_mm_extract_epi8(b, 2)	
		);
	}
	if(count == 3)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11), 
			_mm_extract_epi8(b, 10), 
			_mm_extract_epi8(b, 9), 
			_mm_extract_epi8(b, 8), 
			_mm_extract_epi8(b, 7), 
			_mm_extract_epi8(b, 6), 
			_mm_extract_epi8(b, 5), 
			_mm_extract_epi8(b, 4), 
			_mm_extract_epi8(b, 3) 
		);
	}
	if(count == 4)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11), 
			_mm_extract_epi8(b, 10), 
			_mm_extract_epi8(b, 9), 
			_mm_extract_epi8(b, 8), 
			_mm_extract_epi8(b, 7), 
			_mm_extract_epi8(b, 6), 
			_mm_extract_epi8(b, 5), 
			_mm_extract_epi8(b, 4)
		);
	}
	if(count == 5)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11), 
			_mm_extract_epi8(b, 10), 
			_mm_extract_epi8(b, 9), 
			_mm_extract_epi8(b, 8), 
			_mm_extract_epi8(b, 7), 
			_mm_extract_epi8(b, 6), 
			_mm_extract_epi8(b, 5)
		);
	}
	if(count == 6)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11), 
			_mm_extract_epi8(b, 10), 
			_mm_extract_epi8(b, 9), 
			_mm_extract_epi8(b, 8), 
			_mm_extract_epi8(b, 7), 
			_mm_extract_epi8(b, 6)
		);
	}
	if(count == 7)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11), 
			_mm_extract_epi8(b, 10), 
			_mm_extract_epi8(b, 9), 
			_mm_extract_epi8(b, 8), 
			_mm_extract_epi8(b, 7)
		);
	}
	if(count == 8)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11), 
			_mm_extract_epi8(b, 10), 
			_mm_extract_epi8(b, 9), 
			_mm_extract_epi8(b, 8)
		);
	}
	if(count == 9)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11), 
			_mm_extract_epi8(b, 10), 
			_mm_extract_epi8(b, 9)
		);
	}
	if(count == 10)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11), 
			_mm_extract_epi8(b, 10)
		);
	}
	if(count == 11)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12), 
			_mm_extract_epi8(b, 11)
		);
	}
	if(count == 12)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13), 
			_mm_extract_epi8(b, 12)
		);
	}
	if(count == 13)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14), 
			_mm_extract_epi8(b, 13)
		);
	}
	if(count == 14)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15), 
			_mm_extract_epi8(b, 14)
		);
	}
	if(count == 15)
	{
		return _mm_set_epi8
		( 
					
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1),
			_mm_extract_epi8(a, 0),
			
			_mm_extract_epi8(b, 15)
		);
	}
	
	if(count == 16)
	{
		return a;
	}
	
	if(count == 17)
	{
		return _mm_set_epi8
		( 
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2),
			_mm_extract_epi8(a, 1)
		);
	}
	if(count == 18)
	{
		return _mm_set_epi8
		( 
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3),
			_mm_extract_epi8(a, 2)
		);
	}
	if(count == 19)
	{
		return _mm_set_epi8
		( 
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4),
			_mm_extract_epi8(a, 3)
		);
	}
	if(count == 20)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5),
			_mm_extract_epi8(a, 4)
		);
	}
	if(count == 21)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6),
			_mm_extract_epi8(a, 5)
		);
	}
	if(count == 22)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7),
			_mm_extract_epi8(a, 6)
		);
	}
	if(count == 23)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8),
			_mm_extract_epi8(a, 7)
		);
	}
	if(count == 24)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9),
			_mm_extract_epi8(a, 8)
		);
	}
	if(count == 25)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10),
			_mm_extract_epi8(a, 9)
		);
	}
	if(count == 26)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11),
			_mm_extract_epi8(a, 10)
		);
	}
	if(count == 27)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12),
			_mm_extract_epi8(a, 11)
		);
	}
	if(count == 28)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13),
			_mm_extract_epi8(a, 12)
		);
	}
	if(count == 29)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14),
			_mm_extract_epi8(a, 13)
		);
	}
	if(count == 30)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15),
			_mm_extract_epi8(a, 14)
		);
	}
	if(count == 31)
	{
		return _mm_set_epi8
		( 
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,	
			0,		
			_mm_extract_epi8(a, 15)
		);
	}
	
	if(count >= 32)
	{
		return _mm_setzero_si128();
	}
	
}

#if __MMX__
#undef _mm_hadd_pi16
#define _mm_hadd_pi16 _mm_hadd_pi16_impl
__forceinline __m64 _mm_hadd_pi16_impl (__m64 a, __m64 b)
{
    __m64 t;
	
	union __v64
    {
        __m64 m64;
        __uint32 m64_u32[2];
    };
	__v64 RET;
	__v64 A;
	__v64 B;
	
	
    t = _mm_shuffle_pi16( a ,_MM_SHUFFLE(2, 3, 0, 1) );
    a = _mm_add_pi16( a, t );
	a = _mm_shuffle_pi16( a ,_MM_SHUFFLE(3, 1, 2, 0) );

    t = _mm_shuffle_pi16( b ,_MM_SHUFFLE(2, 3, 0, 1) );
    b = _mm_add_pi16( b, t );
	b = _mm_shuffle_pi16( b ,_MM_SHUFFLE(2, 0, 3, 1) );

	A.m64 = a;	
	B.m64 = b;
	
	RET.m64_u32[0] = A.m64_u32[0];
	RET.m64_u32[1] = B.m64_u32[1];
	
    return RET.m64;
}
#endif


#undef _mm_hadd_pi32
#define _mm_hadd_pi32 _mm_hadd_pi32_impl
__forceinline __m64 _mm_hadd_pi32 (__m64 a, __m64 b)
{
	union __v64
    {
        __m64 m64;
        __int32 m64_i32[2];
    };
	__v64 RET;
	__v64 A;
	__v64 B;

	A.m64 = a;
	B.m64 = b;
	
	RET.m64_i32[0] = A.m64_i32[0] + A.m64_i32[1];
	RET.m64_i32[1] = B.m64_i32[0] + B.m64_i32[1];
	
    return RET.m64;
} 


#undef _mm_hadd_epi16
#define _mm_hadd_epi16 _mm_hadd_epi16_impl
__forceinline __m128i _mm_hadd_epi16_impl (__m128i a, __m128i b)
{
		union __v128i
		{
			__m128i m128i;
			__int8 m128i_i16[8];
		};
	
		__v128i RET;
		__v128i A;
		__v128i B;
		
		A.m128i = a;
		B.m128i = b;
		
		RET.m128i_i16[0] = A.m128i_i16[0] + A.m128i_i16[1];
		RET.m128i_i16[1] = A.m128i_i16[2] + A.m128i_i16[3];
		RET.m128i_i16[2] = A.m128i_i16[4] + A.m128i_i16[5];
		RET.m128i_i16[3] = A.m128i_i16[6] + A.m128i_i16[7];		
		RET.m128i_i16[4] = B.m128i_i16[0] + B.m128i_i16[1];
		RET.m128i_i16[5] = B.m128i_i16[2] + B.m128i_i16[3];
		RET.m128i_i16[6] = B.m128i_i16[4] + B.m128i_i16[5];
		RET.m128i_i16[7] = B.m128i_i16[6] + B.m128i_i16[7];

		return RET.m128i;
}

#undef _mm_hadd_epi32
#define _mm_hadd_epi32 _mm_hadd_epi32_impl
__forceinline __m128i _mm_hadd_epi32_impl (__m128i a, __m128i b)
{
    __m128i t;
    t = _mm_shuffle_epi32( a ,_MM_SHUFFLE(2, 3, 0, 1) ); 
    a = _mm_add_epi32( a, t );

	t = _mm_shuffle_epi32( b ,_MM_SHUFFLE(2, 3, 0, 1) );
    b = _mm_add_epi32( b, t );
	
	
    return _mm_castps_si128(_mm_shuffle_ps( _mm_castsi128_ps(a) , _mm_castsi128_ps(b), _MM_SHUFFLE(2, 0, 2, 0) ));
} 

#if __MMX__
#undef _mm_hadds_pi16
#define _mm_hadds_pi16 _mm_hadds_pi16_impl
__forceinline __m64 _mm_hadd_pi16_impl (__m64 a, __m64 b)
{
    __m64 t;
	
	union __v64
    {
        __m64 m64;
        __uint32 m64_u32[2];
    };
	__v64 RET;
	__v64 A;
	__v64 B;
	
	
    t = _mm_shuffle_pi16( a ,_MM_SHUFFLE(2, 3, 0, 1) );
    a = _mm_adds_pi16( a, t );
	a = _mm_shuffle_pi16( a ,_MM_SHUFFLE(3, 1, 2, 0) );

    t = _mm_shuffle_pi16( b ,_MM_SHUFFLE(2, 3, 0, 1) );
    b = _mm_adds_pi16( b, t );
	b = _mm_shuffle_pi16( b ,_MM_SHUFFLE(2, 0, 3, 1) );

	A.m64 = a;	
	B.m64 = b;
	
	RET.m64_u32[0] = A.m64_u32[0];
	RET.m64_u32[1] = B.m64_u32[1];
	
    return RET.m64;
}
#endif

//TODO
//__m128i _mm_hadds_epi16 (__m128i a, __m128i b)



#undef _mm_hsub_epi16//REWORK???
#define _mm_hsub_epi16 _mm_hsub_epi16_impl
__forceinline __m128i _mm_hsub_epi16_impl (__m128i a, __m128i b)
{
		union __v128i
		{
			__m128i m128i;
			__int8 m128i_i16[8];
		};
	
		__v128i RET;
		__v128i A;
		__v128i B;
		
		A.m128i = a;
		B.m128i = b;
		
		RET.m128i_i16[0] = A.m128i_i16[0] - A.m128i_i16[1];
		RET.m128i_i16[1] = A.m128i_i16[2] - A.m128i_i16[3];
		RET.m128i_i16[2] = A.m128i_i16[4] - A.m128i_i16[5];
		RET.m128i_i16[3] = A.m128i_i16[6] - A.m128i_i16[7];		
		RET.m128i_i16[4] = B.m128i_i16[0] - B.m128i_i16[1];
		RET.m128i_i16[5] = B.m128i_i16[2] - B.m128i_i16[3];
		RET.m128i_i16[6] = B.m128i_i16[4] - B.m128i_i16[5];
		RET.m128i_i16[7] = B.m128i_i16[6] - B.m128i_i16[7];

		return RET.m128i;
}


#undef _mm_hsub_epi32
#define _mm_hsub_epi32 _mm_hsub_epi32_impl
__forceinline __m128i _mm_hsub_epi32 (__m128i a, __m128i b)
{
    __m128i t;
    t = _mm_shuffle_epi32( a ,_MM_SHUFFLE(2, 3, 0, 1) ); 
    a = _mm_sub_epi32( a, t );

	t = _mm_shuffle_epi32( b ,_MM_SHUFFLE(2, 3, 0, 1) );
    b = _mm_sub_epi32( b, t );
	
	
    return _mm_castps_si128(_mm_shuffle_ps( _mm_castsi128_ps(a) , _mm_castsi128_ps(b), _MM_SHUFFLE(2, 0, 2, 0) ));
} 

#if __MMX__
#undef _mm_hsub_pi16
#define _mm_hsub_pi16 _mm_hsub_pi16_impl
__forceinline __m64 _mm_hsub_pi16_impl (__m64 a, __m64 b)
{
    __m64 t;
	
	union __v64
    {
        __m64 m64;
        __uint32 m64_u32[2];
    };
	__v64 RET;
	__v64 A;
	__v64 B;
	
	
    t = _mm_shuffle_pi16( a ,_MM_SHUFFLE(2, 3, 0, 1) );
    a = _mm_sub_pi16( a, t );
	a = _mm_shuffle_pi16( a ,_MM_SHUFFLE(3, 1, 2, 0) );

    t = _mm_shuffle_pi16( b ,_MM_SHUFFLE(2, 3, 0, 1) );
    b = _mm_sub_pi16( b, t );
	b = _mm_shuffle_pi16( b ,_MM_SHUFFLE(2, 0, 3, 1) );

	A.m64 = a;	
	B.m64 = b;
	
	RET.m64_u32[0] = A.m64_u32[0];
	RET.m64_u32[1] = B.m64_u32[1];
	
    return RET.m64;
}
#endif

#undef _mm_hsub_pi32
#define _mm_hsub_pi32 _mm_hsub_pi32_impl
__forceinline __m64 _mm_hsub_pi32 (__m64 a, __m64 b)
{
	union __v64
    {
        __m64 m64;
        __int32 m64_i32[2];
    };
	__v64 RET;
	__v64 A;
	__v64 B;

	A.m64 = a;
	B.m64 = b;
	
	RET.m64_i32[0] = A.m64_i32[0] - A.m64_i32[1];
	RET.m64_i32[1] = B.m64_i32[0] - B.m64_i32[1];
	
    return RET.m64;
} 

//TODO
//__m128i _mm_hsubs_epi16 (__m128i a, __m128i b)

#if __MMX__
#undef _mm_hsubs_pi16
#define _mm_hsubs_pi16 _mm_hsubs_pi16_impl
__forceinline __m64 _mm_hsubs_pi16_impl (__m64 a, __m64 b)
{
    __m64 t;
	
	union __v64
    {
        __m64 m64;
        __uint32 m64_u32[2];
    };
	__v64 RET;
	__v64 A;
	__v64 B;
	
	
    t = _mm_shuffle_pi16( a ,_MM_SHUFFLE(2, 3, 0, 1) );
    a = _mm_subs_pi16( a, t );
	a = _mm_shuffle_pi16( a ,_MM_SHUFFLE(3, 1, 2, 0) );

    t = _mm_shuffle_pi16( b ,_MM_SHUFFLE(2, 3, 0, 1) );
    b = _mm_subs_pi16( b, t );
	b = _mm_shuffle_pi16( b ,_MM_SHUFFLE(2, 0, 3, 1) );

	A.m64 = a;	
	B.m64 = b;
	
	RET.m64_u32[0] = A.m64_u32[0];
	RET.m64_u32[1] = B.m64_u32[1];
	
    return RET.m64;
}
#endif

//TODO
//__m128i _mm_maddubs_epi16 (__m128i a, __m128i b)
//__m64 _mm_maddubs_pi16 (__m64 a, __m64 b)

/**************************************************************/
#undef _mm_shuffle_pi8
#define _mm_shuffle_pi8 _mm_shuffle_pi8_impl
__forceinline __m64 _mm_shuffle_pi8_impl (__m64 a, __m64 b)
{
		union __v64
		{
			__m64 m64;
			__uint64 m64_i8[8];
		};
	
		__v64 RET;
		__v64 A;
		__v64 B;
		
		A.m64 = a;
		B.m64 = b;
		
		RET.m64_i8[0] = A.m64_i8[B.m64_i8[0]];
		RET.m64_i8[1] = A.m64_i8[B.m64_i8[1]];
		RET.m64_i8[2] = A.m64_i8[B.m64_i8[2]];
		RET.m64_i8[3] = A.m64_i8[B.m64_i8[3]];
		RET.m64_i8[4] = A.m64_i8[B.m64_i8[4]];
		RET.m64_i8[5] = A.m64_i8[B.m64_i8[5]];
		RET.m64_i8[6] = A.m64_i8[B.m64_i8[6]];
		RET.m64_i8[7] = A.m64_i8[B.m64_i8[7]];
		
		return RET.m64;
}


#undef _mm_shuffle_epi8
#define _mm_shuffle_epi8 _mm_shuffle_epi8_impl
__forceinline __m128i _mm_shuffle_epi8_impl (__m128i a, __m128i b)
{
	
		union __v128i
		{
			__m128i m128i;
			__int8 m128i_i8[16];
		};
	
		__v128i RET;
		__v128i A;
		__v128i B;
		
		A.m128i = a;
		B.m128i = b;
		
		RET.m128i_i8[0] = A.m128i_i8[B.m128i_i8[0]];
		RET.m128i_i8[1] = A.m128i_i8[B.m128i_i8[1]];
		RET.m128i_i8[2] = A.m128i_i8[B.m128i_i8[2]];
		RET.m128i_i8[3] = A.m128i_i8[B.m128i_i8[3]];
		RET.m128i_i8[4] = A.m128i_i8[B.m128i_i8[4]];
		RET.m128i_i8[5] = A.m128i_i8[B.m128i_i8[5]];
		RET.m128i_i8[6] = A.m128i_i8[B.m128i_i8[6]];
		RET.m128i_i8[7] = A.m128i_i8[B.m128i_i8[7]];
		RET.m128i_i8[8] = A.m128i_i8[B.m128i_i8[8]];
		RET.m128i_i8[9] = A.m128i_i8[B.m128i_i8[9]];
		RET.m128i_i8[10] = A.m128i_i8[B.m128i_i8[10]];
		RET.m128i_i8[11] = A.m128i_i8[B.m128i_i8[11]];
		RET.m128i_i8[12] = A.m128i_i8[B.m128i_i8[12]];
		RET.m128i_i8[13] = A.m128i_i8[B.m128i_i8[13]];
		RET.m128i_i8[14] = A.m128i_i8[B.m128i_i8[14]];
		RET.m128i_i8[15] = A.m128i_i8[B.m128i_i8[15]];
		
		return RET.m128i;
}
/****************************************************************/
//TODO
//__m128i _mm_mulhrs_epi16 (__m128i a, __m128i b)
//__m64 _mm_mulhrs_pi16 (__m64 a, __m64 b)
/*
vmovl ==== _mm_cvtps_epi32(_mm_cvtpi16(a))

_mm_srli_epi32(,14)

__forceinline __m64i_i16 _mm_mulhrs_pi16 (__m64i_i16 a, __m64i_i16 b)
{
	return  vmovn_s32((vshlq_s32(vmulq_s32(vmovl_s16(a), vmovl_s16(b)), vnegq_s32(vdupq_n_s32(14)))));
}
*/




#if __MMX__
#undef _mm_sign_pi8
#define _mm_sign_pi8 _mm_sign_pi8_impl
__forceinline __m64 _mm_sign_pi8_impl (__m64 a, __m64 b)
{

	__m64 chs_a;
	
	__m64 mask_ltzero;
	__m64 mask_eqzero;

	__m64 ret;
	
	chs_a = _mm_add_pi8(_mm_xor_si64(a,_mm_cmpeq_pi8(_mm_setzero_si64(),_mm_setzero_si64())),_mm_set1_pi8(1));

	mask_ltzero = _mm_cmpgt_pi8(_mm_setzero_si64(), b);
	mask_eqzero = _mm_cmpeq_pi8(b,_mm_setzero_si64());
	
	ret = _mm_or_si64(_mm_andnot_si64(mask_ltzero, a), _mm_and_si64(mask_ltzero, chs_a));
	ret = _mm_or_si64(_mm_andnot_si64(mask_eqzero, ret), _mm_and_si64(mask_eqzero, _mm_setzero_si64()));
	
	return ret;
}
#endif

#if __MMX__
#undef _mm_sign_pi16
#define _mm_sign_pi16 _mm_sign_pi16_impl
__forceinline __m64 _mm_sign_pi16_impl (__m64 a, __m64 b)
{

	__m64 chs_a;
	
	__m64 mask_ltzero;
	__m64 mask_eqzero;

	__m64 ret;
	
	chs_a = _mm_add_pi16(_mm_xor_si64(a,_mm_cmpeq_pi16(_mm_setzero_si64(),_mm_setzero_si64())),_mm_set1_pi16(1));

	mask_ltzero = _mm_cmpgt_pi8(_mm_setzero_si64(), b);
	mask_eqzero = _mm_cmpeq_pi16(b,_mm_setzero_si64());
	
	ret = _mm_or_si64(_mm_andnot_si64(mask_ltzero, a), _mm_and_si64(mask_ltzero, chs_a));
	ret = _mm_or_si64(_mm_andnot_si64(mask_eqzero, ret), _mm_and_si64(mask_eqzero, _mm_setzero_si64()));
	
	return ret;
}
#endif

#if __MMX__
#undef _mm_sign_pi32
#define _mm_sign_pi32 _mm_sign_pi32_impl
__forceinline __m64 _mm_sign_pi32_impl (__m64 a, __m64 b)
{

	__m64 chs_a;
	
	__m64 mask_ltzero;
	__m64 mask_eqzero;

	__m64 ret;
	
	chs_a = _mm_add_pi32(_mm_xor_si64(a,_mm_cmpeq_pi32(_mm_setzero_si64(),_mm_setzero_si64())),_mm_set1_pi32(1));

	mask_ltzero = _mm_cmpgt_pi8(_mm_setzero_si64(), b);
	mask_eqzero = _mm_cmpeq_pi32(b,_mm_setzero_si64());
	
	ret = _mm_or_si64(_mm_andnot_si64(mask_ltzero, a), _mm_and_si64(mask_ltzero, chs_a));
	ret = _mm_or_si64(_mm_andnot_si64(mask_eqzero, ret), _mm_and_si64(mask_eqzero, _mm_setzero_si64()));
	
	return ret;
}
#endif

#undef _mm_sign_epi8
#define _mm_sign_epi8 _mm_sign_epi8_impl
__forceinline __forceinline __m128i _mm_sign_epi8_impl (__m128i a, __m128i b)
{

	__m128i chs_a;
	
	__m128i mask_ltzero;
	__m128i mask_eqzero;

	__m128i ret;
	
	chs_a = _mm_add_epi8(_mm_xor_si128(a,_mm_cmpeq_epi8(_mm_setzero_si128(),_mm_setzero_si128())),_mm_set1_epi8(1));

	mask_ltzero = _mm_cmplt_epi8(b,_mm_setzero_si128());
	mask_eqzero = _mm_cmpeq_epi8(b,_mm_setzero_si128());
	
	ret = _mm_or_si128(_mm_andnot_si128(mask_ltzero, a), _mm_and_si128(mask_ltzero, chs_a));
	ret = _mm_or_si128(_mm_andnot_si128(mask_eqzero, ret), _mm_and_si128(mask_eqzero, _mm_setzero_si128()));
	
	return ret;
}


#undef _mm_sign_epi16
#define _mm_sign_epi16 _mm_sign_epi16_impl
__forceinline __m128i _mm_sign_epi16_impl (__m128i a, __m128i b)
{

	__m128i chs_a;
	
	__m128i mask_ltzero;
	__m128i mask_eqzero;

	__m128i ret;
	
	chs_a = _mm_add_epi16(_mm_xor_si128(a,_mm_cmpeq_epi16(_mm_setzero_si128(),_mm_setzero_si128())),_mm_set1_epi16(1));

	mask_ltzero = _mm_cmplt_epi16(b,_mm_setzero_si128());
	mask_eqzero = _mm_cmpeq_epi16(b,_mm_setzero_si128());
	
	ret = _mm_or_si128(_mm_andnot_si128(mask_ltzero, a), _mm_and_si128(mask_ltzero, chs_a));
	ret = _mm_or_si128(_mm_andnot_si128(mask_eqzero, ret), _mm_and_si128(mask_eqzero, _mm_setzero_si128()));
	
	return ret;
}

#undef _mm_sign_epi32
#define _mm_sign_epi32 _mm_sign_epi32_impl
__forceinline __m128i _mm_sign_epi32_impl (__m128i a, __m128i b)
{

	__m128i chs_a;
	
	__m128i mask_ltzero;
	__m128i mask_eqzero;

	__m128i ret;
	
	chs_a = _mm_add_epi32(_mm_xor_si128(a,_mm_cmpeq_epi32(_mm_setzero_si128(),_mm_setzero_si128())),_mm_set1_epi32(1));

	mask_ltzero = _mm_cmplt_epi32(b,_mm_setzero_si128());
	mask_eqzero = _mm_cmpeq_epi32(b,_mm_setzero_si128());
	
	ret = _mm_or_si128(_mm_andnot_si128(mask_ltzero, a), _mm_and_si128(mask_ltzero, chs_a));
	ret = _mm_or_si128(_mm_andnot_si128(mask_eqzero, ret), _mm_and_si128(mask_eqzero, _mm_setzero_si128()));
	
	return ret;
}



