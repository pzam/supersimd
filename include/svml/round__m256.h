/***************************************************************************************************/
// HEADER_NAME /svml/round__m256.h
/***************************************************************************************************/

#if __AVX__
/*************************************************************************************/
// SVML Redefines!
/*************************************************************************************/
#undef _mm256_svml_ceil_ps
#define _mm256_svml_ceil_ps _mm256_svml_ceil_ps_impl
__forceinline __m256 _mm256_svml_ceil_ps_impl(__m256 a)
{
	return _mm256_ceil_ps(a);
}

#undef _mm256_svml_floor_ps
#define _mm256_svml_floor_ps _mm256_svml_floor_ps_impl
__forceinline __m256 _mm256_svml_floor_ps_impl(__m256 a)
{
	return _mm256_floor_ps(a);
}

#undef _mm256_svml_round_ps
#define _mm256_svml_round_ps _mm256_svml_round_ps_impl
__forceinline __m256 _mm256_svml_round_ps_impl(__m256 a)
{
	return _mm256_round_ps(a, _MM_FROUND_TO_NEAREST_INT);
}
/*************************************************************************************/

#undef _mm256_trunc_ps
#define _mm256_trunc_ps _mm256_trunc_ps_impl
__forceinline __m256 _mm256_trunc_ps_impl(__m256 a) {
	return _mm256_round_ps(a, _MM_FROUND_TRUNC);
}
#endif

