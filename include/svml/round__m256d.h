/***************************************************************************************************/
// HEADER_NAME /svml/round__m256d.h
/***************************************************************************************************/

#if __AVX__
/*************************************************************************************/
// SVML Redefines!
/*************************************************************************************/
#undef _mm256_svml_ceil_pd
#define _mm256_svml_ceil_pd _mm256_svml_ceil_pd_impl
__forceinline __m256d _mm256_svml_ceil_pd_impl(__m256d a)
{
	return _mm256_ceil_pd(a);
}

#undef _mm256_svml_floor_pd
#define _mm256_svml_floor_pd _mm256_svml_floor_pd_impl
__forceinline __m256d _mm256_svml_floor_pd_impl(__m256d a)
{
	return _mm256_floor_pd(a);
}

#undef _mm256_svml_round_pd
#define _mm256_svml_round_pd _mm256_svml_round_pd_impl
__forceinline __m256d _mm256_svml_round_pd_impl(__m256d a)
{
	return _mm256_round_pd(a, _MM_FROUND_TO_NEAREST_INT);
}
/*************************************************************************************/

// Similar to _mm256_cvttepi32_pd
#undef _mm256_trunc_pd
#define _mm256_trunc_pd _mm256_trunc_pd_impl
__forceinline __m256d _mm256_trunc_pd_impl(__m256d a) {
	return _mm256_round_pd(a, _MM_FROUND_TRUNC);
}
#endif

