/***************************************************************************************************/
// HEADER_NAME /svml/distribution__m128.h
/***************************************************************************************************/

#undef _mm_erf_ps
#define _mm_erf_ps _mm_erf_ps_impl
__forceinline __m128 _mm_erf_ps_impl(__m128 a) {
	__m128 sign_bit, t, y, ex;
	
	sign_bit = _mm_preservesignbit_ps(a);
	a = _mm_abs_ps(a);
	
	ex = _mm_exp_ps(_mm_mul_ps(_mm_chs_ps(a), a));
	t = _mm_rcp_ps(_mm_fmadd_ps(__m128_erf_p0, a, __m128_1));
	
	
	y = _mm_fmadd_ps(__m128_erf_p1, t, __m128_erf_p2);
	y = _mm_fmadd_ps(y, t, __m128_erf_p3);
	y = _mm_fmadd_ps(y, t, __m128_erf_p4);
	y = _mm_fmadd_ps(y, t, __m128_erf_p5);
	y = _mm_mul_ps(y, t);
	y = _mm_fnmadd_ps(y, ex,__m128_1);
	
	return _mm_effectsignbit_ps(sign_bit, y);
}

#undef _mm_erfc_ps
#define _mm_erfc_ps _mm_erfc_ps_impl
__forceinline __m128 _mm_erfc_ps_impl(__m128 a) {
	return _mm_sub_ps(__m128_1, _mm_erf_ps(a));
}

#undef _mm_erfinv_ps
#define _mm_erfinv_ps _mm_erfinv_ps_impl
__forceinline __m128 _mm_erfinv_ps_impl(__m128 a) {
	__m128 sign_bit,r,r1,r2, x, y, z;
	
	sign_bit = _mm_preservesignbit_ps(a);
	a = _mm_abs_ps(a);
	
	x = _mm_mul_ps(a, a);
	r1 = _mm_fmadd_ps(__m128_erfinv_a0, x, __m128_erfinv_a1);
	r1 = _mm_fmadd_ps(r1, x, __m128_erfinv_a2);
	r1 = _mm_fmadd_ps(r1, x, __m128_erfinv_a3);
	
	r1 = _mm_mul_ps(a, r1);
	
	z = _mm_fmadd_ps(__m128_erfinv_b0, x, __m128_erfinv_b1);
	z = _mm_fmadd_ps(z, x, __m128_erfinv_b2);
	z = _mm_fmadd_ps(z, x, __m128_erfinv_b3);
	z = _mm_fmadd_ps(z, x, __m128_1);
	
	r1 = _mm_div_ps(r1, z);
	
	
	y = _mm_sub_ps(__m128_1, a);
	y = _mm_log_ps(y);
	y = _mm_chs_ps(y);
	y = _mm_div_ps(y, __m128_2);
	
	r2 = _mm_fmadd_ps(__m128_erfinv_c0, y, __m128_erfinv_c1);
	r2 = _mm_fmadd_ps(r2, y, __m128_erfinv_c2);
	r2 = _mm_fmadd_ps(r2, y, __m128_erfinv_c3);
	
	z = _mm_fmadd_ps(__m128_erfinv_d0, y, __m128_erfinv_d1);
	z = _mm_fmadd_ps(z, y, __m128_1);
	
	r2 = _mm_div_ps(r2, z);
	
	
	r = _mm_max_ps(r1, r2);
	r = _mm_effectsignbit_ps(sign_bit, r);
	a = _mm_effectsignbit_ps(sign_bit, a);
	
	
	y = _mm_erf_ps(r);
	y = _mm_sub_ps(y, a);
	
	z = _mm_mul_ps(_mm_chs_ps(r), r);
	z = _mm_exp_ps(z);
	z = _mm_mul_ps(__m128_SQRTPI, z);
	z = _mm_div_ps(__m128_2, z);
	
	y = _mm_div_ps(y, z);
	
	r = _mm_sub_ps(r, y);
	
	
	y = _mm_erf_ps(r);
	y = _mm_sub_ps(y, a);
	
	z = _mm_mul_ps(_mm_chs_ps(r), r);
	z = _mm_mul_ps(__m128_SQRTPI, z);
	z = _mm_div_ps(__m128_2, z);
	
	y = _mm_div_ps(y, z);
	
	return  _mm_sub_ps(r, y);
}

#undef _mm_erfcinv_ps
#define _mm_erfcinv_ps _mm_erfcinv_ps_impl
__forceinline __m128 _mm_erfcinv_ps_impl(__m128 a) {
	return _mm_erfinv_ps(_mm_sub_ps(__m128_1, a));
}

#undef _mm_cdfnorm_ps
#define _mm_cdfnorm_ps _mm_cdfnorm_ps_impl
__forceinline __m128 _mm_cdfnorm_ps_impl(__m128 a) {
	__m128 sign_bit, t, y, ex;
	
	sign_bit = _mm_preservesignbit_ps(a);
	
	a = _mm_div_ps(_mm_abs_ps(a), _mm_sqrt_ps(__m128_2));
	
	t = _mm_rcp_ps(_mm_fmadd_ps(__m128_erf_p0,a,__m128_1));
	ex = _mm_exp_ps(_mm_mul_ps(_mm_chs_ps(a), a));
	
	y = _mm_fmadd_ps(__m128_erf_p1, t, __m128_erf_p2);
	y = _mm_fmadd_ps(y, t, __m128_erf_p3);
	y = _mm_fmadd_ps(y, t, __m128_erf_p4);
	y = _mm_fmadd_ps(y, t, __m128_erf_p5);
	y = _mm_mul_ps(y, t);
	y = _mm_fnmadd_ps(y, ex, __m128_1);
	
	return _mm_mul_ps(__m128_1o2, _mm_add_ps(_mm_effectsignbit_ps(sign_bit, y), __m128_1));
}

/***************************************************************************************************/
// NOT PART OF SVML 
// Alternate name to cdfnorm
#undef _mm_phi_ps
#define _mm_phi_ps _mm_phi_ps_impl
__forceinline __m128 _mm_phi_ps_impl(__m128 a) {
	return _mm_cdfnorm_ps(a);
}
/***************************************************************************************************/

#undef _mm_cdfnorminv_ps
#define _mm_cdfnorminv_ps _mm_cdfnorminv_ps_impl
__forceinline __m128 _mm_cdfnorminv_ps_impl(__m128 a) {
	__m128 y;
	
	y = _mm_fmsub_ps(__m128_2, a, __m128_1);
	y = _mm_erfinv_ps(y);
	y = _mm_mul_ps(__m128_2, y);
	
	return _mm_div_ps(_mm_mul_ps(_mm_sqrt_ps(__m128_2),y), __m128_2);
}

