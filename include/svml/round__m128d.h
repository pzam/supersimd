/***************************************************************************************************/
// HEADER_NAME /svml/round__m128d.h
/***************************************************************************************************/

#if BIT64
/*************************************************************************************/
// SVML Redefines!
/*************************************************************************************/
#undef _mm_svml_ceil_pd
#define _mm_svml_ceil_pd _mm_svml_ceil_pd_impl
__forceinline __m128d _mm_svml_ceil_pd_impl(__m128d a)
{
	return _mm_ceil_pd(a);
}

#undef _mm_svml_floor_pd
#define _mm_svml_floor_pd _mm_svml_floor_pd_impl
__forceinline __m128d _mm_svml_floor_pd_impl(__m128d a)
{
	return _mm_floor_pd(a);
}

#undef _mm_svml_round_pd
#define _mm_svml_round_pd _mm_svml_round_pd_impl
__forceinline __m128d _mm_svml_round_pd_impl(__m128d a)
{
	return _mm_round_pd(a, _MM_FROUND_TO_NEAREST_INT);
}
/*************************************************************************************/

// Similar to _mm_cvttepi32_pd
#undef _mm_trunc_pd
#define _mm_trunc_pd _mm_trunc_pd_impl
__forceinline __m128d _mm_trunc_pd_impl(__m128d a) {
	return _mm_round_pd(a, _MM_FROUND_TRUNC);
}
#endif

