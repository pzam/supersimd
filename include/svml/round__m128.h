/***************************************************************************************************/
// HEADER_NAME /svml/round__m128.h
/***************************************************************************************************/

/*************************************************************************************/
// SVML Redefines!
/*************************************************************************************/
#undef _mm_svml_ceil_ps
#define _mm_svml_ceil_ps _mm_svml_ceil_ps_impl
__forceinline __m128 _mm_svml_ceil_ps_impl(__m128 a)
{
	return _mm_ceil_ps(a);
}

#undef _mm_svml_floor_ps
#define _mm_svml_floor_ps _mm_svml_floor_ps_impl
__forceinline __m128 _mm_svml_floor_ps_impl(__m128 a)
{
	return _mm_floor_ps(a);
}

#undef _mm_svml_round_ps
#define _mm_svml_round_ps _mm_svml_round_ps_impl
__forceinline __m128 _mm_svml_round_ps_impl(__m128 a)
{
	return _mm_round_ps(a, _MM_FROUND_TO_NEAREST_INT);
}
/*************************************************************************************/

// Similar to _mm_cvttepi32_ps
#undef _mm_trunc_ps
#define _mm_trunc_ps _mm_trunc_ps_impl
__forceinline __m128 _mm_trunc_ps_impl(__m128 a) {
	return _mm_round_ps(a, _MM_FROUND_TRUNC);
}

