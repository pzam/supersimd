/***************************************************************************************************/
// HEADER_NAME /svml/distribution__m256d.h
/***************************************************************************************************/

#if __AVX2__
#undef _mm256_erf_pd
#define _mm256_erf_pd _mm256_erf_pd_impl
__forceinline __m256d _mm256_erf_pd_impl(__m256d a) {
	__m256d sign_bit, t, y, ex;
	
	sign_bit = _mm256_preservesignbit_pd(a);
	a = _mm256_abs_pd(a);
	
	ex = _mm256_exp_pd(_mm256_mul_pd(_mm256_chs_pd(a), a));
	t = _mm256_rcp14_pd(_mm256_fmadd_pd(__m256d_erf_p0, a, __m256d_1));
	
	
	y = _mm256_fmadd_pd(__m256d_erf_p1, t, __m256d_erf_p2);
	y = _mm256_fmadd_pd(y, t, __m256d_erf_p3);
	y = _mm256_fmadd_pd(y, t, __m256d_erf_p4);
	y = _mm256_fmadd_pd(y, t, __m256d_erf_p5);
	y = _mm256_mul_pd(y, t);
	y = _mm256_fnmadd_pd(y, ex,__m256d_1);
	
	return _mm256_effectsignbit_pd(sign_bit, y);
}

#undef _mm256_erfc_pd
#define _mm256_erfc_pd _mm256_erfc_pd_impl
__forceinline __m256d _mm256_erfc_pd_impl(__m256d a) {
	return _mm256_sub_pd(__m256d_1, _mm256_erf_pd(a));
}

#undef _mm256_erfinv_pd
#define _mm256_erfinv_pd _mm256_erfinv_pd_impl
__forceinline __m256d _mm256_erfinv_pd_impl(__m256d a) {
	__m256d sign_bit,r,r1,r2, x, y, z;
	
	sign_bit = _mm256_preservesignbit_pd(a);
	a = _mm256_abs_pd(a);
	
	x = _mm256_mul_pd(a, a);
	r1 = _mm256_fmadd_pd(__m256d_erfinv_a0, x, __m256d_erfinv_a1);
	r1 = _mm256_fmadd_pd(r1, x, __m256d_erfinv_a2);
	r1 = _mm256_fmadd_pd(r1, x, __m256d_erfinv_a3);
	
	r1 = _mm256_mul_pd(a, r1);
	
	z = _mm256_fmadd_pd(__m256d_erfinv_b0, x, __m256d_erfinv_b1);
	z = _mm256_fmadd_pd(z, x, __m256d_erfinv_b2);
	z = _mm256_fmadd_pd(z, x, __m256d_erfinv_b3);
	z = _mm256_fmadd_pd(z, x, __m256d_1);
	
	r1 = _mm256_div_pd(r1, z);
	
	
	y = _mm256_sub_pd(__m256d_1, a);
	y = _mm256_log_pd(y);
	y = _mm256_chs_pd(y);
	y = _mm256_div_pd(y, __m256d_2);
	
	r2 = _mm256_fmadd_pd(__m256d_erfinv_c0, y, __m256d_erfinv_c1);
	r2 = _mm256_fmadd_pd(r2, y, __m256d_erfinv_c2);
	r2 = _mm256_fmadd_pd(r2, y, __m256d_erfinv_c3);
	
	z = _mm256_fmadd_pd(__m256d_erfinv_d0, y, __m256d_erfinv_d1);
	z = _mm256_fmadd_pd(z, y, __m256d_1);
	
	r2 = _mm256_div_pd(r2, z);
	
	
	r = _mm256_max_pd(r1, r2);
	r = _mm256_effectsignbit_pd(sign_bit, r);
	a = _mm256_effectsignbit_pd(sign_bit, a);
	
	
	y = _mm256_erf_pd(r);
	y = _mm256_sub_pd(y, a);
	
	z = _mm256_mul_pd(_mm256_chs_pd(r), r);
	z = _mm256_exp_pd(z);
	z = _mm256_mul_pd(__m256d_SQRTPI, z);
	z = _mm256_div_pd(__m256d_2, z);
	
	y = _mm256_div_pd(y, z);
	
	r = _mm256_sub_pd(r, y);
	
	
	y = _mm256_erf_pd(r);
	y = _mm256_sub_pd(y, a);
	
	z = _mm256_mul_pd(_mm256_chs_pd(r), r);
	z = _mm256_mul_pd(__m256d_SQRTPI, z);
	z = _mm256_div_pd(__m256d_2, z);
	
	y = _mm256_div_pd(y, z);
	
	return  _mm256_sub_pd(r, y);
}

#undef _mm256_erfcinv_pd
#define _mm256_erfcinv_pd _mm256_erfcinv_pd_impl
__forceinline __m256d _mm256_erfcinv_pd_impl(__m256d a) {
	return _mm256_erfinv_pd(_mm256_sub_pd(__m256d_1, a));
}

#undef _mm256_cdfnorm_pd
#define _mm256_cdfnorm_pd _mm256_cdfnorm_pd_impl
__forceinline __m256d _mm256_cdfnorm_pd_impl(__m256d a) {
	__m256d sign_bit, t, y, ex;
	
	sign_bit = _mm256_preservesignbit_pd(a);
	
	a = _mm256_div_pd(_mm256_abs_pd(a), _mm256_sqrt_pd(__m256d_2));
	
	t = _mm256_rcp14_pd(_mm256_fmadd_pd(__m256d_erf_p0,a,__m256d_1));
	ex = _mm256_exp_pd(_mm256_mul_pd(_mm256_chs_pd(a), a));
	
	y = _mm256_fmadd_pd(__m256d_erf_p1, t, __m256d_erf_p2);
	y = _mm256_fmadd_pd(y, t, __m256d_erf_p3);
	y = _mm256_fmadd_pd(y, t, __m256d_erf_p4);
	y = _mm256_fmadd_pd(y, t, __m256d_erf_p5);
	y = _mm256_mul_pd(y, t);
	y = _mm256_fnmadd_pd(y, ex, __m256d_1);
	
	return _mm256_mul_pd(__m256d_1o2, _mm256_add_pd(_mm256_effectsignbit_pd(sign_bit, y), __m256d_1));
}

/***************************************************************************************************/
// NOT PART OF SVML 
// Alternate name to cdfnorm
#undef _mm256_phi_pd
#define _mm256_phi_pd _mm256_phi_pd_impl
__forceinline __m256d _mm256_phi_pd(__m256d a) {
	return _mm256_cdfnorm_pd(a);
}
/***************************************************************************************************/

#undef _mm256_cdfnorminv_pd
#define _mm256_cdfnorminv_pd _mm256_cdfnorminv_pd_impl
__forceinline __m256d _mm256_cdfnorminv_pd_impl(__m256d a) {
	__m256d y;
	
	y = _mm256_fmsub_pd(__m256d_2, a, __m256d_1);
	y = _mm256_erfinv_pd(y);
	y = _mm256_mul_pd(__m256d_2, y);
	
	return _mm256_div_pd(_mm256_mul_pd(_mm256_sqrt_pd(__m256d_2),y), __m256d_2);
}
#endif

