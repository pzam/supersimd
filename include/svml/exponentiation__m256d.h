/***************************************************************************************************/
// HEADER_NAME /svml/exponentiation__m256d.h
/***************************************************************************************************/

/*************************************************************************************/
// SVML Redefines!
/*************************************************************************************/
#if __AVX__
#undef _mm256_svml_sqrt_pd
#define _mm256_svml_sqrt_pd _mm256_svml_sqrt_pd_impl
__forceinline __m256d _mm256_svml_sqrt_pd_impl(__m256d a)
{
	return _mm256_sqrt_pd(a);
}
#endif
/*************************************************************************************/
#if __AVX2__
#undef _mm256_log_pd
#define _mm256_log_pd _mm256_log_pd_impl
__forceinline __m256d _mm256_log_pd_impl(__m256d a) {
	__m256d invalid_mask, e, aa, mask, y;
	__m256i_i64 emm0;
	
	invalid_mask = _mm256_cmple_pd(a, _mm256_setzero_pd());
	
	a = _mm256_max_pd(a, __m256d_min_norm_pos);
	
	emm0 = _mm256_srli_epi64(_mm256_castpd_epi64(a), IEE754_64BIT_MANTISSA);
	
	
	a = _mm256_and_pd(a, __m256d_inv_mant_mask);
	a = _mm256_or_pd(a, __m256d_1o2);
	
	emm0 = _mm256_sub_epi64(emm0, __m256i_i64_0x400);
	e = _mm256_cvtepi64_pd(emm0);
	e = _mm256_add_pd(e, __m256d_1);
	
	mask = _mm256_cmplt_pd(a, __m256d_SQRTHF);
	y = _mm256_and_pd(a, mask);
	
	e = _mm256_sub_pd(e, _mm256_and_pd(__m256d_1, mask));
	
	a = _mm256_sub_pd(a, __m256d_1);
	a = _mm256_add_pd(a, y);
	
	aa = _mm256_mul_pd(a,a);
	
	y = __m256d_log_p0;
	y = _mm256_fmadd_pd(y, a, __m256d_log_p1);
	y = _mm256_fmadd_pd(y, a, __m256d_log_p2);
	y = _mm256_fmadd_pd(y, a, __m256d_log_p3);
	y = _mm256_fmadd_pd(y, a, __m256d_log_p4);
	y = _mm256_fmadd_pd(y, a, __m256d_log_p5);
	y = _mm256_fmadd_pd(y, a, __m256d_log_p6);
	y = _mm256_fmadd_pd(y, a, __m256d_log_p7);
	y = _mm256_fmadd_pd(y, a, __m256d_log_p8);
	y = _mm256_mul_pd(y, a);
	y = _mm256_mul_pd(y, aa);
	y = _mm256_fmadd_pd(e, __m256d_log_q1, y);
	y = _mm256_fnmadd_pd(aa, __m256d_1o2, y);
	
	a = _mm256_add_pd(a, y);
	a = _mm256_fmadd_pd(e, __m256d_log_q2, a);
	
	return _mm256_or_pd(a, invalid_mask);
}

#undef _mm256_log1p_pd
#define _mm256_log1p_pd _mm256_log1p_pd_impl
__forceinline __m256d _mm256_log1p_pd_impl(__m256d a) {
	return _mm256_log_pd(_mm256_add_pd(__m256d_1, a));
}

/*************************************************************************************/
// NOT PART OF SVML 
// Recommended only use with high base.
#undef _mm256_logn_pd
#define _mm256_logn_pd _mm256_logn_pd_impl
__forceinline __m256d _mm256_logn_pd_impl(__m256d a, __m256d b)
{
	return _mm256_div_pd(_mm256_log_pd(b),_mm256_log_pd(a));
}
/*************************************************************************************/

#undef _mm256_log2_pd
#define _mm256_log2_pd _mm256_log2_pd_impl
__forceinline __m256d _mm256_log2_pd_impl(__m256d a)
{
	return _mm256_mul_pd(_mm256_log_pd(a), __m256d_LOG2EF);
}

#undef _mm256_logb_pd
#define _mm256_logb_pd _mm256_logb_pd_impl
__forceinline __m256d _mm256_logb_pd_impl(__m256d a)
{
	return _mm256_floor_pd(_mm256_log2_pd(_mm256_abs_pd(a)));
} 

#undef _mm256_log10_pd
#define _mm256_log10_pd _mm256_log10_pd_impl
__forceinline __m256d _mm256_log10_pd_impl(__m256d a)
{
	return _mm256_mul_pd(_mm256_log_pd(a), __m256d_LOG10EF);
} 

#undef _mm256_exp_pd
#define _mm256_exp_pd _mm256_exp_pd_impl
__forceinline __m256d _mm256_exp_pd_impl(__m256d a) {
	__m256d ex, aa, y, z;
	__m256i_i64 emm0;
	
	a = _mm256_min_pd(a, __m256d_exp_hi);
	a = _mm256_max_pd(a, __m256d_exp_lo);
	
	ex = _mm256_fmadd_pd(a, __m256d_LOG2EF, __m256d_1o2);
	ex = _mm256_floor_pd(ex);
	
	a = _mm256_fnmadd_pd(ex, __m256d_exp_C1,a);
	a = _mm256_fnmadd_pd(ex, __m256d_exp_C2,a);
	
	aa = _mm256_mul_pd(a,a);
	
	y = __m256d_exp_p0;
	y = _mm256_fmadd_pd(y, a, __m256d_exp_p1);
	y = _mm256_fmadd_pd(y, a, __m256d_exp_p2);
	y = _mm256_fmadd_pd(y, a, __m256d_exp_p3);
	y = _mm256_fmadd_pd(y, a, __m256d_exp_p4);
	y = _mm256_fmadd_pd(y, a, __m256d_exp_p5);
	y = _mm256_fmadd_pd(y, aa, a);
	y = _mm256_add_pd(y, __m256d_1);
	
	emm0 = _mm256_cvttpd_epi64(ex);
	emm0 = _mm256_add_epi64(emm0, __m256i_i64_0x400);
	emm0 = _mm256_slli_epi64(emm0, IEE754_64BIT_MANTISSA);
	
	z = _mm256_castepi64_pd(emm0);
	
	return _mm256_mul_pd(_mm256_mul_pd(y, z), __m256d_1o2);
}

#undef _mm256_expm1_pd
#define _mm256_expm1_pd _mm256_expm1_pd_impl
__forceinline __m256d _mm256_expm1_pd_impl(__m256d a) {
	return _mm256_sub_pd(_mm256_exp_pd(a), __m256d_1);
}

#undef _mm256_exp2_pd
#define _mm256_exp2_pd _mm256_exp2_pd_impl
__forceinline __m256d _mm256_exp2_pd_impl(__m256d a) {
	return _mm256_exp_pd(_mm256_mul_pd(__m256d_LOG2, a));
}

#undef _mm256_exp10_pd
#define _mm256_exp10_pd _mm256_exp10_pd_impl
__forceinline __m256d _mm256_exp10_pd_impl(__m256d a) {
	return _mm256_exp_pd(_mm256_mul_pd(__m256d_LOG10, a));
}

#undef _mm256_pow_pd
#define _mm256_pow_pd _mm256_pow_pd_impl
__forceinline __m256d _mm256_pow_pd_impl(__m256d a,__m256d b){
	return _mm256_exp_pd( _mm256_mul_pd(_mm256_log_pd(_mm256_abs_pd(a)), b) );
}

/*************************************************************************************/
// NOT PART OF SVML 
// Alternate name to exp2
#undef _mm256_pow2_pd
#define _mm256_pow2_pd _mm256_pow2_pd_impl
__forceinline __m256d _mm256_pow2_pd_impl(__m256d a){
	return _mm256_exp2_pd(a);
}
// NOT PART OF SVML 
// Alternate name to exp10
#undef _mm256_pow10_pd
#define _mm256_pow10_pd _mm256_pow10_pd_impl
__forceinline __m256d _mm256_pow10_pd_impl(__m256d a) {
	return _mm256_exp10_pd(a);
}
// NOT PART OF SVML 
#undef _mm256_nthrt_pd
#define _mm256_nthrt_pd _mm256_nthrt_pd_impl
__forceinline __m256d _mm256_nthrt_pd_impl(__m256d a, __m256d b) {
	__m256d sign_bit = _mm256_preservesignbit_pd(a);
	return _mm256_effectsignbit_pd(_mm256_exp_pd(_mm256_mul_pd(_mm256_log_pd(_mm256_abs_pd(a)), _mm256_rcp14_pd(b))), sign_bit);
}
// NOT PART OF SVML 
#undef _mm256_rnthrt_pd
#define _mm256_rnthrt_pd _mm256_rnthrt_pd_impl
__forceinline __m256d _mm256_rnthrt_pd_impl(__m256d a, __m256d b) {
	return _mm256_rcp14_pd(_mm256_nthrt_pd( a, b));
}
/*************************************************************************************/

#undef _mm256_cbrt_pd
#define _mm256_cbrt_pd _mm256_cbrt_pd_impl
__forceinline __m256d _mm256_cbrt_pd_impl(__m256d a) {
	__m256d three = __m256d_3;
	return _mm256_nthrt_pd(a, three);
}
#endif
#if __AVX__
#undef _mm256_invsqrt_pd
#define _mm256_invsqrt_pd _mm256_invsqrt_pd_impl
__forceinline __m256d _mm256_invsqrt_pd_impl(__m256d a) {
	return _mm256_rsqrt_pd(a);
}
#endif
#if __AVX2__
#undef _mm256_invcbrt_pd
#define _mm256_invcbrt_pd _mm256_invcbrt_pd_impl
__forceinline __m256d _mm256_invcbrt_pd_impl(__m256d a) {
	__m256d three = __m256d_3;
	return _mm256_rnthrt_pd(a, __m256d_3);
}
#endif

