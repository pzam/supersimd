/***************************************************************************************************/
// HEADER_NAME /svml/distribution__m256.h
/***************************************************************************************************/

#if __AVX2__
#undef _mm256_erf_ps
#define _mm256_erf_ps _mm256_erf_ps_impl
__forceinline __m256 _mm256_erf_ps_impl(__m256 a) {
	__m256 sign_bit, t, y, ex;
	
	sign_bit = _mm256_preservesignbit_ps(a);
	a = _mm256_abs_ps(a);
	
	ex = _mm256_exp_ps(_mm256_mul_ps(_mm256_chs_ps(a), a));
	t = _mm256_rcp_ps(_mm256_fmadd_ps(__m256_erf_p0, a, __m256_1));
	
	
	y = _mm256_fmadd_ps(__m256_erf_p1, t, __m256_erf_p2);
	y = _mm256_fmadd_ps(y, t, __m256_erf_p3);
	y = _mm256_fmadd_ps(y, t, __m256_erf_p4);
	y = _mm256_fmadd_ps(y, t, __m256_erf_p5);
	y = _mm256_mul_ps(y, t);
	y = _mm256_fnmadd_ps(y, ex,__m256_1);
	
	return _mm256_effectsignbit_ps(sign_bit, y);
}

#undef _mm256_erfc_ps
#define _mm256_erfc_ps _mm256_erfc_ps_impl
__forceinline __m256 _mm256_erfc_ps_impl(__m256 a) {
	return _mm256_sub_ps(__m256_1, _mm256_erf_ps(a));
}

#undef _mm256_erfinv_ps
#define _mm256_erfinv_ps _mm256_erfinv_ps_impl
__forceinline __m256 _mm256_erfinv_ps_impl(__m256 a) {
	__m256 sign_bit,r,r1,r2, x, y, z;
	
	sign_bit = _mm256_preservesignbit_ps(a);
	a = _mm256_abs_ps(a);
	
	x = _mm256_mul_ps(a, a);
	r1 = _mm256_fmadd_ps(__m256_erfinv_a0, x, __m256_erfinv_a1);
	r1 = _mm256_fmadd_ps(r1, x, __m256_erfinv_a2);
	r1 = _mm256_fmadd_ps(r1, x, __m256_erfinv_a3);
	
	r1 = _mm256_mul_ps(a, r1);
	
	z = _mm256_fmadd_ps(__m256_erfinv_b0, x, __m256_erfinv_b1);
	z = _mm256_fmadd_ps(z, x, __m256_erfinv_b2);
	z = _mm256_fmadd_ps(z, x, __m256_erfinv_b3);
	z = _mm256_fmadd_ps(z, x, __m256_1);
	
	r1 = _mm256_div_ps(r1, z);
	
	
	y = _mm256_sub_ps(__m256_1, a);
	y = _mm256_log_ps(y);
	y = _mm256_chs_ps(y);
	y = _mm256_div_ps(y, __m256_2);
	
	r2 = _mm256_fmadd_ps(__m256_erfinv_c0, y, __m256_erfinv_c1);
	r2 = _mm256_fmadd_ps(r2, y, __m256_erfinv_c2);
	r2 = _mm256_fmadd_ps(r2, y, __m256_erfinv_c3);
	
	z = _mm256_fmadd_ps(__m256_erfinv_d0, y, __m256_erfinv_d1);
	z = _mm256_fmadd_ps(z, y, __m256_1);
	
	r2 = _mm256_div_ps(r2, z);
	
	
	r = _mm256_max_ps(r1, r2);
	r = _mm256_effectsignbit_ps(sign_bit, r);
	a = _mm256_effectsignbit_ps(sign_bit, a);
	
	
	y = _mm256_erf_ps(r);
	y = _mm256_sub_ps(y, a);
	
	z = _mm256_mul_ps(_mm256_chs_ps(r), r);
	z = _mm256_exp_ps(z);
	z = _mm256_mul_ps(__m256_SQRTPI, z);
	z = _mm256_div_ps(__m256_2, z);
	
	y = _mm256_div_ps(y, z);
	
	r = _mm256_sub_ps(r, y);
	
	
	y = _mm256_erf_ps(r);
	y = _mm256_sub_ps(y, a);
	
	z = _mm256_mul_ps(_mm256_chs_ps(r), r);
	z = _mm256_mul_ps(__m256_SQRTPI, z);
	z = _mm256_div_ps(__m256_2, z);
	
	y = _mm256_div_ps(y, z);
	
	return  _mm256_sub_ps(r, y);
}

#undef _mm256_erfcinv_ps
#define _mm256_erfcinv_ps _mm256_erfcinv_ps_impl
__forceinline __m256 _mm256_erfcinv_ps_impl(__m256 a) {
	return _mm256_erfinv_ps(_mm256_sub_ps(__m256_1, a));
}

#undef _mm256_cdfnorm_ps
#define _mm256_cdfnorm_ps _mm256_cdfnorm_ps_impl
__forceinline __m256 _mm256_cdfnorm_ps_impl(__m256 a) {
	__m256 sign_bit, t, y, ex;
	
	sign_bit = _mm256_preservesignbit_ps(a);
	
	a = _mm256_div_ps(_mm256_abs_ps(a), _mm256_sqrt_ps(__m256_2));
	
	t = _mm256_rcp_ps(_mm256_fmadd_ps(__m256_erf_p0,a,__m256_1));
	ex = _mm256_exp_ps(_mm256_mul_ps(_mm256_chs_ps(a), a));
	
	y = _mm256_fmadd_ps(__m256_erf_p1, t, __m256_erf_p2);
	y = _mm256_fmadd_ps(y, t, __m256_erf_p3);
	y = _mm256_fmadd_ps(y, t, __m256_erf_p4);
	y = _mm256_fmadd_ps(y, t, __m256_erf_p5);
	y = _mm256_mul_ps(y, t);
	y = _mm256_fnmadd_ps(y, ex, __m256_1);
	
	return _mm256_mul_ps(__m256_1o2, _mm256_add_ps(_mm256_effectsignbit_ps(sign_bit, y), __m256_1));
}

/***************************************************************************************************/
// NOT PART OF SVML 
// Alternate name to cdfnorm
#undef _mm256_phi_ps
#define _mm256_phi_ps _mm256_phi_ps_impl
__forceinline __m256 _mm256_phi_ps_impl(__m256 a) {
	return _mm256_cdfnorm_ps(a);
}
/***************************************************************************************************/

#undef _mm256_cdfnorminv_ps
#define _mm256_cdfnorminv_ps _mm256_cdfnorminv_ps_impl
__forceinline __m256 _mm256_cdfnorminv_ps_impl(__m256 a) {
	__m256 y;
	
	y = _mm256_fmsub_ps(__m256_2, a, __m256_1);
	y = _mm256_erfinv_ps(y);
	y = _mm256_mul_ps(__m256_2, y);
	
	return _mm256_div_ps(_mm256_mul_ps(_mm256_sqrt_ps(__m256_2),y), __m256_2);
}
#endif

