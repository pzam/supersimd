/***************************************************************************************************/
// HEADER_NAME /svml/exponentiation__m256.h
/***************************************************************************************************/

/*************************************************************************************/
// SVML Redefines!
/*************************************************************************************/
#if __AVX__
#undef _mm256_svml_sqrt_ps
#define _mm256_svml_sqrt_ps _mm256_svml_sqrt_ps_impl
__forceinline __m256 _mm256_svml_sqrt_ps_impl(__m256 a)
{
	return _mm256_sqrt_ps(a);
}
#endif
/*************************************************************************************/
#if __AVX2__
#undef _mm256_log_ps
#define _mm256_log_ps _mm256_log_ps_impl
__forceinline __m256 _mm256_log_ps_impl(__m256 a) {
	__m256 invalid_mask, e, aa, mask, y;
	__m256i_i32 emm0;
	
	invalid_mask = _mm256_cmple_ps(a, _mm256_setzero_ps());
	
	a = _mm256_max_ps(a, __m256_min_norm_pos);	/* cut off demoralized stuff */
	
	emm0 = _mm256_srli_epi32(_mm256_castps_epi32(a), IEE754_32BIT_MANTISSA);
	
	a = _mm256_and_ps(a, __m256_inv_mant_mask);
	a = _mm256_or_ps(a, __m256_1o2);
	
	emm0 = _mm256_sub_epi32(emm0, __m256i_i32_0x7f);
	e = _mm256_cvtepi32_ps(emm0);
	e = _mm256_add_ps(e, __m256_1);
	
	mask = _mm256_cmplt_ps(a, __m256_SQRTHF);
	y = _mm256_and_ps(a, mask);
	
	e = _mm256_sub_ps(e, _mm256_and_ps(__m256_1, mask));
	
	a = _mm256_sub_ps(a, __m256_1);
	a = _mm256_add_ps(a, y);
	
	aa = _mm256_mul_ps(a,a);
	
	y = __m256_log_p0;
	y = _mm256_fmadd_ps(y, a, __m256_log_p1);
	y = _mm256_fmadd_ps(y, a, __m256_log_p2);
	y = _mm256_fmadd_ps(y, a, __m256_log_p3);
	y = _mm256_fmadd_ps(y, a, __m256_log_p4);
	y = _mm256_fmadd_ps(y, a, __m256_log_p5);
	y = _mm256_fmadd_ps(y, a, __m256_log_p6);
	y = _mm256_fmadd_ps(y, a, __m256_log_p7);
	y = _mm256_fmadd_ps(y, a, __m256_log_p8);
	y = _mm256_mul_ps(y, a);
	y = _mm256_mul_ps(y, aa);
	y = _mm256_fmadd_ps(e, __m256_log_q1, y);
	y = _mm256_fnmadd_ps(aa, __m256_1o2, y);
	
	a = _mm256_add_ps(a, y);
	a = _mm256_fmadd_ps(e, __m256_log_q2, a);
	
	return _mm256_or_ps(a, invalid_mask); // negative arg will be NAN
}

#undef _mm256_log1p_ps
#define _mm256_log1p_ps _mm256_log1p_ps_impl
__forceinline __m256 _mm256_log1p_ps_impl(__m256 a) {
	return _mm256_log_ps(_mm256_add_ps(__m256_1, a));
}

/*************************************************************************************/
// NOT PART OF SVML 
#undef _mm256_logn_ps
#define _mm256_logn_ps _mm256_logn_ps_impl
__forceinline __m256 _mm256_logn_ps_impl(__m256 a, __m256 b)
{
	return _mm256_div_ps(_mm256_log_ps(b), _mm256_log_ps(a));
}
/*************************************************************************************/
 
#undef _mm256_log2_ps
#define _mm256_log2_ps _mm256_log2_ps_impl
__forceinline __m256 _mm256_log2_ps_impl(__m256 a)
{
	return _mm256_mul_ps(_mm256_log_ps(a), __m256_LOG2EF);
}

#undef _mm256_logb_ps
#define _mm256_logb_ps _mm256_logb_ps_impl
__forceinline __m256 _mm256_logb_ps_impl(__m256 a)
{
	return _mm256_floor_ps(_mm256_log2_ps(_mm256_abs_ps(a)));
} 

#undef _mm256_log10_ps
#define _mm256_log10_ps _mm256_log10_ps_impl
__forceinline __m256 _mm256_log10_ps_impl(__m256 a)
{
	return _mm256_mul_ps(_mm256_log_ps(a), __m256_LOG10EF);
} 

#undef _mm256_exp_ps
#define _mm256_exp_ps _mm256_exp_ps_impl
__forceinline __m256 _mm256_exp_ps_impl(__m256 a) {
	__m256 ex, aa, y, z;
	__m256i_i32 emm0;
	
	a = _mm256_min_ps(a, __m256_exp_hi);
	a = _mm256_max_ps(a, __m256_exp_lo);
	
	ex = _mm256_fmadd_ps(a, __m256_LOG2EF, __m256_1o2);
	ex = _mm256_floor_ps(ex);
	
	a = _mm256_fnmadd_ps(ex, __m256_exp_C1,a);
	a = _mm256_fnmadd_ps(ex, __m256_exp_C2,a);
	
	aa = _mm256_mul_ps(a,a);
	
	y = __m256_exp_p0;
	y = _mm256_fmadd_ps(y, a, __m256_exp_p1);
	y = _mm256_fmadd_ps(y, a, __m256_exp_p2);
	y = _mm256_fmadd_ps(y, a, __m256_exp_p3);
	y = _mm256_fmadd_ps(y, a, __m256_exp_p4);
	y = _mm256_fmadd_ps(y, a, __m256_exp_p5);
	y = _mm256_fmadd_ps(y, aa, a);
	y = _mm256_add_ps(y, __m256_1);
	
	emm0 = _mm256_cvttps_epi32(ex);
	emm0 = _mm256_add_epi32(emm0, __m256i_i32_0x7f);
	emm0 = _mm256_slli_epi32(emm0, IEE754_32BIT_MANTISSA);
	
	z = _mm256_castepi32_ps(emm0);
	
	return _mm256_mul_ps(y, z);
}

#undef _mm256_expm1_ps
#define _mm256_expm1_ps _mm256_expm1_ps_impl
__forceinline __m256 _mm256_expm1_ps_impl(__m256 a) {
	return _mm256_sub_ps(_mm256_exp_ps(a), __m256_1);
}

#undef _mm256_exp2_ps
#define _mm256_exp2_ps _mm256_exp2_ps_impl
__forceinline __m256 _mm256_exp2_ps_impl(__m256 a) {
	return _mm256_exp_ps(_mm256_mul_ps(__m256_LOG2, a));
}

#undef _mm256_exp10_ps
#define _mm256_exp10_ps _mm256_exp10_ps_impl
__forceinline __m256 _mm256_exp10_ps_impl(__m256 a) {
	return _mm256_exp_ps(_mm256_mul_ps(__m256_LOG10, a));
}

#undef _mm256_pow_ps
#define _mm256_pow_ps _mm256_pow_ps_impl
__forceinline __m256 _mm256_pow_ps_impl(__m256 a,__m256 b){
	return _mm256_exp_ps( _mm256_mul_ps(_mm256_log_ps(_mm256_abs_ps(a)), b) );
}

/*************************************************************************************/
// NOT PART OF SVML 
// Alternate name to exp2
#undef _mm256_pow2_ps
#define _mm256_pow2_ps _mm256_pow2_ps_impl
__forceinline __m256 _mm256_pow2_ps_impl(__m256 a){
	return _mm256_exp2_ps(a);
}
// NOT PART OF SVML 
// Alternate name to exp10
#undef _mm256_pow10_ps
#define _mm256_pow10_ps _mm256_pow10_ps_impl
__forceinline __m256 _mm256_pow10_ps_impl(__m256 a) {
	return _mm256_exp10_ps(a);
}
// NOT PART OF SVML 
#undef _mm256_nthrt_ps
#define _mm256_nthrt_ps _mm256_nthrt_ps_impl
__forceinline __m256 _mm256_nthrt_ps_impl(__m256 a, __m256 b) {
	__m256 sign_bit = _mm256_preservesignbit_ps(a);
	return _mm256_effectsignbit_ps(_mm256_exp_ps(_mm256_mul_ps(_mm256_log_ps(_mm256_abs_ps(a)), _mm256_rcp_ps(b))), sign_bit);
}
// NOT PART OF SVML 
#undef _mm256_rnthrt_ps
#define _mm256_rnthrt_ps _mm256_rnthrt_ps_impl
__forceinline __m256 _mm256_rnthrt_ps_impl(__m256 a, __m256 b) {
	return _mm256_rcp_ps(_mm256_nthrt_ps( a, b));
}
/*************************************************************************************/

#undef _mm256_cbrt_ps
#define _mm256_cbrt_ps _mm256_cbrt_ps_impl
__forceinline __m256 _mm256_cbrt_ps_impl(__m256 a) {
	__m256 three = __m256_3;
	return _mm256_nthrt_ps(a, three);
}
#endif
#if __AVX__
#undef _mm256_invsqrt_ps
#define _mm256_invsqrt_ps _mm256_invsqrt_ps_impl
__forceinline __m256 _mm256_invsqrt_ps_impl(__m256 a) {
	return _mm256_rsqrt_ps(a);
}
#endif
#if __AVX2__
#undef _mm256_invcbrt_ps
#define _mm256_invcbrt_ps _mm256_invcbrt_ps_impl
__forceinline __m256 _mm256_invcbrt_ps_impl(__m256 a) {
	__m256 three = __m256_3;
	return _mm256_rnthrt_ps(a, __m256_3);
}
#endif

