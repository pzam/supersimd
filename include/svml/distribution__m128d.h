/***************************************************************************************************/
// HEADER_NAME /svml/distribution__m128d.h
/***************************************************************************************************/

#if BIT64
#undef _mm_erf_pd
#define _mm_erf_pd _mm_erf_pd_impl
__forceinline __m128d _mm_erf_pd_impl(__m128d a) {
	__m128d sign_bit, t, y, ex;
	
	sign_bit = _mm_preservesignbit_pd(a);
	a = _mm_abs_pd(a);
	
	ex = _mm_exp_pd(_mm_mul_pd(_mm_chs_pd(a), a));
	t = _mm_rcp14_pd(_mm_fmadd_pd(__m128d_erf_p0, a, __m128d_1));
	
	
	y = _mm_fmadd_pd(__m128d_erf_p1, t, __m128d_erf_p2);
	y = _mm_fmadd_pd(y, t, __m128d_erf_p3);
	y = _mm_fmadd_pd(y, t, __m128d_erf_p4);
	y = _mm_fmadd_pd(y, t, __m128d_erf_p5);
	y = _mm_mul_pd(y, t);
	y = _mm_fnmadd_pd(y, ex,__m128d_1);
	
	return _mm_effectsignbit_pd(sign_bit, y);
}

#undef _mm_erfc_pd
#define _mm_erfc_pd _mm_erfc_pd_impl
__forceinline __m128d _mm_erfc_pd_impl(__m128d a) {
	return _mm_sub_pd(__m128d_1, _mm_erf_pd(a));
}

#undef _mm_erfinv_pd
#define _mm_erfinv_pd _mm_erfinv_pd_impl
__forceinline __m128d _mm_erfinv_pd_impl(__m128d a) {
	__m128d sign_bit,r,r1,r2, x, y, z;
	
	sign_bit = _mm_preservesignbit_pd(a);
	a = _mm_abs_pd(a);
	
	x = _mm_mul_pd(a, a);
	r1 = _mm_fmadd_pd(__m128d_erfinv_a0, x, __m128d_erfinv_a1);
	r1 = _mm_fmadd_pd(r1, x, __m128d_erfinv_a2);
	r1 = _mm_fmadd_pd(r1, x, __m128d_erfinv_a3);
	
	r1 = _mm_mul_pd(a, r1);
	
	z = _mm_fmadd_pd(__m128d_erfinv_b0, x, __m128d_erfinv_b1);
	z = _mm_fmadd_pd(z, x, __m128d_erfinv_b2);
	z = _mm_fmadd_pd(z, x, __m128d_erfinv_b3);
	z = _mm_fmadd_pd(z, x, __m128d_1);
	
	r1 = _mm_div_pd(r1, z);
	
	y = _mm_sub_pd(__m128d_1, a);
	y = _mm_log_pd(y);
	y = _mm_chs_pd(y);
	y = _mm_div_pd(y, __m128d_2);
	
	r2 = _mm_fmadd_pd(__m128d_erfinv_c0, y, __m128d_erfinv_c1);
	r2 = _mm_fmadd_pd(r2, y, __m128d_erfinv_c2);
	r2 = _mm_fmadd_pd(r2, y, __m128d_erfinv_c3);
	
	z = _mm_fmadd_pd(__m128d_erfinv_d0, y, __m128d_erfinv_d1);
	z = _mm_fmadd_pd(z, y, __m128d_1);
	
	r2 = _mm_div_pd(r2, z);
	
	r = _mm_max_pd(r1, r2);
	r = _mm_effectsignbit_pd(sign_bit, r);
	a = _mm_effectsignbit_pd(sign_bit, a);
	
	y = _mm_erf_pd(r);
	y = _mm_sub_pd(y, a);
	
	z = _mm_mul_pd(_mm_chs_pd(r), r);
	z = _mm_exp_pd(z);
	z = _mm_mul_pd(__m128d_SQRTPI, z);
	z = _mm_div_pd(__m128d_2, z);
	
	y = _mm_div_pd(y, z);
	
	r = _mm_sub_pd(r, y);
	
	y = _mm_erf_pd(r);
	y = _mm_sub_pd(y, a);
	
	z = _mm_mul_pd(_mm_chs_pd(r), r);
	z = _mm_mul_pd(__m128d_SQRTPI, z);
	z = _mm_div_pd(__m128d_2, z);
	
	y = _mm_div_pd(y, z);
	
	return  _mm_sub_pd(r, y);
}

#undef _mm_erfcinv_pd
#define _mm_erfcinv_pd _mm_erfcinv_pd_impl
__forceinline __m128d _mm_erfcinv_pd_impl(__m128d a) {
	return _mm_erfinv_pd(_mm_sub_pd(__m128d_1, a));
}

#undef _mm_cdfnorm_pd
#define _mm_cdfnorm_pd _mm_cdfnorm_pd_impl
__forceinline __m128d _mm_cdfnorm_pd_impl(__m128d a) {
	__m128d sign_bit, t, y, ex;
	
	sign_bit = _mm_preservesignbit_pd(a);
	
	a = _mm_div_pd(_mm_abs_pd(a), _mm_sqrt_pd(__m128d_2));
	
	t = _mm_rcp14_pd(_mm_fmadd_pd(__m128d_erf_p0,a,__m128d_1));
	ex = _mm_exp_pd(_mm_mul_pd(_mm_chs_pd(a), a));
	
	y = _mm_fmadd_pd(__m128d_erf_p1, t, __m128d_erf_p2);
	y = _mm_fmadd_pd(y, t, __m128d_erf_p3);
	y = _mm_fmadd_pd(y, t, __m128d_erf_p4);
	y = _mm_fmadd_pd(y, t, __m128d_erf_p5);
	y = _mm_mul_pd(y, t);
	y = _mm_fnmadd_pd(y, ex, __m128d_1);
	
	return _mm_mul_pd(__m128d_1o2, _mm_add_pd(_mm_effectsignbit_pd(sign_bit, y), __m128d_1));
}

/***************************************************************************************************/
// NOT PART OF SVML 
// Alternate name to cdfnorm
#undef _mm_phi_pd
#define _mm_phi_pd _mm_phi_pd_impl
__forceinline __m128d _mm_phi_pd_impl(__m128d a) {
	return _mm_cdfnorm_pd(a);
}
/***************************************************************************************************/
#undef _mm_cdfnorminv_pd
#define _mm_cdfnorminv_pd _mm_cdfnorminv_pd_impl
__forceinline __m128d _mm_cdfnorminv_pd_impl(__m128d a) {
	__m128d y;
	
	y = _mm_fmsub_pd(__m128d_2, a, __m128d_1);
	y = _mm_erfinv_pd(y);
	y = _mm_mul_pd(__m128d_2, y);
	
	return _mm_div_pd(_mm_mul_pd(_mm_sqrt_pd(__m128d_2),y), __m128d_2);
}
#endif

