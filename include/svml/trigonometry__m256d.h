/***************************************************************************************************/
// HEADER_NAME /svml/trigonometry__m256d.h
/***************************************************************************************************/

#if __AVX__
#undef _mm256_hypot_pd
#define _mm256_hypot_pd _mm256_hypot_pd_impl
__forceinline __m256d _mm256_hypot_pd_impl(__m256d a, __m256d b) {
	return _mm256_sqrt_pd( _mm256_fmadd_pd( a,a,_mm256_mul_pd(b,b) ) );
}
#endif
#if __AVX2__
#undef _mm256_sin_pd
#define _mm256_sin_pd _mm256_sin_pd_impl
__forceinline __m256d _mm256_sin_pd_impl(__m256d a) {
	__m256d poly_mask, sign_bit, swap_sign, y, y2, z;
	__m256i_i64	emm0, emm1;
	
	sign_bit = _mm256_preservesignbit_pd(a);
	
	a = _mm256_abs_pd(a);
	
	y = _mm256_mul_pd(a, __m256d_FoPI);
	
	emm0 = _mm256_cvttpd_epi64(y);
	
	emm0 = _mm256_add_epi64(emm0, __m256i_i64_1);
	emm0 = _mm256_and_epi64(emm0, __m256i_i64_inv1);
	y = _mm256_cvtepi64_pd(emm0);
	
	
	emm1 = _mm256_and_epi64(emm0, __m256i_i64_4);
	emm1 = _mm256_slli_epi64(emm1, 29);
	
	emm0 = _mm256_and_epi64(emm0, __m256i_i64_2);
	emm0 = _mm256_cmpeq_epi64(emm0, _mm256_setzero_epi64());
	
	swap_sign = _mm256_castepi64_pd(emm1);
	poly_mask = _mm256_castepi64_pd(emm0);
	sign_bit = _mm256_xor_pd(sign_bit, swap_sign);
	
	a = _mm256_fmadd_pd(y, __m256d_minus_DP1,a);
	a = _mm256_fmadd_pd(y, __m256d_minus_DP2, a);
	a = _mm256_fmadd_pd(y, __m256d_minus_DP3, a);
	
	
	y = __m256d_coscof_p0;
	z = _mm256_mul_pd(a,a);
	
	y = _mm256_fmadd_pd(y, z, __m256d_coscof_p1);
	y = _mm256_fmadd_pd(y, z, __m256d_coscof_p2);
	y = _mm256_mul_pd(y, z);
	y = _mm256_mul_pd(y, z);
	y = _mm256_fnmadd_pd(z, __m256d_1o2, y);
	y = _mm256_add_pd(y, __m256d_1);
	
	
	y2 = __m256d_sincof_p0;
	y2 = _mm256_fmadd_pd(y2, z, __m256d_sincof_p1);
	y2 = _mm256_fmadd_pd(y2, z, __m256d_sincof_p2);
	y2 = _mm256_mul_pd(y2, z);
	y2 = _mm256_fmadd_pd(y2, a, a);

	y = _mm256_blendv_pd(y, y2, poly_mask);
	
	return _mm256_effectsignbit_pd(y, sign_bit);
}

#undef _mm256_sind_pd
#define _mm256_sind_pd _mm256_sind_pd_impl
__forceinline __m256d _mm256_sind_pd_impl(__m256d a) {
	return _mm256_sin_pd(_mm256_mul_pd(a, __m256d_Deg2Rad));
}
#endif
#if __AVX__
#undef _mm256_asin_pd
#define _mm256_asin_pd _mm256_asin_pd_impl
__forceinline __m256d _mm256_asin_pd_impl(__m256d a) {
	__m256d flag, sign_bit, w, y, z;
	
	sign_bit = _mm256_preservesignbit_pd(a);
	a = _mm256_abs_pd(a);
	
	flag = _mm256_cmpgt_pd(a, __m256d_1o2);
	z = _mm256_mul_pd(__m256d_1o2, _mm256_sub_pd(__m256d_1, a));
	w = _mm256_sqrt_pd(z);
	a = _mm256_blendv_pd(a, w, flag);
	z = _mm256_blendv_pd(_mm256_mul_pd(a, a), z, flag);
	
	y = _mm256_fmadd_pd(z, __m256d_asinf_p0, __m256d_asinf_p1);
	y = _mm256_fmadd_pd(y, z, __m256d_asinf_p2);
	y = _mm256_fmadd_pd(y, z, __m256d_asinf_p3);
	y = _mm256_fmadd_pd(y, z, __m256d_asinf_p4);
	y = _mm256_mul_pd(y, z);
	y = _mm256_fmadd_pd(y, a, a);
	
	z = _mm256_sub_pd( __m256d_PIo2F, _mm256_add_pd(y, y));
	y = _mm256_blendv_pd(y, z, flag);
	
	return _mm256_effectsignbit_pd(sign_bit, y);
}

/***************************************************************************************************/
// NOT PART OF SVML 
// Added For Computer Graphics Support
#undef _mm256_radian_asin_pd
#define _mm256_radian_asin_pd _mm256_radian_asin_pd_impl
__forceinline __m256d _mm256_radian_asin_pd_impl(__m256d a)
{
	__m256d y, n, p;
	
	p = _mm256_cmpge_pd(a, __m256d_1);
	n = _mm256_cmple_pd(a, __m256d_minus_1);
	
	y = _mm256_asin_pd(a);
	
	y = _mm256_blendv_pd(y, __m256d_PIo2F, p);
	y = _mm256_blendv_pd(y, __m256d_minus_PIo2F, n);
	
	return y;
}
#endif
/***************************************************************************************************/

#if __AVX2__
#undef _mm256_sinh_pd
#define _mm256_sinh_pd _mm256_sinh_pd_impl
__forceinline __m256d _mm256_sinh_pd_impl(__m256d a) {
	// sinh(a) = ((e^a - e ^ (-a)) / 2)
	__m256d ex;
	
	ex = _mm256_exp_pd(a);
	return _mm256_div_pd(_mm256_sub_pd(ex, _mm256_rcp14_pd(ex)), __m256d_2);
}

#undef _mm256_asinh_pd
#define _mm256_asinh_pd _mm256_asinh_pd_impl
__forceinline __m256d _mm256_asinh_pd_impl(__m256d a) {
	// asinh(a) = ln( a + sqrt(a^2+1) );
	return _mm256_log_pd( _mm256_add_pd(a, _mm256_sqrt_pd(_mm256_fmadd_pd( a, a, __m256d_1) )));
}

#undef _mm256_cos_pd
#define _mm256_cos_pd _mm256_cos_pd_impl
__forceinline __m256d _mm256_cos_pd_impl(__m256d a) {
	__m256d	poly_mask, sign_bit, y, y2, z;
	__m256i_i64 emm0, emm1;
	
	sign_bit = _mm256_preservesignbit_pd(a);
	a = _mm256_abs_pd(a);
	
	y = _mm256_mul_pd(a, __m256d_FoPI);
	
	emm0 = _mm256_cvttpd_epi64(y);
	
	emm0 = _mm256_add_epi64(emm0, __m256i_i64_1);
	emm0 = _mm256_and_epi64(emm0, __m256i_i64_inv1);
	y = _mm256_cvtepi64_pd(emm0);
	
	emm0 = _mm256_sub_epi64(emm0, __m256i_i64_2);
	
	emm1 = _mm256_andnot_epi64(emm0, __m256i_i64_4);
	emm1 = _mm256_slli_epi64(emm1, 29);
	
	emm0 = _mm256_and_epi64(emm0, __m256i_i64_2);
	emm0 = _mm256_cmpeq_epi64(emm0, _mm256_setzero_epi64());
	
	sign_bit = _mm256_castepi64_pd(emm1);
	poly_mask = _mm256_castepi64_pd(emm0);
	
	a = _mm256_fmadd_pd(y, __m256d_minus_DP1, a);
	a = _mm256_fmadd_pd(y, __m256d_minus_DP2, a);
	a = _mm256_fmadd_pd(y, __m256d_minus_DP3, a);
	
	y = __m256d_coscof_p0;
	z = _mm256_mul_pd(a,a);
	
	y = _mm256_fmadd_pd(y, z, __m256d_coscof_p1);
	y = _mm256_fmadd_pd(y, z, __m256d_coscof_p2);
	y = _mm256_mul_pd(y, z);
	y = _mm256_mul_pd(y, z);
	y = _mm256_fnmadd_pd(z, __m256d_1o2, y);
	y = _mm256_add_pd(y, __m256d_1);
	
	y2 = __m256d_sincof_p0;
	
	y2 = _mm256_fmadd_pd(y2, z, __m256d_sincof_p1);
	y2 = _mm256_fmadd_pd(y2, z, __m256d_sincof_p2);
	y2 = _mm256_mul_pd(y2, z);
	y2 = _mm256_fmadd_pd(y2, a, a);
	
	y = _mm256_blendv_pd(y, y2, poly_mask);
	
	return _mm256_effectsignbit_pd(y, sign_bit);
}


#undef _mm256_cosd_pd
#define _mm256_cosd_pd _mm256_cosd_pd_impl
__forceinline __m256d _mm256_cosd_pd_impl(__m256d a) {
	return _mm256_cos_pd(_mm256_mul_pd(a, __m256d_Deg2Rad));
}
#endif
#if __AVX__
#undef _mm256_acos_pd
#define _mm256_acos_pd _mm256_acos_pd_impl
__forceinline __m256d _mm256_acos_pd_impl(__m256d a)
{
	__m256d gt, ngt, lt, mt, p, q, z, s, w, df;
	
	gt = _mm256_cmpgt_pd(a, __m256d_1o2);
	lt = _mm256_cmplt_pd(a, __m256d_minus_1);
	ngt = _mm256_xor_pd(gt, __m256d_maxint);
	mt = _mm256_andnot_pd(lt, ngt);
	
	z = _mm256_mul_pd(_mm256_add_pd(_mm256_or_pd(a, __m256d_sign_mask), __m256d_1), __m256d_1o2);
	z = _mm256_blendv_pd(z, _mm256_mul_pd(a, a), mt);
	s = _mm256_blendv_pd(_mm256_sqrt_pd(z), a, mt);
	
	p = _mm256_fmadd_pd(z, __m256d_acos_p5, __m256d_acos_p4);
	p = _mm256_fmadd_pd(z, p, __m256d_acos_p3);
	p = _mm256_fmadd_pd(z, p, __m256d_acos_p2);
	p = _mm256_fmadd_pd(z, p, __m256d_acos_p1);
	p = _mm256_fmadd_pd(z, p, __m256d_acos_p0);
	p = _mm256_mul_pd(z, p);
	
	q = _mm256_fmadd_pd(z, __m256d_acos_q4,__m256d_acos_q3);
	q = _mm256_fmadd_pd(z, q, __m256d_acos_q2);
	q = _mm256_fmadd_pd(z, q, __m256d_acos_q1);
	q = _mm256_fmadd_pd(z, q, __m256d_1);
	
	p = _mm256_div_pd(p, q);
	
	df = _mm256_and_pd(s, _mm256_or_pd(__m256d_asinf_trunc, ngt));
	q = _mm256_div_pd(_mm256_fnmadd_pd(df, df,z), _mm256_add_pd(s, df));
	gt = _mm256_andnot_pd(_mm256_cmpeq_pd(a, __m256d_1), gt);
	q = _mm256_or_pd(_mm256_and_pd(gt, q), _mm256_and_pd(ngt, __m256d_pio2_lo));
	w = _mm256_fmadd_pd(p, s, q);
	w = _mm256_add_pd(w, df);
	w = _mm256_mul_pd(w, _mm256_blendv_pd(__m256d_2, __m256d_1, mt));
	w = _mm256_xor_pd(w, _mm256_and_pd(__m256d_sign_mask, ngt));
	w = _mm256_add_pd(w, _mm256_and_pd(__m256d_PI, lt));
	w = _mm256_add_pd(w, _mm256_and_pd(__m256d_pio2_hi, mt));

	return w;
}
/***************************************************************************************************/
// NOT PART OF SVML 
// Added For Computer Graphics Support
#undef _mm256_radian_acos_pd
#define _mm256_radian_acos_pd _mm256_radian_acos_pd_impl
__forceinline __m256d _mm256_radian_acos_pd_impl(__m256d a)
{
	__m256d y, n, p;
		
	p = _mm256_cmpge_pd(a, __m256d_1);
	n = _mm256_cmple_pd(a, __m256d_minus_1);
	
	y = _mm256_acos_pd(a);
	
	y = _mm256_blendv_pd(y, _mm256_setzero_pd(), p);
	y = _mm256_blendv_pd(y, __m256d_PI, n);
	
	return y;
}
/***************************************************************************************************/
#endif


#if __AVX2__
#undef _mm256_cosh_pd
#define _mm256_cosh_pd _mm256_cosh_pd_impl
__forceinline __m256d _mm256_cosh_pd_impl(__m256d a) {
	//cosh(a) = (e^a + e ^ (-a)) / 2
	__m256d ex;
	
	ex = _mm256_exp_pd(a);
	return _mm256_div_pd(_mm256_add_pd(ex, _mm256_rcp14_pd(ex)), __m256d_2);
}

#undef _mm256_acosh_pd
#define _mm256_acosh_pd _mm256_acosh_pd_impl
__forceinline __m256d _mm256_acosh_pd_impl(__m256d a) {
	//acosh(a) = ln(a + sqrt(a^2 - 1) )
	return _mm256_log_pd(_mm256_add_pd(a,_mm256_sqrt_pd(_mm256_fmsub_pd(a, a, __m256d_1) )));
}

#undef _mm256_tan_pd
#define _mm256_tan_pd _mm256_tan_pd_impl
__forceinline __m256d _mm256_tan_pd_impl(__m256d a) {
	__m256d sign_bit, w, y, z, zz;
	__m256i_i64 emm0, emm1;
	
	sign_bit = _mm256_preservesignbit_pd(a);
	a = _mm256_abs_pd(a);
	
	
	y = _mm256_mul_pd(__m256d_FoPI, a);
	
	emm0 = _mm256_cvttpd_epi64(y);
	y = _mm256_cvtepi64_pd(emm0);
	
	
	emm1 = _mm256_and_epi64(emm0, __m256i_i64_1);
	emm1 = _mm256_cmpeq_epi64(emm1, __m256i_i64_1);
	emm0 = _mm256_add_epi64(emm0, _mm256_and_epi64(emm1, __m256i_i64_1));
	y = _mm256_add_pd(y, _mm256_and_pd(_mm256_cvtepi64_pd(emm1), __m256d_1));
	
	z = _mm256_fmadd_pd(y, __m256d_minus_DP1, a);
	z = _mm256_fmadd_pd(y, __m256d_minus_DP2, z);
	z = _mm256_fmadd_pd(y, __m256d_minus_DP3, z);
	
	zz = _mm256_mul_pd(z, z);
	
	
	y = _mm256_fmadd_pd(__m256d_tanf_p0, zz, __m256d_tanf_p1);
	y = _mm256_fmadd_pd(y, zz, __m256d_tanf_p2);
	y = _mm256_fmadd_pd(y, zz, __m256d_tanf_p3);
	y = _mm256_fmadd_pd(y, zz, __m256d_tanf_p4);
	y = _mm256_fmadd_pd(y, zz, __m256d_tanf_p5);
	y = _mm256_mul_pd(y, zz);
	y = _mm256_fmadd_pd(y, z, z);
	
	
	z = _mm256_rcp14_pd(y);
	z = _mm256_chs_pd(z);
	
	emm1 = _mm256_and_epi64(emm0, __m256i_i64_2);
	emm1 = _mm256_cmpeq_epi64(emm1, __m256i_i64_2);
	w = _mm256_castepi64_pd(emm1);
	y = _mm256_blendv_pd(y, z, w);
	
	return _mm256_effectsignbit_pd(sign_bit, y);
}


#undef _mm256_tand_pd
#define _mm256_tand_pd _mm256_tand_pd_impl
__forceinline __m256d _mm256_tand_pd_impl(__m256d a) {
	return _mm256_tan_pd(_mm256_mul_pd(a, __m256d_Deg2Rad));
}
#endif
#if __AVX__
#undef _mm256_atan_pd
#define _mm256_atan_pd _mm256_atan_pd_impl
__forceinline __m256d _mm256_atan_pd_impl(__m256d a) {
	__m256d sign_bit, w, x, y, aa, am1, ap1, z;
	
	sign_bit = _mm256_preservesignbit_pd(a);
	a = _mm256_abs_pd(a);
	
	w = _mm256_cmpgt_pd(a, __m256d_TAN3PIo8);
	x = _mm256_cmpgt_pd(a, __m256d_TANPIo8);
	z = _mm256_andnot_pd(w, x);
	
	y = _mm256_and_pd(w, __m256d_PIo2F);
	y = _mm256_or_pd(y, _mm256_and_pd(z, __m256d_PIo4F));
	
	w = _mm256_and_pd(w, _mm256_chs_pd(_mm256_rcp14_pd(a)));
	am1 = _mm256_sub_pd(a, __m256d_1);
	ap1 = _mm256_add_pd(a, __m256d_1);
	w = _mm256_or_pd(w, _mm256_and_pd(z, _mm256_mul_pd(am1, _mm256_rcp14_pd(ap1))));
	
	a = _mm256_or_pd(_mm256_andnot_pd(x, a), w);
	aa = _mm256_mul_pd(a, a);
	
	z = _mm256_fmadd_pd(__m256d_atanf_p0, aa,__m256d_atanf_p1);
	z = _mm256_fmadd_pd(z, aa, __m256d_atanf_p2);
	z = _mm256_fmadd_pd(z, aa, __m256d_atanf_p3);
	z = _mm256_mul_pd(z, aa);
	z = _mm256_fmadd_pd(z, a, a);
	y = _mm256_add_pd(y, z);
	
	return _mm256_effectsignbit_pd(sign_bit, y);
}

#undef _mm256_atan2_pd
#define _mm256_atan2_pd _mm256_atan2_pd_impl
__forceinline __m256d _mm256_atan2_pd_impl(__m256d a, __m256d b) {
	__m256d sign_bit, y, y2, z, w, w2;
	
	w = _mm256_cmplt_pd(b, _mm256_setzero_pd());
	y = _mm256_cmplt_pd(a, _mm256_setzero_pd());
	w2 = _mm256_cmpeq_pd(b, _mm256_setzero_pd());
	y2 = _mm256_cmpeq_pd(a, _mm256_setzero_pd());
	
	sign_bit = _mm256_and_pd(y, __m256d_sign_mask);
	w = _mm256_and_pd(w, __m256d_PI);
	w = _mm256_or_pd(w, sign_bit);
	z = _mm256_atan_pd(_mm256_mul_pd(a, _mm256_rcp14_pd(b)));
	w = _mm256_add_pd(w, z);
	
	y = _mm256_andnot_pd(y, __m256d_PIo2F);
	y = _mm256_or_pd(y, sign_bit);
	y = _mm256_andnot_pd(y2, y);

	return _mm256_blendv_pd(w, y, w2);

}
#endif
#if __AVX2__
#undef _mm256_tanh_pd
#define _mm256_tanh_pd _mm256_tanh_pd_impl
__forceinline __m256d _mm256_tanh_pd_impl(__m256d a) {
	// tanh(a) = e^2x - 1 / e^2x + 1 
	__m256d ex = _mm256_exp_pd(_mm256_mul_pd(a, __m256d_2));
	return _mm256_div_pd( _mm256_sub_pd(ex, __m256d_1	), _mm256_add_pd(ex, __m256d_1));
}

#undef _mm256_atanh_pd
#define _mm256_atanh_pd _mm256_atanh_pd_impl
__forceinline __m256d _mm256_atanh_pd_impl(__m256d a) {
	// artanh(a) = 0.5*ln[(1+a)/(1-a)] 
	return _mm256_mul_pd(__m256d_1o2,_mm256_log_pd(_mm256_div_pd(_mm256_add_pd(__m256d_1, a), _mm256_sub_pd(__m256d_1, a))));
}

#undef _mm256_sincos_pd
#define _mm256_sincos_pd _mm256_sincos_pd_impl
__forceinline __m256d _mm256_sincos_pd_impl(__m256d *mem_addr, __m256d a) {
	__m256d cos_sign_bit, sin_sign_bit, poly_mask, aa, y, y2, z, z2;
	__m256i_i64 emm0, emm1, emm3;
	
	sin_sign_bit = _mm256_preservesignbit_pd(a);
	
	a = _mm256_abs_pd(a);
	
	y = _mm256_mul_pd(a, __m256d_FoPI);
	
	emm0 = _mm256_cvttpd_epi64(y);
	emm0 = _mm256_add_epi64(emm0, __m256i_i64_1);
	emm0 = _mm256_and_epi64(emm0, __m256i_i64_inv1);
	y = _mm256_cvtepi64_pd(emm0);
	
	emm1 = emm0;
	
	emm3 = _mm256_and_epi64(emm0, __m256i_i64_4);
	emm3 = _mm256_slli_epi64(emm3, 29);
	
	emm0 = _mm256_and_epi64(emm0, __m256i_i64_2);
	emm0 = _mm256_cmpeq_epi64(emm0, _mm256_setzero_epi64());
	
	poly_mask = _mm256_castepi64_pd(emm0);
	
	a = _mm256_fmadd_pd(y, __m256d_minus_DP1, a);
	a = _mm256_fmadd_pd(y, __m256d_minus_DP2, a);
	a = _mm256_fmadd_pd(y, __m256d_minus_DP3, a);
	
	
	emm1 = _mm256_sub_epi64(emm1, __m256i_i64_2);
	emm1 = _mm256_andnot_epi64(emm1, __m256i_i64_4);
	emm1 = _mm256_slli_epi64(emm1, 29);
	
	cos_sign_bit = _mm256_castepi64_pd(emm1);
	sin_sign_bit = _mm256_xor_pd(sin_sign_bit, _mm256_castepi64_pd(emm3));
	
	
	aa = _mm256_mul_pd(a, a);
	
	y = __m256d_coscof_p0;
	y = _mm256_fmadd_pd(y, aa, __m256d_coscof_p1);
	y = _mm256_fmadd_pd(y, aa, __m256d_coscof_p2);
	y = _mm256_mul_pd(y, aa);
	y = _mm256_mul_pd(y, aa);
	y = _mm256_fnmadd_pd(aa, __m256d_1o2, y);
	y = _mm256_add_pd(y, __m256d_1);
	
	
	y2 = __m256d_sincof_p0;
	y2 = _mm256_fmadd_pd(y2, aa, __m256d_sincof_p1);
	y2 = _mm256_fmadd_pd(y2, aa, __m256d_sincof_p2);
	y2 = _mm256_mul_pd(y2, aa);
	y2 = _mm256_fmadd_pd(y2, a, a);
	
	z = _mm256_and_pd(poly_mask, y2);
	z2 = _mm256_andnot_pd(poly_mask, y);
	
	y2 = _mm256_sub_pd(y2, z);
	y = _mm256_sub_pd(y, z2);
	
	y = _mm256_add_pd(y, y2);
	y2 = _mm256_add_pd(z2, z);
	
	
	*mem_addr = _mm256_effectsignbit_pd(y, cos_sign_bit);
	
	return	_mm256_effectsignbit_pd(y2, sin_sign_bit);
}
#endif

