/***************************************************************************************************/
// HEADER_NAME /svml/trigonometry__m256.h
/***************************************************************************************************/

#if __AVX__
#undef _mm256_hypot_ps
#define _mm256_hypot_ps _mm256_hypot_ps_impl
__forceinline __m256 _mm256_hypot_ps_impl(__m256 a, __m256 b) {
	return _mm256_sqrt_ps( _mm256_fmadd_ps( a,a,_mm256_mul_ps(b,b) ) );
}
#endif
#if __AVX2__
#undef _mm256_sin_ps
#define _mm256_sin_ps _mm256_sin_ps_impl
__forceinline __m256 _mm256_sin_ps_impl(__m256 a) {
	__m256 poly_mask, sign_bit, swap_sign, y, y2, z;
	__m256i_i32	emm0, emm1;
	
	sign_bit = _mm256_preservesignbit_ps(a);
	
	a = _mm256_abs_ps(a);
	
	y = _mm256_mul_ps(a, __m256_FoPI);
	
	emm0 = _mm256_cvttps_epi32(y);
	emm0 = _mm256_add_epi32(emm0, __m256i_i32_1);
	emm0 = _mm256_and_epi32(emm0, __m256i_i32_inv1);
	y = _mm256_cvtepi32_ps(emm0);
	
	emm1 = _mm256_and_epi32(emm0, __m256i_i32_4);
	emm1 = _mm256_slli_epi32(emm1, 29);
	
	emm0 = _mm256_and_epi32(emm0, __m256i_i32_2);
	emm0 = _mm256_cmpeq_epi32(emm0, _mm256_setzero_epi32());
	
	swap_sign = _mm256_castepi32_ps(emm1);
	poly_mask = _mm256_castepi32_ps(emm0);
	sign_bit = _mm256_xor_ps(sign_bit, swap_sign);
	
	a = _mm256_fmadd_ps(y, __m256_minus_DP1,a);
	a = _mm256_fmadd_ps(y, __m256_minus_DP2, a);
	a = _mm256_fmadd_ps(y, __m256_minus_DP3, a);
	
	y = __m256_coscof_p0;
	z = _mm256_mul_ps(a,a);
	
	y = _mm256_fmadd_ps(y, z, __m256_coscof_p1);
	y = _mm256_fmadd_ps(y, z, __m256_coscof_p2);
	y = _mm256_mul_ps(y, z);
	y = _mm256_mul_ps(y, z);
	y = _mm256_fnmadd_ps(z, __m256_1o2, y);
	y = _mm256_add_ps(y, __m256_1);
	
	
	y2 = __m256_sincof_p0;
	y2 = _mm256_fmadd_ps(y2, z, __m256_sincof_p1);
	y2 = _mm256_fmadd_ps(y2, z, __m256_sincof_p2);
	y2 = _mm256_mul_ps(y2, z);
	y2 = _mm256_fmadd_ps(y2, a, a);
	
	y = _mm256_blendv_ps(y, y2, poly_mask);
	
	return _mm256_effectsignbit_ps(y, sign_bit);
}

#undef _mm256_sind_ps
#define _mm256_sind_ps _mm256_sind_ps_impl
__forceinline __m256 _mm256_sind_ps_impl(__m256 a) {
	return _mm256_sin_ps(_mm256_mul_ps(a, __m256_Deg2Rad));
}
#endif
#if __AVX__
#undef _mm256_asin_ps
#define _mm256_asin_ps _mm256_asin_ps_impl
__forceinline __m256 _mm256_asin_ps_impl(__m256 a) {
	__m256 flag, sign_bit, w, y, z;
	
	sign_bit = _mm256_preservesignbit_ps(a);
	a = _mm256_abs_ps(a);
	
	flag = _mm256_cmpgt_ps(a, __m256_1o2);
	z = _mm256_mul_ps(__m256_1o2, _mm256_sub_ps(__m256_1, a));
	w = _mm256_sqrt_ps(z);
	a = _mm256_blendv_ps(a, w, flag);
	z = _mm256_blendv_ps(_mm256_mul_ps(a, a), z, flag);
	
	y = _mm256_fmadd_ps(z, __m256_asinf_p0, __m256_asinf_p1);
	y = _mm256_fmadd_ps(y, z, __m256_asinf_p2);
	y = _mm256_fmadd_ps(y, z, __m256_asinf_p3);
	y = _mm256_fmadd_ps(y, z, __m256_asinf_p4);
	y = _mm256_mul_ps(y, z);
	y = _mm256_fmadd_ps(y, a, a);
	
	z = _mm256_sub_ps(__m256_PIo2F, _mm256_add_ps(y, y));
	y = _mm256_blendv_ps(y, z, flag);
	
	return _mm256_effectsignbit_ps(sign_bit, y);
}
#endif

/***************************************************************************************************/
// NOT PART OF SVML 
// Added For Computer Graphics Support
#if __AVX__
#undef _mm256_radian_asin_ps
#define _mm256_radian_asin_ps _mm256_radian_asin_ps_impl
__forceinline __m256 _mm256_radian_asin_ps_impl(__m256 a)
{
	__m256 y, n, p;

	p = _mm256_cmpge_ps(a, __m256_1);
	n = _mm256_cmple_ps(a, __m256_minus_1);

	y = _mm256_asin_ps(a);

	y = _mm256_blendv_ps(y, __m256_PIo2F, p);
	y = _mm256_blendv_ps(y, __m256_minus_PIo2F, n);

	return y;
}
#endif
/***************************************************************************************************/

#if __AVX2__
#undef _mm256_sinh_ps
#define _mm256_sinh_ps _mm256_sinh_ps_impl
__forceinline __m256 _mm256_sinh_ps_impl(__m256 a) {
	// sinh(a) = ((e^a - e ^ (-a)) / 2)
	__m256 ex;

	ex = _mm256_exp_ps(a);
	return _mm256_div_ps(_mm256_sub_ps(ex, _mm256_rcp_ps(ex)), __m256_2);
}

#undef _mm256_asinh_ps
#define _mm256_asinh_ps _mm256_asinh_ps_impl
__forceinline __m256 _mm256_asinh_ps_impl(__m256 a) {
	// asinh(a) = ln( a + sqrt(a^2+1) );
	return _mm256_log_ps( _mm256_add_ps(a, _mm256_sqrt_ps(_mm256_fmadd_ps( a, a, __m256_1) )));
}

#undef _mm256_cos_ps
#define _mm256_cos_ps _mm256_cos_ps_impl
__forceinline __m256 _mm256_cos_ps_impl(__m256 a) {
	__m256	poly_mask, sign_bit, y, y2, z;
	__m256i_i32 emm0, emm1;
	
	sign_bit = _mm256_preservesignbit_ps(a);
	a = _mm256_abs_ps(a);
	
	y = _mm256_mul_ps(a, __m256_FoPI);
	
	emm0 = _mm256_cvttps_epi32(y);
	
	emm0 = _mm256_add_epi32(emm0, __m256i_i32_1);
	emm0 = _mm256_and_epi32(emm0, __m256i_i32_inv1);
	y = _mm256_cvtepi32_ps(emm0);
	
	emm0 = _mm256_sub_epi32(emm0, __m256i_i32_2);
	
	emm1 = _mm256_andnot_epi32(emm0, __m256i_i32_4);
	emm1 = _mm256_slli_epi32(emm1, 29);
	
	emm0 = _mm256_and_epi32(emm0, __m256i_i32_2);
	emm0 = _mm256_cmpeq_epi32(emm0, _mm256_setzero_epi32());
	
	sign_bit = _mm256_castepi32_ps(emm1);
	poly_mask = _mm256_castepi32_ps(emm0);
	
	a = _mm256_fmadd_ps(y, __m256_minus_DP1, a);
	a = _mm256_fmadd_ps(y, __m256_minus_DP2, a);
	a = _mm256_fmadd_ps(y, __m256_minus_DP3, a);
	
	y = __m256_coscof_p0;
	z = _mm256_mul_ps(a,a);
	
	y = _mm256_fmadd_ps(y, z, __m256_coscof_p1);
	y = _mm256_fmadd_ps(y, z, __m256_coscof_p2);
	y = _mm256_mul_ps(y, z);
	y = _mm256_mul_ps(y, z);
	y = _mm256_fnmadd_ps(z, __m256_1o2, y);
	y = _mm256_add_ps(y, __m256_1);
	
	y2 = __m256_sincof_p0;
	
	y2 = _mm256_fmadd_ps(y2, z, __m256_sincof_p1);
	y2 = _mm256_fmadd_ps(y2, z, __m256_sincof_p2);
	y2 = _mm256_mul_ps(y2, z);
	y2 = _mm256_fmadd_ps(y2, a, a);
	
	y = _mm256_blendv_ps(y, y2, poly_mask);

	return _mm256_effectsignbit_ps(y, sign_bit);
}

#undef _mm256_cosd_ps
#define _mm256_cosd_ps _mm256_cosd_ps_impl
__forceinline __m256 _mm256_cosd_ps_impl(__m256 a) {
	return _mm256_cos_ps(_mm256_mul_ps(a, __m256_Deg2Rad));
}
#endif
#if __AVX__
#undef _mm256_acos_ps
#define _mm256_acos_ps _mm256_acos_ps_impl
__forceinline __m256 _mm256_acos_ps_impl(__m256 a)
{
	__m256 gt, ngt, lt, mt, p, q, z, s, w, df;
	gt = _mm256_cmpgt_ps(a, __m256_1o2);
	lt = _mm256_cmplt_ps(a, __m256_minus_1);
	ngt = _mm256_xor_ps(gt, __m256_maxint);
	mt = _mm256_andnot_ps(lt, ngt);
	
	
	z = _mm256_mul_ps(_mm256_add_ps(_mm256_or_ps(a, __m256_sign_mask), __m256_1), __m256_1o2);
	z = _mm256_blendv_ps(z, _mm256_mul_ps(a, a), mt);
	s = _mm256_blendv_ps(_mm256_sqrt_ps(z), a, mt);
	
	p = _mm256_fmadd_ps(z, __m256_acos_p5, __m256_acos_p4);
	p = _mm256_fmadd_ps(z, p, __m256_acos_p3);
	p = _mm256_fmadd_ps(z, p, __m256_acos_p2);
	p = _mm256_fmadd_ps(z, p, __m256_acos_p1);
	p = _mm256_fmadd_ps(z, p, __m256_acos_p0);
	p = _mm256_mul_ps(z, p);
	
	q = _mm256_fmadd_ps(z, __m256_acos_q4,__m256_acos_q3);
	q = _mm256_fmadd_ps(z, q, __m256_acos_q2);
	q = _mm256_fmadd_ps(z, q, __m256_acos_q1);
	q = _mm256_fmadd_ps(z, q, __m256_1);
	
	p = _mm256_div_ps(p, q);
	
	df = _mm256_and_ps(s, _mm256_or_ps(__m256_asinf_trunc, ngt));
	q = _mm256_div_ps(_mm256_fnmadd_ps(df, df,z), _mm256_add_ps(s, df));
	gt = _mm256_andnot_ps(_mm256_cmpeq_ps(a, __m256_1), gt);
	q = _mm256_or_ps(_mm256_and_ps(gt, q), _mm256_and_ps(ngt, __m256_pio2_lo));
	w = _mm256_fmadd_ps(p, s, q);
	w = _mm256_add_ps(w, df);
	w = _mm256_mul_ps(w, _mm256_blendv_ps(__m256_2, __m256_1, mt));
	w = _mm256_xor_ps(w, _mm256_and_ps(__m256_sign_mask, ngt));
	w = _mm256_add_ps(w, _mm256_and_ps(__m256_PI, lt));
	w = _mm256_add_ps(w, _mm256_and_ps(__m256_pio2_hi, mt));
	
	return w;
}

/***************************************************************************************************/
// NOT PART OF SVML 
// Added For Computer Graphics Support
#undef _mm256_radian_acos_ps
#define _mm256_radian_acos_ps _mm256_radian_acos_ps_impl
__forceinline __m256 _mm256_radian_acos_ps_impl(__m256 a)
{
	__m256 y, n, p;
	
	p = _mm256_cmpge_ps(a, __m256_1);
	n = _mm256_cmple_ps(a, __m256_minus_1);
	
	y = _mm256_acos_ps(a);
	
	y = _mm256_blendv_ps(y, _mm256_setzero_ps(), p);
	y = _mm256_blendv_ps(y, __m256_PI, n);
	
	return y;
}
#endif
/***************************************************************************************************/

#if __AVX2__
#undef _mm256_cosh_ps
#define _mm256_cosh_ps _mm256_cosh_ps_impl
__forceinline __m256 _mm256_cosh_ps_impl(__m256 a) {
	//cosh(a) = (e^a + e ^ (-a)) / 2
	__m256 ex;
	
	ex = _mm256_exp_ps(a);
	return _mm256_div_ps(_mm256_add_ps(ex, _mm256_rcp_ps(ex)), __m256_2);
}

#undef _mm256_acosh_ps
#define _mm256_acosh_ps _mm256_acosh_ps_impl
__forceinline __m256 _mm256_acosh_ps_impl(__m256 a) {
	//acosh(a) = ln(a + sqrt(a^2 - 1) )
	return _mm256_log_ps(_mm256_add_ps(a,_mm256_sqrt_ps(_mm256_fmsub_ps(a, a,__m256_1) )));
}

#undef _mm256_tan_ps
#define _mm256_tan_ps _mm256_tan_ps_impl
__forceinline __m256 _mm256_tan_ps_impl(__m256 a) {
	__m256 sign_bit, w, y, z, zz;
	__m256i_i32 emm0, emm1;
	
	sign_bit = _mm256_preservesignbit_ps(a);
	a = _mm256_abs_ps(a);
	
	y = _mm256_mul_ps(__m256_FoPI, a);
	
	emm0 = _mm256_cvttps_epi32(y);
	y = _mm256_cvtepi32_ps(emm0);
	
	emm1 = _mm256_and_epi32(emm0, __m256i_i32_1);
	emm1 = _mm256_cmpeq_epi32(emm1, __m256i_i32_1);
	emm0 = _mm256_add_epi32(emm0, _mm256_and_epi32(emm1, __m256i_i32_1));
	y = _mm256_add_ps(y, _mm256_and_ps(_mm256_cvtepi32_ps(emm1), __m256_1));
	
	z = _mm256_fmadd_ps(y, __m256_minus_DP1, a);
	z = _mm256_fmadd_ps(y, __m256_minus_DP2, z);
	z = _mm256_fmadd_ps(y, __m256_minus_DP3, z);
	
	zz = _mm256_mul_ps(z, z);
	
	y = _mm256_fmadd_ps(__m256_tanf_p0, zz, __m256_tanf_p1);
	y = _mm256_fmadd_ps(y, zz, __m256_tanf_p2);
	y = _mm256_fmadd_ps(y, zz, __m256_tanf_p3);
	y = _mm256_fmadd_ps(y, zz, __m256_tanf_p4);
	y = _mm256_fmadd_ps(y, zz, __m256_tanf_p5);
	y = _mm256_mul_ps(y, zz);
	y = _mm256_fmadd_ps(y, z, z);
	
	z = _mm256_rcp_ps(y);
	z = _mm256_chs_ps(z);
	
	emm1 = _mm256_and_epi32(emm0, __m256i_i32_2);
	emm1 = _mm256_cmpeq_epi32(emm1, __m256i_i32_2);
	w = _mm256_castepi32_ps(emm1);
	y = _mm256_blendv_ps(y, z, w);
	
	return _mm256_effectsignbit_ps(sign_bit, y);
}

#undef _mm256_tand_ps
#define _mm256_tand_ps _mm256_tand_ps_impl
__forceinline __m256 _mm256_tand_ps_impl(__m256 a) {
	return _mm256_tan_ps(_mm256_mul_ps(a, __m256_Deg2Rad));
}
#endif
#if __AVX__
#undef _mm256_atan_ps
#define _mm256_atan_ps _mm256_atan_ps_impl
__forceinline __m256 _mm256_atan_ps_impl(__m256 a) {
	__m256 sign_bit, w, x, y, aa, am1, ap1, z;

	sign_bit = _mm256_preservesignbit_ps(a);
	a = _mm256_abs_ps(a);
	
	w = _mm256_cmpgt_ps(a, __m256_TAN3PIo8);
	x = _mm256_cmpgt_ps(a, __m256_TANPIo8);
	z = _mm256_andnot_ps(w, x);
	
	y = _mm256_and_ps(w, __m256_PIo2F);
	y = _mm256_or_ps(y, _mm256_and_ps(z, __m256_PIo4F));
	
	w = _mm256_and_ps(w, _mm256_chs_ps(_mm256_rcp_ps(a)));
	am1 = _mm256_sub_ps(a, __m256_1);
	ap1 = _mm256_add_ps(a, __m256_1);
	w = _mm256_or_ps(w, _mm256_and_ps(z, _mm256_mul_ps(am1, _mm256_rcp_ps(ap1))));
	
	a = _mm256_or_ps(_mm256_andnot_ps(x, a), w);
	aa = _mm256_mul_ps(a, a);
	
	z = _mm256_fmadd_ps(__m256_atanf_p0, aa,__m256_atanf_p1);
	z = _mm256_fmadd_ps(z, aa, __m256_atanf_p2);
	z = _mm256_fmadd_ps(z, aa, __m256_atanf_p3);
	z = _mm256_mul_ps(z, aa);
	z = _mm256_fmadd_ps(z, a, a);
	y = _mm256_add_ps(y, z);
	
	return _mm256_effectsignbit_ps(sign_bit, y);
}

#undef _mm256_atan2_ps
#define _mm256_atan2_ps _mm256_atan2_ps_impl
__forceinline __m256 _mm256_atan2_ps_impl(__m256 a, __m256 b) {
	__m256 sign_bit, y, y2, z, w, w2;
	
	w = _mm256_cmplt_ps(b, _mm256_setzero_ps());
	y = _mm256_cmplt_ps(a, _mm256_setzero_ps());
	w2 = _mm256_cmpeq_ps(b, _mm256_setzero_ps());
	y2 = _mm256_cmpeq_ps(a, _mm256_setzero_ps());
	
	sign_bit = _mm256_and_ps(y, __m256_sign_mask);
	w = _mm256_and_ps(w, __m256_PI);
	w = _mm256_or_ps(w, sign_bit);
	z = _mm256_atan_ps(_mm256_mul_ps(a, _mm256_rcp_ps(b)));
	w = _mm256_add_ps(w, z);
	
	y = _mm256_andnot_ps(y, __m256_PIo2F);
	y = _mm256_or_ps(y, sign_bit);
	y = _mm256_andnot_ps(y2, y);

	return _mm256_blendv_ps(w, y, w2);

}
#endif
#if __AVX2__
#undef _mm256_tanh_ps
#define _mm256_tanh_ps _mm256_tanh_ps_impl
__forceinline __m256 _mm256_tanh_ps_impl(__m256 a) {
	// tanh(a) = e^2x - 1 / e^2x + 1 
	__m256 ex = _mm256_exp_ps(_mm256_mul_ps(a, __m256_2));
	return _mm256_div_ps( _mm256_sub_ps(ex, __m256_1	), _mm256_add_ps(ex, __m256_1));
}

#undef _mm256_atanh_ps
#define _mm256_atanh_ps _mm256_atanh_ps_impl
__forceinline __m256 _mm256_atanh_ps_impl(__m256 a) {
	// artanh(a) = 0.5*ln[(1+a)/(1-a)] 
	return _mm256_mul_ps(__m256_1o2,_mm256_log_ps(_mm256_div_ps(_mm256_add_ps(__m256_1, a), _mm256_sub_ps(__m256_1, a))));
}

#undef _mm256_sincos_ps
#define _mm256_sincos_ps _mm256_sincos_ps_impl
__forceinline __m256 _mm256_sincos_ps_impl(__m256 *mem_addr, __m256 a) {
	__m256 cos_sign_bit, sin_sign_bit, poly_mask, aa, y, y2, z, z2;
	__m256i_i32 emm0, emm1, emm3;
	
	sin_sign_bit = _mm256_preservesignbit_ps(a);
	
	a = _mm256_abs_ps(a);
	
	y = _mm256_mul_ps(a, __m256_FoPI);
	
	emm0 = _mm256_cvttps_epi32(y);
	emm0 = _mm256_add_epi32(emm0, __m256i_i32_1);
	emm0 = _mm256_and_epi32(emm0, __m256i_i32_inv1);
	y = _mm256_cvtepi32_ps(emm0);
	
	emm1 = emm0;
	
	emm3 = _mm256_and_epi32(emm0, __m256i_i32_4);
	emm3 = _mm256_slli_epi32(emm3, 29);
	
	emm0 = _mm256_and_epi32(emm0, __m256i_i32_2);
	emm0 = _mm256_cmpeq_epi32(emm0, _mm256_setzero_epi32());
	
	poly_mask = _mm256_castepi32_ps(emm0);
	
	a = _mm256_fmadd_ps(y, __m256_minus_DP1, a);
	a = _mm256_fmadd_ps(y, __m256_minus_DP2, a);
	a = _mm256_fmadd_ps(y, __m256_minus_DP3, a);
	
	
	emm1 = _mm256_sub_epi32(emm1, __m256i_i32_2);
	emm1 = _mm256_andnot_epi32(emm1, __m256i_i32_4);
	emm1 = _mm256_slli_epi32(emm1, 29);
	
	cos_sign_bit = _mm256_castepi32_ps(emm1);
	sin_sign_bit = _mm256_xor_ps(sin_sign_bit, _mm256_castepi32_ps(emm3));
	
	
	aa = _mm256_mul_ps(a, a);
	
	y = __m256_coscof_p0;
	y = _mm256_fmadd_ps(y, aa, __m256_coscof_p1);
	y = _mm256_fmadd_ps(y, aa, __m256_coscof_p2);
	y = _mm256_mul_ps(y, aa);
	y = _mm256_mul_ps(y, aa);
	y = _mm256_fnmadd_ps(aa, __m256_1o2, y);
	y = _mm256_add_ps(y, __m256_1);
	
	
	y2 = __m256_sincof_p0;
	y2 = _mm256_fmadd_ps(y2, aa, __m256_sincof_p1);
	y2 = _mm256_fmadd_ps(y2, aa, __m256_sincof_p2);
	y2 = _mm256_mul_ps(y2, aa);
	y2 = _mm256_fmadd_ps(y2, a, a);
	
	z = _mm256_and_ps(poly_mask, y2);
	z2 = _mm256_andnot_ps(poly_mask, y);
	
	y2 = _mm256_sub_ps(y2, z);
	y = _mm256_sub_ps(y, z2);
	
	y = _mm256_add_ps(y, y2);
	y2 = _mm256_add_ps(z2, z);
	
	
	*mem_addr = _mm256_effectsignbit_ps(y, cos_sign_bit);
	
	return	_mm256_effectsignbit_ps(y2, sin_sign_bit);
}
#endif

