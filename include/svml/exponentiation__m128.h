/***************************************************************************************************/
// HEADER_NAME /svml/exponentiation__m128.h
/***************************************************************************************************/

/*************************************************************************************/
// SVML Redefines!
/*************************************************************************************/
#undef _mm_svml_sqrt_ps
#define _mm_svml_sqrt_ps _mm_svml_sqrt_ps_impl
__forceinline __m128 _mm_svml_sqrt_ps_impl(__m128 a)
{
	return _mm_sqrt_ps(a);
}
/*************************************************************************************/

#undef _mm_log_ps
#define _mm_log_ps _mm_log_ps_impl
__forceinline __m128 _mm_log_ps_impl(__m128 a) {
	__m128 invalid_mask, e, aa, mask, y;
	__m128i_i32 emm0;
	
	invalid_mask = _mm_cmple_ps(a, _mm_setzero_ps());
	
	a = _mm_max_ps(a, __m128_min_norm_pos);
	
	emm0 = _mm_srli_epi32(_mm_castps_epi32(a), IEE754_32BIT_MANTISSA);
	
	a = _mm_and_ps(a, __m128_inv_mant_mask);
	a = _mm_or_ps(a, __m128_1o2);
	
	emm0 = _mm_sub_epi32(emm0, __m128i_i32_0x7f);
	e = _mm_cvtepi32_ps(emm0);
	e = _mm_add_ps(e, __m128_1);
	
	mask = _mm_cmplt_ps(a, __m128_SQRTHF);
	y = _mm_and_ps(a, mask);
	
	e = _mm_sub_ps(e, _mm_and_ps(__m128_1, mask));
	
	a = _mm_sub_ps(a, __m128_1);
	a = _mm_add_ps(a, y);
	
	aa = _mm_mul_ps(a,a);
	
	y = __m128_log_p0;
	y = _mm_fmadd_ps(y, a, __m128_log_p1);
	y = _mm_fmadd_ps(y, a, __m128_log_p2);
	y = _mm_fmadd_ps(y, a, __m128_log_p3);
	y = _mm_fmadd_ps(y, a, __m128_log_p4);
	y = _mm_fmadd_ps(y, a, __m128_log_p5);
	y = _mm_fmadd_ps(y, a, __m128_log_p6);
	y = _mm_fmadd_ps(y, a, __m128_log_p7);
	y = _mm_fmadd_ps(y, a, __m128_log_p8);
	y = _mm_mul_ps(y, a);
	y = _mm_mul_ps(y, aa);
	y = _mm_fmadd_ps(e, __m128_log_q1, y);
	y = _mm_fnmadd_ps(aa, __m128_1o2, y);
	
	a = _mm_add_ps(a, y);
	a = _mm_fmadd_ps(e, __m128_log_q2, a);
	
	return _mm_or_ps(a, invalid_mask);
}

#undef _mm_log1p_ps
#define _mm_log1p_ps _mm_log1p_ps_impl
__forceinline __m128 _mm_log1p_ps_impl(__m128 a) {
	return _mm_log_ps(_mm_add_ps(__m128_1, a));
}

/*************************************************************************************/
// NOT PART OF SVML 
#undef _mm_logn_ps
#define _mm_logn_ps _mm_logn_ps_impl
__forceinline __m128 _mm_logn_ps_impl(__m128 a, __m128 b)
{
	return _mm_div_ps(_mm_log_ps(b), _mm_log_ps(a));
}
/*************************************************************************************/

#undef _mm_log2_ps
#define _mm_log2_ps _mm_log2_ps_impl
__forceinline __m128 _mm_log2_ps_impl(__m128 a)
{
	return _mm_mul_ps(_mm_log_ps(a), __m128_LOG2EF);
}

#undef _mm_logb_ps
#define _mm_logb_ps _mm_logb_ps_impl
__forceinline __m128 _mm_logb_ps_impl(__m128 a)
{
	return _mm_floor_ps(_mm_log2_ps(_mm_abs_ps(a)));
} 

#undef _mm_log10_ps
#define _mm_log10_ps _mm_log10_ps_impl
__forceinline __m128 _mm_log10_ps_impl(__m128 a)
{
	return _mm_mul_ps(_mm_log_ps(a), __m128_LOG10EF);
} 

#undef _mm_exp_ps_
#define _mm_exp_ps_ _mm_exp_ps_impl
__forceinline __m128 _mm_exp_ps_impl(__m128 a) {
	__m128 ex, aa, y, z;
	__m128i_i32 emm0;
	
	a = _mm_min_ps(a, __m128_exp_hi);
	a = _mm_max_ps(a, __m128_exp_lo);
	
	ex = _mm_fmadd_ps(a, __m128_LOG2EF, __m128_1o2);
	ex = _mm_floor_ps(ex);
	
	a = _mm_fnmadd_ps(ex, __m128_exp_C1,a);
	a = _mm_fnmadd_ps(ex, __m128_exp_C2,a);
	
	aa = _mm_mul_ps(a,a);
	
	y = __m128_exp_p0;
	y = _mm_fmadd_ps(y, a, __m128_exp_p1);
	y = _mm_fmadd_ps(y, a, __m128_exp_p2);
	y = _mm_fmadd_ps(y, a, __m128_exp_p3);
	y = _mm_fmadd_ps(y, a, __m128_exp_p4);
	y = _mm_fmadd_ps(y, a, __m128_exp_p5);
	y = _mm_fmadd_ps(y, aa, a);
	y = _mm_add_ps(y, __m128_1);
	
	emm0 = _mm_cvttps_epi32(ex);
	emm0 = _mm_add_epi32(emm0, __m128i_i32_0x7f);
	emm0 = _mm_slli_epi32(emm0, IEE754_32BIT_MANTISSA);
	
	z = _mm_castepi32_ps(emm0);
	
	return _mm_mul_ps(y, z);
}

#undef _mm_expm1_ps
#define _mm_expm1_ps _mm_expm1_ps_impl
__forceinline __m128 _mm_expm1_ps_impl(__m128 a) {
	return _mm_sub_ps(_mm_exp_ps(a), __m128_1);
}

#undef _mm_exp2_ps
#define _mm_exp2_ps _mm_exp2_ps_impl
__forceinline __m128 _mm_exp2_ps_impl(__m128 a) {
	return _mm_exp_ps(_mm_mul_ps(__m128_LOG2, a));
}

#undef _mm_exp10_ps
#define _mm_exp10_ps _mm_exp10_ps_impl
__forceinline __m128 _mm_exp10_ps_impl(__m128 a) {
	return _mm_exp_ps(_mm_mul_ps(__m128_LOG10, a));
}

#undef _mm_pow_ps
#define _mm_pow_ps _mm_pow_ps_impl
__forceinline __m128 _mm_pow_ps_impl(__m128 a,__m128 b){
	return _mm_exp_ps( _mm_mul_ps(_mm_log_ps(_mm_abs_ps(a)), b) );
};

/*************************************************************************************/
// NOT PART OF SVML 
// Alternate name to exp2
#undef _mm_pow2_ps
#define _mm_pow2_ps _mm_pow2_ps_impl
__forceinline __m128 _mm_pow2_ps_impl(__m128 a){
	return _mm_exp2_ps(a);
}

// NOT PART OF SVML 
// Alternate name to exp10
#undef _mm_pow10_ps
#define _mm_pow10_ps _mm_pow10_ps_impl
__forceinline __m128 _mm_pow10_ps_impl(__m128 a) {
	return _mm_exp10_ps(a);
}
// NOT PART OF SVML 
#undef _mm_nthrt_ps
#define _mm_nthrt_ps _mm_nthrt_ps_impl
__forceinline __m128 _mm_nthrt_ps_impl(__m128 a, __m128 b) {
	__m128 sign_bit = _mm_preservesignbit_ps(a);
	return _mm_effectsignbit_ps(_mm_exp_ps(_mm_mul_ps(_mm_log_ps(_mm_abs_ps(a)), _mm_rcp_ps(b))), sign_bit);
}
// NOT PART OF SVML 
#undef _mm_rnthrt_ps
#define _mm_rnthrt_ps _mm_rnthrt_ps_impl
__forceinline __m128 _mm_rnthrt_ps_impl(__m128 a, __m128 b) {
	return _mm_rcp_ps(_mm_nthrt_ps( a, b));
}
/*************************************************************************************/

#undef _mm_cbrt_ps
#define _mm_cbrt_ps _mm_cbrt_ps_impl
__forceinline __m128 _mm_cbrt_ps_impl(__m128 a) {
	__m128 three = __m128_3;
	return _mm_nthrt_ps(a, three);
}

#undef _mm_invsqrt_ps
#define _mm_invsqrt_ps _mm_invsqrt_ps_impl
__forceinline __m128 _mm_invsqrt_ps_impl(__m128 a) {
	return _mm_rsqrt_ps(a);
}

#undef _mm_invcbrt_ps
#define _mm_invcbrt_ps _mm_invcbrt_ps_impl
__forceinline __m128 _mm_invcbrt_ps_impl(__m128 a) {
	__m128 three = __m128_3;
	return _mm_rnthrt_ps(a, __m128_3);
}

