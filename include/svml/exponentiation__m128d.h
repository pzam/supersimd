/***************************************************************************************************/
// HEADER_NAME /svml/exponentiation__m128d.h
/***************************************************************************************************/

#if BIT64
/*************************************************************************************/
// SVML Redefines!
/*************************************************************************************/
#undef _mm_svml_sqrt_pd
#define _mm_svml_sqrt_pd _mm_svml_sqrt_pd_impl
__forceinline __m128d _mm_svml_sqrt_pd_impl(__m128d a)
{
	return _mm_sqrt_pd(a);
}
/*************************************************************************************/

#undef _mm_log_pd
#define _mm_log_pd _mm_log_pd_impl
__forceinline __m128d _mm_log_pd_impl(__m128d a) {
	__m128d invalid_mask, e, aa, mask, y;
	__m128i_i64 emm0;
	
	invalid_mask = _mm_cmple_pd(a, _mm_setzero_pd());
	
	a = _mm_max_pd(a, __m128d_min_norm_pos);
	
	emm0 = _mm_srli_epi64(_mm_castpd_epi64(a), IEE754_64BIT_MANTISSA);
	
	
	a = _mm_and_pd(a, __m128d_inv_mant_mask);
	a = _mm_or_pd(a, __m128d_1o2);
	
	emm0 = _mm_sub_epi64(emm0, __m128i_i64_0x400);
	e = _mm_cvtepi64_pd(emm0);
	e = _mm_add_pd(e, __m128d_1);
	
	mask = _mm_cmplt_pd(a, __m128d_SQRTHF);
	y = _mm_and_pd(a, mask);
	
	e = _mm_sub_pd(e, _mm_and_pd(__m128d_1, mask));
	
	a = _mm_sub_pd(a, __m128d_1);
	a = _mm_add_pd(a, y);
	
	aa = _mm_mul_pd(a,a);
	
	y = __m128d_log_p0;
	y = _mm_fmadd_pd(y, a, __m128d_log_p1);
	y = _mm_fmadd_pd(y, a, __m128d_log_p2);
	y = _mm_fmadd_pd(y, a, __m128d_log_p3);
	y = _mm_fmadd_pd(y, a, __m128d_log_p4);
	y = _mm_fmadd_pd(y, a, __m128d_log_p5);
	y = _mm_fmadd_pd(y, a, __m128d_log_p6);
	y = _mm_fmadd_pd(y, a, __m128d_log_p7);
	y = _mm_fmadd_pd(y, a, __m128d_log_p8);
	y = _mm_mul_pd(y, a);
	y = _mm_mul_pd(y, aa);
	y = _mm_fmadd_pd(e, __m128d_log_q1, y);
	y = _mm_fnmadd_pd(aa, __m128d_1o2, y);
	
	a = _mm_add_pd(a, y);
	a = _mm_fmadd_pd(e, __m128d_log_q2, a);
	
	return _mm_or_pd(a, invalid_mask);
}

#undef _mm_log1p_pd
#define _mm_log1p_pd _mm_log1p_pd_impl
__forceinline __m128d _mm_log1p_pd_impl(__m128d a) {
	return _mm_log_pd(_mm_add_pd(__m128d_1, a));
}

/*************************************************************************************/
// NOT PART OF SVML 
// Recommended only use with high base.
#undef _mm_logn_pd
#define _mm_logn_pd _mm_logn_pd_impl
__forceinline __m128d _mm_logn_pd_impl(__m128d a, __m128d b)
{
	return _mm_div_pd(_mm_log_pd(b),_mm_log_pd(a));
}
/*************************************************************************************/

#undef _mm_log2_pd
#define _mm_log2_pd _mm_log2_pd_impl
__forceinline __m128d _mm_log2_pd_impl(__m128d a)
{
	return _mm_mul_pd(_mm_log_pd(a), __m128d_LOG2EF);
}

#undef _mm_logb_pd
#define _mm_logb_pd _mm_logb_pd_impl
__forceinline __m128d _mm_logb_pd_impl(__m128d a)
{
	return _mm_floor_pd(_mm_log2_pd(_mm_abs_pd(a)));
} 

#undef _mm_log10_pd
#define _mm_log10_pd _mm_log10_pd_impl
__forceinline __m128d _mm_log10_pd_impl(__m128d a)
{
	return _mm_mul_pd(_mm_log_pd(a), __m128d_LOG10EF);
} 

#undef _mm_exp_pd
#define _mm_exp_pd _mm_exp_pd_impl
__forceinline __m128d _mm_exp_pd_impl(__m128d a) {
	__m128d ex, aa, y, z;
	__m128i_i64 emm0;
	
	a = _mm_min_pd(a, __m128d_exp_hi);
	a = _mm_max_pd(a, __m128d_exp_lo);
	
	ex = _mm_fmadd_pd(a, __m128d_LOG2EF, __m128d_1o2);
	ex = _mm_floor_pd(ex);
	
	a = _mm_fnmadd_pd(ex, __m128d_exp_C1,a);
	a = _mm_fnmadd_pd(ex, __m128d_exp_C2,a);
	
	aa = _mm_mul_pd(a,a);
	
	y = __m128d_exp_p0;
	y = _mm_fmadd_pd(y, a, __m128d_exp_p1);
	y = _mm_fmadd_pd(y, a, __m128d_exp_p2);
	y = _mm_fmadd_pd(y, a, __m128d_exp_p3);
	y = _mm_fmadd_pd(y, a, __m128d_exp_p4);
	y = _mm_fmadd_pd(y, a, __m128d_exp_p5);
	y = _mm_fmadd_pd(y, aa, a);
	y = _mm_add_pd(y, __m128d_1);
	
	emm0 = _mm_cvttpd_epi64(ex);
	emm0 = _mm_add_epi64(emm0, __m128i_i64_0x400);
	emm0 = _mm_slli_epi64(emm0, IEE754_64BIT_MANTISSA);
	
	z = _mm_castepi64_pd(emm0);
	
	return _mm_mul_pd(_mm_mul_pd(y, z), __m128d_1o2);
}

#undef _mm_expm1_pd
#define _mm_expm1_pd _mm_expm1_pd_impl
__forceinline __m128d _mm_expm1_pd_impl(__m128d a) {
	return _mm_sub_pd(_mm_exp_pd(a), __m128d_1);
}

#undef _mm_exp2_pd
#define _mm_exp2_pd _mm_exp2_pd_impl
__forceinline __m128d _mm_exp2_pd_impl(__m128d a) {
	return _mm_exp_pd(_mm_mul_pd(__m128d_LOG2, a));
}

#undef _mm_exp10_pd
#define _mm_exp10_pd _mm_exp10_pd_impl
__forceinline __m128d _mm_exp10_pd_impl(__m128d a) {
	return _mm_exp_pd(_mm_mul_pd(__m128d_LOG10, a));
}

#undef _mm_pow_pd
#define _mm_pow_pd _mm_pow_pd_impl
__forceinline __m128d _mm_pow_pd_impl(__m128d a,__m128d b){
	return _mm_exp_pd( _mm_mul_pd(_mm_log_pd(_mm_abs_pd(a)), b) );
}

/*************************************************************************************/
// NOT PART OF SVML 
// Alternate name to exp2
#undef _mm_pow2_pd
#define _mm_pow2_pd _mm_pow2_pd_impl
__forceinline __m128d _mm_pow2_pd_impl(__m128d a){
	return _mm_exp2_pd(a);
}
// NOT PART OF SVML 
// Alternate name to exp10
#undef _mm_pow10_pd
#define _mm_pow10_pd _mm_pow10_pd_impl
__forceinline __m128d _mm_pow10_pd_impl(__m128d a) {
	return _mm_exp10_pd(a);
}
// NOT PART OF SVML 
#undef _mm_nthrt_pd
#define _mm_nthrt_pd _mm_nthrt_pd_impl
__forceinline __m128d _mm_nthrt_pd_impl(__m128d a, __m128d b) {
	__m128d sign_bit = _mm_preservesignbit_pd(a);
	return _mm_effectsignbit_pd(_mm_exp_pd(_mm_mul_pd(_mm_log_pd(_mm_abs_pd(a)), _mm_rcp14_pd(b))), sign_bit);
}
// NOT PART OF SVML 
#undef _mm_rnthrt_pd
#define _mm_rnthrt_pd _mm_rnthrt_pd_impl
__forceinline __m128d _mm_rnthrt_pd_impl(__m128d a, __m128d b) {
	return _mm_rcp14_pd(_mm_nthrt_pd( a, b));
}
/*************************************************************************************/


#undef _mm_cbrt_pd
#define _mm_cbrt_pd _mm_cbrt_pd_impl
__forceinline __m128d _mm_cbrt_pd_impl(__m128d a) {
	__m128d three = __m128d_3;
	return _mm_nthrt_pd(a, three);
}

#undef _mm_invsqrt_pd
#define _mm_invsqrt_pd _mm_invsqrt_pd_impl
__forceinline __m128d _mm_invsqrt_pd_impl(__m128d a) {
	return _mm_rsqrt_pd(a);
}

#undef _mm_invcbrt_pd
#define _mm_invcbrt_pd _mm_invcbrt_pd_impl
__forceinline __m128d _mm_invcbrt_pd_impl(__m128d a) {
	__m128d three = __m128d_3;
	return _mm_rnthrt_pd(a, __m128d_3);
}

#endif

