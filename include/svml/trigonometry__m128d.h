/***************************************************************************************************/
// HEADER_NAME /svml/trigonometry__m128d.h
/***************************************************************************************************/

#if BIT64
#undef _mm_hypot_pd
#define _mm_hypot_pd _mm_hypot_pd_impl
__forceinline __m128d _mm_hypot_pd_impl(__m128d a, __m128d b) {
	return _mm_sqrt_pd( _mm_fmadd_pd( a,a,_mm_mul_pd(b,b) ) );
}

#undef _mm_sin_pd
#define _mm_sin_pd _mm_sin_pd_impl
__forceinline __m128d _mm_sin_pd_impl(__m128d a) {
	__m128d poly_mask, sign_bit, swap_sign, y, y2, z;
	__m128i_i64	emm0, emm1;
	
	sign_bit = _mm_preservesignbit_pd(a);
	
	a = _mm_abs_pd(a);
	
	
	y = _mm_mul_pd(a, __m128d_FoPI);
	
	emm0 = _mm_cvttpd_epi64(y);
	
	emm0 = _mm_add_epi64(emm0, __m128i_i64_1);
	emm0 = _mm_and_epi64(emm0, __m128i_i64_inv1);
	y = _mm_cvtepi64_pd(emm0);
	
	emm1 = _mm_and_epi64(emm0, __m128i_i64_4);
	emm1 = _mm_slli_epi64(emm1, 29);
	
	
	emm0 = _mm_and_epi64(emm0, __m128i_i64_2);
	emm0 = _mm_cmpeq_epi64(emm0, _mm_setzero_epi64());
	
	swap_sign = _mm_castepi64_pd(emm1);
	poly_mask = _mm_castepi64_pd(emm0);
	sign_bit = _mm_xor_pd(sign_bit, swap_sign);
	
	a = _mm_fmadd_pd(y, __m128d_minus_DP1,a);
	a = _mm_fmadd_pd(y, __m128d_minus_DP2, a);
	a = _mm_fmadd_pd(y, __m128d_minus_DP3, a);
	
	y = __m128d_coscof_p0;
	z = _mm_mul_pd(a,a);
	
	y = _mm_fmadd_pd(y, z, __m128d_coscof_p1);
	y = _mm_fmadd_pd(y, z, __m128d_coscof_p2);
	y = _mm_mul_pd(y, z);
	y = _mm_mul_pd(y, z);
	y = _mm_fnmadd_pd(z, __m128d_1o2, y);
	y = _mm_add_pd(y, __m128d_1);
	
	
	y2 = __m128d_sincof_p0;
	y2 = _mm_fmadd_pd(y2, z, __m128d_sincof_p1);
	y2 = _mm_fmadd_pd(y2, z, __m128d_sincof_p2);
	y2 = _mm_mul_pd(y2, z);
	y2 = _mm_fmadd_pd(y2, a, a);
	
	y = _mm_blendv_pd(y, y2, poly_mask);
	
	return _mm_effectsignbit_pd(y, sign_bit);
}

#undef _mm_sind_pd
#define _mm_sind_pd _mm_sind_pd_impl
__forceinline __m128d _mm_sind_pd_impl(__m128d a) {
	return _mm_sin_pd(_mm_mul_pd(a, __m128d_Deg2Rad));
}

#undef _mm_asin_pd
#define _mm_asin_pd _mm_asin_pd_impl
__forceinline __m128d _mm_asin_pd_impl(__m128d a) {
	__m128d flag, sign_bit, w, y, z;
	
	sign_bit = _mm_preservesignbit_pd(a);
	a = _mm_abs_pd(a);
	
	flag = _mm_cmpgt_pd(a, __m128d_1o2);
	z = _mm_mul_pd(__m128d_1o2, _mm_sub_pd(__m128d_1, a));
	w = _mm_sqrt_pd(z);
	a = _mm_blendv_pd(a, w, flag);
	z = _mm_blendv_pd(_mm_mul_pd(a, a), z, flag);
	
	y = _mm_fmadd_pd(z, __m128d_asinf_p0, __m128d_asinf_p1);
	y = _mm_fmadd_pd(y, z, __m128d_asinf_p2);
	y = _mm_fmadd_pd(y, z, __m128d_asinf_p3);
	y = _mm_fmadd_pd(y, z, __m128d_asinf_p4);
	y = _mm_mul_pd(y, z);
	y = _mm_fmadd_pd(y, a, a);
	
	z = _mm_sub_pd( __m128d_PIo2F, _mm_add_pd(y, y));
	y = _mm_blendv_pd(y, z, flag);
	
	return _mm_effectsignbit_pd(sign_bit, y);
}

/***************************************************************************************************/
// NOT PART OF SVML 
// Added For Computer Graphics Support
#undef _mm_radian_asin_pd
#define _mm_radian_asin_pd _mm_radian_asin_pd_impl
__forceinline __m128d _mm_radian_asin_pd_impl(__m128d a)
{
	__m128d y, n, p;
	
	p = _mm_cmpge_pd(a, __m128d_1);
	n = _mm_cmple_pd(a, __m128d_minus_1);
	
	y = _mm_asin_pd(a);
	
	y = _mm_blendv_pd(y, __m128d_PIo2F, p);
	y = _mm_blendv_pd(y, __m128d_minus_PIo2F, n);
	
	return y;
}
/***************************************************************************************************/

#undef _mm_sinh_pd
#define _mm_sinh_pd _mm_sinh_pd_impl
__forceinline __m128d _mm_sinh_pd_impl(__m128d a) {
	// sinh(a) = ((e^a - e ^ (-a)) / 2)
	__m128d ex;
	
	ex = _mm_exp_pd(a);
	return _mm_div_pd(_mm_sub_pd(ex, _mm_rcp14_pd(ex)), __m128d_2);
}

#undef _mm_asinh_pd
#define _mm_asinh_pd _mm_asinh_pd_impl
__forceinline __m128d _mm_asinh_pd_impl(__m128d a) {
	// asinh(a) = ln( a + sqrt(a^2+1) );
	return _mm_log_pd( _mm_add_pd(a, _mm_sqrt_pd(_mm_fmadd_pd( a, a, __m128d_1) )));
}

#undef _mm_cos_pd
#define _mm_cos_pd _mm_cos_pd_impl
__forceinline __m128d _mm_cos_pd_impl(__m128d a) {
	__m128d	poly_mask, sign_bit, y, y2, z;
	__m128i_i64 emm0, emm1;
	
	sign_bit = _mm_preservesignbit_pd(a);
	a = _mm_abs_pd(a);
	
	y = _mm_mul_pd(a, __m128d_FoPI);
	
	emm0 = _mm_cvttpd_epi64(y);
	
	emm0 = _mm_add_epi64(emm0, __m128i_i64_1);
	emm0 = _mm_and_epi64(emm0, __m128i_i64_inv1);
	y = _mm_cvtepi64_pd(emm0);
	
	emm0 = _mm_sub_epi64(emm0, __m128i_i64_2);
	
	emm1 = _mm_andnot_epi64(emm0, __m128i_i64_4);
	emm1 = _mm_slli_epi64(emm1, 29);
	
	emm0 = _mm_and_epi64(emm0, __m128i_i64_2);
	emm0 = _mm_cmpeq_epi64(emm0, _mm_setzero_epi64());
	
	sign_bit = _mm_castepi64_pd(emm1);
	poly_mask = _mm_castepi64_pd(emm0);
	
	a = _mm_fmadd_pd(y, __m128d_minus_DP1, a);
	a = _mm_fmadd_pd(y, __m128d_minus_DP2, a);
	a = _mm_fmadd_pd(y, __m128d_minus_DP3, a);
	
	y = __m128d_coscof_p0;
	z = _mm_mul_pd(a,a);
	
	y = _mm_fmadd_pd(y, z, __m128d_coscof_p1);
	y = _mm_fmadd_pd(y, z, __m128d_coscof_p2);
	y = _mm_mul_pd(y, z);
	y = _mm_mul_pd(y, z);
	y = _mm_fnmadd_pd(z, __m128d_1o2, y);
	y = _mm_add_pd(y, __m128d_1);
	
	y2 = __m128d_sincof_p0;
	
	y2 = _mm_fmadd_pd(y2, z, __m128d_sincof_p1);
	y2 = _mm_fmadd_pd(y2, z, __m128d_sincof_p2);
	y2 = _mm_mul_pd(y2, z);
	y2 = _mm_fmadd_pd(y2, a, a);
	
	y = _mm_blendv_pd(y, y2, poly_mask);

	return _mm_effectsignbit_pd(y, sign_bit);
}

#undef _mm_cosd_pd
#define _mm_cosd_pd _mm_cosd_pd_impl
__forceinline __m128d _mm_cosd_pd_impl(__m128d a) {
	return _mm_cos_pd(_mm_mul_pd(a, __m128d_Deg2Rad));
}

#undef _mm_acos_pd
#define _mm_acos_pd _mm_acos_pd_impl
__forceinline __m128d _mm_acos_pd_impl(__m128d a)
{
	__m128d gt, ngt, lt, mt, p, q, z, s, w, df;
	gt = _mm_cmpgt_pd(a, __m128d_1o2);
	lt = _mm_cmplt_pd(a, __m128d_minus_1);
	ngt = _mm_xor_pd(gt, __m128d_maxint);
	mt = _mm_andnot_pd(lt, ngt);
	
	z = _mm_mul_pd(_mm_add_pd(_mm_or_pd(a, __m128d_sign_mask), __m128d_1), __m128d_1o2);
	z = _mm_blendv_pd(z, _mm_mul_pd(a, a), mt);
	s = _mm_blendv_pd(_mm_sqrt_pd(z), a, mt);
	
	p = _mm_fmadd_pd(z, __m128d_acos_p5, __m128d_acos_p4);
	p = _mm_fmadd_pd(z, p, __m128d_acos_p3);
	p = _mm_fmadd_pd(z, p, __m128d_acos_p2);
	p = _mm_fmadd_pd(z, p, __m128d_acos_p1);
	p = _mm_fmadd_pd(z, p, __m128d_acos_p0);
	p = _mm_mul_pd(z, p);
	
	q = _mm_fmadd_pd(z, __m128d_acos_q4,__m128d_acos_q3);
	q = _mm_fmadd_pd(z, q, __m128d_acos_q2);
	q = _mm_fmadd_pd(z, q, __m128d_acos_q1);
	q = _mm_fmadd_pd(z, q, __m128d_1);
	
	p = _mm_div_pd(p, q);
	
	df = _mm_and_pd(s, _mm_or_pd(__m128d_asinf_trunc, ngt));
	q = _mm_div_pd(_mm_fnmadd_pd(df, df,z), _mm_add_pd(s, df));
	gt = _mm_andnot_pd(_mm_cmpeq_pd(a, __m128d_1), gt);
	q = _mm_or_pd(_mm_and_pd(gt, q), _mm_and_pd(ngt, __m128d_pio2_lo));
	w = _mm_fmadd_pd(p, s, q);
	w = _mm_add_pd(w, df);
	w = _mm_mul_pd(w, _mm_blendv_pd(__m128d_2, __m128d_1, mt));
	w = _mm_xor_pd(w, _mm_and_pd(__m128d_sign_mask, ngt));
	w = _mm_add_pd(w, _mm_and_pd(__m128d_PI, lt));
	w = _mm_add_pd(w, _mm_and_pd(__m128d_pio2_hi, mt));

	return w;
}

/***************************************************************************************************/
// NOT PART OF SVML 
// Added For Computer Graphics Support
#undef _mm_radian_acos_pd
#define _mm_radian_acos_pd _mm_radian_acos_pd_impl
__forceinline __m128d _mm_radian_acos_pd_impl(__m128d a)
{
	__m128d y, n, p;
		
	p = _mm_cmpge_pd(a, __m128d_1);
	n = _mm_cmple_pd(a, __m128d_minus_1);
	
	y = _mm_acos_pd(a);
	
	y = _mm_blendv_pd(y, _mm_setzero_pd(), p);
	y = _mm_blendv_pd(y, __m128d_PI, n);
	
	return y;
}
/***************************************************************************************************/

#undef _mm_cosh_pd
#define _mm_cosh_pd _mm_cosh_pd_impl
__forceinline __m128d _mm_cosh_pd_impl(__m128d a) {
	//cosh(a) = (e^a + e ^ (-a)) / 2
	__m128d ex;

	ex = _mm_exp_pd(a);
	return _mm_div_pd(_mm_add_pd(ex, _mm_rcp14_pd(ex)), __m128d_2);
}

#undef _mm_acosh_pd
#define _mm_acosh_pd _mm_acosh_pd_impl
__forceinline __m128d _mm_acosh_pd_impl(__m128d a) {
	//acosh(a) = ln(a + sqrt(a^2 - 1) )
	return _mm_log_pd(_mm_add_pd(a,_mm_sqrt_pd(_mm_fmsub_pd(a, a,__m128d_1) )));
}

#undef _mm_tan_pd
#define _mm_tan_pd _mm_tan_pd_impl
__forceinline __m128d _mm_tan_pd_impl(__m128d a) {
	__m128d sign_bit, w, y, z, zz;
	__m128i_i64 emm0, emm1;
	
	sign_bit = _mm_preservesignbit_pd(a);
	a = _mm_abs_pd(a);
	
	y = _mm_mul_pd(__m128d_FoPI, a);
	
	emm0 = _mm_cvttpd_epi64(y);
	y = _mm_cvtepi64_pd(emm0);
	
	emm1 = _mm_and_epi64(emm0, __m128i_i64_1);
	emm1 = _mm_cmpeq_epi64(emm1, __m128i_i64_1);
	emm0 = _mm_add_epi64(emm0, _mm_and_epi64(emm1, __m128i_i64_1));
	y = _mm_add_pd(y, _mm_and_pd(_mm_cvtepi64_pd(emm1), __m128d_1));
	
	z = _mm_fmadd_pd(y, __m128d_minus_DP1, a);
	z = _mm_fmadd_pd(y, __m128d_minus_DP2, z);
	z = _mm_fmadd_pd(y, __m128d_minus_DP3, z);
	
	zz = _mm_mul_pd(z, z);
	
	y = _mm_fmadd_pd(__m128d_tanf_p0, zz, __m128d_tanf_p1);
	y = _mm_fmadd_pd(y, zz, __m128d_tanf_p2);
	y = _mm_fmadd_pd(y, zz, __m128d_tanf_p3);
	y = _mm_fmadd_pd(y, zz, __m128d_tanf_p4);
	y = _mm_fmadd_pd(y, zz, __m128d_tanf_p5);
	y = _mm_mul_pd(y, zz);
	y = _mm_fmadd_pd(y, z, z);
	
	z = _mm_rcp14_pd(y);
	z = _mm_chs_pd(z);
	
	emm1 = _mm_and_epi64(emm0, __m128i_i64_2);
	emm1 = _mm_cmpeq_epi64(emm1, __m128i_i64_2);
	w = _mm_castepi64_pd(emm1);
	y = _mm_blendv_pd(y, z, w);
	
	return _mm_effectsignbit_pd(sign_bit, y);
}

#undef _mm_tand_pd
#define _mm_tand_pd _mm_tand_pd_impl
__forceinline __m128d _mm_tand_pd_impl(__m128d a) {
	return _mm_tan_pd(_mm_mul_pd(a, __m128d_Deg2Rad));
}

#undef _mm_atan_pd
#define _mm_atan_pd _mm_atan_pd_impl
__forceinline __m128d _mm_atan_pd_impl(__m128d a) {
	__m128d sign_bit, w, x, y, aa, am1, ap1, z;
	
	sign_bit = _mm_preservesignbit_pd(a);
	a = _mm_abs_pd(a);
	
	w = _mm_cmpgt_pd(a, __m128d_TAN3PIo8);
	x = _mm_cmpgt_pd(a, __m128d_TANPIo8);
	z = _mm_andnot_pd(w, x);
	
	y = _mm_and_pd(w, __m128d_PIo2F);
	y = _mm_or_pd(y, _mm_and_pd(z, __m128d_PIo4F));
	
	w = _mm_and_pd(w, _mm_chs_pd(_mm_rcp14_pd(a)));
	am1 = _mm_sub_pd(a, __m128d_1);
	ap1 = _mm_add_pd(a, __m128d_1);
	w = _mm_or_pd(w, _mm_and_pd(z, _mm_mul_pd(am1, _mm_rcp14_pd(ap1))));
	
	a = _mm_or_pd(_mm_andnot_pd(x, a), w);
	aa = _mm_mul_pd(a, a);
	
	z = _mm_fmadd_pd(__m128d_atanf_p0, aa,__m128d_atanf_p1);
	z = _mm_fmadd_pd(z, aa, __m128d_atanf_p2);
	z = _mm_fmadd_pd(z, aa, __m128d_atanf_p3);
	z = _mm_mul_pd(z, aa);
	z = _mm_fmadd_pd(z, a, a);
	y = _mm_add_pd(y, z);
	
	return _mm_effectsignbit_pd(sign_bit, y);
}

#undef _mm_atan2_pd
#define _mm_atan2_pd _mm_atan2_pd_impl
__forceinline __m128d _mm_atan2_pd_impl(__m128d a, __m128d b) {
	__m128d sign_bit, y, y2, z, w, w2;
	
	w = _mm_cmplt_pd(b, _mm_setzero_pd());
	y = _mm_cmplt_pd(a, _mm_setzero_pd());
	w2 = _mm_cmpeq_pd(b, _mm_setzero_pd());
	y2 = _mm_cmpeq_pd(a, _mm_setzero_pd());
	
	sign_bit = _mm_and_pd(y, __m128d_sign_mask);
	w = _mm_and_pd(w, __m128d_PI);
	w = _mm_or_pd(w, sign_bit);
	z = _mm_atan_pd(_mm_mul_pd(a, _mm_rcp14_pd(b)));
	w = _mm_add_pd(w, z);
	
	y = _mm_andnot_pd(y, __m128d_PIo2F);
	y = _mm_or_pd(y, sign_bit);
	y = _mm_andnot_pd(y2, y);

	return _mm_blendv_pd(w, y, w2);

}

#undef _mm_tanh_pd
#define _mm_tanh_pd _mm_tanh_pd_impl
__forceinline __m128d _mm_tanh_pd_impl(__m128d a) {
	// tanh(a) = e^2x - 1 / e^2x + 1 
	__m128d ex = _mm_exp_pd(_mm_mul_pd(a, __m128d_2));
	return _mm_div_pd( _mm_sub_pd(ex, __m128d_1	), _mm_add_pd(ex, __m128d_1));
}

#undef _mm_atanh_pd
#define _mm_atanh_pd _mm_atanh_pd_impl
__forceinline __m128d _mm_atanh_pd_impl(__m128d a) {
	// artanh(a) = 0.5*ln[(1+a)/(1-a)] 
	return _mm_mul_pd(__m128d_1o2,_mm_log_pd(_mm_div_pd(_mm_add_pd(__m128d_1, a), _mm_sub_pd(__m128d_1, a))));
}

#undef _mm_sincos_pd
#define _mm_sincos_pd _mm_sincos_pd_impl
__forceinline __m128d _mm_sincos_pd_impl(__m128d *mem_addr, __m128d a) {
	__m128d cos_sign_bit, sin_sign_bit, poly_mask, aa, y, y2, z, z2;
	__m128i_i64 emm0, emm1, emm3;
	
	sin_sign_bit = _mm_preservesignbit_pd(a);
	
	a = _mm_abs_pd(a);
	
	y = _mm_mul_pd(a, __m128d_FoPI);
	
	emm0 = _mm_cvttpd_epi64(y);
	emm0 = _mm_add_epi64(emm0, __m128i_i64_1);
	emm0 = _mm_and_epi64(emm0, __m128i_i64_inv1);
	y = _mm_cvtepi64_pd(emm0);
	
	emm1 = emm0;
	
	emm3 = _mm_and_epi64(emm0, __m128i_i64_4);
	emm3 = _mm_slli_epi64(emm3, 29);
	
	emm0 = _mm_and_epi64(emm0, __m128i_i64_2);
	emm0 = _mm_cmpeq_epi64(emm0, _mm_setzero_epi64());
	
	poly_mask = _mm_castepi64_pd(emm0);
	
	a = _mm_fmadd_pd(y, __m128d_minus_DP1, a);
	a = _mm_fmadd_pd(y, __m128d_minus_DP2, a);
	a = _mm_fmadd_pd(y, __m128d_minus_DP3, a);
	
	emm1 = _mm_sub_epi64(emm1, __m128i_i64_2);
	emm1 = _mm_andnot_epi64(emm1, __m128i_i64_4);
	emm1 = _mm_slli_epi64(emm1, 29);
	
	cos_sign_bit = _mm_castepi64_pd(emm1);
	sin_sign_bit = _mm_xor_pd(sin_sign_bit, _mm_castepi64_pd(emm3));
	
	
	aa = _mm_mul_pd(a, a);
	
	y = __m128d_coscof_p0;
	y = _mm_fmadd_pd(y, aa, __m128d_coscof_p1);
	y = _mm_fmadd_pd(y, aa, __m128d_coscof_p2);
	y = _mm_mul_pd(y, aa);
	y = _mm_mul_pd(y, aa);
	y = _mm_fnmadd_pd(aa, __m128d_1o2, y);
	y = _mm_add_pd(y, __m128d_1);
	
	y2 = __m128d_sincof_p0;
	y2 = _mm_fmadd_pd(y2, aa, __m128d_sincof_p1);
	y2 = _mm_fmadd_pd(y2, aa, __m128d_sincof_p2);
	y2 = _mm_mul_pd(y2, aa);
	y2 = _mm_fmadd_pd(y2, a, a);
	
	z = _mm_and_pd(poly_mask, y2);
	z2 = _mm_andnot_pd(poly_mask, y);
	
	y2 = _mm_sub_pd(y2, z);
	y = _mm_sub_pd(y, z2);
	
	y = _mm_add_pd(y, y2);
	y2 = _mm_add_pd(z2, z);
	
	*mem_addr = _mm_effectsignbit_pd(y, cos_sign_bit);
	
	return	_mm_effectsignbit_pd(y2, sin_sign_bit);
}
#endif

